const config     = require("../config/index.js");
const nodemailer = require('nodemailer');

// variables constantes 
const USUARIO_CORREO  =  'staff.fastenglish@gmail.com'
const PASSWORD_CORREO =  'jzxmdixwyauwxfbf'
const ESCUELA =          'FAST ENGLISH'
const LOGO =             'https://fastenglish.com.mx/public/images/logo.png'
const FACEBOOK =         'https://www.facebook.com/fastenglishmty'
const INSTAGRAM =        'https://instagram.com/fastenglishschool?utm_medium=copy_link'
const ICONO_FACEBOOK =   'https://fastenglish.com.mx/public/images/ico_facebook.jpg'
const ICONO_INSTAGRAM =  'https://fastenglish.com.mx/public/images/ico_instagram.jpg'

//const constructor
const helperCorreoBienvenida = {

	enviarCorreoBienvenida ( payload ) {
		let nombre, matricula = ''
		const correo = `
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
				<head>
					<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
				  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
					<meta name="format-detection" content="date=no" />
					<meta name="format-detection" content="address=no" />
					<meta name="format-detection" content="telephone=no" />
					<meta name="x-apple-disable-message-reformatting" />
					<link href="https://fonts.googleapis.com/css?family=Merriweather:400,400i,700,700i" rel="stylesheet" />

					<style type="text/css" media="screen">
						/* Linked Styles */
						body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#001f51; -webkit-text-size-adjust:none }
						a { color:#000001; text-decoration:none }
						p { padding:0 !important; margin:0 !important } 
						img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
						.mcnPreviewText { display: none !important; }

						.cke_editable,
						.cke_editable a,
						.cke_editable span,
						.cke_editable a span { color: #000001 !important; }		
						/* Mobile styles */
						@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
							.mobile-shell { width: 100% !important; min-width: 100% !important; }
							.bg { background-size: 100% auto !important; -webkit-background-size: 100% auto !important; }
							
							.text-header,
							.m-center { text-align: center !important; }
							
							.center { margin: 0 auto !important; }
							.container { padding: 20px 10px !important }
							
							.td { width: 100% !important; min-width: 100% !important; }

							.m-br-15 { height: 15px !important; }
							.p30-15 { padding: 30px 15px !important; }
							.p0-15-30 { padding: 0px 15px 30px 15px !important; }
							.mpb30 { padding-bottom: 30px !important; }

							.m-td,
							.m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

							.m-block { display: block !important; }

							.fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }

							.column,
							.column-dir,
							.column-top,
							.column-empty,
							.column-empty2,
							.column-dir-top { float: left !important; width: 100% !important; display: block !important; }

							.column-empty { padding-bottom: 30px !important; }
							.column-empty2 { padding-bottom: 10px !important; }

							.content-spacing { width: 15px !important; }
						}
					</style>
				</head>
				<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#F1F1F1; -webkit-text-size-adjust:none;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#E4E4E4">
						<tr>
							<td align="center" valign="top">
								<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
									<tr>
										<td class="td container" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; margin:0; font-weight:normal; padding:55px 0px;">


											<!-- LOGO DE FAST ENGLISH -->
											<!-- Header -->
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td class="p30-15 tbrr" style="padding: 30px; border-radius:12px 12px 0px 0px;" bgcolor="#ffffff">
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<th class="column-top" width="165" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td bgcolor="#ffffff" class="p30-15 img-center" style="padding: 30px; border-radius: 20px 20px 0px 0px; font-size:0pt; line-height:0pt; text-align:center;">
																			<a href="https://www.fastenglish.com.mx/" target="_blank"><img src="https://fastenglish.com.mx/public/images/logo_fast.png" width="165" height="145" mc:edit="image_6" border="0" alt="" /></a></td>
																		</tr>
																	</table>
																</th>
																<th class="column-empty2" width="1" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;"></th>
															</tr>
														</table>
													</td>
												</tr>
											</table>
											<!-- END Header -->
											
											<!-- Hero Image -->
											<div mc:repeatable="Select" mc:variant="Hero Image">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td class="fluid-img" style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://fastenglish.com.mx/public/bienvenida/banner.jpg" border="0" width="650" height="366" mc:edit="image_2" style="max-width:650px;" alt="" /></td>
													</tr>
												</table>
											</div>
											<!-- END Hero Image -->
											
											<!-- Intro -->
											<div mc:repeatable="Select" mc:variant="Intro">
												<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#001037">
													<tr>
														<td style="padding-bottom: 10px;">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="p30-15" style="padding: 60px 30px;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td class="h1 pb25" style="color:#ffffff; font-family:'Merriweather', Georgia,serif; font-size:35px; line-height:42px; text-align:center; padding-bottom:25px;"><div mc:edit="text_2">Bienvienido a tu curso</div></td>
																			</tr>
																			<tr>
																				<td class="text-center pb25" style="color:#ffffff; font-family:Arial,sans-serif; font-size:16px; line-height:30px; text-align:center; padding-bottom:25px;">
																					<div mc:edit="text_3">Hola, ${ nombre } 
																						<span class="m-hide">
																							<br />
																						</span>
																						Tu matricula es: ${ matricula }
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td class="text white pb20" style="font-family:Arial,sans-serif; font-size:14px; line-height:26px; text-align:left; color:#ffffff; padding-bottom:20px;"><div mc:edit="text_6">A continuación te explicaremos paso a paso como ingresar a tu plataforma por primera vez</div></td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</div>
											<!-- END Intro -->
											
											<!-- Article / Blue Background -->
											<div mc:repeatable="Select" mc:variant="Article / Blue Background">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td bgcolor="#fff">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<th class="column" width="50" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;"></th>
																	<th class="column" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td class="p30-15" style="padding: 20px 0px;">
																					<table width="100%" border="0" cellspacing="0" cellpadding="0">
																						<tr>
																							<td class="h3 white pb20" style="font-family:'Merriweather', Georgia,serif; font-size:25px; line-height:32px; text-align:left; color:#000000; padding-bottom:20px;">
																								<div mc:edit="text_5">1-. Descarga la aplicación</div>
																							</td>
																						</tr>

																						<tr>
																							<td class="pb20" style="padding-bottom:20px;">
																								<table width="100%" border="0" cellspacing="0" cellpadding="0">
																									<tr mc:repeatable>
																										<td class="pb10" style="padding-bottom:10px;">
																											<table width="100%" border="0" cellspacing="0" cellpadding="0">
																												<tr>
																													<td valign="top" class="img" width="20" style="font-size:0pt; line-height:0pt; text-align:left;"></td>
																													<td valign="top" class="img" width="24" style="font-size:0pt; line-height:0pt; text-align:left;">
																														<img src="https://fastenglish.com.mx/public/images/t3_ico_yellow_check.png" width="12" height="15" mc:edit="image_4" style="max-width:12px;" border="0" alt="" />
																													</td>
																													<td valign="top" class="text2 white" style="font-family:'Merriweather', Georgia,serif; font-size:14px; line-height:26px; text-align:left; font-style:italic; color:#000000;">
																														<div mc:edit="text_7">
																															Descarga la aplicación directo desde las siguientes plataformas
																														</div>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																						<!-- Button -->
																						<tr mc:hideable>
																							<td align="left">
																								<table border="0" cellspacing="0" cellpadding="0">
																									<tr >
																										<td class="text-button" style="background:#009507; color:#fff; font-family:'Merriweather', Georgia,serif; font-size:14px; line-height:18px; padding:12px 15px; text-align:center; border-radius:10px; text-transform:uppercase; margin-right: 25px ; ">
																											<div mc:edit="text_11">
																												<a href="https://play.google.com/store/apps/details?id=app.movilfast.com&hl=es_MX" target="_blank" class="link" style="color:#fff; text-decoration:none;">
																													<span 	class="link" style="color:#fff; text-decoration:none; ">
																														Play Store
																													</span>
																												</a>
																											</div>
																										</td>

																										<td class="text-button" style="background:transparent; color:#444444; font-family:'Merriweather', Georgia,serif; font-size:14px; line-height:18px; padding:12px 15px; text-align:center; border-radius:10px; text-transform:uppercase; margin-right: 25px ; ">
																										</td>

																										<td class="text-button" style="background:#0070C2; color:#fff; font-family:'Merriweather', Georgia,serif; font-size:14px; line-height:18px; padding:12px 15px; text-align:center; border-radius:10px; text-transform:uppercase;  margin-left: 25px ;">
																											<div mc:edit="text_11">
																												<a href="https://apps.apple.com/us/app/fast-english/id1618828260?platform=iphone" target="_blank" class="link" style="color:#fff; text-decoration:none;">
																													<span 	class="link" style="color:#fff; text-decoration:none; ">
																														App Store
																													</span>
																												</a>
																											</div>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																						<!-- END Button -->
																					</table>
																				</td>
																			</tr>
																		</table>
																	</th>
																	<th class="column" width="50" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;"></th>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</div>
											<!-- END Article / Blue Background -->
											
											<!-- Article / White Background -->
											<div mc:repeatable="Select" mc:variant="Article / White Background">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td bgcolor="#001037">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" dir="rtl" style="direction: rtl;">
																<tr>
																	<th class="column-dir" dir="ltr" width="50" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																	<th class="column-dir" dir="ltr" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td class="p30-15" style="padding: 20px 0px;">
																					<table width="100%" border="0" cellspacing="0" cellpadding="0">
																						<tr>
																							<td class="h3 pb20" style="color:#ffffff; font-family:'Merriweather', Georgia,serif; font-size:25px; line-height:32px; text-align:left; padding-bottom:20px;">
																								<div mc:edit="text_12">2-. Regístrate</div>
																							</td>
																						</tr>
																						<tr>
																							<td class="text pb20" style="color:#ffffff; font-family:Arial,sans-serif; font-size:14px; line-height:26px; text-align:left; padding-bottom:20px;">
																								<div mc:edit="text_13">Una vez que hayas descargado la aplicación, deberás ingresar usando tu matricula y contraseña, utiliza los siguientes datos para ingresar.
																								</div>
																							</td>
																						</tr>
																						<tr>
																							<td class="pb20" style="padding-bottom:20px;">
																								<table width="100%" border="0" cellspacing="0" cellpadding="0">
																									<tr mc:repeatable>
																										<td class="pb10" style="padding-bottom:10px;">
																											<table width="100%" border="0" cellspacing="0" cellpadding="0">
																												<tr>
																													<td valign="top" class="img" width="20" style="font-size:0pt; line-height:0pt; text-align:left;"></td>
																													<td valign="top" class="img" width="24" style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://fastenglish.com.mx/public/images/t3_ico_blue_check.png" width="12" height="15" mc:edit="image_9" style="max-width:12px;" border="0" alt="" /></td>
																													<td valign="top" class="text2" style="color:#ffffff; font-family:'Merriweather', Georgia,serif; font-size:14px; line-height:26px; text-align:left; font-style:italic;"><div mc:edit="text_14">Matricula: ${ matricula } </div></td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<tr mc:repeatable>
																										<td class="pb10" style="padding-bottom:10px;">
																											<table width="100%" border="0" cellspacing="0" cellpadding="0">
																												<tr>
																													<td valign="top" class="img" width="20" style="font-size:0pt; line-height:0pt; text-align:left;"></td>
																													<td valign="top" class="img" width="24" style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://fastenglish.com.mx/public/images/t3_ico_blue_check.png" width="12" height="15" mc:edit="image_10" style="max-width:12px;" border="0" alt="" /></td>
																													<td valign="top" class="text2" style="color:#ffffff; font-family:'Merriweather', Georgia,serif; font-size:14px; line-height:26px; text-align:left; font-style:italic;"><div mc:edit="text_15">Contraseña: ${ matricula }</div></td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</th>
																	<th class="column" width="50" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;"></th>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</div>
											<!-- END Article / White Background -->
											
											<!-- Article / Purple Background -->
											<div mc:repeatable="Select" mc:variant="Article / Purple Background">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td bgcolor="#ffffff">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<th class="column" width="50" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;"></th>
																	<th class="column" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td class="p30-15" style="padding: 20px 0px;">
																					<table width="100%" border="0" cellspacing="0" cellpadding="0">
																						<tr>
																							<td class="h3 white pb20" style="font-family:'Merriweather', Georgia,serif; font-size:25px; line-height:32px; text-align:left; color:#000000; padding-bottom:20px;">
																								<div mc:edit="text_19">3-. Confirmación de cuenta</div>
																							</td>
																						</tr>
																						<tr>
																							<td class="text white pb20" style="font-family:Arial,sans-serif; font-size:14px; line-height:26px; text-align:left; color:#000000; padding-bottom:20px;">
																								<div mc:edit="text_20">Después de haber ingresado a la aplicación, se te solicitarán algunos datos para confirmar tu cuenta, .</div>
																							</td>
																						</tr>
																						<tr>
																							<td class="pb20" style="padding-bottom:20px;">
																								<table width="100%" border="0" cellspacing="0" cellpadding="0">
																									<tr mc:repeatable>
																										<td class="pb10" style="padding-bottom:10px;">
																											<table width="100%" border="0" cellspacing="0" cellpadding="0">
																												<tr>
																													<td valign="top" class="img" width="20" style="font-size:0pt; line-height:0pt; text-align:left;"></td>
																													<td valign="top" class="img" width="24" style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://fastenglish.com.mx/public/images/t3_ico_yellow_check.png" width="12" height="15" mc:edit="image_14" style="max-width:12px;" border="0" alt="" /></td>
																													<td valign="top" class="text2 white" style="font-family:'Merriweather', Georgia,serif; font-size:14px; line-height:26px; text-align:left; font-style:italic; color:#000000;"><div mc:edit="text_21">Te llegará un correo de confirmación a tu bandeja de entrada</div></td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<tr mc:repeatable>
																										<td class="pb10" style="padding-bottom:10px;">
																											<table width="100%" border="0" cellspacing="0" cellpadding="0">
																												<tr>
																													<td valign="top" class="img" width="20" style="font-size:0pt; line-height:0pt; text-align:left;"></td>
																													<td valign="top" class="img" width="24" style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://fastenglish.com.mx/public/images/t3_ico_yellow_check.png" width="12" height="15" mc:edit="image_15" style="max-width:12px;" border="0" alt="" /></td>
																													<td valign="top" class="text2 white" style="font-family:'Merriweather', Georgia,serif; font-size:14px; line-height:26px; text-align:left; font-style:italic; color:#000000;"><div mc:edit="text_22">Da clic en el botón de confirumar cuenta</div></td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<tr mc:repeatable>
																										<td class="pb10" style="padding-bottom:10px;">
																											<table width="100%" border="0" cellspacing="0" cellpadding="0">
																												<tr>
																													<td valign="top" class="img" width="20" style="font-size:0pt; line-height:0pt; text-align:left;"></td>
																													<td valign="top" class="img" width="24" style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://fastenglish.com.mx/public/images/t3_ico_yellow_check.png" width="12" height="15" mc:edit="image_16" style="max-width:12px;" border="0" alt="" /></td>
																													<td valign="top" class="text2 white" style="font-family:'Merriweather', Georgia,serif; font-size:14px; line-height:26px; text-align:left; font-style:italic; color:#000000;"><div mc:edit="text_23">¡Listo! Tu cuenta estará activa</div></td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</th>
																	<th class="column" width="50" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;"></th>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</div>
											<!-- END Article / Purple Background -->
											
											<!-- Article Secondary / White Background -->
											<div mc:repeatable="Select" mc:variant="Article Secondary / White Background">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td bgcolor="#001037">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" dir="rtl" style="direction: rtl;">
																<tr>
																	<th class="column-dir" dir="ltr" width="50" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																	<th class="column-dir" dir="ltr" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td class="p30-15" style="padding: 20px 0px;">
																					<table width="100%" border="0" cellspacing="0" cellpadding="0">
																						<tr>
																							<td class="h3 pb20" style="color:#ffffff; font-family:'Merriweather', Georgia,serif; font-size:25px; line-height:32px; text-align:left; padding-bottom:20px;">
																								<div mc:edit="text_26">4-. Realiza tu curso</div>
																							</td>
																						</tr>
																						<tr>
																							<td class="text pb20" style="color:#ffffff; font-family:Arial,sans-serif; font-size:14px; line-height:26px; text-align:left; padding-bottom:20px;"><div mc:edit="text_27">Excelente, ahora, podrás ingresar a tu aplicación nuevamente y difrutar de todo su contenido</div></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</th>
																	<th class="column" width="50" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;"></th>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</div>
											<!-- END Article Secondary / White Background -->
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</body>
			</html>

		`
	  return correo
	},

	enviarCorreo ( asunto, correoHtml, correo, transporter, archivos ) {
		return new Promise(async (resolve, reject)=>{
	    // Preparamos las opciones del correo
	    const mailOptions = {
	      from:        `${ ESCUELA } <info@testsnode.com>`,
	      to:          correo,
	      subject:     asunto,
	      html:        correoHtml,
	      attachments: archivos ? archivos : []
	    }

	    // Enviamos el correo, PARAMETRO: mailOptions
	    await transporter.sendMail(mailOptions, (error, info) =>{
        if (error) {
        	
          return reject(error)
        } else {
        	
          return resolve(info.response)
        }
	    });

		})
	},
};


module.exports = helperCorreoBienvenida;