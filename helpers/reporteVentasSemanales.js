const PDFDocument = require('pdfkit');
const fs = require('fs');


const generateReporteVentasSemanal = ({ infoTabla1, resultado1, infoTabla2, resultado2, resultado3, nombre1, nombre2, infoTexto, infoTabla3, infoTabla4, imageData, imageData2, porcentajeMensajeGrafica, ventaGeneral, infoTablaEspecial, escuela }) => {
    ventas_semana = infoTabla1;
    mensaje_ventas_semana = resultado1;
    recuadro = infoTexto;
    ventas_vendedora = infoTabla2;
    mensaje_ventas_vendedora = resultado2;
    porcentaje_ventas = resultado3;
    menos_ventas1 = nombre1;
    menos_ventas2 = nombre2;
    contactos_vendedora = infoTabla3;
    inscritos_vendedora = infoTabla4;
    grafica1 = imageData;
    grafica2 = imageData2;
    porcentajeContactos = porcentajeMensajeGrafica;
    ventaGenerals = ventaGeneral;
    especial = infoTablaEspecial;

    let ultimoObjeto = {}

    if( escuela == 1 ){

        ultimoObjeto = especial.find( el => el.vendedora.match('INBI') );
    }else{
        ultimoObjeto = especial.find( el => el.vendedora.match('FAST') );
    }

    const conteoSemanaActual = ultimoObjeto.conteo_semana_actual;
    const conteoSemanaAnterior = ultimoObjeto.conteo_semana_anterior;

    const doc = new PDFDocument();

    // Nombre del y lugar del documento con número de folio
    doc.pipe(fs.createWriteStream(`../../recibos-pagos/NombrePdf.pdf`));

    //Tabla Ventas por semana

    doc.fontSize(14).font('Helvetica-Bold').text(`Ventas panorama general`, 50, 30, { align: 'left' });
    doc.moveDown();

    doc.font('Helvetica-Bold').fontSize(10); 
    const tableHeaders6 = ['Semana Ant', 'Semana Ant', 'Semana Ant', 'Semana Ant', 'Semana Ant', 'Semana Act'];
    let yPosition6 = 60;

    doc.font('Helvetica-Bold');
    tableHeaders6.forEach((header, index) => {
        const xPosition = 50 + index * 80; // Adjusted x position
        doc.rect(xPosition, yPosition6, 80, 25).stroke();
        doc.text(header, xPosition + 5, yPosition6 + 8, { width: 80, align: 'center' });
    });

    // Create the table rows
    doc.font('Helvetica').fontSize(9); // Reduced font size
    ventaGenerals.forEach((venta, rowIndex) => {
        yPosition6 += 25; // Reduced row height
        const { columna7, columna6, columna5, columna4, columna2, columna3 } = venta;
        const rowData = [columna7, columna6, columna5, columna4, columna2, columna3];
        rowData.forEach((data, columnIndex) => {
            const xPosition = 50 + columnIndex * 80; // Adjusted x position
            doc.rect(xPosition, yPosition6, 80, 25).stroke();
            doc.text(data || '', xPosition + 1, yPosition6 + 5, { width: 80, align: 'center' });
        });
    });

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Gráfica de Contactos

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    const grafica_ventas = grafica2

    doc.image(grafica_ventas, {
        fit: [500, 250], // Tamaño de la imagen en el PDF
        align: 'center', // Alineación en el centro
        valign: 'center', // Alineación vertical en el centro
        pageNumber: 1, // Página en la que deseas agregar la imagen
        x: 50
    });

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    // FOLIO DE PAGO
    doc.fontSize(14).font('Helvetica-Bold').text(`Ventas de semana a semana`, 50, 380, { align: 'left' });
    doc.moveDown();

    const mensaje_ventas_semanaX = 50;
    const mensaje_ventas_semanaY = 650;
    const mensaje_ventas_semanaWidth = 250;
    const mensaje_ventas_semanaHeight = 30;

    doc.rect(mensaje_ventas_semanaX, mensaje_ventas_semanaY, mensaje_ventas_semanaWidth, mensaje_ventas_semanaHeight).stroke();

    const mensaje_ventas_semanadato = `${mensaje_ventas_semana}`;
    const mensaje_ventas_semanadatoPosX = mensaje_ventas_semanaX + 10;
    const mensaje_ventas_semanadatoPosY = mensaje_ventas_semanaY + 10;

    doc.text(mensaje_ventas_semanadato, mensaje_ventas_semanadatoPosX, mensaje_ventas_semanadatoPosY, { width: mensaje_ventas_semanaWidth - 20, align: 'center' });

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Tabla Ventas por semana


    const tableHeaders = ['Campo', 'Semana Ant', 'Semana Act', 'Avance'];


    let yPosition = 435;

    doc.font('Helvetica-Bold');
    tableHeaders.forEach((header, index) => {
        doc.rect(50 + index * 100, yPosition, 100, 30).stroke();
        doc.text(header, 40 + index * 100 + 10, yPosition + 10, { width: 100, align: 'center' });
    });

    // Create the table rows
    doc.font('Helvetica').fontSize(8);
    ventas_semana.forEach((venta, rowIndex) => {
        yPosition += 30;
        const { columna1, columna2, columna3, resultado } = venta;
        const rowData = [columna1, columna2, columna3, resultado];
        rowData.forEach((data, columnIndex) => {
            doc.rect(50 + columnIndex * 100, yPosition, 100, 30).stroke();
            doc.text(data || '', 40 + columnIndex * 100 + 10, yPosition + 10, { width: 100, align: 'center' });
        });
    });

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    doc.fontSize(14).font('Helvetica-Bold')
    
    const recuadroX = 300;
    const recuadroY = 380;
    const recuadroWidth = 290;
    const recuadroHeight = 30;

    doc.rect(recuadroX, recuadroY, recuadroWidth, recuadroHeight).stroke();

    const dato = `${recuadro}`;
    const datoPosX = recuadroX + 10;
    const datoPosY = recuadroY + 10;

    doc.text(dato, datoPosX, datoPosY, { width: recuadroWidth - 20, align: 'center' });

    doc.addPage();

    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Tabla Ventas por semana

    doc.font('Helvetica-Bold').fontSize(14); // Aplicar fuente en negritas y tamaño de letra
    doc.text('Ventas semana a semana Encargadas', 35, 30, { width: 300, align: 'center' });

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    doc.font('Helvetica').fontSize(8);

    const tableHeaders2 = ['Vendedora', 'Mat.Ant.Liq', 'Mat.Nvas.Liq', 'Diferencia'];

    let yPosition2 = 60;
    const cellHeight = 20;

    doc.font('Helvetica-Bold');
    tableHeaders2.forEach((header, index) => {
        doc.rect(50 + index * 100, yPosition2, 100, cellHeight).stroke();
        doc.text(header, 50 + index * 100 + 4, yPosition2 + 4, { width: 100, align: 'center' });
    });

    doc.font('Helvetica').fontSize(8);

    ventas_vendedora.forEach((venta, rowIndex) => {
        yPosition2 += cellHeight;
        const { nombre_completo, matriculas_liquidadasSemAnt, matriculas_liquidadas, resultado } = venta;
        const rowData = [nombre_completo, matriculas_liquidadasSemAnt, matriculas_liquidadas, resultado];

        const isLastRow = rowIndex === ventas_vendedora.length - 1;

        rowData.forEach((data, columnIndex) => {
            const cellX = 50 + columnIndex * 100;
            doc.rect(cellX, yPosition2, 100, cellHeight).stroke();

            if (isLastRow) {
                // Pintar el fondo en amarillo para la última fila
                doc.fillColor('#FFFF00'); // Amarillo
                doc.rect(cellX, yPosition2, 100, cellHeight).fillAndStroke();
                doc.fillColor('#000000'); // Cambia el color del texto a tu preferencia
            } else if (columnIndex === tableHeaders2.length - 1) {
                // Pintar el fondo en rojo para números negativos y verde para números positivos
                if (data < 0) {
                    doc.fillColor('#FF0000'); // Rojo
                } else if (data > 0) {
                    doc.fillColor('#00FF00'); // Verde
                }
                if (data !== 0) {
                    doc.rect(cellX, yPosition2, 100, cellHeight).fillAndStroke();
                }
                doc.fillColor('#000000'); // Cambia el color del texto a tu preferencia
            }

            doc.text(data || '', cellX + 4, yPosition2 + 4, { width: 100, align: 'center' });
        });
    });

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    doc.font('Helvetica-Bold').fontSize(10); 
    const mensaje_ventas_vendedoraX = 50;
    const mensaje_ventas_vendedoraY = 340;
    const mensaje_ventas_vendedoraWidth = 250;
    const mensaje_ventas_vendedoraHeight = 35;

    doc.rect(mensaje_ventas_vendedoraX, mensaje_ventas_vendedoraY, mensaje_ventas_vendedoraWidth, mensaje_ventas_vendedoraHeight).stroke();

    const mensaje_ventas_vendedoradato = `${porcentaje_ventas}`;
    const mensaje_ventas_vendedoraPosX = mensaje_ventas_vendedoraX + 10;
    const mensaje_ventas_vendedoraPosY = mensaje_ventas_vendedoraY + 10;

    doc.text(mensaje_ventas_vendedoradato, mensaje_ventas_vendedoraPosX, mensaje_ventas_vendedoraPosY, { width: mensaje_ventas_vendedoraWidth - 20, align: 'center' });

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    const mensaje_ventass_vendedoraX = 320;
    const mensaje_ventass_vendedoraY = 340;
    const mensaje_ventass_vendedoraWidth = 250;
    const mensaje_ventass_vendedoraHeight = 45;

    doc.rect(mensaje_ventass_vendedoraX, mensaje_ventass_vendedoraY, mensaje_ventass_vendedoraWidth, mensaje_ventass_vendedoraHeight).stroke();

    const mensaje_ventass_vendedoradato = `${menos_ventas1} y ${menos_ventas2} son las que menos vendieron`;
    const mensaje_ventass_vendedoraPosX = mensaje_ventass_vendedoraX + 10;
    const mensaje_ventass_vendedoraPosY = mensaje_ventass_vendedoraY + 10;

    doc.text(mensaje_ventass_vendedoradato, mensaje_ventass_vendedoraPosX, mensaje_ventass_vendedoraPosY, { width: mensaje_ventass_vendedoraWidth - 20, align: 'center' });

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    doc.fontSize(14).font('Helvetica-Bold').text(`Contactos por semana`, 50, 400, { align: 'left' });
   

    const grafica_contactos = grafica1

    doc.image(grafica_contactos, {
        fit: [500, 250], // Tamaño de la imagen en el PDF
        align: 'center', // Alineación en el centro
        valign: 'center', // Alineación vertical en el centro
        pageNumber: 1 // Página en la que deseas agregar la imagen
    });

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    const mensaje_contacto_siguienteX = 140;
    const mensaje_contacto_siguienteY = 640;
    const mensaje_contacto_siguienteWidth = 80;
    const mensaje_contacto_siguienteHeight = 30;

    doc.rect(mensaje_contacto_siguienteX, mensaje_contacto_siguienteY, mensaje_contacto_siguienteWidth, mensaje_contacto_siguienteHeight).stroke();

    const mensaje_contacto_siguientedato = `${conteoSemanaAnterior}`;
    const mensaje_contacto_siguientePosX = mensaje_contacto_siguienteX + 10;
    const mensaje_contacto_siguientePosY = mensaje_contacto_siguienteY + 10;

    doc.text(mensaje_contacto_siguientedato, mensaje_contacto_siguientePosX, mensaje_contacto_siguientePosY, { width: mensaje_contacto_siguienteWidth - 20, align: 'center' });

   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    const mensaje_contacto_anteriorX = 380;
    const mensaje_contacto_anteriorY = 640;
    const mensaje_contacto_anteriorWidth = 80;
    const mensaje_contacto_anteriorHeight = 30;

    doc.rect(mensaje_contacto_anteriorX, mensaje_contacto_anteriorY, mensaje_contacto_anteriorWidth, mensaje_contacto_anteriorHeight).stroke();

    const mensaje_contacto_anteriordato = `${conteoSemanaActual}`;
    const mensaje_contacto_anteriorPosX = mensaje_contacto_anteriorX + 10;
    const mensaje_contacto_anteriorPosY = mensaje_contacto_anteriorY + 10;

    doc.text(mensaje_contacto_anteriordato, mensaje_contacto_anteriorPosX, mensaje_contacto_anteriorPosY, { width: mensaje_contacto_anteriorWidth - 20, align: 'center' });


     ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

     const mensaje_contactos_subieronX = 180;
     const mensaje_contactos_subieronY = 680;
     const mensaje_contactos_subieronWidth = 280;
     const mensaje_contactos_subieronHeight = 35;
 
     doc.rect(mensaje_contactos_subieronX, mensaje_contactos_subieronY, mensaje_contactos_subieronWidth, mensaje_contactos_subieronHeight).stroke();
 
     const mensaje_contactos_subierondato = `${porcentajeContactos}`;
     const mensaje_contactos_subieronPosX = mensaje_contactos_subieronX + 10;
     const mensaje_contactos_subieronPosY = mensaje_contactos_subieronY + 10;
 
     doc.text(mensaje_contactos_subierondato, mensaje_contactos_subieronPosX, mensaje_contactos_subieronPosY, { width: mensaje_contactos_subieronWidth - 20, align: 'center' });
 

    doc.addPage();

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    doc.fontSize(14).font('Helvetica-Bold').text(`Contactos por semana distribución de encargadas`, 50, 40, { align: 'left' });
    doc.moveDown();

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Tabla Contactos por Vendedora


    doc.font('Helvetica-Bold').fontSize(10);
    const tableHeaders3 = ['Vendedora', 'Semana Anterior', 'Semana Siguiente', 'Diferencia'];

    const pageWidth = doc.page.width;
    const tableWidth = tableHeaders3.length * 100;
    const startX = (pageWidth - tableWidth) / 2;

    let yPosition3 = 60;

    doc.font('Helvetica-Bold');
    tableHeaders3.forEach((header, index) => {
        doc.rect(startX + index * 100, yPosition3, 100, 30).stroke();
        doc.text(header, startX + index * 100 + 3, yPosition3 + 10, { width: 100, align: 'center' });
    });

    doc.font('Helvetica').fontSize(8);
    contactos_vendedora.forEach((venta, rowIndex) => {
        yPosition3 += 30;
        const { vendedora, conteo_semana_anterior, conteo_semana_actual, diferencia } = venta;
        const rowData = [vendedora, conteo_semana_anterior, conteo_semana_actual, diferencia];

        if (rowIndex === contactos_vendedora.length - 1) {
            // Pintar la última fila con un color de fondo diferente
            doc.fillColor('#FFFF00'); // Cambia el color a tu preferencia
            doc.rect(startX, yPosition3, tableWidth, 30).fillAndStroke();
            doc.fillColor('#000000'); // Cambia el color del texto a tu preferencia
        }

        rowData.forEach((data, columnIndex) => {
            doc.rect(startX + columnIndex * 100, yPosition3, 100, 30).stroke();
            doc.text(data || '', startX + columnIndex * 100 + 3, yPosition3 + 10, { width: 100, align: 'center' });
        });
    });

        doc.end();
    };

module.exports = generateReporteVentasSemanal;