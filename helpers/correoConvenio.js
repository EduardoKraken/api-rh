const config     = require("../config/index.js");
const nodemailer = require('nodemailer');

// variables constantes 
//const constructor
const helperCorreos = {

	creaPlantillaConvenio ( ) {
		return correo = `
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
		<head>
			<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
			<meta http-equiv="X-UA-Compatible" content="IE=edge" />
			<meta name="format-detection" content="date=no" />
			<meta name="format-detection" content="address=no" />
			<meta name="format-detection" content="telephone=no" />
			<meta name="x-apple-disable-message-reformatting" />
			<link href="https://fonts.googleapis.com/css?family=Kreon:400,700|Playfair+Display:400,400i,700,700i|Raleway:400,400i,700,700i|Roboto:400,400i,700,700i" rel="stylesheet" />
			<title>CONVENIO INBI SCHOOL</title>


			<style type="text/css" media="screen">
				/* Linked Styles */
				body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#B5EEFC; -webkit-text-size-adjust:none }
				a { color:#000001; text-decoration:none }
				p { padding:0 !important; margin:0 !important } 
				img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
				.mcnPreviewText { display: none !important; }
				.text-footer2 a { color: #ffffff; } 

				/* Mobile styles */
				@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
					.mobile-shell { width: 100% !important; min-width: 100% !important; }

					.m-center { text-align: center !important; }
					.m-left { text-align: left !important; margin-right: auto !important; }

					.center { margin: 0 auto !important; }
					.content2 { padding: 8px 15px 12px !important; }
					.t-left { float: left !important; margin-right: 30px !important; }
					.t-left-2  { float: left !important; }

					.td { width: 100% !important; min-width: 100% !important; }

					.content { padding: 30px 15px !important; }
					.section { padding: 30px 15px 0px !important; }

					.m-br-15 { height: 15px !important; }
					.mpb5 { padding-bottom: 5px !important; }
					.mpb15 { padding-bottom: 15px !important; }
					.mpb20 { padding-bottom: 20px !important; }
					.mpb30 { padding-bottom: 30px !important; }
					.mp30 { padding-bottom: 30px !important; }
					.m-padder { padding: 0px 15px !important; }
					.m-padder2 { padding-left: 15px !important; padding-right: 15px !important; }
					.p70 { padding: 30px 0px !important; }
					.pt70 { padding-top: 30px !important; }
					.p0-15 { padding: 0px 15px !important; }
					.p30-15 { padding: 30px 15px !important; }			
					.p30-15-0 { padding: 30px 15px 0px 15px !important; }			
					.p0-15-30 { padding: 0px 15px 30px 15px !important; }			


					.text-footer { text-align: center !important; }

					.m-td,
					.m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

					.m-block { display: block !important; }

					.fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }

					.column,
					.column-dir,
					.column-top,
					.column-empty,
					.column-top-30,
					.column-top-60,
					.column-empty2,
					.column-bottom { float: left !important; width: 100% !important; display: block !important; }

					.column-empty { padding-bottom: 15px !important; }
					.column-empty2 { padding-bottom: 30px !important; }

					.content-spacing { width: 15px !important; }
				}
			</style>
		</head>
		<body class="body"style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#B5EEFC; -webkit-text-size-adjust:none;">
			<!--*|IF:MC_PREVIEW_TEXT|*-->
			<!--[if !gte mso 9]><!-->
				<span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">CONVENIO INBI SCHOOL</span>
				<!--<![endif]-->
					<!--*|END:IF|*-->
					<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#B5EEFC">
						<tr>
							<td align="center" valign="top">
								<!-- Main -->
								<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
									<tr>
										<td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
											<!-- Header -->
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td class="p30-15" style="padding: 40px 0px 20px 0px;">
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<th class="column-top" width="200"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																</th>
																<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																</th>
															</tr>
														</table>
													</td>
												</tr>
												<!-- END Top bar -->
												<!-- Logo -->
												<tr>
													<td 
													bgcolor="#ffffff" 
													class="p30-15 img-center" 
													style="padding: 30px; border-radius: 20px 20px 0px 0px; font-size:0pt; line-height:0pt; text-align:center;"
													>
												</td>
											</tr>
											<!-- END Logo -->
											<!-- Nav -->
											<tr>
												<td class="text-nav-white" bgcolor="#07C9F9"style="color:#ffffff; font-family:'Roboto', Arial, sans-serif; font-size:12px; line-height:22px; text-align:center; text-transform:uppercase; padding:12px 0px;">
													<div mc:edit="text_2">
														&nbsp; &nbsp; &nbsp;<span class="m-hide"> &nbsp; &nbsp; </span>
														<a href="https://inbi.mx/promociones" target="_blank" class="link-white"style="color:#ffffff; text-decoration:none;"><span class="link-white"style="color:#ffffff; text-decoration:none;">Promociones</span></a>
														&nbsp; &nbsp; &nbsp;<span class="m-hide"> &nbsp; &nbsp; </span>
														<a href="https://inbi.mx/contacto-vinculacion" target="_blank" class="link-white"style="color:#ffffff; text-decoration:none;"><span class="link-white"style="color:#ffffff; text-decoration:none;">Contacto</span></a>
														<span class="m-block"><span class="m-hide">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span></span>
														<a href="https://www.inbi.mx/examen_ubicacion_pagina" target="_blank" class="link-white"style="color:#ffffff; text-decoration:none;"><span class="link-white"style="color:#ffffff; text-decoration:none;">Examen ubicación</span></a>
														&nbsp; &nbsp; &nbsp;<span class="m-hide"> &nbsp; &nbsp; </span>
														<a href="https://inbi.mx/cursos" target="_blank" class="link-white"style="color:#ffffff; text-decoration:none;"><span class="link-white"style="color:#ffffff; text-decoration:none;">Cursos</span></a>
														<span class="m-block"><span class="m-hide">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span></span>
														&nbsp; &nbsp; &nbsp;<span class="m-hide"> &nbsp; &nbsp; </span>
													</div>
												</td>
											</tr>
											<!-- END Nav -->
										</table>
										<!-- END Header -->

										<!-- Section 1 -->
										<div mc:repeatable="Select" mc:variant="Section 1">
											<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
												<tr>
													<td class="fluid-img"style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://inbi.mx/public/images/correo/convenio/PARA-WEB.jpg" width="650" height="358" mc:edit="image_7" style="max-width:650px;" border="0" alt="" /></td>
												</tr>
												<tr>
													<td class="p30-15-0" style="padding: 50px 30px 0px;" bgcolor="#ffffff">
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td class="h5-center"style="color:#a1a1a1; font-family:'Raleway', Arial,sans-serif; font-size:16px; line-height:22px; text-align:center; padding-bottom:5px;"><div mc:edit="text_3">INBI SCHOOL OF ENGLISH</div></td>
															</tr>
															<tr>
																<td class="h2-center"style="color:#000000; font-size:24px; line-height:40px; text-align:center; padding-bottom:20px;">
																	<div mc:edit="text_4">¡Aprender inglés no tiene que ser una meta imposible de alcanzar! Nuestros métodos especializados apoyan a cada alumno a cumplirla. ¿Trabajan tiempo completo? ¡No hay problema!</div>
																</td>
															</tr>
															<tr>
																<td align="center">
																	<table border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td class="text-button-orange"style="background:#07C9F9; color:#ffffff; font-family:'Kreon', 'Times New Roman', Georgia, serif; font-size:14px; line-height:18px; text-align:center; padding:10px 30px; border-radius:20px;"><div mc:edit="text_6"><a href="https://inbi.mx/" target="_blank" class="link-white"style="color:#ffffff; text-decoration:none;"><span class="link-white"style="color:#ffffff; text-decoration:none;">Ver más</span></a></div></td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</div>
										<!-- END Section 1 -->

										<!-- Section 2 -->
										<div mc:repeatable="Select" mc:variant="Section 2">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td>
														<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#07C9F9">
															<tr>
																<td class="fluid-img"style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://inbi.mx/public/images/correo/convenio/free_white_blue.jpg" width="650" height="162" mc:edit="image_8" style="max-width:650px;" border="0" alt="" /></td>
															</tr>
															<tr>
																<td class="p0-15" style="padding: 0px 30px;">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td class="h2-center"style="color:#FFF; font-family:'Playfair Display', Times, 'Times New Roman', serif; font-size:32px; line-height:36px; text-align:center; padding-bottom:20px;">
																				<div mc:edit="text_7">
																					Obtén un convenio a la medida de tu personal
																				</div>
																			</td>
																		</tr>

																		<tr>
																			<td >
																				<table width="100%" border="0" cellspacing="0" cellpadding="0">
																					<tr>
																						<td class="event-separator"style="padding-bottom:20px;">
																							<table width="100%" border="0" cellspacing="0" cellpadding="0">

																								<tr class="mpb20"style="padding-bottom:30px;">
																									<th class="column-top mpb20" width="40"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																										<table width="100%" border="0" cellspacing="0" cellpadding="0">
																											<tr>
																												<td class="day"style="color:#FFF; font-family:'Raleway', Arial,sans-serif; font-size:40px; line-height:20px; text-align:left; font-weight:bold;"><div mc:edit="text_6">1</div></td>
																											</tr>
																										</table>
																									</th>

																									<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>

																									<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																										<table width="100%" border="0" cellspacing="0" cellpadding="0">
																											<tr>
																												<td class="h5-black black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#FFF;">
																													<div mc:edit="text_10">
																														Sin costo para ti como empleador.
																													</div>
																												</td>
																											</tr>
																										</table>
																									</th>
																								</tr>

																							</table>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>

																		<tr>
																			<td class="pb40">
																				<table width="100%" border="0" cellspacing="0" cellpadding="0">
																					<tr>
																						<td class="event-separator"style="padding-bottom:20px;">
																							<table width="100%" border="0" cellspacing="0" cellpadding="0">

																								<tr class="mpb20"style="padding-bottom:30px;">
																									<th class="column-top mpb20" width="40"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																										<table width="100%" border="0" cellspacing="0" cellpadding="0">
																											<tr>
																												<td class="day"style="color:#FFF; font-family:'Raleway', Arial,sans-serif; font-size:40px; line-height:20px; text-align:left; font-weight:bold;"><div mc:edit="text_6">2</div></td>
																											</tr>
																										</table>
																									</th>

																									<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>

																									<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																										<table width="100%" border="0" cellspacing="0" cellpadding="0">
																											<tr>
																												<td class="h5-black black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#FFF;">
																													<div mc:edit="text_10">
																														TUS COLABORADORES LLEGARÁN A UN NIVEL INTERMEDIO-AVANZADO EN POCOS MESES.
																													</div>
																												</td>
																											</tr>
																										</table>
																									</th>
																								</tr>

																							</table>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>


																		<tr>
																			<td class="pb40">
																				<table width="100%" border="0" cellspacing="0" cellpadding="0">
																					<tr>
																						<td class="event-separator"style="padding-bottom:20px;">
																							<table width="100%" border="0" cellspacing="0" cellpadding="0">

																								<tr class="mpb20"style="padding-bottom:30px;">
																									<th class="column-top mpb20" width="40"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																										<table width="100%" border="0" cellspacing="0" cellpadding="0">
																											<tr>
																												<td class="day"style="color:#FFF; font-family:'Raleway', Arial,sans-serif; font-size:40px; line-height:20px; text-align:left; font-weight:bold;"><div mc:edit="text_6">3</div></td>
																											</tr>
																										</table>
																									</th>

																									<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>

																									<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																										<table width="100%" border="0" cellspacing="0" cellpadding="0">
																											<tr>
																												<td class="h5-black black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#FFF;">
																													<div mc:edit="text_10">
																														CERTIFICACIÓN DEL IDIOMA ANTE CAMBRIDGE.
																													</div>
																												</td>
																											</tr>
																										</table>
																									</th>
																								</tr>

																							</table>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>


																		<tr>
																			<td class="pb40">
																				<table width="100%" border="0" cellspacing="0" cellpadding="0">
																					<tr>
																						<td class="event-separator"style="padding-bottom:20px;">
																							<table width="100%" border="0" cellspacing="0" cellpadding="0">

																								<tr class="mpb20"style="padding-bottom:30px;">
																									<th class="column-top mpb20" width="40"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																										<table width="100%" border="0" cellspacing="0" cellpadding="0">
																											<tr>
																												<td class="day"style="color:#FFF; font-family:'Raleway', Arial,sans-serif; font-size:40px; line-height:20px; text-align:left; font-weight:bold;"><div mc:edit="text_6">4</div></td>
																											</tr>
																										</table>
																									</th>

																									<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>

																									<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																										<table width="100%" border="0" cellspacing="0" cellpadding="0">
																											<tr>
																												<td class="h5-black black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#FFF;">
																													<div mc:edit="text_10">
																														SIN MÍNIMO DE PERSONAL INSCRITO.
																													</div>
																												</td>
																											</tr>
																										</table>
																									</th>
																								</tr>

																							</table>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>

																		<tr>
																			<td class="pb40">
																				<table width="100%" border="0" cellspacing="0" cellpadding="0">
																					<tr>
																						<td class="event-separator"style="padding-bottom:20px;">
																							<table width="100%" border="0" cellspacing="0" cellpadding="0">

																								<tr class="mpb20"style="padding-bottom:30px;">
																									<th class="column-top mpb20" width="40"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																										<table width="100%" border="0" cellspacing="0" cellpadding="0">
																											<tr>
																												<td class="day"style="color:#FFF; font-family:'Raleway', Arial,sans-serif; font-size:40px; line-height:20px; text-align:left; font-weight:bold;"><div mc:edit="text_6">5</div></td>
																											</tr>
																										</table>
																									</th>

																									<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>

																									<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																										<table width="100%" border="0" cellspacing="0" cellpadding="0">
																											<tr>
																												<td class="h5-black black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#FFF;">
																													<div mc:edit="text_10">
																														APLICA PARA EL COLABORADOR, SU CONYUGUE E HIJOS.
																													</div>
																												</td>
																											</tr>
																										</table>
																									</th>
																								</tr>

																							</table>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>

																		<tr>
																			<td class="pb40">
																				<table width="100%" border="0" cellspacing="0" cellpadding="0">
																					<tr>
																						<td class="event-separator"style="padding-bottom:20px;">
																							<table width="100%" border="0" cellspacing="0" cellpadding="0">

																								<tr class="mpb20"style="padding-bottom:30px;">
																									<th class="column-top mpb20" width="40"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																										<table width="100%" border="0" cellspacing="0" cellpadding="0">
																											<tr>
																												<td class="day"style="color:#FFF; font-family:'Raleway', Arial,sans-serif; font-size:40px; line-height:20px; text-align:left; font-weight:bold;"><div mc:edit="text_6">6</div></td>
																											</tr>
																										</table>
																									</th>

																									<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>

																									<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																										<table width="100%" border="0" cellspacing="0" cellpadding="0">
																											<tr>
																												<td class="h5-black black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#FFF;">
																													<div mc:edit="text_10">
																														CONTAMOS CON 7 SUCURSALES Y MODALIDAD ONLINE PARA TI Y TUS EMPLEADOS.
																													</div>
																												</td>
																											</tr>
																										</table>
																									</th>
																								</tr>

																							</table>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>

																		<tr>
																			<td align="center">
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td class="fluid-img"style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://inbi.mx/public/images/correo/convenio/free_blue_white.jpg" width="650" height="160" mc:edit="image_9" style="max-width:650px;" border="0" alt="" /></td>
												</tr>
											</table>
										</div>
										<!-- END Section 2 -->

										<!-- SECCION  -->
										<div mc:repeatable="Select" mc:variant="Section 3">
											<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
												<tr>
													<td class="p0-15-30" style="padding: 0px 30px 70px 30px;" bgcolor="#ffffff">
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td class="h2-center pb40"style="color:#000000; font-family:'Playfair Display', Times, 'Times New Roman', serif; font-size:28px; line-height:36px; text-align:center; padding-bottom:40px;">
																	<div mc:edit="text_19">
																		¿CÓMO FUNCIONA?
																	</div>
																</td>
															</tr>
															<tr mc:repeatable>
																<td class="pb30"style="padding-bottom:30px;">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<th class="column" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
																				<table width="100%" border="0" cellspacing="0" cellpadding="0">
																					<tr>
																						<td class="h5-black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#000000;">
																							<div mc:edit="text_26">
																								<span style="font-family:'Raleway', Arial,sans-serif; font-size:22px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#000000;">1-.</span> INBI SCHOOL OF ENGLISH Y TU INSTITUCIÓN FIRMAN EL CONVENIO CORRESPONDIENTE.
																								<ol>
																									EL CONVENIO NO TIENE COSTO ALGUNO PARA LA INSTITUCIÓN.
																								</ol>
																							</div>
																						</td>
																					</tr>
																				</table>
																			</th>
																		</tr>
																	</table>
																</td>
															</tr>

															<tr mc:repeatable>
																<td class="pb30"style="padding-bottom:30px;">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td>
																				<table width="100%" border="0" cellspacing="0" cellpadding="0">
																					<tr>
																						<td class="h5-black black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#000000;">
																							<div mc:edit="text_26">
																								<span 
																									style="font-family:'Raleway', Arial,sans-serif; font-size:22px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#000000;"
																								>
																									2-.
																								</span>
																								EL COLABORADOR SE PRESENTA CON UNA IDENTIFICACIÓN O NÚMERO DE IDENTIFICACIÓN BRINDADO POR LA EMPRESA.
																							</div>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>

															<tr mc:repeatable>
																<td class="pb30"style="padding-bottom:30px;">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td>
																				<table width="100%" border="0" cellspacing="0" cellpadding="0">
																					<tr>
																						<td class="h5-black black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#000000;">
																							<div mc:edit="text_26">
																								<span 
																									style="font-family:'Raleway', Arial,sans-serif; font-size:22px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#000000;"
																								>
																									3-.
																								</span>
																								INBI SCHOOL OF ENGLISH BRINDA UNA BECA AL COLABORADOR CON UN DESCUENTO PREFERENCIAL.
																							</div>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>

															<tr mc:repeatable>
																<td class="pb30"style="padding-bottom:30px;">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td>
																				<table width="100%" border="0" cellspacing="0" cellpadding="0">
																					<tr>
																						<td class="h5-black black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#000000;">
																							<div mc:edit="text_26">
																								<span 
																									style="font-family:'Raleway', Arial,sans-serif; font-size:22px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#000000;"
																								>
																									4-.
																								</span>
																								EL CONVENIO ENTRE INBI SCHOOL OF ENGLISH Y LA INSTITUCIÓN SERÁ RENOVADO CADA 12 MESES.
																								<ol>
																									EN ESTA ETAPA PODEMOS REALIZAR CAMBIOS DE ACUERDO A LAS NECESIDADES DE TUS EMPLEADOS.
																								</ol>
																							</div>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</div>
										<!-- END Section 3 -->

										<!-- SECCIÓN CURSOS -->
										<div mc:repeatable="Select" mc:variant="Section 3">
											<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
												<tr>
													<td class="p0-15-30" style="padding: 0px 30px 70px 30px;" bgcolor="#ffffff">
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td class="h2-center pb40"style="color:#000000; font-family:'Playfair Display', Times, 'Times New Roman', serif; font-size:28px; line-height:36px; text-align:center; padding-bottom:40px;">
																	<div mc:edit="text_19">
																		CURSOS IMPARTIDOS
																	</div>
																</td>
															</tr>

															<tr>
																<td style="padding-bottom: 15px;">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td class="fluid-img"style="font-size:0pt; line-height:0pt; text-align:left;">
																				<a href="https://www.inbi.mx/curso-kids" target="_blank">
																					<img src="https://inbi.mx/public/img/cursos/1-KIDS.webp" width="170" height="120" mc:edit="image_15" style="max-width:290px;" border="0" alt="" />
																				</a>
																			</td>
																			<td class="img" width="10"style="font-size:0pt; line-height:0pt; text-align:left;"></td>
																			<td class="fluid-img"style="font-size:0pt; line-height:0pt; text-align:left;">
																				<a href="https://www.inbi.mx/curso-intensivo-teens" target="_blank">
																					<img src="https://inbi.mx/public/img/cursos/3-TEENS.webp" width="170" height="120" mc:edit="image_15" style="max-width:290px;" border="0" alt="" />
																				</a>
																			</td>
																			<td class="img" width="10"style="font-size:0pt; line-height:0pt; text-align:left;"></td>
																			<td class="fluid-img"style="font-size:0pt; line-height:0pt; text-align:left;">
																				<a href="https://www.inbi.mx/curso-intensivo-adultos" target="_blank">
																					<img src="https://inbi.mx/public/img/cursos/5-intensivo.webp" width="170" height="120" mc:edit="image_15" style="max-width:290px;" border="0" alt="" />
																				</a>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>

															<tr>
																<td>
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td class="h5-black black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:center; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#000;">
																				<div mc:edit="text_10">
																					Kids (5 a 8 años)
																				</div>
																			</td>
																			<td class="h5-black black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:center; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#000;">
																				<div mc:edit="text_10">
																					Teens ( 9 a 15 años)
																				</div>
																			</td>
																			<td class="h5-black black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:center; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#000;">
																				<div mc:edit="text_10">
																					Jóvenes y Adultos
																				</div>
																			</td>
																		</tr>

																	</table>
																</td>
															</tr>

														</table>
													</td>
												</tr>

												<tr bgcolor="#ffffff">
													<td align="center">
														<table border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td class="text-button-orange"style="background:#07C9F9; color:#ffffff; font-family:'Kreon', 'Times New Roman', Georgia, serif; font-size:32px; line-height:18px; text-align:center; padding:20px 50px; border-radius:50px;"><div mc:edit="text_6"><a href="http://bit.ly/3Ra5lUR" target="_blank" class="link-white"style="color:#ffffff; text-decoration:none;"><span class="link-white"style="color:#ffffff; text-decoration:none;">Más información</span></a></div></td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</div>
										<!-- END Section 3 -->

										<!-- Footer -->
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="p30-15-0" bgcolor="#ffffff" style="border-radius: 0px 0px 20px 20px; padding: 70px 30px 0px 30px;">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="m-padder2 pb30" align="center"style="padding-bottom:30px;">
																<table class="center" border="0" cellspacing="0" cellpadding="0"style="text-align:center;">
																	<tr>
																		<td class="img" width="40"style="font-size:0pt; line-height:0pt; text-align:left;"><a href="https://www.facebook.com/INBImx/" target="_blank"><img src="https://inbi.mx/public/images/correo/convenio/ico4_facebook.png" width="26" height="26" mc:edit="image_27" style="max-width:26px;" border="0" alt="" /></a></td>
																		<td class="img" width="40"style="font-size:0pt; line-height:0pt; text-align:left;"><a href="https://www.instagram.com/inbimx/" target="_blank"><img src="https://inbi.mx/public/images/correo/convenio/ico4_instagram.png" width="26" height="26" mc:edit="image_31" style="max-width:26px;" border="0" alt="" /></a></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
														</tr>
													</table>
												</td>
											</tr>
										</table>

										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="text-footer2 p30-15" style="padding: 30px 15px 50px 15px; color:#a9b6e0; font-family:'Raleway', Arial,sans-serif; font-size:12px; line-height:22px; text-align:center;">
												</tr>
											</table>
											<!-- END Footer -->
										</td>
									</tr>
								</table>
								<!-- END Main -->

							</td>
						</tr>
					</table>
				</body>
				</html>
		`
	},

	enviarCorreo ( asunto, correoHtml, correo, transporter ) {
		return new Promise(async (resolve, reject)=>{
	    // Preparamos las opciones del correo
	    const mailOptions = {
	      from:        `INBI SCHOOL <vinculacion@inbi.mx>`,
	      to:          correo,
	      subject:     asunto,
	      html:        correoHtml,
	      cc:          ['vinculacion@inbi.mx'],
	    }

	    // Enviamos el correo, PARAMETRO: mailOptions
	    await transporter.sendMail(mailOptions, (error, info) =>{
        if (error) {
          return reject(error)
        } else {
          return resolve(info)
        }
	    });

		})
	},
};


module.exports = helperCorreos;