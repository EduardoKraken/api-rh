const { v4: uuidv4 } = require('uuid');

const agregarArchivo = ( file, extensiones, path = './archivos' ) => {
    return new Promise(( resolve, reject ) => {
        try {

            // DIVIDE NOMBRE POR PUNTOS
            const nombreSplit = file.name.split('.');

            // EXTENSIÓN DEL ARCHIVO 
            const extensionArchivo = nombreSplit[ nombreSplit.length - 1 ];

            // VALIDAR QUE LA EXTENSIÓN DEL ARCHIVO RECIBIDO SEA VÁLIDA
            if( !extensiones.includes( extensionArchivo ) ){
                return reject({message: `La extensión ${ extensionArchivo } no es válida, solo se aceptan: ${ extensiones }`});
            }

            // MODIFICAR EL NOMBRE DEL ARCHIVO CON UN UUID UNICO
            const nombreUuid = uuidv4() + '.' + extensionArchivo;

            // RUTA DONDE SE VA A GUARDAR LA IMAGEN
            const ruta = `${path}/${nombreUuid}`;

            // MOVER ARCHIVO A LA RUTA INDICADA
            file.mv( ruta, ( err ) => {
                if( err ) return reject({ message: err });

                return resolve({ nombreUuid, extensionArchivo });
            });

        } catch (error) {
            reject({ message: error ? error.message : 'Error en el servidor' });
        }
    });
}   


module.exports = {
    agregarArchivo
}