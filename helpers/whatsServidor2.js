const { Client, LegacySessionAuth, LocalAuth } = require('whatsapp-web.js');

const whatsapp             = require("../models/operaciones/whatsapp.model");

const wa     = require("@open-wa/wa-automate");
const qrcode = require('qrcode-terminal');
const fs     = require('fs');
const mime   = require("mime-types");
const hash   = require("hash.js");
const moment = require("moment");
const path   = require("path");
const { open, writeFile } = require("fs").promises;


let BASE_IMAGE_PATH = path.resolve(__dirname, "..", "..", "whatsapp-imagenes");


/*****************************************************/
/***                  LINDA VISTA                    */
/*****************************************************/

const clientLV = new Client({
  authStrategy: new LocalAuth({
    clientId: "client-lv" //Un identificador(Sugiero que no lo modifiques)
  })
})

// Save session values to the file upon successful auth
clientLV.on('authenticated', (session) => {
  console.log( 'sesion conectada' )
});

clientLV.initialize();

clientLV.on('qr', qr => {
  console.log('FAST -> FAST LINDA VISTA -> RECEPCIÓN')
  qrcode.generate(qr, {small: true});
});

clientLV.on('ready', () => {
  console.log( 'Sesión lista: FAST -> FAST LINDA VISTA -> RECEPCIÓN' )
});

clientLV.on('message_create', async ( message_create ) => {
  try {

    let fullFileName = ''
    if( message_create._data.mimetype ){
      const fileName     = hash.sha256().update(moment.now().toString()).digest("hex");
      const extension    = mime.extension(message_create._data.mimetype);
      fullFileName       = `${fileName}.${extension}`;
      const fullPath     = path.join(
        path.relative(__dirname, BASE_IMAGE_PATH),
        fullFileName
      );
      const mediaData = await wa.decryptMedia(message_create._data);
      try {
          await writeFile(fullPath, mediaData);
      } catch (error) {
        console.log(error, 'here 3');
      }
    }
    
    if( message_create._data.id.remote != 'status@broadcast' ){
      let saldoFavor = await whatsapp.agregarMensajeWhatsApp( message_create, fullFileName ).then( response => response )
    }

  } catch (error) {
    console.log( error )
  }
});



/*****************************************************/
/***                     MITRAS                      */
/*****************************************************/

const clientMTR = new Client({
  authStrategy: new LocalAuth({
    clientId: "client-mtr" //Un identificador(Sugiero que no lo modifiques)
  })
})

// Save session values to the file upon successful auth
clientMTR.on('authenticated', (session) => {
  console.log( 'sesion conectada' )
});

clientMTR.initialize();

clientMTR.on('qr', qr => {
  console.log('FAST -> FAST MITRAS -> RECEPCIÓN')
  qrcode.generate(qr, {small: true});
});

clientMTR.on('ready', () => {
  console.log( 'Sesión lista: FAST -> FAST MITRAS -> RECEPCIÓN' )
});

clientMTR.on('message_create', async ( message_create ) => {
  try {

    let fullFileName = ''
    if( message_create._data.mimetype ){
      const fileName     = hash.sha256().update(moment.now().toString()).digest("hex");
      const extension    = mime.extension(message_create._data.mimetype);
      fullFileName       = `${fileName}.${extension}`;
      const fullPath     = path.join(
        path.relative(__dirname, BASE_IMAGE_PATH),
        fullFileName
      );
      const mediaData = await wa.decryptMedia(message_create._data);
      try {
          await writeFile(fullPath, mediaData);
      } catch (error) {
        console.log(error, 'here 3');
      }
    }
    
    if( message_create._data.id.remote != 'status@broadcast' ){
      let saldoFavor = await whatsapp.agregarMensajeWhatsApp( message_create, fullFileName ).then( response => response )
    }

  } catch (error) {
    console.log( error )
  }
});


/*****************************************************/
/***                     NORIA                       */
/*****************************************************/

const clientNoria = new Client({
  authStrategy: new LocalAuth({
    clientId: "client-noria" //Un identificador(Sugiero que no lo modifiques)
  })
})

// Save session values to the file upon successful auth
clientNoria.on('authenticated', (session) => {
  console.log( 'sesion conectada' )
});

clientNoria.initialize();

clientNoria.on('qr', qr => {
  console.log('FAST -> FAST NORIA -> RECEPCIÓN')
  qrcode.generate(qr, {small: true});
});

clientNoria.on('ready', () => {
  console.log( 'Sesión lista: FAST -> FAST NORIA -> RECEPCIÓN' )
});

clientNoria.on('message_create', async ( message_create ) => {
  try {

    let fullFileName = ''
    if( message_create._data.mimetype ){
      const fileName     = hash.sha256().update(moment.now().toString()).digest("hex");
      const extension    = mime.extension(message_create._data.mimetype);
      fullFileName       = `${fileName}.${extension}`;
      const fullPath     = path.join(
        path.relative(__dirname, BASE_IMAGE_PATH),
        fullFileName
      );
      const mediaData = await wa.decryptMedia(message_create._data);
      try {
          await writeFile(fullPath, mediaData);
      } catch (error) {
        console.log(error, 'here 3');
      }
    }
    
    if( message_create._data.id.remote != 'status@broadcast' ){
      let saldoFavor = await whatsapp.agregarMensajeWhatsApp( message_create, fullFileName ).then( response => response )
    }

  } catch (error) {
    console.log( error )
  }
});


/*****************************************************/
/***                   APODACA                       */
/*****************************************************/

const clientApodaca = new Client({
  authStrategy: new LocalAuth({
    clientId: "client-apodaca" //Un identificador(Sugiero que no lo modifiques)
  })
})

// Save session values to the file upon successful auth
clientApodaca.on('authenticated', (session) => {
  console.log( 'sesion conectada' )
});

clientApodaca.initialize();

clientApodaca.on('qr', qr => {
  console.log('FAST -> FAST APODACA -> RECEPCIÓN')
  qrcode.generate(qr, {small: true});
});

clientApodaca.on('ready', () => {
  console.log( 'Sesión lista: FAST -> FAST APODACA -> RECEPCIÓN' )
});

clientApodaca.on('message_create', async ( message_create ) => {
  try {

    let fullFileName = ''
    if( message_create._data.mimetype ){
      const fileName     = hash.sha256().update(moment.now().toString()).digest("hex");
      const extension    = mime.extension(message_create._data.mimetype);
      fullFileName       = `${fileName}.${extension}`;
      const fullPath     = path.join(
        path.relative(__dirname, BASE_IMAGE_PATH),
        fullFileName
      );
      const mediaData = await wa.decryptMedia(message_create._data);
      try {
          await writeFile(fullPath, mediaData);
      } catch (error) {
        console.log(error, 'here 3');
      }
    }
    
    if( message_create._data.id.remote != 'status@broadcast' ){
      let saldoFavor = await whatsapp.agregarMensajeWhatsApp( message_create, fullFileName ).then( response => response )
    }

  } catch (error) {
    console.log( error )
  }
});


/*****************************************************/
/***                   ONLINE                        */
/*****************************************************/

const clientOnline = new Client({
  authStrategy: new LocalAuth({
    clientId: "client-fa-online" //Un identificador(Sugiero que no lo modifiques)
  })
})

// Save session values to the file upon successful auth
clientOnline.on('authenticated', (session) => {
  console.log( 'sesion conectada' )
});

clientOnline.initialize();

clientOnline.on('qr', qr => {
  console.log('FAST -> FAST ONLINE -> RECEPCIÓN')
  qrcode.generate(qr, {small: true});
});

clientOnline.on('ready', () => {
  console.log( 'Sesión lista: FAST -> FAST ONLINE -> RECEPCIÓN' )
});

clientOnline.on('message_create', async ( message_create ) => {
  try {

    let fullFileName = ''
    if( message_create._data.mimetype ){
      const fileName     = hash.sha256().update(moment.now().toString()).digest("hex");
      const extension    = mime.extension(message_create._data.mimetype);
      fullFileName       = `${fileName}.${extension}`;
      const fullPath     = path.join(
        path.relative(__dirname, BASE_IMAGE_PATH),
        fullFileName
      );
      const mediaData = await wa.decryptMedia(message_create._data);
      try {
          await writeFile(fullPath, mediaData);
      } catch (error) {
        console.log(error, 'here 3');
      }
    }
    
    if( message_create._data.id.remote != 'status@broadcast' ){
      let saldoFavor = await whatsapp.agregarMensajeWhatsApp( message_create, fullFileName ).then( response => response )
    }

  } catch (error) {
    console.log( error )
  }
});

/*****************************************************/
/***                   ELOY                          */
/*****************************************************/

const clientEloy = new Client({
  authStrategy: new LocalAuth({
    clientId: "client-eloy" //Un identificador(Sugiero que no lo modifiques)
  })
})

// Save session values to the file upon successful auth
clientEloy.on('authenticated', (session) => {
  console.log( 'sesion conectada' )
});

clientEloy.initialize();

clientEloy.on('qr', qr => {
  console.log('FAST -> FAST ELOY -> RECEPCIÓN')
  qrcode.generate(qr, {small: true});
});

clientEloy.on('ready', () => {
  console.log( 'Sesión lista: FAST -> FAST ELOY -> RECEPCIÓN' )
});

clientEloy.on('message_create', async ( message_create ) => {
  try {

    let fullFileName = ''
    if( message_create._data.mimetype ){
      const fileName     = hash.sha256().update(moment.now().toString()).digest("hex");
      const extension    = mime.extension(message_create._data.mimetype);
      fullFileName       = `${fileName}.${extension}`;
      const fullPath     = path.join(
        path.relative(__dirname, BASE_IMAGE_PATH),
        fullFileName
      );
      const mediaData = await wa.decryptMedia(message_create._data);
      try {
          await writeFile(fullPath, mediaData);
      } catch (error) {
        console.log(error, 'here 3');
      }
    }
    
    if( message_create._data.id.remote != 'status@broadcast' ){
      let saldoFavor = await whatsapp.agregarMensajeWhatsApp( message_create, fullFileName ).then( response => response )
    }

  } catch (error) {
    console.log( error )
  }
});




/*****************************************************/
/***                UNIVERSIDAD                      */
/*****************************************************/

const clientUni = new Client({
  authStrategy: new LocalAuth({
    clientId: "client-uni" //Un identificador(Sugiero que no lo modifiques)
  })
})

// Save session values to the file upon successful auth
clientUni.on('authenticated', (session) => {
  console.log( 'sesion conectada' )
});

clientUni.initialize();

clientUni.on('qr', qr => {
  console.log('FAST -> FAST UNIVERSIDAD -> RECEPCIÓN')
  qrcode.generate(qr, {small: true});
});

clientUni.on('ready', () => {
  console.log( 'Sesión lista: FAST -> FAST UNIVERSIDAD -> RECEPCIÓN' )
});

clientUni.on('message_create', async ( message_create ) => {
  try {

    let fullFileName = ''
    if( message_create._data.mimetype ){
      const fileName     = hash.sha256().update(moment.now().toString()).digest("hex");
      const extension    = mime.extension(message_create._data.mimetype);
      fullFileName       = `${fileName}.${extension}`;
      const fullPath     = path.join(
        path.relative(__dirname, BASE_IMAGE_PATH),
        fullFileName
      );
      const mediaData = await wa.decryptMedia(message_create._data);
      try {
          await writeFile(fullPath, mediaData);
      } catch (error) {
        console.log(error, 'here 3');
      }
    }
    
    if( message_create._data.id.remote != 'status@broadcast' ){
      let saldoFavor = await whatsapp.agregarMensajeWhatsApp( message_create, fullFileName ).then( response => response )
    }

  } catch (error) {
    console.log( error )
  }
});


/*****************************************************/
/***                ROMULO GARZA                     */
/*****************************************************/

const clientRomulo = new Client({
  authStrategy: new LocalAuth({
    clientId: "client-romulo" //Un identificador(Sugiero que no lo modifiques)
  })
})

// Save session values to the file upon successful auth
clientRomulo.on('authenticated', (session) => {
  console.log( 'sesion conectada' )
});

clientRomulo.initialize();

clientRomulo.on('qr', qr => {
  console.log('FAST -> FAST ROMULO -> RECEPCIÓN')
  qrcode.generate(qr, {small: true});
});

clientRomulo.on('ready', () => {
  console.log( 'Sesión lista: FAST -> FAST ROMULO -> RECEPCIÓN' )
});

clientRomulo.on('message_create', async ( message_create ) => {
  try {

    let fullFileName = ''
    if( message_create._data.mimetype ){
      const fileName     = hash.sha256().update(moment.now().toString()).digest("hex");
      const extension    = mime.extension(message_create._data.mimetype);
      fullFileName       = `${fileName}.${extension}`;
      const fullPath     = path.join(
        path.relative(__dirname, BASE_IMAGE_PATH),
        fullFileName
      );
      const mediaData = await wa.decryptMedia(message_create._data);
      try {
          await writeFile(fullPath, mediaData);
      } catch (error) {
        console.log(error, 'here 3');
      }
    }
    
    if( message_create._data.id.remote != 'status@broadcast' ){
      let saldoFavor = await whatsapp.agregarMensajeWhatsApp( message_create, fullFileName ).then( response => response )
    }

  } catch (error) {
    console.log( error )
  }
});
