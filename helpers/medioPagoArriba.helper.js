const config     = require("../config/index.js");
const nodemailer = require('nodemailer');

//const constructor
const medioPagoArriba = {

	crearCorreoMedioPagoArriba ( payload ) {
		const correo = `
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
					<head>
						<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
						<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
					  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
						<meta name="format-detection" content="date=no" />
						<meta name="format-detection" content="address=no" />
						<meta name="format-detection" content="telephone=no" />
						<meta name="x-apple-disable-message-reformatting" />
						<link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400i,700,700i|Raleway:400,400i,700,700i" rel="stylesheet" />
						<title>CLASES EN VIVO</title>
						

						<style type="text/css" media="screen">
							/* Linked Styles */
							body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#333545; -webkit-text-size-adjust:none }
							a { color:#4e54cb; text-decoration:none }
							p { padding:0 !important; margin:0 !important } 
							img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
							.mcnPreviewText { display: none !important; }
							.text-footer a { color: #7e7e7e !important; }
							.text-footer2 a { color: #c3c3c3 !important; }
							
							/* Mobile styles */
							@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
								.mobile-shell { width: 100% !important; min-width: 100% !important; }
								
								.m-center { text-align: center !important; }
								.m-left { margin-right: auto !important; }
								
								.center { margin: 0 auto !important; }
								
								.td { width: 100% !important; min-width: 100% !important; }

								.m-br-15 { height: 15px !important; }
								.m-separator { border-bottom: 1px solid #000000; }
								.small-separator { border-top: 1px solid #333333 !important; padding-bottom: 20px !important; }

								.m-td,
								.m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

								.m-block { display: block !important; }

								.fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }
								
								.content-middle { width: 140px !important; padding: 0px !important; }

								.text-white { font-size: 12px !important; }

								.h2-white { font-size: 46px !important; line-height: 50px !important; }
								.h3-white { font-size: 24px !important; line-height: 30px !important; }

								.mpb15 { padding-bottom: 15px; }
								.content { padding: 20px 15px !important; }

								.section-inner { padding: 0px !important; }

								.content-2 { padding: 30px 15px 30px 15px !important; }
								.pt30 { padding-top: 20px !important; }
								.p30-15 { padding: 30px 15px !important; }
								.footer { padding: 30px 15px !important; }
								.main { padding: 0px !important; }
								.section { padding: 30px 15px 30px 15px !important; }
								.section2 { padding: 0px 15px 30px 15px !important; }
								.section4 { padding: 30px 15px !important; }
								.section-inner2 { padding: 30px 15px !important; }

								.separator-outer { padding-bottom: 30px !important; }
								.section3 { padding: 30px 15px !important; }
								.mpb10 { padding-bottom: 10px !important; padding-top: 5px !important; }
								.preheader { padding-bottom: 20px !important; } 

								.column,
								.column-dir,
								.column-top,
								.column-empty,
								.column-empty2,
								.column-bottom,
								.column-dir-top,
								.column-dir-bottom { float: left !important; width: 100% !important; display: block !important; }
								.column-empty { padding-bottom: 30px !important; }
								.column-empty2 { padding-bottom: 10px !important; }
								.content-spacing { width: 15px !important; }
							}
						</style>
					</head>
					<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#333545; -webkit-text-size-adjust:none;">
							<span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">CLASES EN VIVO</span>
						<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#333545">
							<tr>
								<td align="center" valign="top">
									<!-- Main -->
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="center">
												<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
													<tr>
														<td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
															<!-- Header -->
															<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
																<tr>
																	<td style="padding: 30px 0px 30px 30px;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0" dir="rtl" style="direction: rtl;">
																			<tr>
																				<th class="column-dir" dir="ltr" width="200" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;">
																					<table width="100%" border="0" cellspacing="0" cellpadding="0">
																						<tr>
																							<td class="img m-center mpb10" style="font-size:0pt; line-height:0pt; text-align:left;">
																								<img src="https://fastenglish.com.mx/public/images/logo.png" width="100" height="90" mc:edit="image_2" style="max-width:197px;" border="0" alt="" />
																							</td>
																						</tr>
																					</table>
																				</th>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<!-- END Header -->

															<!-- Intro -->
															<div mc:repeatable="Select" mc:variant="Intro">
																<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#cc0607">
																	<tr>
																		<th class="column" width="325" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<td class="fluid-img" style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://fastenglish.com.mx/public/images/t3_image1.jpg" width="325" height="400" mc:edit="image_3" style="max-width:325px;" border="0" alt="" /></td>
																				</tr>
																			</table>
																		</th>
																		<th class="column" width="325" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<td class="content" style="padding:30px 50px;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td class="h2-white left pb20" style="color:#ffffff; font-family:Arial, sans-serif; font-size:52px; line-height:58px; text-transform:uppercase; font-weight:bold; text-align:left; padding-bottom:20px;"><div mc:edit="text_4">CLASES <br />EN VIVO</div></td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</th>
																	</tr>
																</table>
															</div>
															<!-- END Intro -->

															<!-- Three Products -->
															<div mc:repeatable="Select" mc:variant="Three Products">
																<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
																	<tr>
																		<td class="p30-15" style="padding: 60px 30px;">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<td>
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<th class="column-top" width="190" bgcolor="#ffffff" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																									<table width="100%" border="0" cellspacing="0" cellpadding="0">
																										<tr>
																											<td class="text-title2 pb15 m-center" style="color:#000000; font-family:'Noto Serif', Georgia, serif; font-size:18px; line-height:24px; text-align:left; padding-bottom:15px;">
																												<div >
																													Hola, si eres alumno de nuevo ingreso o te perdiste la clase 1 este mensaje es para recordarte que el sábado 20 de mayo tendremos nuestra primera clase del curso EXCI de 9:00 a.m. a 2:00 p.m. vía Zoom, recuerda que para tomar el curso tu cuenta debe estar al corriente, así que los datos de la sesión son los siguientes:
																													<br/>
																													<br/>
																												</div>
																											</td>
																										</tr>

																										<tr>
																											<td class="text-title2 pb15 m-center" style="color:#000000; font-family:'Noto Serif', Georgia, serif; font-size:32px; line-height:24px; text-align:left; padding-bottom:15px;"><div mc:edit="text_8">Instrucciones</div></td>
																										</tr>
																										
																										<tr>
																											<td class="text2 pb20 m-center" style="color:#585858; font-family:'Raleway', Arial,sans-serif; font-size:18px; line-height:26px; text-align:left; padding-bottom:20px;">
																												<div mc:edit="text_12">
																													1-. Descargar zoom
																												</div>
																											</td>
																										</tr>
																										<tr>
																											<td align="left">
																												<table class="center" width="140" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">
																													<tr>
																														<td class="text-button purple-button" style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:center; text-transform:uppercase; padding:10px; background:transparent; border:1px solid #000000; color:#000000;"><div mc:edit="text_11"><a href="https://zoom.us/download" target="_blank" class="link-black" style="color:#000001; text-decoration:none;"><span class="link-black" style="color:#000001; text-decoration:none;">Descargar</span></a></div></td>
																													</tr>
																												</table>
																											</td>
																										</tr>

																										<tr>
																											<td class="text2 pb20 m-center" style="color:#585858; font-family:'Raleway', Arial,sans-serif; font-size:18px; line-height:26px; text-align:left; padding-bottom:20px;">
																												<div mc:edit="text_12">
																													<br/>
																													<br/>
																													2-. Ingresar a la reunión
																													<br/>
																													<b>ID:</b> 316 439 6948
																													<br/>
																													<b>Contraseña:</b> 123
																													<br/>
																													<br/>

																													Ó bien, mediante el siguiente link
																												</div>
																											</td>
																										</tr>
																										<!-- Button -->
																										<tr>
																											<td align="left">
																												<table class="center" width="140" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">
																													<tr>
																														<td class="text-button purple-button" style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:center; text-transform:uppercase; padding:10px; background:transparent; border:1px solid #000000; color:#000000;"><div mc:edit="text_11">
																															<a href="https://us02web.zoom.us/j/3164396948?pwd=eHFxQ205bUJhR3ZUa1B6N0JGOTdFZz09" target="_blank" class="link-black" style="color:#000001; text-decoration:none;"><span class="link-black" style="color:#000001; text-decoration:none;">Link</span></a></div></td>
																													</tr>
																												</table>
																											</td>
																										</tr>

																										<tr>
																											<td class="text-title2 pb15 m-center" style="color:#000000; font-family:'Noto Serif', Georgia, serif; font-size:32px; line-height:24px; text-align:left; padding-bottom:15px;"><div mc:edit="text_8"><br/><br/>Horarios</div></td>
																										</tr>

																										<tr>
																											<td class="text2 pb20 m-center" style="color:#585858; font-family:'Raleway', Arial,sans-serif; font-size:18px; line-height:26px; text-align:left; padding-bottom:20px;">
																												<div mc:edit="text_12">
																													<b>Días:</b> Domingo 15 de mayo y domingo 22 de mayo<br/>
																													<b>Horario:</b> de 9 a.m. a 2 p.m.<br/>
																													<b>Modalidad:</b> ONLINE
																												</div>
																											</td>
																										</tr>


																										<tr>
																											<td class="text-title2 pb15 m-center" style="color:#000000; font-family:'Noto Serif', Georgia, serif; font-size:32px; line-height:24px; text-align:left; padding-bottom:15px;"><div mc:edit="text_8"><br/><br/>Aplicación móvil</div></td>
																										</tr>

																										<tr>
																											<td class="text2 pb20 m-center" style="color:#585858; font-family:'Raleway', Arial,sans-serif; font-size:18px; line-height:26px; text-align:left; padding-bottom:20px;">
																												<div mc:edit="text_12">
																													Recuerda que para esta clase deberás haber descargado nuestra aplicación, ya que será necesaria para realizar los ejercicios de las clases. La puedes encontrar poniendo "Fast English" en tu playstore ya sea que tengas un teléfono Iphone o Android. Para acceder necesitarás tu matrícula, si aún no la tienes solicítala vía WhatsApp.
																													<br/>
																												</div>
																											</td>
																										</tr>

																										<tr>
																											<td class="text-title2 pb15 m-center" style="color:#000000; font-family:'Noto Serif', Georgia, serif; font-size:32px; line-height:24px; text-align:left; padding-bottom:15px;"><div mc:edit="text_8"><br/><br/>Pago</div></td>
																										</tr>

																										<tr>
																											<td class="text2 pb20 m-center" style="color:#585858; font-family:'Raleway', Arial,sans-serif; font-size:18px; line-height:26px; text-align:left; padding-bottom:20px;">
																												<div mc:edit="text_12">
																													Si aun no has liquidado tu beca, te recordamos que la fecha límite para hacerlo es el día 21 de mayo, antes del inicio de tu clase (9:00am), esto será requisito para que los ejercicios te aparezcan en tu plataforma y te permitan el acceso a la clase, de lo contrario esto te impedirá seguir avanzando a clase 2 y al examen de simulación y revisión de este. Recuerda que puedes realizar tu pago Aquí o bien por medio de depósito en OXXO, Seven o por transferencia Bancaria
																													<br/>
																												</div>
																											</td>
																										</tr>

																										<tr>
																											<td class="text-title2 pb15 m-center" style="color:#000000; font-family:'Noto Serif', Georgia, serif; font-size:32px; line-height:24px; text-align:left; padding-bottom:15px;"><div mc:edit="text_8"><br/><br/>Dudas</div></td>
																										</tr>
																										<tr>
																											<td class="text2 pb20 m-center" style="color:#585858; font-family:'Raleway', Arial,sans-serif; font-size:18px; line-height:26px; text-align:left; padding-bottom:20px;">
																												<div mc:edit="text_12">
																													<b>Envía un WhatsApp a:</b> (81) 2567 9723
																												</div>
																											</td>

																										</tr>

																										<!-- END Button -->
																									</table>
																								</th>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</div>
															<!-- END Three Products -->
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- END Main -->
								</td>
							</tr>
						</table>
					</body>
				</html>
		`
	  return correo
	},
};


module.exports = medioPagoArriba;