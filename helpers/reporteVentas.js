const PDFDocument = require('pdfkit');

const fs = require('fs');

const newReporteVentas = ({
	fechaini,
  nombre_completo,
  fecha_inicio_format,
  fecha_final_format,
  matriculas_nuevas,
	matriculas_liquidadas,
	matriculas_parciales,
	matriculas_nuevas_monto,
	matriculas_liquidadas_monto,
	matriculas_parciales_monto,
	matriculas_liquidadas_ant,
	matriculas_liquidadas_ant_monto,
	prospectosAll,
  prospectos_vendedora,
  prospectos_vendedora_percent,
  prospectos_mkt,
  prospectos_mkt_percent,
  prospectosv_cerrados_no_exito,
  prospectosv_cerrados_no_exito_percent,
  prospectosm_cerrados_no_exito,
  prospectosm_cerrados_no_exito_percent,
  prospectosv_cerrados_si_exito,
  prospectosv_cerrados_si_exito_percent,
  prospectosm_cerrados_si_exito,
  prospectosm_cerrados_si_exito_percent,

  prospectosv_seguimiento,
  prospectosm_seguimiento,
  prospectosv_seguimiento_percent,
  prospectosm_seguimiento_percent,

  prospectosv_seguimiento_buenos,
  prospectosv_seguimiento_malos,
  prospectosv_seguimiento_ambos,
  prospectosm_seguimiento_buenos,
  prospectosm_seguimiento_malos,
  prospectosm_seguimiento_ambos,

  prospectosv_cerrados_buenos,
  prospectosv_cerrados_malos,
  prospectosv_cerrados_ambos,
  prospectosm_cerrados_buenos,
  prospectosm_cerrados_malos,
  prospectosm_cerrados_ambos,

  riPorciento,
  reinscribibles,
  acumulado,
}) => {

  // Crear un nuevo documento PDF
  const doc = new PDFDocument();

  // Nombre del y lugar del documento con número de folio
  doc.pipe(fs.createWriteStream(`../../recibos-pagos/${nombre_completo}-${fechaini}.pdf`));

  // FOLIO DE PAGO
  doc.fontSize(14).font('Helvetica-Bold').text(`Reporte semanal por Vendedora`, 35, 40, { align: 'left' });
  doc.moveDown();
  
  // FECHA DE PAGO
  doc.fontSize(12).font('Helvetica-Bold').text(`Vendedora: ${ nombre_completo }`, 35, 75, { align: 'left' });
  doc.fontSize(12).font('Helvetica-Bold').text(`Fecha: ${ fechaini }`, 35, 75, { align: 'right' });

  doc.moveDown();
  doc.fontSize(10).font('Helvetica-Bold').text(`Rango de fechas:`, 35, 110, { align: 'left' });
  doc.fontSize(11).font('Helvetica-Bold').text(`${ fecha_inicio_format} al ${ fecha_final_format }`, 35, 125, { align: 'left' });

  // doc.moveDown();


  doc.fill('purple')
  doc.fontSize(13).font('Helvetica-Bold').text(`Matriculas Nuevas:`, 35, 170 );
  doc.fill('black')
  doc.fontSize(13).font('Helvetica-Bold').text(`# | ${ matriculas_nuevas } | ${ matriculas_nuevas_monto }`, 35, 190 );

  doc.fill('purple')
  doc.fontSize(13).font('Helvetica-Bold').text(`Matriculas Liquidadas:`, 210, 170 );
  doc.fill('black')
  doc.fontSize(13).font('Helvetica-Bold').text(`# | ${ matriculas_liquidadas } | ${ matriculas_liquidadas_monto }`, 210, 190 );

  doc.fill('purple')
  doc.fontSize(13).font('Helvetica-Bold').text(`Matriculas Parciales:`, 390, 170 );
  doc.fill('black')
  doc.fontSize(13).font('Helvetica-Bold').text(`# | ${ matriculas_parciales } | ${ matriculas_parciales_monto }`, 390, 190 );



  doc.fill('purple')
  doc.fontSize(13).font('Helvetica-Bold').text(`Liq. Mat. Anteriores:`, 35, 230 );
  doc.fill('black')
  doc.fontSize(13).font('Helvetica-Bold').text(`# | ${ matriculas_liquidadas_ant } | ${ matriculas_liquidadas_ant_monto }`, 35, 250 );

  doc.fill('purple')
  doc.fontSize(24).font('Helvetica-Bold').text(`${ prospectosAll } Contactos`, 35, 300, { align: 'center' } );
  doc.fill('black')

  // CONTACTOS

  doc.fontSize(13).font('Helvetica-Bold').text(`CONTACTOS MARKETING`, 35, 340 );
  doc.fill('purple')
  doc.fontSize(15).font('Helvetica-Bold').text(`${ prospectos_mkt }`, 35, 360 );
  doc.fill('black')
  doc.fontSize(11).font('Helvetica-Bold').text(`Contactos MKT`, 60, 363 );
  doc.fill('purple')
  doc.fill('purple')
  doc.fontSize(15).font('Helvetica-Bold').text(`%${ prospectos_mkt_percent }`, 150, 360 );
  doc.fill('black')
  doc.fill('black')

	doc.rect( 35, 375, 210, 5).stroke();
  // Dibujar la barra de progreso
  let barLength = (210 - 2) * (prospectos_mkt_percent / 100);
  doc.rect( 35 + 1, 375 + 1, barLength, 5 - 2).fill('green');
  doc.fill('black')


  doc.fontSize(13).font('Helvetica-Bold').text(`CONTACTOS SUCURSAL`, 310, 340 );
  doc.fill('purple')
  doc.fontSize(15).font('Helvetica-Bold').text(`${ prospectos_vendedora }`, 310, 360 );
  doc.fill('black')
  doc.fontSize(11).font('Helvetica-Bold').text(`Conctatos Sucursal`, 335, 363 );
  doc.fill('purple')
  doc.fontSize(15).font('Helvetica-Bold').text(`%${ prospectos_vendedora_percent }`, 450, 360 );
  doc.fill('black')

  doc.rect( 310, 375, 210, 5).stroke();
  // Dibujar la barra de progreso
  barLength = (210 - 2) * ( prospectos_vendedora_percent / 100);
  doc.rect( 310 + 1, 375 + 1, barLength, 5 - 2).fill('green');
  doc.fill('black')


  // SECCIÓN 2
  doc.fill('purple')
  doc.fontSize(15).font('Helvetica-Bold').text(`${ prospectosm_cerrados_no_exito }`, 35, 400 );
  doc.fill('black')
  doc.fontSize(11).font('Helvetica-Bold').text(`Cerrados Sin exito`, 60, 403 );
  doc.fill('purple')
  doc.fontSize(15).font('Helvetica-Bold').text(`%${ prospectosm_cerrados_no_exito_percent }`, 160, 400 );
  doc.fill('black')
  doc.fontSize(11).font('Helvetica-Bold').text(`Buenos: | ${ prospectosm_cerrados_buenos } | Malos | ${ prospectosm_cerrados_malos } | Sin Clasif. | ${ prospectosm_cerrados_ambos } |`, 35, 420 );
  doc.rect( 35, 435, 210, 5).stroke();
  // Dibujar la barra de progreso
  barLength = (210 - 2) * ( prospectosm_cerrados_no_exito_percent / 100);
  doc.rect( 35 + 1, 435 + 1, barLength, 5 - 2).fill('green');
  doc.fill('black')

  doc.fill('purple')
  doc.fontSize(15).font('Helvetica-Bold').text(`${ prospectosv_cerrados_no_exito }`, 310, 400 );
  doc.fill('black')
  doc.fontSize(11).font('Helvetica-Bold').text(`Cerrados Sin exito`, 335, 403 );
  doc.fill('purple')
  doc.fontSize(15).font('Helvetica-Bold').text(`%${ prospectosv_cerrados_no_exito_percent }`, 450, 400 );
  doc.fill('black')
  doc.fontSize(11).font('Helvetica-Bold').text(`Buenos: | ${ prospectosv_cerrados_buenos } | Malos | ${ prospectosv_cerrados_malos } | Sin Clasif. | ${ prospectosv_cerrados_ambos } |`, 310, 420 );
  doc.rect( 310, 435, 210, 5).stroke();
  // Dibujar la barra de progreso
  barLength = (210 - 2) * ( prospectosv_cerrados_no_exito_percent / 100);
  doc.rect( 310 + 1, 435 + 1, barLength, 5 - 2).fill('green');
  doc.fill('black')

  // SECCIÓN 3
  doc.fill('purple')
  doc.fontSize(15).font('Helvetica-Bold').text(`${ prospectosm_seguimiento }`, 35, 470 );
  doc.fill('black')
  doc.fontSize(11).font('Helvetica-Bold').text(`Contactos en seguimiento`, 60, 476 );
  doc.fill('purple')
  doc.fontSize(15).font('Helvetica-Bold').text(`%${ prospectosm_seguimiento_percent }`, 200, 470 );
  doc.fill('black')
  doc.fontSize(11).font('Helvetica-Bold').text(`Buenos: | ${ prospectosm_seguimiento_buenos } | Malos | ${ prospectosm_seguimiento_malos } | Sin Clasif. | ${ prospectosm_seguimiento_ambos } |`, 35, 490 );

  doc.rect( 35, 505, 210, 5).stroke();
  // Dibujar la barra de progreso
  barLength = (210 - 2) * ( prospectosm_seguimiento_percent / 100);
  doc.rect( 35 + 1, 505 + 1, barLength, 5 - 2).fill('green');
  doc.fill('black')


  doc.fill('purple')
  doc.fontSize(15).font('Helvetica-Bold').text(`${ prospectosv_seguimiento }`, 310, 470 );
  doc.fill('black')
  doc.fontSize(11).font('Helvetica-Bold').text(`Contactos en seguimiento`, 335, 476 );
  doc.fill('purple')
  doc.fontSize(15).font('Helvetica-Bold').text(`%${ prospectosv_seguimiento_percent }`, 480, 470 );
  doc.fill('black')
  doc.fontSize(11).font('Helvetica-Bold').text(`Buenos: | ${ prospectosv_seguimiento_buenos } | Malos | ${ prospectosv_seguimiento_malos } | Sin Clasif. | ${ prospectosv_seguimiento_ambos } |`, 310, 490 );
  doc.rect( 310, 505, 210, 5).stroke();
  // Dibujar la barra de progreso
  barLength = (210 - 2) * ( prospectosv_seguimiento_percent / 100);
  doc.rect( 310 + 1, 505 + 1, barLength, 5 - 2).fill('green');
  doc.fill('black')

  // SECCIÓN 4
  doc.fill('purple')
  doc.fontSize(15).font('Helvetica-Bold').text(`${ prospectosm_cerrados_si_exito }`, 35, 540 );
  doc.fill('black')
  doc.fontSize(11).font('Helvetica-Bold').text(`Inscritos y % de conven.`, 60, 543 );
  doc.fill('purple')
  doc.fontSize(15).font('Helvetica-Bold').text(`%${ prospectosm_cerrados_si_exito_percent }`, 200, 540 );
  doc.fill('black')
  doc.rect( 35, 555, 210, 5).stroke();
  // Dibujar la barra de progreso
  barLength = (210 - 2) * ( prospectosm_seguimiento_percent / 100);
  doc.rect( 35 + 1, 555 + 1, barLength, 5 - 2).fill('green');
  doc.fill('black')

  doc.fill('purple')
  doc.fontSize(15).font('Helvetica-Bold').text(`${ prospectosv_cerrados_si_exito }`, 310, 540 );
  doc.fill('black')
  doc.fontSize(11).font('Helvetica-Bold').text(`Inscritos y % de conven.`, 335, 543 );
  doc.fill('purple')
  doc.fontSize(15).font('Helvetica-Bold').text(`%${ prospectosv_cerrados_si_exito_percent }`, 475, 540 );
  doc.fill('black')
	doc.rect( 310, 555, 210, 5).stroke();
  // Dibujar la barra de progreso
  barLength = (210 - 2) * ( prospectosv_seguimiento_percent / 100);
  doc.rect( 310 + 1, 555 + 1, barLength, 5 - 2).fill('green');
  doc.fill('black')


  doc.dash(1, { space: 2 }); // Establecer el patrón de línea punteada
  doc.moveTo(30, 580).lineTo(580, 580).stroke(); // Dibujar línea punteada horizontal
  doc.undash();

  doc.fill('purple')
  doc.fontSize(13).font('Helvetica-Bold').text(`REPORTE RI`, 35, 600 );


  doc.fill('black')
  doc.fontSize(18).font('Helvetica-Bold').text(`Reinscribibles: ${ reinscribibles }`, 35, 630  );
  doc.fontSize(18).font('Helvetica-Bold').text(`Acumulado: ${ acumulado }`, 250, 630 );
  doc.fontSize(18).font('Helvetica-Bold').text(`${ riPorciento } %RI`, 400, 630, { align: 'center' } );

  
  doc.end();

}

module.exports = newReporteVentas;
