const PDFDocument = require('pdfkit');

const fs = require('fs');

const newPDF = ({
  folio,
	alumno,
	matricula,
	plantel,
	ciclo,
	curso,
	cantidad_recibida,
	cantidad_letra,
	monto_descuento,
	adeudo,
	vendedora,
	fecha_pago
}) => {

  // Crear un nuevo documento PDF
  const doc = new PDFDocument();

  // Nombre del y lugar del documento con número de folio
  doc.pipe(fs.createWriteStream(`../../recibos-pagos/${ folio }.pdf`));


  if( plantel.match('FAST') ){
    doc.image('../../recibos-pagos/logo_fast.png', 35, 10, { width: 95 });
  }else{
    doc.image('../../recibos-pagos/inbi.png', 35, 10, { width: 70 });
  }

  // FOLIO DE PAGO
  doc.fontSize(12).font('Helvetica-Bold').text(`Folio de pago: ${ folio }`, 35, 120, { align: 'left' });
  
  // FECHA DE PAGO
  doc.fontSize(12).font('Helvetica-Bold').text(`Fecha pago: ${ fecha_pago }`, 35, 120, { align: 'right' });

  doc.moveDown();
  doc.moveDown();
  
  doc.fontSize(11).font('Helvetica-Bold').text( `Alumno`             , 35, 150, { align: 'left' });
  doc.fontSize(11).font('Helvetica').text( `${ alumno }`             , 200, 150 );

  doc.fontSize(11).font('Helvetica-Bold').text( `Matricula`          , 35, 170, { align: 'left' });
  doc.fontSize(11).font('Helvetica').text( `${ matricula }`          , 200, 170 );
  
  doc.fontSize(11).font('Helvetica-Bold').text( `Plantel`            , 35, 190, { align: 'left' });
  doc.fontSize(11).font('Helvetica').text( `${ plantel }`            , 200, 190 );
  
  doc.fontSize(11).font('Helvetica-Bold').text( `Ciclo`              , 35, 210, { align: 'left' });
  doc.fontSize(11).font('Helvetica').text( `${ ciclo }`              , 200, 210 );
  
  doc.fontSize(11).font('Helvetica-Bold').text( `Curso`              , 35, 230, { align: 'left' });
  doc.fontSize(11).font('Helvetica').text( `${ curso }`              , 200, 230 );
  
  doc.fontSize(11).font('Helvetica-Bold').text( `Cantidad Recibida`  , 35, 250, { align: 'left' });
  doc.fontSize(11).font('Helvetica').text( `${ cantidad_recibida }`  , 200, 250 );
  
  doc.fontSize(11).font('Helvetica-Bold').text( `Cantidad en Letra`  , 35, 270, { align: 'left' });
  doc.fontSize(11).font('Helvetica').text( `${ cantidad_letra }`     , 200, 270 );
  
  doc.fontSize(11).font('Helvetica-Bold').text( `Monto de Descuento` , 35, 290, { align: 'left' });
  doc.fontSize(11).font('Helvetica').text( `${ monto_descuento }`    , 200, 290 );
  
  doc.fontSize(11).font('Helvetica-Bold').text( `Adeudo del ciclo`   , 35, 310, { align: 'left' });
  doc.fontSize(11).font('Helvetica').text( `${ adeudo }`             , 200, 310 );
  
  doc.fontSize(11).font('Helvetica-Bold').text( `Recibió`            , 35, 330, { align: 'left' });
  doc.fontSize(11).font('Helvetica').text( `${ vendedora }`          , 200, 330 );


  // NO SE ACEPTAN DEVOLUCIONES
  doc.image('../../recibos-pagos/sello.jpeg', 430, 300, { width: 120 });
  
  doc.end();

}

module.exports = newPDF;
