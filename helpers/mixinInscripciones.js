const inscripciones     = require("../models/operaciones/inscripciones.model");
const becas             = require("../models/operaciones/becas.model");

//const constructor
const mixinInscripciones = {

	async validarEmpleado ( id_alumno, id_grupo, aplicaBeca ) {
		return new Promise(async(resolve, reject) => {
			try{
				const empleado = await inscripciones.getAlumnoEmpleadoActivo( id_alumno ).then( response => response )
		      
		    if( empleado  ){
		      // AGREGAR BECA AL SISTEMA
		      // Verificar que no haya una beca
		      if( !aplicaBeca ){
		        aplicaBeca   = await becas.solicitarBeca( id_alumno, id_grupo ).then( response => response )
		      }

		      aplicaBeca   = await becas.getBecaGrupoAlumno( id_alumno, id_grupo ).then( response => response )

		      resolve(aplicaBeca)

		    }else{
		    	resolve(false)
		    }
		  }catch( error ){
				reject( error )
			}
		})
	},


	async validaDescuentoAplicado ( tipoAlumno, descuentos, descuentoPorAplicar, id_alumno, factura ) {
		return new Promise(async(resolve, reject) => {
			try{
				// GUARDAR EL DESCUENTO QUE SE DEBE APLICAR
		    let descuentoAplicado = null

		    // Si es tipo 2 ( alumno regular ) -> el numero de descuento es en base al día o semana en la que esta pagando 
		    if( tipoAlumno == 2 || tipoAlumno == 5 )
		      descuentoAplicado = descuentoPorAplicar.find( el => el.numero_descuento == descuentos.numero_descuento )

		    // Si es tipo 1 ( alumno NI ) -> el numero del descuento siempre es 1
		    if( tipoAlumno == 1 || factura )
		      descuentoAplicado = descuentoPorAplicar.find( el => el.numero_descuento == 1 )
		    else if ( tipoAlumno == 3 )
		      descuentoAplicado = descuentoPorAplicar.find( el => el.numero_descuento == 3 )

		    if( tipoAlumno == 4 ){
		      // Validar si es de nuevo ingreso y para eso hay que evaluar los grupos en los que esta inscrito el alumno
		      const gruposAsignado = await inscripciones.getGruposAlumnoNi( id_alumno ).then( response => response )
		      if( gruposAsignado.length <= 1 ){
		        descuentoAplicado = descuentoPorAplicar.find( el => el.numero_descuento == 1 )
		      }else{
		        descuentoAplicado = descuentoPorAplicar.find( el => el.numero_descuento == descuentos.numero_descuento )
		      }
		    }

		    resolve( descuentoAplicado )
			}catch( error ){
				reject( error )
			}
	  })
	},

	validaSaldoFavor ( id_alumno ) {
		return new Promise(async(resolve, reject) => {
			try{
				const saldoPositivo = await inscripciones.getSaldoPositivo( id_alumno ).then( response => response )
		    const saldoNegativo = await inscripciones.getSaldoNegativo( id_alumno ).then( response => response )

		    if( !saldoPositivo ||  !saldoNegativo){ reject({ message: 'Error en los saldos, favor de comunicarte con sistemas :p '}) }

		    const { acumulado } = saldoPositivo
		    const { resta }     = saldoNegativo

		    let saldoFavor    = acumulado - resta
				resolve( saldoFavor )
			}catch( error ){
				reject( error )
			}
		})
	},

	obtenerSaldoFavorGrupo ( id_alumno, id_grupo ) {
		return new Promise(async(resolve, reject) => {
			try{
				const saldoPositivo = await inscripciones.getSaldoFavorGrupo( id_alumno, id_grupo ).then( response => response )

				const { acumulado } = saldoPositivo

		    let saldoFavor    = acumulado

				resolve( saldoFavor )
				
			}catch( error ){
				reject( error )
			}
		})
	}
};


module.exports = mixinInscripciones;