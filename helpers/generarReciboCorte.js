const PDFDocument = require('pdfkit');

const fs = require('fs');

const reciboCorte = ({
  folio,
  usuario,
  fecha,
  plantel,
  desglose,
  montos
}) => {

  // Crear un nuevo documento PDF
  const doc = new PDFDocument();

  // Nombre del y lugar del documento con número de folio
  doc.pipe(fs.createWriteStream(`../../recibos-pagos/${ folio }.pdf`));

  // FOLIO DE PAGO
  doc.fontSize(14).font('Helvetica-Bold').text(`Corte Diario: ${ plantel }`, 40, 40, { align: 'center' });

  // DATOS INICIALES
  doc.fontSize(12).font('Helvetica-Bold').text(`Sucursal: ${ plantel }`, 35, 80, { align: 'left' });
  doc.fontSize(12).font('Helvetica-Bold').text(`Nombre: ${ usuario }`, 35, 100, { align: 'left' });
  doc.fontSize(12).font('Helvetica-Bold').text(`Fecha Reporte: ${ fecha }`, 35, 120, { align: 'left' });


  // DESGLOSEEEEEEEEEEEEEEEEEEEEEEEEE

  doc.fontSize(8).text(`Folio`, 35, 180 ,{
	  width: 30,
	  align: 'justify'
	})

	doc.fontSize(8).text(`Fecha`, 70, 180 , {
	  width: 40,
	  align: 'justify'
	})

	doc.fontSize(8).text(`Monto`, 170, 180 , {
	  width: 45,
	  align: 'justify'
	})

	doc.fontSize(8).text(`Tipo de Pago`, 230, 180 , {
	  width: 60,
	  align: 'justify'
	})

	doc.fontSize(8).text(`Estudiante`, 300, 180 , {
	  width: 60,
	  align: 'justify'
	})

	doc.fontSize(8).text(`Ciclo`, 430, 180 , {
	  width: 60,
	  align: 'justify'
	})

	doc.fontSize(8).text(`Plantel`, 500, 180 , {
	  width: 60,
	  align: 'justify'
	})


  let inicio = 35
  let final  = 200

  for (const movimiento of desglose) {

  	doc.fontSize(9).font('Helvetica').text(`${movimiento.id_ingreso}`, 35, final, {
		  width: 35,
		  align: 'left'
		})
		doc.rect( 35, final - 3, 33, 30).stroke();


		doc.fontSize(9).font('Helvetica').text(`${movimiento.fecha_pago}`, 70, final, {
		  width: 160,
		  align: 'left'
		})
		doc.rect( 68, final - 3, 160, 30).stroke();


		doc.fontSize(9).font('Helvetica').text(`${movimiento.monto_pagado}`, 170, final, {
		  width: 60,
		  align: 'left'
		})
		doc.rect( 168, final - 3, 60, 30).stroke();


		doc.fontSize(9).font('Helvetica').text(`${movimiento.forma_pago}`, 230, final, {
		  width: 70,
		  align: 'left'
		})
		doc.rect( 228, final - 3, 70, 30).stroke();


		doc.fontSize(9).font('Helvetica').text(`${movimiento.alumno}`, 300, final, {
		  width: 130,
		  align: 'left'
		})
		doc.rect( 298, final - 3, 130, 30).stroke();


		doc.fontSize(9).font('Helvetica').text(`${movimiento.ciclo}`, 430, final, {
		  width: 70,
		  align: 'left'
		})
		doc.rect( 428, final - 3, 70, 30).stroke();


		doc.fontSize(9).font('Helvetica').text(`${movimiento.plantel}`, 500, final, {
		  width: 80,
		  align: 'left'
		})
		doc.rect( 498, final - 3, 80, 30).stroke();

		final += 30
  }



  // MONTOS FINALES
  const montosFinales = montos.filter( el => { return el.id_plantel == 1000 })

  const efectivo       = montosFinales[0].efectivo
	const transferencia  = montosFinales[0].transferencia
	const depositos      = montosFinales[0].depositos
	const tarjeta        = montosFinales[0].tarjeta
	const total          = montosFinales[0].total



	final += 30
  doc.fontSize(10).font('Helvetica-Bold').text(`Total cobrado: `, 35, final , { align: 'left' });
  doc.fontSize(10).font('Helvetica').text(total, 150, final , { align: 'left' });
	
	final += 18
	doc.rect( 35, final - 8, 170, .5).stroke();
  doc.fontSize(10).font('Helvetica-Bold').text(`Tarjeta: `, 35, final, { align: 'left' });
  doc.fontSize(10).font('Helvetica').text(tarjeta, 150, final, { align: 'left' });
	
	final += 18
	doc.rect( 35, final - 8, 170, .5).stroke();
  doc.fontSize(10).font('Helvetica-Bold').text(`Depositos:`, 35, final, { align: 'left' });
  doc.fontSize(10).font('Helvetica').text(depositos, 150, final, { align: 'left' });
	
	final += 18
	doc.rect( 35, final - 8, 170, .5).stroke();
  doc.fontSize(10).font('Helvetica-Bold').text(`Transferencias: `, 35, final, { align: 'left' });
  doc.fontSize(10).font('Helvetica').text(transferencia, 150, final, { align: 'left' });
  
  final += 18
	doc.rect( 35, final - 8, 170, .5).stroke();
  doc.fontSize(10).font('Helvetica-Bold').text(`Total a entregar: `, 35, final, { align: 'left' });
  doc.fontSize(10).font('Helvetica').text(efectivo, 150, final, { align: 'left' });
	doc.rect( 35, final + 10, 170, .5).stroke();
	

  // doc.moveDown();
  // doc.moveDown();
  
  
  doc.end();

}

module.exports = reciboCorte;
