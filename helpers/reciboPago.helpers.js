const config     = require("../config/index.js");
const nodemailer = require('nodemailer');


//const constructor
const helperReciboPago = {

	plantillaReciboPago ( payload ) {

		let USUARIO_CORREO, PASSWORD_CORREO, LOGO = ''

		if( payload.unidad_negocio == 1 ){
			USUARIO_CORREO  = 'staff.inbischool@gmail.com'
	    PASSWORD_CORREO = 'sgjywlyabjawvlzv'
	    LOGO            = 'https://inbi.mx/public/images/logo.png'
	    CONDICION_PAGO  = 'https://inbi.mx/public/images/sello.jpeg'
	    CHECK_PAGO      = 'https://inbi.mx/public/images/check-icon.png'
		}else{
	    USUARIO_CORREO  = 'staff.fastenglish@gmail.com'
	    PASSWORD_CORREO = 'jzxmdixwyauwxfbf'
	    LOGO            = 'https://fastenglish.com.mx/public/images/logo_fast.png'
	    CONDICION_PAGO  = 'https://fastenglish.com.mx/public/images/sello.jpeg'
	    CHECK_PAGO      = 'https://fastenglish.com.mx/public/images/check-icon.png'
		}


		const correo = `
			<!DOCTYPE html>

			<html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
			<head>
				<title></title>
				<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
				<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
				<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
				<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css"/>
				<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css"/>
				<link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet" type="text/css"/>
				<link href="https://fonts.googleapis.com/css?family=Abril+Fatface" rel="stylesheet" type="text/css"/>
				<style>
					* {
						box-sizing: border-box;
					}

					body {
						margin: 0;
						padding: 0;
					}

					a[x-apple-data-detectors] {
						color: inherit !important;
						text-decoration: inherit !important;
					}

					#MessageViewBody a {
						color: inherit;
						text-decoration: none;
					}

					p {
						line-height: inherit
					}

					.desktop_hide,
					.desktop_hide table {
						mso-hide: all;
						display: none;
						max-height: 0px;
						overflow: hidden;
					}

					@media (max-width:700px) {

						.desktop_hide table.icons-inner,
						.social_block.desktop_hide .social-table {
							display: inline-block !important;
						}

						.icons-inner {
							text-align: center;
						}

						.icons-inner td {
							margin: 0 auto;
						}

						.fullMobileWidth,
						.row-content {
							width: 100% !important;
						}

						.mobile_hide {
							display: none;
						}

						.stack .column {
							width: 100%;
							display: block;
						}

						.mobile_hide {
							min-height: 0;
							max-height: 0;
							max-width: 0;
							overflow: hidden;
							font-size: 0px;
						}

						.desktop_hide,
						.desktop_hide table {
							display: table !important;
							max-height: none !important;
						}
					}
				</style>
			</head>
			<body style="background-color: #f9f9f9; margin: 0; padding: 0; -webkit-text-size-adjust: none; text-size-adjust: none;">
			<table border="0" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f9f9f9;" width="100%">
			<tbody>
				<tr>
					<td>

						<!-- PAGO RECIBIDO -->
						<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
							<tbody>
								<tr>
									<td>
										<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #cbdbef; color: #000000; width: 680px;" width="680">
											<tbody>
												<tr>
													<td class="column column-1" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 20px; padding-bottom: 20px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
														<table border="0" cellpadding="0" cellspacing="0" class="image_block block-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
															<tr>
																<td class="pad" style="width:100%;padding-right:0px;padding-left:0px;padding-top:20px;">
																	<div align="center" class="alignment" style="line-height:10px"><img alt="Check Icon" src="${ CHECK_PAGO }" style="display: block; height: auto; border: 0; width: 93px; max-width: 100%;" title="Check Icon" width="93"/></div>
																</td>
															</tr>
														</table>
														<table border="0" cellpadding="0" cellspacing="0" class="text_block block-3" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
															<tr>
																<td class="pad" style="padding-bottom:25px;padding-left:20px;padding-right:20px;padding-top:10px;">
																	<div style="font-family: Georgia, 'Times New Roman', serif">
																		<div class="" style="font-size: 14px; font-family: Georgia, Times, 'Times New Roman', serif; mso-line-height-alt: 16.8px; color: #2f2f2f; line-height: 1.2;">
																			<p style="margin: 0; font-size: 14px; text-align: center; mso-line-height-alt: 16.8px;"><span style="font-size:42px;">Pago Recibido</span></p>
																		</div>
																	</div>
																</td>
															</tr>
														</table>
														<table border="0" cellpadding="0" cellspacing="0" class="text_block block-4" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
															<tr>
																<td class="pad" style="padding-bottom:20px;padding-left:30px;padding-right:30px;padding-top:10px;">
																	<div style="font-family: sans-serif">
																		<div class="" style="font-size: 14px; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 21px; color: #2f2f2f; line-height: 1.5;">
																			<p style="margin: 0; font-size: 14px; text-align: center; mso-line-height-alt: 24px;"><span style="font-size:16px;">Hola <strong><u>${payload.alumno}</u></strong>,</span></p>
																			<p style="margin: 0; font-size: 14px; text-align: center; mso-line-height-alt: 21px;"> </p>
																			<p style="margin: 0; font-size: 14px; text-align: center; mso-line-height-alt: 24px;"><span style="font-size:16px;">Gracias por tu pago de <strong><span style="">${payload.cantidad_recibida}</span></strong> el dia <strong><span style="">${payload.fecha_pago}</span></strong></span></p>
																		</div>
																	</div>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>

						<!-- DETALLE Del PAGO -->
						<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-3" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
							<tbody>
								<tr>
									<td>
										<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff; color: #000000; width: 680px;" width="680">
											<tbody>
												<tr>
													<td class="column column-1" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 0px; padding-bottom: 0px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
														<table border="0" cellpadding="0" cellspacing="0" class="text_block block-1" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
															<tr>
																<td class="pad" style="padding-bottom:20px;padding-left:20px;padding-right:20px;padding-top:50px;">
																	<div style="font-family: sans-serif">
																		<div class="" style="font-size: 14px; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 16.8px; color: #2f2f2f; line-height: 1.2;">
																			<p style="margin: 0; text-align: center; mso-line-height-alt: 16.8px; letter-spacing: 1px;"><strong><span style="font-size:18px;">DETALLE DEL PAGO</span></strong></p>
																		</div>
																	</div>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>

						<!-- DESGLOSE DEL PAGO -->
						<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-4" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
							<tbody>
								<tr>
									<td>
										<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff; color: #000000; width: 680px;" width="680">
											<tbody>
												<tr>

													<td class="column column-1" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; border-bottom: 0px solid #5D77A9; border-left: 0px solid #5D77A9; border-right: 0px solid #5D77A9; border-top: 0px solid #5D77A9; vertical-align: top;" width="30%">
														<table border="0" cellpadding="0" cellspacing="0" class="text_block block-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
															<tr>
																<td class="pad" style="padding-bottom:15px;padding-left:20px;padding-right:20px;padding-top:15px;">
																	<div style="font-family: sans-serif">
																		<div class="" style="font-size: 14px; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 28px; color: #393d47; line-height: 2; text-align: left;">
																			<p style="margin: 0; font-size: 12px;  mso-line-height-alt: 32px;">
																				<strong>
																					<span style="font-size: 12px;">
																						<span style="color:#5d77a9;">Folio de pago:</span>
																					</span>
																				</strong>
																			</p>
																			
																			<p style="margin: 0; font-size: 12px;  mso-line-height-alt: 32px;">
																				<strong>
																					<span style="font-size: 12px;">
																						<span style="color:#5d77a9;">Alumno:
																						</span>
																					</span>
																				</strong>
																			</p>
																			<p style="margin: 0; font-size: 12px;  mso-line-height-alt: 32px;">
																				<strong>
																					<span style="font-size: 12px;">
																						<span style="color:#5d77a9;">Matricula:
																						</span>
																					</span>
																				</strong>
																			</p>
																			<p style="margin: 0; font-size: 12px;  mso-line-height-alt: 32px;">
																				<strong>
																					<span style="font-size: 12px;">
																						<span style="color:#5d77a9;">Plantel:
																						</span>
																					</span>
																				</strong>
																			</p>
																			<p style="margin: 0; font-size: 12px;  mso-line-height-alt: 32px;">
																				<strong>
																					<span style="font-size: 12px;">
																						<span style="color:#5d77a9;">Ciclo:
																						</span>
																					</span>
																				</strong>
																			</p>
																			<p style="margin: 0; font-size: 12px;  mso-line-height-alt: 32px;">
																				<strong>
																					<span style="font-size: 12px;">
																						<span style="color:#5d77a9;">Curso:
																						</span>
																					</span>
																				</strong>
																			</p>
																			<p style="margin: 0; font-size: 12px;  mso-line-height-alt: 32px;">
																				<strong>
																					<span style="font-size: 12px;">
																						<span style="color:#5d77a9;">Cantidad Recibida:
																						</span>
																					</span>
																				</strong>
																			</p>
																			<p style="margin: 0; font-size: 12px;  mso-line-height-alt: 32px;">
																				<strong>
																					<span style="font-size: 12px;">
																						<span style="color:#5d77a9;">Cantidad en Letra:
																						</span>
																					</span>
																				</strong>
																			</p>
																			<p style="margin: 0; font-size: 12px;  mso-line-height-alt: 32px;">
																				<strong>
																					<span style="font-size: 12px;">
																						<span style="color:#5d77a9;">Monto de Descuento:
																						</span>
																					</span>
																				</strong>
																			</p>
																			<p style="margin: 0; font-size: 12px;  mso-line-height-alt: 32px;">
																				<strong>
																					<span style="font-size: 12px;">
																						<span style="color:#5d77a9;">Adeudo del Ciclo
																						</span>
																					</span>
																				</strong>
																			</p>
																			<p style="margin: 0; font-size: 12px;  mso-line-height-alt: 32px;">
																				<strong>
																					<span style="font-size: 12px;">
																						<span style="color:#5d77a9;">Recibio:
																						</span>
																					</span>
																				</strong>
																			</p>
																		</div>
																	</div>
																</td>
															</tr>
														</table>
													</td>

													<td class="column column-2" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; border-bottom: 0px solid #5D77A9; border-left: 0px solid #5D77A9; border-right: 0px solid #5D77A9; border-top: 0px solid #5D77A9; vertical-align: top;" width="70%">
														<table border="0" cellpadding="0" cellspacing="0" class="text_block block-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
															<tr>
																<td class="pad" style="padding-bottom:10px;padding-left:20px;padding-right:10px;padding-top:10px;">
																	<div style="font-family: sans-serif">

																		<div class="" style="font-size: 12px; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 28px; color: #2f2f2f; line-height: 2;">
																			<p style="margin: 0; font-size: 12px; text-align: left; mso-line-height-alt: 32px;">
																				<span style="font-size:12px;">
																					${payload.folio}
																				</span>
																			</p>
																			<p style="margin: 0; font-size: 12px; text-align: left; mso-line-height-alt: 32px;">
																				${payload.alumno}
																			</p>
																			<p style="margin: 0; font-size: 12px; text-align: left; mso-line-height-alt: 32px;">
																				${payload.matricula}
																			</p>
																			<p style="margin: 0; font-size: 12px; text-align: left; mso-line-height-alt: 32px;">
																				<span style="font-size:12px;">
																					${payload.plantel}
																				</span>
																			</p>
																			<p style="margin: 0; font-size: 12px; text-align: left; mso-line-height-alt: 32px;">
																				<span style="font-size:12px;">
																					${payload.ciclo}
																				</span>
																			</p>
																			<p style="margin: 0; font-size: 12px; text-align: left; mso-line-height-alt: 32px;">
																				${payload.curso}
																			</p>
																			<p style="margin: 0; font-size: 12px; text-align: left; mso-line-height-alt: 32px;">
																				$ ${payload.cantidad_recibida}
																			</p>
																			<p style="margin: 0; font-size: 12px; text-align: left; mso-line-height-alt: 32px;">
																				<span style="font-size:12px;">
																					${payload.cantidad_letra}
																				</span>
																			</p>
																			<p style="margin: 0; font-size: 12px; text-align: left; mso-line-height-alt: 32px;">
																				$ ${payload.descuento_pronto_pago}
																			</p>
																			<p style="margin: 0; font-size: 12px; text-align: left; mso-line-height-alt: 32px;">
																				$ ${payload.adeudo}
																			</p>
																			<p style="margin: 0; font-size: 12px; text-align: left; mso-line-height-alt: 32px;">
																				${payload.recepcion}
																			</p>
																		</div>
																	</div>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>

						<!-- LOGO Y CONDICIONES DE PAGO -->
						<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-6" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding-bottom: 30px;" width="100%">
							<tbody>
								<tr>
									<td>
										<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: white; color: #000000; width: 680px;" width="680">
											<tbody>
												<tr>
													<td class="column column-1" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="10%">
														<table border="0" cellpadding="0" cellspacing="0" class="image_block block-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
															<tr>
																<td class="pad" style="width:100%;padding-right:0px;padding-left:0px;">
																</td>
															</tr>
														</table>
													</td>
													<td class="column column-1" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="20%">
														<table border="0" cellpadding="0" cellspacing="0" class="image_block block-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
															<tr>
																<td class="pad" style="width:100%;padding-right:0px;padding-left:0px;">
																	<div align="center" class="alignment" style="line-height:10px">
																	<img alt="Boucher-img" class="fullMobileWidth" src="${ LOGO }" style="display: block; height: auto; border: 0; width: 340px; max-width: 100%;" title="Boucher-img" width="340"/></div>
																</td>
															</tr>
														</table>
														<!----------imagenes------------->
													</td>
													<td class="column column-2" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="30%">
														<table border="0" cellpadding="0" cellspacing="0" class="text_block block-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
															<tr>
																<td class="pad" style="padding-left:20px;padding-right:20px;">
																	<div style="font-family: sans-serif">
																		<div class="" style="font-size: 14px; mso-line-height-alt: 21px; color: #393d47; line-height: 1.5; font-family: 'Permanent Marker', Impact, Charcoal, sans-serif;">

																		</div>
																	</div>
																</td>
															</tr>
														</table>
													</td>
													<td class="column column-3" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="30%">
														<table border="0" cellpadding="0" cellspacing="0" class="text_block block-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
															<tr>
																<td class="pad" style="padding-left:20px;padding-right:20px;">
																	<div style="font-family: sans-serif">
																		<div class="" style="font-size: 14px; mso-line-height-alt: 21px; color: #393d47; line-height: 1.5; font-family: 'Permanent Marker', Impact, Charcoal, sans-serif;">

																			<div align="center" class="alignment" style="line-height:5px">
																			<img alt="logo-app" class="fullMobileWidth" src="${ CONDICION_PAGO }" style="display: block; height: auto; border: 0; width: 220px; max-width: 100%;" title="logo-app" width="340"/></div>

																		</div>
																	</div>
																</td>
															</tr>
														</table>
													</td>
													<td class="column column-1" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="10%">
														<table border="0" cellpadding="0" cellspacing="0" class="image_block block-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
															<tr>
																<td class="pad" style="width:100%;padding-right:0px;padding-left:0px;">
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</table><!-- End -->
				</body>
			</html>
		`
	  return correo
	},

	enviarCorreo ( asunto, correoHtml, correo, unidad_negocio ) {
		return new Promise(async (resolve, reject)=>{

			let user, pass, ESCUELA = ''
			if( unidad_negocio == 1 ){
				user =  'staff.inbischool@gmail.com'
		    pass = 'sgjywlyabjawvlzv'
		    ESCUELA = 'INBI SCHOOL'
			}else{
		    user =  'staff.fastenglish@gmail.com'
		    pass = 'jzxmdixwyauwxfbf'
		    ESCUELA = 'FAST ENGLISH'
			}

			let transporter = nodemailer.createTransport({
			  host: 'smtp.gmail.com',
			  port: 465, 
			  secure:true,
			  pool: true,
			  auth: {
			    user,
			    pass
			  }
			});

	    // Preparamos las opciones del correo
	    const mailOptions = {
	      from:        `${ ESCUELA } <info@testsnode.com>`,
	      to:          correo,
	      subject:     asunto,
	      html:        correoHtml,
	    }

	    // Enviamos el correo, PARAMETRO: mailOptions
	    await transporter.sendMail(mailOptions, (error, info) =>{
        if (error) {
        	
          return reject(error)
        } else {
        	
          return resolve(info.response)
        }
	    });

		})
	},
};


module.exports = helperReciboPago;