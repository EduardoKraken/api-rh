const prospectos           = require("../../models/lms/prospectoslms.model.js");
const exci                 = require("../../models/exci/exci.model.js");
const reciboExci           = require("../../helpers/reciboExci.helper.js");
const sendMatricula        = require("../../helpers/sendMatricula.helper.js");
const sendClaseEnVivo      = require("../../helpers/sendClaseEnVivo.helper.js");
const recordatorio         = require("../../helpers/recordatorio.helper.js");
const recordatorio2Partes  = require("../../helpers/pagoDosPartesExci.helper.js");
const catalogos            = require("../../models/lms/catalogoslms.model.js");
const medioPagoArriba      = require("../../helpers/medioPagoArriba.helper.js");
const remarketing          = require("../../helpers/remarketginPlantilla.helper.js");

const md5 = require('md5');


/******************************************/
/*            ENVIAR CORREOS              */
/******************************************/
const USUARIO_CORREO  =  'staff.fastenglish@gmail.com'
const PASSWORD_CORREO =  'jzxmdixwyauwxfbf'
const ESCUELA         =          'FAST ENGLISH'
const nodemailer      = require('nodemailer');

const user = USUARIO_CORREO
const pass = PASSWORD_CORREO

// Preparamos el transporte
let transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 465, 
  secure:true,
  pool: true,
  auth: {
    user,
    pass
  }
});

const userq = 'apps@inbi.mx'
const passq = 'H4v#3hr4'

// Preparamos el transporte
let transporter2 = nodemailer.createTransport({
  host: 'inbi.mx',
  port: 465, 
  pool: true,
  maxConnections: 100000,
  auth: {
    user: userq,
    pass: passq
  }
});

exports.pruebasMasivas = async(req, res) => {
  try {
    
    const alumnosParaRemarketing = await exci.getExciConAdeudo( ).then( response => response )

    const mailOptions = {
      from:    `FAST <eduardo.hdz@inbi.mx>`,
      to:      'eduardo.zav.dev@gmail.com',
      subject: 'Sin asunto',
      html:    '<div>Hola</div>',
    }
    
    for( const i in alumnosParaRemarketing ){
      if( parseInt( i ) < 150 ){
        // Enviamos el correo, PARAMETRO: mailOptions
        await transporter2.sendMail(mailOptions, (error, info) =>{
          if (error) {
            console.log( error )
          }else{
            console.log('enviado', i)
          }
        });
      }
    }

    // Enviamos el correo, PARAMETRO: mailOptions
    res.send({message: 'Correo enviado'});

  } catch (error) {
    res.status(500).send({message:error})
  }
};

// // Verifcamos que se pueda enviar el error
// transporter.verify().then(() =>{
//   // return resolve( transporter )
// }).catch((error)=>{
//   // console.log( error )
//   // Retornamos el error
//   // return res.status(500).send({ message: 'El correo no se pudo enviar, intenta más tarde'})
// })

/******************************************/
/******************************************/

// Enviar el recibo de pago al alumno
exports.enviarRecibo = async(req, res) => {
  try {
    const { matricula, adeudo, correo } = req.body
    // Actualizar los datos de pago
    // const updatePagoExci = await exci.updatePagoExci( req.body ).then( response=> response ) 

    // Preparamos el correo
    const correoHtml   = reciboExci.enviarReciboPagoExci( req.body )

    // enviarmos el correo, PARAMETROS: asunto, correo (fotmaro html), mail ( del alumno ), tranportes, archivos -> de ser necesario
    // Enviar imagen de los archivos

    const archivos = [{ path: 'https://fastenglish.com.mx/public/images/metodos_pago.jpeg' }]
    const correoEnviado  = await reciboExci.enviarCorreo( 'Recibo de pago', correoHtml, correo, transporter, archivos ).then( response=> response ) 

    res.send({message: 'Correo enviado'});

  } catch (error) {
    res.status(500).send({message:error})
  }
};

// Obtener a todos los alumnos registrados del exci y ver si ya tienen una matricula o no
exports.getRegistrosExci = async(req, res) => {
  try {

    let registros = await exci.getRegistrosExci( ).then( response => response )
    const usuariosFast = await catalogos.getUsuariosLMSFAST().then( response => response)

    const activarCuentas = await exci.activarCuentas( ).then( response => response )

    for( const i in registros){
      const { matricula } = registros[i]

      const existeMatricula = usuariosFast.find( el=> el.usuario == matricula )
      registros[i]['lms']    = existeMatricula ? 1 : 0 
    }

    res.send(registros);
  } catch (error) {
    res.status(500).send({message:error})
  }
};


// Obtener el listado de codigos
exports.getAlumnosAll = async(req, res) => {
  try {
    // Consultar todos los alumnos activos
    const alumnos = await exci.getAlumnosAll( ).then( response => response )

    res.send(alumnos);
  } catch (error) {
    res.status(500).send({message:error})
  }
};


// Obtener el listado de codigos
exports.addMatriculaAlumno = async(req, res) => {
  try {
    const { id_alumno, matricula, idexci_registro, nombre, correo, adeudo } = req.body
    // Consultar todos los alumnos activos
    const alumno = await exci.addMatriculaAlumno( id_alumno, matricula, idexci_registro ).then( response => response )

    // Preparamos el correo
    const correoHtml   = sendMatricula.enviarMatriculaExci( nombre,  matricula)

    // enviarmos el correo, PARAMETROS: asunto, correo (fotmaro html), mail ( del alumno )
    const correoEnviado  = await reciboExci.enviarCorreo( 'MATRICULA FAST ENGLISH', correoHtml, correo ).then( response=> response ) 

    res.send({message: 'Matricula registrada correctamente'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addSeguimientoAlumnoExci = async(req, res) => {
  try {
    const { idexci_registro, seguimiento } = req.body
    // Consultar todos los alumnos activos
    const alumno = await exci.addSeguimientoAlumnoExci( idexci_registro, seguimiento ).then( response => response )

    res.send({message: 'Seguimiento agregado correctamente'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.sendRecordatorioPago = async(req, res) => {
  try {

    // Actualizar los datos de pago
    const alumnosAdeudo = await exci.alumnosAdeudo( ).then( response=> response ) 

    // Preparamos el correo
    const correoHtml   = recordatorio.crearRecordatorioPago( )

    for( const i in alumnosAdeudo ){
      const { correo } = alumnosAdeudo[i]
      // enviarmos el correo, PARAMETROS: asunto, correo (fotmaro html), mail ( del alumno )
      const archivos = null
      const correoEnviado  = await reciboExci.enviarCorreo( 'Becas disponibles', correoHtml, correo , transporter, archivos ).then( response=> response ) 
    }
    
    res.send({message: 'Correo enviado', alumnosAdeudo});

  } catch (error) {
    res.status(500).send({message:error})
  }
};


exports.updateCorreoExci = async(req, res) => {
  try {
    const { idexci_registro, correo } = req.body
    // Actualizar los datos de pago
    const updateCorreoExci = await exci.updateCorreoExci( correo, idexci_registro ).then( response=> response ) 

    res.send({message: 'Correo actualizado'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};


exports.sendMatricula = async(req, res) => {
  try {
    const { idexci_registro, correo, matricula, nombre } = req.body

    // Preparamos el correo
    const correoHtml   = sendMatricula.enviarMatriculaExci( nombre,  matricula)

    // enviarmos el correo, PARAMETROS: asunto, correo (fotmaro html), mail ( del alumno )
    const archivos = null
    const correoEnviado  = await reciboExci.enviarCorreo( 'MATRICULA FAST ENGLISH', correoHtml, correo, transporter, archivos ).then( response=> response ) 

    res.send({message: 'Correo actualizado'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.sendZoom = async(req, res) => {
  try {
    const { correo } = req.body

    const correoHtml     = await sendClaseEnVivo.enviarCorreoClasesEnVivo()
    const archivos = null
    const correoEnviado  = await reciboExci.enviarCorreo( 'CLASES EN VIVO', correoHtml, correo, transporter, archivos ).then( response=> response ) 

    res.send({message: 'Correo enviado correctamente'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.sendRecordatorioPagoIndividual = async(req, res) => {
  try {
    const { correo, folio } = req.body
    
    const correoHtml   = recordatorio.crearRecordatorioPago( folio )
    const correoEnviado  = await reciboExci.enviarCorreo( 'Becas disponibles', correoHtml, correo ).then( response=> response ) 

    res.send({message: 'Correo enviado'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};


exports.getBecasExciIngles = async(req, res) => {
  try {

    let registros = await exci.getRegistrosExciBeca( ).then( response => response )
    // Solo los que ya tienen grupo
    const alumnosConGrupo = registros.filter( el=> { return el.id_grupo })
    // Sacamos los ids
    const ids = alumnosConGrupo.map((registro)=>{ return registro.id_alumno })
    // Consultadmos las becas
    let becas     = await exci.getBecas( ids ).then( response => response )
    let grupos    = await exci.gruposAlumnosExci( ids ).then( response => response )

    for( const i in registros ){
      const { id_grupo_encuesta, id_alumno } = registros[i]
      // Buscamos la beca
      const existeBeca  = becas.find( el=> el.id_grupo == id_grupo_encuesta && el.id_alumno == id_alumno )
      const existeGrupo = grupos.find( el=> el.id_grupo == id_grupo_encuesta && el.id_alumno == id_alumno )
      registros[i]['beca']     = existeBeca  ? existeBeca  : null 
      registros[i]['inscrito'] = existeGrupo ? existeGrupo : null 
    }


    res.send(registros);
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.sendLigaMedioPagoArriba = async(req, res) => {
  try {
    let registros = await exci.getRegistrosExci( ).then( response => response )

    for( const i in registros){
      const { pago_realizado, correo } = registros[i]

      if( pago_realizado > 0 && i > 701 && i <= 850 ){
        const correoHtml     = await recordatorio.crearRecordatorioPago( )
        const correoEnviado  = await reciboExci.enviarCorreo( 'Exci Clase 2', correoHtml, correo, transporter ).then( response=> response ) 
      }
    }
    // const correo = 'eduardo.zav.dev@gmail.com' 
    // const correoHtml     = await recordatorio.crearRecordatorioPago( )
    // const correoEnviado  = await reciboExci.enviarCorreo( 'Exci Clase 2', correoHtml, correo, transporter ).then( response=> response ) 

    res.send(registros);
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getExciGruposCiclo = async(req, res) => {
  try {

    let grupos = await exci.getExciGruposCiclo( ).then( response => response )

    res.send(grupos);
  } catch (error) {
    res.status(500).send({message:error})
  }
};



exports.agregarGrupoAlumno = async(req, res) => {
  try {
    const { idexci_registro, id_grupo } = req.body
    let grupos = await exci.agregarGrupoAlumno( idexci_registro, id_grupo ).then( response => response )

    res.send({message:'grupo agregado correctamente'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.solicitarBeca = async(req, res) => {
  try {
    const { id_alumno, id_grupo_encuesta } = req.body
    let becas = await exci.solicitarBeca( id_alumno, id_grupo_encuesta ).then( response => response )

    res.send({message:'Beca solicitada'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};



exports.enviarRemarketing = async(req, res) => {
  try {

    const { inicio, fin } = req.body 

    const correoHtml = remarketing.crearHtmlRemarketing( )

    const alumnosParaRemarketing = await exci.getExciConAdeudo( ).then( response => response )

    for( const i in alumnosParaRemarketing ){
      const { correo } = alumnosParaRemarketing[i]

      if( i >= inicio && i <= fin  ){
        const correoEnviado  = await reciboExci.enviarCorreo( 'Tienes una BECA para estudiar inglés por haber estado en CEAA', correoHtml, correo, transporter ).then( response=> response ) 
      }

    }

    res.send({message:'Terminé'});
  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};