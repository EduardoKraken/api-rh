const clases   = require("../../models/calidad/clases.model.js");
const kpi      = require("../../models/kpi/kpi.model.js");
const lms      = require("../../models/lms/catalogoslms.model.js");
const usuarios = require("../../models/operaciones/comentarios.model.js");
const fs       = require('fs');

// Obtener el listado de horarios
exports.getRolClases = async(req, res) => {
  try {
    // Cargamos el plantel 
    const { id_ciclo, id_ciclo_relacionado } = req.body

    // Consultamos los grupos dados de alta en el LMS
    let gruposRolFast = await clases.gruposRol( id_ciclo_relacionado, 2 ).then( response => response )
    let gruposRolInbi = await clases.gruposRol( id_ciclo            , 1 ).then( response => response )

    // Consultamos todos los teachers
    const teachers = await clases.getTeachersErp().then( response => response )

    let respuesta = gruposRolFast.concat( gruposRolInbi )


    // Obtener todos los grupos registrados en la base de datos
    // Sacar primoero los id de los grupos
    const idgrupos      = respuesta.map((registro) => { return registro.iderp })
    const rolRegistrado = await clases.obtenerRolRegistrado( idgrupos ).then( response => response )


    // Obtener los reemplazos realizados
    const rolReemplazo = await clases.obtenerRolReemplazo( idgrupos ).then( response => response )

    for( const i in respuesta ){
    	// Desestructuramos el objeto y obtenemos los teachers
    	const { id_teacher, id_teacher_2, iderp } = respuesta[i]

    	// buscar el teacher 1
    	const existeTeacher1 = teachers.find( el => el.id_usuario == id_teacher )

    	// Guardar su nombre
    	const nombreTeacher1 = existeTeacher1 ? existeTeacher1.nombre_completo : ''
    	const idTeacher1     = existeTeacher1 ? existeTeacher1.id_usuario : 0

    	// Buscar el teacher 2
    	const existeTeacher2 = teachers.find( el => el.id_usuario == id_teacher_2 )
    	// Guardar su nombre
    	const nombreTeacher2 = existeTeacher2 ? existeTeacher2.nombre_completo : ''
    	const idTeacher2     = existeTeacher2 ? existeTeacher2.id_usuario : 0

    	// Validar si existe estos datos registrados
    	const existeGrupo     = rolRegistrado.find( el => el.idgrupo == iderp )

    	// Validar si existe un reemplazo
    	const existeReemplazo = rolReemplazo.find( el => el.idgrupo == iderp )

    	respuesta[i]['idrol_clases'] = existeGrupo ? existeGrupo.idrol_clases : null

    	// Bloque 1
    	respuesta[i]['lu1_n']    = existeReemplazo ? existeReemplazo.lu1_n ? existeReemplazo.lu1_n : existeGrupo ? existeGrupo.lu1_n : nombreTeacher1 : existeGrupo ? existeGrupo.lu1_n : nombreTeacher1
    	respuesta[i]['ma1_n']    = existeReemplazo ? existeReemplazo.ma1_n ? existeReemplazo.ma1_n : existeGrupo ? existeGrupo.ma1_n : nombreTeacher1 : existeGrupo ? existeGrupo.ma1_n : nombreTeacher1
    	respuesta[i]['mi1_n']    = existeReemplazo ? existeReemplazo.mi1_n ? existeReemplazo.mi1_n : existeGrupo ? existeGrupo.mi1_n : nombreTeacher1 : existeGrupo ? existeGrupo.mi1_n : nombreTeacher1
    	respuesta[i]['ju1_n']    = existeReemplazo ? existeReemplazo.ju1_n ? existeReemplazo.ju1_n : existeGrupo ? existeGrupo.ju1_n : nombreTeacher2 : existeGrupo ? existeGrupo.ju1_n : nombreTeacher2
    	respuesta[i]['vi1_n']    = existeReemplazo ? existeReemplazo.vi1_n ? existeReemplazo.vi1_n : existeGrupo ? existeGrupo.vi1_n : nombreTeacher2 : existeGrupo ? existeGrupo.vi1_n : nombreTeacher2
    	respuesta[i]['lu1']      = existeReemplazo ? existeReemplazo.lu1   ? existeReemplazo.lu1   : existeGrupo ? existeGrupo.lu1   : idTeacher1     : existeGrupo ? existeGrupo.lu1   : idTeacher1
    	respuesta[i]['ma1']      = existeReemplazo ? existeReemplazo.ma1   ? existeReemplazo.ma1   : existeGrupo ? existeGrupo.ma1   : idTeacher1     : existeGrupo ? existeGrupo.ma1   : idTeacher1
    	respuesta[i]['mi1']      = existeReemplazo ? existeReemplazo.mi1   ? existeReemplazo.mi1   : existeGrupo ? existeGrupo.mi1   : idTeacher1     : existeGrupo ? existeGrupo.mi1   : idTeacher1
    	respuesta[i]['ju1']      = existeReemplazo ? existeReemplazo.ju1   ? existeReemplazo.ju1   : existeGrupo ? existeGrupo.ju1   : idTeacher2     : existeGrupo ? existeGrupo.ju1   : idTeacher2
    	respuesta[i]['vi1']      = existeReemplazo ? existeReemplazo.vi1   ? existeReemplazo.vi1   : existeGrupo ? existeGrupo.vi1   : idTeacher2     : existeGrupo ? existeGrupo.vi1   : idTeacher2

    	// Bloque 2
    	respuesta[i]['lu2_n']    = existeReemplazo ? existeReemplazo.lu2_n ? existeReemplazo.lu2_n : existeGrupo ? existeGrupo.lu2_n : nombreTeacher1 : existeGrupo ? existeGrupo.lu2_n : nombreTeacher1
    	respuesta[i]['ma2_n']    = existeReemplazo ? existeReemplazo.ma2_n ? existeReemplazo.ma2_n : existeGrupo ? existeGrupo.ma2_n : nombreTeacher1 : existeGrupo ? existeGrupo.ma2_n : nombreTeacher1
    	respuesta[i]['mi2_n']    = existeReemplazo ? existeReemplazo.mi2_n ? existeReemplazo.mi2_n : existeGrupo ? existeGrupo.mi2_n : nombreTeacher1 : existeGrupo ? existeGrupo.mi2_n : nombreTeacher1
    	respuesta[i]['ju2_n']    = existeReemplazo ? existeReemplazo.ju2_n ? existeReemplazo.ju2_n : existeGrupo ? existeGrupo.ju2_n : nombreTeacher2 : existeGrupo ? existeGrupo.ju2_n : nombreTeacher2
    	respuesta[i]['vi2_n']    = existeReemplazo ? existeReemplazo.vi2_n ? existeReemplazo.vi2_n : existeGrupo ? existeGrupo.vi2_n : nombreTeacher2 : existeGrupo ? existeGrupo.vi2_n : nombreTeacher2
    	respuesta[i]['lu2']      = existeReemplazo ? existeReemplazo.lu2   ? existeReemplazo.lu2   : existeGrupo ? existeGrupo.lu2   : idTeacher1     : existeGrupo ? existeGrupo.lu2   : idTeacher1
    	respuesta[i]['ma2']      = existeReemplazo ? existeReemplazo.ma2   ? existeReemplazo.ma2   : existeGrupo ? existeGrupo.ma2   : idTeacher1     : existeGrupo ? existeGrupo.ma2   : idTeacher1
    	respuesta[i]['mi2']      = existeReemplazo ? existeReemplazo.mi2   ? existeReemplazo.mi2   : existeGrupo ? existeGrupo.mi2   : idTeacher1     : existeGrupo ? existeGrupo.mi2   : idTeacher1
    	respuesta[i]['ju2']      = existeReemplazo ? existeReemplazo.ju2   ? existeReemplazo.ju2   : existeGrupo ? existeGrupo.ju2   : idTeacher2     : existeGrupo ? existeGrupo.ju2   : idTeacher2
    	respuesta[i]['vi2']      = existeReemplazo ? existeReemplazo.vi2   ? existeReemplazo.vi2   : existeGrupo ? existeGrupo.vi2   : idTeacher2     : existeGrupo ? existeGrupo.vi2   : idTeacher2

    	// Bloque 3
    	respuesta[i]['lu3_n']    = existeReemplazo ? existeReemplazo.lu3_n ? existeReemplazo.lu3_n : existeGrupo ? existeGrupo.lu3_n : nombreTeacher1 : existeGrupo ? existeGrupo.lu3_n : nombreTeacher1
    	respuesta[i]['ma3_n']    = existeReemplazo ? existeReemplazo.ma3_n ? existeReemplazo.ma3_n : existeGrupo ? existeGrupo.ma3_n : nombreTeacher1 : existeGrupo ? existeGrupo.ma3_n : nombreTeacher1
    	respuesta[i]['mi3_n']    = existeReemplazo ? existeReemplazo.mi3_n ? existeReemplazo.mi3_n : existeGrupo ? existeGrupo.mi3_n : nombreTeacher1 : existeGrupo ? existeGrupo.mi3_n : nombreTeacher1
    	respuesta[i]['ju3_n']    = existeReemplazo ? existeReemplazo.ju3_n ? existeReemplazo.ju3_n : existeGrupo ? existeGrupo.ju3_n : nombreTeacher2 : existeGrupo ? existeGrupo.ju3_n : nombreTeacher2
    	respuesta[i]['vi3_n']    = existeReemplazo ? existeReemplazo.vi3_n ? existeReemplazo.vi3_n : existeGrupo ? existeGrupo.vi3_n : nombreTeacher2 : existeGrupo ? existeGrupo.vi3_n : nombreTeacher2
    	respuesta[i]['lu3']      = existeReemplazo ? existeReemplazo.lu3   ? existeReemplazo.lu3   : existeGrupo ? existeGrupo.lu3   : idTeacher1     : existeGrupo ? existeGrupo.lu3   : idTeacher1
    	respuesta[i]['ma3']      = existeReemplazo ? existeReemplazo.ma3   ? existeReemplazo.ma3   : existeGrupo ? existeGrupo.ma3   : idTeacher1     : existeGrupo ? existeGrupo.ma3   : idTeacher1
    	respuesta[i]['mi3']      = existeReemplazo ? existeReemplazo.mi3   ? existeReemplazo.mi3   : existeGrupo ? existeGrupo.mi3   : idTeacher1     : existeGrupo ? existeGrupo.mi3   : idTeacher1
    	respuesta[i]['ju3']      = existeReemplazo ? existeReemplazo.ju3   ? existeReemplazo.ju3   : existeGrupo ? existeGrupo.ju3   : idTeacher2     : existeGrupo ? existeGrupo.ju3   : idTeacher2
    	respuesta[i]['vi3']      = existeReemplazo ? existeReemplazo.vi3   ? existeReemplazo.vi3   : existeGrupo ? existeGrupo.vi3   : idTeacher2     : existeGrupo ? existeGrupo.vi3   : idTeacher2

    	// Bloque 4
    	respuesta[i]['lu4_n']    = existeReemplazo ? existeReemplazo.lu4_n ? existeReemplazo.lu4_n : existeGrupo ? existeGrupo.lu4_n : nombreTeacher1 : existeGrupo ? existeGrupo.lu4_n : nombreTeacher1
    	respuesta[i]['ma4_n']    = existeReemplazo ? existeReemplazo.ma4_n ? existeReemplazo.ma4_n : existeGrupo ? existeGrupo.ma4_n : nombreTeacher1 : existeGrupo ? existeGrupo.ma4_n : nombreTeacher1
    	respuesta[i]['mi4_n']    = existeReemplazo ? existeReemplazo.mi4_n ? existeReemplazo.mi4_n : existeGrupo ? existeGrupo.mi4_n : nombreTeacher1 : existeGrupo ? existeGrupo.mi4_n : nombreTeacher1
    	respuesta[i]['ju4_n']    = existeReemplazo ? existeReemplazo.ju4_n ? existeReemplazo.ju4_n : existeGrupo ? existeGrupo.ju4_n : nombreTeacher2 : existeGrupo ? existeGrupo.ju4_n : nombreTeacher2
    	respuesta[i]['vi4_n']    = existeReemplazo ? existeReemplazo.vi4_n ? existeReemplazo.vi4_n : existeGrupo ? existeGrupo.vi4_n : nombreTeacher2 : existeGrupo ? existeGrupo.vi4_n : nombreTeacher2
    	respuesta[i]['lu4']      = existeReemplazo ? existeReemplazo.lu4   ? existeReemplazo.lu4   : existeGrupo ? existeGrupo.lu4   : idTeacher1     : existeGrupo ? existeGrupo.lu4   : idTeacher1
    	respuesta[i]['ma4']      = existeReemplazo ? existeReemplazo.ma4   ? existeReemplazo.ma4   : existeGrupo ? existeGrupo.ma4   : idTeacher1     : existeGrupo ? existeGrupo.ma4   : idTeacher1
    	respuesta[i]['mi4']      = existeReemplazo ? existeReemplazo.mi4   ? existeReemplazo.mi4   : existeGrupo ? existeGrupo.mi4   : idTeacher1     : existeGrupo ? existeGrupo.mi4   : idTeacher1
    	respuesta[i]['ju4']      = existeReemplazo ? existeReemplazo.ju4   ? existeReemplazo.ju4   : existeGrupo ? existeGrupo.ju4   : idTeacher2     : existeGrupo ? existeGrupo.ju4   : idTeacher2
    	respuesta[i]['vi4']      = existeReemplazo ? existeReemplazo.vi4   ? existeReemplazo.vi4   : existeGrupo ? existeGrupo.vi4   : idTeacher2     : existeGrupo ? existeGrupo.vi4   : idTeacher2

    }

		res.send(respuesta);

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};


// Obtener el listado de horarios
exports.agregarRolClases = async(req, res) => {
  try {
  	for( const i in req.body ){
  		const { iderp } = req.body[i]
  		// Primero hay que verificar que NO exista ya un registro de ese grupo
    	const existeRolClase = await clases.existeRolClase( iderp ).then(response=> response)

    	if( !existeRolClase ){
    		const insertarRol = await clases.insertarRol( req.body[i] ).then(response=> response)
    	}
  	}

    res.send({message: 'Datos registrados correctamente'});

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};


exports.agregarRolReemplazo = async(req, res) => {
  try {
  	
  	const { bloque, id_teacher, idgrupo } = req.body

  	// Validar si existe un registro de ese grupo
    const existeRolClase = await clases.obtenerRolReemplazoGrupo( idgrupo ).then(response=> response)

    if( existeRolClase ){
    	const actualizarReemplazo = await clases.updateRolReemplazo( idgrupo, bloque, id_teacher ).then(response=> response)
    }else{
    	const agregarReemplazo    = await clases.insertarRolReemplazo( req.body ).then(response=> response)
    }

    res.send({message: 'Datos registrados correctamente'});

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};