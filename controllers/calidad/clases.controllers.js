const clases   = require("../../models/calidad/clases.model.js");
const kpi      = require("../../models/kpi/kpi.model.js");
const lms      = require("../../models/lms/catalogoslms.model.js");
const usuarios = require("../../models/operaciones/comentarios.model.js");
const fs       = require('fs');

// Obtener el listado de horarios
exports.getGruposPlantel = async(req, res) => {
  try {
    // Cargamos el plantel 
    const { id_plantel, cicloFast, cicloInbi, planteles, fecha } = req.body


    // Regresamos los planteles del usuario
    let plantelesUsuarios = []
    for(const i in planteles){
      plantelesUsuarios.push(Object.values(planteles[i])[2])
    }

		// Obtenemos el día que es
    const { diaEspecifico } = await kpi.getNumberDiaEspecificoBD2( fecha ).then(response => response)

  	// Obtenemos los grupos que dan clase ese día y en ese ciclo y en ese plantel
		let getGruposPlantelFAST = await clases.getGruposPlantelFast( id_plantel, cicloFast, diaEspecifico, plantelesUsuarios, fecha ).then(response=> response)
		// Obtenemos los grupos que dan clase ese día y en ese ciclo y en ese plantel
		let getGruposPlantelINBI = await clases.getGruposPlantelInbi( id_plantel, cicloInbi, diaEspecifico, plantelesUsuarios, fecha ).then(response=> response)

    const grupos = getGruposPlantelFAST.concat(getGruposPlantelINBI)

    // Cargamos las evaluaciones para si ya tiene evaluada una clase
    const getClasesEvaluadasHoyFAST = await clases.getClasesEvaluadasHoy( ).then(response=> response)
    // Cargamos las evaluaciones para si ya tiene evaluada una clase
    const getClasesEvaluadasHoyINBI = await clases.getClasesEvaluadasHoy( ).then(response=> response)

    const clasesEvaluadas = getClasesEvaluadasHoyFAST.concat(getClasesEvaluadasHoyINBI)

    // recorremos las clases
    for(const i in grupos){
      // Buscamos su evalaucion
      const existeEvaluacion = clasesEvaluadas.find( el => el.id_grupo == grupos[i].id_grupo && el.unidad_negocio == grupos[i].id_unidad_negocio)
      if(existeEvaluacion){
        grupos[i] = {...grupos[i], evaluada: true}
      }
    }

		res.send(grupos);

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};

exports.addEvaluacionClase = async(req, res) => {
  try {
    // Separamos las respuestas de lo que estamos recibiendo 
    const { preguntas, id_grupo, unidad_negocio } = req.body
    // Validar si esl grupo ya esta evaluado
    const existeEvaluacion  = await clases.getEvaluacion( id_grupo, unidad_negocio ).then(response=> response)

    if(existeEvaluacion.length > 0){
      res.send({estatusError: 0});
    }else{
      // Agregamos la evaluación 
      const evaluacionAgregada = await clases.addEvaluacionClase( req.body ).then(response=> response)
      
      // Guardamos el id de la evaluacion generada
      const id  = evaluacionAgregada.id

      // Necesitamos recorrer las preguntas para ver las respuestas y agregarlas 
      for(const i in preguntas){
        preguntas[i].check = preguntas[i].check == true ? 1 : 0
        const addRespuestas = await clases.addRespuestas( preguntas[i], id ).then(response=> response)
      }
      res.send(evaluacionAgregada);
    }
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};

exports.addEvaluacionClaseImagen = async( req, res ) => {
  try {
    // Obtenemos el archivo
    let EDFile = req.files.file

    let nombreArchivo = req.params.id + '.' + req.params.ext
    // Enivamos a guardar el archivo
    const addImagen = await addImagenClaseEvaluacion( EDFile, nombreArchivo ).then(response=> response)
    // Actualizamos la ruta de la imagen en la evaluación 
    const updateNombreArchivoEvalclase = await clases.updateClaseEvalArchivo( req.params.id, nombreArchivo ).then(response=> response)

    res.send({message:'Actualización correcta'});
  } catch ( error ){
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
  
};


addImagenClaseEvaluacion = ( archivo, nombre ) => {
  return new Promise((resolve,reject)=>{
    let EDFile = archivo //EDFile.name
    EDFile.mv(`./../../imagenes-clases/${ nombre }`, err => {
      if (err) {
        reject( err )
      }else{
        resolve ( true )
      }
    })
  })
};


exports.getClasesEvaluadas = async(req, res) => {
  try {
    const getClasesEvaluadas = await clases.getClasesEvaluadas( ).then(response=> response)
    res.send(getClasesEvaluadas);
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};


exports.getClasesEvaluadasUsuario = async(req, res) => {
  try {
    const { id_usuario_erp, cicloFast, cicloInbi } = req.body

    // Consultamos los grupos de ese usuario
    const getClasesEvaluadasUsuario = await clases.getClasesEvaluadasUsuario( id_usuario_erp, cicloFast, cicloInbi ).then(response=> response)
    
    // Consultamos todas las respuestas
    const getCalificacionesClaseEvaluada = await clases.getCalificacionesClaseEvaluada( ).then(response=> response)

    // cONSULTAMOS LOS GRUPOS
    const getGruposLMSFAST = await lms.getGruposLMSFAST( cicloFast, 2 ).then(response=> response)
    const getGruposLMSINBI = await lms.getGruposLMSFAST( cicloInbi, 1 ).then(response=> response)

    // CONSULTAMOS LOS MAESTROS
    const getTeachersLMSFAST = await lms.getTeachersLMSFAST( 2 ).then(response=> response)
    const getTeachersLMSINBI = await lms.getTeachersLMSFAST( 1 ).then(response=> response)



    for(const i in getClasesEvaluadasUsuario){
      const { id_grupo, unidad_negocio, id_teacher, idcalidad_eval_clase } = getClasesEvaluadasUsuario[i]
      let grupo   = null
      let teacher = null
      let calificacion = 0

      // Validamos la escuela
      if(unidad_negocio == 1){
        // INBI Y BUSCAMOS EL GRUPO EN INBI
        grupo = getGruposLMSINBI.find(el => el.id == id_grupo)
      }else{
        // FAST Y BUSCAMOS EL GRUPO EN FAST
        grupo = getGruposLMSFAST.find(el => el.id == id_grupo)
      }

      // Validamos la escuela
      if(unidad_negocio == 1){
        // INBI Y BUSCAMOS EL TEACHER EN INBI
        teacher = getTeachersLMSINBI.find(el => el.id == id_teacher)
      }else{
        // FAST Y BUSCAMOS EL TEACHER EN FAST
        teacher = getTeachersLMSFAST.find(el => el.id == id_teacher)
      }

      calificacion = getCalificacionesClaseEvaluada.find(el => el.idcalidad_eval_clase == idcalidad_eval_clase)


      // SI ENCONTRO EL GRUPO...
      if(grupo){
        getClasesEvaluadasUsuario[i] = {
          ...getClasesEvaluadasUsuario[i], 
          grupo:   grupo.nombre,
        }
      }

      // SI ENCONTRO EL TEACHER...
      if(teacher){
        getClasesEvaluadasUsuario[i] = {
          ...getClasesEvaluadasUsuario[i], 
          teacher: teacher.nombre_teacher,
        }
      }

      if(calificacion){
        getClasesEvaluadasUsuario[i] = {
          ...getClasesEvaluadasUsuario[i], 
          calificacion: calificacion.calificacion,
        }
      }

    }

    res.send(getClasesEvaluadasUsuario);
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};

exports.getClasesRespuestas = async(req, res) => {
  try {
    const { id } = req.params

    const getRespuestasEvaluacion = await clases.getRespuestasEvaluacion( id ).then(response=> response)
    res.send(getRespuestasEvaluacion);
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};


exports.getClasesEvaluadasAll = async(req, res) => {
  try {
    const { id_usuario_erp, cicloFast, cicloInbi } = req.body

    // Consultamos los grupos de ese usuario
    const getClasesEvaluadasAll = await clases.getClasesEvaluadasAll( cicloFast, cicloInbi ).then(response=> response)
    
    // Consultamos todas las respuestas
    const getCalificacionesClaseEvaluada = await clases.getCalificacionesClaseEvaluada( ).then(response=> response)

    // cONSULTAMOS LOS GRUPOS
    const getGruposLMSFAST = await lms.getGruposLMSFAST( cicloFast, 2 ).then(response=> response)
    const getGruposLMSINBI = await lms.getGruposLMSFAST( cicloInbi, 1 ).then(response=> response)

    // CONSULTAMOS LOS MAESTROS
    const getTeachersLMSFAST = await lms.getTeachersLMSFAST( 2 ).then(response=> response)
    const getTeachersLMSINBI = await lms.getTeachersLMSFAST( 1 ).then(response=> response)



    for(const i in getClasesEvaluadasAll){
      const { id_grupo, unidad_negocio, id_teacher, idcalidad_eval_clase } = getClasesEvaluadasAll[i]
      let grupo   = null
      let teacher = null
      let calificacion = 0

      // Validamos la escuela
      if(unidad_negocio == 1){
        // INBI Y BUSCAMOS EL GRUPO EN INBI
        grupo = getGruposLMSINBI.find(el => el.id == id_grupo)
      }else{
        // FAST Y BUSCAMOS EL GRUPO EN FAST
        grupo = getGruposLMSFAST.find(el => el.id == id_grupo)
      }

      // Validamos la escuela
      if(unidad_negocio == 1){
        // INBI Y BUSCAMOS EL TEACHER EN INBI
        teacher = getTeachersLMSINBI.find(el => el.id == id_teacher)
      }else{
        // FAST Y BUSCAMOS EL TEACHER EN FAST
        teacher = getTeachersLMSFAST.find(el => el.id == id_teacher)
      }

      calificacion = getCalificacionesClaseEvaluada.find(el => el.idcalidad_eval_clase == idcalidad_eval_clase)


      // SI ENCONTRO EL GRUPO...
      if(grupo){
        getClasesEvaluadasAll[i] = {
          ...getClasesEvaluadasAll[i], 
          grupo:   grupo.nombre,
        }
      }

      // SI ENCONTRO EL TEACHER...
      if(teacher){
        getClasesEvaluadasAll[i] = {
          ...getClasesEvaluadasAll[i], 
          teacher: teacher.nombre_teacher,
        }
      }

      if(calificacion){
        getClasesEvaluadasAll[i] = {
          ...getClasesEvaluadasAll[i], 
          calificacion: calificacion.calificacion,
        }
      }
    }

    const usuario  = await usuarios.getUsuariosActivos().then(response=>response)

    for(const i in getClasesEvaluadasAll){
      const { id_usuario_erp } = getClasesEvaluadasAll[i]
      const existe = usuario.find(el=> el.id_usuario == id_usuario_erp)

      if( existe ){
        getClasesEvaluadasAll[i] = {
          ...getClasesEvaluadasAll[i],
          usuario: existe.nombre_completo
        }
      }

    }

    res.send(getClasesEvaluadasAll);
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};


exports.getClasesEvaluadasTeacher = async(req, res) => {
  try {
    const { id_usuario_erp, cicloFast, cicloInbi, email } = req.body

    // obtener los id del usuarios de fast y de inbi
    const { id_fast } = await clases.getIdFastTeacher( email ).then(response=> response)
    const { id_inbi } = await clases.getIdInbiTeacher( email ).then(response=> response)

    // consultamos las evaluaciones del techer
    const getClasesEvaluadasTeacher = await clases.getClasesEvaluadasTeacher( id_fast, id_inbi, cicloFast, cicloInbi ).then(response=> response)
    
    // Consultamos todas las respuestas
    const getCalificacionesClaseEvaluada = await clases.getCalificacionesClaseEvaluada( ).then(response=> response)

    // cONSULTAMOS LOS GRUPOS
    const getGruposLMSFAST = await lms.getGruposLMSFAST( cicloFast, 2 ).then(response=> response)
    const getGruposLMSINBI = await lms.getGruposLMSFAST( cicloInbi, 1 ).then(response=> response)

    // CONSULTAMOS LOS MAESTROS
    const getTeachersLMSFAST = await lms.getTeachersLMSFAST( 2 ).then(response=> response)
    const getTeachersLMSINBI = await lms.getTeachersLMSFAST( 1 ).then(response=> response)


    for(const i in getClasesEvaluadasTeacher){
      const { id_grupo, unidad_negocio, id_teacher, idcalidad_eval_clase } = getClasesEvaluadasTeacher[i]
      let grupo   = null
      let teacher = null
      let calificacion = 0

      // Validamos la escuela
      if(unidad_negocio == 1){
        // INBI Y BUSCAMOS EL GRUPO EN INBI
        grupo = getGruposLMSINBI.find(el => el.id == id_grupo)
      }else{
        // FAST Y BUSCAMOS EL GRUPO EN FAST
        grupo = getGruposLMSFAST.find(el => el.id == id_grupo)
      }

      // Validamos la escuela
      if(unidad_negocio == 1){
        // INBI Y BUSCAMOS EL TEACHER EN INBI
        teacher = getTeachersLMSINBI.find(el => el.id == id_teacher)
      }else{
        // FAST Y BUSCAMOS EL TEACHER EN FAST
        teacher = getTeachersLMSFAST.find(el => el.id == id_teacher)
      }

      calificacion = getCalificacionesClaseEvaluada.find(el => el.idcalidad_eval_clase == idcalidad_eval_clase)


      // SI ENCONTRO EL GRUPO...
      if(grupo){
        getClasesEvaluadasTeacher[i] = {
          ...getClasesEvaluadasTeacher[i], 
          grupo:   grupo.nombre,
        }
      }

      // SI ENCONTRO EL TEACHER...
      if(teacher){
        getClasesEvaluadasTeacher[i] = {
          ...getClasesEvaluadasTeacher[i], 
          teacher: teacher.nombre_teacher,
        }
      }

      if(calificacion){
        getClasesEvaluadasTeacher[i] = {
          ...getClasesEvaluadasTeacher[i], 
          calificacion: calificacion.calificacion,
        }
      }

    }

    res.send(getClasesEvaluadasTeacher);
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};

exports.evaluacionClaseUpdate = async(req, res) => {
  try {
    // Separamos las respuestas de lo que estamos recibiendo 
    const { preguntas, idcalidad_eval_clase, estatus } = req.body
    // Agregamos la evaluación 
    const evaluacionActualizada = await clases.evaluacionClaseUpdate( idcalidad_eval_clase, estatus ).then(response=> response)

    // Necesitamos recorrer las preguntas para ver las respuestas y agregarlas 
    for(const i in preguntas){
      const updateRespuesta = await clases.updateRespuesta( preguntas[i] ).then(response=> response)
    }
    res.send(evaluacionActualizada);

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};