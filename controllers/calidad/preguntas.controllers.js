const preguntas = require("../../models/calidad/preguntas.model.js");

// Obtener el listado de horarios
exports.getPreguntasAll = async(req, res) => {
  try {
    // Cargamos todos las preguntas no borradas
		const getPreguntas = await preguntas.getPreguntasAll(  ).then(response=> response)
		res.send(getPreguntas);
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addPreguntas = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  preguntas.addPreguntas(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la pregunta"
      })
    else res.status(200).send({ message: `La pregunta se ha creado correctamente` });
  })
};


exports.updatePreguntas = (req, res) => {

  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }

  preguntas.updatePreguntas(req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro el puesto con id : ${req.body.idcalidad_preguntas}` });
      } else {
        res.status(500).send({ message: "Error al actualizar el puesto con el id : " + req.body.idcalidad_preguntas });
      }
    } else {
      res.status(200).send({ message: `El puesto se ha actualizado correctamente.` })
    }
  })
}