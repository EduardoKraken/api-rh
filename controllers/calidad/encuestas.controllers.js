const encuestas = require("../../models/calidad/encuestas.model.js");

// Obtener el listado de horarios
exports.getEncuestasSatisfaccion = async(req, res) => {
	try {

		const { escuela } = req.body

		const id_ciclo = escuela == 1 ? req.body.id_ciclo : req.body.id_ciclo_relacionado

    // Cargamos todos las preguntas no borradas
		const cantAlumnosEncuestados = await encuestas.cantidadAlumnosEncuestados( id_ciclo, escuela ).then(response=> response)
		// VALIDAR QUE HAYA ALUMNOS ENCUESTADOS
		if( !cantAlumnosEncuestados.length){ return res.status( 400 ).send({ message: 'No hay encuestas realizadas' })}

		const cantAlumnosxEncuestar  = await encuestas.cantidadAlumnosPorEncuestar( id_ciclo, escuela ).then(response=> response)

		const alumnosEncuestados = cantAlumnosEncuestados.length
		const totalAlumnos       = cantAlumnosxEncuestar.cantAlumnos

		const porcentaje = (( alumnosEncuestados / totalAlumnos ) * 100 ).toFixed(2)

		/*
			OBTENER LAS PREGUTNAS DE LAS ENCUESTAS
		*/
		const getPreguntasSatisfaccion = await encuestas.getPreguntasSatisfaccion( escuela ).then( response => response )

    const getOpcionesSatisfaccion  = await encuestas.getOpcionesSatisfaccion( escuela ).then( response => response )

    const getRespuestasEncuesta    = await encuestas.getRespuestasEncuesta( id_ciclo, escuela ).then( response => response )

    let preguntasSeccion1  = getPreguntasSatisfaccion.filter( el => { return [1,3].includes( el.seccion ) && [1,3].includes( el.tipo_pregunta ) })

    for( const i in preguntasSeccion1 ){

	    let series = []
	    let campos = []

    	let chartOptions = {
	      chart: {
	        type: 'donut',
	      },
	      labels:[],
	      responsive: [{
	        breakpoint: 480,
	        options: {
	          chart: {
	            width: 200
	          },
	          legend: {
	            position: 'bottom'
	          }
	        }
	      }]
	    }

	    let chartOptionsBarras = {
        chart: {
          type: 'bar',
          height: 350
        },
        plotOptions: {
          bar: {
            borderRadius: 4,
            horizontal: true,
          }
        },
        dataLabels: {
          enabled: false
        },
        xaxis: {
          categories: [],
        }
      }

      // desestructuramos el arreglo y sacamos le id
      const { idpre_enc_satis, tipo_pregunta, pregunta } = preguntasSeccion1[i]

      series = []
      campos = []
      
      // Buscamos las preguntas siempre y cuando no sea de tipo de que son preguntas abiertas
      const opciones = getOpcionesSatisfaccion.filter( el => { return el.idpre_enc_satis == idpre_enc_satis })
      
      if( tipo_pregunta != 2 ){
        // Buscamos las opciones
        for( const j in opciones ){
        	const { idopc_enc_satis, opcion } = opciones[j]
        	const cantidadRespuestas  = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == idpre_enc_satis && el.idopc_enc_satis == idopc_enc_satis }).length
          opciones[j]['respuestas'] = cantidadRespuestas
          const porcentajeEncuesta = (( cantidadRespuestas / cantAlumnosEncuestados.length ) * 100 ).toFixed(2)
          opciones[j]['porcentaje'] = porcentajeEncuesta
          series.push(cantidadRespuestas)
          campos.push(`${ opciones[j].opcion} ${ porcentajeEncuesta }%`)
        }

      }

      preguntasSeccion1[i]['opciones']       = opciones
      preguntasSeccion1[i]['series']         = tipo_pregunta == 1 ? series : [{ data: series }]
      chartOptions.labels                    = campos
      chartOptionsBarras.xaxis.categories    = campos
      preguntasSeccion1[i]['chartOptions']   = tipo_pregunta == 1 ? chartOptions : chartOptionsBarras
      preguntasSeccion1[i]['cols']           = tipo_pregunta == 1 ? '6' : '12' 
    }
    
    let preguntasSeccion2  = []
    preguntasSeccion2  = getPreguntasSatisfaccion.filter( el => { return [2].includes( el.seccion ) && [1].includes( el.tipo_pregunta )})
    for( const i in preguntasSeccion2 ){

	    let series = []
	    let campos = []

    	let chartOptions = {
	      chart: {
	        type: 'donut',
	      },
	      labels:[],
	      responsive: [{
	        breakpoint: 480,
	        options: {
	          chart: {
	            width: 200
	          },
	          legend: {
	            position: 'bottom'
	          }
	        }
	      }]
	    }

      // desestructuramos el arreglo y sacamos le id
      const { idpre_enc_satis, tipo_pregunta, pregunta } = preguntasSeccion2[i]

      series = []
      campos = []
      
      // Buscamos las preguntas siempre y cuando no sea de tipo de que son preguntas abiertas
      const opciones = getOpcionesSatisfaccion.filter( el => { return el.idpre_enc_satis == idpre_enc_satis })
      
      if( tipo_pregunta != 2 ){
        // Buscamos las opciones
        for( const j in opciones ){
        	const { idopc_enc_satis, opcion } = opciones[j]
        	const cantidadRespuestas  = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == idpre_enc_satis && el.idopc_enc_satis == idopc_enc_satis && el.num_teacher == 1 }).length
          opciones[j]['respuestas'] = cantidadRespuestas
          const porcentajeEncuesta = (( cantidadRespuestas / cantAlumnosEncuestados.length ) * 100 ).toFixed(2)
          opciones[j]['porcentaje'] = porcentajeEncuesta
          series.push(cantidadRespuestas)
          campos.push(`${ opciones[j].opcion} ${ porcentajeEncuesta }%`)
        }

      }

      preguntasSeccion2[i]['opciones']       = opciones
      preguntasSeccion2[i]['series']         = series
      chartOptions.labels                    = campos
      preguntasSeccion2[i]['chartOptions']   = chartOptions
      preguntasSeccion2[i]['cols']           = tipo_pregunta == 1 ? '6' : '6' 
    }

    const getPreguntasSatisfaccion2 = await encuestas.getPreguntasSatisfaccion( escuela ).then( response => response )
    const getOpcionesSatisfaccion2  = await encuestas.getOpcionesSatisfaccion( escuela ).then( response => response )
    let preguntasSeccion3 = getPreguntasSatisfaccion2.filter( el => { return [2].includes( el.seccion ) && [1].includes( el.tipo_pregunta )})

    for( const i in preguntasSeccion3 ){

	    let series = []
	    let campos = []

    	let chartOptions = {
	      chart: {
	        type: 'donut',
	      },
	      labels:[],
	      responsive: [{
	        breakpoint: 480,
	        options: {
	          chart: {
	            width: 200
	          },
	          legend: {
	            position: 'bottom'
	          }
	        }
	      }]
	    }

      // desestructuramos el arreglo y sacamos le id
      const { idpre_enc_satis, tipo_pregunta, pregunta } = preguntasSeccion3[i]

      series = []
      campos = []
      
      // Buscamos las preguntas siempre y cuando no sea de tipo de que son preguntas abiertas
      const opciones = getOpcionesSatisfaccion2.filter( el => { return el.idpre_enc_satis == idpre_enc_satis })
      
      if( tipo_pregunta != 2 ){
        // Buscamos las opciones
        for( const j in opciones ){
        	const { idopc_enc_satis, opcion } = opciones[j]
        	const cantidadRespuestas  = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == idpre_enc_satis && el.idopc_enc_satis == idopc_enc_satis && el.num_teacher == 2 }).length
          opciones[j]['respuestas'] = cantidadRespuestas
          const porcentajeEncuesta = (( cantidadRespuestas / cantAlumnosEncuestados.length ) * 100 ).toFixed(2)
          opciones[j]['porcentaje'] = porcentajeEncuesta
          series.push(cantidadRespuestas)
          campos.push(`${ opciones[j].opcion} ${ porcentajeEncuesta }%`)
        }

      }

      preguntasSeccion3[i]['opciones']       = opciones
      preguntasSeccion3[i]['series']         = series
      chartOptions.labels                    = campos
      preguntasSeccion3[i]['chartOptions']   = chartOptions
      preguntasSeccion3[i]['cols']           = tipo_pregunta == 1 ? '6' : '6' 
    }

    /*
			ENCUESTAS INDIVIDUALES POR NIVEL SECCIÓN 1
    */

    const encuestasNivel = await getEncuestasNivel( escuela, id_ciclo ).then( response => response )

    const tablaTeacherA = await getEncuestaTeacher( escuela, id_ciclo, 1 ).then( response => response )
    const tablaTeacherB = await getEncuestaTeacher( escuela, id_ciclo, 2 ).then( response => response )

    const preguntasAbiertas        = await encuestas.obtenerPreguntasAbiertas( id_ciclo, escuela ).then( response => response )
    const preguntasAbiertasTacherA = preguntasAbiertas.filter( el => { return el.seccion == 2 && el.num_teacher == 1 })
    const preguntasAbiertasTacherB = preguntasAbiertas.filter( el => { return el.seccion == 2 && el.num_teacher == 2 })
    const preguntasAbiertasGeneral = preguntasAbiertas.filter( el => { return [1,3].includes(el.seccion) })

		res.send({
			tablaTeacherA,
			tablaTeacherB,
			encuestasNivel,
			alumnosEncuestados,
			totalAlumnos,
			porcentaje,
			preguntasSeccion1,
			preguntasSeccion2,
			preguntasSeccion3,
			preguntasAbiertasTacherA,
			preguntasAbiertasTacherB,
			preguntasAbiertasGeneral
		});

	} catch (error) {
		res.status(500).send({message:error ? error.message : 'Error en el servidor'})
	}
};


getEncuestasNivel = async ( escuela, id_ciclo ) => {
	return new Promise(async(resolve, reject) => {
		try{

			// Cargamos todos las preguntas no borradas
			const cantAlumnosEncuestados = await encuestas.cantidadAlumnosEncuestadosNivel( id_ciclo, escuela ).then(response=> response)

			// VALIDAR QUE HAYA ALUMNOS ENCUESTADOS
			if( !cantAlumnosEncuestados.length){ return res.status( 400 ).send({ message: 'No hay encuestas realizadas' })}

			const cantAlumnosxEncuestar  = await encuestas.cantidadAlumnosPorEncuestarNivel( id_ciclo, escuela ).then(response=> response)
			
			/*
				OBTENER LAS PREGUNTAS DE LAS ENCUESTAS
			*/


			const getPreguntasSatisfaccion = await encuestas.getPreguntasSatisfaccion( escuela ).then( response => response )

	    const getOpcionesSatisfaccion  = await encuestas.getOpcionesSatisfaccion( escuela ).then( response => response )

	    const getRespuestasEncuesta    = await encuestas.getRespuestasEncuesta( id_ciclo, escuela ).then( response => response )

	    let preguntasSeccion1  = getPreguntasSatisfaccion.filter( el => { return [1].includes( el.seccion ) && [1].includes( el.tipo_pregunta ) })

	    let headers = [
	    	{ text: 'Nivel'     , value: 'nivel' },
        { text: 'Alumno'    , value: 'alumnos' },
        { text: 'Encuestas' , value: 'respuestas' },
        { text: '%'         , value: 'porcentaje' },
        { text: 'Mucho'     , value: 'MUCHO' },
        { text: 'Poco'      , value: 'POCO' },
        { text: 'Nada'      , value: 'NADA' },
	    ]

	    for( const i in preguntasSeccion1 ){
				let nivelesOp = [{nivel: 1},{nivel: 2},{nivel: 3},{nivel: 4},{nivel: 5},{nivel: 6},{nivel: 7},{nivel: 8},{nivel: 9},{nivel: 10},{nivel: 11},{nivel: 12},{nivel: 13},{nivel: 14}]

				preguntasSeccion1[i]['niveles']        = nivelesOp
	      preguntasSeccion1[i]['cols']           = '6'

	      // desestructuramos el arreglo y sacamos le id
	      const { idpre_enc_satis, tipo_pregunta, pregunta, niveles } = preguntasSeccion1[i]

	      preguntasSeccion1[i]['headers'] = headers

	      // Buscamos las preguntas siempre y cuando no sea de tipo de que son preguntas abiertas
	      const opciones = getOpcionesSatisfaccion.filter( el => { return el.idpre_enc_satis == idpre_enc_satis })
	      
	      if( tipo_pregunta != 2 ){
	        // Buscamos las opciones
        	for( const k in niveles){
        		const { nivel } = niveles[k]

		        for( const j in opciones ){
		        	const { idopc_enc_satis, opcion } = opciones[j]

		        	// 1-. VER CUANTAS PERSONAS DEBIERON HACER LA ENCUESTA
		          let alumnosxEncuestar = cantAlumnosxEncuestar.find( el => el.id_nivel == nivel )
		          alumnosxEncuestar = alumnosxEncuestar ? alumnosxEncuestar.cantAlumnos : 0
		          niveles[k][`alumnos`] = alumnosxEncuestar

		          // VER EL TOTAL DE PERSONA QUE CONTESTARON LA ENCUESTA
		        	const cantidadRespuestas  = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == idpre_enc_satis && el.id_nivel == nivel }).length
		          niveles[k][`respuestas`] = cantidadRespuestas


		          // PORCENTAJE GENERAL
		          const porcentajeEncuesta = (( cantidadRespuestas / alumnosxEncuestar ) * 100 ).toFixed(2)
		          niveles[k][`porcentaje`] = parseFloat( porcentajeEncuesta ) ? porcentajeEncuesta : 0


		          // VER EL TOTAL DE PERSONA QUE CONTESTARON LA ENCUESTA POR OPCION
		          let alumnosEncuestadosOpcion = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == idpre_enc_satis && el.idopc_enc_satis == idopc_enc_satis && el.id_nivel == nivel }).length

		          // PORCENTAJE GENERAL
		          const porcentajeEncuestaOpcion = (( alumnosEncuestadosOpcion / cantidadRespuestas ) * 100 ).toFixed(2)
		          
		          // AGREGAR EL TITULO DE OPCIÓN
		        	niveles[k][`${opcion}`] = parseFloat( porcentajeEncuestaOpcion ) ? porcentajeEncuestaOpcion : 0
	        	}
	        }
	      }

	    }

	    resolve( preguntasSeccion1 )
		}catch( error ){
			reject( error )
		}
  })
}

getEncuestaTeacher = async ( escuela, id_ciclo, num_teacher ) => {
	return new Promise(async(resolve, reject) => {
		try{

			// Cargamos todos las preguntas no borradas
			const cantAlumnosEncuestados = await encuestas.cantidadAlumnosEncuestadosNivel( id_ciclo, escuela ).then(response=> response)

			// VALIDAR QUE HAYA ALUMNOS ENCUESTADOS
			if( !cantAlumnosEncuestados.length){ return res.status( 400 ).send({ message: 'No hay encuestas realizadas' })}

			const cantAlumnosxEncuestar  = await encuestas.cantidadAlumnosPorEncuestarNivel( id_ciclo, escuela ).then(response=> response)
			
			/*
				OBTENER LAS PREGUNTAS DE LAS ENCUESTAS
			*/

			const getPreguntasSatisfaccion = await encuestas.getPreguntasSatisfaccion( escuela ).then( response => response )

	    const getOpcionesSatisfaccion  = await encuestas.getOpcionesSatisfaccion( escuela ).then( response => response )

	    const getRespuestasEncuesta    = await encuestas.getRespuestasEncuesta( id_ciclo, escuela ).then( response => response )

	    const idTeacher1 = getRespuestasEncuesta.map(( registro => { return registro.id_teacher }))

	    const teachers = await encuestas.obtenerTeachersNombre( idTeacher1, escuela ).then( response => response )

	    let preguntasSeccion1  = getPreguntasSatisfaccion.filter( el => { return [2].includes( el.seccion ) && [1].includes( el.tipo_pregunta ) })

	    let headers = [
	    	{ text: 'Teacher'       , value: 'nombre'             },
        { text: 'Encuestas'     , value: 'encuestas'          },
        { text: 'Total'         , value: 'totalExcelente'     },
        { text: 'Excelente'     , value: 'excelencia'         },
        { text: 'Total'         , value: 'totalEntendimiento' },
        { text: 'Entendimiento' , value: 'entendimiento'      },
        { text: 'Total'         , value: 'totalPreparacion'   },
        { text: 'Preparación'   , value: 'preparacion'        },
        { text: 'Total'         , value: 'totalRepetir'       },
        { text: 'Repetir'       , value: 'repetir'            },
	    ]

	    
	    for( const i in teachers ){

	    	const { id } = teachers[i]

	    	teachers[i]['encuestas'] = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == 5 && el.id_teacher == id && el.num_teacher == num_teacher }).length

	    	/*
					MAESTRO EXCELENCIA
					idpre_enc_satis = 5
					idopc_enc_satis = 7
		    */

	    	const cantidadExcelenciaTotal = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == 5 && el.id_teacher == id && el.num_teacher == num_teacher }).length
	    	const cantidadExcelencia      = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == 5 && el.idopc_enc_satis == 7 && el.id_teacher == id && el.num_teacher == num_teacher }).length
	    	let   porcentajeExcelente     = (( cantidadExcelencia / cantidadExcelenciaTotal)*100).toFixed(2)
	    	porcentajeExcelente = parseFloat( porcentajeExcelente ) ? porcentajeExcelente : 0

	    	teachers[i]['totalExcelente'] = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == 5 && el.idopc_enc_satis == 7 && el.id_teacher == id && el.num_teacher == num_teacher }).length
	    	teachers[i]['excelencia']     = porcentajeExcelente

	    	/*
					MAESTRO LE ENTIENDEN
					idpre_enc_satis = 6
					idopc_enc_satis = 11
		    */

	    	const cantidadEntiendeTotal = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == 6 && el.id_teacher == id && el.num_teacher == num_teacher }).length
	    	const cantidadEntiendeBien  = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == 6 && el.idopc_enc_satis == 11 && el.id_teacher == id && el.num_teacher == num_teacher }).length
	    	let   porcentajeEntiende    = (( cantidadEntiendeBien / cantidadEntiendeTotal)*100).toFixed(2)
	    	porcentajeEntiende          = parseFloat( porcentajeEntiende ) ? porcentajeEntiende : 0

	    	teachers[i]['totalEntendimiento'] = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == 6 && el.idopc_enc_satis == 11 && el.id_teacher == id && el.num_teacher == num_teacher }).length
	    	teachers[i]['entendimiento']      = porcentajeEntiende

	    	/*
					PREPARA BIEN SU CLASE
					idpre_enc_satis = 7
					idopc_enc_satis = 14
		    */

	    	const cantidadPreparaTotal     = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == 7 && el.id_teacher == id && el.num_teacher == num_teacher }).length
	    	const cantidadPreparaExcelente = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == 7 && el.idopc_enc_satis == 14 && el.id_teacher == id && el.num_teacher == num_teacher }).length
	    	let   porcetajePrepara         = (( cantidadPreparaExcelente / cantidadPreparaTotal)*100).toFixed(2)
	    	porcetajePrepara               = parseFloat( porcetajePrepara ) ? porcetajePrepara : 0

	    	teachers[i]['totalPreparacion'] = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == 7 && el.idopc_enc_satis == 14 && el.id_teacher == id && el.num_teacher == num_teacher }).length
	    	teachers[i]['preparacion']      = porcetajePrepara


	    	/*
					PREPARA BIEN SU CLASE
					idpre_enc_satis = 8
					idopc_enc_satis = 17
		    */

	    	const cantidadRepetirTotal  = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == 8 && el.id_teacher == id && el.num_teacher == num_teacher }).length
	    	const cantidadRepetir       = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == 8 && el.idopc_enc_satis == 17 && el.id_teacher == id && el.num_teacher == num_teacher }).length
	    	let   porcentajeRepetir     = (( cantidadRepetir / cantidadRepetirTotal)*100).toFixed(2)
	    	porcentajeRepetir           = parseFloat( porcentajeRepetir ) ? porcentajeRepetir : 0

	    	teachers[i]['totalRepetir'] = getRespuestasEncuesta.filter( el => { return el.idpre_enc_satis == 8 && el.idopc_enc_satis == 17 && el.id_teacher == id && el.num_teacher == num_teacher }).length
	    	teachers[i]['repetir']      = porcentajeRepetir

	    }

	    const respuesta = {
	    	teachers,
	    	headers,
	    }
    	
	    resolve( respuesta )
		}catch( error ){
			reject( error )
		}
  })
}