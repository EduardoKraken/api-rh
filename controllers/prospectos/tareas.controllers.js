const tareas = require("../../models/prospectos/tareas.model.js");

// Consultar las Tareas con deleted 0
exports.getTareasList = (req, res) => {
  tareas.getTareasList((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las Tareas"
      });
    else res.send(data);
  });
};

exports.getTareasActivos = (req, res) => {
  tareas.getTareasActivos((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las Tareas"
      });
    else res.send(data);
  });
};

exports.getTareasActivosFormulario = (req, res) => {
  tareas.getTareasActivosFormulario((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las Tareas"
      });
    else res.send(data);
  });
};

exports.addTareas = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  tareas.addTareas(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la tarea"
      })
    else res.status(200).send({ message: `La tarea se creo correctamente` });
  })
};

exports.updateTareas = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }
  tareas.updateTareas(req.params.id, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro la tarea con id : ${req.params.id}, ${ err }` });
      } else {
        res.status(500).send({ message: `Error al actualizar la tarea con el id : ${req.params.id}, ${ err } `});
      }
    } else {
      res.status(200).send({ message: `La tarea se ha actualizado correctamente.` })
    }
  })
}
