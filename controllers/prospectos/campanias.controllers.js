const campanias = require("../../models/prospectos/campanias.model.js");

exports.getCampaniasList = async (req, res) => {

  try{
    const respuesta = await campanias.getCampaniasList( ).then( response => response )

    res.send( respuesta )

  }catch( error ){
    res.status( 500 ).send( { message: 'Error al intentar recuperar los recursos', error } )
  }
};

exports.getCampaniasActivos = async (req, res) => {
  try{
    const respuesta = await campanias.getCampaniasActivos( ).then( response => response )

    res.send( respuesta )

  }catch( error ){
    res.status( 500 ).send( { message: 'Error al intentar recuperar los recursos', error } )
  }

};

exports.addCampanias = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  campanias.addCampanias(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la campañia"
      })
    else res.status(200).send({ message: `La campañia se creo correctamente` });
  })
};

exports.updateCampanias = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }
  campanias.updateCampanias(req.params.id, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro la campañia con id : ${req.params.id}, ${ err }` });
      } else {
        res.status(500).send({ message: `Error al actualizar la campañia con el id : ${req.params.id}, ${ err } `});
      }
    } else {
      res.status(200).send({ message: `La campañia se ha actualizado correctamente.` })
    }
  })
}
