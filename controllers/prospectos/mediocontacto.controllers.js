const mediocontacto = require("../../models/prospectos/mediocontacto.model.js");

// Consultar las MedioContacto con deleted 0
exports.getMedioContactoList = (req, res) => {
  mediocontacto.getMedioContactoList((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las MedioContacto"
      });
    else res.send(data);
  });
};

exports.addMedioContacto = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  mediocontacto.addMedioContacto(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la etapa"
      })
    else res.status(200).send({ message: `La etapa se creo correctamente` });
  })
};

exports.updateMedioContacto = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }
  mediocontacto.updateMedioContacto(req.params.id, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro la etapa con id : ${req.params.id}, ${ err }` });
      } else {
        res.status(500).send({ message: `Error al actualizar la etapa con el id : ${req.params.id}, ${ err } `});
      }
    } else {
      res.status(200).send({ message: `La etapa se ha actualizado correctamente.` })
    }
  })
}

exports.getMedioActivas = async(req, res) => {
  try {
    // Consultar solo los horarios activos
    const getMedioActivas = await mediocontacto.getMedioActivas( ).then(response=> response)

    // enviar los horarios
    res.send(getMedioActivas);

  } catch (error) {
    res.status(500).send({message:error})
  }
};
