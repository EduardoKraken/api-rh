const leds = require("../../models/prospectos/leds.model.js");
const prospectos    = require("../../models/prospectos/prospectos.model.js");


// Consultar las Fuente con deleted 0
exports.getLeds = (req, res) => {
  leds.getLeds(req.params.escuela,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las Fuente"
      });
    else res.send(data);
  });
};

exports.buscarTelefono = async (req, res) => {
  try {
    const { escuela, telefono } = req.body
    // Primeroooo vamos a validar si el usuario ya existe
    const existeProspecto = await prospectos.existeProspecto( escuela, telefono ).then(response => response)
    if(existeProspecto){
      const { folio } = existeProspecto
      return res.send({message:`El prospecto que intentas ingresar ya existe: ${ folio }`})
    }

    // Ahora hay que validar ese ese telefono no exista pero en el ERP viejito
    const existeProspectoERP = await prospectos.existeProspectoERP( escuela, telefono ).then(response => response)
    if(existeProspectoERP){
      const { nombre, grupo, matricula, plantel } = existeProspectoERP
      const mensaje = 
      `Alumno: ${ nombre } \n Matricula: ${ matricula } \n Grupo: ${ grupo } \n Plantel: ${ plantel }`
      return res.send({ message: mensaje })
    }

    res.status(500).send({message:'No existe :p'})

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.deleteLead = async (req, res) => {
  try {
    const { id } = req.params
    // Primeroooo vamos a validar si el usuario ya existe
    const deleteLead = await leds.deleteLead( id ).then(response => response)
    return res.send({ message:'Ok' })
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error interno'})
  }
};


exports.addLeds = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  leds.addLeds(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la etapa"
      })
    else res.status(200).send( data );
  })
};


exports.updateLead = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  leds.updateLead(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la etapa"
      })
    else res.status(200).send( data );
  })
};



exports.updateFuente = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }
  leds.updateFuente(req.params.id, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro la etapa con id : ${req.params.id}, ${ err }` });
      } else {
        res.status(500).send({ message: `Error al actualizar la etapa con el id : ${req.params.id}, ${ err } `});
      }
    } else {
      res.status(200).send({ message: `La etapa se ha actualizado correctamente.` })
    }
  })
}


exports.getFuenteActivos = (req, res) => {
  leds.getFuenteActivos((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las Fuente"
      });
    else res.send(data);
  });
};

