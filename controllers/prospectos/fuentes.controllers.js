const fuentes = require("../../models/prospectos/fuentes.model.js");

// Consultar las Fuente con deleted 0
exports.getFuenteList = (req, res) => {
  fuentes.getFuenteList((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las Fuente"
      });
    else res.send(data);
  });
};

exports.addFuente = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  fuentes.addFuente(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la etapa"
      })
    else res.status(200).send({ message: `La etapa se creo correctamente` });
  })
};

exports.updateFuente = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }
  fuentes.updateFuente(req.params.id, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro la etapa con id : ${req.params.id}, ${ err }` });
      } else {
        res.status(500).send({ message: `Error al actualizar la etapa con el id : ${req.params.id}, ${ err } `});
      }
    } else {
      res.status(200).send({ message: `La etapa se ha actualizado correctamente.` })
    }
  })
}


exports.getFuenteActivos = (req, res) => {
  fuentes.getFuenteActivos((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las Fuente"
      });
    else res.send(data);
  });
};

// Consultar las Fuente con deleted 0
exports.getFuenteDetalleList = (req, res) => {
  fuentes.getFuenteDetalleList((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las Fuente"
      });
    else res.send(data);
  });
};

exports.addFuenteDetalle = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  fuentes.addFuenteDetalle(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la etapa"
      })
    else res.status(200).send({ message: `La etapa se creo correctamente` });
  })
};

exports.updateFuenteDetalle = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }
  fuentes.updateFuenteDetalle(req.params.id, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro la etapa con id : ${req.params.id}, ${ err }` });
      } else {
        res.status(500).send({ message: `Error al actualizar la etapa con el id : ${req.params.id}, ${ err } `});
      }
    } else {
      res.status(200).send({ message: `La etapa se ha actualizado correctamente.` })
    }
  })
}


exports.getFuenteDetalleActivos = (req, res) => {
  fuentes.getFuenteDetalleActivos((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las Fuente"
      });
    else res.send(data);
  });
};