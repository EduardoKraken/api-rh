const erpviejo        = require("../../models/prospectos/erpviejo.model.js");
const usuarios        = require("../../models/usuarios.model.js");
const planteles       = require("../../models/planteles_usuario.model.js");
const dashboardVentas = require("../../models/prospectos/dashboardVentas.model.js");
const prospectos      = require("../../models/prospectos/prospectos.model.js");

exports.getDashboardVendedora = async(req, res) => {
  try {
  	const { id } = req.params
    let inicio =  '0000:00:00'
    let fin    =  '9999:12:12'
    
    let parametro = ` AND p.usuario_asignado = ${id} `
    
    const respuesta = await prospectos.getProspectosVendedora( parametro ).then(response=> response)

    // Calcular las fechas actuales del ciclo
    
    const fechasActuales = await dashboardVentas.fechasActuales( parametro ).then(response=> response)
    if(fechasActuales){
      inicio = fechasActuales.inicio
      fin    = fechasActuales.fin
    }
    
    // Sacamos la información de las etapas
    const dataEtapas = [
      { etapa: 'Nuevos'           , total: respuesta.filter((el)=>{ return el.idetapa == 2}).length, color:'success' , icon: 'mdi-account'        },
      { etapa: 'Con información'  , total: respuesta.filter((el)=>{ return el.idetapa == 4}).length, color:'orange'  , icon: 'mdi-account-plus'   },
      { etapa: 'Sin información'  , total: respuesta.filter((el)=>{ return el.idetapa == 5}).length, color:'brown'   , icon: 'mdi-account-remove' },
      { etapa: 'Inducción'        , total: respuesta.filter((el)=>{ return el.idetapa == 3}).length, color:'pink'    , icon: 'mdi-account-check'  },
    ]

    // A cuantos se les dio información
    const contactosConInfo = await dashboardVentas.conInformacion( id ).then(response=> response)

    // Tareas para el día de hoy
    const tareasHoy = await dashboardVentas.tareasHoy( id ).then(response=> response)

    // Tareas para el día de hoy
    const inscritosHoy = await dashboardVentas.inscritosHoy( id, inicio, fin ).then(response=> response)

    const seguimientos = await dashboardVentas.seguimientos( id ).then(response=> response)

    // Ver los ventos que se realizaron hoy
    const eventosHoy = await dashboardVentas.eventosHoy( id ).then(response=> response)
    // Sacamos el total de llamdas
    const llamadas       = eventosHoy.filter((el)=>{ return el.idtipo_evento == 11 || el.idtipo_evento == 12 }).length
  	const mensajes       = eventosHoy.filter((el)=>{ return el.idtipo_evento == 5 }).length
  	const intentoWha     = eventosHoy.filter((el)=>{ return el.idtipo_evento == 10 }).length
  	const intentoLlamada = eventosHoy.filter((el)=>{ return el.idtipo_evento == 9 }).length

    const dataLlamadas =  [
      {
        evento: 'Llamadas realizadas',
        total : llamadas,
        color : 'success',
        value : llamadas > 0 ? ((llamadas / eventosHoy.length) * 100) : 0
      },
      {
        evento: 'Intento llamada normal',
        total : intentoLlamada,
        color : 'error',
        value : intentoLlamada > 0 ? ((intentoLlamada / eventosHoy.length) * 100) : 0
      },
      {
        evento: 'Intento llamada whatsApp',
        total : intentoWha,
        color : 'orange',
        value : intentoWha > 0 ? ((intentoWha / eventosHoy.length) * 100) : 0
      },

      {
        evento: 'Mensajes enviados',
        total : mensajes,
        color : 'blue',
        value : mensajes > 0 ? ((mensajes / eventosHoy.length) * 100) : 0
      },
		]

    res.send({
    	dataEtapas, 
    	conInformacion: contactosConInfo.length,
    	tareasHechas:   tareasHoy.filter((el)=>{ return el.estatus == 1 || el.estatus == 1 }).length,
    	tareasTotales:  tareasHoy.length,
    	inscritosHoy:   inscritosHoy.filter((el)=>{ return el.hoy == 1 }).length,
    	inscritosTotal: inscritosHoy.length,
    	dataLlamadas,
    	totalEventos:   eventosHoy.length,
    	seguimientos:   seguimientos.length
    });

    
  } catch (error) {
    res.status(500).send({message:error})
  }

};

