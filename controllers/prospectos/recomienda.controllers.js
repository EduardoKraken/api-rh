const recomienda = require("../../models/prospectos/recomienda.model.js");

// Consultar las anuncios con deleted 0
exports.getRecomienda = async (req, res) => {
  try {
    const recomiendaInbi = await recomienda.getRecomienda( 1 ).then( response => response )
    const recomiendaFast = await recomienda.getRecomienda( 2 ).then( response => response )

    const respuesta = recomiendaInbi.concat( recomiendaFast )

    let id_grupos              = respuesta.map((registro) => { return registro.id_grupo })
    let id_grupos_recomendados = respuesta.map((registro) => { return registro.id_grupo_recomendado })

    let idgrupos = id_grupos.concat( id_grupos_recomendados )

    idgrupos = idgrupos.length > 0 ? idgrupos : [0]

    const gruposMasivosRecomendado = await recomienda.gruposMasivosRecomendado( idgrupos ).then( response => response ) 


    for( const i in respuesta ){
      const { id_grupo, id_grupo_recomendado } = respuesta[i]

      const existeGrupoNormal      = gruposMasivosRecomendado.find( el => el.id_grupo == id_grupo )
      const existeGrupoRecomendado = gruposMasivosRecomendado.find( el => el.id_grupo == id_grupo_recomendado )

      respuesta[i]['grupo']             = existeGrupoNormal      ? existeGrupoNormal.grupo      : 'Sin grupo'
      respuesta[i]['grupo_recomendado'] = existeGrupoRecomendado ? existeGrupoRecomendado.grupo : 'Sin grupo'

    }

    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getRecomiendaInscritos = async (req, res) => {
  try {
    const recomiendaInbi = await recomienda.getRecomiendaInscritos( 1 ).then( response => response )
    const recomiendaFast = await recomienda.getRecomiendaInscritos( 2 ).then( response => response )
    
    const respuesta = recomiendaInbi.concat( recomiendaFast )

    let id_grupos              = respuesta.map((registro) => { return registro.id_grupo })
    let id_grupos_recomendados = respuesta.map((registro) => { return registro.id_grupo_recomendado })

    let idgrupos = id_grupos.concat( id_grupos_recomendados )

    idgrupos = idgrupos.length > 0 ? idgrupos : [0]

    const gruposMasivosRecomendado = await recomienda.gruposMasivosRecomendado( idgrupos ).then( response => response ) 


    for( const i in respuesta ){
      const { id_grupo, id_grupo_recomendado } = respuesta[i]

      const existeGrupoNormal      = gruposMasivosRecomendado.find( el => el.id_grupo == id_grupo )
      const existeGrupoRecomendado = gruposMasivosRecomendado.find( el => el.id_grupo == id_grupo_recomendado )

      respuesta[i]['grupo']             = existeGrupoNormal      ? existeGrupoNormal.grupo      : 'Sin grupo'
      respuesta[i]['grupo_recomendado'] = existeGrupoRecomendado ? existeGrupoRecomendado.grupo : 'Sin grupo'

    }

    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};


exports.addRecomienda = async(req, res) => {
	try {
	  //validamos que tenga algo el req.body
	  if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }
 		
 		const { id_alumno, nombre_recomendado, escuela } = req.body

    const codigo = generarCodigo(6)

    const addRecomienda = await recomienda.addRecomienda( id_alumno, nombre_recomendado, escuela, codigo ).then( response => response )
    
    res.send({ codigo })
  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

generarCodigo = (length) => {
  let characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

  let pass = "";
  for (i=0; i < length; i++){
    pass += characters.charAt(Math.floor(Math.random()*characters.length));   
  }
  return pass;
}


exports.updateRecomienda = async(req, res) => {
	try {
	  if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }

    const updateRecomienda = await recomienda.updateRecomienda( req.body, req.body.escuela ).then( response => response )
    
    res.send(updateRecomienda)
  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};