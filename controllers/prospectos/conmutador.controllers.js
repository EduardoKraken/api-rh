const conmutador = require("../../models/prospectos/conmutador.model.js");
const erpviejo   = require("../../models/prospectos/erpviejo.model.js");
const planteles  = require("../../models/planteles_usuario.model.js");

// Consultar las anuncios con deleted 0
exports.getConmutador = async (req, res) => {
  try {
    const data = await conmutador.getConmutador( ).then( response => response )
    res.send(data)
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addConmutador = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  conmutador.addConmutador(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la etapa"
      })
    else res.status(200).send({ message: `La etapa se creo correctamente` });
  })
};

exports.updateConmutador = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }
  conmutador.updateConmutador(req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro la etapa con id : ${ err }` });
      } else {
        res.status(500).send({ message: `Error al actualizar la etapa con el id : ${ err } `});
      }
    } else {
      res.status(200).send({ message: `La etapa se ha actualizado correctamente.` })
    }
  })
}


// CARGAR LOS DATOS DEL EXCEL EN LA BASE DE DATOS
exports.cargarDatos = async (req, res) => {
  try {

    // VALIDAR QUE SE ESTÉN RECIBIENDO LOS DATOS
    if (!req.body || Object.keys(req.body).length === 0) {
      res.status(500).send({ message: 'El contenido no puede ir vacio' })
    }

    let data = req.body

    //  IMPORTANTEEEE ANTES DE GABAR HAY QUE VER BUSCAR LA VENDEDORAAAAAAA
    const vendedoraConmutador = await conmutador.getConmutador( ).then( response => response )

    // RECORREMOS PARA BUSCAR LA VENDEDORA DE CADA LLAMADA
    for( const i in data ){
      const { caller_number, callee_number } = data[i] //DESESTRUCUTRAMOS PARA SACAR SOLO LOS DATOS QUE NECESITAMOS PARA VALIDAR SI EXISTE EL REGISTRO O NO

      const ext = callee_number.toString().substr(0,2)

      // HACEMOS LA BUSQUEDA PRIMERO
      const existeVendedora = vendedoraConmutador.find( el => el.ext == ext )

      data[i]['id_usuario'] = existeVendedora ? existeVendedora.id_usuario : 0
      data[i]['ext']        = existeVendedora ? ext                        : 0
    }


    // RECORREMOS CADA UNA DE LAS FILAS DE LOS DATOS
    for( const i in data ){
      const { uniqueid, start_time, answer_time, end_time } = data[i] //DESESTRUCUTRAMOS PARA SACAR SOLO LOS DATOS QUE NECESITAMOS PARA VALIDAR SI EXISTE EL REGISTRO O NO

      // HACEMOS LA BUSQUEDA PRIMERO
      const existeRegistro = await conmutador.existeRegistro( uniqueid, start_time, answer_time, end_time ).then( response => response )

      // SI EL REGISTRO NO EXISTE, ENTONCES HAY QUE AGREGARLO
      if( !existeRegistro ){
        const addRegistroConmutador = await conmutador.addRegistroConmutador( data[i] ).then( response => response )
      }
    }

    res.send({ message: 'Datos agregados correctamente' })

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};


// GENERAR EL REPORTE DE LLAMADAS POR VENDEDORA Y FECHAS
exports.reporteLlamadas = async (req, res) => {
  try {

    // VALIDAR QUE SE ESTÉN RECIBIENDO LOS DATOS
    if (!req.body || Object.keys(req.body).length === 0) {
      res.status(500).send({ message: 'El contenido no puede ir vacio' })
    }

    let { fecha_inicio, fecha_fin } = req.body

    // PRIMERO CONSULTAMOS LAS LLAMADAS
    const llamadas = await conmutador.getConmutadorFechas( fecha_inicio, fecha_fin ).then( response => response )

    // AHORA CONSULTAMOS A LAS VENDEDORAS UNICAS
    const usuariosERP = await erpviejo.getUsuariosERP( ).then(response=> response)
    const vendedoras  = await planteles.plantelesVendedora( ).then(response=> response)

    for(const i in vendedoras){
      const { iderp, idusuario } = vendedoras[i]

      const existeUsuario = usuariosERP.find(el => el.id_usuario == iderp)
      if(existeUsuario){
        vendedoras[i] = {
          ...vendedoras[i],
          nombre_completo: existeUsuario.nombre_completo,
        }
      }
    }

    vendedoras.sort(function (a, b) {
      if (a.nombre_completo > b.nombre_completo) {
        return 1;
      }
      if (a.nombre_completo < b.nombre_completo) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });



    // RECORREMOS TODO EL ARREGLO DE LAS VENDEDORAS Y EMPEZAMOS A REALIZAR EL DESGLOSE DE LA INFORMACIÓN
    for( const i in vendedoras ){
      const { iderp } = vendedoras[i] // DESESTRUCTURAMOS LA INFORMACIÓN Y SACAMOS EL ID_USUARIO DE LA VENDEDORA

      // TOTAL DE LLAMADAS
      vendedoras[i]['totalLlamadas']       = llamadas.filter( el => { return el.id_usuario == iderp })

      // LLAMADAS INTERNAS DESGLOSE
      const llamadasInternas = llamadas.filter( el => { return el.id_usuario == iderp && el.internas == 1 })
      vendedoras[i]['llamadasInternas']      = llamadasInternas
      vendedoras[i]['inLlamadasEntrantes']   = llamadasInternas.filter( el => { return el.userfield == 'Inbound' })
      vendedoras[i]['inLlamadasSalientes']   = llamadasInternas.filter( el => { return el.userfield == 'Outbound' })

      vendedoras[i]['inLlamadasInAnswer']    = llamadasInternas.filter( el => { return el.userfield == 'Inbound' && el.disposition == 'ANSWERED' })
      vendedoras[i]['inLlamadasInNOAnswer']  = llamadasInternas.filter( el => { return el.userfield == 'Inbound' && el.disposition == 'NO ANSWER' })
      vendedoras[i]['inLlamadasInBusy']      = llamadasInternas.filter( el => { return el.userfield == 'Inbound' && el.disposition == 'BUSY' })
      vendedoras[i]['inLlamadasInFailed']    = llamadasInternas.filter( el => { return el.userfield == 'Inbound' && el.disposition == 'FAILED' })

      vendedoras[i]['inLlamadasOutAnswer']   = llamadasInternas.filter( el => { return el.userfield == 'Outbound' && el.disposition == 'ANSWERED' })
      vendedoras[i]['inLlamadasOutNOAnswer'] = llamadasInternas.filter( el => { return el.userfield == 'Outbound' && el.disposition == 'NO ANSWER' })
      vendedoras[i]['inLlamadasOutBusy']     = llamadasInternas.filter( el => { return el.userfield == 'Outbound' && el.disposition == 'BUSY' })
      vendedoras[i]['inLlamadasOutFailed']   = llamadasInternas.filter( el => { return el.userfield == 'Outbound' && el.disposition == 'FAILED' })


      // LLAMADAS EXTERNAS DESGLOSE
      const llamadasExternas = llamadas.filter( el => { return el.id_usuario == iderp && el.internas == 0 })
      vendedoras[i]['llamadasExternas']      = llamadas.filter( el => { return el.id_usuario == iderp && el.internas == 0 })
      vendedoras[i]['exLlamadasEntrantes']   = llamadasExternas.filter( el => { return el.userfield == 'Inbound' })
      vendedoras[i]['exLlamadasSalientes']   = llamadasExternas.filter( el => { return el.userfield == 'Outbound' })

      vendedoras[i]['exLlamadasInAnswer']    = llamadasExternas.filter( el => { return el.userfield == 'Inbound' && el.disposition == 'ANSWERED' })
      vendedoras[i]['exLlamadasInNOAnswer']  = llamadasExternas.filter( el => { return el.userfield == 'Inbound' && el.disposition == 'NO ANSWER' })
      vendedoras[i]['exLlamadasInBusy']      = llamadasExternas.filter( el => { return el.userfield == 'Inbound' && el.disposition == 'BUSY' })
      vendedoras[i]['exLlamadasInFailed']    = llamadasExternas.filter( el => { return el.userfield == 'Inbound' && el.disposition == 'FAILED' })

      const exLlamadasOutAnswer              = llamadasExternas.filter( el => { return el.userfield == 'Outbound' && el.disposition == 'ANSWERED' })
      vendedoras[i]['exLlamadasOutAnswer']   = exLlamadasOutAnswer
      vendedoras[i]['exLlamadasOutNOAnswer'] = llamadasExternas.filter( el => { return el.userfield == 'Outbound' && el.disposition == 'NO ANSWER' })
      vendedoras[i]['exLlamadasOutBusy']     = llamadasExternas.filter( el => { return el.userfield == 'Outbound' && el.disposition == 'BUSY' })
      vendedoras[i]['exLlamadasOutFailed']   = llamadasExternas.filter( el => { return el.userfield == 'Outbound' && el.disposition == 'FAILED' })

      const exLlamadasOutAnswerTime5min      = llamadasExternas.filter( el => { return el.userfield == 'Outbound' && el.disposition == 'ANSWERED' && el.talk_time >= 300 })


      vendedoras[i]['exLlamadasOutAnswerTime5min']    = exLlamadasOutAnswerTime5min
      vendedoras[i]['exLlamadasOutAnswerTimeNo5min']  = llamadasExternas.filter( el => { return el.userfield == 'Outbound' && el.disposition == 'ANSWERED' && el.talk_time <= 300 })

      const efectividad = exLlamadasOutAnswerTime5min.length > 0 ? (( exLlamadasOutAnswerTime5min.length / exLlamadasOutAnswer.length ) * 100 ) : 0
      vendedoras[i]['efectividad']  = efectividad.toFixed(2)

    }

    res.send(vendedoras)

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};