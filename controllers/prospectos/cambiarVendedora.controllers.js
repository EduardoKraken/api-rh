const { response } = require("express");
const { rest } = require("lodash");
const cambiarVendedora = require("../../models/prospectos/cambiarVendedora.model.js");


exports.getAlumnos = async (req, res) => {
  try {
    const respuesta = await cambiarVendedora.getAlumnos( ).then(response => response)
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};


exports.getVendedora = async (req, res) => {
  try {
    const respuesta = await cambiarVendedora.getVendedora( ).then(response => response)
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};

exports.updateAlumnoVendedora = async(req, res) => {
  try {
    const { id_usuario_ultimo_cambio, id_alumno } = req.body
    
    // Actualizar "finalizo"
    const respuesta = await cambiarVendedora.updateAlumnoVendedora( id_usuario_ultimo_cambio, id_alumno ).then(response => response) 
    res.send({message: 'Alumno Actualizado'});
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};


//ANGEL RODRIGUEZ -- TODO