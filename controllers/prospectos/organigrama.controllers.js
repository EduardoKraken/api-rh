const { response } = require("express");
const { rest } = require("lodash");
const organigrama      = require("../../models/prospectos/organigrama.model.js");
const entradas         = require("../../models/catalogos/entradas.model.js");
const calificaciones   = require("../../models/capacitacion/calificaciones.model.js");


exports.getOrganigrama = async (req, res) => {
  try {

    // Consultar el organigrama
    let respuesta       = await organigrama.getOrganigrama( ).then(response => response)
    
    // Consultamos las vacantes
    const getVacantes   = await organigrama.getVacantes( ).then(response => response)

    // Sacamos los iderp de todos (let respuesta)
    const iderps = respuesta.map((registro)=>{ return registro.iderp })

    // Sacamos los idjesfes de todos (let respuesta)
    const idjefes = respuesta.map((registro)=>{ return registro.idjefe })

    // Obtener los usuarios del sistema
    const usuariosSistema = await entradas.getUsuariosERP( iderps ).then( response => response )

    // Obtener los jefes
    const jefeSistemas = await entradas.getUsuariosERP( iderps ).then( response => response )
 
    // Obtener el plantel desde IDERP
    const plantel = await organigrama.getPlantel( iderps ).then( response => response ) 

    // Obtener los usuarios del Nuevo ERP
    const usuariosNuevoERP = await calificaciones.usuariosNuevoERP( ).then( response => response )


    //Recorremos la tabla organigrama
    for (const i in respuesta){

      //Obtenemos el iderp de todos
      const { iderp, idjefe, pid } = respuesta[i]
      //Guardamos en una varible el resultado de la consulta cuando el idusuario y el iderp coincidan (depende del nombre del id)
      const existeUsuario = usuariosSistema.find(el=>el.id_usuario == iderp)    
      const existeJefe    = jefeSistemas.find(el=>el.id_usuario == idjefe)
      const existePlantel = plantel.find(el=>el.id_usuario == iderp)
      const existeEscuela = plantel.find(el=>el.id_usuario == iderp)


  
      if( iderp == 10000 ){

        respuesta[i]['name']    = 'FAST ENGLISH'    
        respuesta[i]['title']   = 'FAST ENGLISH'    
        respuesta[i]['img']     = "https://ui-avatars.com/api/?background=1976d2&color=fff&format=svg&name=FAST ENGLISH"
        respuesta[i]['jefe']    = 'FAST ENGLISH'
        respuesta[i]['plantel'] = 'FAST'
        respuesta[i]['escuela'] = 2
  
      }else if( iderp == 10001 ){

        respuesta[i]['name']    = 'INBI SCHOOL'    
        respuesta[i]['title']   = 'INBI SCHOOL'    
        respuesta[i]['img']     = "https://ui-avatars.com/api/?background=1976d2&color=fff&format=svg&name=INBI SCHOOL"
        respuesta[i]['jefe']    = 'INBI SCHOOL'
        respuesta[i]['plantel'] = 'INBI'
        respuesta[i]['escuela'] = 1
  
      }else if( iderp == 10002 ){

        respuesta[i]['name']    = 'REQUERIDA'    
        respuesta[i]['title']   = 'REQUERIDA'    
        respuesta[i]['img']     = "https://ui-avatars.com/api/?background=1976d2&color=fff&format=svg&name=REQUERIDA"
        respuesta[i]['jefe']    = 'REQUERIDA'
        respuesta[i]['plantel'] = 'REQUERIDA'
        respuesta[i]['escuela'] = 1

      }else if( iderp > 1000000 ){

        const idBusqueda = iderp - 1000000

        const buscarVacante = getVacantes.find( el => el.idvacantes == idBusqueda )
        let nombreJefe      = existeJefe      ? existeJefe.nombre_completo : ( 'SIN NOMBRE -> ' + i )

        nombreJefe = pid == 172 ? 'FAST ENGLISH' : pid == 173 ? 'INBI SCHOOL' : nombreJefe

        respuesta[i]['name']    = buscarVacante ? ( 'VACANTE - ' + buscarVacante.puesto )   : ( 'SIN NOMBRE -> ' + i )
        respuesta[i]['img']     = "https://ui-avatars.com/api/?background=1976d2&color=fff&format=svg&name=" + ( buscarVacante ? 'VACANTE - ' + buscarVacante.puesto    : '')
        respuesta[i]['jefe']    = nombreJefe
        respuesta[i]['plantel'] = buscarVacante ? buscarVacante.plantel : 'Sin plantel'
        respuesta[i]['escuela'] = 1
        respuesta[i]['title']   = buscarVacante ? buscarVacante.puesto  : 'Sin puesto'

      }else{

        let nombre             = existeUsuario   ? existeUsuario.nombre_completo : "SIN NOMBRE"
        let nombreJefe         = existeJefe      ? existeJefe.nombre_completo : "SIN NOMBRE"
        const plantelUsuario   = existePlantel   ? existePlantel.plantel : "SIN NOMBRE"
        const escuela          = existeEscuela   ? existeEscuela.escuela : 0
        const existePuesto     = usuariosNuevoERP.find(el=>el.iderp == iderp)

        nombreJefe = pid == 172 ? 'FAST ENGLISH' : pid == 173 ? 'INBI SCHOOL' : nombreJefe

        respuesta[i]['name']    = nombre 
        respuesta[i]['img']     = "https://ui-avatars.com/api/?background=1976d2&color=fff&format=svg&name=" + nombre
        respuesta[i]['jefe']    = nombreJefe
        respuesta[i]['plantel'] = plantelUsuario
        respuesta[i]['escuela'] = escuela
        respuesta[i]['title']   = existePuesto ? existePuesto.puesto : 'Sin puesto'

      }
      
    }    

    //Ordenarlo alfabeticamente por "puesto"
    respuesta.sort(function (a, b) {
      if (a.name.toUpperCase() > b.name.toUpperCase()) {
        return 1;
      }
      if (a.name.toUpperCase() < b.name.toUpperCase()) {
        return -1;
      }
      return 0;
    });
    
    
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};

exports.addOrganigrama = async(req, res) => {
  try {
    const {pid, iderp}= req.body
    const addOrganigrama = await organigrama.addOrganigrama(pid,iderp).then(response => response)
    res.send({message:"Datos grabados correctamente"})
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};

exports.getPuestosOrganigrama = async (req, res) => {
  
  try {
    const getPuestos      = await organigrama.getPuestos( ).then(response => response) 
    const getPersonas     = await organigrama.getPersonas( ).then(response => response)
    const getVacantes     = await organigrama.getVacantes( ).then(response => response)
    const getRequerida    = await organigrama.getRequerida( ).then(response => response)
    const reclutadoras    = await organigrama.getReclutadoras( ).then(response => response)

    // Obtener los nombres de la recluatadora
    let iderpsReclutadoras     = reclutadoras.map((registro)=> {return registro.iderp } );


    // Trae el nombre completo de los usuarios(idusuario)
    let usuariosRecluta = await entradas.getUsuariosERP( iderpsReclutadoras ).then( response => response )

    for( const i in reclutadoras ){
      const { iderp } = reclutadoras[i]

      const existeUsuario = usuariosRecluta.find( el => { return el.id_usuario == iderp })
      reclutadoras[i]['name'] = existeUsuario ? existeUsuario.nombre_completo : 'Sin nombre'

    }

    // Obtener los nombres de la recluatadora
    let iderpsRecluta     = getVacantes.map((registro)=> {return registro.reclutadora } );

    iderpsRecluta = iderpsRecluta.length ?  iderpsRecluta : [0]

    // Trae el nombre completo de los usuarios(idusuario)
    let usuariosSistema = await entradas.getUsuariosERP( iderpsRecluta ).then( response => response )

    let vacantesRequeridas = []
    for( const i in getVacantes ){
      const { reclutadora, idpuesto, puesto, capacitacion } = getVacantes[i]

      const existeUsuario = usuariosSistema.find( el => { return el.id_usuario == reclutadora })
      getVacantes[i]['nombre']     = existeUsuario ? existeUsuario.nombre_completo : 'Sin nombre'
      getVacantes[i]['name']       = existeUsuario ? existeUsuario.nombre_completo : 'Sin nombre'

      const puestoNombre = puesto ? puesto : 'Sin puesto'

      vacantesRequeridas.push({ puesto: puestoNombre, idpuesto, nombre: 'VACANTE', capacitacion })
    }


    // ----------Requisitos, puesto, requerido, ocupados y vacantes----------------------------------------------//


    let iderps = getRequerida.map((registro)=> {return registro.iderp} );
    
    // Trae informacion de los usuarios (iderp) y su puesto  
    const usuariosNuevoERP = await calificaciones.usuariosNuevoERP( ).then( response => response )
    // Trae el nombre completo de los usuarios(idusuario)
    usuariosSistema = await entradas.getUsuariosERP( iderps ).then( response => response )

    let puestosRequeridos = []
    
    for( const i in getRequerida){
      //recorremos la sentencia una por una y obtenermos el iderp de cada fila
      const { iderp } = getRequerida[i]

      //guardarmos los usuarios(iderp) y puestos cuando coincidan los iderps
      const existePuesto    = usuariosNuevoERP.find(registro=>registro.iderp == iderp)
      //guardamos los nombres completos de los usuarios cuando coincidan los iderps
      const existeUsuario   = usuariosSistema.find(registro=>registro.id_usuario == iderp)  
      
      //guardamos en getRequerida los valores de puesto y nombre_completo unicamente
      getRequerida[i]['puesto']   = existePuesto  ? existePuesto.puesto           : 'Sin puesto'
      getRequerida[i]['idpuesto'] = existePuesto  ? existePuesto.idpuesto         : 'Sin puesto'
      getRequerida[i]['nombre']   = existeUsuario ? existeUsuario.nombre_completo : 'Sin nombre'
      getRequerida[i]['name']     = existeUsuario ? existeUsuario.nombre_completo : 'Sin nombre'

      if( existePuesto ){
        puestosRequeridos.push({
          puesto:       existePuesto  ? existePuesto.puesto   : 'Sin puesto',
          idpuesto:     existePuesto  ? existePuesto.idpuesto : 'Sin puesto',
          nombre:       'Sin nombre',
          capacitacion: 0
        })
      }
      
    }

    puestosRequeridos = puestosRequeridos.concat( vacantesRequeridas )

    // Puestos unicos, separamos la propiedad puesto, de los demas datos
    let arrayPuestos = puestosRequeridos.map(item=>{ return [item.idpuesto,item] });
    // Creamos un map de los puestos//
    var puestos = new Map(arrayPuestos); // Pares de clave y valor
    // y ahora si, extraemos los valores unicos
    let puestosUnicos = [...puestos.values()]; // Conversión a un array
  

    //agregamos el valor "total", que contiene la cantidad de puestos de cada tipo
    for( const i in puestosUnicos){
      const { puesto } = puestosUnicos[i]
      puestosUnicos[i]['total'] = getRequerida.filter( el => { return el.puesto == puesto }).length
    }
    
    //obtenemos unicamente el puesto y total
    for( const i in puestosUnicos){
      const { puesto, total } = puestosUnicos[i]
      let vacantes      = puestosRequeridos.filter(el => {return el.puesto == puesto && el.nombre.toUpperCase().match("VACANTE")}).length 
      let totalPuestos  = puestosRequeridos.filter(el => {return el.puesto == puesto }).length 
      let capacitacion  = puestosRequeridos.filter( el => {return el.puesto == puesto }).map(item => item.capacitacion).reduce((prev, curr) => prev + curr, 0);
      let ocupados      = totalPuestos - vacantes;

      puestosUnicos[i]['vacantes']     = vacantes
      puestosUnicos[i]['ocupados']     = ocupados
      puestosUnicos[i]['total']        = total
      puestosUnicos[i]['requeridos']   = totalPuestos
      puestosUnicos[i].capacitacion    = capacitacion
    }

    // Ordenarlo alfabeticamente por "puesto"
    puestosUnicos.sort(function (a, b) {
      if (a.puesto.toUpperCase() > b.puesto.toUpperCase()) {
        return 1;
      }
      if (a.puesto.toUpperCase() < b.puesto.toUpperCase()) {
        return -1;
      }
      return 0;
    });

    // -----------Nombre y Puestos------------------------------------------------------------------------------------//

    for( const i in getPersonas){
      //recorremos la sentencia una por una y obtenermos el iderp de cada fila
      const { iderp } = getPersonas[i]
 
      //guardarmos los usuarios(iderp) y puestos cuando coincidan los iderps
      const existePuesto    = usuariosNuevoERP.find(registro=>registro.iderp == iderp)
      //guardarmos los nombres completos de los usuarios cuando coincidan los iderps
      const existeUsuario   = usuariosSistema.find(registro=>registro.id_usuario == iderp)  
       
      //guardamos en getRequerida los valores de puesto y nombre_completo unicamente
      getPersonas[i]['puesto'] = existePuesto  ? existePuesto.puesto           : 'Sin puesto'
      getPersonas[i]['nombre'] = existeUsuario ? existeUsuario.nombre_completo : 'Sin nombre'
      getPersonas[i]['name']   = existeUsuario ? existeUsuario.nombre_completo : 'Sin nombre'
    }

    // -----------------------------------------------------------------------------------------------//

    // res.send({ getVacantes })
    res.send({ getPuestos, getPersonas, getVacantes, getRequerida, puestosUnicos, reclutadoras })

  } catch (error) {

    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};

exports.deleteOrganigrama = async(req, res) => {
  try {
    const {id}= req.body
    const deleteOrganigrama = await organigrama.deleteOrganigrama(id).then(response => response)
    res.send({message:"Datos eliminados correctamente"})
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
 
};

exports.updateOrganigrama = async(req, res) => {
  try {
    const {iderp, idorganigrama}= req.body
    const updateOrganigrama = await organigrama.updateOrganigrama(iderp, idorganigrama).then(response => response)
    res.send({message:"Datos actualizados correctamente"})
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};

exports.updateTablaVacantes = async(req, res) => {
  try {
    const updateTablaVacantes = await organigrama.updateTablaVacantes( req.body ).then(response => response)
    res.send({message:"Datos actualizados correctamente"})
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
}; 

exports.addVacante = async(req, res) => {
  try {
    const addVacante = await organigrama.addVacante( req.body ).then(response => response)
    res.send({message:"Datos actualizados correctamente"})
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};

exports.getUsuariosOrganigrama = async(req, res) => {
  try {

    // Consutlamos las vacantes
    const getVacantes   = await organigrama.getVacantes( ).then(response => response)

    let usuarios = await organigrama.getUsuariosOrganigrama( req.body ).then(response => response)
    
    // Obtener los nombres de la recluatadora
    let iderpsRecluta     = getVacantes.map((registro)=> {return registro.reclutadora } );

    iderpsRecluta = iderpsRecluta.length ? iderpsRecluta : [0]

    // Trae el nombre completo de los usuarios(idusuario)
    let usuariosSistema = await entradas.getUsuariosERP( iderpsRecluta ).then( response => response )

    let vacantes = []

   
    for( const i in getVacantes ){
      const { reclutadora, puesto, idvacantes, plantel, horario } = getVacantes[i]

      vacantes.push({
        nombre_completo: `VACANTE - ${ plantel } - ${ horario } - ${ puesto }`,
        id_usuario     : idvacantes + 1000000
      })
    }

    respuesta = usuarios.concat(vacantes)

    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};



//ANGEL RODRIGUEZ -- TODO