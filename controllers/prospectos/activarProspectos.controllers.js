const { response } = require("express");
const { rest } = require("lodash");
const activarProspectos = require("../../models/prospectos/activarProspectos.model.js");


exports.getProspectos = async (req, res) => {
  try {
    const respuesta = await activarProspectos.getProspectos( ).then(response => response)
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};


exports.updateProspecto = async(req, res) => {
  try {
    const { idprospectos } = req.body
    // Actualizar "finalizo"
    const respuesta = await activarProspectos.updateProspecto( idprospectos ).then(response => response) 
    res.send({message: 'Prospecto Activado'});
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};


//ANGEL RODRIGUEZ -- TODO