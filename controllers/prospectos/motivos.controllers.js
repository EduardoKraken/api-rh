const motivos = require("../../models/prospectos/motivos.model.js");

// Consultar las Motivo con deleted 0
exports.getMotivoList = (req, res) => {
  motivos.getMotivoList((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las Motivo"
      });
    else res.send(data);
  });
};

exports.addMotivo = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  motivos.addMotivo(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la etapa"
      })
    else res.status(200).send({ message: `La etapa se creo correctamente` });
  })
};

exports.updateMotivo = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }
  motivos.updateMotivo(req.params.id, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro la etapa con id : ${req.params.id}, ${ err }` });
      } else {
        res.status(500).send({ message: `Error al actualizar la etapa con el id : ${req.params.id}, ${ err } `});
      }
    } else {
      res.status(200).send({ message: `La etapa se ha actualizado correctamente.` })
    }
  })
}


exports.getMotivoActivos = (req, res) => {
  motivos.getMotivoActivos((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las Motivo"
      });
    else res.send(data);
  });
};

exports.getMotivoActivosFormulario = (req, res) => {
  motivos.getMotivoActivosFormulario((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las Motivo"
      });
    else res.send(data);
  });
};