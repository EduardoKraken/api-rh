const etapas = require("../../models/prospectos/etapas.model.js");

// Consultar las etapas con deleted 0
exports.getEtapasList = (req, res) => {
  etapas.getEtapasList((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las etapas"
      });
    else res.send(data);
  });
};

exports.getEtapasActivas = (req, res) => {
  etapas.getEtapasActivas((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las etapas"
      });
    else res.send(data);
  });
};

exports.addEtapas = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  etapas.addEtapas(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la etapa"
      })
    else res.status(200).send({ message: `La etapa se creo correctamente` });
  })
};

exports.updateEtapas = (req, res) => {
    if (!req.body || Object.keys(req.body).length === 0) {
      res.status(400).send({ message: "El contenido no puede estar vacío" });
    }
    etapas.updateEtapas(req.params.id, req.body, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({ message: `No se encontro la etapa con id : ${req.params.id}, ${ err }` });
        } else {
          res.status(500).send({ message: `Error al actualizar la etapa con el id : ${req.params.id}, ${ err } `});
        }
      } else {
        res.status(200).send({ message: `La etapa se ha actualizado correctamente.` })
      }
    })
}
