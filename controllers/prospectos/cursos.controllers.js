const cursos = require("../../models/prospectos/cursos.model.js");

// Consultar las Cursos con deleted 0
exports.getCursosList = (req, res) => {
  cursos.getCursosList((err, data) => {
    if (err)
      res.status(400).send({
        message: err.message || "Se produjo algún error al recuperar las Cursos"
      });
    else res.send(data);
  });
};

exports.getCursosActivos = (req, res) => {
  cursos.getCursosActivos((err, data) => {
    if (err)
      res.status(400).send({
        message: err.message || "Se produjo algún error al recuperar las Cursos"
      });
    else res.send(data);
  });
};

exports.addCursos = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  cursos.addCursos(req.body, (err, data) => {
    if (err)
      res.status(400).send({
        message: err.message || "Se produjo algún error al crear el curso"
      })
    else res.status(200).send({ message: `el curso se creo correctamente` });
  })
};

exports.updateCursos = (req, res) => {
    if (!req.body || Object.keys(req.body).length === 0) {
      res.status(400).send({ message: "El contenido no puede estar vacío" });
    }
    cursos.updateCursos(req.params.id, req.body, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(400).send({ message: `No se encontro el curso con id : ${req.params.id}, ${ err }` });
        } else {
          res.status(400).send({ message: `Error al actualizar el curso con el id : ${req.params.id}, ${ err } `});
        }
      } else {
        res.status(200).send({ message: `el curso se ha actualizado correctamente.` })
      }
    })
}

/*************************** M O D A L I D A D E S ***************************/
exports.getModalidades = (req, res) => {
  cursos.getModalidades((err, data) => {
    if (err)
      res.status(400).send({
        message: err.message || "Se produjo algún error al recuperar las Cursos"
      });
    else res.send(data);
  });
};

exports.getModalidadesActivas = (req, res) => {
  cursos.getModalidadesActivas((err, data) => {
    if (err)
      res.status(400).send({
        message: err.message || "Se produjo algún error al recuperar las Cursos"
      });
    else res.send(data);
  });
};

exports.addModalidades = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  cursos.addModalidades(req.body, (err, data) => {
    if (err)
      res.status(400).send({
        message: err.message || "Se produjo algún error al crear el curso"
      })
    else res.status(200).send({ message: `el curso se creo correctamente` });
  })
};

exports.updateModalidades = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }
  cursos.updateModalidades(req.params.id, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(400).send({ message: `No se encontro el curso con id : ${req.params.id}, ${ err }` });
      } else {
        res.status(400).send({ message: `Error al actualizar el curso con el id : ${req.params.id}, ${ err } `});
      }
    } else {
      res.status(200).send({ message: `el curso se ha actualizado correctamente.` })
    }
  })
}

/*************************** F R E C U E N C I A S ***************************/
exports.getFrecuencias = (req, res) => {
  cursos.getFrecuencias((err, data) => {
    if (err)
      res.status(400).send({
        message: err.message || "Se produjo algún error al recuperar las Cursos"
      });
    else res.send(data);
  });
};

exports.getFrecuenciasActivas = (req, res) => {
  cursos.getFrecuenciasActivas((err, data) => {
    if (err)
      res.status(400).send({
        message: err.message || "Se produjo algún error al recuperar las Cursos"
      });
    else res.send(data);
  });
};

exports.addFrecuencias = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  cursos.addFrecuencias(req.body, (err, data) => {
    if (err)
      res.status(400).send({
        message: err.message || "Se produjo algún error al crear el curso"
      })
    else res.status(200).send({ message: `el curso se creo correctamente` });
  })
};

exports.updateFrecuencias = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }
  cursos.updateFrecuencias(req.params.id, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(400).send({ message: `No se encontro el curso con id : ${req.params.id}, ${ err }` });
      } else {
        res.status(400).send({ message: `Error al actualizar el curso con el id : ${req.params.id}, ${ err } `});
      }
    } else {
      res.status(200).send({ message: `el curso se ha actualizado correctamente.` })
    }
  })
}
