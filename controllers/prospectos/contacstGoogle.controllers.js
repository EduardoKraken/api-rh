// const anuncios = require("../../models/prospectos/anuncios.model.js");

const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');

exports.validarAccesoGoogleContact = (req, res) => {
	// If modifying these scopes, delete token.json.
	const SCOPES = ['https://www.googleapis.com/auth/script.projects'];
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	const TOKEN_PATH = 'token.json';

	// Load client secrets from a local file.
	fs.readFile('credentials.json', (err, content) => {
	  if (err) return // console.log('Error loading client secret file:', err);
	  // Authorize a client with credentials, then call the Google Apps Script API.
	  authorize(JSON.parse(content), callAppsScript);
	});

  res.send({message: 'Listo'});
};

