const erpviejo      = require("../../models/prospectos/erpviejo.model.js");
const prospectos    = require("../../models/prospectos/prospectos.model.js");
const familiares    = require("../../models/prospectos/familiares.model.js");
const conmutador    = require("../../models/prospectos/conmutador.model.js");

const prospectosLMS = require("../../models/lms/prospectoslms.model.js");

const planteles     = require("../../models/planteles_usuario.model.js");

const kpi           = require("../../models/kpi/reportes.model.js");

const riesgo        = require("../../models/operaciones/riesgo.model.js");

const md5           = require('md5');

const { v4: uuidv4 } = require('uuid');
const nodemailer     = require('nodemailer');

exports.addProspectos = async(req, res) => {
  try {
  	const { escuela, sucursal_interes, telefono } = req.body

    // Primeroooo vamos a validar si el usuario ya existe
    const existeProspecto = await prospectos.existeProspecto( escuela, telefono ).then(response => response)
    if(existeProspecto){
      res.status(400).send({message:'El prospecto que intentas ingresar ya existe'})
      return
    }

    // Ahora hay que validar ese ese telefono no exista pero en el ERP viejito
    const existeProspectoERP = await prospectos.existeProspectoERP( escuela, telefono ).then(response => response)
    if(existeProspectoERP){
      const { nombre, grupo, matricula, plantel } = existeProspectoERP
      const mensaje = 
      `Alumno: ${ nombre } \n Matricula: ${ matricula } \n Grupo: ${ grupo } \n Plantel: ${ plantel }`
      return res.status(400).send({ message: mensaje })
    }

    // Consultamos los ciclos activos para ver a que ciclo se agregará el prospecto
    const cicloProspecto = await erpviejo.getCiclosActivoParaProspectos( ).then(response => response)
    // Declaramos las variables iniciaeles para eso 
    let id_ciclo_erp   = 0
    let id_grupo_erp   = 0
    let id_usuario_erp = 0

    /*
      Si la sucursal de interes es = 0, eso significa que se desconoce el plantel entonces, hay que buscar las vendedoras, ver quién tiene menos tareas y asignarselo a la vendedora
    */
    // Buscar los planteles con las vendedoras, primero, se filtraran aquellas sucurales que sean de la escuela elegida
    // Para traer los plateles por sucursal, es necesario hacer un group by y para ello se tiene que habilitar la opción con la siguiente petición
    await prospectos.habilitarGroupBy( ).then(response=> response)

    const plantelesv        = await prospectos.plantelesVendedora( escuela ).then(response=> response)
    // Sacamos el ciclo del LMS al que se tiene que agregar
    // Vamos a sacar la información para agregarlo al LMS
    const { id_ciclo } = cicloProspecto.find(el=> el.unidad_negocio == escuela)

    // Validamos la sucursal de interes
    if(sucursal_interes == 0){
      // Como es cero, ahora hay que buscar a la vendedora que sea de Fast o de inbi
      // Validaremos aquí la carga de trabajo de cada vendedoraaaaa
      let cantMayor = 100000
      let vendedora = null
      let getTareasUsuario = null
      // Recorremos a las vendedoras
      for(const i in plantelesv){
        // Sacamos el iduaurioerp de la vendedora
        const { iderp } = plantelesv[i]
        // Aquí ya tenemos el iderp de la vendedora, ahora hay que sacar sus actividades
        getTareasUsuario  = await prospectos.getTareasUsuario( iderp ).then(response=> response)
        // Comparamos su carga de trabajo con las demás
        if(getTareasUsuario.length < cantMayor){
          // guardamos la nueva cantidad de trabajo
          cantMayor = getTareasUsuario.length
          // Guardamos a la vendedora
          vendedora = plantelesv[i]
        }
      }
      // Aquí ya tenemos a la vendora con mayor carga
      // Modificamos del prospecto el usuario asignado y el folio
      req.body.usuario_asignado = vendedora.iderp
      // Modificamos la sucursal de interes
      req.body.sucursal_interes = vendedora.idplantel
      // Lo agregamos 
      const addProspecto  = await prospectos.addProspecto( req.body ).then(response=> response)
      // Y ahora actualizamos el folio del prospecto
      let folio = vendedora.acronimo + addProspecto.id
      const updateProspecto  = await prospectos.updateProspectoFolio( addProspecto.id, folio ).then(response=> response)
      // Y para finalizar agregaremos el evento 
      let eventoTxt = 'El prospecto fue ingresado al sistema'
      const evento  = await prospectos.addEvento( eventoTxt, addProspecto.id, 1, addProspecto.usuario_creo ).then(response=> response)

      // Ahoraaaaaa se tiene que agregar al LMS
      // Paso por paso
      // Paso # 1 validar que el ciclo exista
      let existeCiclo          = null
      let addCiclo             = null
      let existegrupoERP       = null
      let addGrupoErp          = null
      let existeAlumnoErp      = null
      let existeGrupoAlumnoERP = null
      let addUsuarioErp        = null

      if(escuela == 2){
        existeCiclo = await prospectosLMS.getExisteCicloFast( id_ciclo ).then(response=> response)
      }else{
        existeCiclo = await prospectosLMS.getExisteCicloInbi( id_ciclo ).then(response=> response)
      }

      if( !existeCiclo ){
        const cicloERP = await prospectosLMS.getcicloERP( id_ciclo ).then(response => response)
        let add_ciclo = {
          nombre:               cicloERP.ciclo,
          estatus:              0,
          fecha_inicio:         cicloERP.fecha_inicio_ciclo.substr(0,10),
          fecha_fin:            cicloERP.fecha_fin_ciclo.substr(0,10),
          fecha_inicio_excluir: null,
          fecha_fin_excluir:    null,
          comentarios:          'sin comentarios',
          usuario_creo:         1,
          usuario_modifico:     1,
          deleted:              0,
          iderp:                cicloERP.id_ciclo
        }

        if(escuela == 2){
          addCiclo = await prospectosLMS.addCicloFast( add_ciclo ).then(response=> response)
        }else{
          addCiclo = await prospectosLMS.addCicloInbi( add_ciclo ).then(response=> response)
        }
        
        id_ciclo_erp = addCiclo.id_ciclo
      }else{
        id_ciclo_erp = existeCiclo.id
      }

      // Aquí ya tenemos ahora si el id_ciclo_erp lleno con el id_ciclo correspondiente
      // Paso #2 Buscar el grupo  de inducción de ese ciclo

      if(escuela == 2){
        existegrupoERP = await prospectosLMS.getGrupoFast( id_ciclo_erp ) .then(response => response)
      }else{
        existegrupoERP = await prospectosLMS.getGrupoInbi( id_ciclo_erp ) .then(response => response)
      }

      if( !existegrupoERP ){
        let id_grupo_erp = {
          nombre:               'FAST_INDUCCION - CICLO NA FE - 10_00 AM a 11_00 A-M - NIVEL 1',
          estatus:              0,
          id_unidad_negocio:    escuela,
          id_plantel:           14,
          id_ciclo:             id_ciclo_erp,
          id_curso:             1,
          id_horario:           0,
          id_salon:             1,
          id_nivel:             1,
          codigo_acceso:        0,
          cupo:                 20,
          precio:               0,
          inscritos:            0,
          disponibles:          0,
          descripcion:          'Sin descripcion',
          usuario_creo:         0,
          usuario_modifico:     1,
          url_imagen:           '',
          deleted:              0,
          iderp:                0,
          editado:              0, 
          optimizado:           0,
          id_zona:              0
        }
        if(escuela == 2){
          addGrupoErp = await prospectosLMS.addGrupoFast( id_grupo_erp ).then(response=> response)
        }else{
          addGrupoErp = await prospectosLMS.addGrupoInbi( add_grupo_inbi ).then(response=> response)
        }
        id_grupo_erp = addGrupoErp.id_grupo
      }else{
        id_grupo_erp = existegrupoERP.id
      }

      // Validamos si el usuario existe
      if(escuela == 2){
        existeAlumnoErp = await prospectosLMS.getAlumnoFAST( folio ).then(response=> response)
      }else{
        existeAlumnoErp = await prospectosLMS.getAlumnoINBI( folio ).then(response=> response)
      }


      // si no existe hay que agregarlo
      if(!existeAlumnoErp){
        // Preparamos el usuario
        let add_usuario_fast = {
          nombre: req.body.nombre_completo,
          apellido_paterno: '',
          apellido_materno: '',
          usuario: folio,
          password: md5(folio),
          email:'',
          estado: 1,
          telefono:0,
          movil: 0,
          id_direccion:0,
          roll:1,
          id_tipo_usuario: 1,
          deleted: 0,
          iderp:0,
          contra_universal: md5('12345r2'),
          id_plantel: 2
        }

        // Agregamos al usuario
        if(escuela == 2){
          addUsuarioErp = await prospectosLMS.addUsuarioFast( add_usuario_fast ).then(response=>response)
        }else{
          addUsuarioErp = await prospectosLMS.addUsuarioInbi( add_usuario_fast ).then(response=>response)
        }
        

        // Preparamos el alumno
        if(addUsuarioErp){
          // Se prepara el paylod a agregar (alumno)
          let add_alumno_erp = {
            id_usuario: addUsuarioErp.id_usuario,
            matricula:  folio,
            id_plantel: 1,
            id_unidad_negocio: escuela,
            comentarios:'',
          }
          // Agregamos al alumno
          if(escuela == 2){
            const addAlumnoErp = await prospectosLMS.addAlumnoFast( add_alumno_erp ).then(response => response)
          }else{
            const addAlumnoErp = await prospectosLMS.addAlumnoInbi( add_alumno_erp ).then(response => response)
          }
          
          id_usuario_erp = addUsuarioErp.id_usuario
        }
      }else{
        id_usuario_erp = existeAlumnoErp.id
      }

      // Ahora ya por ultimo, le asignamos el grupo
      // ( id_ciclo_erp, id_grupo_erp, id_usuario_erp)
      if(escuela == 2){
        existeGrupoAlumnoERP = await prospectosLMS.getAlumnoGrupo( id_grupo_erp, id_usuario_erp ).then(response => response)
      }else{
        existeGrupoAlumnoERP = await prospectosLMS.getAlumnoGrupoInbi( id_grupo_erp, id_usuario_erp ).then(response => response)
      }
      
      if( !existeGrupoAlumnoERP ){
        let add_grupo_alumno_erp = {
          id_grupo: id_grupo_erp,
          id_curso: 1,
          id_alumno: id_usuario_erp,
          deleted:0,
          estatus:1
        }
        if(escuela == 2){
          const addGrupoAlumno = await prospectosLMS.addGrupoAlumnoFast( add_grupo_alumno_erp ).then(response=> response)
          // Y para finalizar agregaremos el evento 
          let eventoTxt = 'El prospecto esta en el LMS ' + folio
          const evento  = await prospectos.addEvento( eventoTxt, addProspecto.id, 1, addProspecto.usuario_creo ).then(response=> response)
          return res.send({folio, id: addProspecto.id });
        }else{
          const addGrupoAlumno = await prospectosLMS.addGrupoAlumnoInbi( add_grupo_alumno_erp ).then(response=> response)
          // Y para finalizar agregaremos el evento 
          let eventoTxt = 'El prospecto esta en el LMS ' + folio
          const evento  = await prospectos.addEvento( eventoTxt, addProspecto.id, 1, addProspecto.usuario_creo ).then(response=> response)
          return res.send({folio, id: addProspecto.id });
        }
      }else{
        return res.status(400).send({message: 'Ya existe'});
      }
    }else{
      // como si hay una sucursal de interes, se tiene que validar a quién le perteneces esa sucursal en si
      // Lo agregamos 
      const addProspecto  = await prospectos.addProspecto( req.body ).then(response=> response)
      // Y ahora actualizamos el folio del prospecto
      let folio = addProspecto.acronimo + addProspecto.id
      const updateProspecto  = await prospectos.updateProspectoFolio( addProspecto.id, folio ).then(response=> response)
      // Y para finalizar agregaremos el evento 
      let eventoTxt = 'El prospecto fue ingresado al sistema'
      const evento  = await prospectos.addEvento( eventoTxt, addProspecto.id, 1, addProspecto.usuario_creo ).then(response=> response)

      // Ahoraaaaaa se tiene que agregar al LMS
      // Paso por paso
      // Paso # 1 validar que el ciclo exista
      let existeCiclo          = null
      let addCiclo             = null
      let existegrupoERP       = null
      let addGrupoErp          = null
      let existeAlumnoErp      = null
      let existeGrupoAlumnoERP = null
      let addUsuarioErp        = null

      if(escuela == 2){
        existeCiclo = await prospectosLMS.getExisteCicloFast( id_ciclo ).then(response=> response)
      }else{
        existeCiclo = await prospectosLMS.getExisteCicloInbi( id_ciclo ).then(response=> response)
      }


      if( !existeCiclo ){
        const cicloERP = await prospectosLMS.getcicloERP( id_ciclo ).then(response => response)
        let add_ciclo = {
          nombre:               cicloERP.ciclo,
          estatus:              0,
          fecha_inicio:         cicloERP.fecha_inicio_ciclo.substr(0,10),
          fecha_fin:            cicloERP.fecha_fin_ciclo.substr(0,10),
          fecha_inicio_excluir: null,
          fecha_fin_excluir:    null,
          comentarios:          'sin comentarios',
          usuario_creo:         1,
          usuario_modifico:     1,
          deleted:              0,
          iderp:                cicloERP.id_ciclo
        }
        if(escuela == 2){
          addCiclo = await prospectosLMS.addCicloFast( add_ciclo ).then(response=> response)
        }else{
          addCiclo = await prospectosLMS.addCicloInbi( add_ciclo ).then(response=> response)
        }
        
        id_ciclo_erp = addCiclo.id_ciclo
      }else{
        id_ciclo_erp = existeCiclo.id
      }


      // Aquí ya tenemos ahora si el id_ciclo_erp lleno con el id_ciclo correspondiente
      // Paso #2 Buscar el grupo  de inducción de ese ciclo

      if(escuela == 2){
        existegrupoERP = await prospectosLMS.getGrupoFast( id_ciclo_erp ).then(response => response)
      }else{
        existegrupoERP = await prospectosLMS.getGrupoInbi( id_ciclo_erp ).then(response => response)
      }
      
      // No existe el grupo, hay que crearlo
      if( !existegrupoERP ){
        let add_grupo_payload = {
          nombre:               'INDUCCION - CICLO NA FE - 10_00 AM a 11_00 A-M - NIVEL 1',
          estatus:              0,
          id_unidad_negocio:    escuela,
          id_plantel:           14,
          id_ciclo:             id_ciclo_erp,
          id_curso:             1,
          id_horario:           0,
          id_salon:             1,
          id_nivel:             1,
          codigo_acceso:        0,
          cupo:                 20,
          precio:               0,
          inscritos:            0,
          disponibles:          0,
          descripcion:          'Sin descripcion',
          usuario_creo:         0,
          usuario_modifico:     1,
          url_imagen:           '',
          deleted:              0,
          iderp:                0,
          editado:              0, 
          optimizado:           0,
          id_zona:              0
        }
        if(escuela == 2){
          addGrupoErp = await prospectosLMS.addGrupoFast( add_grupo_payload ).then(response=> response)
        }else{
          addGrupoErp = await prospectosLMS.addGrupoInbi( add_grupo_payload ).then(response=> response)
        }
        id_grupo_erp = addGrupoErp.id_grupo
      }else{
        id_grupo_erp = existegrupoERP.id
      }


      if(escuela == 2){
        existeAlumnoErp = await prospectosLMS.getAlumnoFAST( req.body.folio ).then(response=> response)
      }else{
        existeAlumnoErp = await prospectosLMS.getAlumnoINBI( req.body.folio ).then(response=> response)
      }

      // si no existe hay que agregarlo
      if(!existeAlumnoErp){
        // Preparamos el usuario
        let add_usuario_fast = {
          nombre: req.body.nombre_completo,
          apellido_paterno: '',
          apellido_materno: '',
          usuario: req.body.folio,
          password: md5(req.body.folio),
          email:'',
          estado: 1,
          telefono:0,
          movil: 0,
          id_direccion:0,
          roll:1,
          id_tipo_usuario: 1,
          deleted: 0,
          iderp:0,
          contra_universal: md5('12345r2'),
          id_plantel: 2
        }

        // Agregamos al usuario
        if(escuela == 2){
          addUsuarioErp = await prospectosLMS.addUsuarioFast( add_usuario_fast ).then(response=>response)
        }else{
          addUsuarioErp = await prospectosLMS.addUsuarioInbi( add_usuario_fast ).then(response=>response)
        }
        

        // Preparamos el alumno
        if(addUsuarioErp){
          // Se prepara el paylod a agregar (alumno)
          let add_alumno_erp = {
            id_usuario: addUsuarioErp.id_usuario,
            matricula:  req.body.folio,
            id_plantel: 1,
            id_unidad_negocio: escuela,
            comentarios:'',
          }
          // Agregamos al alumno
          if(escuela == 2){
            const addAlumnoErp = await prospectosLMS.addAlumnoFast( add_alumno_erp ).then(response => response)
          }else{
            const addAlumnoErp = await prospectosLMS.addAlumnoInbi( add_alumno_erp ).then(response => response)
          }
          
          id_usuario_erp = addUsuarioErp.id_usuario
        }
      }else{
        id_usuario_erp = existeAlumnoErp.id
      }

      // Ahora ya por ultimo, le asignamos el grupo
      // ( id_ciclo_erp, id_grupo_erp, id_usuario_erp)
      if(escuela == 2){
        existeGrupoAlumnoERP = await prospectosLMS.getAlumnoGrupo( id_grupo_erp, id_usuario_erp ).then(response => response)
      }else{
        existeGrupoAlumnoERP = await prospectosLMS.getAlumnoGrupoInbi( id_grupo_erp, id_usuario_erp ).then(response => response)
      }
      
      if( !existeGrupoAlumnoERP ){
        let add_grupo_alumno_erp = {
          id_grupo: id_grupo_erp,
          id_curso: 1,
          id_alumno: id_usuario_erp,
          deleted:0,
          estatus:1
        }
        if(escuela == 2){
          const addGrupoAlumno = await prospectosLMS.addGrupoAlumnoFast( add_grupo_alumno_erp ).then(response=> response)
          // Y para finalizar agregaremos el evento 
          let eventoTxt = 'El prospecto esta en el LMS ' + req.body.folio
          const evento  = await prospectos.addEvento( eventoTxt, addProspecto.id, 1, addProspecto.usuario_creo ).then(response=> response)
          return res.send({folio, id: addProspecto.id });
        }else{
          const addGrupoAlumno = await prospectosLMS.addGrupoAlumnoInbi( add_grupo_alumno_erp ).then(response=> response)
          // Y para finalizar agregaremos el evento 
          let eventoTxt = 'El prospecto esta en el LMS ' + req.body.folio
          const evento  = await prospectos.addEvento( eventoTxt, addProspecto.id, 1, addProspecto.usuario_creo ).then(response=> response)
          return res.send({folio, id: addProspecto.id });
        }
      }

      return res.send({folio, id: addProspecto.id });
    }

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};

exports.getProspectos = async(req, res) => {
  try {
    // Agregar el prospecto
  	const getProspectos  = await prospectos.getProspectos( req.body ).then(response=> response)
  	const cursos         = await prospectos.getCursosActivos( ).then(response=> response)
  	const ciclos         = await prospectos.getCiclosActivos( ).then(response=> response)
  	const usuariosERP    = await erpviejo.getUsuariosERP( ).then(response=> response)

  	for(const i in getProspectos){
  		const { curso_interes, ciclo_interes, usuario_asignado,usuario_creo } = getProspectos[i]

  		const curso    = cursos.find(el=> el.id_curso == curso_interes)
  		const ciclo    = ciclos.find(el=> el.id_ciclo == ciclo_interes)
      const vend     = usuariosERP.find(el=> el.id_usuario == usuario_asignado)
  		const recluta  = usuariosERP.find(el=> el.id_usuario == usuario_creo)

  		getProspectos[i] = {
  			...getProspectos[i],
  			curso:     curso    ? curso.curso : '' ,
  			ciclo:     ciclo    ? ciclo.ciclo : '' ,
        vendedora: vend     ? vend.nombre_completo : '',
  			recluta:   recluta  ? recluta.nombre_completo : '' 
  		}
  	}

    // Agregar quien y aquien se le asigno

		res.send(getProspectos);

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.getProspecto = async(req, res) => {
  try {
  	const { id } = await req.params
    // Agregar el prospecto
  	const getProspecto   = await prospectos.getProspecto( id ).then(response=> response)
  	const cursos         = await prospectos.getCursosActivos( ).then(response=> response)
  	const ciclos         = await prospectos.getCiclosActivos( ).then(response=> response)
  	const usuariosERP    = await erpviejo.getUsuariosERP( ).then(response=> response)

  	for(const i in getProspecto){
  		const { curso_interes, ciclo_interes, usuario_asignado } = getProspecto

  		const curso = cursos.find(el=> el.id_curso == curso_interes)
  		const ciclo = ciclos.find(el=> el.id_ciclo == ciclo_interes)
  		const vend  = usuariosERP.find(el=> el.id_usuario == usuario_asignado)

  		getProspecto[i] = {
  			...getProspecto[i],
  			curso:     curso ? curso.curso : '' ,
  			ciclo:     ciclo ? ciclo.ciclo : '' ,
  			vendedora: vend  ? vend.nombre_completo : '' 
  		}
  	}

		res.send(getProspecto);

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.addComentario = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  prospectos.addComentario(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el comentario"
      })
    else res.status(200).send({ message: `El comentario se ha creado correctamente` });
  })
};

exports.updateProspectoEtapa = async(req, res) => {
  try {
    const { id }                 = req.params
    const { idetapa, etapa, usuario_creo } = req.body

    const updateEtapaProspecto  = await prospectos.updateEtapaProspecto( id, idetapa ).then(response=> response)

    // Agregar un evento
  	let eventoTxt = 'El prospecto cambió a: ' + etapa
  	const evento  = await prospectos.addEvento( eventoTxt, id, 2, usuario_creo ).then(response=> response)

  	res.send({message: 'Actualización correcta'});

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.updateProspecto = async(req, res) => {
  try {
    const { id }                      = req.params
    const { usuario_creo, induccion } = req.body
    let induccionViejo                = null

    const getProspecto   = await prospectos.getProspecto( id ).then(response=> response)
    if(getProspecto.length > 0){
      induccionViejo  = getProspecto[0].induccion
      if(induccionViejo != induccion && induccion == 1){
        let eventoTxt = 'Usuario en inducción'
        const evento  = await prospectos.addEvento( eventoTxt, id, 6, usuario_creo ).then(response=> response)
      }
    }

    const updateProspecto  = await prospectos.updateProspecto( id, req.body ).then(response=> response)
    let eventoTxt          = 'Actualización de datos'
	  const evento           = await prospectos.addEvento( eventoTxt, id, 1, usuario_creo ).then(response=> response)

    res.send({message: 'Actualización correcta'});

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.getTareasUsuario = async(req, res) => {
  try {
  	const { id }            = req.params
    // Agregar el prospecto
  	const getTareasUsuario  = await prospectos.getTareasUsuario( id ).then(response=> response)

		res.send(getTareasUsuario);

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.addTareaProspecto = async(req, res) => {
  try {
    const { idprospectos, tarea, idusuarioerp, dia, hora } = req.body

    // Verificar si se puede programar una tarea
    const existeTarea = await prospectos.existeTarea( idusuarioerp, dia, hora ).then(response=> response)
    if(existeTarea.length > 0){
    	res.status(500).send({message:'Se empalma la tarea'})
    }else{
	    const addTareaProspecto  = await prospectos.addTareaProspecto( req.body ).then(response=> response)
	    
	    let eventoTxt = 'Se genero la tarea: ' + tarea
		  const evento  = await prospectos.addEvento( eventoTxt, idprospectos, 3, idusuarioerp ).then(response=> response)

	  	res.send({message: 'Actualización correcta'});
    }

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.updateTareaProspecto = async(req, res) => {
  try {
    const { idtareas_prospectos, notaAnterior, idprospectos, idusuarioerp } = req.body

    const actualizarTareaProspecto  = await prospectos.actualizarTareaProspecto( req.body, idtareas_prospectos ).then(response=> response)

    let eventoTxt = 'Se reprogramo la tarea: ' + notaAnterior.motivo
	  const evento  = await prospectos.addEvento( eventoTxt, idprospectos, 3, idusuarioerp ).then(response=> response)

  	res.send({message: 'Actualización correcta'});

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.deletedTareaProspecto = async(req, res) => {
  try {
    const { id } = req.params

    const tarea = await prospectos.getTarea( id ).then(response => response)
    const eliminarTarea  = await prospectos.eliminarTarea( id ).then(response=> response)

    
    let eventoTxt = 'Se elimino la tarea: ' + tarea.tarea
	  const evento  = await prospectos.addEvento( eventoTxt, tarea.idprospectos, 8, tarea.idusuarioerp ).then(response=> response)

  	res.send({message: 'Actualización correcta'});

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.updateTareaCheck = async(req, res) => {
  try {
    const { id } = req.params

    if(id > 0){
      const tarea            = await prospectos.getTarea( id ).then(response => response)
      const teareaRealizada  = await prospectos.teareaRealizada( id ).then(response=> response)      
      let eventoTxt = 'Se realizó la tarea: ' + tarea.tarea
  	  const evento  = await prospectos.addEvento( eventoTxt, tarea.idprospectos, 3, tarea.idusuarioerp ).then(response=> response)
    }

  	res.send({message: 'Actualización correcta'});

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.addUsuarioLlamada = async(req, res) => {
  try {
    const { idusuarioerp, estatus, idprospectos } = req.body

		// vemos si el usuario tiene una llamada registrada, si no es así, la agregamos  
    const existeLlamada = await prospectos.existeLlamada( idusuarioerp ).then(response=> response)
    // Verificamos si hay una llamada
    if(existeLlamada.length > 0){
    	// si hay, la actualizamos
    	const updateEstatusLlamada = await prospectos.updateEstatusLlamada( idusuarioerp, estatus ).then(response=> response)
    	res.send(updateEstatusLlamada);
    }else{
      // No hay, la agregamos
	    const addLlamada    = await prospectos.addLlamada( idusuarioerp, estatus, idprospectos ).then(response=> response)
      let eventoTxt = 'Se realizó una llamada'
      const evento  = await prospectos.addEvento( eventoTxt, idprospectos, 7, idusuarioerp ).then(response=> response)
	  	res.send(addLlamada);
    }

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.getEstatusllamada = async(req, res) => {
  try {
    const { id } = req.params

		// vemos si el usuario tiene una llamada registrada, si no es así, la agregamos  
    const existeLlamada = await prospectos.existeLlamada( id ).then(response=> response)
    // Verificamos si hay una llamada
    if(existeLlamada.length > 0){
    	res.send(existeLlamada[0]);
    }else{
	  	res.send({message:'No existe llamada activa'});
    }

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.addPuntosLlamada = async(req, res) => {
  try {
    const { idprospectos, idusuarioerp } = req.body

    // Validamos si el prospecto ya tiene la información de su llamada
    const existePuntoLlamada = await prospectos.existePuntoLlamada( idprospectos ).then(response=> response)
    // Verificamos si hay una llamada
    if(existePuntoLlamada.length > 0){
      // si hay, la actualizamos
      const updatePuntosLlamada = await prospectos.updatePuntosLlamada( existePuntoLlamada[0].idpuntos_llamada ,req.body ).then(response=> response)
      let eventoTxt = 'Se actualizo el perfil del prospecto'
      const evento  = await prospectos.addEvento( eventoTxt, idprospectos, 1, idusuarioerp ).then(response=> response)
      res.send({message: 'Actualización de datos'});
    }else{
      // No hay, la agregamos
      const addPuntosLlamada = await prospectos.addPuntosLlamada( req.body ).then(response=> response)
      let eventoTxt = 'Se creo el perfil del prospecto'
      const evento  = await prospectos.addEvento( eventoTxt, idprospectos, 13, idusuarioerp ).then(response=> response)
      res.send({message: 'Datos grabados correctamente'});
    }

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.getDatosProspecto = async(req, res) => {
  try {
    const { id } = await req.params

    const datosGenerales             = await prospectos.getProspecto( id ).then(response=> response)

    // Consultar las tareas ya realizadas
    // const tareas_realizadas          = await prospectos.getTareasFinalizdasProspecto(id).then(response=> response)
    const tareas_realizadas = []

    // Buscamos los comentarios de los prospectos
    const comentarios  = await prospectos.getComentarios( id ).then(response=> response)
    const usuariosERP  = await erpviejo.getUsuariosERP( ).then(response=> response)

    // Recorresmos los comentarios para poder agregar el nommbre de la persona que hizo el comentario
    for(const i in comentarios){
      const { idusuarioerp } = comentarios[i]
      // Lo buscamos
      const usuario = usuariosERP.find(el=> el.id_usuario == idusuarioerp )
      // Si lo encuentra, agrega el nombre
      comentarios[i]['nombre_completo'] = usuario ? usuario.nombre_completo : ''
    }

    // Ahora buscamos las tareas programadas del prospecto
    const tareas  = await prospectos.getTareasProspectos( id ).then(response=> response)
    // const tareas = []

    // Sacamos las tareas del prospecto
    // const eventos  = await prospectos.getEventosProspectos( id ).then(response=> response)

    const eventos = []

    // Bandera para abrir los mensajes de whatsApp
    for(const i in eventos){
      eventos[i]['abrirMensaje'] = false
    }

    // Puntos de la llamada
    const llamada = await prospectos.existePuntoLlamada( id ).then(response=> response)
    let datosLlamada = llamada[0]
    let ciclo = ''
    let curso = ''

    if(llamada.length > 0){
      // Buscamos el ciclo y buscamos el curso
      curso = await prospectos.getCursosActivosId( datosLlamada.idcurso ).then(response=> response)
      ciclo = await prospectos.getCiclosActivosId( datosLlamada.idciclo ).then(response=> response)
      // Como si existe, solo es questión de llenarlo
      datosLlamada = {
        ...datosLlamada,
        curso,
        ciclo
      }
    }else{
      datosLlamada = null
    }

    // Sacamos las llamadas realizadas
    // const llamadas     = await prospectos.getEstatusllamadaList( id ).then(response=> response)
    const llamadas = []
    // Sacamos las notas de esa llamada
    const notas        = await prospectos.getNotasLlamada( id ).then(response=> response)
    
    // Recorresmos los notas para poder agregar el nommbre de la persona que hizo el comentario
    for(const i in llamadas){
      const { idusuarioerp, idestatus_llamada } = llamadas[i]
      // Lo buscamos
      const usuario = usuariosERP.find(el=> el.id_usuario == idusuarioerp)
      // Si lo encuentra, agrega el nombre
      const notasFilter = notas.filter(el=>{
        return el.idestatus_llamada == idestatus_llamada
      })

      let usuarioNota = ''
      for(const j in notasFilter){
        usuarioNota = usuariosERP.find(el=> el.id_usuario == notasFilter[j].idusuarioerp)
        notasFilter[j] = {
          ...notasFilter[j],
          nombre_completo: usuarioNota ? usuarioNota.nombre_completo : ''
        }
      }

      llamadas[i] = {
        ...llamadas[i],
        nombre_completo: usuario ? usuario.nombre_completo : '',
        notas: notasFilter
      }
    }

    // Generar el timeline
    let timeLineTareas = eventos.filter(el=> { return el.idtipo_evento == 3})
    // modificaremos el resultado
    for(const i in timeLineTareas){
      timeLineTareas[i] = {
        idtipo_evento: 4,
        evento: timeLineTareas[i].evento,
        fecha_creacion: timeLineTareas[i].fecha_creacion,
        icono:'mdi-clipboard-check',
        color:'red'
      }
    }

    // Generar el timeline
    let timeLineWhats = eventos.filter(el=> { return el.idtipo_evento == 5})
    // modificaremos el resultado
    for(const i in timeLineWhats){
      timeLineWhats[i] = {
        idtipo_evento: 1,
        evento: timeLineWhats[i].evento,
        fecha_creacion: timeLineWhats[i].fecha_creacion,
        icono:'mdi-whatsapp',
        color:'green'
      }
    }

    // Generar el timeline
    let timeLineIndu = eventos.filter(el=> { return el.idtipo_evento == 6})
    // modificaremos el resultado
    for(const i in timeLineIndu){
      timeLineIndu[i] = {
        idtipo_evento: 2,
        evento: timeLineIndu[i].evento,
        fecha_creacion: timeLineIndu[i].fecha_creacion,
        icono:'mdi-emoticon-happy',
        color:'pink'
      }
    }

    // Intento de llamada normal 
    let timeLineIntentoLlamada = eventos.filter(el=> { return el.idtipo_evento == 9})
    // modificaremos el resultado
    for(const i in timeLineIntentoLlamada){
      timeLineIntentoLlamada[i] = {
        idtipo_evento: 5,
        evento: timeLineIntentoLlamada[i].evento,
        fecha_creacion: timeLineIntentoLlamada[i].fecha_creacion,
        icono:'mdi-phone-hangup',
        color:'red'
      }
    }

    // Intento de llamada whatsApp 
    let timeLineItentoWhats = eventos.filter(el=> { return el.idtipo_evento == 10})
    // modificaremos el resultado
    for(const i in timeLineItentoWhats){
      timeLineItentoWhats[i] = {
        idtipo_evento: 6,
        evento: timeLineItentoWhats[i].evento,
        fecha_creacion: timeLineItentoWhats[i].fecha_creacion,
        icono:'mdi-phone-hangup',
        color:'#64DD17'
      }
    }

    // Intento de llamada normal 
    let timeLineLlamadaRealizada = eventos.filter(el=> { return el.idtipo_evento == 11})
    // modificaremos el resultado
    for(const i in timeLineLlamadaRealizada){
      timeLineLlamadaRealizada[i] = {
        idtipo_evento: 7,
        evento: timeLineLlamadaRealizada[i].evento,
        fecha_creacion: timeLineLlamadaRealizada[i].fecha_creacion,
        icono:'mdi-phone',
        color:'warning'
      }
    }

    // Intento de llamada normal 
    let timeLineLlamadaWathsApp = eventos.filter(el=> { return el.idtipo_evento == 12})
    // modificaremos el resultado
    for(const i in timeLineLlamadaWathsApp){
      timeLineLlamadaWathsApp[i] = {
        idtipo_evento: 8,
        evento: timeLineLlamadaWathsApp[i].evento,
        fecha_creacion: timeLineLlamadaWathsApp[i].fecha_creacion,
        icono:'mdi-phone',
        color:'#64DD17'
      }
    }

    // Intento de llamada normal 
    let timeLinePerfil = eventos.filter(el=> { return el.idtipo_evento == 13})
    // modificaremos el resultado
    for(const i in timeLinePerfil){
      timeLinePerfil[i] = {
        idtipo_evento: 8,
        evento: timeLinePerfil[i].evento,
        fecha_creacion: timeLinePerfil[i].fecha_creacion,
        icono:'mdi-account-check',
        color:'#311B92'
      }
    }

    // Generar el timeline
    // modificaremos el resultado
    // let timeLineLlamada = llamadas
    // for(const i in timeLineLlamada){
    //   timeLineLlamada[i] = {
    //     ...llamadas[i],
    //     idtipo_evento: 3,
    //     evento: 'Llamada',
    //     fecha_creacion: timeLineLlamada[i].fecha_creacion,
    //     icono:'mdi-phone',
    //     color:'warning'
    //   }
    // }

    let timeLine = timeLineWhats.concat(timeLineIndu).concat(timeLineTareas).concat(timeLineIntentoLlamada).concat(timeLineItentoWhats).concat(timeLineLlamadaRealizada).concat(timeLineLlamadaWathsApp).concat(timeLinePerfil)
    
    // Ordenarlo por fecha
    timeLine.sort(function (a, b) {
      if (a.fecha_creacion > b.fecha_creacion) {
        return 1;
      }
      if (a.fecha_creacion < b.fecha_creacion) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });

    // recuperar los familiares
    const getFamiliares = []
    // const getFamiliares = await familiares.getFamiliares( id ).then(response=> response)

    // Reponder
    res.send({comentarios, tareas, eventos, datosLlamada, llamadas, datosGenerales, timeLine, getFamiliares, tareas_realizadas });

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.getNotasLlamada = async(req, res) => {
  try {
    const { id } = await req.params
    // Buscamos los notas de los prospectos
    const notas  = await prospectos.getNotasLlamada( id ).then(response=> response)
    const usuariosERP  = await erpviejo.getUsuariosERP( ).then(response=> response)

    // Recorresmos los notas para poder agregar el nommbre de la persona que hizo el comentario
    for(const i in notas){
      const { idusuarioerp } = notas[i]
      // Lo buscamos
      const usuario = usuariosERP.find(el=> el.id_usuario == idusuarioerp)
      // Si lo encuentra, agrega el nombre
      notas[i] = {
        ...notas[i],
        nombre_completo: usuario.nombre_completo
      }
    }

    res.send(notas);
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.addNotaLlamada = async(req, res) => {
  try {
    // Agregamos la nota
    const notas  = await prospectos.addNotaLlamada( req.body ).then(response=> response)

    res.send(notas);
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.getEstatusllamadaList = async(req, res) => {
  try {
    const { id } = await req.params
    // Sacamos las llamadas de ese prospecto
    const llamadas     = await prospectos.getEstatusllamadaList( id ).then(response=> response)
    const usuariosERP  = await erpviejo.getUsuariosERP( ).then(response=> response)

    // Recorresmos los notas para poder agregar el nommbre de la persona que hizo el comentario
    for(const i in llamadas){
      const { idusuarioerp } = llamadas[i]
      // Lo buscamos
      const usuario = usuariosERP.find(el=> el.id_usuario == idusuarioerp)
      // Si lo encuentra, agrega el nombre
      llamadas[i] = {
        ...llamadas[i],
        nombre_completo: usuario.nombre_completo
      }
    }

    res.send(llamadas);
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.reasignarProspecto = async(req, res) => {
  try {
    // Agregamos la nota
    const reasignado  = await prospectos.reasignarProspecto( req.body ).then(response=> response)

    let eventoTxt = 'Usuario reasignado'
    const evento  = await prospectos.addEvento( eventoTxt, req.body.idprospectos, 4, req.body.idusuarioerp ).then(response=> response)

    res.send(reasignado);
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.finalizarProspecto = async(req, res) => {
  try {
    const { idprospectos, matricula, idgrupo, idusuarioerp, codigo_clase } = req.body
    // // Agregamos la nota
    const { id_alumno } = await prospectos.getAlumnoMatricula( matricula ).then(response=> response)

    // Validamos que no exista un registro anterior con ese alumno
    const existeRegistro = await prospectos.getRegistroProspecto( id_alumno ).then(response=> response)
    if(existeRegistro){
      res.status(500).send({message:'El prospecto ya cuenta con un registro'})
      return
    }

    // Validamos que el usuario este asignado a ese grupo
    existeGrupoAlumno = await prospectos.getGrupoAlumno(id_alumno, idgrupo).then(response=> response)
    if(!existeGrupoAlumno){
      res.status(500).send({message:'El prospecto no se encuentra en ese grupo'})
      return
    }

    // Hasta este punto ya paso todas las barreras, 

    // 1-. Actualizar el prospecto
    // 2-. Agregar el evento
    // finalizo =  1, fecha_finalizo = NOW()
    // Evento

    const finalizar = await prospectos.updateProspectoFinalizo(idprospectos, id_alumno, idgrupo, codigo_clase).then(response=> response)
    let eventoTxt = 'Prospecto finalizo'
    const evento  = await prospectos.addEvento( eventoTxt, idprospectos, 1, idusuarioerp ).then(response=> response)

    res.send({id_alumno});
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.finalizarProspectoRechazo = async(req, res) => {
  try {
    const { idprospectos, notaRechazo, idusuarioerp } = req.body

    // 1-. Actualizar el prospecto
    // 2-. Agregar el evento
    // finalizo =  1, fecha_finalizo = NOW()
    // Evento

    const finalizar = await prospectos.updateProspectoFinalizoRechazo(idprospectos, notaRechazo).then(response=> response)
    let eventoTxt = 'Prospecto finalizo, no se cumplió meta'
    const evento  = await prospectos.addEvento( eventoTxt, idprospectos, 1, idusuarioerp ).then(response=> response)

    // Consultamos las tareas, y las vamos cerrando
    const tareas  = await prospectos.getTareasProspectos( idprospectos ).then(response=> response)
    for(const i in tareas){
      const { idtareas_prospectos } = tareas[i]
      const eliminarTarea  = await prospectos.eliminarTarea( idtareas_prospectos ).then(response=> response)
    }

    res.send({idprospectos});
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

//Angel Rodriguez
exports.activarProspectoCerrado = async(req, res) => {
  try {
    const { idprospectos, idusuarioerp } = req.body

    // 1-. Actualizar el prospecto
    // 2-. Agregar el evento
    // finalizo =  0, fecha_finalizo = ""
    // Evento

    const activar = await prospectos.updateProspectoCerrado(idprospectos).then(response=> response)
    let eventoTxt = 'Prospecto reactivado'
    const evento  = await prospectos.addEvento( eventoTxt, idprospectos, 1, idusuarioerp ).then(response=> response)

    res.send({idprospectos});
  } catch (error) {
    res.status(500).send({ message : error ? error.message: "error en el servidor" })
  }
};

exports.activarProspectoCerradoRecluta = async(req, res) => {
  try {
    const { idformulario, idusuarioerp } = req.body

    // 1-. Actualizar el prospecto
    // 2-. Agregar el evento
    // finalizo =  0, fecha_finalizo = ""
    // Evento

    const activar = await prospectos.activarProspectoCerradoRecluta(idformulario).then(response=> response)
    let eventoTxt = 'Prospecto reactivado'
    const evento  = await prospectos.addEvento( eventoTxt, idformulario, 1, idusuarioerp ).then(response=> response)

    res.send({idformulario});
  } catch (error) {
    res.status(500).send({ message : error ? error.message: "error en el servidor" })
  }
};

exports.getGrupos = async(req, res) => {
  try {
    const { id } = req.body
    // const escuela = id == 2 ? ` AND ciclo LIKE '%FE%'`: ` AND ciclo NOT LIKE '%FE%'`
    const escuela = ''

    const grupos = await prospectos.getGrupos( escuela ).then(response=> response)
    res.send(grupos);
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.addProspectoWhatsApp = async(req, res) => {
  try {
    // Agregamos la nota
    const { idusuarioerp, idprospectos, nota } = req.body
    let eventoTxt = 'Nota de whatsApp ' + nota
    const evento  = await prospectos.addEvento( eventoTxt, idprospectos, 5, idusuarioerp ).then(response=> response)

    res.send({message: 'Se agrego la nota con el mensaje de whatsApp'});
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

// MARKETING
exports.getRemarketing = async(req, res) => {
  try {
    const { id } = req.params
    const remarketing = await prospectos.getRemarketing( id ).then(response=> response)
    res.send(remarketing);
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.addImagenCampania = async(req, res) => {
  try {
    // Recupero el archivo
    const { file } = req.files
    // Separo el nombre
    const nombreCortado = file.name.split('.')
    // Saco la extensión del archivo
    const extension = nombreCortado[ nombreCortado.length - 1 ]
    // Validar extensio
    const extensiones = ['png','jpg','jpeg','gif','webp']
    // Valido que la extensión sea la correcta o valida
    if(extensiones.includes( extension )){
      // Genero el nombre temporal 
      const nombreTemp = uuidv4() + '.' + extension
      // Agrego los datos de la imagen de la campaña a la base de datos
      const { id } = await prospectos.addImagenCampania( nombreTemp, extension ).then(response=> response)
      // Subo el archivo al servidor
      file.mv(`./../../imagenes-campanias/${ nombreTemp }`, err => {
        // Valido los errores
        if (err) {
          res.status(500).send({message:err})
        }else{
          res.send({nombreTemp})
        }
      })
      
    }else{
      // Reporto que la extensión no es valida
      res.status(400).send({message: `La extensión '${ extension }' no es valida: ${ extensiones }`})
    }
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.enviarCampania = async (req, res) => {
  const { titulo, nombreArchivo, escuela } = req.body.correo
  const user = escuela == 1 ? 'staff.inbischool@gmail.com':'staff.fastenglish@gmail.com'
  const pass = escuela == 1 ? 'sgjywlyabjawvlzv':'jzxmdixwyauwxfbf'

  // 0bw%7U8q

  let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure:true,
    auth: {
      user,
      pass
    }
  });

  let from = escuela == 1 ? 'INBI SCHOOL' : 'FAST ENGLISH'

  // transporter.verify().then(() =>{
  //   // console.log('Ready for send')
  // })


  for(const i in req.body.correos){
    const { correo } = req.body.correos[i]
    if(correo != ''){
      
      var mailOptions = {
        from: `${ from } <info@testsnode.com>`,
        to:  `${ correo }`,
        subject: titulo,
        text: 'That was easy!',
        html: `<!DOCTYPE html><html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml"><head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"><!--[if !mso]><!-->
          <meta http-equiv="X-UA-Compatible" content="IE=Edge"><!--<![endif]-->

          <style type="text/css">
            body, p, div {
              font-family: arial;
              font-size: 14px;
            }
            body {
              color: #000000;
            }
            body a {
              color: #1188E6;
              text-decoration: none;
            }
            p { margin: 0; padding: 0; }
            table.wrapper {
              width:100% !important;
              table-layout: fixed;
              -webkit-font-smoothing: antialiased;
              -webkit-text-size-adjust: 100%;
              -moz-text-size-adjust: 100%;
              -ms-text-size-adjust: 100%;
            }
            img.max-width {
              max-width: 100% !important;
            }
            .column.of-2 {
              width: 50%;
            }
            .column.of-3 {
              width: 33.333%;
            }
            .column.of-4 {
              width: 25%;
            }
            @media screen and (max-width:480px) {
              .preheader .rightColumnContent,
              .footer .rightColumnContent {
                  text-align: left !important;
              }
              .preheader .rightColumnContent div,
              .preheader .rightColumnContent span,
              .footer .rightColumnContent div,
              .footer .rightColumnContent span {
                text-align: left !important;
              }
              .preheader .rightColumnContent,
              .preheader .leftColumnContent {
                font-size: 80% !important;
                padding: 5px 0;
              }
              table.wrapper-mobile {
                width: 100% !important;
                table-layout: fixed;
              }
              img.max-width {
                height: auto !important;
                max-width: 480px !important;
              }
              a.bulletproof-button {
                display: block !important;
                width: auto !important;
                font-size: 80%;
                padding-left: 0 !important;
                padding-right: 0 !important;
              }
              .columns {
                width: 100% !important;
              }
              .column {
                display: block !important;
                width: 100% !important;
                padding-left: 0 !important;
                padding-right: 0 !important;
                margin-left: 0 !important;
                margin-right: 0 !important;
              }
              .total_spacer {
                padding:0px 0px 0px 0px;
              }
            }
          </style>
          </head>
          <body>
          <center class="wrapper" data-link-color="#1188E6" data-body-style="font-size: 14px; font-family: arial; color: #000000; background-color: #ebebeb;">
            <div class="webkit">
              <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#ebebeb">
                <tbody><tr>
                  <td valign="top" bgcolor="#ebebeb" width="100%">
                    <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
                      <tbody><tr>
                        <td width="100%">
                          <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                              <td width="600">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width:600px;" align="center">
                                  <tbody>
                                    <tr>
                                      <td role="modules-container" style="padding: 0px 0px 0px 0px; color: #000000; text-align: left;" bgcolor="#ffffff" width="100%" align="left">
                                        <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                          <tbody>
                                            <tr>
                                              <td style="" valign="top" align="center">
                                                <img  border="0" style="display:block;color:#000000;max-width: 100%;max-height:600px !important;" src="https://web-back-inbi.club/kpi/imagenes-campanias/${nombreArchivo}" alt="" >
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>


            </body>
          </html>
        `
      };

      await transporter.sendMail(mailOptions, (error, info) =>{
        if (error) {
          // console.log(error);
          return
        } else {
          // console.log('Email sent: ' + info.response);
          return
        }
      });
    }
  }

  res.send({message: 'correo enviado'})
};

// INSCRITOS
exports.getProspectosInscritos = async(req, res) => {
  try {
    // Scamos la escuela
    const { escuela, idciclo, fechaini, fechafin } = req.body

    kpi.habilitarGroupInbi()
    
    // Traer el nombre de la vendedora
    const usuariosERP  = await erpviejo.getUsuariosERP( ).then(response=> response)
    // Consultamos las vendedoras
    const vendedoras = await prospectos.getVendedoras( escuela ).then(response => response)
    // Recorremos las vendedoras para sacar el nombre
    for(const i in vendedoras){
      const { iderp } =  vendedoras[i]
      // Buscamos la vendedora
      const vendedora = usuariosERP.find(el=> el.id_usuario == iderp)
      // Verificamos que el nombre exista
      if(vendedora){
        vendedoras[i] = {
          ...vendedoras[i],
          vendedora: vendedora.nombre_completo,
        }
      }else{
        vendedoras[i] = {
          ...vendedoras[i],
          vendedora: '',
        }
      }
    }

    // Traemos los inscritos y mandamos como parametro el 2 ya que 2 representa la escuela de fast
    const inscritos = await prospectos.getProspectosInscritos( escuela, fechaini, fechafin ).then(response => response)

    const eventos   = await prospectos.getEventosPorVendedora( fechaini, fechafin ).then(response => response)

    // Sacamos los grupos de los inscritos
    gruposProspecto = []
    for(const i in inscritos){
      gruposProspecto.push(inscritos[i].idgrupo)
    }

    if(gruposProspecto.length == 0){ gruposProspecto = 0 }

    // Ahora consultamos los grupos y mandamos como parametro los grupos
    const grupos = await prospectos.getGruposProspectos( gruposProspecto ).then(response=>response)
    // Concatenamos o añadimos el ciclo y el nombre del ciclo
    for(const i in inscritos){
      const { idgrupo } =  inscritos[i]
      // Buscamos la vendedora
      const ciclo = grupos.find(el=> el.id_grupo == idgrupo)
      // Validamos que exista la ciclo
      if(ciclo){
        inscritos[i] = {
          ...inscritos[i],
          idciclo: ciclo.id_ciclo
        }
      }else{
        inscritos[i] = {
          ...inscritos[i],
          idciclo: ''
        }
      }
    }

    // Vamos a calcular cuantas 
    for(const i in vendedoras){
      // Sacamos el iderp de la vendedora
      const { iderp } = vendedoras[i]
      // Vamos a filtrar los inscritos por vendedora
      const inscritosVendedora = inscritos.filter(el=> { return el.usuario_asignado == iderp && el.idciclo == idciclo})
      const eventosVendedra    = eventos.filter( el=> { return el.usuario_registro == iderp })

      // Guardamos la cantidad de inscritos por vendedora
      vendedoras[i] = {
        ...vendedoras[i],
        total: inscritosVendedora.length,
        inscritos: inscritosVendedora,
        eventosVendedra: eventosVendedra ? eventosVendedra.length : 0
      }
    }


    // Scar el total de insicritos
    let total = 0
    // Vamos a calcular cuantas 
    for(const i in vendedoras){
      total += vendedoras[i].total
    }

    res.send({total, vendedoras});
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

// info escuela
exports.addInfoEscuelaProspecto = async(req, res) => {
  try {
    // Scamos la escuela
    const { idprospectos, iderp, informacion } = req.body
    // Validamos que no exista información
    const existeInfo = await prospectos.getInfoEscuelaProspecto (idprospectos).then(response=>response)
    if(existeInfo){
      // Actualzamos
      const update   = await prospectos.updateInfoEscuelaProspecto (idprospectos, iderp, informacion).then(response=>response)
      res.send({message: 'Información guardada correctamente'});
    }else{
      // Agreamos
      const add      = await prospectos.addInfoEscuelaProspecto (idprospectos, iderp, informacion).then(response=>response)
      res.send(add);
    }
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.getNuevosProspectos = async(req, res) => {
  try {
    const { id } = req.params
    const { nuevos } = await prospectos.getNuevosProspectos( id ).then(response=> response)
    res.send({ nuevos });
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

// Nuevas rutas y controladores
exports.getProspectosVendedora = async(req, res) => {
  try {
    const { tipo, id } = req.params
    let parametro = ''

    if(tipo == 1){ // vendedora
      parametro = ` AND p.usuario_asignado = ${id} `
    }

    // Consultamos usuarios y tareas
    const usuariosERP    = await erpviejo.getUsuariosERP( ).then(response=> response)
    // const tareas         = await prospectos.getTareasProspectosAll( ).then(response=> response)

    const nuevos = await prospectos.getProspectosVendedora( parametro ).then(response=> response)
    
    if( !nuevos.length ){ return res.status( 400 ).send({ message: 'No tienes prospectos' })}

    // Sacamos los puros id
    let prospectosID         = nuevos.map((registro) => registro.idprospectos);

    // SACAR SOLO LOS TELEFONOS
    let prospectosTelefonos  = nuevos.map((registro) => registro.telefono);

    // const llamadasRealizadas = await conmutador.getLlamadasTelefonos( prospectosTelefonos ).then( response => response )

    const llamadasRealizadas = []

    //  623 ms

    // CONSULTAMOS LAS tareasProspectos
    const tareasProspectos     = await prospectos.tareasProspectosCount( prospectosID ).then( response => response) // 300 ms
    const eventosProspectos    = await prospectos.eventosProspectosCount( prospectosID ).then( response => response) // 550 ms
    const eventosProspectosHoy = await prospectos.eventosProspectosHoyCount( prospectosID ).then( response => response) // 600 ms
    const citas                = await prospectos.citasProspectos( prospectosID ).then( response => response) // 210 ms
    const tareas               = await prospectos.getTareasProspectosID( prospectosID  ).then(response=> response) // 250 ms
    
    // const tareasProspectos     = []
    // const eventosProspectos    = []
    // const eventosProspectosHoy = []
    // const citas                = []
    // const tareas               = []

    // const intentoLlamadas      = await prospectos.getIntentosLlamada( prospectosID  ).then(response=> response)
    let respuesta = nuevos

    // Buscamos los cursos y los nombres de vendedor y el que lo asgino
    for(const i in respuesta){
      const { usuario_asignado, usuario_creo, idprospectos, telefono } = respuesta[i]

      const vend               = usuariosERP.find(el=> el.id_usuario == usuario_asignado)
      const recluta            = usuariosERP.find(el=> el.id_usuario == usuario_creo)
      const tareasP            = tareas.filter(el=>{ return el.idprospectos == idprospectos })
      const tareasProgramadas  = tareasProspectos.find(el=> el.idprospectos == idprospectos )
      const cantEventos        = eventosProspectos.find(el=> el.idprospectos == idprospectos )
      const cantEventosHoy     = eventosProspectosHoy.find(el=> el.idprospectos == idprospectos )
      const cita               = citas.filter(el=>{ return el.idprospectos == idprospectos })
      // const intentoLlamada     = intentoLlamadas.find(el=> el.idprospectos == idprospectos )
      // VER SI HAY llamda realizada y el tiempo de llamada
      const existeConmutador   = llamadasRealizadas.find( el => el.telefono == telefono && el.id_usuario == usuario_asignado )

      const totalLlamadas      = llamadasRealizadas.filter( el =>{ return el.telefono == telefono && el.id_usuario == usuario_asignado })


      respuesta[i] = {
        ...respuesta[i],
        vendedora:         vend              ? vend.nombre_completo       : '',
        recluta:           recluta           ? recluta.nombre_completo    : '',
        tareasProgramadas: tareasProgramadas ? tareasProgramadas.cantidad : 0,
        cantEventos:       cantEventos       ? cantEventos.cantidad       : 0,
        cantEventosHoy:    cantEventosHoy    ? cantEventosHoy.cantidad    : 0,
        cita:              cita              ? cita                       : 0,
        // intentoLlamada:    intentoLlamada    ? intentoLlamada.idtipo_evento : 0,
        tareasP,
        existeConmutador: totalLlamadas
      }
    }

    // Ordenar los prospectos
    respuesta.sort((a,b)=> (a.cantEventos - b.cantEventos || a.cantEventosHoy - b.cantEventosHoy  || a.tiempo_transcurrido - b.tiempo_transcurrido || a.idetapa - b.idetapa ));


    // Hay que agregarles el examen de ubicación si es que ya lo tiene contestado el prospecto
    // respuesta
    let uniqINBI = respuesta.filter( el => { return el.idexamenubicacion != 0 && el.escuela == 1}).map((registro)=> { return registro.idexamenubicacion })
    let uniqFAST = respuesta.filter( el => { return el.idexamenubicacion != 0 && el.escuela == 2}).map((registro)=> { return registro.idexamenubicacion })
    uniqINBI = uniqINBI.length ? uniqINBI : [0]
    uniqFAST = uniqFAST.length ? uniqFAST : [0]

    // Consultar esos UNIQ en sus respectivas bases de datos
    // :)

    const examenInduccionFast = await prospectos.examenInduccionFast( uniqFAST ).then( response => response )
    const examenInduccionInbi = await prospectos.examenInduccionInbi( uniqINBI ).then( response => response )

    for( const i in respuesta ){

      const { escuela, idexamenubicacion } = respuesta[i]

      let existeRegistro = null

      if( escuela == 2 ){
        existeRegistro = examenInduccionFast.find( el => el.uniq ==  idexamenubicacion )
      }else{
        existeRegistro = examenInduccionInbi.find( el => el.uniq ==  idexamenubicacion )
      }

      if( !existeRegistro ){
        respuesta[i]['diploma']  = null
        respuesta[i]['nivel']    = null

      }else{
        respuesta[i]['diploma']  = escuela == 2 ? `https://www.fastenglish.com.mx/examen-interno/diploma.php?id=${existeRegistro.id}` : `https://www.inbi.mx/examen-interno/diploma.php?id=${existeRegistro.id}`
        respuesta[i]['nivel']    = existeRegistro.nivel ? existeRegistro.nivel : 0
      }

    }

    /*  
      Tipos de tareas
      1-. Agendada
      2-. Recordatorio o nota
    */

    /* 
      Tarea hoy? -> tareaParaHoy
      1-. Hoy
      0-. Otro día
    */

    const prospectosAgendadaInduccion = respuesta.filter(el =>{
      // Solo prospectos en inducción con tarea programada que no sea para hoy
      // Prospectos con tarea programada que no sea para hoy
      if(el.idetapa == 3){ 
        // Validar las tareas
        if(el.tareasP.length > 0){
          // La tarea es nota? o es programada pero no para hoy
          if(el.tareasP[0].tipo_tarea == 2 || el.tareasP[0].tipo_tarea == 1 && el.tareasP[0].tareaParaHoy == 0){
            return true
          }
        }else{
          // No tiene tarea, llega directo a prospectosAgendadaInduccion
          return true
        }
      }else{
        if(el.tareasP.length > 0){
          // es programada pero no para hoy
          if(el.tareasP[0].tipo_tarea == 1 && el.tareasP[0].tareaParaHoy == 0){
            return true
          }
        }
      }
    })

    // Prospectos sin seguimiento hoy y sin tareas programadas
    const prospectosSeguimiento = respuesta.filter(el =>{
      /*
        condiciones
        1-. No ser de induccion y tener 0 evenos hoy ( tareas -> solo si tiene progamada para hoy tareaParaHoy == 1)
        2-. Ser de inducción, tener tarea programada para hoy y 0 ventos hoy tareaParaHoy == 1
      */

      if(el.cantEventosHoy == 0){ 
        // Si no es inducción
        if(el.idetapa != 3){
          // Tiene tareas asignadas
          if(el.tareasP.length > 0){
            /*
              Agendada, no importa el día
              Programada, solo si es para hoy o antes de hoy
            */
            if(el.tareasP[0].tipo_tarea == 1 && el.tareasP[0].tareaParaHoy == 1 || el.tareasP[0].tipo_tarea == 2){
              return true
            }
          }else{
            return true
          }
        }else{
          // Si es inducción y tiene tareas
          if(el.tareasP.length > 0){
            //  y la tarea es agendada y es para hoy
            if(el.tareasP[0].tipo_tarea == 1 && el.tareasP[0].tareaParaHoy == 1){
              return true
            }
          }
        }
      }
    })

    // Prospectos que ya tienen un seguimiento el día de hoy
    const prospectosConSeguimiento = respuesta.filter(el =>{
      if(el.cantEventosHoy > 0){ 
        // Si no es inducción
        if(el.idetapa != 3){
          // Tiene tareas asignadas
          if(el.tareasP.length > 0){
            /*
              Agendada, no importa el día
              Programada, solo si es para hoy o antes de hoy
            */
            if(el.tareasP[0].tipo_tarea == 1 && el.tareasP[0].tareaParaHoy == 1 || el.tareasP[0].tipo_tarea == 2){
              return true
            }
          }else{
            return true
          }
        }else{
          // Si es inducción y tiene tareas
          if(el.tareasP.length > 0){
            //  y la tarea es agendada y es para hoy
            if(el.tareasP[0].tipo_tarea == 1 && el.tareasP[0].tareaParaHoy == 1){
              return true
            }
          }
        }
      }
    })

    const prospectosCerrados = await prospectos.getProspectosCerrados( parametro ).then(response => response)   //Angel Rodriguez


    const prospectosBasura = respuesta.filter((el)=>{ return el.idetapa == 5 && el.tiempo_transcurrido > 7 && el.cantEventos > 4 || el.idetapa == 4 && el.tiempo_transcurrido > 10 && el.cantEventos > 4 })  

    res.send({respuesta, prospectosAgendadaInduccion, prospectosSeguimiento, prospectosConSeguimiento, prospectosBasura, prospectosCerrados});
  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.getProspectosFinalizados = async(req, res) => {
  try {
    const usuariosERP    = await erpviejo.getUsuariosERP( ).then(response=> response)
    const tareas         = await prospectos.getTareasProspectosAll( ).then(response=> response)

    // Vamos a traer a los prospectos que son nuevos por completo
    /*
      PROSPECTOS NUEVO ( 2 )
        Sin tareas
        Ingreso el día de hoy
        Sin tareas
        Sin información
    */
    const nuevos = await prospectos.getProspectosVendedora( ).then(response=> response)
    /*
      PROSPECTOS ATRASADOS ( 1 )
        Sin tareas
        Tiempo de ingreso menor a hoy
        Sin información
    */
    const atrasados = await prospectos.getProspectosVendedoraAtrasados( ).then(response=> response)

    // El resto
    const getProspectosOrdenados = await prospectos.getProspectosOrdenados( ).then(response=> response)

    
    const getProspectosFinalizados = await prospectos.getProspectosFinalizados( ).then(response=> response)
    const getProspectosInscritos = await prospectos.getProspectosInscritos( ).then(response=> response)

    // concatenamos los prospectos
    // let respuesta = atrasados.concat(nuevos).concat(prospectosSeguimiento)
    let respuesta = atrasados.concat(nuevos).concat(getProspectosOrdenados).concat(getProspectosFinalizados).concat(getProspectosInscritos)

    // Buscamos los cursos y los nombres de vendedor y el que lo asgino
    for(const i in respuesta){
      const { curso_interes, ciclo_interes, usuario_asignado,usuario_creo, idprospectos } = respuesta[i]

      const vend     = usuariosERP.find(el=> el.id_usuario == usuario_asignado)
      const recluta  = usuariosERP.find(el=> el.id_usuario == usuario_creo)
      const tareasP  = tareas.filter(el=>{ return el.idprospectos == idprospectos })

      respuesta[i] = {
        ...respuesta[i],
        vendedora: vend     ? vend.nombre_completo : '',
        recluta:   recluta  ? recluta.nombre_completo : '',
        tareasP
      }
    }

    res.send(respuesta);
  } catch (error) {
    res.status(500).send({message:error?error.message:"Error en el servidor"})
  }
};

// Agregar eventos de llamada
exports.addEventosLlamada = async(req, res) => {
  try {
    const { idprospectos, tipo_evento, idusuarioerp } = req.body
    /*
      Tipos de eventos
      9 -. Intento de llamada normal
      10-. Intento de llamada WhatsApp
      11-. Se realizó llamada normal
      12-. Se realizó llamada por WhatsApp
    */
    let eventoTxt = ''

    switch(tipo_evento){
      case 9:
        eventoTxt = 'Intento de llamada normal'
      break;

      case 10:
        eventoTxt = 'Intento de llamada WhatsApp'
      break;

      case 11:
        eventoTxt = 'Se realizó llamada normal'
      break;

      case 12:
        eventoTxt = 'Se realizó llamada WhatsApp'
      break;
    }
    
    if( [9,10,11,12].includes( tipo_evento )){
      const update = await prospectos.updateUltimoEvento( idprospectos, tipo_evento ).then( response => response )
    }

    const evento  = await prospectos.addEvento( eventoTxt, idprospectos, tipo_evento, idusuarioerp ).then(response=> response)

    res.send(evento);
  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor'})
  }
};


exports.getLlamadaActiva = async(req, res) => {
  try {
    const { id } = req.params

    const llamadaActiva = await prospectos.getLlamadaActiva( id ).then(response => response)
    res.send(llamadaActiva);
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.getProspectosVendedoraCantidad = async(req, res) => {
  try {
    const usuariosERP  = await erpviejo.getUsuariosERP( ).then(response=> response)
    let contactos      = await prospectos.getContactosVendedora().then(response => response)

    // Buscamos los cursos y los nombres de vendedor y el que lo asgino
    for(const i in contactos){
      const { usuario_asignado, usuario_creo } = contactos[i]

      const vendedora   = usuariosERP.find(el=> el.id_usuario == usuario_asignado)
      const recluta     = usuariosERP.find(el=> el.id_usuario == usuario_creo)

      contactos[i] = {
        ...contactos[i],
        vendedora: vendedora ? vendedora.nombre_completo : '',
        recluta:   recluta   ? recluta.nombre_completo : '',
      }
    }

    res.send(contactos);
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.calisificarProspecto = async(req, res) => {
  try {

    const { prospecto, clasificacion, justificacion } = req.body 

    const updateProspecto = await prospectos.updateProspectoClasificar(  clasificacion, justificacion, prospecto ).then( response => response )

    res.send({message:'listo'});
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.seguimientoProspectosNI = async(req, res) => {
  try {

    const { tipo, id } = req.params

    let parametro = ''

    if(tipo == 1){ // vendedora
      parametro = ` AND usuario_asignado = ${id} `
    }

    // Consultamos usuarios y tareas
    const usuariosERP    = await erpviejo.getUsuariosERP( ).then(response=> response)

    const nuevos = await prospectos.getProspectosSeguimiento( parametro ).then(response=> response)
    
    if( !nuevos.length ){ return res.status( 400 ).send({ message: 'No tienes prospectos' })}

    // SACAR SOLO LOS TELEFONOS
    let prospectosTelefonos  = nuevos.map((registro) => registro.telefono);

    const llamadasRealizadas = await conmutador.getLlamadasTelefonosAll( prospectosTelefonos ).then( response => response )

    // Buscamos los cursos y los nombres de vendedor y el que lo asgino
    for(const i in nuevos){
      const { usuario_asignado, usuario_creo, idprospectos, telefono } = nuevos[i]

      const vend               = usuariosERP.find(el=> el.id_usuario == usuario_asignado)

      // VER SI HAY llamda realizada y el tiempo de llamada
      const existeConmutador   = llamadasRealizadas.find( el => el.telefono == telefono && el.id_usuario == usuario_asignado )
      const totalLlamadas      = llamadasRealizadas.filter( el =>{ return el.telefono == telefono })
      const contestadas        = llamadasRealizadas.filter( el =>{ return el.telefono == telefono && el.disposition == 'ANSWERED'}).length
      const rechazadas         = llamadasRealizadas.filter( el =>{ return el.telefono == telefono && el.disposition == 'NO ANSWER'}).length
      const ocupadas           = llamadasRealizadas.filter( el =>{ return el.telefono == telefono && el.disposition == 'BUSY'}).length
      const fallidas           = llamadasRealizadas.filter( el =>{ return el.telefono == telefono && el.disposition == 'FAILED'}).length
      const mastresmin         = llamadasRealizadas.filter( el =>{ return el.telefono == telefono && el.talk_time >= 180 }).length

      nuevos[i] = {
        ...nuevos[i],
        vendedora   : vend ? vend.nombre_completo : '',
        conmutador  : totalLlamadas,
        contestadas,
        rechazadas,
        ocupadas,
        fallidas,
        mastresmin
      }
    }

    /**********************************************************************************
     * SACAR LO DE WHATSAPP PARA SABER SI SE LE ESTA DANDO SEGUIMIENTO AL ALUMNO O NO
    ***********************************************************************************/

    const mensajesWhats  = await riesgo.mensajesWhaAlumnos( ).then(response => response)

    for( const i in nuevos ){
      let { telefono  } = nuevos[i]

      if( telefono ){
        const existeWhatsApp  = mensajesWhats.find(  el => el.tos.match( telefono ) )

        nuevos[i]['configurado']          = telefono
        nuevos[i]['whatsapp']             = telefono
        nuevos[i]['seguimiento_whatsApp'] = existeWhatsApp ? existeWhatsApp.diferencia_dias : 10000
        nuevos[i]['ultimo_mensaje']       = existeWhatsApp ? existeWhatsApp.max_fecha       : null

      }else{
        nuevos[i]['configurado']          = null
        nuevos[i]['whatsapp']             = null
        nuevos[i]['seguimiento_whatsApp'] = 10000
        nuevos[i]['ultimo_mensaje']       = null
      }

    }


    // Ordenar los prospectos
    nuevos.sort((a,b)=> ( a.idetapa - b.idetapa ));

    const vendedoras = [...new Set( nuevos.map((row) => { return row.vendedora }) )]

    let mensaje = ''
    let mensaje2 = ''
    let arregloVendedorasFast = []
    let arregloVendedorasInbi = []


    for( const i in vendedoras ){

      const existeVendedora = usuariosERP.find( el => el.nombre_completo == vendedoras[i] )
      let contador          = nuevos.filter( el => { return el.vendedora == vendedoras[i] }).length
      let sinseguimiento    = nuevos.filter( el => { return el.vendedora == vendedoras[i] &&  el.seguimiento_whatsApp > 3 }).length

      if( existeVendedora ){
        const { id_plantel } = existeVendedora

        if( [1,2,3,4,5,6,21,22,23].includes( id_plantel ) ){
          arregloVendedorasInbi.push({
            vendedora: vendedoras[i],
            contador,
            sinseguimiento
          })

        }else{
          arregloVendedorasFast.push({
            vendedora: vendedoras[i],
            contador,
            sinseguimiento
          })
        }
      }
    }


    arregloVendedorasFast.sort((a,b)=> ( b.sinseguimiento - a.sinseguimiento ));
    arregloVendedorasInbi.sort((a,b)=> ( b.sinseguimiento - a.sinseguimiento ));

    res.send({ nuevos, arregloVendedorasFast, arregloVendedorasInbi });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getCalidadProspectos = async(req, res) => {
  try {

    const { fechaini, fechafin, id, tipo } = req.body

    let parametro = ''

    if(tipo == 1){ // vendedora
      parametro = ` AND usuario_asignado = ${id} `
    }

    // Consultamos usuarios y tareas
    const usuariosERP    = await erpviejo.getUsuariosERP( ).then(response=> response)

    const prospectosList = await prospectos.getProspectosCalidad( parametro, fechaini, fechafin ).then(response=> response)
    
    if( !prospectosList.length ){ return res.status( 400 ).send({ message: 'No tienes prospectos' })}

    // SACAR SOLO LOS TELEFONOS
    // Buscamos los cursos y los nombres de vendedor y el que lo asgino
    for(const i in prospectosList){
      const { usuario_asignado, usuario_creo, idprospectos, telefono } = prospectosList[i]

      const vend  = usuariosERP.find(el=> el.id_usuario == usuario_asignado)

      prospectosList[i]['vendedora'] = vend ? vend.nombre_completo : ''
    }


    const vendedoras = [...new Set( prospectosList.map((row) => { return row.vendedora }) )]

    let reporteFast = []
    let reporteInbi = []

    for( const i in vendedoras ){

      const existeVendedora = usuariosERP.find( el => el.nombre_completo == vendedoras[i] )

      let buenos     = prospectosList.filter( el => { return el.vendedora == vendedoras[i] &&  el.clasificacion == 1 })
      let malos      = prospectosList.filter( el => { return el.vendedora == vendedoras[i] &&  el.clasificacion == 2 })
      let nulos      = prospectosList.filter( el => { return el.vendedora == vendedoras[i] &&  el.clasificacion == 0 })
      let total      = prospectosList.filter( el => { return el.vendedora == vendedoras[i] })
      let rechazados = prospectosList.filter( el => { return el.vendedora == vendedoras[i] &&  el.justificacion2 })
      let alumnos    = prospectosList.filter( el => { return el.vendedora == vendedoras[i] })

      if( existeVendedora ){
        const { id_plantel } = existeVendedora

        if( ![1,2,3,4,5,6,21].includes( id_plantel ) ){
          reporteFast.push({
            vendedora: vendedoras[i],
            buenos,
            malos,
            nulos,
            total,
            alumnos,
            rechazados
          })

        }else{
          reporteInbi.push({
            vendedora: vendedoras[i],
            buenos,
            malos,
            nulos,
            total,
            alumnos,
            rechazados
          })
        }
      }
    }


    reporteFast.sort((a,b)=> ( b.sinseguimiento - a.sinseguimiento ));
    reporteInbi.sort((a,b)=> ( b.sinseguimiento - a.sinseguimiento ));

    res.send({ prospectosList, reporteFast, reporteInbi });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.addRespuestaMkt = async(req, res) => {
  try {

    const { idprospectos, justificacion2 } = req.body

    const addRespuestaMkt = await prospectos.addRespuestaMkt( justificacion2, idprospectos ).then(response=> response)

    res.send({ message: 'Dato actualizado correctamente' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getProspectosReclutadora = async(req, res) => {
  try {

    const nuevos = await prospectos.getProspectosReclutadora( ).then(response=> response)

    
    if( !nuevos.length ){ return res.status( 400 ).send({ message: 'No tienes prospectos' })}

    // Sacamos los puros id
    let prospectosID         = nuevos.map((registro) => registro.idformulario );


    const llamadasRealizadas = []

    //  623 ms

    // CONSULTAMOS LAS tareasProspectos
    const tareasProspectos     = await prospectos.tareasReclutaCount( prospectosID ).then( response => response) // 300 ms
    const eventosProspectos    = await prospectos.eventosReclutadoraCount( prospectosID ).then( response => response) // 550 ms
    const eventosProspectosHoy = await prospectos.eventosReclutadoraHoyCount( prospectosID ).then( response => response) // 600 ms
    const citas                = await prospectos.citasReclutadora( prospectosID ).then( response => response) // 210 ms
    const tareas               = await prospectos.getTareasReclutadoraID( prospectosID  ).then(response=> response) // 250 ms
    
    
    let respuesta = nuevos


    // Buscamos los cursos y los nombres de vendedor y el que lo asgino
    for(const i in respuesta){
      const { usuario_asignado, usuario_creo, idformulario, telefono } = respuesta[i]
      

      const tareasP            = tareas.filter(el=>{ return el.idformularios     == idformulario })
      const tareasProgramadas  = tareasProspectos.find(el=> el.idformularios     == idformulario )
      const cantEventos        = eventosProspectos.find(el=> el.idformularios    == idformulario )
      const cantEventosHoy     = eventosProspectosHoy.find(el=> el.idformularios == idformulario )
      const cita               = citas.filter(el=>{ return el.idformularios      == idformulario })

      // VER SI HAY llamda realizada y el tiempo de llamada
      respuesta[i] = {
        ...respuesta[i],
        tareasProgramadas: tareasProgramadas ? tareasProgramadas.cantidad : 0,
        cantEventos:       cantEventos       ? cantEventos.cantidad       : 0,
        cantEventosHoy:    cantEventosHoy    ? cantEventosHoy.cantidad    : 0,
        cita:              cita              ? cita                       : 0,
        tareasP,
        existeConmutador: []
      }

    }

    // Ordenar los prospectos
    respuesta.sort((a,b)=> (a.cantEventos - b.cantEventos || a.cantEventosHoy - b.cantEventosHoy  || a.tiempo_transcurrido - b.tiempo_transcurrido || a.idetapa - b.idetapa ));
    
    /*  
      Tipos de tareas
      1-. Agendada
      2-. Recordatorio o nota
    */

    /* 
      Tarea hoy? -> tareaParaHoy
      1-. Hoy
      0-. Otro día
    */
    const prospectosAgendadaInduccion = respuesta.filter(el =>{
      // Solo prospectos en inducción con tarea programada que no sea para hoy
      // Prospectos con tarea programada que no sea para hoy
      if(el.idetapa == 3){ 
        // Validar las tareas
        if(el.tareasP.length > 0){
          // La tarea es nota? o es programada pero no para hoy
          if(el.tareasP[0].tipo_tarea == 2 || el.tareasP[0].tipo_tarea == 1 && el.tareasP[0].tareaParaHoy == 0){
            return true
          }
        }else{
          // No tiene tarea, llega directo a prospectosAgendadaInduccion
          return true
        }
      }else{
        if(el.tareasP.length > 0){
          // es programada pero no para hoy
          if(el.tareasP[0].tipo_tarea == 1 && el.tareasP[0].tareaParaHoy == 0){
            return true
          }
        }
      }
    })

    // Prospectos sin seguimiento hoy y sin tareas programadas
    const prospectosSeguimiento = respuesta.filter(el =>{
      /*
        condiciones
        1-. No ser de induccion y tener 0 evenos hoy ( tareas -> solo si tiene progamada para hoy tareaParaHoy == 1)
        2-. Ser de inducción, tener tarea programada para hoy y 0 ventos hoy tareaParaHoy == 1
      */

      if(el.cantEventosHoy == 0){ 
        // Si no es inducción
        if(el.idetapa != 3){
          // Tiene tareas asignadas
          if(el.tareasP.length > 0){
            /*
              Agendada, no importa el día
              Programada, solo si es para hoy o antes de hoy
            */
            if(el.tareasP[0].tipo_tarea == 1 && el.tareasP[0].tareaParaHoy == 1 || el.tareasP[0].tipo_tarea == 2){
              return true
            }
          }else{
            return true
          }
        }else{
          // Si es inducción y tiene tareas
          if(el.tareasP.length > 0){
            //  y la tarea es agendada y es para hoy
            if(el.tareasP[0].tipo_tarea == 1 && el.tareasP[0].tareaParaHoy == 1){
              return true
            }
          }
        }
      }
    })

    // Prospectos que ya tienen un seguimiento el día de hoy
    const prospectosConSeguimiento = respuesta.filter(el =>{
      if(el.cantEventosHoy > 0){ 
        // Si no es inducción
        if(el.idetapa != 3){
          // Tiene tareas asignadas
          if(el.tareasP.length > 0){
            /*
              Agendada, no importa el día
              Programada, solo si es para hoy o antes de hoy
            */
            if(el.tareasP[0].tipo_tarea == 1 && el.tareasP[0].tareaParaHoy == 1 || el.tareasP[0].tipo_tarea == 2){
              return true
            }
          }else{
            return true
          }
        }else{
          // Si es inducción y tiene tareas
          if(el.tareasP.length > 0){
            //  y la tarea es agendada y es para hoy
            if(el.tareasP[0].tipo_tarea == 1 && el.tareasP[0].tareaParaHoy == 1){
              return true
            }
          }
        }
      }
    })

    const prospectosCerrados = await prospectos.getProspectosCerradosRecluta( ).then(response => response)   //Angel Rodriguez

    const prospectosBasura = respuesta.filter((el)=>{ return el.idetapa == 5 && el.tiempo_transcurrido > 7 && el.cantEventos > 4 || el.idetapa == 4 && el.tiempo_transcurrido > 10 && el.cantEventos > 4 })  

    res.send({respuesta, prospectosAgendadaInduccion, prospectosSeguimiento, prospectosConSeguimiento, prospectosBasura, prospectosCerrados});
  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.getDatosFormulario = async(req, res) => {
  try {
    const { id } = await req.params

    const tareas_realizadas = []

    // Buscamos los comentarios de los prospectos
    const comentarios  = await prospectos.getComentariosFormulario( id ).then(response=> response)
    const usuariosERP  = await erpviejo.getUsuariosERP( ).then(response=> response)

    // Recorresmos los comentarios para poder agregar el nommbre de la persona que hizo el comentario
    for(const i in comentarios){
      const { idusuarioerp } = comentarios[i]
      // Lo buscamos
      const usuario = usuariosERP.find(el=> el.id_usuario == idusuarioerp )
      // Si lo encuentra, agrega el nombre
      comentarios[i]['nombre_completo'] = usuario ? usuario.nombre_completo : ''
    }

    // Ahora buscamos las tareas programadas del prospecto
    const tareas  = await prospectos.getTareasFormulario( id ).then(response=> response)
    // const tareas = []

    // Sacamos las tareas del prospecto
    const eventos  = await prospectos.getEventosRecluta( id ).then(response=> response)

    // Bandera para abrir los mensajes de whatsApp
    for(const i in eventos){
      eventos[i]['abrirMensaje'] = false
    }

    // Sacamos las llamadas realizadas
    // const llamadas     = await prospectos.getEstatusllamadaList( id ).then(response=> response)
    const llamadas = []
    // Sacamos las notas de esa llamada
    const notas        = await prospectos.getNotasLlamada( id ).then(response=> response)
    
    // Recorresmos los notas para poder agregar el nommbre de la persona que hizo el comentario
    for(const i in llamadas){
      const { idusuarioerp, idestatus_llamada } = llamadas[i]
      // Lo buscamos
      const usuario = usuariosERP.find(el=> el.id_usuario == idusuarioerp)
      // Si lo encuentra, agrega el nombre
      const notasFilter = notas.filter(el=>{
        return el.idestatus_llamada == idestatus_llamada
      })

      let usuarioNota = ''
      for(const j in notasFilter){
        usuarioNota = usuariosERP.find(el=> el.id_usuario == notasFilter[j].idusuarioerp)
        notasFilter[j] = {
          ...notasFilter[j],
          nombre_completo: usuarioNota ? usuarioNota.nombre_completo : ''
        }
      }

      llamadas[i] = {
        ...llamadas[i],
        nombre_completo: usuario ? usuario.nombre_completo : '',
        notas: notasFilter
      }
    }

    // Generar el timeline
    let timeLineTareas = eventos.filter(el=> { return el.idtipo_evento == 3})
    // modificaremos el resultado
    for(const i in timeLineTareas){
      timeLineTareas[i] = {
        idtipo_evento: 4,
        evento: timeLineTareas[i].evento,
        fecha_creacion: timeLineTareas[i].fecha_creacion,
        icono:'mdi-clipboard-check',
        color:'red'
      }
    }

    // Generar el timeline
    let timeLineWhats = eventos.filter(el=> { return el.idtipo_evento == 5})
    // modificaremos el resultado
    for(const i in timeLineWhats){
      timeLineWhats[i] = {
        idtipo_evento: 1,
        evento: timeLineWhats[i].evento,
        fecha_creacion: timeLineWhats[i].fecha_creacion,
        icono:'mdi-whatsapp',
        color:'green'
      }
    }

    // Generar el timeline
    let timeLineIndu = eventos.filter(el=> { return el.idtipo_evento == 6})
    // modificaremos el resultado
    for(const i in timeLineIndu){
      timeLineIndu[i] = {
        idtipo_evento: 2,
        evento: timeLineIndu[i].evento,
        fecha_creacion: timeLineIndu[i].fecha_creacion,
        icono:'mdi-emoticon-happy',
        color:'pink'
      }
    }

    // Intento de llamada normal 
    let timeLineIntentoLlamada = eventos.filter(el=> { return el.idtipo_evento == 9})
    // modificaremos el resultado
    for(const i in timeLineIntentoLlamada){
      timeLineIntentoLlamada[i] = {
        idtipo_evento: 5,
        evento: timeLineIntentoLlamada[i].evento,
        fecha_creacion: timeLineIntentoLlamada[i].fecha_creacion,
        icono:'mdi-phone-hangup',
        color:'red'
      }
    }

    // Intento de llamada whatsApp 
    let timeLineItentoWhats = eventos.filter(el=> { return el.idtipo_evento == 10})
    // modificaremos el resultado
    for(const i in timeLineItentoWhats){
      timeLineItentoWhats[i] = {
        idtipo_evento: 6,
        evento: timeLineItentoWhats[i].evento,
        fecha_creacion: timeLineItentoWhats[i].fecha_creacion,
        icono:'mdi-phone-hangup',
        color:'#64DD17'
      }
    }

    // Intento de llamada normal 
    let timeLineLlamadaRealizada = eventos.filter(el=> { return el.idtipo_evento == 11})
    // modificaremos el resultado
    for(const i in timeLineLlamadaRealizada){
      timeLineLlamadaRealizada[i] = {
        idtipo_evento: 7,
        evento: timeLineLlamadaRealizada[i].evento,
        fecha_creacion: timeLineLlamadaRealizada[i].fecha_creacion,
        icono:'mdi-phone',
        color:'warning'
      }
    }

    // Intento de llamada normal 
    let timeLineLlamadaWathsApp = eventos.filter(el=> { return el.idtipo_evento == 12})
    // modificaremos el resultado
    for(const i in timeLineLlamadaWathsApp){
      timeLineLlamadaWathsApp[i] = {
        idtipo_evento: 8,
        evento: timeLineLlamadaWathsApp[i].evento,
        fecha_creacion: timeLineLlamadaWathsApp[i].fecha_creacion,
        icono:'mdi-phone',
        color:'#64DD17'
      }
    }

    // Intento de llamada normal 
    let timeLinePerfil = eventos.filter(el=> { return el.idtipo_evento == 13})
    // modificaremos el resultado
    for(const i in timeLinePerfil){
      timeLinePerfil[i] = {
        idtipo_evento: 8,
        evento: timeLinePerfil[i].evento,
        fecha_creacion: timeLinePerfil[i].fecha_creacion,
        icono:'mdi-account-check',
        color:'#311B92'
      }
    }

    let timeLine = timeLineWhats.concat(timeLineIndu).concat(timeLineTareas).concat(timeLineIntentoLlamada).concat(timeLineItentoWhats).concat(timeLineLlamadaRealizada).concat(timeLineLlamadaWathsApp).concat(timeLinePerfil)
    
    // Ordenarlo por fecha
    timeLine.sort(function (a, b) {
      if (a.fecha_creacion > b.fecha_creacion) {
        return 1;
      }
      if (a.fecha_creacion < b.fecha_creacion) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });

    // Reponder
    res.send({comentarios, tareas, eventos, llamadas, timeLine, tareas_realizadas });

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.addComentarioFormulario = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  prospectos.addComentarioFormulario(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el comentario"
      })
    else res.status(200).send({ message: `El comentario se ha creado correctamente` });
  })
};

exports.addTareaReclutadora = async(req, res) => {
  try {
    const { idformularios, tarea, idusuarioerp, dia, hora } = req.body

    // Verificar si se puede programar una tarea
    const existeTarea = await prospectos.existeTareaReclutadora( idusuarioerp, dia, hora ).then(response=> response)
    if(existeTarea.length > 0){
      res.status(500).send({message:'Se empalma la tarea'})
    }else{
      const addTareaReclutadora  = await prospectos.addTareaReclutadora( req.body ).then(response=> response)
      
      let eventoTxt = 'Se genero la tarea: ' + tarea
      const evento  = await prospectos.addEventoRecluta( eventoTxt, idformularios, 3, idusuarioerp ).then(response=> response)

      res.send({message: 'Actualización correcta'});
    }

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.getTareasUsuarioRecluta = async(req, res) => {
  try {
    const { id }            = req.params
    // Agregar el prospecto
    const getTareasUsuarioRecluta  = await prospectos.getTareasUsuarioRecluta( id ).then(response=> response)

    res.send(getTareasUsuarioRecluta);

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.updateFormularioEtapa = async(req, res) => {
  try {
    const { id }                 = req.params
    const { idetapa, etapa, usuario_creo } = req.body

    const updateEtapaFormulario  = await prospectos.updateEtapaFormulario( id, idetapa ).then(response=> response)

    // Agregar un evento
    let eventoTxt = 'El prospecto cambió a: ' + etapa
    const evento  = await prospectos.addEventoRecluta( eventoTxt, id, 1, usuario_creo ).then(response=> response)

    res.send({message: 'Actualización correcta'});

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.updateEtapaActualFormulario = async(req, res) => {
  try {
    const { id }                 = req.params
    const { idetapa, etapa, usuario_creo } = req.body

    const updateEtapaActualFormulario  = await prospectos.updateEtapaActualFormulario( id, idetapa ).then(response=> response)

    // Agregar un evento
    let eventoTxt = 'El prospecto cambió a: ' + etapa
    const evento  = await prospectos.addEventoRecluta( eventoTxt, id, 2, usuario_creo ).then(response=> response)

    res.send({message: 'Actualización correcta'});

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.deletedTareaReclutadora = async(req, res) => {
  try {
    const { id } = req.params

    const tarea = await prospectos.getTareaRecluta( id ).then(response => response)
    const eliminarTarea  = await prospectos.eliminarTareaRecluta( id ).then(response=> response)

    
    let eventoTxt = 'Se elimino la tarea: ' + tarea.tarea
    const evento  = await prospectos.addEventoRecluta( eventoTxt, tarea.idformularios, 8, tarea.idusuarioerp ).then(response=> response)

    res.send({message: 'Actualización correcta'});

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.updateTareaCheckRecluta = async(req, res) => {
  try {
    const { id } = req.params

    if(id > 0){
      const tarea            = await prospectos.getTareaRecluta( id ).then(response => response)
      const teareaRealizada  = await prospectos.teareaRealizadaRecluta( id ).then(response=> response)      
      let eventoTxt = 'Se realizó la tarea: ' + tarea.tarea
      const evento  = await prospectos.addEvento( eventoTxt, tarea.idformularios, 3, tarea.idusuarioerp ).then(response=> response)
    }

    res.send({message: 'Actualización correcta'});

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.addUsuarioLlamadaRecluta = async(req, res) => {
  try {
    const { idusuarioerp, estatus, idformularios } = req.body

    // vemos si el usuario tiene una llamada registrada, si no es así, la agregamos  
    const existeLlamada = await prospectos.existeLlamadaRecluta( idusuarioerp ).then(response=> response)
    // Verificamos si hay una llamada
    if(existeLlamada.length > 0){
      // si hay, la actualizamos
      const updateEstatusLlamada = await prospectos.updateEstatusLlamadaRecluta( idusuarioerp, estatus ).then(response=> response)
      res.send(updateEstatusLlamada);
    }else{
      // No hay, la agregamos
      const addLlamada    = await prospectos.addLlamadaRecluta( idusuarioerp, estatus, idformularios ).then(response=> response)
      let eventoTxt = 'Se realizó una llamada'
      const evento  = await prospectos.addEventoRecluta( eventoTxt, idformularios, 7, idusuarioerp ).then(response=> response)
      res.send(addLlamada);
    }

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

// Agregar eventos de llamada
exports.addEventosLlamadaRecluta = async(req, res) => {
  try {
    const { idformularios, tipo_evento, idusuarioerp } = req.body
    /*
      Tipos de eventos
      9 -. Intento de llamada normal
      10-. Intento de llamada WhatsApp
      11-. Se realizó llamada normal
      12-. Se realizó llamada por WhatsApp
    */
    let eventoTxt = ''

    switch(tipo_evento){
      case 9:
        eventoTxt = 'Intento de llamada normal'
      break;

      case 10:
        eventoTxt = 'Intento de llamada WhatsApp'
      break;

      case 11:
        eventoTxt = 'Se realizó llamada normal'
      break;

      case 12:
        eventoTxt = 'Se realizó llamada WhatsApp'
      break;
    }
    
    if( [9,10,11,12].includes( tipo_evento )){
      const update = await prospectos.updateUltimoEventoRecluta( idformularios, tipo_evento ).then( response => response )
    }

    const evento  = await prospectos.addEventoRecluta( eventoTxt, idformularios, tipo_evento, idusuarioerp ).then(response=> response)

    res.send(evento);
  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor'})
  }
};

exports.finalizarProspectoFormulario = async(req, res) => {
  try {
    const { idformulario, matricula, idgrupo, idusuarioerp, nombres, apellido_paterno, apellido_materno } = req.body

    const finalizar = await prospectos.updateProspectoFinalizoFormulario(idformulario, matricula, idgrupo, nombres, apellido_paterno, apellido_materno).then(response=> response)

    let eventoTxt = 'Prospecto finalizo'
    const evento  = await prospectos.addEventoRecluta( eventoTxt, idformulario, 1, idusuarioerp ).then(response=> response)

    res.send({matricula});
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};

exports.finalizarProspectoRechazoFormulario = async(req, res) => {
  try {
    const { idformulario, notaRechazo, idusuarioerp } = req.body

    // 1-. Actualizar el prospecto
    // 2-. Agregar el evento
    // finalizo =  1, fecha_finalizo = NOW()
    // Evento

    const finalizar = await prospectos.updateProspectoFinalizoRechazoRecluta(idformulario, notaRechazo).then(response=> response)
    let eventoTxt = 'Prospecto finalizo, no se cumplió meta'
    const evento  = await prospectos.addEventoRecluta( eventoTxt, idformulario, 1, idusuarioerp ).then(response=> response)

    // // Consultamos las tareas, y las vamos cerrando
    // const tareas  = await prospectos.getTareasProspectos( idformulario ).then(response=> response)
    // for(const i in tareas){
    //   const { idtareas_prospectos } = tareas[i]
    //   const eliminarTarea  = await prospectos.eliminarTareaRecluta( idtareas_prospectos ).then(response=> response)
    // }

    res.send({idformulario});
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};


exports.getColaboradoresList = async(req, res) => {
  try {

    const colaboradores = await prospectos.getColaboradoresList( ).then(response=> response)

    res.send( colaboradores );

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, pégale a sistemas :p'})
  }
};