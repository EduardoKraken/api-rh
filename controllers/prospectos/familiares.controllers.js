const familiares    = require("../../models/prospectos/familiares.model.js");
const prospectos    = require("../../models/prospectos/prospectos.model.js");

exports.addFamiliar = async(req, res) => {
  try {
    const addFamiliar    = await familiares.addFamiliar( req.body ).then(response=> response)
    const getFamiliares  = await familiares.getFamiliares( req.body.idprospectos ).then(response=> response)
    res.send(getFamiliares);

  } catch (error) {
    res.status(500).send({message:error})
  }
};


exports.deleteFamiliar = async(req, res) => {
  try {
    const { idfamiliares, idprospectos } = req.body

    const deleteFamiliar   = await familiares.deleteFamiliar( idfamiliares ).then(response=> response)
   
    const getFamiliares    = await familiares.getFamiliares( idprospectos ).then(response=> response)
    res.send(getFamiliares);

  } catch (error) {
    res.status(500).send({message:error})
  }
};


exports.finalizarFamiliar = async(req, res) => {
  try {
    const { idfamiliares, matricula, idgrupo, idusuarioerp, codigo_clase, idprospectos } = req.body
    
    // primero obtenemos el id_alumno con la matricula
    const { id_alumno } = await prospectos.getAlumnoMatricula( matricula ).then(response=> response)

    // Validamos que no exista un registro anterior con ese alumno en los prospectos
    const existeRegistro = await prospectos.getRegistroProspecto( id_alumno ).then(response=> response)
    // Validamos que no exista un registro anterior con ese alumno en los familiares
    const existeRegistroFamiliar = await familiares.getRegistroFamilair( id_alumno ).then(response=> response)

    if(existeRegistro || existeRegistroFamiliar){
      res.status(500).send({message:'El prospecto ya cuenta con un registro'})
      return
    }

    // // Validamos que el usuario este asignado a ese grupo
    const existeGrupoAlumno = await prospectos.getGrupoAlumno(id_alumno, idgrupo).then(response=> response)
    if(!existeGrupoAlumno){
      res.status(500).send({message:'El prospecto no se encuentra en ese grupo'})
      return
    }

    // Finalizamos el prospecto
	  const finalizar = await familiares.updateFamiliarFinalizo(idfamiliares, id_alumno, idgrupo, codigo_clase).then(response=> response)
    // Agregamos el evento
    let eventoTxt = 'Prospecto finalizó con exito'
    const evento  = await familiares.addEvento( eventoTxt, idprospectos, idfamiliares, 14, idusuarioerp ).then(response=> response)

    res.send({id_alumno});
  } catch (error) {
    res.status(500).send({message:error})
  }
};

// finalizar el prospecto
exports.finalizarFamiliarRechazo = async(req, res) => {
  try {
    const { idfamiliares, notaRechazo, idusuarioerp, idprospectos } = req.body

    // 1-. Actualizar el familiar con su nota de rechazó
    const finalizar = await familiares.updateFamiliarFinalizoRechazo(idfamiliares, notaRechazo).then(response=> response)

    // 2-. Agregar el evento
    let eventoTxt = 'Familiar finalizó, no se cumplió meta'
    const evento  = await familiares.addEvento( eventoTxt, idprospectos, idfamiliares, 14, idusuarioerp ).then(response=> response)

    res.send({idfamiliares});
  } catch (error) {
    res.status(500).send({message:error})
  }
};


