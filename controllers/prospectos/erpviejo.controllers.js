const erpviejo   = require("../../models/prospectos/erpviejo.model.js");
const usuarios   = require("../../models/usuarios.model.js");
const planteles  = require("../../models/planteles_usuario.model.js");

// Consultar las anuncios con deleted 0
exports.getCursosActivos = (req, res) => {
  erpviejo.getCursosActivos((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las anuncios"
      });
    else res.send(data);
  });
};

exports.getCiclosActivos = (req, res) => {
  erpviejo.getCiclosActivos((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las anuncios"
      });
    else res.send(data);
  });
};


exports.getVendedoras = async(req, res) => {
  try {
    
  	const usuariosERP = await erpviejo.getUsuariosERP( ).then(response=> response)
  	const plantelesv  = await planteles.plantelesVendedora( ).then(response=> response)

  	for(const i in plantelesv){
  		const { iderp, idusuario } = plantelesv[i]

  		const existeUsuario = usuariosERP.find(el => el.id_usuario == iderp)
  		if(existeUsuario){
  			plantelesv[i] = {
  				...plantelesv[i],
  				nombre: existeUsuario.nombre_completo,
  			}
  		}
  	}

		res.send(plantelesv);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

