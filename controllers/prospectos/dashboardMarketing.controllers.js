const erpviejo           = require("../../models/prospectos/erpviejo.model.js");
const usuarios           = require("../../models/usuarios.model.js");
const planteles          = require("../../models/planteles_usuario.model.js");
const dashboardMarketing = require("../../models/prospectos/dashboardMarketing.model.js");
const prospectos         = require("../../models/prospectos/prospectos.model.js");
const mediocontacto      = require("../../models/prospectos/mediocontacto.model.js");
const Kpi                = require("../../models/kpi/kpi.model.js");
const entradas           = require("../../models/catalogos/entradas.model.js");
const correoConvenio     = require("../../helpers/correoConvenio.js");
const correosConvenio    = require("../../helpers/correosConvenio.js");
const campanias          = require("../../models/prospectos/campanias.model.js");
const plantelesEscuela   = require("../../models/catalogos/planteles.model.js");

/******************************************/
/*            ENVIAR CORREOS              */
/******************************************/
const USUARIO_CORREO  =  'staff.fastenglish@gmail.com'
const PASSWORD_CORREO =  'jzxmdixwyauwxfbf'
const ESCUELA         =          'FAST ENGLISH'
const nodemailer      = require('nodemailer');

const user = USUARIO_CORREO
const pass = PASSWORD_CORREO

// Preparamos el transporte
let transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 465, 
  secure:true,
  pool: true,
  auth: {
    user,
    pass
  }
});

const userq = 'vinculacion@inbi.mx'
const passq = 'vincu14c10n@/'

// Preparamos el transporte
let transporter2 = nodemailer.createTransport({
  host: 'inbi.mx',
  port: 465, //25, 465, 587, 
  secure:true,
  auth: {
    user: userq,
    pass: passq
  }
});

transporter2.verify(function (error, success) {
  if (error) {
    console.log(error);
  } else {
    console.log("Server is ready to take our messages");
  }
});

exports.getDashboardMarketing = async(req, res) => {
  try {
  	const { fecha_inicio, fecha_final, escuela } = req.body
		
	  // Obtener todos los LEADS
    const getLEADS = await dashboardMarketing.getLEADS( fecha_inicio, fecha_final, escuela ).then(response=> response)

    // Obtener todas las fuentes activas de los leads
    let fuentes = await dashboardMarketing.getFuentesLeads( ).then(response=>response)

    // detalles de la fuente
    let detalleFuente = await dashboardMarketing.getDetalleFuentesLeads( ).then(response=> response)

    // Recorremos las fuentes para poder indicar sus leads
    for(const i in fuentes){
    	const { idfuentes } = fuentes[i]
    	// Consultar los detalles de la fuente
    	let detalles = detalleFuente.filter(el=> { return  el.idfuentes ==  idfuentes })

    	// Ahoraaa tenemos que desmenuzar jajaja los contactos y todo eso pero del detalle de la fuente
    	for(const j in detalles){
    		const { iddetalle_fuentes } = detalles[j]
    		detalles[j] = {
	    		...detalles[j],
	    		cantidad         : getLEADS.filter(el=> { return  el.iddetalle_fuentes ==  iddetalle_fuentes }).length,
	    		cantidadContactos: getLEADS.filter(el=> { return  el.iddetalle_fuentes ==  iddetalle_fuentes && el.folio}).length,
	    		cantidadInscritos: getLEADS.filter(el=> { return  el.iddetalle_fuentes ==  iddetalle_fuentes && el.finalizo == 1 && el.idgrupo > 0}).length
	    	}
    	}

    	fuentes[i] = {
    		...fuentes[i],
    		cantidad         : getLEADS.filter(el=> { return  el.idfuentes ==  idfuentes }).length,
    		cantidadContactos: getLEADS.filter(el=> { return  el.idfuentes ==  idfuentes && el.folio}).length,
    		cantidadInscritos: getLEADS.filter(el=> { return  el.idfuentes ==  idfuentes && el.finalizo == 1 && el.idgrupo > 0}).length,
    		detalles
    	}
    }

    // Ordenar las fuentes para de mayor a menor de leads
    fuentes.sort((a, b) => {
		  if (a.cantidad < b.cantidad) {
		    return 1;
		  }
		  if (a.cantidad > b.cantidad) {
		    return -1;
		  }
		  return 0;
		});

    /*
      Ver los inscritos
    */

    let telefonosLeads    = getLEADS.filter( el => { return el.folio }).map((registro) => { return registro.telefono })
    let idAlumnosLeads    = getLEADS.filter( el => { return el.folio && el.idalumno > 0 }).map((registro) => { return registro.idalumno })
    idAlumnosLeads        = idAlumnosLeads.length ? idAlumnosLeads : [11111111111111]
    telefonosLeads        = telefonosLeads.length ? telefonosLeads : [11111111111111]

    // Ahora vamos a sacar a los alumnos 
    // Consultar esos alumnos finalizados y los teléfonos
    const alumnosInscritos         = await dashboardMarketing.getAlumnosInscritos( idAlumnosLeads, telefonosLeads ).then( response => response )

    // Total de contactos, inscritos y rechazados
    const contactos  = getLEADS.filter(el=> { return  el.folio  }).length
    const inscritos  = getLEADS.filter(el=> { return  el.folio && el.finalizo == 1 && el.idgrupo > 0 }).length
    const rechazados = getLEADS.filter(el=> { return  el.folio && el.finalizo == 1 && el.idgrupo == 0 }).length
  	
  	// DESLGOSE DE LOS LEADS
  	const desgloseLeads = [
  		{ 
  			nombre    : 'Contactos',
  			total     : contactos,
  			color     : 'success',
  			porcentaje: contactos ? (( contactos / getLEADS.length) * 100 ).toFixed(1) : 0,
  		},
  		{ 
  			nombre    : 'Inscritos',
  			total     : inscritos,
  			color     : 'error',
  			porcentaje: inscritos ? (( inscritos / getLEADS.length) * 100 ).toFixed(1) : 0,
  		},
  		{ 
  			nombre    : 'Rechazados',
  			total     : rechazados,
  			color     : 'orange',
  			porcentaje: rechazados ? (( rechazados / getLEADS.length) * 100 ).toFixed(1) : 0,
  		},
  	]

  	// Cantidad de conactos por fuente
  	let contactosFuente = []
  	for(const i in fuentes){
    	const { idfuentes } = fuentes[i]
    	const payload = {
    		...fuentes[i],
    		cantidad: getLEADS.filter(el=> { return  el.idfuentes ==  idfuentes &&  el.folio }).length
    	}

    	contactosFuente.push(payload)
    }

    // Ordenar las fuentes para de mayor a menor de leads
    contactosFuente.sort((a, b) => {
		  if (a.cantidad < b.cantidad) {
		    return 1;
		  }
		  if (a.cantidad > b.cantidad) {
		    return -1;
		  }
		  return 0;
		});


		// Sacar las vendedoras
		let contactosInscritos = getLEADS.filter(el=> { return  el.folio })
		var hash = {};
		let vendedoras = contactosInscritos.filter(function(current) {
		  var exists = !hash[current.usuario_asignado];
		  hash[current.usuario_asignado] = true;
		  return exists;
		});

		// Agregarle el nombre a las vendedoras
    const usuariosERP    = await erpviejo.getUsuariosERP( ).then(response=> response)
    const cursos_escuela = await dashboardMarketing.getCursosEscuela( escuela )

    const alumnos         = await dashboardMarketing.getAlumnosInscritos( idAlumnosLeads, telefonosLeads ).then( response => response )
 
		for(const i in vendedoras){
			const { usuario_asignado } = vendedoras[i]
			// Buscar el usuario del ERP 
      const vend     = usuariosERP.find(el=> el.id_usuario == usuario_asignado)

      // Cantidad de contactos por vendedora
      const contactosVendedora = getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado}).length
      const inscritosVendedora = alumnosInscritos.filter(el=> { return  el.id_usuario_ultimo_cambio == usuario_asignado}).length
      
      if( escuela == 2){
      	vendedoras[i]['ADULTOS']   = getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.idcursos_escuela == 4}).length
      	vendedoras[i]['EXCI']      = getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.idcursos_escuela == 5}).length
      	vendedoras[i]['SIN CURSO'] = getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.idcursos_escuela == 0}).length
      }else{
      	vendedoras[i]['ADULTOS']   = getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.idcursos_escuela == 1}).length
      	vendedoras[i]['EXCI']      = getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.idcursos_escuela == 2}).length
      	vendedoras[i]['TEENS']     = getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.idcursos_escuela == 3}).length
      	vendedoras[i]['SIN CURSO'] = getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.idcursos_escuela == 0}).length
      }

      // Agregar los usuarios y nombres
      vendedoras[i] = {
        ...vendedoras[i],
        vendedora: vend  ? vend.nombre_completo : '',
        cantidadInscritos: inscritosVendedora,
        convencimiento: (( inscritosVendedora / contactosVendedora) * 100 ).toFixed(1),
        contactos: contactosVendedora,
        conInformacion: getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.idetapa == 4}).length,
        foraneos: getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.isForaneo == 1}).length,
      }
		}

		// Sacar los planteles
		var hash = {};

		let planteles = contactosInscritos.filter(function(current) {
		  var exists = !hash[current.idplantel];
		  hash[current.idplantel] = true;
		  return exists;
		});

		// Agregarle el nombre a las vendedoras
		for(const i in planteles){
			const { idplantel } = planteles[i]
      // Cantidad de contactos por plantel
      const contactosPlantel = getLEADS.filter(el=> { return  el.folio && el.idplantel == idplantel }).length
      const inscritosPlantel = getLEADS.filter(el=> { return  el.folio && el.finalizo == 1 && el.idgrupo > 0 && el.idplantel == idplantel }).length

      // Agregar los usuarios y nombres
      planteles[i] = {
        ...planteles[i],
        cantidadInscritos: inscritosPlantel,
        convencimiento: (( inscritosPlantel / contactosPlantel) * 100 ).toFixed(1),
        contactos: contactosPlantel,
      }
		}

		// Sacar los id de los contactos inscritos
		let idGruposInscritos = [0]
		for(const i in contactosInscritos){
			idGruposInscritos.push(contactosInscritos[i].idgrupo)
		}

		// consultar los cursos de los grupos en los que se inscribieron los alumnos
		const gruposInscritos = await dashboardMarketing.getGruposalumnosInscritos( idGruposInscritos ).then(response=> response)
		// Sacar los planteles
		var hash = {};
		let cursos = gruposInscritos.filter(function(current) {
		  var exists = !hash[current.id_curso];
		  hash[current.id_curso] = true;
		  return exists;
		});


		for(const i in cursos){
			const { id_curso, id_grupo } = cursos[i]
      const inscritosCurso = getLEADS.filter(el=> { return  el.folio && el.finalizo == 1 && el.idgrupo > 0 && el.idgrupo == id_grupo }).length

      // Agregar los usuarios y nombres
      cursos[i] = {
        ...cursos[i],
        cantidadInscritos: inscritosCurso,
      }
		}


    const planteles_all = await plantelesEscuela.getPlantelesActivos( ).then( response => response )
    let   planteles_list = planteles_all.filter( el => { return escuela == 2 ? el.plantel.match('FAST') :  !el.plantel.match('FAST') && !el.plantel.match('CEAA') && !el.plantel.match('CORPORA') && !el.plantel.match('Entrenamiento') })

    planteles_list.push({ id_plantel: 0, plantel: 'Sin plantel'})

    for( const i in planteles_list ){
      const { id_plantel } = planteles_list[i]

      planteles_list[i]['contactos'] = getLEADS.filter( el => { return el.id_sucursal_interes == id_plantel }).length

    }

    let campanias_list = await campanias.getCampaniasList().then( response => response );
    campanias_list.push({ idcampanias: 0, campania: 'Sin campaña'})
    

    for( const i in campanias_list ){
      const { idcampanias } = campanias_list[i]

      campanias_list[i]['contactos'] = getLEADS.filter( el => { return el.idcampanias == idcampanias }).length
    }

    res.send({
    	fuentes,
    	cursos,
    	totalLeads: getLEADS.length,
    	totalContactos: contactos,
    	desgloseLeads,
    	conversionContactos: contactos > 0 ? (( contactos / getLEADS.length) * 100 ).toFixed(1) : 0,
    	conversionInscritos: inscritos > 0 ? (( inscritos / contactos) * 100 ).toFixed(1) : 0,
    	conversionConvencimiento: inscritos > 0 ? (( inscritos / contactos) * 100 ).toFixed(1) : 0,
    	contactosFuente,
    	inscritosFuente:     inscritos,
    	vendedoras,
    	planteles,
      campanias_list,
      planteles_list,
    });

    
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor, favor de comunicarte con sistemas :p'})
  }
};

exports.dashboardMarketing = async(req, res) => {
  try {
  	const { fecha_inicio, fecha_final, escuela } = req.body
		
	  // Obtener todos los LEADS
    let getLEADS       = await dashboardMarketing.getLEADS( fecha_inicio, fecha_final, escuela ).then(response=> response)
    let contactados    = getLEADS.filter( el => { return el.folio && [4,3].includes(el.idetapa) })

    /*****************************/
    /*     C O N T A C T O S     */
    /*****************************/

    // Hay que sacar los números de celular del alumno para poder ver la vendedora
    let telefonosLeads    = contactados.filter( el => { return el.folio }).map((registro) => { return registro.telefono })
    let idAlumnosLeads    = contactados.filter( el => { return el.folio && el.idalumno > 0 }).map((registro) => { return registro.idalumno })
    idAlumnosLeads        = idAlumnosLeads.length ? idAlumnosLeads : [11111111111111]
    telefonosLeads        = telefonosLeads.length ? telefonosLeads : [11111111111111]
    // Ahora vamos a sacar a los alumnos 
    // Consultar esos alumnos finalizados y los teléfonos
    const alumnos         = await dashboardMarketing.getAlumnosInscritos( idAlumnosLeads, telefonosLeads ).then( response => response )

    const celulares       = alumnos.map((registro) => { return registro.celular })
    const telefonos       = alumnos.map((registro) => { return registro.telefono })
    const idAlumnos       = alumnos.map((registro) => { return registro.id_alumno })
    const telefonoTutor   = alumnos.map((registro) => { return registro.telefonoTutor })
    const celularTutor    = alumnos.map((registro) => { return registro.celularTutor })

    // Consultar los alumnos con esos teléfonos en los prospectos
    const idVendedoras        = contactados.map(( registro ) => { return registro.usuario_asignado })

    // Obtener el nombre de las vendedoras
    const nombreVendedoras    = idVendedoras.length ? await Kpi.getUsuariosxId( idVendedoras ).then( response => response ) : []

    // Agregamos el alumno real al prospecto
    for( const i in contactados ){
      const { telefono, idalumno } = contactados[i]
      // Buscar vendedora
      const existeContacto = alumnos.find( el => el.id_alumno == idalumno || el.celular == telefono
        || el.telefono == telefono || el.telefonoTutor == telefono || el.celularTutor == telefono )

      contactados[i]['alumno_asignado'] = existeContacto ? existeContacto.id_alumno : null
    }

    // quienes se inscribieron a un curso y cuantos no
    let alumnosVenta = contactados.filter( el => { return el.alumno_asignado })

    // De esos que tiene un curso o venta mejor dicho, es necesario saber cuantos de esos se inscribieron solo a inducción, cuando solo a curso y cuantos a ambos
    
    let idAlumnosVenta   = alumnosVenta.map((registro) => { return registro.alumno_asignado })
    idAlumnosVenta       = idAlumnosVenta.length ? idAlumnosVenta : [11111111111111]

    const gruposAlumnosInscritos = await dashboardMarketing.getGruposAlumnos( idAlumnosVenta ).then( response => response )

    // Validaremos los 3 puntos anteriores
    let totalSoloInduccion  = 0 
    let totalSoloCurso      = 0 
    let totalInduccionCurso = 0 

    for( const i in alumnosVenta ){
      const { alumno_asignado } = alumnosVenta[i]

      // Validar si tiene indudccion el alumno
      const induccion = gruposAlumnosInscritos.find( el => el.id_alumno == alumno_asignado && el.induccion )
      const curso     = gruposAlumnosInscritos.find( el => el.id_alumno == alumno_asignado && !el.induccion )

      if( induccion && !curso ){ totalSoloInduccion += 1 }
      if( !induccion && curso ){ totalSoloCurso += 1 }
      if( induccion && curso ){ totalInduccionCurso += 1 }

    }



    /*****************************/
    /*         L E A D S         */
    /*****************************/

    let  comoNosConocio = await mediocontacto.getMedioActivas().then(response=> response)

    // Hay que ver cuantos leds vienen de ahí
    for( const i in comoNosConocio ){
    	const { idmedio_contacto } = comoNosConocio[i]
    	// Datos generales
    	const leads      = getLEADS.filter( el => { return el.idmedio_contacto == idmedio_contacto  }).length
    	const contactos  = getLEADS.filter( el => { return el.idmedio_contacto == idmedio_contacto && el.folio }).length
    	const porcentaje = leads && contactos ? ((contactos/leads)*100).toFixed(0) : 0
    	// Captura de datos
    	comoNosConocio[i]['leads']      = leads
    	comoNosConocio[i]['contactos']  = contactos
    	comoNosConocio[i]['porcentaje'] = porcentaje + ' %'
    }

    // FUENTES
    // Obtener todas las fuentes activas de los leads
    // Las fuentes se dividirán en 2, las de por sucursal y las normales
    let fuentes = await dashboardMarketing.getFuentesLeads( ).then(response=>response)

    // detalles de la fuente
    let detalleFuente = await dashboardMarketing.getDetalleFuentesLeads( ).then(response=> response)

    // Recorremos las fuentes para poder indicar sus leads
    let fuentesDesglosadas = []
    for(const i in fuentes){
    	const { idfuentes, fuente } = fuentes[i]
    	// Consultar los detalles de la fuente
    	let detalles = detalleFuente.filter(el=> { return  el.idfuentes ==  idfuentes })

    	// Revisamos si no tiene detalle la fuenta
    	if( !detalles.length ){
	    	// Datos generales
	    	const leads      = getLEADS.filter( el => { return el.idfuentes == idfuentes  }).length
	    	const contactos  = getLEADS.filter( el => { return el.idfuentes == idfuentes && el.folio }).length
	    	const porcentaje = leads && contactos ? ((contactos/leads)*100).toFixed(0) : 0
	    	// Captura de datos
	    	fuentes[i]['leads']      = leads
	    	fuentes[i]['contactos']  = contactos
	    	fuentes[i]['porcentaje'] = porcentaje + ' %'
	    	fuentes[i]['detalles']   = detalles

	    	fuentesDesglosadas.push( fuentes[i] )
    	}else{
    		// Si tiene detalles, hay que convertir esos detalles en fuente, como si fuera fuente y n detalle
    		for(const j in detalles){

	    		const { iddetalle_fuentes, detalle_fuente } = detalles[j]

	    		const leads2      = getLEADS.filter( el => { return el.iddetalle_fuentes ==  iddetalle_fuentes }).length
		    	const contactos2  = getLEADS.filter( el => { return el.iddetalle_fuentes ==  iddetalle_fuentes && el.folio }).length
		    	const porcentaje2 = leads2 && contactos2 ? ((contactos2/leads2)*100).toFixed(0) : 0

	    		detalles[j]['leads']      = leads2
	    		detalles[j]['contactos']  = contactos2
	    		// Aquí creamos el nombre de la fuente
	    		detalles[j]['fuente']     = fuente + ' ' + detalle_fuente
	    		detalles[j]['porcentaje'] = porcentaje2 + ' %'

	    		fuentesDesglosadas.push( detalles[j] )
	    	}
    	}
    }


    res.send({
    	totalLeads:        getLEADS.length,
    	totalContactos:    getLEADS.filter( el => { return el.folio }).length,
    	totalInscritos:    alumnos.length,
    	totalContactados:  contactados.length,
    	contactadosInsc:   alumnosVenta.length,
    	comoNosConocio,
    	fuentesDesglosadas,
      totalSoloInduccion,
      totalSoloCurso,
      totalInduccionCurso
    })

    
  } catch (error) {
    res.status(500).send({message: error ? error.message: 'Error en el servidor' })
  }
};

exports.getDashboardMarketing2 = async(req, res) => {
  try {
  	const { fecha_inicio, fecha_final, escuela } = req.body
		
	  // Obtener todos los LEADS
    const getLEADS = await dashboardMarketing.getLEADS( fecha_inicio, fecha_final, escuela ).then(response=> response)

    // Obtener todas las fuentes activas de los leads
    let fuentes = await dashboardMarketing.getFuentesLeads( ).then(response=>response)

    // detalles de la fuente
    let detalleFuente = await dashboardMarketing.getDetalleFuentesLeads( ).then(response=> response)

    // Recorremos las fuentes para poder indicar sus leads
    for(const i in fuentes){
    	const { idfuentes } = fuentes[i]
    	// Consultar los detalles de la fuente
    	let detalles = detalleFuente.filter(el=> { return  el.idfuentes ==  idfuentes })

    	// Ahoraaa tenemos que desmenuzar jajaja los contactos y todo eso pero del detalle de la fuente
    	for(const j in detalles){
    		const { iddetalle_fuentes } = detalles[j]
    		detalles[j] = {
	    		...detalles[j],
	    		cantidad         : getLEADS.filter(el=> { return  el.iddetalle_fuentes ==  iddetalle_fuentes }).length,
	    		cantidadContactos: getLEADS.filter(el=> { return  el.iddetalle_fuentes ==  iddetalle_fuentes && el.folio}).length,
	    		cantidadInscritos: getLEADS.filter(el=> { return  el.iddetalle_fuentes ==  iddetalle_fuentes && el.finalizo == 1 && el.idgrupo > 0}).length
	    	}
    	}

    	fuentes[i] = {
    		...fuentes[i],
    		cantidad         : getLEADS.filter(el=> { return  el.idfuentes ==  idfuentes }).length,
    		cantidadContactos: getLEADS.filter(el=> { return  el.idfuentes ==  idfuentes && el.folio}).length,
    		cantidadInscritos: getLEADS.filter(el=> { return  el.idfuentes ==  idfuentes && el.finalizo == 1 && el.idgrupo > 0}).length,
    		detalles
    	}
    }

    // Ordenar las fuentes para de mayor a menor de leads
    fuentes.sort((a, b) => {
		  if (a.cantidad < b.cantidad) {
		    return 1;
		  }
		  if (a.cantidad > b.cantidad) {
		    return -1;
		  }
		  return 0;
		});

    // Total de contactos, inscritos y rechazados
    const contactos  = getLEADS.filter(el=> { return  el.folio  }).length
    const inscritos  = getLEADS.filter(el=> { return  el.folio && el.finalizo == 1 && el.idgrupo > 0 }).length
    const rechazados = getLEADS.filter(el=> { return  el.folio && el.finalizo == 1 && el.idgrupo == 0 }).length
  	
  	// DESLGOSE DE LOS LEADS
  	const desgloseLeads = [
  		{ 
  			nombre    : 'Contactos',
  			total     : contactos,
  			color     : 'success',
  			porcentaje: contactos ? (( contactos / getLEADS.length) * 100 ).toFixed(1) : 0,
  		},
  		{ 
  			nombre    : 'Inscritos',
  			total     : inscritos,
  			color     : 'error',
  			porcentaje: inscritos ? (( inscritos / getLEADS.length) * 100 ).toFixed(1) : 0,
  		},
  		{ 
  			nombre    : 'Rechazados',
  			total     : rechazados,
  			color     : 'orange',
  			porcentaje: rechazados ? (( rechazados / getLEADS.length) * 100 ).toFixed(1) : 0,
  		},
  	]

  	// Cantidad de conactos por fuente
  	let contactosFuente = []
  	for(const i in fuentes){
    	const { idfuentes } = fuentes[i]
    	const payload = {
    		...fuentes[i],
    		cantidad: getLEADS.filter(el=> { return  el.idfuentes ==  idfuentes &&  el.folio }).length
    	}

    	contactosFuente.push(payload)
    }

    // Ordenar las fuentes para de mayor a menor de leads
    contactosFuente.sort((a, b) => {
		  if (a.cantidad < b.cantidad) {
		    return 1;
		  }
		  if (a.cantidad > b.cantidad) {
		    return -1;
		  }
		  return 0;
		});


		// Sacar las vendedoras
		let contactosInscritos = getLEADS.filter(el=> { return  el.folio })
		var hash = {};
		let vendedoras = contactosInscritos.filter(function(current) {
		  var exists = !hash[current.usuario_asignado];
		  hash[current.usuario_asignado] = true;
		  return exists;
		});

		// Agregarle el nombre a las vendedoras
    const usuariosERP    = await erpviejo.getUsuariosERP( ).then(response=> response)
    const cursos_escuela = await dashboardMarketing.getCursosEscuela( escuela )
 
		for(const i in vendedoras){
			const { usuario_asignado } = vendedoras[i]
			// Buscar el usuario del ERP 
      const vend     = usuariosERP.find(el=> el.id_usuario == usuario_asignado)

      // Cantidad de contactos por vendedora
      const contactosVendedora = getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado}).length
      const inscritosVendedora = getLEADS.filter(el=> { return  el.folio && el.finalizo == 1 && el.idgrupo > 0 && el.usuario_asignado == usuario_asignado}).length
      
      if( escuela == 2){
      	vendedoras[i]['ADULTOS']   = getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.idcursos_escuela == 4}).length
      	vendedoras[i]['EXCI']      = getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.idcursos_escuela == 5}).length
      	vendedoras[i]['SIN CURSO'] = getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.idcursos_escuela == 0}).length
      }else{
      	vendedoras[i]['ADULTOS']   = getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.idcursos_escuela == 1}).length
      	vendedoras[i]['EXCI']      = getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.idcursos_escuela == 2}).length
      	vendedoras[i]['TEENS']     = getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.idcursos_escuela == 3}).length
      	vendedoras[i]['SIN CURSO'] = getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.idcursos_escuela == 0}).length
      }

      // Agregar los usuarios y nombres
      vendedoras[i] = {
        ...vendedoras[i],
        vendedora: vend  ? vend.nombre_completo : '',
        cantidadInscritos: inscritosVendedora,
        convencimiento: (( inscritosVendedora / contactosVendedora) * 100 ).toFixed(1),
        contactos: contactosVendedora,
        conInformacion: getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.idetapa == 4}).length,
        foraneos: getLEADS.filter(el=> { return  el.folio && el.usuario_asignado == usuario_asignado && el.foraneo == 1}).length,
      }


		}

		// Sacar los planteles
		var hash = {};
		let planteles = contactosInscritos.filter(function(current) {
		  var exists = !hash[current.idplantel];
		  hash[current.idplantel] = true;
		  return exists;
		});

		// Agregarle el nombre a las vendedoras
		for(const i in planteles){
			const { idplantel } = planteles[i]
      // Cantidad de contactos por plantel
      const contactosPlantel = getLEADS.filter(el=> { return  el.folio && el.idplantel == idplantel }).length
      const inscritosPlantel = getLEADS.filter(el=> { return  el.folio && el.finalizo == 1 && el.idgrupo > 0 && el.idplantel == idplantel }).length

      // Agregar los usuarios y nombres
      planteles[i] = {
        ...planteles[i],
        cantidadInscritos: inscritosPlantel,
        convencimiento: (( inscritosPlantel / contactosPlantel) * 100 ).toFixed(1),
        contactos: contactosPlantel,
      }
		}

		// Sacar los id de los contactos inscritos
		let idGruposInscritos = [0]
		for(const i in contactosInscritos){
			idGruposInscritos.push(contactosInscritos[i].idgrupo)
		}

		// consultar los cursos de los grupos en los que se inscribieron los alumnos
		const gruposInscritos = await dashboardMarketing.getGruposalumnosInscritos( idGruposInscritos ).then(response=> response)
		// Sacar los planteles
		var hash = {};
		let cursos = gruposInscritos.filter(function(current) {
		  var exists = !hash[current.id_curso];
		  hash[current.id_curso] = true;
		  return exists;
		});


		for(const i in cursos){
			const { id_curso, id_grupo } = cursos[i]
      const inscritosCurso = getLEADS.filter(el=> { return  el.folio && el.finalizo == 1 && el.idgrupo > 0 && el.idgrupo == id_grupo }).length

      // Agregar los usuarios y nombres
      cursos[i] = {
        ...cursos[i],
        cantidadInscritos: inscritosCurso,
      }
		}


		/**************************************/
		/* 			    C O N T A C T O S         */
		/**************************************/

		const contactosTotales = await dashboardMarketing.getContactos( fecha_inicio, fecha_final, escuela ).then( response => response )
		const contactosDetalle = {
			total             : contactosTotales.length,
			leads             : contactosTotales.filter( el=> { return el.idleds }).length,
			sucursal          : contactosTotales.filter( el=> { return !el.idleds }).length,
			finalizados       : contactosTotales.filter( el=> { return el.finalizo == 1 }).length,
			finalizo_sinexito : contactosTotales.filter( el=> { return el.finalizo == 1 && !el.idgrupo }).length,
			finalizo_conexito : contactosTotales.filter( el=> { return el.finalizo == 1 && el.idgrupo > 0 }).length,
		}

		/****************************/
		// Inscritos
		/****************************/
		let idalumnos  = contactosTotales.map((registro) => registro.idalumno);
		let telefonos  = contactosTotales.map((registro) => registro.telefono);

		const inscritosTotales = await dashboardMarketing.getAlumnosInscritos( idalumnos, telefonos ).then( response => response )

		res.send({
			inscritosTotales,
    	contactosDetalle
    });

    // res.send({
    // 	fuentes,
    // 	cursos,
    // 	totalLeads: getLEADS.length,
    // 	totalContactos: contactos,
    // 	desgloseLeads,
    // 	conversionContactos: contactos > 0 ? (( contactos / getLEADS.length) * 100 ).toFixed(1) : 0,
    // 	conversionInscritos: inscritos > 0 ? (( inscritos / contactos) * 100 ).toFixed(1) : 0,
    // 	conversionConvencimiento: inscritos > 0 ? (( inscritos / contactos) * 100 ).toFixed(1) : 0,
    // 	contactosFuente,
    // 	inscritosFuente:     inscritos,
    // 	vendedoras,
    // 	planteles
    // });

    
  } catch (error) {
    res.status(500).send({message:error.message})
  }
};

exports.graficasContacto = async(req, res) => {
  try {
    const { fecha_inicio, fecha_final } = req.body
    
    // Obtener todos los LEADS
    const graficaContactos = await dashboardMarketing.graficaContactos( fecha_inicio, fecha_final ).then(response=> response)
    const getFechasUnicas  = await dashboardMarketing.getFechasUnicas( fecha_inicio, fecha_final ).then(response=> response)

    const graficaContactosVendedora = await dashboardMarketing.graficaContactosVendedora( fecha_inicio, fecha_final ).then(response=> response)
    let   getVendedorasUnicas       = await dashboardMarketing.getVendedorasUnicas( fecha_inicio, fecha_final ).then(response=> response)

    const idVendedoras = getVendedorasUnicas.map((registro) => { return registro.usuario_asignado })

    const usuariosSistema = await erpviejo.getUsuariosERP( idVendedoras ).then( response => response )


    let headers2 = []

    headers2.push({text: 'Vendedora', value: 'vendedora' })

    for( const j in getFechasUnicas ){
      const { fecha_creacion } = getFechasUnicas[j] 
      headers2.push({text: fecha_creacion, value: `${fecha_creacion}`})
    }

    let dataTabla = []

    for( const i in getVendedorasUnicas ){
      const { usuario_asignado } = getVendedorasUnicas[i]

      const existeUsuario = usuariosSistema.find( el => el.id_usuario == usuario_asignado )
      getVendedorasUnicas[i]['vendedora'] = existeUsuario ? existeUsuario.nombre_completo : 'Sin nombre'

    }

    for( const i in getVendedorasUnicas ){
      const { usuario_asignado, vendedora } = getVendedorasUnicas[i]
      dataTabla.push({ vendedora })
      for( const j in getFechasUnicas ){
        const { fecha_creacion } = getFechasUnicas[j] 
        const existeConteo = graficaContactosVendedora.find( el => el.usuario_asignado == usuario_asignado && el.fecha_creacion == fecha_creacion)
        dataTabla[ dataTabla.length - 1 ][`${fecha_creacion}`] = existeConteo ? existeConteo.conteo : 0
      }
    }



    res.send({graficaContactos, getFechasUnicas, headers2, dataTabla })
    
  } catch (error) {
    res.status(500).send({message:error.message})
  }
};

exports.graficasInscritos = async(req, res) => {
  try {
    const { fecha_inicio, fecha_final } = req.body
    
    // Obtener todos los LEADS
    const fechasUnicas = await dashboardMarketing.getFechasUnicasInscritos( fecha_inicio, fecha_final ).then(response=> response)
    const inscritos    = await dashboardMarketing.getFechasInscritos( fecha_inicio, fecha_final ).then(response=> response)

    // SACAR LAS VENDEDORAS UNICAS DE INBI
    let arrayInscritos = inscritos.map(item=>{ return [item.vendedora,item] });
    // Creamos un map de los alumnos
    var alumnos = new Map(arrayInscritos); // Pares de clave y valor
    // y ahora si, extraemos los valores unicos
    let alumnosInscritos = [...alumnos.values()]; // Conversión a un array

    let datosFast = []
    let datosInbi = []

    for( const i in fechasUnicas ){
      const { fecha_creacion } = fechasUnicas[i]
      let conteoFast = inscritos.filter( el => { return el.fecha_creacion == fecha_creacion && el.unidad_negocio == 2 }).length
      datosFast.push( conteoFast )
      let conteoInbi = inscritos.filter( el => { return el.fecha_creacion == fecha_creacion && el.unidad_negocio == 1 }).length
      datosInbi.push( conteoInbi )
    }


    /* VENDEDORAS Y DATOS DE TABLA */

    let   getVendedorasUnicas       = await dashboardMarketing.getFechasInscritosVendedora( fecha_inicio, fecha_final ).then(response=> response)

    let   getVendedorasAll          = await dashboardMarketing.getVendedorasAll( ).then( response => response )

    const idVendedoras = getVendedorasUnicas.map((registro) => { return registro.iderp })

    const usuariosSistema = await erpviejo.getUsuariosERP( idVendedoras ).then( response => response )


    const inscritosVendedora  = await dashboardMarketing.getFechasInscritosVendedora( fecha_inicio, fecha_final ).then(response=> response)

    // AGREGAR VENDEDORA AL USUARIO
    for( const i in getVendedorasAll ){
      const { iderp } = getVendedorasAll[i]
      const existeUsuario = usuariosSistema.find( el => el.id_usuario == iderp )
      getVendedorasAll[i]['vendedora'] = existeUsuario ? existeUsuario.nombre_completo : 'Sin nombre'
    }


    let dataTablaInscritos = []
    // Recorremos a las vendedoras
    for( const i in getVendedorasAll ){

      const { iderp, vendedora } = getVendedorasAll[i]

      dataTablaInscritos.push({ vendedora })

      for( const j in fechasUnicas ){

        const { fecha_creacion } = fechasUnicas[j] 

        const existeConteo  = inscritos.filter( el => el.id_usuario_ultimo_cambio == iderp && el.fecha_creacion == fecha_creacion)

        dataTablaInscritos[ dataTablaInscritos.length - 1 ][`${fecha_creacion}`] = existeConteo
      }
    }

    res.send({ fechasUnicas, datosFast, datosInbi, dataTablaInscritos })
    
  } catch (error) {
    res.status(500).send({message:error.message})
  }
};

// Enviar el recibo de pago al alumno
exports.enviarcorreoConvenio = async(req, res) => {
  try {
    // const {  correo } = req.body
    // Actualizar los datos de pago

    let correos = correosConvenio.correosAEnviar()

    for( const i in correos ){
      console.log( correos[i] )
      // Preparamos el correo
      const correoHtml   = correoConvenio.creaPlantillaConvenio( )

      const correoEnviado  = await correoConvenio.enviarCorreo( 'CONVENIO INBI SCHOOL', correoHtml, correos[i], transporter2 ).then( response=> response ) 
      
      console.log( correoEnviado )
    }



    res.send({message: 'Correo enviado', correos });

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};
