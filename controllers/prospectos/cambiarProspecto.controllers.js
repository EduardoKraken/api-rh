const { response } = require("express");
const { rest } = require("lodash");
const cambiarProspecto = require("../../models/prospectos/cambiarProspecto.model.js");


exports.getProspectosAllCambiar = async (req, res) => {
  try {

    const respuesta = await cambiarProspecto.getProspectosAllCambiar( ).then(response => response)

    const usuario_asignados = respuesta.map((registro)=>{ return registro.usuario_asignado })

    const nombrevendedoras = await cambiarProspecto.getNombreVendedora( usuario_asignados ).then( response => response )
    
    for (const i in respuesta){

        const { usuario_asignado } = respuesta[i]

        const existeUsuario = nombrevendedoras.find(el=>el.id_usuario == usuario_asignado)    
        
        let nombre             = existeUsuario   ? existeUsuario.nombre_completo : "SIN NOMBRE"

        respuesta[i]['name']    = nombre            
    }
    
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};


exports.getVendedora = async (req, res) => {
  try {
    const respuesta = await cambiarProspecto.getVendedora( ).then(response => response)
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};

exports.updateProspectoVendedora = async(req, res) => {
  try {
    const { usuario_asignado, idprospectos} = req.body
    
    // Actualizar "finalizo"
    const respuesta = await cambiarProspecto.updateProspectoVendedora( usuario_asignado, idprospectos ).then(response => response) 
    res.send({message: 'Prospecto Actualizado'});
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};


//ANGEL RODRIGUEZ -- TODO