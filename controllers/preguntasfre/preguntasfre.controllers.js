const preguntasfre           = require("../../models/preguntasfre/preguntasfre.model.js");

// Agregrar una pregunta por parte del usuario
exports.addPregunta = async(req, res) => {
  try {
    // Desestructuramos el body
    const { pregunta, quien_pregunto, idpuesto } = req.body
    // Agregamos lo necesario
    const preguntaUsuario = await preguntasfre.addPregunta( pregunta, quien_pregunto, idpuesto ).then( response => response )
    // Respondemos
    res.send({message: 'Pregunta enviada correctamente'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};

// REsponder a una pregunta
exports.responderPreguntaUsuario = async(req, res) => {
  try {
    // Desestructuramos el body
    const { idpreguntas_usuarios, respuesta, quien_respondio } = req.body
    // Agregamos lo necesario
    const preguntaUsuario = await preguntasfre.responderPreguntaUsuario( idpreguntas_usuarios, respuesta, quien_respondio ).then( response => response )
    // Respondemos
    res.send({message: 'Pregunta respondida correctamente'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};

// Actualizar pregunta frecuente
exports.updatePreguntaFrecuente = async(req, res) => {
  try {
    // Desestructuramos el body
    const { idpreguntas_frecuentes, pregunta, respuesta, estatus } = req.body
    // Agregamos lo necesario
    const preguntaUsuario = await preguntasfre.updatePreguntaFrecuente( idpreguntas_frecuentes, pregunta, respuesta, estatus ).then( response => response )
    // Respondemos
    res.send({message: 'Pregunta respondida correctamente'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};

// Agregar una pregunta frecuente
exports.addPreguntaFrecuente = async(req, res) => {
  try {
    // Desestructuramos el body
    const { pregunta, respuesta, idpuesto, iderp, idpreguntas_usuarios } = req.body
    // Agregamos lo necesario
    const preguntaUsuario = await preguntasfre.addPreguntaFrecuente( pregunta, respuesta, idpuesto, iderp, idpreguntas_usuarios ).then( response => response )
    // Respondemos
    res.send({message: 'Pregunta agregada correctamente'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};

// Obteners mis preguntas
exports.misPreguntas = async(req, res) => {
  try {
    // Desestructuramos el body
    const { id } = req.params
    // Agregamos lo necesario
    const mispreguntas = await preguntasfre.misPreguntas( id ).then( response => response )
    // Respondemos
    res.send(mispreguntas);
  } catch (error) {
    res.status(500).send({message:error})
  }
};

// Obtener todas las preguntas frecuentes
exports.getPreguntasFrecuentes = async(req, res) => {
  try {
    // Agregamos lo necesario
    const preguntasAll = await preguntasfre.getPreguntasFrecuentes( ).then( response => response )
    // Obtener los id de los usuarios que registraron la pregunta
    let mapIdUsers  = preguntasAll.map((registro) => registro.quien_pregunto);
    let mapIdUsers2 = preguntasAll.map((registro) => registro.quien_respondio);

    // Obtener los usuarios de esos ID
    if( mapIdUsers.length == 0){
      return res.status(400).send({message: 'Sin preguntas'})
    }
    
    // Consultamos las preguntas de esos usuarios 
    let usuariosERP  = await preguntasfre.getUsuariosERPID( mapIdUsers ).then( response => response )
    let usuariosERP2 = await preguntasfre.getUsuariosERPID( mapIdUsers2 ).then( response => response )

    for( const i in preguntasAll ){
      // desestructuramos los usuarios 
      const { quien_pregunto, quien_respondio } = preguntasAll[i]
      // Le agregamos a cada usuario sus pregutnas
      const existeUsuario1 = usuariosERP.find( el=> el.id_usuario == quien_pregunto )
      const existeUsuario2 = usuariosERP.find( el=> el.id_usuario == quien_respondio )
      preguntasAll[i]['pregunto']  = existeUsuario1 ? existeUsuario1.nombre_completo : ''
      preguntasAll[i]['respondio'] = existeUsuario2 ? existeUsuario1.nombre_completo : ''
    }

    // Respondemos
    res.send(preguntasAll);
  } catch (error) {
    res.status(500).send({message:error})
  }
};


// Actualizar pregunta frecuente
exports.eliminarFrecuente = async(req, res) => {
  try {
    // Desestructuramos el body
    const { id } = req.params
    // Agregamos lo necesario
    const preguntaUsuario = await preguntasfre.eliminarFrecuente( id ).then( response => response )
    // Respondemos
    res.send({message: 'Pregunta elimininada correctamente'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};

// Obtener todas las preguntas de los usuarios
exports.getPreguntasUsuario = async(req, res) => {
  try {
    // Agregamos lo necesario
    let preguntasAll = await preguntasfre.getPreguntasUsuario( ).then( response => response )
    for( const i in preguntasAll ){
      preguntasAll[i]['frecuenteActive'] = false
    }
    // Obtener los id de los usuarios que registraron la pregunta
    let mapIdUsers = preguntasAll.map((registro) => registro.quien_pregunto);

    // Obtener los usuarios de esos ID
    if( mapIdUsers.length == 0){
      return res.status(400).send({message: 'Sin preguntas'})
    }
    
    // Consultamos las preguntas de esos usuarios 
    let usuariosERP = await preguntasfre.getUsuariosERPID( mapIdUsers ).then( response => response )
    // Recorremos los usuarios
    for( const i in usuariosERP ){
      // desestructuramos los usuarios 
      const { id_usuario } = usuariosERP[i]
      // Le agregamos a cada usuario sus pregutnas
      usuariosERP[i]['preguntas'] = preguntasAll.filter( el=> { return el.quien_pregunto == id_usuario })
    }

    // retornamos los usuarios con sus preguntas
    res.send(usuariosERP);
  } catch (error) {
    res.status(500).send({message:error})
  }
};

