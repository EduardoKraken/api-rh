//declaramos en una const nuestro model
const NivelesUsuario = require("../models/niveles_usuario.model.js");

exports.getNivelesUsuario = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  NivelesUsuario.getNivelesUsuario((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los niveles del usuario"
      });
    else res.send(data);
  });
};

exports.getNivelesUsuarioId = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  NivelesUsuario.getNivelesUsuarioId(req.params.idusuario,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los niveles del usuario"
      });
    else res.send(data);
  });
};

exports.addNivelesUsuario = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  NivelesUsuario.addNivelesUsuario(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el nivel del usuario"
      })
    else res.status(200).send({ message: `El nivel del usuario se ha creado correctamente` });
  })
};

exports.updateNivelesUsuario = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }

  NivelesUsuario.updateNivelesUsuario(req.params.idniveles_usuario, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro el nivel del usuario con id : ${req.params.idniveles_usuario}` });
      } else {
        res.status(500).send({ message: "Error al actualizar el nivel del usuario con el id : " + req.params.idniveles_usuario });
      }
    } else {
      res.status(200).send({ message: `El nivel del usuario se ha actualizado correctamente.` })
    }
  })
}

exports.deleteNivelesUsuario = (req, res) => {
  NivelesUsuario.deleteNivelesUsuario(req.params.idniveles_usuario, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.idniveles_usuario}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el nivel usuario con el id " + req.params.idniveles_usuario
        });
      }
    } else res.send({ message: `El nivel usuario se elimino correctamente!` });
  });
};

/******************************************************************************************************************************/
/******************************************************************************************************************************/
/******************************************************************************************************************************/
exports.getNiveles = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  NivelesUsuario.getNiveles((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los niveles del usuario"
      });
    else res.send(data);
  });
};
exports.addNiveles = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  NivelesUsuario.addNiveles(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el nivel del usuario"
      })
    else res.status(200).send({ message: `El nivel del usuario se ha creado correctamente` });
  })
};

exports.updateNiveles = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }

  NivelesUsuario.updateNiveles(req.params.idniveles, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro el nivel del usuario con id : ${req.params.idniveles}` });
      } else {
        res.status(500).send({ message: "Error al actualizar el nivel del usuario con el id : " + req.params.idniveles });
      }
    } else {
      res.status(200).send({ message: `El nivel del usuario se ha actualizado correctamente.` })
    }
  })
}
