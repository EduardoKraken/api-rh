//declaramos en una const nuestro model
const DisponibilidadUsuario = require("../models/disponibilidad_usuario.model.js");

exports.getDisponibilidadUsuario = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  DisponibilidadUsuario.getDisponibilidadUsuario((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las disponibilidad del usuario"
      });
    else res.send(data);
  });
};

exports.getDisponibilidadUsuarioId = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  DisponibilidadUsuario.getDisponibilidadUsuarioId(req.params.idusuario,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las disponibilidad del usuario"
      });
    else res.send(data);
  });
};

exports.addDisponibilidadUsuario = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  DisponibilidadUsuario.addDisponibilidadUsuario(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la disponibilidad del usuario"
      })
    else res.status(200).send({ message: `la disponibilidad del usuario se ha creado correctamente` });
  })
};

exports.updateDisponibilidadUsuario = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }

  DisponibilidadUsuario.updateDisponibilidadUsuario(req.params.iddisponibilidad_usuario, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro la disponibilidad del usuario con id : ${req.params.iddisponibilidad_usuario}` });
      } else {
        res.status(500).send({ message: "Error al actualizar la disponibilidad del usuario con el id : " + req.params.iddisponibilidad_usuario });
      }
    } else {
      res.status(200).send({ message: `la disponibilidad del usuario se ha actualizado correctamente.` })
    }
  })
}

exports.deleteDisponibilidadUsuario = (req, res) => {
  DisponibilidadUsuario.deleteDisponibilidadUsuario(req.params.iddisponibilidad_usuario, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.iddisponibilidad_usuario}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre la disponibilidad usuario con el id " + req.params.iddisponibilidad_usuario
        });
      }
    } else res.send({ message: `la disponibilidad usuario se elimino correctamente!` });
  });
};
