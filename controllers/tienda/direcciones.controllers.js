
const Direcciones = require("../../models/tienda/direcciones.model.js");

// DIRECCIONES DE ENVIO
exports.agregar_direccion_cliente = (req, res) => {
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Direcciones.agregar_direccion_cliente(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear la dirección de envío"
  		})
  	else res.status(200).send({ message:'La dirección de envío se guardo correctamente.'})

  })
};
exports.direcciones_cliente = (req, res)=>{
    Direcciones.direcciones_cliente(req.params.idcliente,(err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo algún erro al recuperar las direcciónes del cliente."
				});
				else res.send(data);
    });
};
exports.actualiza_direccion_envio = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Direcciones.actualiza_direccion_envio( req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre la direccion con el id ${req.data.iddireccion }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar la direccion con el id" + req.data.iddireccion 
				});
			}
		} 
  	else res.status(200).send({ message:'La dirección de envío se guardo correctamente.'})
	});
};
exports.cambiar_direccion_envio_activa = (req, res) => {
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Direcciones.cambiar_direccion_envio_activa(req.body, (err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear la dirección de envío"
  		})
  	else res.status(200).send({ message:'Dirección predeterminada actualizada '})

  })
};

// DIRECCIONES DE FACTURACION 
exports.agregar_direccion_cliente_facturacion = (req, res) => {
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Direcciones.agregar_direccion_cliente_facturacion(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear la dirección de envío"
  		})
  	else res.status(200).send({ message:'La dirección de envío se guardo correctamente.'})

  })
};
exports.direcciones_cliente_facturacion = (req, res)=>{
    Direcciones.direcciones_cliente_facturacion(req.params.idcliente,(err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo algún erro al recuperar las direcciónes del cliente."
				});
				else res.send(data);
    });
};
exports.actualiza_direccion_facturacion = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Direcciones.actualiza_direccion_facturacion( req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre la direccion con el id ${req.data.idfacturacion }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar la direccion con el id" + req.data.idfacturacion 
				});
			}
		} 
  	else res.status(200).send({ message:'La dirección de envío se guardo correctamente.'})
	});
};
exports.cambiar_direccion_facturacion_activa = (req, res) => {
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Direcciones.cambiar_direccion_facturacion_activa(req.body, (err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear la dirección de envío"
  		})
  	else res.status(200).send({ message:'Dirección predeterminada actualizada '})

  })
};












// Delete a users with the specified usersId in the request
exports.deleteDireccion = (req, res) => {
  Direcciones.deleteDireccion(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el usuario con el id " + req.params.id
        });
      }
    } else res.send({ message: `El usuario se elimino correctamente!` });
  });
};