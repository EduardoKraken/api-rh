
const Depositos = require("../../models/tienda/depositos.model.js");

// Crear un cliente
exports.addDepositos = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Depositos.addDepositos(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};

exports.getDepositosId = (req, res)=>{
    Depositos.getDepositosId(req.params.idpagoxfac,(err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo algún erro al recuperar los grupos por escuelas."
				});
				else res.send(data);
    });
};

exports.getDepositos = (req,res) => {
    Depositos.getDepositos(req.params.idfacturas,(err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los clientes"
        });
      else res.send(data);
    });
};

exports.updateDepositos = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Depositos.updateDepositos( req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre el cliente con el id `
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar el cliente con el id "
				});
			}
		} 
		else res.send(data);
	}
	);
}







