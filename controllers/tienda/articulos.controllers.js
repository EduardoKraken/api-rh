const Articulos = require("../../models/tienda/articulos.model.js");
// const Fotos = require("../../models/tienda/articulos.model.js");
const cursos      = require("../../models/capacitacion/cursos.model.js");
const planteles = require("../../models/catalogos/planteles.model");
var articulos = []

exports.obtener_articulos_random = (req, res)=>{
  Articulos.obtener_articulos_random((err,data)=>{
    if(err){
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
    }else {
      res.status(200).send(data);
    }
      
  });
};

// Crear un cliente
exports.addArticulos = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Articulos.addArticulos(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};


exports.findAll =  (req,res)=>{
  Articulos.getAll((err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
      // else res.send(data);
    }else res.send(data);
  });
};


exports.getArticuloId =  (req,res)=>{
  Articulos.getArticuloId(req.params.id,(err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo un error, contácta a sistemas"
      });
      // else res.send(data);
    }else res.send(data);
  });
};


exports.getArticulosMovil =  (req,res)=>{
  Articulos.getAll((err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
      // else res.send(data);
    }else{
      res.send(data)
    } 
  });
};

fotos = (callback) =>{
	Articulos.fotosArt((err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
    }else{
    	callback(data);
    }
  });
};

exports.getArticuloCodigo = (req, res)=>{
    Articulos.getArticuloCodigo(req.params.codigo,(err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo un error."
				});
      else res.send(data);
				
    });
};

exports.getArticuloTienda = (req, res)=>{
    Articulos.getArticuloTienda(req.params.codigo,(err,data)=>{
      if(err)
        res.status(500).send({
          message:
            err.message || "Se produjo un error."
        });
        else res.send(data);
    });
};


exports.updateArticulos = (req, res) =>{
 if (!req.body) {
   res.status(400).send({
     message: "El Contenido no puede estar vacio!"
   });
 }
  
 Articulos.updateArticulos(req.params.id, req.body ,(err, data) => {
   if (err) {
     if (err.kind === "not_found") {
       res.status(404).send({
       message: `No encontre el articulo con el id ${req.params.id }.`
       });
     } else {
       res.status(500).send({ message: "Error al actualizar el articulo con el id " + req.params.id });
     }
   } 
   else res.send(data);
 });
};

// Traer fotos por articulo
exports.fotosxArt = (req, res)=>{
    Articulos.fotosxArt(req.params.codigo,(err,data)=>{
      if(err)
        res.status(500).send({
          message:
            err.message || "Se produjo un error."
        });
        else res.send(data);
    });
};

// Agregar fotos por codigo
exports.addFoto = (req, res) => {
  // Validacion de request
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  // // console.log(req.body)
  // req.body.forEach(element=>{
    // Guardar el CLiente en la BD
    Articulos.addFoto(req.body, (err, data)=>{
      // EVALUO QUE NO EXISTA UN ERROR
      if(err)
        res.status(500).send({
          message:
          err.message || "Se produjo algún error al crear el cliente"
        })
      else res.send(data)
    // })
  })
};

exports.updateNovedades = (req, res) =>{
 if (!req.body) {
   res.status(400).send({
     message: "El Contenido no puede estar vacio!"
   });
 }
  
 Articulos.updateNovedades(req.params.id, req.body ,(err, data) => {
   if (err) {
     if (err.kind === "not_found") {
       res.status(404).send({
       message: `No encontre el articulo con el id ${req.params.id }.`
       });
     } else {
       res.status(500).send({
       message: "Error al actualizar el articulo con el id" + req.params.id 
       });
     }
   } 
   else res.send(data);
 });
};


exports.getArticulosActivo = (req, res)=>{
  Articulos.getArticulosActivo((err,data)=>{
    if(err)
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
      else res.send(data);
  });
};


exports.getNovedades = (req, res)=>{
    Articulos.getNovedades((err,data)=>{
      if(err)
        res.status(500).send({
          message:
            err.message || "Se produjo un error."
        });
        fotos((result)=> {
          articulos = []
          data.forEach((element, index) =>{
            articulos.push({             
              id            : element.id,
              nomart        : element.nomart,
              codigo        : element.codigo,
              idlaboratorio : element.idlaboratorio,
              nomlab        : element.nomlab,
              descrip       : element.descrip,
              statusweb     : element.statusweb,
              precio1       : element.precio1,
              sal           : element.sal,
              destacados    : element.destacados,
              novedades     : element.novedades,
              fotos         : [],
              pjedesc       : element.pjedesc,
            });

            result.forEach(element2 =>{
              if(element.codigo == element2.codigo){
                // console.log('entre')
                articulos[index].fotos.push(element2)
                // console.log('articulo con foto: ',articulos[index])
              }             
            })

            if(index == data.length -1){
              res.send(articulos)
            }
          })
        });
    });
};

exports.getDestacados = (req, res)=>{
  Articulos.getDestacados((err,data)=>{
    if(err)
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
      fotos((result)=> {
        articulos = []
        data.forEach((element, index) =>{
          articulos.push({             
            id            : element.id,
            nomart        : element.nomart,
            codigo        : element.codigo,
            idlaboratorio : element.idlaboratorio,
            nomlab        : element.nomlab,
            descrip       : element.descrip,
            statusweb     : element.statusweb,
            precio1       : element.precio1,
            sal           : element.sal,
            destacados    : element.destacados,
            novedades     : element.novedades,
            fotos         : [],
            pjedesc       : element.pjedesc,
          });

          result.forEach(element2 =>{
            if(element.codigo == element2.codigo){
              // console.log('entre')
              articulos[index].fotos.push(element2)
              // console.log('articulo con foto: ',articulos[index])
            }             
          })

          if(index == data.length -1){
            res.send(articulos)
          }
        })
      });
  });
};

exports.getPromociones = (req, res)=>{
  Articulos.getPromociones((err,data)=>{
    if(err)
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
      fotos((result)=> {
        articulos = []
        data.forEach((element, index) =>{
          articulos.push({             
           id            : element.id,
           nomart        : element.nomart,
           codigo        : element.codigo,
           idlaboratorio : element.idlaboratorio,
           nomlab        : element.nomlab,
           descrip       : element.descrip,
           statusweb     : element.statusweb,
           precio1       : element.precio1,
           sal           : element.sal,
           fotos         : [],
           pjedesc       : element.pjedesc,
          });

          result.forEach(element2 =>{
            if(element.codigo == element2.codigo){
              // console.log('entre')
              articulos[index].fotos.push(element2)
              // console.log('articulo con foto: ',articulos[index])
            }             
          })

          if(index == data.length -1){
            res.send(articulos)
          }
        })
      });
  });
};

exports.getArtxLab = (req, res)=>{
  Articulos.getArtxLab(req.params.id,(err,data)=>{
    if(err)
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
      fotos((result)=> {
        articulos = []
        data.forEach((element, index) =>{
          articulos.push({             
           id            : element.id,
           nomart        : element.nomart,
           codigo        : element.codigo,
           idlaboratorio : element.idlaboratorio,
           nomlab        : element.nomlab,
           descrip       : element.descrip,
           statusweb     : element.statusweb,
           precio1       : element.precio1,
           sal           : element.sal,
           fotos         : [],
           pjedesc       : element.pjedesc,
          });

          result.forEach(element2 =>{
            if(element.codigo == element2.codigo){
              // console.log('entre')
              articulos[index].fotos.push(element2)
              // console.log('articulo con foto: ',articulos[index])
            }             
          })

          if(index == data.length -1){
            res.send(articulos)
          }
        })
      });
  });
};


// Delete a users with the specified usersId in the request
exports.deleteFoto = (req, res) => {
  Articulos.deleteFoto(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el usuario con el id " + req.params.id
        });
      }
    } else res.send({ message: `El usuario se elimino correctamente!` });
  });
};


/*******************************************************************************/
exports.updateFotoPrincipal = (req, res) =>{
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  
 Articulos.updateFotoPrincipal(req.params.id, req.body ,(err, data) => {
  if (err) {
    if (err.kind === "not_found") {
      res.status(404).send({
        message: `No encontre el articulo con el id ${req.params.id }.`
      });
    } else {
      res.status(500).send({
        message: "Error al actualizar el articulo con el id" + req.params.id 
      });
    }
  } 
  else res.send(data);
 });
};


exports.getArticuloFotos =  (req, res)=>{
  Articulos.getArticuloFotos(req.params.codigo,(err,data)=>{
    if(err){
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
    }else{
      res.send(data);
    }
  });
};


exports.getArticuloFam =  (req, res)=>{
  Articulos.getArticuloFam(req.params.id,(err,data)=>{
    if(err){
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
    }else{
      res.send(data);
    }
  });
};

exports.getArticuloCat =  (req, res)=>{
  Articulos.getArticuloCat(req.params.id,(err,data)=>{
    if(err){
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
    }else{
      res.send(data);
    }
  });
};

exports.getArticuloSub =  (req, res)=>{
  Articulos.getArticuloSub(req.params.id,(err,data)=>{
    if(err){
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
    }else{
      res.send(data);
    }
  });
};


exports.addVenta =  async (req, res)=>{
  try{
    const { idplantel, iderp, articulos } = req.body 
    // // Consultar todos los tickets
    let addSolicitud     = await Articulos.addSolicitud(  idplantel, iderp ).then( response => response )

    for( const i in articulos ){
      const { id, foto, cantidad } = articulos[i]

      const departamentos = await Articulos.addDesgloseSolicitud( addSolicitud.id, id, foto, cantidad).then( response => response )
    }

    // Falta consultar las areas, los jefes de area y consultar los auxiliares
    res.send(articulos);
  }catch ( error ) {
    res.status(500).send({ message: error.message || "Se produjo algún error al recuperar los Tickets" });
  }
};


exports.getSolicitudesCompra =  async (req, res)=>{
  try{
    // Consultar todas las solicitudes
    let getSolicitudes     = await Articulos.getSolicitudes(  ).then( response => response )

    // Solicitar todos los desgloses
    let getDesglose        = await Articulos.getDesglose(  ).then( response => response )
    
    // Tenemos que buscar esos usuarios y sus nombres
    const usuariosERP = await cursos.usuariosERP( ).then(response=> response)

    // Consultar solo los grupos activos
    const getPlantelesActivos = await planteles.getPlantelesActivos( ).then(response=> response)

    for( const i in getSolicitudes ){
      const { idsolicitudes, iderp, idplantel } = getSolicitudes[i]

      const existeUsuario = usuariosERP.find( el => el.id_usuario == iderp )
      const existePlantel = getPlantelesActivos.find( el => el.id_plantel == idplantel )

      getSolicitudes[i]['empleado']  = existeUsuario ? existeUsuario.nombre_completo : ''
      getSolicitudes[i]['plantel']   = existePlantel ? existePlantel.plantel : ''

      getSolicitudes[i]['articulos'] = getDesglose.filter( el=> { return el.idsolicitudes == idsolicitudes })
    }

    // Falta consultar las areas, los jefes de area y consultar los auxiliares
    res.send(getSolicitudes);
  }catch ( error ) {
    res.status(500).send({ message: error.message || "Se produjo algún error al recuperar los Tickets" });
  }
};


exports.eliminarSolicitud =  async (req, res)=>{
  try{
    // // Consultar todos los tickets
    const { id } = req.params
    let eliminarSolicitud     = await Articulos.eliminarSolicitud( id ).then( response => response )

    // Falta consultar las areas, los jefes de area y consultar los auxiliares
    res.send({message: 'Solicitud eliminada correctamente'});
  }catch ( error ) {
    res.status(500).send({ message: error.message || "Se produjo algún error al recuperar los Tickets" });
  }
};

exports.aceptarSolicitud =  async (req, res)=>{
  try{
    // // Consultar todos los tickets
    const { id } = req.params
    let aceptarSolicitud     = await Articulos.aceptarSolicitud( id, 1 ).then( response => response )

    // Falta consultar las areas, los jefes de area y consultar los auxiliares
    res.send({message: 'Solicitud aceptada correctamente'});
  }catch ( error ) {
    res.status(500).send({ message: error.message || "Se produjo algún error al recuperar los Tickets" });
  }
};


exports.recibirPedido =  async (req, res)=>{
  try{
    // // Consultar todos los tickets
    const { iddesgloce_solicitud, cantidad, cantidad_recibida, estatus } = req.body

    const updateDeslgose = await Articulos.updateDeslgose( cantidad_recibida, estatus, iddesgloce_solicitud ).then( response => response )

    // Falta consultar las areas, los jefes de area y consultar los auxiliares
    res.send({message: 'Actualización del pedido correcto :p'});
  }catch ( error ) {
    res.status(500).send({ message: error.message || "Se produjo algún error al recuperar los Tickets" });
  }
};


exports.entregaArticulos =  async (req, res)=>{
  try{
    // // Consultar todos los tickets
    const { articulos, idsolicitudes, estatus } = req.body
    let aceptarSolicitud     = await Articulos.aceptarSolicitud( idsolicitudes, estatus ).then( response => response )
    // Falta consultar las areas, los jefes de area y consultar los auxiliares
    res.send({message: 'Solicitud actualizada correctamente'});
  }catch ( error ) {
    res.status(500).send({ message: error.message || "Se produjo algún error al recuperar los Tickets" });
  }
};

exports.diaHabilCompra =  async (req, res)=>{
  try{
    // // Consultar todos los tickets
    let diaDeCompra     = await Articulos.diaActual(  ).then( response => response )
    if( !diaDeCompra ){ return res.status(500).send({ message: 'Día NO hábil para comprar' }) }

    const { dia } = diaDeCompra

    if( dia != 2 ){ return res.status(500).send({ message: 'Día NO hábil para comprar' }) }
    // Falta consultar las areas, los jefes de area y consultar los auxiliares
    res.send({message: 'Día hábil para comprar'});
  }catch ( error ) {
    res.status(500).send({ message: error.message || "Se produjo algún error al recuperar los Tickets" });
  }
};