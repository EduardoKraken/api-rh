const Planteles = require("../models/planteles.model.js");

exports.all_planteles = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Planteles.all_planteles((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los planteles"
            });
        else res.send(data);
    });
};

exports.add_planteles = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    Planteles.add_planteles(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear los planteles"
            })
        else res.status(200).send({ message: `El plantel se ha creado correctamente` });
    })
};

exports.update_plantel = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Planteles.update_plantel(req.params.idplanteles, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro el plantel con id : ${req.params.idplanteles}` });
            } else {
                res.status(500).send({ message: "Error al actualizar el plantel con el id : " + req.params.idplanteles });
            }
        } else {
            res.status(200).send({ message: `El plantel se ha actualizado correctamente.` })
        }
    })
}