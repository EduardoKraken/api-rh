const Unidad_negocio = require("../models/unidad_negocio.model.js");

exports.all_unidad_negocio = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Unidad_negocio.all_unidad_negocio((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar las unidades de negocio"
            });
        else res.send(data);
    });
};

exports.add_unidad_negocio = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    Unidad_negocio.add_unidad_negocio(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la unidad de negocio"
            })
        else res.status(200).send({ message: `La unidad de negocio se ha creado correctamente` });
    })
};

exports.update_unidad_negocio = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Unidad_negocio.update_unidad_negocio(req.params.idunidad_negocio, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro la unidad de negocio con id : ${req.params.idunidad_negocio}` });
            } else {
                res.status(500).send({ message: "Error al actualizar la unidad de negocio con el id : " + req.params.idunidad_negocio });
            }
        } else {
            res.status(200).send({ message: `La unidad de negocio se ha actualizado correctamente.` })
        }
    })
}