const Roles = require("../models/roles.model.js");

// Obtener todos los ciclos  activos
exports.getCiclos = (req, res) => {
  Roles.getCiclos((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

exports.getGruposRoles = (req, res) => {
  Roles.getGruposRoles(req.params.idciclo,req.params.idciclo2, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};


exports.getTeachersRoles = (req, res) => {
  Roles.getTeachersRoles((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};