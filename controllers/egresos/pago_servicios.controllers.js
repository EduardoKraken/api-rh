const egresos   = require("../../models/egresos/pago_servicios.model.js");

// Enviar el recibo de pago al alumno
exports.generarPagoServicios = async(req, res) => {
  try {

    for( const j in req.body ){
    	const { detalles, id_usuario_ultimo_cambio, registro } = req.body[j]
	    // REQUISICIÓN DE COMPRA
	    let idrequisicion_compra = 0

	    if( !registro ){
	      const addRequiOperaciones = await egresos.addRequisicionOperaciones( req.body[j] ).then( response=> response ) 

	      idrequisicion_compra = addRequiOperaciones.idrequisicion_compra     
	    }else{
	      idrequisicion_compra = registro
	    }

	    // LLENAR EL DETALLE DE LA REQUISICIÓN
	    for( const i in detalles ){
	      const addDetalleRequiOper = await egresos.addDetalleRequiOper( idrequisicion_compra, detalles[i], id_usuario_ultimo_cambio ).then( response=> response ) 
	      const { iddetalle_requi } = addDetalleRequiOper
	      // fyn_proc_egresos
	      const addFyn_proc_egresosOper = await egresos.addFyn_proc_egresosOper( iddetalle_requi, id_usuario_ultimo_cambio ).then( response=> response ) 

	    }
    }


    res.send({message: 'Correo enviado', ...req.body, idrequisicion_compra:0 });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.consultarPagoServicios = async(req, res) => {
  try {

    const { fecha_inicio, fecha_final } = req.body

    // REQUISICIÓN DE COMPRA
    const consultarPagoServicios = await egresos.consultarPagoServicios( fecha_inicio, fecha_final ).then( response=> response ) 

    // Sacarlos los id
    let ids = consultarPagoServicios.map(( registro ) => { return registro.id })
    ids = ids.length ? ids : [0]
    
    // CONSULTAR EL DETALLE DE LA REQUISICIÓN
    const detallesRequi = await egresos.consultarDetalleRequiId( ids ).then( response => response )

    for( const i in consultarPagoServicios ){
      const { id } = consultarPagoServicios[i]

      const detalles = detallesRequi.filter( el => { return el.id_requisicion_compra == id })
      consultarPagoServicios[i]['detalle'] = detalles

      let nomina = []
      for( const j in detalles ){
        nomina.push({
          'Nómina':   detalles[j].nomina,
          'Empleado': detalles[j].empleado,
          'Cuenta':   detalles[j].cuenta,
          'Monto':    detalles[j].costo_total
        })
      }

      consultarPagoServicios[i]['nomina'] = nomina

    } 

    res.send( consultarPagoServicios );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


