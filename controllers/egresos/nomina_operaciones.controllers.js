const egresos   = require("../../models/egresos/nomina_operaciones.model.js");
const { v4: uuidv4 } = require('uuid')
var util = require('util');

// Enviar el recibo de pago al alumno
exports.generarNominaOperaciones = async(req, res) => {
  try {

    const { detalles, id_usuario_ultimo_cambio, registro, fotos} = req.body

    // REQUISICIÓN DE COMPRA
    let idrequisicion_compra = 0

    if( !registro ){
      const addRequiOperaciones = await egresos.addRequisicionOperaciones( req.body ).then( response=> response ) 

      idrequisicion_compra = addRequiOperaciones.idrequisicion_compra     
    }else{
      idrequisicion_compra = registro
    }

    // LLENAR EL DETALLE DE LA REQUISICIÓN
    for( const i in detalles ){

      const addDetalleRequiOper = await egresos.addDetalleRequiOper( idrequisicion_compra, detalles[i], id_usuario_ultimo_cambio ).then( response=> response ) 
      const { iddetalle_requi } = addDetalleRequiOper

      // fyn_proc_egresos
      const addFyn_proc_egresosOper = await egresos.addFyn_proc_egresosOper( iddetalle_requi, id_usuario_ultimo_cambio ).then( response=> response ) 

    }
   
    for( const i in fotos ){
      let nombre_base = fotos[i].nombre_base;
      let name = fotos[i].name;
      let extension = fotos[i].extension;
    
      let fotoSubida   = await egresos.addFotoCotizaciones( nombre_base, name, idrequisicion_compra, id_usuario_ultimo_cambio, extension ).then( response => response )
    }

    res.send({message: 'Exito en el proceso', ...req.body, idrequisicion_compra });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.subirFotosCotizaciones= async(req, res) => {
  try {

    let nombreArchivos = []

    // desestrucutramos los arvhios cargados
    let { file  } = req.files

    if(!file.length){
      file = [file]
    }

    for( const i in file ){
      const nombreSplit = file[i].name.split('.')
      const name = file[i].name
      
      // Obtener la extensión del archivo 
      const extensioArchivo = nombreSplit[ nombreSplit.length - 1 ]
      
      // modificar el nombre del archivo con un uuid unico
      const nombreUuid = uuidv4() + '.' + extensioArchivo
      
      // extensiones validas
      let extensiones  = [ 'BMP', 'GIF', 'jpg', 'jpeg', 'png', 'webp', 'pdf', 'PDF' ]
      let ruta         = './../../comprobantes-egresos/' + nombreUuid 

      let foto = {extension: extensioArchivo, name: nombreUuid, nombre_base: name}

      nombreArchivos.push( foto )

      // validar que la extensión del archivo recibido sea valido
      if( !extensiones.includes( extensioArchivo) )
        return res.status( 400 ).send({message: `Hola, la extensión: ${ extensioArchivo } no es valida, solo se aceptan: ${ extensiones }`})
      

      file[i].mv(`${ruta}`, async ( err ) => {
        if( err )
          return res.status( 500 ).send({ message: err ? err : 'Error' })
      })
    }

    
    res.send( nombreArchivos );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};



exports.consultarNominaOperaciones = async(req, res) => {
  try {

    const { fecha_inicio, fecha_final } = req.body

    // REQUISICIÓN DE COMPRA
    const consultarNominaOperaciones = await egresos.consultarNominaOperaciones( fecha_inicio, fecha_final ).then( response=> response ) 

    // Sacarlos los id
    let ids = consultarNominaOperaciones.map(( registro ) => { return registro.id })
    ids = ids.length ? ids : [0]
    
    // CONSULTAR EL DETALLE DE LA REQUISICIÓN
    const detallesRequi = await egresos.consultarDetalleRequiId( ids ).then( response => response )

    for( const i in consultarNominaOperaciones ){
      const { id } = consultarNominaOperaciones[i]

      const detalles = detallesRequi.filter( el => { return el.id_requisicion_compra == id })
      consultarNominaOperaciones[i]['detalle'] = detalles

      let nomina = []
      for( const j in detalles ){
        nomina.push({
          'Nómina':   detalles[j].nomina,
          'Empleado': detalles[j].empleado,
          'Cuenta':   detalles[j].cuenta,
          'Monto':    detalles[j].costo_total
        })
      }

      consultarNominaOperaciones[i]['nomina'] = nomina

    } 
      
    res.send( consultarNominaOperaciones );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getMisRequisiciones = async(req, res) => {
  try {

    const { fecha_inicio, fecha_final, id_usuario } = req.body

    // REQUISICIÓN DE COMPRA
    const misRequisiciones = await egresos.getMisRequisiciones( fecha_inicio, fecha_final, id_usuario ).then( response=> response ) 

    // Sacarlos los id
    let ids = misRequisiciones.map(( registro ) => { return registro.id })
    ids = ids.length ? ids : [0]
    
    // CONSULTAR EL DETALLE DE LA REQUISICIÓN
    const detallesRequi = await egresos.consultarDetalleRequiId( ids ).then( response => response )

    let idsRequis     = detallesRequi.map(( registro ) => { return registro.id })
    idsRequis = idsRequis.length ? idsRequis : [0]

    // CONSULTAR LAS FOTOS DE LAS REQUIS
    const getFotoRequi = await egresos.getFotoRequi( idsRequis ).then( response => response )
    const getFotoRequi2 = await egresos.getFotoRequi( ids ).then( response => response )

    // AGREGARLE A CADA DETALLE SUS FOTOS
    for( const i in detallesRequi ){
      const { id } = detallesRequi[i]

      let fotosCoti = getFotoRequi.filter( el => { return el.id_egresos == id && el.tipo == 1 })
      let fotosPago = getFotoRequi.filter( el => { return el.id_egresos == id && el.tipo == 2 })

      detallesRequi[i]['fotos']       = fotosCoti
      detallesRequi[i]['fotosPago']   = fotosPago
      detallesRequi[i]['foto_unica']  = fotosCoti.length ? fotosCoti[0].name : null
      detallesRequi[i]['imagen_pago'] = fotosPago.length ? fotosPago[0].name : null
    }

    // Llenar las requisiciones con sus detalles
    for( const i in misRequisiciones ){
      const { id } = misRequisiciones[i]

      const detalles = detallesRequi.filter( el => { return el.id_requisicion_compra == id })

      misRequisiciones[i]['detalle'] = detalles

      misRequisiciones[i]['total'] = detalles.filter( el => { return el.activo_sn == 1 }).map(item => item.costo_total).reduce((prev, curr) => prev + curr, 0)

    } 

    for( const i in misRequisiciones ){
      const { id } = misRequisiciones[i]


      misRequisiciones[i]['fotos'] = getFotoRequi2.filter( el => { return el.id_egresos == id })
    }
    
    const respuesta = misRequisiciones.filter(el=>{ return el.detalle.length > 0})
    res.send( respuesta );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getRequisiciones = async(req, res) => {
  try {

    const { fecha_inicio, fecha_final, ciclo } = req.body

    // REQUISICIÓN DE COMPRA
    const misRequisiciones = await egresos.getRequisiciones( fecha_inicio, fecha_final, ciclo ).then( response=> response ) 

    // Sacarlos los id
    let ids = misRequisiciones.map(( registro ) => { return registro.id })
    ids = ids.length ? ids : [0]
    
    // CONSULTAR EL DETALLE DE LA REQUISICIÓN
    const detallesRequi = await egresos.consultarDetalleRequiId( ids ).then( response => response )

    let idsRequis     = detallesRequi.map(( registro ) => { return registro.id })
    idsRequis = idsRequis.length ? idsRequis : [0]

    // CONSULTAR LAS FOTOS DE LAS REQUIS
    const getFotoRequi = await egresos.getFotoRequi( ids ).then( response => response )
    
    // AGREGARLE A CADA DETALLE SUS FOTOS
    for( const i in detallesRequi ){
      const { id } = detallesRequi[i]

      let fotosCoti = getFotoRequi.filter( el => { return el.id_egresos == id })
      // let fotosPago = getFotoRequi.filter( el => { return el.id_egresos == id && el.tipo == 2 })

      detallesRequi[i]['fotos']       = fotosCoti
      // detallesRequi[i]['fotosPago']   = fotosPago
      detallesRequi[i]['foto_unica']  = fotosCoti.length ? fotosCoti[0].name : null
      // detallesRequi[i]['imagen_pago'] = fotosPago.length ? fotosPago[0].name : null
    }

    // // Llenar las requisiciones con sus detalles
    for( const i in misRequisiciones ){
      const { id } = misRequisiciones[i]

      const detalles = detallesRequi.filter( el => { return el.id_requisicion_compra == id })

      misRequisiciones[i]['detalle'] = detalles

      misRequisiciones[i]['total'] = detalles.filter( el => { return el.activo_sn == 1 }).map(item => item.costo_total).reduce((prev, curr) => prev + curr, 0)

    } 


    for( const i in misRequisiciones ){
      const { id } = misRequisiciones[i]


      misRequisiciones[i]['fotos'] = getFotoRequi.filter( el => { return el.id_egresos == id })
    }

    const respuesta = misRequisiciones.filter(el=>{ return el.detalle.length > 0})
    res.send( respuesta );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getElementosCompra = async(req, res) => {
  try {
    // REQUISICIÓN DE COMPRA
    const getElementosCompra = await egresos.getElementosCompra( ).then( response=> response ) 

    res.send( getElementosCompra );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getTipoRequisiciones = async(req, res) => {
  try {
    // REQUISICIÓN DE COMPRA
    const tipoRequisiciones = await egresos.getTipoRequisiciones( ).then( response=> response ) 

    res.send( tipoRequisiciones );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.subirArchivosRequi = async(req, res) => {
  try {

    // desestrucutramos los arvhios cargados
    const { file  } = req.files
    const { requi, id_usuario_subio, tipo_pago } = req.body 

    // Si no tiene archivo se debe agregar como quiera
    if( !file || !requi.length ){
      return res.status( 400 ).send({ message : 'El contenido no puede ir vacio' })
    }
    
    const nombreSplit = file.name.split('.')
    
    // Obtener la extensión del archivo 
    const extensioArchivo = nombreSplit[ nombreSplit.length - 1 ]
    
    // modificar el nombre del archivo con un uuid unico
    const nombreUuid = uuidv4() + '.' + extensioArchivo
    
    // extensiones validas
    let extensiones  = [ 'BMP', 'GIF', 'jpg', 'jpeg', 'png', 'webp', 'xls', 'pdf' ]
    let ruta         = './../../comprobantes-egresos/' + nombreUuid

    // validar que la extensión del archivo recibido sea valido
    if( !extensiones.includes( extensioArchivo) )
      return res.status( 400 ).send({message: `Hola, la extensión: ${ extensioArchivo } no es valida, solo se aceptan: ${ extensiones }`})
    

    file.mv(`${ruta}`, async ( err ) => {
      if( err )
        return res.status( 500 ).send({ message: err ? err : 'Error' })

      let requisiciones = JSON.parse(JSON.stringify(requi)).split(',')

      for( const i in requisiciones ){
        const payload = {
          name_base  : file.name,
          name       : nombreUuid,
          tipo       : tipo_pago,
          id_egresos : requisiciones[i],
          extension  : extensioArchivo,
          id_usuario_subio,
        }

        const addArchivoEgreso = await egresos.addArchivoEgreso( payload ).then( response => response )
      }

      return res.send({ message: 'Datos grabados correctamente' })
    })
    
    // res.send({message: 'Correo enviado' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.subirComprobantePartida = async(req, res) => {
  try {

    // desestrucutramos los arvhios cargados
    //contiene la informacion de la foto
    const { file  } = req.files
    //contiene las id de las partidas
    let { requi, id_partida, sueldo1, sueldo2, comisiones, caja_de_ahorro, a_depositar, dep_1 ,dep_2, deuda} = req.body 


    // Si no tiene archivo se debe agregar como quiera
    if( !file || !requi.length ){
      return res.status( 400 ).send({ message : 'El contenido no puede ir vacio' })
    }
    
    const nombreSplit = file.name.split('.')
    
    // Obtener la extensión del archivo 
    const extensioArchivo = nombreSplit[ nombreSplit.length - 1 ]
    
    // modificar el nombre del archivo con un uuid unico
    const nombreUuid = uuidv4() + '.' + extensioArchivo
    
    // extensiones validas
    let extensiones  = [ 'BMP', 'GIF', 'jpg', 'jpeg', 'png', 'webp', 'xls', 'pdf' ]
    let ruta         = './../../comprobantes-egresos/' + nombreUuid

    // validar que la extensión del archivo recibido sea valido
    if( !extensiones.includes( extensioArchivo) )
      return res.status( 400 ).send({message: `Hola, la extensión: ${ extensioArchivo } no es valida, solo se aceptan: ${ extensiones }`})
    

    file.mv(`${ruta}`, async ( err ) => {
      if( err )
        return res.status( 500 ).send({ message: err ? err : 'Error' })

      let requisiciones = JSON.parse(JSON.stringify(requi)).split(',')
  

      for( const i in requisiciones ){
        const payload = {
          name_base  : file.name,
          name       : nombreUuid,
          id_partida : requisiciones[i],
          extension  : extensioArchivo,

        }

        const updateDeleteComprobante = await egresos.updateDeleteComprobante( requisiciones[i] ).then( response=> response ) 
        const addArchivoEgreso = await egresos.addComprobantePartida( payload ).then( response => response )
        const updateEstatusPartida = await egresos.updateEstatusPartida( requisiciones[i]).then( response=> response ) 
        const addComentarioComprobante= await egresos.addComentarioComprobante( requisiciones[i], sueldo1, sueldo2, comisiones, caja_de_ahorro, a_depositar, dep_1 ,dep_2, deuda ).then( response => response )

      }

      return res.send({ message: 'Datos grabados correctamente' })
    })
    
    // res.send({message: 'Correo enviado' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.eliminarPartida = async(req, res) => {
  try {

    const { id } = req.params

    // REQUISICIÓN DE COMPRA
    const eliminarPartida = await egresos.eliminarPartida( id ).then( response=> response ) 

    res.send({ message: 'Partida eliminada correctamente' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.preAutorizarRequi = async(req, res) => {
  try {

    const { id_usuario_autoriza_rechaza, id_requisicion_compra_estatus, id_requisicion } = req.body

    // REQUISICIÓN DE COMPRA
    const preAutorizarRequi = await egresos.preAutorizarRequi( id_usuario_autoriza_rechaza, id_requisicion_compra_estatus, id_requisicion ).then( response=> response ) 

    res.send({ message: 'Requisición actualizada correctamente' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateEstatusRequi = async(req, res) => {
  try {

    const { id_requisicion_compra_estatus, id_requisicion } = req.body

    // REQUISICIÓN DE COMPRA
    const updateEstatusRequi = await egresos.updateEstatusRequi( id_requisicion_compra_estatus, id_requisicion ).then( response=> response ) 

    res.send({ message: 'Requisición actualizada correctamente' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateEstatusPartida = async(req, res) => {
  try {

    const {  id } = req.body

    // REQUISICIÓN DE COMPRA
    const updateEstatusPartida = await egresos.updateEstatusPartida( id ).then( response=> response ) 

    res.send({ message: 'Partida actualizada correctamente' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateRechazarPartidaRequi = async(req, res) => {
  try {

    const {  id } = req.body

    // REQUISICIÓN DE COMPRA
    if (Array.isArray( id )) {
      for (const i in id) {
        const updateRechazarPartidaRequi = await egresos.updateRechazarPartidaRequi( id[i] ).then( response=> response ) 
      }
      } else {
        const updateRechazarPartidaRequi = await egresos.updateRechazarPartidaRequi( id ).then( response=> response ) 
      }

    res.send({ message: 'Partida actualizada correctamente' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateEstatusPartidaRevision = async(req, res) => {
  try {
    const { id } = req.body
    if (Array.isArray( id )) { 
      for (const i in id) {
        const updateEstatusPartidaRevision = await egresos.updateEstatusPartidaRevision( id[i] ).then( response=> response ) 
      }
    } else {
      const updateEstatusPartidaRevision = await egresos.updateEstatusPartidaRevision( id ).then( response=> response )  
    }
      res.send({ message: 'Partida actualizada correctamente' });
  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateAceptarComprobante = async(req, res) => {
  try {

    const {  id } = req.body
    // REQUISICIÓN DE COMPRA
    if (Array.isArray( id )) {
    for (const i in id) {
       const updateAceptarComprobante = await egresos.updateAceptarComprobante( id[i] ).then( response=> response ) 
    }
    } else {
       const updateAceptarComprobante = await egresos.updateAceptarComprobante( id ).then(response => response)
    }
    res.send({ message: 'Partida actualizada correctamente' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateTipoPago = async(req, res) => {
  try {

    const { tipo_pago, id_requisicion_compra } = req.body

    // REQUISICIÓN DE COMPRA
    const updateTipoPago = await egresos.updateTipoPago( tipo_pago, id_requisicion_compra ).then( response=> response ) 

    res.send({ message: 'Requisición actualizada correctamente' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.consultarPartidasEgreso = async (req, res,) => {
  try {
    const { ciclo, fecha_inicio, fecha_final } = req.body

    if( !ciclo  && !fecha_inicio ){ return res.status(400).send({ message: 'Favor de seleccionar un ciclo o una fecha válida' }) }

    //Trae los datos para llenar la tabla de egresos
    const respuesta = await egresos.consultarPartidasEgresos( ciclo, fecha_inicio, fecha_final).then(response => response)

    let ids = respuesta.map(( registro ) => { return registro.id })

    ids = ids.length  ? ids : [0]

    //Trae los datos de las fotos de de comprobantes de egresos
    const getFotoPartida = await egresos.getFotoPartida( ids ).then( response => response )
  

    for( const i in respuesta ){
      const { id } = respuesta[i]

      let fotosPago = getFotoPartida.filter( el => { return el.id_partida == id })
      
      respuesta[i]['fotosPago']   = fotosPago
      respuesta[i]['imagen_pago'] = fotosPago.length ? fotosPago[0].name : null
    }
  
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};

exports.subirPostCotizaciones = async(req, res) => {
  try {

    const { fotos, idrequisicion_compra, id_usuario_ultimo_cambio }  = req.body
   
    for( const i in fotos ){
      let nombre_base = fotos[i].nombre_base;
      let name = fotos[i].name;
      let extension = fotos[i].extension;
    
      let fotoSubida   = await egresos.addFotoCotizaciones( nombre_base, name, idrequisicion_compra, id_usuario_ultimo_cambio, extension ).then( response => response )
    }


    res.send({message: 'Exito en el proceso', ...req.body, idrequisicion_compra });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.actualizarRequisiciones = async(req, res) => {
  try {

    const {id_requisicion_compra, id_elemento_requisicion, id_plantel, concepto, cantidad, costo_unitario, costo_total, id_usuario_ultimo_cambio, id_empleado  } = req.body

   
      const addDetalleRequiOper = await egresos.actualizarRequisicionPartida( id_requisicion_compra, id_elemento_requisicion, id_plantel, concepto, cantidad, costo_unitario, costo_total, id_usuario_ultimo_cambio, id_empleado ).then( response=> response ) 
      console.log(addDetalleRequiOper)
      const {iddetalle_requi}  = addDetalleRequiOper
    
      const addFyn_proc_egresosOper = await egresos.addFyn_proc_egresosOper( iddetalle_requi, id_usuario_ultimo_cambio ).then( response=> response ) 

  
    res.send({message: 'Exito en el proceso', ...req.body });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.addegresoMensaje = async(req, res) => {
  try{
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }
    //Extraemos los datos
    const { id_partida, id, mensaje } = req.body;

    //Verificamos si es un array
    if (Array.isArray(id_partida) && id_partida.length > 0 && Array.isArray(id) && id.length > 0) {
      //Hacemos un for para insertar los datos uno por uno
      for (let i in id_partida) {
        let datoArray1 = id_partida[i];
        let datoArray2 = id[i];
        //Dividmos los datos y los enviamos como "datoInsert"
        let datoInsert = {
          id_partida: datoArray1,
          id: datoArray2,
          mensaje: mensaje
        };
        //Aquí mero
        await egresos.addegresoMensaje(datoInsert).then(response => response)
      }
      res.status(200).send({ message: `El mensaje se ha creado correctamente` });
    } else {
      //En caso de que no sea un array se envia de manera normal
      await egresos.addegresoMensaje(req.body).then(response => response)
      res.status(200).send({ message: `El mensaje se ha creado correctamente` });
    }
  } catch(error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};

exports.consultarMensajesRechazoEgreso = async (req, res,) => {
  try {
    const { id_partida } = req.body

    //Trae los datos para llenar la tabla de egresos
    const respuesta = await egresos.consultarMensajesRechazoEgreso( id_partida ).then(response => response)

    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};

exports.updateDeleteComprobante = async(req, res) => {
  try {

    const {  id } = req.body

    // REQUISICIÓN DE COMPRA
    const updateDeleteComprobante = await egresos.updateDeleteComprobante( id ).then( response=> response ) 

    res.send({ message: 'Partida actualizada correctamente' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateMensajeRechazoRespuesta = async(req, res) => {
  try {
    const { respuesta, id_partida, id} = req.body
        const updateMensajeRechazoRespuesta = await egresos.updateMensajeRechazoRespuesta( respuesta, id_partida, id ).then( response=> response ) 
   
      res.send({ message: 'Partida actualizada correctamente' });
  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getComentarioComprobante = async(req, res) => {
  try {
 
    const { id_partida } = req.body

    const getComentarioComprobante = await egresos.getComentarioComprobante( id_partida ).then( response=> response ) 

    res.send( getComentarioComprobante );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.addElementoRequisicion = async(req, res) => {
  try{

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    const { elemento_mantenimiento, id_usuario_ultimo_cambio } = req.body;

    await egresos.addElementoRequisicion(elemento_mantenimiento, id_usuario_ultimo_cambio).then(response => response)
      
    res.status(200).send({ message: `El elemento se ha creado correctamente` });
    
    
  } catch(error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};


exports.getProyectos = async(req, res) => {
  try {

    const getProyectos = await egresos.getProyectos( ).then( response=> response ) 

    res.send( getProyectos );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.updateProyectos = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }
  egresos.updateProyectos(req.params.id, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(400).send({ message: `No se encontro el proyecto con id : ${req.params.id}, ${ err }` });
      } else {
        res.status(400).send({ message: `Error al actualizar el proyecto con el id : ${req.params.id}, ${ err } `});
      }
    } else {
      res.status(200).send({ message: `El proyecto se ha actualizado correctamente.` })
    }
  })
};

exports.addProyectos = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  egresos.addProyectos(req.body, (err, data) => {
    if (err)
      res.status(400).send({
        message: err.message || "Se produjo algún error al crear el proyecto"
      })
    else res.status(200).send({ message: `El proyecto se creo correctamente` });
  })
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//26/03/23//
exports.updateMandarEgreso = async(req, res) => {
  try {

    const { tipo_pago, id, id_usuario_autoriza_rechaza, id_requisicion_compra_estatus, id_requisicion } = req.body

    for( const i in id ){
      
      const updateMandarEgreso = await egresos.updateMandarEgreso( tipo_pago, id[i] ).then( response=> response ) 

    }
  
    //Transformar el array de objetos a numerico
    const sin_procesar_partidas = await egresos.getSinProcesarPartidas( id_requisicion ).then( response=> response ) 

    //"partida" no significa nada 
    const hayDatos = sin_procesar_partidas.filter(partida =>{ return partida != null && partida != undefined });
    //

    console.log( sin_procesar_partidas )

    if(hayDatos.length <= 0){

      console.log('7')

      const preAutorizarRequi = await egresos.preAutorizarRequi( id_usuario_autoriza_rechaza, id_requisicion_compra_estatus, id_requisicion ).then( response=> response )

    }
  
    res.send({ message: 'Requisición actualizada correctamente' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

exports.getCiclosEgresos = async(req, res) => {
  try {

    const getCiclosEgresos = await egresos.getCiclosEgresos(  ).then( response=> response ) 

    res.send( getCiclosEgresos );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


<<<<<<< HEAD
exports.updatePartida = async(req, res) => {
  try {

    const {  id_elemento_requisicion, id_plantel, concepto, cantidad, costo_unitario, costo_total, id } = req.body

    // REQUISICIÓN DE COMPRA
    const updatePartida = await egresos.updatePartida( id_elemento_requisicion, id_plantel, concepto, cantidad, costo_unitario, costo_total, id ).then( response=> response ) 

    res.send({ message: 'Partida actualizada correctamente' });
=======

exports.solicitarCambioEgreso = async(req, res) => {
  try {

    const solicitarCambioEgreso = await egresos.solicitarCambioEgreso( req.body ).then( response=> response ) 

    res.send({ message: 'Cambio solicitado de manera correcta' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateCambioEgreso = async(req, res) => {
  try {

    const { id } = req.params

    const updateCambioEgreso = await egresos.updateCambioEgreso( req.body, id ).then( response=> response ) 

    res.send({ message: 'Cambio actualizado de manera correcta' });
>>>>>>> Kraken

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

<<<<<<< HEAD

exports.updateRequi = async(req, res) => {
  try {

    const {  id_ciclo, comentarios, id } = req.body

    // REQUISICIÓN DE COMPRA
    const updateRequi = await egresos.updateRequi( id_ciclo, comentarios, id ).then( response=> response ) 

    res.send({ message: 'Requisición actualizada correctamente' });
=======
exports.getEgresosEliminados = async(req, res) => {
  try {

    const getEgresosEliminados = await egresos.getEgresosEliminados( ).then( response=> response ) 

    res.send( getEgresosEliminados );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.procesarCambioEgreso = async(req, res) => {
  try {

    // 21728,   3314

    const { nuevo_ciclo, eliminar, nuevo_concepto, idautorizo, idcambios_egresos, id_requisicion_compra, id } = req.body

    // Modiifcar que ya se autorizo
    const procesarCambioEgreso = await egresos.updateCambioEgreso( idautorizo, 1, 0, idcambios_egresos  ).then( response=> response ) 

    if( nuevo_ciclo ){ 
      const updateRequiOPartida = await egresos.updateCicloRequi( nuevo_ciclo, id_requisicion_compra ).then( response=> response ) 
    }
    
    if( eliminar ){ 
      const updateRequiOPartida = await egresos.eliminarPartida( id ).then( response=> response ) 
    }
    
    if( nuevo_concepto ){ 
      const updateRequiOPartida = await egresos.updateConcepto( nuevo_concepto, id ).then( response=> response ) 
    }


    res.send( procesarCambioEgreso );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.procesarCambioEgresoRechazado = async(req, res) => {
  try {

    // 21728,   3314

    const { idautorizo, idcambios_egresos } = req.body

    // Modiifcar que ya se autorizo
    const procesarCambioEgreso = await egresos.updateCambioEgreso( idautorizo, 2, 0, idcambios_egresos  ).then( response=> response ) 

    res.send( procesarCambioEgreso );
>>>>>>> Kraken

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

<<<<<<< HEAD
=======
exports.papeleraEgresos = async(req, res) => {
  try {

    //Trae los datos para llenar la tabla de egresos
    const respuesta = await egresos.papeleraEgresos( ).then(response => response)

    res.send(respuesta)

  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};
>>>>>>> Kraken
