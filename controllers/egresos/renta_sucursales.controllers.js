const egresos   = require("../../models/egresos/renta_sucursales.model.js");


exports.getCiclosRenta = async(req, res) => {
  try {
    // REQUISICIÓN DE COMPRA
    const ciclosRentaFast = await egresos.ciclosRenta(`AND ciclo LIKE '%FE%'`).then( response=> response ) 
    const ciclosRentaInbi = await egresos.ciclosRenta(`AND ciclo NOT LIKE '%FE%'`).then( response=> response ) 

    res.send({ ciclosRentaFast, ciclosRentaInbi });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

// Enviar el recibo de pago al alumno
exports.generarRentaSucursales = async(req, res) => {
  try {

    for( const j in req.body ){
    	const { detalles, id_usuario_ultimo_cambio, registro } = req.body[j]
	    // REQUISICIÓN DE COMPRA
	    let idrequisicion_compra = 0

	    if( !registro ){
	      const addRequiOperaciones = await egresos.addRequisicionOperaciones( req.body[j] ).then( response=> response ) 

	      idrequisicion_compra = addRequiOperaciones.idrequisicion_compra     
	    }else{
	      idrequisicion_compra = registro
	    }

	    // LLENAR EL DETALLE DE LA REQUISICIÓN
	    for( const i in detalles ){
	      const addDetalleRequiOper = await egresos.addDetalleRequiOper( idrequisicion_compra, detalles[i], id_usuario_ultimo_cambio ).then( response=> response ) 
	      const { iddetalle_requi } = addDetalleRequiOper
	      // fyn_proc_egresos
	      const addFyn_proc_egresosOper = await egresos.addFyn_proc_egresosOper( iddetalle_requi, id_usuario_ultimo_cambio ).then( response=> response ) 

	    }
    }


    res.send({message: 'Correo enviado', ...req.body, idrequisicion_compra:0 });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.consultarRentaSucursales = async(req, res) => {
  try {

    const { fecha_inicio, fecha_final } = req.body

    // REQUISICIÓN DE COMPRA
    const consultarRentaSucursales = await egresos.consultarRentaSucursales( fecha_inicio, fecha_final ).then( response=> response ) 

    // Sacarlos los id
    let ids = consultarRentaSucursales.map(( registro ) => { return registro.id })
    ids = ids.length ? ids : [0]
    
    // CONSULTAR EL DETALLE DE LA REQUISICIÓN
    const detallesRequi = await egresos.consultarDetalleRequiId( ids ).then( response => response )

    for( const i in consultarRentaSucursales ){
      const { id } = consultarRentaSucursales[i]

      const detalles = detallesRequi.filter( el => { return el.id_requisicion_compra == id })
      consultarRentaSucursales[i]['detalle'] = detalles

      let nomina = []
      for( const j in detalles ){
        nomina.push({
          'Nómina':   detalles[j].nomina,
          'Empleado': detalles[j].empleado,
          'Cuenta':   detalles[j].cuenta,
          'Monto':    detalles[j].costo_total
        })
      }

      consultarRentaSucursales[i]['nomina'] = nomina

    } 

    res.send( consultarRentaSucursales );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


