const prospectos = require("../../models/lms/prospectoslms.model.js");
const md5 = require('md5');

// Obtener el listado de codigos
exports.addProspectosLMS = async(req, res) => {
	try {
		let { cicloFast, cicloInbi, fecha } = req.body
		let id_ciclo_fast   = 0
		let id_grupo_fast   = 0
		let id_usuario_fast = 0
		let id_ciclo_inbi   = 0
		let id_grupo_inbi   = 0
		let id_usuario_inbi = 0
		// Paso por paso
	  // Paso # 1 validar que el ciclo exista
	  const existecicloFast = await prospectos.getExisteCicloFast( cicloFast ).then(response=> response)
	  if( !existecicloFast ){
	  	const cicloFASTERP = await prospectos.getcicloERP( cicloFast ).then(response => response)
	  	let add_ciclo = {
		    nombre:               cicloFASTERP.ciclo,
		    estatus:              0,
		    fecha_inicio: 				cicloFASTERP.fecha_inicio_ciclo.substr(0,10),
		    fecha_fin:    				cicloFASTERP.fecha_fin_ciclo.substr(0,10),
		    fecha_inicio_excluir: null,
		    fecha_fin_excluir: 		null,
		    comentarios:          'sin comentarios',
		    usuario_creo:         1,
		    usuario_modifico:     1,
		    deleted:              0,
		    iderp:                cicloFASTERP.id_ciclo
	  	}

	  	const addCiclo = await prospectos.addCicloFast( add_ciclo ).then(response=> response)
	  	id_ciclo_fast = addCiclo.id_ciclo
	  }else{
	  	id_ciclo_fast = existecicloFast.id
	  }

	  // Aquí ya tenemos ahora si el id_ciclo_fast lleno con el id_ciclo correspondiente
	  // Paso #2 Buscar el grupo  de inducción de ese ciclo

	  const existegrupoFast = await prospectos.getGrupoFast ( id_ciclo_fast ) .then(response => response)
	  if( !existegrupoFast ){
	  	let add_grupo_fast = {
		    nombre:               'FAST_INDUCCION - CICLO NA FE - 10_00 AM a 11_00 A-M - NIVEL 1',
		    estatus:              0,
		    id_unidad_negocio:    0,
		    id_plantel:           14,
		    id_ciclo:             id_ciclo_fast,
		    id_curso:             1,
		    id_horario:           0,
		    id_salon:             1,
		    id_nivel:             1,
		    codigo_acceso:        0,
		    cupo:                 20,
		    precio:               0,
		    inscritos:            0,
		    disponibles:          0,
		    descripcion:          'Sin descripcion',
		    usuario_creo:         0,
		    usuario_modifico:     1,
		    url_imagen:           '',
		    deleted:              0,
		    iderp:                0,
		    editado:              0, 
		    optimizado:           0,
		    id_zona:              0
	  	}
	  	const addGrupoFast = await prospectos.addGrupoFast( add_grupo_fast ).then(response=> response)
	  	id_grupo_fast = addGrupoFast.id_grupo
	  }else{
	  	id_grupo_fast = existegrupoFast.id
	  }

	  // Hasta aquí el grupo ya esta creado y ya tienes el ciclo ( id_ciclo_fast, id_grupo_fast)
	  // Paso #3, Consultar los folios de de la fecha solicitada, como usuario
  	const alumnosFast = await prospectos.getAlumnosFASTERP ( fecha ).then(response => response)

  	for(const i in alumnosFast){
  		// Validamos si el usuario existe
  		const existeAlumnoFast = await prospectos.getAlumnoFAST( alumnosFast[i].folio ).then(response=> response)
  		// si no existe hay que agregarlo
  		if(!existeAlumnoFast){
  			// Preparamos el usuario
  			let add_usuario_fast = {
  				nombre: alumnosFast[i].nombres,
  				apellido_paterno: alumnosFast[i].apellidos,
  				apellido_materno: '',
  				usuario: alumnosFast[i].folio,
  				password: md5(alumnosFast[i].folio),
  				email:'',
  				estado: 1,
  				telefono:0,
  				movil: 0,
  				id_direccion:0,
  				roll:1,
  				id_tipo_usuario: 1,
  				deleted: 0,
  				iderp:0,
  				contra_universal: md5('12345r2'),
  				id_plantel: 2
  			}

  			// Agregamos al usuario
  			const addUsuarioFast = await prospectos.addUsuarioFast( add_usuario_fast ).then(response=>response)

  			// Preparamos el alumno
  			if(addUsuarioFast){
  				let add_alumno_fast = {
  					id_usuario: addUsuarioFast.id_usuario,
  					matricula:  alumnosFast[i].folio,
  					id_plantel: 1,
  					id_unidad_negocio: 1,
  					comentarios:'',
  					fecha_creacion: "2021-05-13 18:44:13",
  					fecha_ultimo_cambio:"2021-05-13 18:44:13",
  					deleted: 0
  				}
  				// Agregamos al alumno
  				const addAlumnoFast = await prospectos.addAlumnoFast( add_alumno_fast ).then(response => response)
  				id_usuario_fast = addUsuarioFast.id_usuario
  			}
  		}else{
  			id_usuario_fast = existeAlumnoFast.id
  		}
  		// Ahora ya por ultimo, le asignamos el grupo
  		// ( id_ciclo_fast, id_grupo_fast, id_usuario_fast)
  		const existeGrupoAlumnoFast = await prospectos.getAlumnoGrupo ( id_grupo_fast, id_usuario_fast ).then(response => response)
  		if( !existeGrupoAlumnoFast ){
  			let add_grupo_alumno_fast = {
  				id_grupo: id_grupo_fast,
  				id_curso: 1,
  				id_alumno: id_usuario_fast,
  				deleted:0,
  				estatus:1
  			}

  			const addGrupoAlumno = await prospectos.addGrupoAlumnoFast( add_grupo_alumno_fast ).then(response=> response)
  		}
  	}

  	/************************************************************************************************/
  	/************************************************************************************************/
  	/************************************************************************************************/
  	// Paso # 1 validar que el ciclo exista
	  const existecicloInbi = await prospectos.getExisteCicloInbi( cicloInbi ).then(response=> response)
	  if( !existecicloInbi ){
	  	const cicloINBIERP = await prospectos.getcicloERP( cicloInbi ).then(response => response)
	  	let add_ciclo_inbi = {
		    nombre:               cicloINBIERP.ciclo,
		    estatus:              0,
		    fecha_inicio: 				cicloINBIERP.fecha_inicio_ciclo.substr(0,10),
		    fecha_fin:    				cicloINBIERP.fecha_fin_ciclo.substr(0,10),
		    fecha_inicio_excluir: null,
		    fecha_fin_excluir: 		null,
		    comentarios:          'sin comentarios',
		    usuario_creo:         1,
		    usuario_modifico:     1,
		    deleted:              0,
		    iderp:                cicloINBIERP.id_ciclo

	  	}

	  	const addCicloInbi = await prospectos.addCicloInbi( add_ciclo_inbi ).then(response=> response)
	  	id_ciclo_inbi = addCicloInbi.id_ciclo
	  }else{
	  	id_ciclo_inbi = existecicloInbi.id
	  }

	  // Aquí ya tenemos ahora si el id_ciclo_inbi lleno con el id_ciclo correspondiente
	  // Paso #2 Buscar el grupo  de inducción de ese ciclo

	  const existegrupoInbi = await prospectos.getGrupoInbi ( id_ciclo_inbi ) .then(response => response)
	  if( !existegrupoInbi ){
	  	let add_grupo_inbi = {
		    nombre:               'INBI_INDUCCION - CICLO NA - 10_00 AM a 11_00 A-M - NIVEL 1',
		    estatus:              0,
		    id_unidad_negocio:    0,
		    id_plantel:           14,
		    id_ciclo:             id_ciclo_inbi,
		    id_curso:             1,
		    id_horario:           0,
		    id_salon:             1,
		    id_nivel:             1,
		    codigo_acceso:        0,
		    cupo:                 20,
		    precio:               0,
		    inscritos:            0,
		    disponibles:          0,
		    descripcion:          'Sin descripcion',
		    usuario_creo:         0,
		    usuario_modifico:     1,
		    url_imagen:           '',
		    deleted:              0,
		    iderp:                0,
		    editado:              0, 
		    optimizado:           0,
		    id_zona:              0
	  	}
	  	const addGrupoInbi = await prospectos.addGrupoInbi( add_grupo_inbi ).then(response=> response)
	  	id_grupo_inbi = addGrupoInbi.id_grupo
	  }else{
	  	id_grupo_inbi = existegrupoInbi.id
	  }

	  // Hasta aquí el grupo ya esta creado y ya tienes el ciclo ( id_ciclo_inbi, id_grupo_inbi)
	  // Paso #3, Consultar los folios de de la fecha solicitada, como usuario
  	const alumnosInbi = await prospectos.getAlumnosInbiERP ( fecha ).then(response => response)

  	for(const i in alumnosInbi){
  		// Validamos si el usuario existe
  		const existeAlumnoInbi = await prospectos.getAlumnoINBI( alumnosInbi[i].folio ).then(response=> response)
  		// si no existe hay que agregarlo
  		if(!existeAlumnoInbi){
  			// Preparamos el usuario
  			let add_usuario_inbi = {
  				nombre: alumnosInbi[i].nombres,
  				apellido_paterno: alumnosInbi[i].apellidos,
  				apellido_materno: '',
  				usuario: alumnosInbi[i].folio,
  				password: md5(alumnosInbi[i].folio),
  				email:'',
  				estado: 1,
  				telefono:0,
  				movil: 0,
  				id_direccion:0,
  				roll:1,
  				id_tipo_usuario: 1,
  				deleted: 0,
  				iderp:0,
  				contra_universal: md5('12345r2'),
  				id_plantel: 2
  			}

  			// Agregamos al usuario
  			const addUsuarioInbi = await prospectos.addUsuarioInbi( add_usuario_inbi ).then(response=>response)

  			// Preparamos el alumno
  			if(addUsuarioInbi){
  				let add_alumno_inbi = {
  					id_usuario: addUsuarioInbi.id_usuario,
  					matricula:  alumnosInbi[i].folio,
  					id_plantel: 1,
  					id_unidad_negocio: 1,
  					comentarios:'',
  					fecha_creacion: "2021-05-13 18:44:13",
  					fecha_ultimo_cambio:"2021-05-13 18:44:13",
  					deleted: 0
  				}
  				// Agregamos al alumno
  				const addAlumnoInbi = await prospectos.addAlumnoInbi( add_alumno_inbi ).then(response => response)
  				id_usuario_inbi = addUsuarioInbi.id_usuario
  			}
  		}else{
  			id_usuario_inbi = existeAlumnoInbi.id
  		}
  		// Ahora ya por ultimo, le asignamos el grupo
  		// ( id_ciclo_inbi, id_grupo_inbi, id_usuario_inbi)
  		const existeGrupoAlumnoInbi = await prospectos.getAlumnoGrupoInbi( id_grupo_inbi, id_usuario_inbi ).then(response => response)
  		if( !existeGrupoAlumnoInbi ){
  			let add_grupo_alumno_inbi = {
  				id_grupo: id_grupo_inbi,
  				id_curso: 1,
  				id_alumno: id_usuario_inbi,
  				deleted:0,
  				estatus:1
  			}

  			const addGrupoAlumnoInbi = await prospectos.addGrupoAlumnoInbi( add_grupo_alumno_inbi ).then(response=> response)
  		}
  	}

  	let respuesta = {
  		alumnosFast,
  		alumnosInbi
  	}
		res.send(respuesta);
  } catch (error) {
		res.status(500).send({message:error})
  }
};

