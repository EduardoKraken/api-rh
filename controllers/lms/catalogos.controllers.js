const catalogos        = require("../../models/lms/catalogoslms.model.js");
const calificaciones   = require("../../models/operaciones/calificaciones.model.js");
const reprobados       = require("../../models/operaciones/reprobados.model.js");
const prospectos       = require("../../models/prospectos/prospectos.model.js");
const cicloserpactivos = require("../../models/catalogos/ciclos.model");


/***************************************************/
/********************CRON GENERAL*******************/
/***************************************************/


exports.cargaCatalagos = async(req, res) => {
	try {

		const { id_ciclo, id_ciclo_relacionado } = req.body

		let cicloFast = 0
		let cicloInbi = 0

		// consultar los ciclos primero, para sacar de quien es cada ciclo
		const cicloErpFast = await catalogos.getCicloEscuela( id_ciclo_relacionado, 2 ).then( response => response )
		const cicloErpInbi = await catalogos.getCicloEscuela( id_ciclo, 1 ).then( response => response )

		// Un ciclo no existe hay que cargarlos
		if( !cicloErpFast || !cicloErpInbi){
			/* 
				CARGAR HORARIOS CICLOS
			*/

			const ciclos = await cronCiclos( ).then( response => response )
		}

		cicloFast = id_ciclo_relacionado
		cicloInbi = id_ciclo

		/* 
			CARGAR HORARIOS AL ERP
		*/

		const horarios = await cronHorarios( ).then( response => response )


		/* 
			CARGAR GRUPOS CICLOS
		*/

		const grupos = await cronGrupos( cicloFast, cicloInbi ).then( response => response )


		/* 
			CARGAR TEACHERS
		*/

		const teachers = await cronTeaches( ).then( response => response )

		res.send({ message: 'Datos cargados correctamente' });

  } catch (error) {
		res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};


/* CRON PARA CARGAR LOS HORARIOS AL ERP */
cronHorarios = ( ) => {
  return new Promise(async (resolve,reject)=>{
    try {

			// Obtenemos los horarios del ERP
			const horariosErp = await catalogos.getHorariosLMSERP( ).then(response=> response)
			// Hacemos una peticion async para obtener los datos de ambas bases de datos
			const horariosFast = await catalogos.getHorariosLMSFAST( 2 ).then(response => response)

			// Verificamos que horario del ERP no esta en FAST
			for(const i in horariosErp){
				// Buscamos si ya esta el horario
				const horario = horariosFast.find( el => el.iderp == horariosErp[i].id_horario )
				// Si no esta, se dede insertar
				if(!horario){
					const horariosAddFast = await catalogos.horariosAddFast(horariosErp[i], 2).then(response => response)
				}
			}

			// Consultamos los horarios de inbi
			const horariosInbi = await catalogos.getHorariosLMSFAST( 1 ).then(response => response)
			// Verificamos que horario del ERP no esta en INBI
			for(const i in horariosErp){
				// Buscamos si ya esta el horario
				const horario = horariosInbi.find(el => el.iderp == horariosErp[i].id_horario)
				// Si no esta, se dede insertar
				if(!horario){
					const horariosAddInbi = await catalogos.horariosAddFast(horariosErp[i], 1).then(response => response)
				}

			}

			// Envíamos la respuesta
			resolve({ message: 'Todo se cargo correctamente' });
	  } catch (error) {
			reject({message: error ? error.message : 'Error en el servidor'})
	  }
  })
};

/* CRON PARA CARGAR LOS CICLOS */
cronCiclos = ( ) => {
  return new Promise(async (resolve,reject)=>{
    try {

			let cicloBusqueda = `AND ciclo LIKE '%FE%' AND ciclo LIKE '%ciclo%'`
			// Obtenemos los horarios del ERP y mandamos como parametro la busqueda para el FE
			let ciclosErp = await catalogos.getCiclosLMSERP(cicloBusqueda).then(response=> response)
			// Hacemos una peticion async para obtener los datos de ambas bases de datos
			const ciclosFast = await catalogos.getCiclosLMSFAST( 2 ).then(response => response)

			// Verificamos que ciclo del ERP no esta en FAST
			for(const i in ciclosErp){
				// Buscamos si ya esta el ciclo
				const ciclo = ciclosFast.find(el => el.iderp == ciclosErp[i].id_ciclo)
				// Si no esta, se dede insertar
				if(!ciclo){
					const ciclosAddFast = await catalogos.ciclosAddFast(ciclosErp[i]).then(response => response)
				}
			}

			cicloBusqueda = `AND ciclo NOT LIKE '%FE%' AND ciclo LIKE '%ciclo%'`
			// Obtenemos los horarios del ERP y mandamos como parametro la busqueda para el FE
			ciclosErp = await catalogos.getCiclosLMSERP(cicloBusqueda).then(response=> response)
			// Consultamos los ciclos de inbi
			const ciclosInbi = await catalogos.getCiclosLMSFAST( 1 ).then(response => response)
			// Verificamos que ciclo del ERP no esta en INBI
			for(const i in ciclosErp){
				// Buscamos si ya esta el ciclo
				const ciclo = ciclosInbi.find(el => el.iderp == ciclosErp[i].id_ciclo)
				// Si no esta, se dede insertar
				if(!ciclo){
					const ciclosAddInbi = await catalogos.ciclosAddInbi(ciclosErp[i]).then(response => response)
				}

			}

			// Envíamos la respuesta
			resolve({ message: 'Todo se cargo correctamente' });
	  } catch (error) {
			reject({message: error ? error.message : 'Error en el servidor'})
	  }
  })
};

/* CRON PARA CARGAR LOS GRUPOS */
cronGrupos = ( cicloFast, cicloInbi ) => {
  return new Promise(async (resolve,reject)=>{
    try {

			// Obtenemos los horarios del ERP y mandamos como parametro la busqueda para el FE
			let gruposErp = await catalogos.getGruposLMSERP( cicloFast ).then(response=> response)
			// Hacemos una peticion async para obtener los datos de ambas bases de datos
			const gruposFast = await catalogos.getGruposLMSFASTAll( cicloFast, 2 ).then(response => response)

			// Para agregarlo, se necesita enviar id_ciclo del lms y no del erp y el horario igual del lms y no del erp
			// consultamos los horarios
			const horariosFast = await catalogos.getHorariosLMSFAST( 2 ).then(response => response)
			// y ahora el ciclo
			const ciclosFast = await catalogos.getCiclosLMSFAST( 2 ).then(response => response)
			// y ahora el nivel
			const nivelesFast = await catalogos.getNivelesLMSFAST().then(response => response)

			// Verificamos que ciclo del ERP no esta en FAST
			for(const i in gruposErp){
				// Buscamos si ya esta el ciclo
				const ciclo = gruposFast.find(el => el.iderp == gruposErp[i].id_grupo)
				// Si no esta, se dede insertar
				if(!ciclo){
					// Buscamos el horario
					const horario      = horariosFast.find(el => el.iderp == gruposErp[i].id_horario)
					// Buscamos el ciclo
					const ciclo      = ciclosFast.find(el=> el.iderp == gruposErp[i].id_ciclo)
					// Buscamos el nivel 
					const nivel      = nivelesFast.find(el=> el.iderp == gruposErp[i].id_nivel)
					// Validamos que esten los datos correctos y agregamos el grupo
					if(horario && ciclo && nivel){
						const gruposAddFast = await catalogos.gruposAddFast(gruposErp[i], ciclo.id, horario.id, nivel.valor, 2).then(response => response)
					}
				}
			}

			// Obtenemos los horarios del ERP y mandamos como parametro la busqueda para el FE
			gruposErp = await catalogos.getGruposLMSERP( cicloInbi ).then(response=> response)
			// Para agregarlo, se necesita enviar id_ciclo del lms y no del erp y el horario igual del lms y no del erp
			// consultamos los horarios
			const horariosInbi = await catalogos.getHorariosLMSFAST( 1 ).then(response => response)
			// y ahora el ciclo
			const ciclosInbi = await catalogos.getCiclosLMSFAST( 1 ).then(response => response)
			// y ahora el nivel
			const nivelesInbi = await catalogos.getNivelesLMSFAST().then(response => response)
			// Consultamos los ciclos de inbi
			const gruposInbi = await catalogos.getGruposLMSFASTAll( cicloInbi, 1 ).then(response => response)
			// Verificamos que ciclo del ERP no esta en INBI
			for(const i in gruposErp){
				// Buscamos si ya esta el ciclo
				const ciclo = gruposInbi.find(el => el.iderp == gruposErp[i].id_grupo)
				// Si no esta, se dede insertar
				if(!ciclo){
					// Buscamos el horario
					const horario    = horariosInbi.find(el => el.iderp == gruposErp[i].id_horario)
					// Buscamos el ciclo
					const ciclo      = ciclosInbi.find(el=> el.iderp == gruposErp[i].id_ciclo)
					// Buscamos el nivel 
					const nivel      = nivelesInbi.find(el=> el.iderp == gruposErp[i].id_nivel)
					// Validamos que esten los datos correctos y agregamos el grupo
					if(horario && ciclo && nivel){
						const gruposAddInbi = await catalogos.gruposAddFast(gruposErp[i], ciclo.id, horario.id, nivel.valor, 1).then(response => response)
					}
				}

			}

			// Envíamos la respuesta
			resolve({ message: 'Todo se cargo correctamente' });
	  } catch (error) {
			reject({message: error ? error.message : 'Error en el servidor'})
	  }
  })
};

/* CRON PARA CARGAR LOS TEACHERS */
cronTeaches = ( ) => {
  return new Promise(async (resolve,reject)=>{
    try {

			let email = ''
			// Obtener los usuarios del ERP 
			let teachersERP = await catalogos.getTeachersLMSERP( ).then(response=> response)
			// Hacemos una peticion async para obtener los datos de ambas bases de datos
			const teachersFast = await catalogos.getTeachersLMSFAST( 2 ).then(response => response)

			// Verificamos que usuario del ERP no esta en FAST
			for(const i in teachersERP){
				// Buscamos si ya esta el usuario
				const usuario = teachersFast.find(el => el.email == teachersERP[i].email)
				// Si no esta, se dede insertar
				if(!usuario){
					const email = teachersERP[i].email.split('@')[0]
					// Para agregar al usuario se manda como parametro el usuario del erp
					const teachersAddFast    = await catalogos.teachersAddFast(teachersERP[i], email, 2).then(response => response)
				}
			}

			// Hacemos una peticion async para obtener los datos de ambas bases de datos
			const teachersInbi = await catalogos.getTeachersLMSFAST( 1 ).then(response => response)

			// Verificamos que usuario del ERP no esta en FAST
			for(const i in teachersERP){
				// Buscamos si ya esta el usuario
				const usuario = teachersInbi.find(el => el.email == teachersERP[i].email)
				// Si no esta, se dede insertar
				if(!usuario){
					if(teachersERP[i].email.indexOf('fastenglish') > 0){
						email = teachersERP[i].email.replace('@fastenglish.com.mx','')
					}else{
						email = teachersERP[i].email.replace('@inbi.mx','')
					}
					// Para agregar al usuario se manda como parametro el usuario del erp
					const teachersAddInbi    = await catalogos.teachersAddFast(teachersERP[i], email, 1).then(response => response)
				}
			}

			// Envíamos la respuesta
			resolve({ message: 'Todo se cargo correctamente' });
	  } catch (error) {
			reject({message: error ? error.message : 'Error en el servidor'})
	  }
  })
};

/***************************************************/
/**********************HORARIOS*********************/
/***************************************************/

// Obtener el listado de horarios
exports.getHorariosLMS = async(req, res) => {
	try {

		// Hacemos una peticion async para obtener los datos de ambas bases de datos
		const horariosFast = await catalogos.getHorariosLMSFAST( 2 ).then(response => response)
		const horariosInbi = await catalogos.getHorariosLMSFAST( 1 ).then(response => response)

		// Generamos la repsuesta
		const respuesta = {
			horariosFast,
			horariosInbi
		}

		// Envíamos la respuesta
		res.send(respuesta);

  } catch (error) {
		res.status(500).send({message:error})
  }
};


// Obtener el listado de horarios
exports.getHorariosLMSCron = async(req, res) => {
	try {

		/* 
			CARGAR HORARIOS AL ERP
		*/

		const horarios = await cronHorarios( ).then( response => response )

		res.send({ message: 'Datos cargados correctamente' });
  } catch (error) {
		res.status(500).send({message:error})
  }
};


/***************************************************/
/************************CICLOS*********************/
/***************************************************/

// Obtener el listado de ciclos
exports.getCiclosLMS = async(req, res) => {
	try {

		// Hacemos una peticion async para obtener los datos de ambas bases de datos
		const ciclosFast = await catalogos.getCiclosLMSFAST( 2 ).then(response => response)
		const ciclosInbi = await catalogos.getCiclosLMSFAST( 1 ).then(response => response)

		// Generamos la repsuesta
		const respuesta = {
			ciclosFast,
			ciclosInbi
		}

		// Envíamos la respuesta
		res.send(respuesta);
  } catch (error) {
		res.status(500).send({message:error})
  }
};

// Obtener el listado de ciclos
exports.getCiclosLMSCron = async(req, res) => {
	try {

		const ciclos = await cronCiclos( ).then( response => response )

		res.send({ message: 'Datos cargados correctamente' });

  } catch (error) {
		res.status(500).send({message:error})
  }
};

/***************************************************/
/************************GRUPOS*********************/
/***************************************************/

// Obtener el listado de grupos
exports.getGruposLMS = async(req, res) => {
	try {
		// Caputramos los datos
		const { cicloFast, cicloInbi } = req.params
		// Hacemos una peticion async para obtener los datos de ambas bases de datos
		const gruposFast = await catalogos.getGruposLMSFAST( cicloFast, 2 ).then(response => response)
		const gruposInbi = await catalogos.getGruposLMSFAST( cicloInbi, 1 ).then(response => response)

		// Generamos la repsuesta
		const respuesta = {
			gruposFast,
			gruposInbi
		}

		// Envíamos la respuesta
		res.send(respuesta);
  } catch (error) {
		res.status(500).send({message:error})
  }
};

// Obtener el listado de grupos
exports.getGruposLMSCron = async(req, res) => {
	try {

		// Caputramos los datos
		const { cicloFast, cicloInbi } = req.params

		const grupos = await cronGrupos( cicloFast, cicloInbi ).then( response => response )

		res.send({ message: 'Datos cargados correctamente' });

  } catch (error) {
		res.status(500).send({message:error})
  }
};

exports.updateGruposLMSCron = async(req, res) => {
	try {
		const { id_unidad_negocio, optimizado, deleted, online, id } = req.body
		// Actualizar en FAST
		const usuariosFast = await catalogos.updateGruposLMSFAST( optimizado, deleted, online, id, id_unidad_negocio ).then(response => response)

		// Envíamos la respuesta
		res.send({message: 'Actualización correcta'});
  } catch (error) {
		res.status(500).send({message:error})
  }
};

/***************************************************/
/**********************USUARIOS*********************/
/***************************************************/

// Obtener el listado de codigos
exports.getUsuariosLMS = async(req, res) => {
	try {
		// Hacemos una peticion async para obtener los datos de ambas bases de datos
		const usuariosFast = await catalogos.getUsuariosLMSFAST( 2 ).then(response => response)
		const usuariosInbi = await catalogos.getUsuariosLMSINBI( ).then(response => response)

		// Generamos la repsuesta
		const respuesta = {
			usuariosFast,
			usuariosInbi
		}

		// Envíamos la respuesta
		res.send(respuesta);
  } catch (error) {
		res.status(500).send({message:error})
  }
};

// Obtener el listado de codigos
exports.getUsuariosLMSCron = async(req, res) => {
	try {

		// Caputramos los datos
		const { cicloFast, cicloInbi } = req.params

		// Obtener los usuarios del ERP 
		let usuariosErp = await catalogos.getUsuariosLMSERPConAdeudo(cicloFast).then(response=> response)

		// Hacemos una peticion async para obtener los datos de ambas bases de datos
		const usuariosFast = await catalogos.getUsuariosLMSFAST( 2 ).then(response => response)
		// Consultamos el grupo alumno que ya existe en este momento
		const getGrupoAlumnosLMSFAST = await catalogos.getGrupoAlumnosLMSFAST().then(response => response)
		// Consultamos los grupos de ese ciclo
		const gruposFast = await catalogos.getGruposLMSFAST( cicloFast, 2 ).then(response => response)

		// Verificamos que usuario del ERP no esta en FAST
		for(const i in usuariosErp){
			// Buscamos si ya esta el usuario
			const usuario = usuariosFast.find(el => el.iderp == usuariosErp[i].id_alumno)
			// Si no esta, se dede insertar
			if(!usuario){
				// y ahora el grupo
				const grupo      = gruposFast.find(el=> el.iderp == usuariosErp[i].id_grupo)
				
				// Validamos que esten los datos correctos y agregamos el grupo
				if(grupo){
					// Para agregar al usuario se manda como parametro el usuario del erp
					const usuariosAddFast    = await catalogos.usuariosAddFast(usuariosErp[i], 2).then(response => response)
					// Para agregar el alumno mandamos como parametros los datos del usuario
					const addAlumnoFast      = await catalogos.addAlumnoFast( usuariosAddFast, 2 ).then(response => response)
					// Ahora hay que agregar el grupo_alumno
					const addAlumnoGrupoFast = await catalogos.addAlumnoGrupoFast(usuariosAddFast, grupo.id, 2 ).then(response => response)
				}
			}
		}

		// Obtenemos los horarios del ERP y mandamos como parametro la busqueda para el FE
		usuariosErp = await catalogos.getUsuariosLMSERPConAdeudo( cicloInbi ).then(response=> response)
		// Consultamos los ciclos de inbi
		const usuariosInbi = await catalogos.getUsuariosLMSINBI( cicloInbi ).then(response => response)
		// Consultamos el grupo alumno que ya existe en este momento
		const getGrupoAlumnosLMSINBI = await catalogos.getGrupoAlumnosLMSINBI().then(response => response)
		// Consultamos los grupos de ese ciclo
		const gruposInbi = await catalogos.getGruposLMSFAST( cicloInbi, 1 ).then(response => response)

		// Verificamos que usuario del ERP no esta en INBI
		for(const i in usuariosErp){
			// Buscamos si ya esta el usuario
			const usuario = usuariosInbi.find(el => el.iderp == usuariosErp[i].id_alumno)
			// Si no esta, se dede insertar
			if(!usuario){
				// y ahora el grupo
				const grupo      = gruposInbi.find(el=> el.iderp == usuariosErp[i].id_grupo)
				// Validamos que esten los datos correctos y agregamos el grupo
				if(grupo){
					// Para agregar al usuario se manda como parametro el usuario del erp
					const usuariosAddInbi    = await catalogos.usuariosAddFast( usuariosErp[i], 1 ).then(response => response)
					// // Para agregar el alumno mandamos como parametros los datos del usuario
					const addAlumnoInbi      = await catalogos.addAlumnoFast( usuariosAddInbi, 1 ).then(response => response)
					// // Ahora hay que agregar el grupo_alumno
					const addAlumnoGrupoInbi = await catalogos.addAlumnoGrupoFast(usuariosAddInbi, grupo.id, 1 ).then(response => response)
				}
			}
		}

		// Envíamos la respuesta
		res.send({ message: usuariosErp });
  } catch (error) {
		res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};


exports.cronUsuariosLMS = async(req, res) => {
	try {

		// OBTENER PRIMERO LOS CICLOS
		const ciclosEscuela = await catalogos.getCiclosCron( ).then( response => response )

		for( const i in ciclosEscuela ){
			const { id_ciclo, ciclo, escuela } = ciclosEscuela[i] 

			// Obtener los usuarios del ERP 
			let alumnosERP = await catalogos.getUsuariosLMSERPConAdeudo( id_ciclo ).then(response=> response)

			// Hacemos una peticion async para obtener los datos de ambas bases de datos
			const alumnosEscuela = await catalogos.getUsuariosLMSFAST( escuela ).then(response => response)
			
			// Consultamos los grupos de ese ciclo
			const gruposEscuela = await catalogos.getGruposLMSFAST( id_ciclo, escuela ).then(response => response)

			// Verificamos que usuario del ERP no esta en FAST
			for(const i in alumnosERP){
				// Buscamos si ya esta el usuario
				const usuario = alumnosEscuela.find(el => el.iderp == alumnosERP[i].id_alumno )
				// Si no esta, se dede insertar
				if(!usuario){
					// y ahora el grupo
					const grupo      = gruposEscuela.find(el=> el.iderp == alumnosERP[i].id_grupo )
					
					// Validamos que esten los datos correctos y agregamos el grupo
					if(grupo){

						// Para agregar al usuario se manda como parametro el usuario del erp
						const usuariosAddFast    = await catalogos.usuariosAddFast( alumnosERP[i], escuela).then(response => response)
						
						// Para agregar el alumno mandamos como parametros los datos del usuario
						const addAlumnoFast      = await catalogos.addAlumnoFast( usuariosAddFast, escuela ).then(response => response)
						
						// Ahora hay que agreagar el grupo_alumno
						const addAlumnoGrupoFast = await catalogos.addAlumnoGrupoFast( usuariosAddFast, grupo.id, escuela ).then(response => response)
					}
				}
			}

			/*********************************************************/
			/************* G R U P O  A L U M N O S ******************/
			/*********************************************************/
			// Cargamos los alumnos del erp
			let gruposAlumnosErp = await catalogos.getGruposAlumnosLMSERP( id_ciclo ).then(response=> response)

			// Cargamos los alumnos del lms
			// Hacemos una peticion async para obtener los datos de ambas bases de datos
			const gruposAlumnosEscuela = await catalogos.getGruposAlumnosLMS( id_ciclo, escuela ).then(response => response)

			// Cargamos los grupos
			const gruposEscuela2 = await catalogos.getGruposLMSFASTAll( id_ciclo, escuela ).then(response => response)

			// Cargamos los alumnos
			const alumnosEscuela2 = await catalogos.getAlumnosLMSFAST( escuela ).then(response => response)

			// Verificamos que ciclo del ERP no esta en FAST
			for(const i in gruposAlumnosErp){
				const { id_grupo, id_alumno } = gruposAlumnosErp[i]

				// Vemos si ya 
				const existeGrupoAlumno = gruposAlumnosEscuela.find( el=> el.u_iderp == id_alumno )

				// si no existe hay que agregarlo
				if(!existeGrupoAlumno){
					// Buscamos el grupo en el que va
					const existeGrupo  = gruposEscuela2.find( el=> el.iderp == id_grupo )
					const existeAlumno = alumnosEscuela2.find( el=> el.iderp == id_alumno )

					// Verificamos que el alumno se agregue correctamente
					if(existeGrupo && existeAlumno){
						// Agregamos el grupo_alumno, mandamos id_grupo del lms y id alumno del LMS
		  			const addGrupoAlumnoFast = await catalogos.addGrupoAlumnoFast( existeGrupo.id, existeAlumno.id, escuela ).then(response => response)
					}
				}else{
					// Buscamos el alumno
					const existeAlumno = alumnosEscuela2.find( el=> el.iderp == id_alumno )
					// Buscamos el grupo en el que va
					const existeGrupo  = gruposEscuela2.find( el=> el.iderp == id_grupo )
					if( id_alumno == 10115 ){ console.log(existeGrupoAlumno, existeGrupo)}
					// vemos que sea el mismos grupo el del ERP y el del LMS, si son diferentes se tiene que actualizar
					if(existeGrupoAlumno.iderp !=  existeGrupo.iderp){

						// NUEVO GRUPO: existeGrupo.id
						const updateGruposAlumnoFast = await catalogos.updateGruposAlumnoFast( existeGrupoAlumno.id_grupo_alumnos, existeGrupo.id, escuela ).then(response => response)

						// Validar las asistencias
						if( existeGrupoAlumno.id_curso == existeGrupo.id_curso ) {
							// Actualizar las asistencias si es que estan creadas
							// Mandamos el grupo anterior para ver si ya hay asistencias en ese grupo
							// VIEJO GRUPO: existeGrupoAlumno.id_grupo
							const getAsistenciasFast = await catalogos.getAsistenciasFast( existeGrupoAlumno.id_grupo, existeAlumno.id, escuela ).then(response=> response)
							// Actualizar las asistencias si es que hay ya unas grabadas
							if(getAsistenciasFast.length > 0){
								// Actualizamos mandando el nuevo grupo, el grupo viejo y el usuario
								// NUEVO GRUPO: existeGrupo.id
								// VIEJO GRUPO: existeGrupoAlumno.id_grupo
								const updateAsistenciasFast = await catalogos.updateAsistenciasFast( existeGrupo.id, existeGrupoAlumno.id_grupo, existeAlumno.id, escuela ).then(response=> response)
							}
						}
					}

				}

	  	}

		}
		// // Envíamos la respuesta
		res.send({ message: 'Datos agregados correctamente' });
  } catch (error) {
		res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};

exports.getUsuariosLMSCronExci = async(req, res) => {
	try {

		// Caputramos los datos
		const { cicloFast } = req.params

		// Obtener los usuarios del ERP 
		let usuariosErp = await catalogos.getUsuariosLMSERPEXCI(cicloFast).then(response=> response)
		// Hacemos una peticion async para obtener los datos de ambas bases de datos
		const usuariosFast = await catalogos.getUsuariosLMSFAST( 2 ).then(response => response)
		// Consultamos el grupo alumno que ya existe en este momento
		const getGrupoAlumnosLMSFAST = await catalogos.getGrupoAlumnosLMSFAST().then(response => response)
		// Consultamos los grupos de ese ciclo
		const gruposFast = await catalogos.getGruposLMSFAST( cicloFast, 2 ).then(response => response)

		// Verificamos que usuario del ERP no esta en FAST
		for(const i in usuariosErp){
			// Buscamos si ya esta el usuario
			const usuario = usuariosFast.find(el => el.iderp == usuariosErp[i].id_alumno)
			// Si no esta, se dede insertar
			if(!usuario){
				// Para agregar al usuario se manda como parametro el usuario del erp
				const usuariosAddFast    = await catalogos.usuariosAddFast(usuariosErp[i], 2).then(response => response)
				// Para agregar el alumno mandamos como parametros los datos del usuario
				const addAlumnoFast      = await catalogos.addAlumnoFast( usuariosAddFast, 2 ).then(response => response)
			}
		}
		// Envíamos la respuesta
		res.send({ message: usuariosErp });
  } catch (error) {
		res.status(500).send({message:error})
  }
};

// Saber si el usuario tiene adeudo o no
exports.getUsuariosLMSAdeudo = async(req, res) => {
	try {
		// Buscamos los adeudos del LMS
		const usuariosFast = await catalogos.getUsuariosAdeudosFAST( req.body.usuario ).then(response => response)
		const usuariosInbi = await catalogos.getUsuariosAdeudosINBI( req.body.usuario ).then(response => response)

		if(usuariosFast.length > 0 || usuariosInbi.length > 0){
			res.send({estatus: 1 });
		}else{
			res.send({estatus: 0 });
		}
  } catch (error) {
		res.status(500).send({message:error})
  }
};
// Ver si el alumno tiene acceso a su nuevo grupo o no

exports.getUsuariosLMSAcceso = async(req, res) => {
	try {
		// Buscamos los adeudos del LMS
		if(req.body.escuela == 2){

			// Buscamos l aultima calificación de su kardex
			const kardex = await catalogos.getUsuariosAccesoFAST( req.body.idusuario ).then(response => response)
			if(kardex.length > 0){
				// Si cuenta con Kardex
				// Buscamos su ultimo grupo asignado del usuario
				const ultimoGrupo = await catalogos.getUltimoGrupoAlumnoFast( req.body.idusuario ).then(response => response)
				// Sacamos el último
				let grupo = ultimoGrupo[ultimoGrupo.length-1]

				// Validar el pase del alumno
				if(kardex[0].calificacion_final_primera_oportunidad >= 70 || kardex[0].calificacion_final_segunda_oportunidad >= 70 ) {
					// El alumno paso con exito su nivel anterior
					res.send({estatus: 1 });
				}else{
					// No paso, peor hay que validar si el grupo en el que esta ahora, es menorrrr al grupo que no paso 
					// Si es menor o igual, significa que si lo rehubicaron 
					if( grupo.id_nivel <= kardex[0].nivel ){
						res.send({estatus: 1 });
					}else{
						// Si no lo es, hay que rehubicarlo
						res.send({estatus: 0 });
					}
				}
			}else{
				res.send({estatus: 1 });
			}

		}else{
			// Buscamos l aultima calificación de su kardex
			const kardex = await catalogos.getUsuariosAccesoINBI( req.body.idusuario ).then(response => response)
			if(kardex.length > 0){
				// Si cuenta con Kardex
				// Buscamos su ultimo grupo asignado del usuario
				const ultimoGrupo = await catalogos.getUltimoGrupoAlumnoInbi( req.body.idusuario ).then(response => response)
				// Sacamos el último
				let grupo = ultimoGrupo[ultimoGrupo.length-1]

				// Validar el pase del alumno
				if(kardex[0].calificacion_final_primera_oportunidad >= 70 || kardex[0].calificacion_final_segunda_oportunidad >= 70 ) {
					// El alumno paso con exito su nivel anterior
					res.send({estatus: 1 });
				}else{
					// No paso, peor hay que validar si el grupo en el que esta ahora, es menorrrr al grupo que no paso 
					// Si es menor o igual, significa que si lo rehubicaron 
					if( grupo.id_nivel <= kardex[0].nivel ){
						res.send({estatus: 1 });
					}else{
						// Si no lo es, hay que rehubicarlo
						res.send({estatus: 0 });
					}
				}
			}else{
				res.send({estatus: 1 });
			}
		}
  } catch (error) {
		res.status(500).send({message:error})
  }
};

/***************************************************/
/**********************TEACHERS*********************/
/***************************************************/

// Obtener el listado de codigos
exports.getTeachersLMS = async(req, res) => {
	try {
		// Hacemos una peticion async para obtener los datos de ambas bases de datos
		const teachersFast = await catalogos.getTeachersLMSFAST( 2 ).then(response => response)
		const teachersInbi = await catalogos.getTeachersLMSFAST( 1 ).then(response => response)

		// Generamos la repsuesta
		const respuesta = {
			teachersFast,
			teachersInbi
		}

		// Envíamos la respuesta
		res.send(respuesta);
  } catch (error) {
		res.status(500).send({message:error})
  }
};

// Obtener el listado de codigos
exports.getTeachersLMSCron = async(req, res) => {
	try {

		/* 
			CARGAR TEACHERS
		*/

		const teachers = await cronTeaches( cicloFast, cicloInbi ).then( response => response )

		res.send({ message: 'Datos cargados correctamente' });
  } catch (error) {
		res.status(500).send({message:error})
  }
};

/***************************************************/
/******************GRUPOS_TEACHERS******************/
/***************************************************/

// Obtener el listado de grupos
exports.getGruposTeachersLMS = async(req, res) => {
	try {
		// Caputramos los datos
		const { cicloFast, cicloInbi } = req.params
		// Hacemos una peticion async para obtener los datos de ambas bases de datos
		const gruposTeachersFast = await catalogos.getGruposTeacherLMSFAST( cicloFast, 2 ).then(response => response)
		const gruposTeachersInbi = await catalogos.getGruposTeacherLMSFAST( cicloInbi, 1 ).then(response => response)

		// Generamos la repsuesta
		const respuesta = {
			gruposTeachersFast,
			gruposTeachersInbi
		}

		// Envíamos la respuesta
		res.send(respuesta);
  } catch (error) {
		res.status(500).send({message:error})
  }
};

// Obtener el listado de grupos
exports.getGruposTeachersLMSCron = async(req, res) => {
	try {

		// Caputramos los datos
		const { cicloFast, cicloInbi } = req.params

		// Cargamos los teacher activos
		// Hacemos una peticion async para obtener los datos de ambas bases de datos
		const teachersFast = await catalogos.getTeachersLMSFAST( 2 ).then(response => response)
		const teachersInbi = await catalogos.getTeachersLMSFAST( 1 ).then(response => response)

		// Cargamos los grupos
		const gruposFast = await catalogos.getGruposLMSFAST( cicloFast, 2 ).then(response => response)
		const gruposInbi = await catalogos.getGruposLMSFAST( cicloInbi, 1 ).then(response => response)

		// Cargamos los grupo y los teacher del erp
		let gruposTeachersErp = await catalogos.getGruposTeacherLMSERP( cicloFast ).then(response=> response)
		// Hay que volver a buscar los grupos_teacher pero del ERP 
  	let gruposTeachersErp2 = await catalogos.getGruposTeacherLMSERP( cicloInbi ).then(response=> response)

		// cargar los grupo_teachers del LMS de FAST
		const gruposTeachersFast = await catalogos.getGruposTeacherLMSFAST( cicloFast, 2 ).then(response => response)
		const gruposTeachersInbi = await catalogos.getGruposTeacherLMSFAST( cicloInbi, 1 ).then(response => response)

		// Declaramos las variables a utilizar 
		let id_teacher1 = 0
		let id_teacher2 = 0
		let busqueda1   = 0
		let busqueda2   = 0

		// Verificamos que ciclo del ERP no esta en FAST
		for(const i in gruposFast){

			id_teacher1 = 0
			id_teacher2 = 0

			const existeGrupoTeacher = gruposTeachersFast.find( el=> el.id_grupo == gruposFast[i].id )

			// primero hay que buscar el primer teacher
			let teacher   = gruposTeachersErp.find(el => el.id_grupo == gruposFast[i].iderp && el.numero_teacher == 1)
			let teacher2  = gruposTeachersErp.find(el => el.id_grupo == gruposFast[i].iderp && el.numero_teacher == 2)

			// Buscar el id_usuario
			if( teacher ){   
				let busqueda1 = teachersFast.find(el=> el.email == teacher.email)
				if( busqueda1 ){ id_teacher1 = busqueda1.id }else{ id_teacher1 = 0 }
			}

			// Buscar el id_usuario2
			if( teacher2 ){   
				let busqueda2 = teachersFast.find(el=> el.email == teacher2.email)
				if( busqueda2 ){ id_teacher2 = busqueda2.id }else{ id_teacher2 = 0 }
			}

			// si no existe hay que agregarlo
			if(!existeGrupoTeacher){
				// Ahora si ya podemos agregar el grupo , ya que tenemos validado si existe o no
	  		// const addGruposTeacherFast = await catalogos.addGruposTeacherFast(gruposFast[i].id, id_teacher1, id_teacher2, 2).then(response => response)
			}else{
				const updateGruposTeacherFast = await catalogos.updateGruposTeacherFast(gruposFast[i].id, id_teacher1, id_teacher2, 2).then(response => response)
			}
  	}

		// Verificamos que ciclo del ERP no esta en FAST
		for(const i in gruposInbi){
			// Limpiamos los datos
			id_teacher1 = 0
			id_teacher2 = 0
			const existeGrupoTeacher2 = gruposTeachersInbi.find( el=> el.id_grupo == gruposInbi[i].id )
			// si no existe hay que agregarlo
			// primero hay que buscar el primer teacher
			let teacher   = gruposTeachersErp2.find(el => el.id_grupo == gruposInbi[i].iderp && el.numero_teacher == 1)
			let teacher2  = gruposTeachersErp2.find(el => el.id_grupo == gruposInbi[i].iderp && el.numero_teacher == 2)

			// Buscar el id_usuario
			if( teacher ){   
				let busqueda1 = teachersInbi.find(el=> el.email == teacher.email)
				if( busqueda1 ){ id_teacher1 = busqueda1.id }else{ id_teacher1 = 0 }
			}

			// Buscar el id_usuario2
			if( teacher2 ){   
				let busqueda2 = teachersInbi.find(el=> el.email == teacher2.email)
				if( busqueda2 ){ id_teacher2 = busqueda2.id }else{ id_teacher2 = 0 }
			}

			// si no existe hay que agregarlo
			if(!existeGrupoTeacher2){
				// Ahora si ya podemos agregar el grupo , ya que tenemos validado si existe o no
	  		const addGruposTeacherInbi = await catalogos.addGruposTeacherFast(gruposInbi[i].id, id_teacher1, id_teacher2, 1).then(response => response)
			}else{
				const updateGruposTeacherInbi = await catalogos.updateGruposTeacherFast(gruposInbi[i].id, id_teacher1, id_teacher2, 1).then(response => response)
			}
  	}

		// Envíamos la respuesta
		res.send({ message: 'Todo se cargo correctamente' });
  } catch (error) {
		res.status(500).send({message:error})
  }
};

/***************************************************/
/******************GRUPOS_ALUMNOS*******************/
/***************************************************/

// Obtener el listado de grupos
exports.getGruposAlumnosLMS = async(req, res) => {
	try {
		// Caputramos los datos
		const { cicloFast, cicloInbi } = req.params
		
		let fechaEspaniol    = await calificaciones.fechaEspaniol( 1 ).then( response => response )
  	fechaEspaniol        = await calificaciones.fechaEspaniol( 2 ).then( response => response )

		// Hacemos una peticion async para obtener los datos de ambas bases de datos
		const gruposAlumnosFast = await catalogos.getGruposAlumnosLMS( cicloFast, 2 ).then(response => response)
		const gruposAlumnosInbi = await catalogos.getGruposAlumnosLMS( cicloInbi, 1 ).then(response => response)

		let mapIdFast  = gruposAlumnosFast.map((registro) => registro.id);
  	let mapIdInbi  = gruposAlumnosInbi.map((registro) => registro.id);

		const alumnosFast = await reprobados.getAlumnosFechaPagoFast( mapIdFast, cicloFast ).then(response=> response)
  	const alumnosInbi = await reprobados.getAlumnosFechaPagoInbi( mapIdInbi, cicloInbi ).then(response=> response)

  	for( const i in gruposAlumnosFast){
  		const { id } = gruposAlumnosFast[i]
  		gruposAlumnosFast[i]['fecha_proximo_pago'] = alumnosFast.filter( el=> { return el.id_usuario == id }).map((registro) => registro.fecha_pago);
  	}

  	for( const i in gruposAlumnosInbi){
  		const { id } = gruposAlumnosInbi[i]
  		gruposAlumnosInbi[i]['fecha_proximo_pago'] = alumnosInbi.filter( el=> { return el.id_usuario == id }).map((registro) => registro.fecha_pago);
  	}

		// Generamos la repsuesta
		const respuesta = {
			gruposAlumnosFast,
			gruposAlumnosInbi
		}

		// Envíamos la respuesta
		res.send(respuesta);
  } catch (error) {
		res.status(500).send({message: error ? error.message : 'Error en el servidor'})
  }
};

exports.getGruposAlumnosLMSExci = async(req, res) => {
	try {
		// Caputramos los datos
		const { id_ciclo } = req.body
		// Hacemos una peticion async para obtener los datos de ambas bases de datos
		const gruposAlumnosFast = await catalogos.getGruposAlumnosLMSExci( id_ciclo ).then(response => response)

		// Envíamos la respuesta
		res.send(gruposAlumnosFast);
  } catch (error) {
		res.status(500).send({message: error ? error.message : 'Error en el servidor'})
  }
};

// Obtener el listado de grupos
exports.getGruposAlumnosLMSCron = async(req, res) => {
	try {

		// Caputramos los datos
		const { cicloFast, cicloInbi } = req.params

		// Cargamos los alumnos del erp
		let gruposAlumnosErp = await catalogos.getGruposAlumnosLMSERP(cicloFast).then(response=> response)

		// Cargamos los alumnos del lms
		// Hacemos una peticion async para obtener los datos de ambas bases de datos
		const gruposAlumnosFast = await catalogos.getGruposAlumnosLMS( cicloFast, 2 ).then(response => response)
		const gruposAlumnosInbi = await catalogos.getGruposAlumnosLMS( cicloInbi, 1 ).then(response => response)

		// Cargamos los grupos
		const gruposFast = await catalogos.getGruposLMSFASTAll( cicloFast, 2 ).then(response => response)
		const gruposInbi = await catalogos.getGruposLMSFASTAll( cicloInbi, 1 ).then(response => response)

		// Cargamos los alumnos
		const alumnosFast = await catalogos.getAlumnosLMSFAST( 2 ).then(response => response)
		const alumnosInbi = await catalogos.getAlumnosLMSFAST( 1 ).then(response => response)

		// Verificamos que ciclo del ERP no esta en FAST
		for(const i in gruposAlumnosErp){
			const { id_grupo, id_alumno } = gruposAlumnosErp[i]

			// Vemos si ya 
			const existeGrupoAlumno = gruposAlumnosFast.find( el=> el.u_iderp == id_alumno )

			// si no existe hay que agregarlo
			if(!existeGrupoAlumno){
				// Buscamos el grupo en el que va
				const existeGrupo  = gruposFast.find( el=> el.iderp == id_grupo )
				const existeAlumno = alumnosFast.find( el=> el.iderp == id_alumno )

				// Verificamos que el alumno se agregue correctamente
				if(existeGrupo && existeAlumno){
					// Agregamos el grupo_alumno, mandamos id_grupo del lms y id alumno del LMS
	  			const addGrupoAlumnoFast = await catalogos.addGrupoAlumnoFast( existeGrupo.id, existeAlumno.id, 2 ).then(response => response)
				}
			}else{
				// Buscamos el alumno
				const existeAlumno = alumnosFast.find( el=> el.iderp == id_alumno )
				// Buscamos el grupo en el que va
				const existeGrupo  = gruposFast.find( el=> el.iderp == id_grupo )
				// vemos que sea el mismos grupo el del ERP y el del LMS, si son diferentes se tiene que actualizar
				if(existeGrupoAlumno.iderp !=  existeGrupo.iderp){
					// NUEVO GRUPO: existeGrupo.id
					const updateGruposAlumnoFast = await catalogos.updateGruposAlumnoFast(existeGrupoAlumno.id_grupo_alumnos, existeGrupo.id, 2).then(response => response)
				}

			}

  	}

  	// Hay que volver a buscar los grupos_teacher pero del ERP 
  	gruposAlumnosErp = await catalogos.getGruposAlumnosLMSERP(cicloInbi).then(response=> response)

		// Verificamos que ciclo del ERP no esta en FAST
		for(const i in gruposAlumnosErp){
			const { id_grupo, id_alumno } = gruposAlumnosErp[i]
			// Vemos si ya 
			const existeGrupoAlumno = gruposAlumnosInbi.find( el=> el.u_iderp == id_alumno )

			// si no existe hay que agregarlo
			if(!existeGrupoAlumno){
				// Buscamos el grupo en el que va
				const existeGrupo  = gruposInbi.find( el=> el.iderp == id_grupo )
				const existeAlumno = alumnosInbi.find( el=> el.iderp == id_alumno )

				// Verificamos que el alumno se agregue correctamente
				if(existeGrupo && existeAlumno){
					// Agregamos el grupo_alumno, mandamos id_grupo del lms y id alumno del LMS
	  			const addGrupoAlumnoInbi = await catalogos.addGrupoAlumnoFast( existeGrupo.id, existeAlumno.id, 1 ).then(response => response)
				}
			}else{
				// Buscamos el grupo en el que va
				const existeGrupo  = gruposInbi.find( el=> el.iderp == id_grupo )
				// vemos que sea el mismos grupo el del ERP y el del LMS, si son diferentes se tiene que actualizar
				if(existeGrupoAlumno.iderp !=  existeGrupo.iderp){
					// el grupo correcto es: existeGrupo.id
					const updateGruposAlumnoInbi = await catalogos.updateGruposAlumnoFast(existeGrupoAlumno.id_grupo_alumnos, existeGrupo.id, 1).then(response => response)
				}

			}

  	}

		// Envíamos la respuesta
		res.send({ message: 'Todo se cargo correctamente' });
  } catch (error) {
		res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};

exports.getGruposAlumnosLMSCronExci = async(req, res) => {
	try {

		// Caputramos los datos
		const { id_ciclo } = req.body

		/****************
		// HACEMOS EL CRON DE LOS ALUMNOS DEL EXCI
		****************/

		// Obtener los usuarios del ERP 
		let usuariosErp = await catalogos.getUsuariosLMSERPEXCI( id_ciclo ).then(response=> response)
		// Hacemos una peticion async para obtener los datos de ambas bases de datos
		const usuariosFast = await catalogos.getUsuariosLMSFAST( 2 ).then(response => response)

		// Verificamos que usuario del ERP no esta en FAST
		for(const i in usuariosErp){
			// Buscamos si ya esta el usuario
			const usuario = usuariosFast.find(el => el.iderp == usuariosErp[i].id_alumno)
			// Si no esta, se dede insertar
			if(!usuario){
				// Para agregar al usuario se manda como parametro el usuario del erp
				const usuariosAddFast    = await catalogos.usuariosAddFast( usuariosErp[i], 2 ).then(response => response)
				// Para agregar el alumno mandamos como parametros los datos del usuario
				const addAlumnoFast      = await catalogos.addAlumnoFast( usuariosAddFast, 2 ).then(response => response)
				// Agregar ahora al exci registros
    		const registrarExci     = await catalogos.registrarExci( usuariosAddFast, id_ciclo ).then( response => response )
			}else{
				// Validar si el usuario ya se dio de alta en el exci
				const existeUsuarioFast = await catalogos.existeUsuarioFast( usuariosErp[i].id_alumno ).then( response => response )
				if(!existeUsuarioFast){
					const payload = {
						nombre:     usuariosErp[i].nombre,
						email:      usuariosErp[i].email,
						iderp:      usuariosErp[i].id_alumno,
						matricula:  usuariosErp[i].matricula
					}
    			const registrarExci     = await catalogos.registrarExci( payload, id_ciclo ).then( response => response )
				}
			}
		}

		/****************
		// *********** //
		****************/

		// Envíamos la respuesta
		res.send({ message: 'Todo se cargo correctamente' });
  } catch (error) {
		res.status(500).send({message:error})
  }
};

/**************************************************/
/************** V A C A C I O N E S ***************/
/**************************************************/
exports.generarVaciones = async(req, res) => {
	try {

		// Caputramos los datos
		const { cicloFast, cicloInbi } = req.params

		// // Consultar los grupos activos del ciclo
		// const gruposFast = await catalogos.gruposFast(cicloFast).then(response=> response)

		// for(const i in gruposFast){
		// 	const { id, id_nivel } = gruposFast[i]
		// 	// console.log('Grupo por editar: ', id)
		// 	const exito = await catalogos.vacacionesFast( id, id_nivel ).then(response => response)

		// 	if(exito){
		// 		// console.log('Grupo editado con exito: ', id)
		// 	}else{
		// 		// console.log('Grupo NO editado', id)
		// 	}
		// }


		// Consultar los grupos activos del ciclo
		const gruposInbi = await catalogos.gruposInbi(cicloInbi).then(response=> response)

		for(const i in gruposInbi){
			const { id, id_nivel } = gruposInbi[i]
			// console.log('Grupo por editar: ', id)
			const exito = await catalogos.vacacionesInbi( id, id_nivel ).then(response => response)

			if(exito){
				// console.log('Grupo editado con exito: ', id)
			}else{
				// console.log('Grupo NO editado', id)
			}
		}

		// Envíamos la respuesta
		res.send({ message: 'Todo se cargo correctamente' });
  } catch (error) {
		res.status(500).send({message:error ? error.message :'Error en el servidor'})
  }
};


/**************************************************/
/********************** L M S *********************/
/**************************************************/

exports.getResultadoToefl = async(req, res) => {
	try {	

		let resultado = null
		// Caputramos los datos
		const { escuela, iderp } = req.params
		// Buscar la matricula
		const { matricula, id } = await catalogos.getMatricula( iderp ).then(response=> response)

		if(escuela == 2){	
			resultado = await catalogos.getResultadoToeflFAST( matricula ).then(response=> response)
			if(resultado){
				res.send({ ...resultado, matricula, estatus:1 });
			}else{
				resultado = await catalogos.getResultadoToeflFAST2( id ).then(response=> response)
				if(resultado){
					res.send({ ...resultado, matricula, estatus:1 });
				}else{
					res.send({ matricula, estatus:0, message:'Sin resultados' });	
				}
			}
			// Buscar la matricula del usuario en base a su iderp
		}else{
			// Escuela de inbi
			resultado = await catalogos.getResultadoToeflINBI( matricula ).then(response=> response)
			if(resultado){
				res.send({ ...resultado, matricula, estatus:1 });
			}else{
				resultado = await catalogos.getResultadoToeflINBI2( id ).then(response=> response)
				if(resultado){
					res.send({ ...resultado, matricula, estatus:1 });
				}else{
					res.send({ matricula, estatus:0, message:'Sin resultados' });	
				}
			}
		}
		
  } catch (error) {
		res.status(500).send({message:error})
  }
};

// Saber si el usuario tiene adeudo o no
exports.getEvaluacionCertificacion = async(req, res) => {
	try {
		const { usuario, escuela } = req.body

		let calificaciones = null 
		if(escuela == 2){
			calificaciones = await catalogos.getCalificacionEvaluacionCertificacionFast( usuario ).then(response => response)
		}else{
			calificaciones = await catalogos.getCalificacionEvaluacionCertificacionInbi( usuario ).then(response => response)
		}

		if(calificaciones){
			res.send({estatus: 1, calificacion: calificaciones.calificacion });
		}else{
			res.send({estatus: 0 });
		}
  } catch (error) {
		res.status(500).send({message:error})
  }
};

// Validar evaluacion de la clase
exports.validarEvaluacion = async(req, res) => {
	try {	
		const { id_usuario, escuela, valoracion } = req.body
		let   existeClaseActiva= null

		if( escuela == 2 ){
			existeClaseActiva = await catalogos.existeClaseActivaFast( id_usuario ).then(response => response)
		}else{
			existeClaseActiva = await catalogos.existeClaseActivaInbi( id_usuario ).then(response => response)
		}


		if( existeClaseActiva ){
			// Validar si el alumno ya contesto la evaluación
			const { id_grupo, id_ciclo, id_teacher, num_teacher, dia } = existeClaseActiva
			// Consultar la evaluación
			const claseEvaluada = await catalogos.existeValoracion( id_usuario, id_grupo, id_ciclo, escuela, dia ).then(response=> response)
			if( claseEvaluada ){
				return res.send({ status: 0, message: 'Valoración existente', valoracion })	
			}

			let puntuacion = 0
			switch(valoracion){
				case 'angry':
					puntuacion = 20
				break;

				case 'sad':
					puntuacion = 40
				break;

				case 'ok':
					puntuacion = 60
				break;

				case 'good':
					puntuacion = 80
				break;

				case 'happy':
					puntuacion = 100
				break;

			}
			// Agrgear valoracion
			const agregarValoracion = await catalogos.agregarValoracion( id_usuario, id_grupo, id_ciclo, id_teacher, num_teacher, puntuacion, escuela, dia ).then(response=> response)

			res.send({ status: 1, message: 'Evaluación eviada', puntuacion })
		}else{
			res.send({ status: 0, message: 'No aplica', puntuacion })			
		}
  } catch (error) {
		res.status(500).send({message:error})
  }
};

// Validar si hay clase activa o no
exports.validarClaseActiva = async(req, res) => {
	try {	
		const { id_usuario, escuela } = req.body
		let   existeClaseActiva= null

		if( escuela == 2 ){
			existeClaseActiva = await catalogos.existeClaseActivaFast( id_usuario ).then(response => response)
		}else{
			existeClaseActiva = await catalogos.existeClaseActivaInbi( id_usuario ).then(response => response)
		}

		if( existeClaseActiva ){
			// Validar si el alumno ya contesto la evaluación
			const { id_grupo, id_ciclo, dia } = existeClaseActiva

			// Consultar la evaluación
			const claseEvaluada = await catalogos.existeValoracion( id_usuario, id_grupo, id_ciclo, escuela, dia ).then(response=> response)

			if( claseEvaluada ){
				return res.send({ status: 0, message: 'Valoración existente' })	
			}
			res.send({ status: 1, message: 'Clase Active' })
		}else{
			res.send({ status: 0, message: 'Clase Inactive' })			
		}

  } catch (error) {
		res.status(500).send({message:error})
  }
};

exports.getCalificacionClase = async(req, res) => {
	try {	
		// Caputramos los datos
		const { cicloInbi, cicloFast, fecha, fecha2, tipo } = req.body
		
		/* FAST */
		let teachersActualesFast = []
		let gruposValoracionFast = []

		let teachersActualesInbi = []
		let gruposValoracionInbi = []

		// Header básico
		let headers = [
      { text: 'id'             , value: 'id_grupo' },
      { text: 'Grupo'          , value: 'grupo' },
      { text: 'Teachers'       , value: 'teachers' },
      { text: 'Cant. Alumnos'  , value: 'cant_alumnos' },
    ]

    // incialización de las clases
		let clasesFast = []
		let clasesInbi = []

		let cantAlumnosFAST = 0
		let cantAlumnosINBI = 0

		// Obtener la diferencia de fechas en días
		const { dias } = await catalogos.getDiferenciaDias( fecha, fecha2 ).then( response => response )

		// Recorremos todas esas fechas
		for (var i = 0; i <= dias; i++) {
			clasesFast = []

			// Obtenemos las clases que se dan en ese dia
			clasesFast = await catalogos.teachersFastDiaMaestros( cicloFast, fecha, i ).then( response => response )
			clasesInbi = await catalogos.teachersInbiDiaMaestros( cicloInbi, fecha, i ).then( response => response )

			// Ahora las calificaciones en una fecha en especifico 
			gruposValoracionFast = await catalogos.getValoracionGrupo( cicloInbi, cicloFast, fecha, fecha, 2, i ).then(response => response)
			gruposValoracionInbi = await catalogos.getValoracionGrupo( cicloInbi, cicloFast, fecha, fecha, 1, i ).then(response => response)



			/**********************************************************/
			/*             CREAMOS EL ARREGLOS DE CLASES
			/**********************************************************/

			// hacemos este for para poder saber si esa clase ya existía en nuestro arreglo
			// si no existe, se agraga, si existe se omite
			for(const j in clasesFast){
				const { id_grupo, num_teacher, cant_alumnos } = clasesFast[j]
				cantAlumnosFAST += cant_alumnos
				// Validar si ese grupo ya existe en: teachersActualesFast
				const existe = teachersActualesFast.find( el=> el.id_grupo == id_grupo)
				if(!existe){ 
					// Agregamos e iniciamos en 0
					clasesFast[j]['acumulado'] = cant_alumnos
					clasesFast[j]['acumulado-t1'] = num_teacher == 1 ? cant_alumnos : 0
					clasesFast[j]['acumulado-t2'] = num_teacher == 2 ? cant_alumnos : 0
					teachersActualesFast.push( clasesFast[j] ) 
				}else{
					// Acumulamos
					existe['acumulado'] += cant_alumnos
					existe['acumulado-t1'] += num_teacher == 1 ? cant_alumnos : 0 
					existe['acumulado-t2'] += num_teacher == 2 ? cant_alumnos : 0 
				}
			}

			for(const j in clasesInbi){
				const { id_grupo, num_teacher, cant_alumnos } = clasesInbi[j]
				cantAlumnosINBI += cant_alumnos
				// Validar si ese grupo ya existe en: teachersActualesInbi
				const existe = teachersActualesInbi.find( el=> el.id_grupo == id_grupo)
				if(!existe){ 
					// Agregamos e iniciamos en 0
					clasesInbi[j]['acumulado'] = cant_alumnos
					clasesInbi[j]['acumulado-t1'] = num_teacher == 1 ? cant_alumnos : 0
					clasesInbi[j]['acumulado-t2'] = num_teacher == 2 ? cant_alumnos : 0
					teachersActualesInbi.push( clasesInbi[j] ) 
				}else{
					// Acumulamos
					existe['acumulado'] += cant_alumnos
					existe['acumulado-t1'] += num_teacher == 1 ? cant_alumnos : 0 
					existe['acumulado-t2'] += num_teacher == 2 ? cant_alumnos : 0 
				}
			}

			/**********************************************************/
			/*             AGREGAR CALIFICACION POR FECHA
			/**********************************************************/
			
			// Aignamos sus valores a cada clase
			const { fecha2 } = await catalogos.returnFecha( fecha, i ).then( response=> response ) 
			
			//agrega las calificaciones de cada fecha en FAST
			for(const k in teachersActualesFast){
				const { id_grupo } = teachersActualesFast[k]
				// Vamos a buscar ese teacher en los teacher 1
				const existe = gruposValoracionFast.find(el=> el.id_grupo == id_grupo )
				teachersActualesFast[k][`cant_calif-${fecha2}`] = existe ? existe.cant_calif : 0
				teachersActualesFast[k][`promedio-${fecha2}`] = existe ? existe.promedio : 0
			}

			//agrega las calificaciones de cada fecha en INBI
			for(const k in teachersActualesInbi){
				const { id_grupo } = teachersActualesInbi[k]
				// Vamos a buscar ese teacher en los teacher 1
				const existe = gruposValoracionInbi.find(el=> el.id_grupo == id_grupo )
				teachersActualesInbi[k][`cant_calif-${fecha2}`] = existe ? existe.cant_calif : 0
				teachersActualesInbi[k][`promedio-${fecha2}`] = existe ? existe.promedio : 0
			}


			/**********************************************************/
			/*         AGREGAMOS LOS HEADER PARA CADA COLUMNA
			/**********************************************************/
			headers.push(
				{ text: `cant_calif-${fecha2}` , value: `cant_calif-${fecha2}` },
				{ text: `promedio-${fecha2}`   , value: `promedio-${fecha2}` }
			)

		}

		/**********************************************************/
		/*            PROMEDIO GENERAL POR TEACHER
		/**********************************************************/

		// OBTENER LAS CALIFICACIONES EN GENERAL ENTRE ESAS FECHAS
		// CICLOINBI, CICLOFAST, FECHAINICIO, FECHAFINAL, ESCUELA, 0 ( EN LA CONSULTA ESTE NUMERO SIRVE PARA AGREGARLE DÍAS A LA FECHA)
		gruposValoracionFast = await catalogos.getValoracionGrupo( cicloInbi, cicloFast, fecha, fecha2, 2, 0 ).then(response => response)


		// Aignamos sus valores a cada clase
		for(const k in teachersActualesFast){
			const { id_grupo, id_teacher, id_teacher_2, diasdeFrecuencia } = teachersActualesFast[k]
			// Vamos a buscar ese teacher en los teacher 1
			const existe = gruposValoracionFast.find(el=> el.id_grupo == id_grupo )
			teachersActualesFast[k][`cant_calif-general`] = existe ? existe.cant_calif : 0
			teachersActualesFast[k][`promedio-general`] = existe ? existe.promedio : 0

			const existe1 = gruposValoracionFast.find(el=> el.id_grupo == id_grupo && el.id_teacher == id_teacher )
			teachersActualesFast[k][`cant_calif-general-T1`] = existe1 ? existe1.cant_calif : 0
			teachersActualesFast[k][`promedio-general-T1`] = existe1 ? existe1.promedio : 0

			let existe2 = null
			if(id_teacher != id_teacher_2){
				existe2 = gruposValoracionFast.find(el=> el.id_grupo == id_grupo && el.id_teacher == id_teacher )
				teachersActualesFast[k][`cant_calif-general-T2`] = existe2 ? existe2.cant_calif : 0
				teachersActualesFast[k][`promedio-general-T2`] = existe2 ? existe2.promedio : 0
			}
		}


		// Ahora las calificaciones generales
		gruposValoracionInbi = await catalogos.getValoracionGrupo( cicloInbi, cicloFast, fecha, fecha2, 1, 0 ).then(response => response)

		// Aignamos sus valores a cada clase
		
		for(const k in teachersActualesInbi){
			const { id_grupo, id_teacher, id_teacher_2, diasdeFrecuencia } = teachersActualesInbi[k]
			// Vamos a buscar ese teacher en los teacher 1
			const existe = gruposValoracionInbi.find(el=> el.id_grupo == id_grupo )
			teachersActualesInbi[k][`cant_calif-general`] = existe ? existe.cant_calif : 0
			teachersActualesInbi[k][`promedio-general`] = existe ? existe.promedio : 0

			const existe1 = gruposValoracionInbi.find(el=> el.id_grupo == id_grupo && el.id_teacher == id_teacher )
			teachersActualesInbi[k][`cant_calif-general-T1`] = existe1 ? existe1.cant_calif : 0
			teachersActualesInbi[k][`promedio-general-T1`] = existe1 ? existe1.promedio : 0

			if(id_teacher != id_teacher_2){
				const existe2 = gruposValoracionInbi.find(el=> el.id_grupo == id_grupo && el.id_teacher == id_teacher )
				teachersActualesInbi[k][`cant_calif-general-T2`] = existe2 ? existe2.cant_calif : 0
				teachersActualesInbi[k][`promedio-general-T2`] = existe2 ? existe2.promedio : 0
			}
			
		}

		headers.push(
			{ text: `cant_calif-real`        , value: `acumulado` },
			{ text: `cant_calif-general`     , value: `cant_calif-general` },
			{ text: `promedio-general`       , value: `promedio-general` },
			{ text: `teacher1`               , value: `teacher1` },
			{ text: `cant_calif-real-T1`     , value: `acumulado-t1` },
			{ text: `cant_calif-general-T1`  , value: `cant_calif-general-T1` },
			{ text: `promedio-general-T1`    , value: `promedio-general-T1` },
			{ text: `teacher2`               , value: `teacher2` },
			{ text: `cant_calif-real-T2`     , value: `acumulado-t2` },
			{ text: `cant_calif-general-T2`  , value: `cant_calif-general-T2` },
			{ text: `promedio-general-T2`    , value: `promedio-general-T2` },
		)

		/**********************************************************/
		/*                    PROMEDIO GENERAL
		/**********************************************************/

		let promedioFast = await catalogos.getPromedioClaseFecha( cicloInbi, cicloFast, fecha, fecha2, 2, 0 ).then(response => response)
		let promedioInbi = await catalogos.getPromedioClaseFecha( cicloInbi, cicloFast, fecha, fecha2, 1, 0 ).then(response => response)

		promedioFast['cantClases'] = clasesFast.length
		promedioInbi['cantClases'] = clasesInbi.length

		let clases_calidad_fast     = 0
		let clases_no_calidad_fast  = 0
		let clases_sin_calidad_fast = 0
		for( const i in teachersActualesFast){
			let promedio = 0
			if( teachersActualesFast[i]['cant_calif-general'] > 0){
				promedio = (teachersActualesFast[i]['cant_calif-general']/teachersActualesFast[i]['acumulado'])*100
			}

			if( teachersActualesFast[i]['cant_calif-general'] > 0 && promedio >= 80){
				clases_calidad_fast ++
			}

			if( promedio < 80 && teachersActualesFast[i]['cant_calif-general'] > 0 ){
				clases_no_calidad_fast ++
			}

			if( teachersActualesFast[i]['cant_calif-general'] == 0 ){
				clases_sin_calidad_fast ++
			}
		}

		promedioFast['clases_calidad_fast']     = clases_calidad_fast
		promedioFast['clases_no_calidad_fast']  = clases_no_calidad_fast
		promedioFast['clases_sin_calidad_fast'] = clases_sin_calidad_fast
		promedioFast['cantAlumnosFAST'] = cantAlumnosFAST


		/**********************/

		let clases_calidad_inbi     = 0
		let clases_no_calidad_inbi  = 0
		let clases_sin_calidad_inbi = 0
		for( const i in teachersActualesInbi){

			let promedio = 0
			if( teachersActualesInbi[i]['cant_calif-general'] > 0){
				promedio = (teachersActualesInbi[i]['cant_calif-general']/teachersActualesInbi[i]['acumulado'])*100
			}

			if( promedio >= 80 && teachersActualesInbi[i]['cant_calif-general'] > 0 ){
				clases_calidad_inbi ++
			}

			if( promedio < 80 && teachersActualesInbi[i]['cant_calif-general'] > 0 ){
				clases_no_calidad_inbi ++
			}

			if( teachersActualesInbi[i]['cant_calif-general'] == 0 ){
				clases_sin_calidad_inbi ++
			}
		}

		promedioInbi['clases_calidad_inbi']     = clases_calidad_inbi
		promedioInbi['clases_no_calidad_inbi']  = clases_no_calidad_inbi
		promedioInbi['clases_sin_calidad_inbi'] = clases_sin_calidad_inbi
		promedioInbi['cantAlumnosINBI']         = cantAlumnosINBI


		// Generamos la repsuesta
		const respuesta = {
			tipo,
			teachersActualesFast,
			teachersActualesInbi,
			headers,
			promedioFast,
			promedioInbi,
		}

		// Envíamos la respuesta
		res.send(respuesta);
  } catch (error) {
		res.status(500).send({message:error})
  }
};

exports.getCalificacionClaseDesglose = async(req, res) => {
	try {	
		// Caputramos los datos
		const { cicloInbi, cicloFast, fecha, fecha2 } = req.body
		
		/* FAST */
		let teachersActualesFast = []
		let gruposValoracionFast = []

		let teachersActualesInbi = []
		let gruposValoracionInbi = []

		// Header básico
		let headers = [
      { text: 'id'             , value: 'id_grupo' },
      { text: 'Grupo'          , value: 'grupo' },
      { text: 'Teachers'       , value: 'teachers' },
      { text: 'Cant. Alumnos'  , value: 'cant_alumnos' },
    ]

    // incialización de las clases
		let clasesFast = []
		let clasesInbi = []

		let cantAlumnosFAST = 0
		let cantAlumnosINBI = 0

		// Obtenemos las clases que se dan en ese dia
		clasesFast = await catalogos.teachersFastDiaMaestros( cicloFast, fecha, 0 ).then( response => response )
		clasesInbi = await catalogos.teachersInbiDiaMaestros( cicloInbi, fecha, 0 ).then( response => response )


		// Ahora las calificaciones en una fecha en especifico 
		gruposValoracionFast = await catalogos.getValoracionGrupo( cicloInbi, cicloFast, fecha, fecha2, 2, 0 ).then(response => response)
		gruposValoracionInbi = await catalogos.getValoracionGrupo( cicloInbi, cicloFast, fecha, fecha2, 1, 0 ).then(response => response)


		/**********************************************************/
		/*             CREAMOS EL ARREGLOS DE CLASES
		/**********************************************************/

		// hacemos este for para poder saber si esa clase ya existía en nuestro arreglo
		// si no existe, se agraga, si existe se omite
		for(const j in clasesFast){
			const { id_grupo, num_teacher, cant_alumnos } = clasesFast[j]
			cantAlumnosFAST += cant_alumnos
			clasesFast[j]['acumulado'] = cant_alumnos
			teachersActualesFast.push( clasesFast[j] ) 
		}

		for(const j in clasesInbi){
			const { id_grupo, num_teacher, cant_alumnos } = clasesInbi[j]
			cantAlumnosINBI += cant_alumnos
			// Agregamos e iniciamos en 0
			clasesInbi[j]['acumulado'] = cant_alumnos
			teachersActualesInbi.push( clasesInbi[j] ) 
		}

		/**********************************************************/
		/*             AGREGAR CALIFICACION POR FECHA
		/**********************************************************/
		
		//agrega las calificaciones de cada fecha en FAST
		for(const k in teachersActualesFast){
			const { id_grupo } = teachersActualesFast[k]
			// Vamos a buscar ese teacher en los teacher 1
			const existe = gruposValoracionFast.find(el=> el.id_grupo == id_grupo )
			teachersActualesFast[k][`cant_calif`] = existe ? existe.cant_calif : 0
			teachersActualesFast[k][`promedio`] = existe ? existe.promedio : 0
		}

		//agrega las calificaciones de cada fecha en INBI
		for(const k in teachersActualesInbi){
			const { id_grupo } = teachersActualesInbi[k]
			// Vamos a buscar ese teacher en los teacher 1
			const existe = gruposValoracionInbi.find(el=> el.id_grupo == id_grupo )
			teachersActualesInbi[k][`cant_calif`] = existe ? existe.cant_calif : 0
			teachersActualesInbi[k][`promedio`]   = existe ? existe.promedio : 0
		}


		/**********************************************************/
		/*         AGREGAMOS LOS HEADER PARA CADA COLUMNA
		/**********************************************************/
		headers.push(
			{ text: `cant_calif` , value: `cant_calif` },
			{ text: `promedio`   , value: `promedio` }
		)


		/**********************************************************/
		/*                    PROMEDIO GENERAL
		/**********************************************************/

		let promedioFast = await catalogos.getPromedioClaseFecha( cicloInbi, cicloFast, fecha, fecha, 2, 0 ).then(response => response)
		let promedioInbi = await catalogos.getPromedioClaseFecha( cicloInbi, cicloFast, fecha, fecha, 1, 0 ).then(response => response)

		console.log( cicloInbi, cicloFast, fecha, fecha )

		let clases_calidad = [
			{
				escuela: 2,
				clases_no_evaluadas: [],
				clases_buenas:       [],
				clases_malas:        [],
				clases_evaluadas:    [],
				todas:               []
			},
			{
				escuela: 1,
				clases_no_evaluadas: [],
				clases_buenas:       [],
				clases_malas:        [],
				clases_evaluadas:    [],
				todas:               []
			}
		]


		let clases_calidad_fast     = 0
		let clases_no_calidad_fast  = 0
		let clases_sin_calidad_fast = 0
		for( const i in teachersActualesFast){
			let promedio = 0
			if( teachersActualesFast[i]['cant_calif'] > 0){
				promedio = (teachersActualesFast[i]['cant_calif']/teachersActualesFast[i]['acumulado'])*100
			}

			if( teachersActualesFast[i]['cant_calif'] > 0 && promedio >= 80){
				clases_calidad_fast ++
				clases_calidad[0].clases_buenas.push( teachersActualesFast[i] )
			}

			if( promedio < 80 && teachersActualesFast[i]['cant_calif'] > 0 ){
				clases_no_calidad_fast ++
				clases_calidad[0].clases_malas.push( teachersActualesFast[i] )
			}

			if( teachersActualesFast[i]['cant_calif'] == 0 ){
				clases_sin_calidad_fast ++
				clases_calidad[0].clases_no_evaluadas.push( teachersActualesFast[i] )
			}else{
				clases_calidad[0].clases_evaluadas.push( teachersActualesFast[i] )
			}

			clases_calidad[0].todas.push( teachersActualesFast[i] )
		}

		promedioFast['cantClases']              = clasesFast.length
		promedioFast['cantClasesEvaluadas']     = gruposValoracionFast.length
		promedioFast['clases_calidad']          = clases_calidad_fast
		promedioFast['clases_no_calidad']       = clases_no_calidad_fast
		promedioFast['clases_sin_calidad']      = clases_sin_calidad_fast
		promedioFast['cantReal']                = cantAlumnosFAST


		/**********************/

		let clases_calidad_inbi     = 0
		let clases_no_calidad_inbi  = 0
		let clases_sin_calidad_inbi = 0
		for( const i in teachersActualesInbi){

			let promedio = 0
			if( teachersActualesInbi[i]['cant_calif'] > 0){
				promedio = (teachersActualesInbi[i]['cant_calif']/teachersActualesInbi[i]['acumulado'])*100
			}

			if( promedio >= 80 && teachersActualesInbi[i]['cant_calif'] > 0 ){
				clases_calidad[1].clases_buenas.push( teachersActualesInbi[i] )
				clases_calidad_inbi ++
			}

			if( promedio < 80 && teachersActualesInbi[i]['cant_calif'] > 0 ){
				clases_no_calidad_inbi ++
				clases_calidad[1].clases_malas.push( teachersActualesInbi[i] )
			}

			if( teachersActualesInbi[i]['cant_calif'] == 0 ){
				clases_sin_calidad_inbi ++
				clases_calidad[1].clases_no_evaluadas.push( teachersActualesInbi[i] )
			}else{
        clases_calidad[1].clases_evaluadas.push( teachersActualesInbi[i] )
			}

			 clases_calidad[1].todas.push( teachersActualesInbi[i] )
		}

		promedioInbi['cantClases']              = clasesInbi.length
		promedioInbi['cantClasesEvaluadas']     = gruposValoracionInbi.length
		promedioInbi['clases_calidad']          = clases_calidad_inbi
		promedioInbi['clases_no_calidad']       = clases_no_calidad_inbi
		promedioInbi['clases_sin_calidad']      = clases_sin_calidad_inbi
		promedioInbi['cantReal']                = cantAlumnosINBI


		// Generamos la repsuesta
		const respuesta = {
			clases_calidad,
			teachersActualesFast,
			teachersActualesInbi,
			headers,
			promedioFast,
			promedioInbi,
		}


		// Envíamos la respuesta
		res.send(respuesta);
  } catch (error) {
		res.status(500).send({message:error})
  }
};

// Validar evaluacion de la clase
exports.getAlumnosCicloActual = async(req, res) => {
	try {	
		
		// Consultamos el ciclo actual
		const cicloActual = await catalogos.getCicloActual( ).then( response => response )

		// Verificamos que haya un ciclo actual 
		if( !cicloActual ){ return res.status(400).send({message: 'No cuentas con ciclos activos'}) }

		// desestructuramos el resultado
		const { ciclo_inbi, ciclo_fast, fecha_inicio_ciclo, fecha_fin_ciclo } = cicloActual

		// Consultamos los alumnos inscritos en ese ciclo
		let alumnos = await catalogos.getAlumnosCicloActual( ciclo_inbi, ciclo_fast ).then( response => response )

		// Consultar el grupoAlumnos
		const gruposAlumnosFast = await catalogos.getGruposAlumnosLMS( ciclo_fast, 2 ).then(response => response)
		const gruposAlumnosInbi = await catalogos.getGruposAlumnosLMS( ciclo_inbi, 1 ).then(response => response)

		// Cargamos los grupos
		const gruposFast = await catalogos.getGruposLMSFASTAll( ciclo_fast, 2 ).then(response => response)
		const gruposInbi = await catalogos.getGruposLMSFASTAll( ciclo_inbi, 1 ).then(response => response)

		// Cargamos los alumnos
		const alumnosFast = await catalogos.getAlumnosLMSFAST( 2 ).then(response => response)
		const alumnosInbi = await catalogos.getAlumnosLMSFAST( 1 ).then(response => response)

		// Recorremos los alumnos
		for( const i in alumnos ){
			// Desestrucutramos el arreglo
			const { escuela, id_alumno, id_grupo } = alumnos[i]
			// Guardamos el nuevo grupo
			const nuevoGrupoERP = id_grupo

			// Verificamos que el alumnoo tenga un grupo en el ciclo 4
			const existeGrupoAlumno = escuela == 1 ? gruposAlumnosInbi.find( el=> el.u_iderp == id_alumno ) : gruposAlumnosFast.find( el=> el.u_iderp == id_alumno )

			// Si existe, hay que validar si son el mismo grupo
			if( existeGrupoAlumno ){
				// si el alumno esta en un grupo diferente, hay que eliminar asistencias y volver a generarlas
				if(existeGrupoAlumno.iderp != id_grupo ){
					const { id_alumno, id_grupo, id_nivel } = existeGrupoAlumno

					// buscamos el nuevo grupo
					let nuevoGrupo = escuela == 1 ? gruposInbi.find( el => el.iderp == nuevoGrupoERP) : gruposFast.find( el => el.iderp == nuevoGrupoERP)

					// Verificamos que existe el nuevo grupo
					if( nuevoGrupo ){ 

						// Actualizar el grupo en grupo alumnos
						if( escuela == 1 ){
							const updateGruposAlumnoInbi = await catalogos.updateGruposAlumnoInbi( existeGrupoAlumno.id_grupo_alumnos, nuevoGrupo.id ).then(response => response)
						}else{
							const updateGruposAlumnoFast = await catalogos.updateGruposAlumnoFast( existeGrupoAlumno.id_grupo_alumnos, nuevoGrupo.id, escuela ).then(response => response)
						}

						// Son diferentes, hay que borrar las asistenciassss, borrarrrr
						if( escuela == 1 ){
							const eliminarAsistencias = await catalogos.eliminarAsistenciasInbi( id_alumno, ciclo_inbi ).then(response => response)
						}else{
							const eliminarAsistencias = await catalogos.eliminarAsistenciasFast( id_alumno, ciclo_fast ).then(response => response)
						}
					}
				}
			}else{
				
				let existeGrupo  = null 
				let existeAlumno = null
				if( escuela == 1 ){
					// Buscamos el grupo en el que va
					existeGrupo  = gruposInbi.find( el=> el.iderp == alumnos[i].id_grupo )
					existeAlumno = alumnosInbi.find( el=> el.iderp == alumnos[i].id_alumno )

					// Verificamos que el alumno se agregue correctamente
					if(existeGrupo && existeAlumno){
						// Agregamos el grupo_alumno, mandamos id_grupo del lms y id alumno del LMS
		  			const addGrupoAlumnoInbi = await catalogos.addGrupoAlumnoInbi( existeGrupo.id, existeAlumno.id ).then(response => response)
					}
				}else{
					// Buscamos el grupo en el que va
					existeGrupo  = gruposFast.find( el=> el.iderp == alumnos[i].id_grupo )
					existeAlumno = alumnosFast.find( el=> el.iderp == alumnos[i].id_alumno )

					// Verificamos que el alumno se agregue correctamente
					if(existeGrupo && existeAlumno){
						// Agregamos el grupo_alumno, mandamos id_grupo del lms y id alumno del LMS
		  			const addGrupoAlumnoFast = await catalogos.addGrupoAlumnoFast( existeGrupo.id, existeAlumno.id, 2 ).then(response => response)
					}
				}
			}
		}

		res.send({
			cicloActual,
			alumnos 
		})

  } catch (error) {
		res.status(500).send({message:error})
  }
};

// Reparar los detalles del alumno
exports.ultimoAccesoAlumnos = async(req, res) => {
	try {	
		// Caputramos los datos
		const { cicloFast, cicloInbi } = req.params
		// Hacemos una peticion async para obtener los datos de ambas bases de datos
		const gruposFast = await catalogos.ultimoAccesoFast(cicloFast).then(response => response)
		const gruposInbi = await catalogos.ultimoAccesoInbi(cicloInbi).then(response => response)
  	
  	let fechaEspaniol    = await calificaciones.fechaEspaniol( 1 ).then( response => response )
  	fechaEspaniol        = await calificaciones.fechaEspaniol( 2 ).then( response => response )

		// Generamos la repsuesta
		const respuesta = gruposFast.concat( gruposInbi )
		// Envíamos la respuesta
		res.send(respuesta);
  } catch (error) {
		res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

/**************************************************/
/******* C O N F I G U R A R ** G R U P O ********/
/**************************************************/

// Reparar los detalles del alumno
exports.configurarGrupo = async(req, res) => {
	try {	

		const{ lunes, martes, miercoles, jueves, viernes, sabado, domingo, fecha_inicio, id_grupo, id_curso, id_ciclo, id_nivel, escuela } = req.body 

		let dias = [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday' ]

		let recorrido    = [1,2,3,4]
		let sesion       = 1
		let diaLunes     = 0
		let diaMartes    = 1
		let diaMiercoles = 2
		let diaJueves    = 3
		let diaViernes   = 4
		let diaSabado    = 5
		let diaDomingo   = 6

		// Valdiar si existe alguna configuración, si existe, se elimina y agrega
		const existeConfiguracionGrupo = await catalogos.existeConfiguracionGrupo( id_grupo, escuela ).then( response => response )

		if( existeConfiguracionGrupo ){
			const eliminarConfiguracionGrupo = await catalogos.eliminarConfiguracionGrupo( id_grupo, escuela ).then( response => response )
		}
		
		for( const i in recorrido ){

			if( lunes ){
				const addCofigGrupo = await catalogos.addCofigGrupo( id_grupo, id_curso, id_ciclo, id_nivel, sesion, dias[0], fecha_inicio, diaLunes, escuela ).then( response => response )
				sesion += 1
				diaLunes += 7
			}

			if( martes ){
				const addCofigGrupo = await catalogos.addCofigGrupo( id_grupo, id_curso, id_ciclo, id_nivel, sesion, dias[1], fecha_inicio, diaMartes, escuela ).then( response => response )
				sesion += 1
				diaMartes += 7
			}

			if( miercoles ){
				const addCofigGrupo = await catalogos.addCofigGrupo( id_grupo, id_curso, id_ciclo, id_nivel, sesion, dias[2], fecha_inicio, diaMiercoles, escuela ).then( response => response )
				sesion += 1
				diaMiercoles += 7
			}

			if( jueves ){
				const addCofigGrupo = await catalogos.addCofigGrupo( id_grupo, id_curso, id_ciclo, id_nivel, sesion, dias[3], fecha_inicio, diaJueves, escuela ).then( response => response )
				sesion += 1
				diaJueves += 7
			}

			if( viernes ){
				const addCofigGrupo = await catalogos.addCofigGrupo( id_grupo, id_curso, id_ciclo, id_nivel, sesion, dias[4], fecha_inicio, diaViernes, escuela ).then( response => response )
				sesion += 1
				diaViernes += 7
			}

			if( sabado ){
				const addCofigGrupo = await catalogos.addCofigGrupo( id_grupo, id_curso, id_ciclo, id_nivel, sesion, dias[5], fecha_inicio, diaSabado, escuela ).then( response => response )
				sesion += 1
				diaSabado += 7
			}

			if( domingo ){
				const addCofigGrupo = await catalogos.addCofigGrupo( id_grupo, id_curso, id_ciclo, id_nivel, sesion, dias[6], fecha_inicio, diaDomingo, escuela ).then( response => response )
				sesion += 1
				diaDomingo += 7
			}

		}
		
		const updateCursoGrupo = await catalogos.updateCursoGrupo( id_grupo, id_curso, escuela ).then( response => response )

		
		res.send({ message: 'Api correcta'});
  } catch (error) {
		res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getCursosLMS = async(req, res) => {
	try {	
		
		const cursosFAST = await catalogos.getCursosLMS( 2 ).then( response => response )
		const cursosINBI = await catalogos.getCursosLMS( 1 ).then( response => response )

		res.send({ cursosFAST, cursosINBI });
  } catch (error) {
		res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

exports.soporteLimpieza = async(req, res) => {
	try {	

		const escuelas = [{escuela: 1, escuela: 2}]

		for( const i in escuelas ){
			const { escuela } = escuelas[i]
			// Habilitar el groupBy
	    await catalogos.habilitarGroupByEscuela( escuela ).then(response=> response)

	    /* ASISTENCIAS */
	    /*********************************************************************************************************************************/
	    let limitAsistencias     = 50
	    let baseAsistencias      = 1
	    let asistenciasRepetidas = []
	    do{

				asistenciasRepetidas = await catalogos.asistenciasRepetidas( escuela ).then( response => response )

				// Si hay más de una asistencia repetida
				if( asistenciasRepetidas.length ){

					//  hay que recorrerlas
					for( const j in asistenciasRepetidas ){
						const { id } = asistenciasRepetidas[j]

						// Y eliminarlas
						const deleteAsistenciaRepetida = await catalogos.deleteAsistenciaRepetida( escuela, id ).then( response => response )
					}
				}

				baseAsistencias += 1 

				if( baseAsistencias >= limitAsistencias ){ break }
	    }while( asistenciasRepetidas.length );
	    /*********************************************************************************************************************************/

	    /* calificaciones */
	  	/*********************************************************************************************************************************/
	    let limitCalificaciones  = 50
	    let baseCalificaciones   = 1
	    let calificacionesRepet  = []
	    do{

				calificacionesRepet = await catalogos.calificacionesRepet( escuela ).then( response => response )

				// Si hay más de una asistencia repetida
				if( calificacionesRepet.length ){

					//  hay que recorrerlas
					for( const j in calificacionesRepet ){
						const { id } = calificacionesRepet[j]

						// Y eliminarlas
						const deleteCalifRepet = await catalogos.deleteCalifRepet( escuela, id ).then( response => response )
					}
				}

				baseCalificaciones += 1 

				if( baseCalificaciones >= limitCalificaciones ){ break }
	    }while( calificacionesRepet.length );
	    /*********************************************************************************************************************************/


	  	/* alumnos repetidos */
	  	/*********************************************************************************************************************************/
	    let limitAlumRepet  = 50
	    let baseAlumRepet   = 1
	    let alumnosRepet  = []
	    do{

				alumnosRepet = await catalogos.alumnosRepet( escuela ).then( response => response )

				// Si hay más de una asistencia repetida
				if( alumnosRepet.length ){

					//  hay que recorrerlas
					for( const j in alumnosRepet ){
						const { id } = alumnosRepet[j]

						// Y eliminarlas
						const deleteAlumnoRepet = await catalogos.deleteAlumnoRepet( escuela, id ).then( response => response )
					}
				}

				baseAlumRepet += 1 

				if( baseAlumRepet >= limitAlumRepet ){ break }
	    }while( alumnosRepet.length );
	    /*********************************************************************************************************************************/


	  	/* datos repetidos */
	  	/*********************************************************************************************************************************/
	    let limitDatosRepet  = 50
	    let baseDatosRepet   = 1
	    let datosRepetAlum  = []
	    do{

				datosRepetAlum = await catalogos.datosRepetAlum( escuela ).then( response => response )

				// Si hay más de una asistencia repetida
				if( datosRepetAlum.length ){

					//  hay que recorrerlas
					for( const j in datosRepetAlum ){
						const { id } = datosRepetAlum[j]

						// Y eliminarlas
						const deleteDatosRepet = await catalogos.deleteDatosRepet( escuela, id ).then( response => response )
					}
				}

				baseDatosRepet += 1 

				if( baseDatosRepet >= limitDatosRepet ){ break }
	    }while( datosRepetAlum.length );
	    /*********************************************************************************************************************************/


	  	/* avatares */
	  	/*********************************************************************************************************************************/
	    let limitAvatares  = 50
	    let baseAvatares   = 1
	    let datosAvatares  = []
	    do{

				datosAvatares = await catalogos.datosAvatares( escuela ).then( response => response )

				// Si hay más de una asistencia repetida
				if( datosAvatares.length ){

					//  hay que recorrerlas
					for( const j in datosAvatares ){
						const { id } = datosAvatares[j]

						// Y eliminarlas
						const deleteAvatares = await catalogos.deleteAvatares( escuela, id ).then( response => response )
					}
				}

				baseAvatares += 1 

				if( baseAvatares >= limitAvatares ){ break }
	    }while( datosAvatares.length );
	    /*********************************************************************************************************************************/
		}

		res.send({ message: 'Datos actualizados correctamente' });
  } catch (error) {
		res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};


exports.gruposSiguiente = async(req, res) => {
	try {
		// Caputramos los datos
		const { id_ciclo, id_ciclo_relacionado, ciclo } = req.body

		/*********    *******/
		// CONSULTAMOS LOS CICLOS 
		const getCiclosActivos = await cicloserpactivos.getCiclosActivos( ).then(response=> response)

		// VAMOS A SACAR EL CICLO QUE LE SIGUE A ESE
		// dividimos en partes el ciclo
		const nombreCiclo = ciclo.split(' ')

    // La segunda parte o la parte de enmedio del ciclo, es el numero
    const numeroCiclo = nombreCiclo[1].split('_')

    const mesCiclo  = parseInt(numeroCiclo[0]) == 12 ? 1 : ( parseInt(numeroCiclo[0]) + 1 )
    const anioCiclo = parseInt(numeroCiclo[0]) == 12 ? (parseInt(numeroCiclo[1]) + 1)  : parseInt(numeroCiclo[1])

    const nuevoCicloInbi = 'CICLO ' + ( mesCiclo < 10 ? ('0'+mesCiclo) : mesCiclo ) + '_' + anioCiclo
    const nuevoCicloFast = 'CICLO ' + ( mesCiclo < 10 ? ('0'+mesCiclo) : mesCiclo ) + '_' + anioCiclo + ' FE'

    // Buscar el nuevo cilo a ver si ya existe 
    const existeNuevoCicloInbi = getCiclosActivos.find( el => el.ciclo == nuevoCicloInbi )
    const existeNuevoCicloFast = getCiclosActivos.find( el => el.ciclo == nuevoCicloFast )

    if( !existeNuevoCicloInbi ){ return res.status( 400 ).send({ message: `El ciclo: ${ nuevoCicloInbi } no existe` }) }
    if( !existeNuevoCicloFast ){ return res.status( 400 ).send({ message: `El ciclo: ${ nuevoCicloFast } no existe` }) }

		/*********    *******/
    // Hacemos una peticion async para obtener los datos de ambas bases de datos
		const gruposAlumnosInbi = await catalogos.getGruposERP( id_ciclo, nuevoCicloInbi, existeNuevoCicloInbi.id_ciclo ).then(response => response)
		const gruposAlumnosFast = await catalogos.getGruposERP( id_ciclo_relacionado, nuevoCicloFast, existeNuevoCicloFast.id_ciclo ).then(response => response)

		// Generamos la repsuesta
		const respuesta = gruposAlumnosFast.concat( gruposAlumnosInbi )
		
		// Envíamos la respuesta
		res.send(respuesta);
  } catch (error) {
		res.status(500).send({message: error ? error.message : 'Error en el servidor'})
  }
};