const whatsapp       = require("../../models/operaciones/whatsapp.model");
const prospectos     = require("../../models/prospectos/prospectos.model.js");
const fs             = require('fs');
const request        = require("request-promise-native");
const { v4: uuidv4 } = require('uuid')


// const configWhatsApp = require("../../helpers/whatsServidor1.js");
// require("../../helpers/whatsServidor2.js");

exports.abrirWhatsApp = async(req, res) => {
  try {

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor' })
  }
};

exports.enviarMensaje = async(req, res) => {
  try {
  	const { from, mensaje, imagen } = req.body

    configWhatsApp.enviarMensajeInbi(from, mensaje, imagen )
    // client.sendMessage( from, mensaje );

		res.send({ message: 'pjpok' })

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor' })
  }
};

exports.getUsuariosWhatsapp = async(req, res) => {
  try {
    
    const usuariosWhatsApp = await whatsapp.usuariosWhatsApp( ).then( response => response )    

    let idUsuarios = usuariosWhatsApp.map((registro) => { return registro.id_usuario })

    const puestos = await whatsapp.puestosUsuarios( idUsuarios ).then( response => response )

    const idTelefonos = usuariosWhatsApp.map(( registro ) => { return registro.whatsappservidor })

    const estatusWhatsApp = await whatsapp.estatusWhatsApp( idTelefonos ).then( response => response )

    for( const i in usuariosWhatsApp ){
      const { whatsappservidor, id_usuario } = usuariosWhatsApp[i]

      const existeWhats = estatusWhatsApp.find( el => el.whatsapp == whatsappservidor )

      const existePuesto = puestos.find( el => el.iderp == id_usuario )

      usuariosWhatsApp[i]['estatus']      = existeWhats ? 0 : 1
      usuariosWhatsApp[i]['idpuesto']     = existePuesto ? existePuesto.idpuesto : 0
      usuariosWhatsApp[i].nombre_completo = existePuesto ? usuariosWhatsApp[i].nombre_completo + ' / ' + existePuesto.puesto : usuariosWhatsApp[i].nombre_completo
    }    

    res.send(usuariosWhatsApp)

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor' })
  }
};

exports.getChatUsuario = async(req, res) => {
  try {
    
    const { whatsappservidor } = req.body

    await prospectos.habilitarGroupBy( ).then(response=> response)

    let chats  = await whatsapp.chatsWhatsApp( whatsappservidor ).then( response => response ) 

    // SACAR EL DE QUIEN FUE EL MENSAJE
    let froms = chats.map((registro) => { return registro.froms })

    let chatsSinRespuesta = await whatsapp.chatsWhatsAppSinRespuesta( froms, whatsappservidor ).then( response => response ) 

    chats = chats.concat( chatsSinRespuesta )

    froms = chats.map((registro) => { return registro.froms })

    const ultimoMensajeWhats  = await whatsapp.ultimoMensajeChats( froms, whatsappservidor ).then( response => response ) 

    for( const i in chats ){
      let { froms, tos, fecha_creacion } = chats[i]

      const existeUltimoMensaje = ultimoMensajeWhats.find( el => el.remote == froms )

      if( existeUltimoMensaje ){
        chats[i].fecha_creacion = existeUltimoMensaje.max_time
      }
    }

    // ORDENAR POR FECHA
    chats.sort((a , b)=>{
      if (a.fecha_creacion < b.fecha_creacion) {
        return 1;
      }
      if (a.fecha_creacion > b.fecha_creacion) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });


    // REVISAR LOS NOMBRES DE LOS USUARIOS
    const usuariosWhaFast = await  whatsapp.getUsuariosWhaEscuela( 2 ).then( response => response )
    const usuariosWhaInbi = await  whatsapp.getUsuariosWhaEscuela( 1 ).then( response => response )

    for( const i in chats ){
      let { notifyName, froms, tos } = chats[i]

      // Validar si existe el numero en fast primero
      const existeFast = usuariosWhaFast.find( el => froms.match( el.whatsapp ) || tos.match( el.whatsapp ) )
      const existeInbi = usuariosWhaInbi.find( el => froms.match( el.whatsapp ) || tos.match( el.whatsapp ) )

      if( existeFast ){ chats[i].notifyName = existeFast.alumno }
      if( existeInbi ){ chats[i].notifyName = existeInbi.alumno }
    }

    res.send( chats )

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor' })
  }
};

exports.getMensajesWhatsApp = async(req, res) => {
  try {
    
    const { tos, froms, whatsappservidor } = req.body

    await prospectos.habilitarGroupBy( ).then(response=> response)

    const mensajes         = await whatsapp.getMensajesWhatsApp( tos, froms, whatsappservidor ).then( response => response )    

    res.send( mensajes )

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor' })
  }
};

exports.generarReciboWhatsApp = async(req, res) => {
  try {
    
    const { tos, mensaje, imagen, url, from } = req.body

    // CONSTRUIR EL NOMBRE DEL ARCHIVO
    let outputFilename = uuidv4() + '.pdf'

    // GENERAR EL ARCHIVO PDF ( DESCARGANDO... )
    let pdfBuffer = await request.get({uri: url, encoding: null});

    // GUARDANDO EL ARCHIVO EN EL SERVIDOR
    let archivo = fs.writeFileSync('./../../whatsapp-imagenes/' + outputFilename, pdfBuffer);

    // ENVIAR EL MENSAJE CON TODO Y ARCHIVO
    const enviarRecioWha = await configWhatsApp.enviarMensajeInbi( tos, mensaje, imagen, outputFilename, from ).then( response => response )

    res.send({ message: 'pjpok' })

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor' })
  }
};

exports.enviarReciboErp = async(req, res) => {
  try {
    
    const { id_usuario, id_pago } = req.body

    // consultar el telefono de la vendedora
    const datosVendedora  = await whatsapp.datosWhatsVendedora( id_usuario ).then( response => response )

    // CONSULTAR PAGO 
    const datosReciboPago = await whatsapp.datosReciboPago( id_pago ).then( response => response )
    console.log( id_pago )

    if( !datosReciboPago ){ return res.status( 400 ).send({ message: 'El pago no existe, favor de validarlo con sistemas' })}

    // Consultar telefono del alumno
    const datosAlumno     = await whatsapp.datosWhatsAlumno( datosReciboPago.id_alumno ).then( response => response )

    res.send({ datosVendedora, datosAlumno, datosReciboPago })

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor' })
  }
};

exports.getContactosWhats = async(req, res) => {
  try {

    // Consultar telefono del alumno
    const datosAlumno     = await configWhatsApp.getContactosWhats( ).then( response => response )

    res.send(datosAlumno)

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor' })
  }
};

exports.verEstatusWhatsApp = async(req, res) => {
  try {
    
    const { from } = req.body

    // Consultar telefono del alumno
    const datosAlumno     = await configWhatsApp.getEstatusWhatsApp( from ).then( response => response )

    res.send({ datosAlumno })

  } catch (error) {
    res.status(500).send( error )
  }
};

exports.crearQR = async(req, res) => {
  try {
    
    const { from } = req.body

    // Consultar telefono del alumno
    const data     = await configWhatsApp.generaQR( from, true ).then( response => response )

    return res.send( data )

  } catch (error) {
    return res.status(500).send( error )
  }
};

exports.destroySesion = async(req, res) => {
  try {
    
    const { from } = req.body

    // Consultar telefono del alumno
    const data     = await configWhatsApp.destroySesion( from ).then( response => response )

    return res.send( data )

  } catch (error) {
    return res.status(500).send( error )
  }
};


exports.updateWhatsApp = async(req, res) => {
  try {
    
    const { escuela, id_alumno, iderp, nuevoTelefono } = req.body

    // Consultar telefono del alumno
    const updateWhaLMS     = await whatsapp.updateWhaLMS( escuela, id_alumno, nuevoTelefono ).then( response => response )
    const updateWhaERP     = await whatsapp.updateWhaERP( iderp, nuevoTelefono ).then( response => response )

    return res.send({ message: 'Datos actualizados correctamente' })

  } catch (error) {
    return res.status(500).send({message : error ? error.message : 'Error en el servidor '})
  }
};


exports.buscarWhaProspecto = async(req, res) => {
  try {
    
    const { prospecto } = req.body

    const existeFolio = await whatsapp.getFolioProspecto( prospecto ).then( response => response )

    if( !existeFolio ){ return res.status( 400 ).send({ message: 'El dato que está buscando no existe' })}

    const { telefono } = existeFolio
    // Buscar los mensajes para mostrar
    const mensajesWha = await whatsapp.getMensajesWhaProspecto( telefono ).then( response => response )

    if( !mensajesWha.length ){ return res.status( 400 ).send({ message: 'Sin mensajes' })}

    const vendedoras   = await whatsapp.usuariosWhatsApp( ).then( response => response ) 

    for( const i in mensajesWha ){
      const { mio, tos, froms } = mensajesWha[i]

      const existeVendedora1 = vendedoras.find( el => el.whatsappservidor == tos )
      const existeVendedora2 = vendedoras.find( el => el.whatsappservidor == froms )

      if( existeVendedora1 ){
        mensajesWha[i].url_servidor = existeVendedora1.url_servidor
        mensajesWha[i].vendedora    = existeVendedora1.nombre_completo
      }else if(existeVendedora2){
        mensajesWha[i].url_servidor = existeVendedora2.url_servidor
        mensajesWha[i].vendedora    = existeVendedora2.nombre_completo
      }else{
        mensajesWha[i].url_servidor = ''
        mensajesWha[i].vendedora    = ''
      }
    }

    return res.send(mensajesWha)

  } catch (error) {
    return res.status(500).send({message : error ? error.message : 'Error en el servidor '})
  }
};


exports.buscarWhatsAppTelefono = async(req, res) => {
  try {
    
    const { whaid, escuela, id_whatsapp } = req.body


    const usuariosWhatsApp = await whatsapp.usuariosWhatsApp( ).then( response => response )    

    const telefonos = usuariosWhatsApp.filter( el => { return el.escuela == escuela }).map(({ whatsappservidor }) => { return whatsappservidor })


    // Obtener si tiene alguna asignación con alguna vendedora o recepcionista:
    const getAsignacion = await whatsapp.getAsignacion( id_whatsapp ).then( response => response )    

    // Si existe alguna asignación, hay que indicarle quién es
    if( getAsignacion ){

      // buscamos si existe la vendedora
      const existeVendedora =  usuariosWhatsApp.find( el => el.id_usuario == getAsignacion.id_vendedora )

      // Si existe, la agregamos, si no, null
      getAsignacion['vendedora'] = existeVendedora ? existeVendedora.nombre_completo : null
    }
    
    // Buscar los mensajes para mostrar y que vengan de parte de esos teléfonos, osea, inbi o fast
    let mensajesWha = await whatsapp.getMensajesIdWhat( whaid, telefonos ).then( response => response )

    mensajesWha = mensajesWha.sort((a, b) => a.idmensajes_whatsapp - b.idmensajes_whatsapp)

    if( !mensajesWha.length ){ return res.status( 400 ).send({ message: 'Sin mensajes' })}

    const vendedoras   = await whatsapp.usuariosWhatsApp( ).then( response => response ) 

    for( const i in mensajesWha ){
      const { mio, tos, froms } = mensajesWha[i]

      const existeVendedora1 = vendedoras.find( el => el.whatsappservidor == tos )
      const existeVendedora2 = vendedoras.find( el => el.whatsappservidor == froms )

      if( existeVendedora1 ){
        mensajesWha[i].url_servidor = existeVendedora1.url_servidor
        mensajesWha[i].vendedora    = existeVendedora1.nombre_completo
      }else if(existeVendedora2){
        mensajesWha[i].url_servidor = existeVendedora2.url_servidor
        mensajesWha[i].vendedora    = existeVendedora2.nombre_completo
      }else{
        mensajesWha[i].url_servidor = ''
        mensajesWha[i].vendedora    = ''
      }
    }

    return res.send({ mensajesWha, getAsignacion })

  } catch (error) {
    return res.status(500).send({message : error ? error.message : 'Error en el servidor '})
  }
};


exports.asignarVendedoraWhats = async(req, res) => {
  try {
    
    // DESESTRUCTURAMOS EL BODY
    const { id_vendedora, id_whatsapp, estatus } = req.body

    // Validamos si whatsapp ya tienen asiganda una vendedora
    const existeRegistro = await whatsapp.existeRegistro( id_whatsapp ).then( response => response )

    // Si ya hay, actualizamos
    if( existeRegistro ){

      // Actualizamos el registro
      const updateRegistro = await whatsapp.updateRegistro( id_vendedora, id_whatsapp, estatus, existeRegistro.idacceso_whats ).then( response => response )

    }else{
      // Si no hay, agregamos
      const addRegistro    = await whatsapp.addRegistro( id_vendedora, id_whatsapp, estatus ).then( response => response )

    }
   
    return res.send({ message: 'Dato registrado correctamente' })

  } catch (error) {
    return res.status(500).send({message : error ? error.message : 'Error en el servidor '})
  }
};


exports.getWhatsAppAsignados = async(req, res) => {
  try {
    
    // DESESTRUCTURAMOS EL BODY
    const { id } = req.params

    // Obtner los whatsapp asignados de la vendedora
    const whatsAppAsignados = await whatsapp.getWhatsAppAsignados( id ).then( response => response )

    // Retornar todos los whatsapp asignados a la vendedora
    return res.send( whatsAppAsignados )

  } catch (error) {
    return res.status(500).send({message : error ? error.message : 'Error en el servidor '})
  }
};