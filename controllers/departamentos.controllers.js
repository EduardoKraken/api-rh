const departamentos = require("../models/departamentos.model.js");
const tickets       = require("../models/tickets/ticket.models.js");


exports.getDepartamentos = (req, res) => {
  departamentos.getDepartamentos((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};


exports.addDepartamento = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  departamentos.addDepartamento(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send({ message: `El departamento se ha creado correctamente` });
  })
};



exports.updateDepartamento = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }

  departamentos.updateDepartamento(req.params.iddepartamento, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro el departamento con id : ${req.params.iddepartamento}` });
      } else {
        res.status(500).send({ message: "Error al actualizar el departamento con el id : " + req.params.iddepartamento });
      }
    } else {
      res.status(200).send({ message: `El departamento se ha actualizado correctamente.` })
    }
  })
}


exports.listDepartamentos = async (req, res) => {

  try{
    // Consultar todos los tickets
    let listDepartamentos = await departamentos.listDepartamentos( ).then( response => response )
    let listPuestos       = await departamentos.listPuestos( ).then( response => response )

    const getJefesArea    = await tickets.getJefesArea( ).then( response => response )

    const idJefes = getJefesArea.map((registro)=>{ return registro.idpuesto })

    for( const i in listDepartamentos ){
      const { iddepartamento } = listDepartamentos[i]

      let puestos = listPuestos.filter( el=> { return el.iddepartamento == iddepartamento && idJefes.includes( el.idpuesto ) })

      for( const j in puestos ){
        const { idpuesto } = puestos[j]

        puestos[j]['jefes'] = getJefesArea.filter( el=> { return el.idpuesto == idpuesto })
      }

      listDepartamentos[i]['puestos'] = puestos

    }

    // Falta consultar las areas, los jefes de area y consultar los auxiliares
    res.send(listDepartamentos);
  }catch ( error ) {
    res.status(500).send({ message: error.message || "Se produjo algún error al recuperar los Tickets" });
  }
};