const revisionexamenes = require("../../models/academico/revisionexamenes.model.js");
const kpi              = require("../../models/kpi/kpi.model.js");

exports.getExamenesRevision = async (req, res,) => {
  try {

  	// Obtenemos la escuela donde se inscribiran los alumnos
  	const { id } = req.params 

  	// Escuela de inbi
  	if( id == 1 ){
  		
			let examen_pagina      = await revisionexamenes.examenPaginaInbi( ).then( response => response )
	    let examen_interno     = await revisionexamenes.examenInternoInbi( ).then( response => response )
	    let examen_teens       = await revisionexamenes.examenTeensInbi( ).then( response => response )
	    let examen_reubicacion = await revisionexamenes.examenReubicacionInbi( ).then( response => response )

	    let idProspectos = examen_interno.map((registro)=>{ return registro.idprospectos })

	    const prospectos = await revisionexamenes.getFolioProspectos( idProspectos ).then( response => response )

	    for( const i in examen_interno ){
	    	const { idprospectos } = examen_interno[i]

	    	const existeFolio = prospectos.find( el => el.idprospectos == idprospectos )

	    	examen_interno[i].folio = existeFolio ? existeFolio.folio : ''
	    }

	    let respuestaFinal     = examen_pagina.concat( examen_interno ).concat( examen_teens ).concat( examen_reubicacion )

    	for( const i in respuestaFinal ){

    		const { nivel } = respuestaFinal[i]

    		respuestaFinal[i]['clase_row'] = nivel == null ? 'falta_examen' : ''

    	}

			respuestaFinal = respuestaFinal.sort((a, b) => {
			  if (a.nivel === null) {
			    return -1; // a viene antes que b
			  } else if (b.nivel === null) {
			    return 1; // b viene antes que a
			  } else {
			    return 0; // no se cambia el orden
			  }
			});

    	return res.send( respuestaFinal )
  	
  	}else{

  		// Escuela de fast
  		let examen_pagina      = await revisionexamenes.examenPaginaFast( ).then( response => response )
	    let examen_interno     = await revisionexamenes.examenInternoFast( ).then( response => response )
	    let examen_reubicacion = await revisionexamenes.examenReubicacionFast( ).then( response => response )

	    let idProspectos = examen_interno.map((registro)=>{ return registro.idprospectos })

	    const prospectos = await revisionexamenes.getFolioProspectos( idProspectos ).then( response => response )

	    for( const i in examen_interno ){
	    	const { idprospectos } = examen_interno[i]

	    	const existeFolio = prospectos.find( el => el.idprospectos == idprospectos )

	    	examen_interno[i].folio = existeFolio ? existeFolio.folio : ''
	    }

	    let respuestaFinal     = examen_pagina.concat( examen_interno ).concat( examen_reubicacion )

    	for( const i in respuestaFinal ){

    		const { nivel } = respuestaFinal[i]

    		respuestaFinal[i]['clase_row'] = nivel == null ? 'falta_examen' : ''

    	}

			respuestaFinal = respuestaFinal.sort((a, b) => {
			  if (a.nivel === null) {
			    return -1; // a viene antes que b
			  } else if (b.nivel === null) {
			    return 1; // b viene antes que a
			  } else {
			    return 0; // no se cambia el orden
			  }
			});

    	return res.send( respuestaFinal )
  		
  	}


  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};


exports.getNivelesEvaluacion = async (req, res,) => {
  try {

		let niveles      = await revisionexamenes.getNivelesEvaluacion( ).then( response => response )
  	
  	return res.send( niveles )
  	
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};


exports.getOpcionRespuestasMalas = async (req, res,) => {
  try {

		let oprespuestas      = await revisionexamenes.getOpcionRespuestasMalas( ).then( response => response )
  	
  	return res.send( oprespuestas )
  	
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};

exports.asignarNivel = async (req, res,) => {
  try {

		let niveles      = await revisionexamenes.asignarNivel( req.body.escuela, req.body ).then( response => response )

		// Ver si existen respuestas
		const existeExamenUbicacion = await revisionexamenes.existeExamenUbicacion( req.body.escuela, req.body ).then( response => response )

		// console.log( existeExamenUbicacion )

		// Actualizar
		if( existeExamenUbicacion ){

			const updateRespuestasUbicacion = await revisionexamenes.updateRespuestasUbicacion( req.body.escuela, req.body ).then( response => response )

		}else{

			// Agregar
			const agregarRespuestasUbicacion = await revisionexamenes.agregarRespuestasUbicacion( req.body.escuela, req.body ).then( response => response )

		}
  	
  	return res.send( niveles )
  	
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};

exports.eliminarExamenUbicacion = async (req, res,) => {
  try {

		const eliminarExamenUbicacion = await revisionexamenes.eliminarExamenUbicacion( req.body.escuela, req.body ).then( response => response )

  	
  	return res.send( eliminarExamenUbicacion )
  	
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};