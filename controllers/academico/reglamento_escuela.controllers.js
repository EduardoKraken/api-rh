const { response } = require("express");
const { rest } = require("lodash");
const reglamento_escuela = require("../../models/academico/reglamento_escuela.model.js");


exports.getRegla = async (req, res,) => {
  try {
    const reglamento = await reglamento_escuela.getReglas( ).then(response => response) 
    const respuesta = reglamento;
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};


exports.addRegla = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
      res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }
  
    reglamento_escuela.addReglas(req.body, (err, data) => {
      if (err)
        res.status(400).send({
          message: err.message || "Se produjo algún error al crear el curso"
        })
      else res.status(200).send({ message: `el curso se creo correctamente` });
    })
  };


  exports.updateRegla = (req, res) => {
    if (!req.body || Object.keys(req.body).length === 0) {
      res.status(400).send({ message: "El contenido no puede estar vacío" });
    }
    reglamento_escuela.updateReglas(req.params.id, req.body, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(400).send({ message: `No se encontro la regla con id : ${req.params.id}, ${ err }` });
        } else {
          res.status(400).send({ message: `Error al actualizar la regla con el id : ${req.params.id}, ${ err } `});
        }
      } else {
        res.status(200).send({ message: `el curso se ha actualizado correctamente.` })
      }
    })
  }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//ANGEL RODRIGUEZ -- TODO