const dashboardKpi               = require("../../models/kpi/dashboardKpi.model.js");
const Kpi                        = require("../../models/kpi/kpi.model.js");
const personal                   = require("../../models/kpi/dashboardPersonal.model.js");
const entradas                   = require("../../models/catalogos/entradas.model.js");
const reporte_modelo_ventas      = require("../../models/academico/reporte_modelo_ventas.model.js");
const dashboardMarketing         = require("../../models/prospectos/dashboardMarketing.model.js");
const erpviejo                   = require("../../models/prospectos/erpviejo.model.js");
const generateReporteVentasSemanal    = require('../../helpers/reporteVentasSemanales');

/////////////////////////////////////////////////////////VENTAS DE SEMANA A SEMANA//////////////////////////////////////////////////////////////////////////

exports.reporteSemanalVentas = async (req, res) => {
    try{
  
      const fechaEspaniol    = await dashboardKpi.fechaEspaniolsqlERP( ).then( response => response )
      
      // 1-. Sacar la fecha
      const { fechaini, escuela } = req.body
  
      // 2-. Obtener las fechas obtenidas de la semana
      const fechas = await Kpi.getFechasReporteSemanal( fechaini ).then( response => response )
      const { fecha_inicio, fecha_final } = fechas

      const fechasFormatos   = await Kpi.fechasFormatos( fecha_inicio, fecha_final ).then( response => response )
      const { fecha_inicio_format, fecha_final_format } = fechasFormatos
  
      // 3-. Obtener las fecahs de la semana anterios a la elegida
      const fechasSemanaAnterior   = await Kpi.getFechaAnterior( fecha_inicio ).then( response => response )
      const fechasFormatosAnterior = await Kpi.fechasFormatos( fechasSemanaAnterior.fecha_inicio, fechasSemanaAnterior.fecha_final ).then( response => response )

      const fechasSemanaAnterior1   = await Kpi.getFechaAnterior( fechasSemanaAnterior.fecha_inicio ).then( response => response )
      const fechasFormatosAnterior1 = await Kpi.fechasFormatos( fechasSemanaAnterior1.fecha_inicio, fechasSemanaAnterior1.fecha_final ).then( response => response )

      const fechasSemanaAnterior2   = await Kpi.getFechaAnterior( fechasSemanaAnterior1.fecha_inicio ).then( response => response )
      const fechasFormatosAnterior2 = await Kpi.fechasFormatos( fechasSemanaAnterior2.fecha_inicio, fechasSemanaAnterior2.fecha_final ).then( response => response )

      const fechasSemanaAnterior3   = await Kpi.getFechaAnterior( fechasSemanaAnterior2.fecha_inicio ).then( response => response )
      const fechasFormatosAnterior3 = await Kpi.fechasFormatos( fechasSemanaAnterior3.fecha_inicio, fechasSemanaAnterior3.fecha_final ).then( response => response )

      const fechasSemanaAnterior4   = await Kpi.getFechaAnterior( fechasSemanaAnterior3.fecha_inicio ).then( response => response )
      const fechasFormatosAnterior4 = await Kpi.fechasFormatos( fechasSemanaAnterior4.fecha_inicio, fechasSemanaAnterior4.fecha_final ).then( response => response )

      // Sacaremos 2 datos, 
      // Vendedoras
      let vendedorasActuales = await personal.getUsuariosVentas( ).then(response => response)
  
      let iderps = vendedorasActuales.map(( registro ) => { return registro.iderp })
  
      // Agregarles el nombre
      const usuariosSistema = await entradas.getUsuariosERP( iderps ).then( response => response )
      
      // Recorremos a la vendedora para "Rehacer" el arreglo de objetos
      for(const i in vendedorasActuales){

        // Sacamos los datos necesarios como el idero y el nombre completo
        const { iderp, nombre_completo } = vendedorasActuales[i]
  
        
        // Buscar el usuario en english_admin con el idero
        const nombre    = usuariosSistema.find(el=> el.id_usuario == iderp)
  
        // Agregar los usuarios y nombres
        vendedorasActuales[i] = {
          id_usuario      : iderp, // agregamos el ider
          nombre_completo : nombre  ? nombre.nombre_completo : '', // el nombre completo
          escuela         : nombre ? nombre.escuela : '0' // y... la escuela a la que perteneces la vendedora
        }
      }
      
      // Vendeodras actuales... esto quiere decir que no esté inactivo el usuario ni que sea vancante, necesitamos solo a vendedoras activas y oficiales
      vendedorasActuales = vendedorasActuales.filter( el => { return el.escuela == escuela && !el.nombre_completo.match("VACANTE")})
  
      // Planteles
      let plantelesActivos = await entradas.getPlantelesApertura( ).then( response => response) 

      // Filtramos solos los que utilizamos que pertenecen a la escuela que queremos
      plantelesActivos = plantelesActivos.filter( el => { return el.idunidad_negocio == escuela })
  
      // Sacamos todas las matriculas nuevas entre esas fechas
      let nuevasMatriculas = await Kpi.getNuevasMatriculas( fecha_inicio, fecha_final ).then( response => response)

      // Filtramos por escuela
      nuevasMatriculas = nuevasMatriculas.filter( el => { return el.unidad_negocio == escuela })
  
      // Sacamos todas las matriculas nuevas entre esas fechas
      let nuevasMatriculasSemanaAnterior  = await Kpi.getNuevasMatriculas( fechasSemanaAnterior.fecha_inicio, fechasSemanaAnterior.fecha_final ).then( response => response) 
      let nuevasMatriculasSemanaAnterior1 = await Kpi.getNuevasMatriculas( fechasSemanaAnterior1.fecha_inicio, fechasSemanaAnterior1.fecha_final ).then( response => response) 
      let nuevasMatriculasSemanaAnterior2 = await Kpi.getNuevasMatriculas( fechasSemanaAnterior2.fecha_inicio, fechasSemanaAnterior2.fecha_final ).then( response => response) 
      let nuevasMatriculasSemanaAnterior3 = await Kpi.getNuevasMatriculas( fechasSemanaAnterior3.fecha_inicio, fechasSemanaAnterior3.fecha_final ).then( response => response) 
      let nuevasMatriculasSemanaAnterior4 = await Kpi.getNuevasMatriculas( fechasSemanaAnterior4.fecha_inicio, fechasSemanaAnterior4.fecha_final ).then( response => response) 
      
      // Filtramos por escuela
      nuevasMatriculasSemanaAnterior  = nuevasMatriculasSemanaAnterior.filter( el => { return el.unidad_negocio == escuela })
      nuevasMatriculasSemanaAnterior1 = nuevasMatriculasSemanaAnterior1.filter( el => { return el.unidad_negocio == escuela })
      nuevasMatriculasSemanaAnterior2 = nuevasMatriculasSemanaAnterior2.filter( el => { return el.unidad_negocio == escuela })
      nuevasMatriculasSemanaAnterior3 = nuevasMatriculasSemanaAnterior3.filter( el => { return el.unidad_negocio == escuela })
      nuevasMatriculasSemanaAnterior4 = nuevasMatriculasSemanaAnterior4.filter( el => { return el.unidad_negocio == escuela })
  
      // matriculas anteriores
      let nuevasMatriculasAnteriores = await Kpi.getNuevasMatriculasAnteriores( fecha_inicio, fecha_final ).then( response => response) 
      // FIltamos por escuela
      nuevasMatriculasAnteriores = nuevasMatriculasAnteriores.filter( el => { return el.unidad_negocio == escuela && el.cant_grupos == 0 })
  
      // Empezamos con el registro
      const encabezados = {
        matriculas_nuevas     : nuevasMatriculas.length,
        matriculas_liquidadas : nuevasMatriculas.filter( el => { return el.adeudo <= 0 }).length,
        matriculas_parciales  : nuevasMatriculas.filter( el => { return el.adeudo > 0 }).length,
  
        matriculas_liquidadas_ant       : nuevasMatriculasAnteriores.filter( el => { return el.adeudo <= 0 }).length,
        matriculas_liquidadas_ant_monto : nuevasMatriculasAnteriores.filter( el => { return el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
  
        matriculas_nuevas_monto     : nuevasMatriculas.map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
        matriculas_liquidadas_monto : nuevasMatriculas.filter( el => { return el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
        matriculas_parciales_monto  : nuevasMatriculas.filter( el => { return el.adeudo > 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
        nuevasMatriculas,
  
        fecha_inicio_format,
        fecha_final_format,
  
        fecha_inicio_formatAnterior       : fechasFormatosAnterior.fecha_inicio_format,
        fecha_final_formatAnterior        : fechasFormatosAnterior.fecha_final_format,
        matriculas_nuevasSemAnt           : nuevasMatriculasSemanaAnterior.length,
        matriculas_liquidadasSemAnt       : nuevasMatriculasSemanaAnterior.filter( el => { return el.adeudo <= 0 }).length,
        matriculas_parcialesSemAnt        : nuevasMatriculasSemanaAnterior.filter( el => { return el.adeudo > 0 }).length,
        matriculas_nuevas_montoSemAnt     : nuevasMatriculasSemanaAnterior.map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
        matriculas_liquidadas_montoSemAnt : nuevasMatriculasSemanaAnterior.filter( el => { return el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
        matriculas_parciales_montoSemAnt  : nuevasMatriculasSemanaAnterior.filter( el => { return el.adeudo > 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
      }

      const infoVentaGeneral = {
        
   
        fecha_inicio_format,
        fecha_final_format,
  
        fecha_inicio_formatAnterior       : fechasFormatosAnterior.fecha_inicio_format,
        fecha_final_formatAnterior        : fechasFormatosAnterior.fecha_final_format,

        fecha_inicio_formatAnterior1      : fechasFormatosAnterior1.fecha_inicio_format,
        fecha_final_formatAnterior1        :fechasFormatosAnterior1.fecha_final_format,

        fecha_inicio_formatAnterior2       : fechasFormatosAnterior2.fecha_inicio_format,
        fecha_final_formatAnterior2        : fechasFormatosAnterior2.fecha_final_format,

        fecha_inicio_formatAnterior3       : fechasFormatosAnterior3.fecha_inicio_format,
        fecha_final_formatAnterior3        : fechasFormatosAnterior3.fecha_final_format,

        fecha_inicio_formatAnterior4       : fechasFormatosAnterior4.fecha_inicio_format,
        fecha_final_formatAnterior4        : fechasFormatosAnterior4.fecha_final_format,
        
        matriculas_liquidadas : nuevasMatriculas.filter( el => { return el.adeudo <= 0 }).length,
        matriculas_liquidadasSemAnt       : nuevasMatriculasSemanaAnterior.filter( el => { return el.adeudo <= 0 }).length,
        matriculas_liquidadasSemAnt1       : nuevasMatriculasSemanaAnterior1.filter( el => { return el.adeudo <= 0 }).length,
        matriculas_liquidadasSemAnt2       : nuevasMatriculasSemanaAnterior2.filter( el => { return el.adeudo <= 0 }).length,
        matriculas_liquidadasSemAnt3       : nuevasMatriculasSemanaAnterior3.filter( el => { return el.adeudo <= 0 }).length,
        matriculas_liquidadasSemAnt4       : nuevasMatriculasSemanaAnterior4.filter( el => { return el.adeudo <= 0 }).length,


      }

  
      for( const i in plantelesActivos ){
  
        // Sacar el id del plantel o el plantel
        const { id_plantel, plantel } =  plantelesActivos[i]
  
        // y ahora si, sacamos el desglose de matriculas
        plantelesActivos[i]['matriculas_nuevas']     = nuevasMatriculas.filter( el => { return el.plantel == plantel }).length
        plantelesActivos[i]['matriculas_liquidadas'] = nuevasMatriculas.filter( el => { return el.plantel == plantel && el.adeudo <= 0 }).length
        plantelesActivos[i]['matriculas_parciales']  = nuevasMatriculas.filter( el => { return el.plantel == plantel && el.adeudo > 0 }).length
        plantelesActivos[i]['nuevasMatriculas']      = nuevasMatriculas.filter( el => { return el.plantel == plantel })
        plantelesActivos[i]['matriculas_ant_liqu']   = nuevasMatriculasAnteriores.filter( el => { return el.plantel == plantel && el.adeudo <= 0 }).length
  
        plantelesActivos[i]['matriculas_nuevas_monto']     = nuevasMatriculas.filter( el => { return el.plantel == plantel }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
        plantelesActivos[i]['matriculas_liquidadas_monto'] = nuevasMatriculas.filter( el => { return el.plantel == plantel && el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
        plantelesActivos[i]['matriculas_parciales_monto']  = nuevasMatriculas.filter( el => { return el.plantel == plantel && el.adeudo > 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
        plantelesActivos[i]['matriculas_ant_liqu_monto']   = nuevasMatriculasAnteriores.filter( el => { return el.plantel == plantel && el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)

        //Cambios Angel Rdz
        plantelesActivos[i]['matriculas_liquidadasSemAnt'] = nuevasMatriculasSemanaAnterior.filter( el => { return el.plantel == plantel && el.adeudo <= 0 }).length

      }
  
      // AGREGAR LOS TOTALES FINALES
      plantelesActivos.push({
        plantel: 'TOTAL',
        matriculas_nuevas     : plantelesActivos.map(item => item.matriculas_nuevas).reduce((prev, curr) => prev + curr, 0),
        matriculas_liquidadas : plantelesActivos.map(item => item.matriculas_liquidadas).reduce((prev, curr) => prev + curr, 0),
        matriculas_parciales  : plantelesActivos.map(item => item.matriculas_parciales).reduce((prev, curr) => prev + curr, 0),
        matriculas_ant_liqu   : plantelesActivos.map(item => item.matriculas_ant_liqu).reduce((prev, curr) => prev + curr, 0),
  
        matriculas_nuevas_monto     : plantelesActivos.map(item => item.matriculas_nuevas_monto).reduce((prev, curr) => prev + curr, 0),
        matriculas_liquidadas_monto : plantelesActivos.map(item => item.matriculas_liquidadas_monto).reduce((prev, curr) => prev + curr, 0),
        matriculas_parciales_monto  : plantelesActivos.map(item => item.matriculas_parciales_monto).reduce((prev, curr) => prev + curr, 0),
        matriculas_ant_liqu_monto   : plantelesActivos.map(item => item.matriculas_ant_liqu_monto).reduce((prev, curr) => prev + curr, 0),
        
        //Cambios Angel Rdz
        matriculas_liquidadasSemAnt : plantelesActivos.map(item => item.matriculas_liquidadasSemAnt).reduce((prev, curr) => prev + curr, 0),

        nuevasMatriculas: []
      })
  
  
      // Ahora siguen las vendedoras
      for( const i in vendedorasActuales ){
  
        // Sacar el id del plantel o el plantel
        const { id_usuario } =  vendedorasActuales[i]
  
        // y ahora si, sacamos el desglose de matriculas
        vendedorasActuales[i]['matriculas_nuevas']     = nuevasMatriculas.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario }).length
        vendedorasActuales[i]['matriculas_liquidadas'] = nuevasMatriculas.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario && el.adeudo <= 0 }).length
        vendedorasActuales[i]['matriculas_parciales']  = nuevasMatriculas.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario && el.adeudo > 0 }).length
        vendedorasActuales[i]['nuevasMatriculas']      = nuevasMatriculas.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario })
        vendedorasActuales[i]['matriculas_ant_liqu']   = nuevasMatriculasAnteriores.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario && el.adeudo <= 0 }).length
  
        vendedorasActuales[i]['matriculas_nuevas_monto']     = nuevasMatriculas.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
        vendedorasActuales[i]['matriculas_liquidadas_monto'] = nuevasMatriculas.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario && el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
        vendedorasActuales[i]['matriculas_parciales_monto']  = nuevasMatriculas.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario && el.adeudo > 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
        vendedorasActuales[i]['matriculas_ant_liqu_monto']   = nuevasMatriculasAnteriores.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario && el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
        
        //Cambios Angel Rdz
        vendedorasActuales[i]['matriculas_liquidadasSemAnt'] = nuevasMatriculasSemanaAnterior.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario && el.adeudo <= 0 }).length
      }
  
   
  
      // VENDEDORA INACTIVA
      vendedorasActuales.push({
        id_usuario: 0,
        nombre_completo: "INACTIVOS",
        matriculas_nuevas     : nuevasMatriculas.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) }).length,
        matriculas_liquidadas : nuevasMatriculas.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) && el.adeudo <= 0 }).length,
        matriculas_parciales  : nuevasMatriculas.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) && el.adeudo > 0 }).length,
        nuevasMatriculas      : nuevasMatriculas.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) }),
        matriculas_ant_liqu   : nuevasMatriculasAnteriores.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) && el.adeudo <= 0 }).length,
  
        matriculas_nuevas_monto     : nuevasMatriculas.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
        matriculas_liquidadas_monto : nuevasMatriculas.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) && el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
        matriculas_parciales_monto  : nuevasMatriculas.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) && el.adeudo > 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
        matriculas_ant_liqu_monto   : nuevasMatriculasAnteriores.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) && el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),

         //Cambios Angel Rdz
         matriculas_liquidadasSemAnt : nuevasMatriculasSemanaAnterior.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) && el.adeudo <= 0 }).length,
      })
  
      // AGREGAR LOS TOTALES FINALES
      vendedorasActuales.push({
        nombre_completo       : 'TOTAL',
        matriculas_nuevas     : vendedorasActuales.map(item => item.matriculas_nuevas).reduce((prev, curr) => prev + curr, 0),
        matriculas_liquidadas : vendedorasActuales.map(item => item.matriculas_liquidadas).reduce((prev, curr) => prev + curr, 0),
        matriculas_parciales  : vendedorasActuales.map(item => item.matriculas_parciales).reduce((prev, curr) => prev + curr, 0),
        matriculas_ant_liqu   : vendedorasActuales.map(item => item.matriculas_ant_liqu).reduce((prev, curr) => prev + curr, 0),
  
        matriculas_nuevas_monto     : vendedorasActuales.map(item => item.matriculas_nuevas_monto).reduce((prev, curr) => prev + curr, 0),
        matriculas_liquidadas_monto : vendedorasActuales.map(item => item.matriculas_liquidadas_monto).reduce((prev, curr) => prev + curr, 0),
        matriculas_parciales_monto  : vendedorasActuales.map(item => item.matriculas_parciales_monto).reduce((prev, curr) => prev + curr, 0),
        matriculas_ant_liqu_monto   : vendedorasActuales.map(item => item.matriculas_ant_liqu_monto).reduce((prev, curr) => prev + curr, 0),

        //Cambios Angel Rdz
        matriculas_liquidadasSemAnt : vendedorasActuales.map(item => item.matriculas_liquidadasSemAnt).reduce((prev, curr) => prev + curr, 0),

        nuevasMatriculas: []
      })
  
  
      res.send({ message: 'Ok', fechaini, fecha_inicio, fecha_final, vendedorasActuales, plantelesActivos, nuevasMatriculas, encabezados, infoVentaGeneral });
  
    } catch (error) {
      res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
    }
  };


  exports.graficasContacto = async(req, res) => {
    try {
      const { fecha_inicio, fecha_final } = req.body


      //Cambios Angel
      /////////////////////////////////////////////////////////////////////////////////////////////////////////
      //Fechas de la semana actual
      const fechas = await reporte_modelo_ventas.getFechasReporteSemanal( fecha_inicio ).then( response => response )
      const { fecha_inicios, fecha_finals } = fechas

      //Fechas de la semana anterior a la actual
      const fechasSemanaAnterior   = await reporte_modelo_ventas.getFechaAnterior( fecha_inicios ).then( response => response )
      const { fecha_inicioss, fecha_finalss } = fechasSemanaAnterior


      const numeroContactosSemanaActual = await reporte_modelo_ventas.cantidadContactosSemanaActual( fecha_inicios, fecha_finals ).then(response=> response)
      const numeroContactosSemanaAnterior = await reporte_modelo_ventas.cantidadContactosSemanaActual( fecha_inicioss, fecha_finalss ).then(response=> response)

      const numeroContactosSemanaActualINBI = await reporte_modelo_ventas.cantidadContactosSemanaActualINBI( fecha_inicios, fecha_finals ).then(response=> response)
      const numeroContactosSemanaAnteriorINBI = await reporte_modelo_ventas.cantidadContactosSemanaActualINBI( fecha_inicioss, fecha_finalss ).then(response=> response)

      const numeroContactosSemanaActualFAST = await reporte_modelo_ventas.cantidadContactosSemanaActualFAST( fecha_inicios, fecha_finals ).then(response=> response)
      const numeroContactosSemanaAnteriorFAST = await reporte_modelo_ventas.cantidadContactosSemanaActualFAST( fecha_inicioss, fecha_finalss ).then(response=> response)


      ///////////////////////////////////////////////////////////////////////////////////////////////////////////
      
      // Obtener todos los LEADS
     //Modificada ANGEL
      const graficaContactos = await dashboardMarketing.graficaContactos( fecha_inicioss, fecha_finals ).then(response=> response)
      const getFechasUnicas  = await dashboardMarketing.getFechasUnicas( fecha_inicioss, fecha_finals ).then(response=> response)
  
      const graficaContactosVendedora = await dashboardMarketing.graficaContactosVendedora( fecha_inicio, fecha_final ).then(response=> response)
      let   getVendedorasUnicas       = await dashboardMarketing.getVendedorasUnicas( fecha_inicioss, fecha_finals ).then(response=> response)
  
      const idVendedoras = getVendedorasUnicas.map((registro) => { return registro.usuario_asignado })
  
      const usuariosSistema = await erpviejo.getUsuariosERP( idVendedoras ).then( response => response )

      let headerstablacontacto = []
  
      headerstablacontacto.push({text: 'Vendedora', value: 'vendedora' })
      headerstablacontacto.push({text: 'Escuela', value: 'escuela' })
  
      for( const j in getFechasUnicas ){
        const { fecha_creacion } = getFechasUnicas[j] 
        headerstablacontacto.push({text: fecha_creacion, value: `${fecha_creacion}`})
      } 
  
      let dataTabla = []
  
      for( const i in getVendedorasUnicas ){
        const { usuario_asignado } = getVendedorasUnicas[i]
  
        const existeUsuario = usuariosSistema.find( el => el.id_usuario == usuario_asignado )
        getVendedorasUnicas[i]['vendedora'] = existeUsuario ? existeUsuario.nombre_completo : 'Sin nombre'
        getVendedorasUnicas[i]['escuela'] = existeUsuario ? existeUsuario.escuela : 'Sin escuela'
  
      }
      
    let totalConteoSemanaActual = 0;
    let totalConteoSemanaAnterior = 0;

    let totalConteoSemanaActualINBI = 0;
    let totalConteoSemanaAnteriorINBI = 0;

    let totalConteoSemanaActualFAST = 0;
    let totalConteoSemanaAnteriorFAST = 0;

    for (const i in getVendedorasUnicas) {
        const { usuario_asignado, vendedora, escuela } = getVendedorasUnicas[i];
            
        const newData = { vendedora, escuela };
        
        const existeConteo = numeroContactosSemanaActual.find(el => el.usuario_asignado === usuario_asignado);
        const existeConteo2 = numeroContactosSemanaAnterior.find(el => el.usuario_asignado === usuario_asignado);

        const existeConteoINBI = numeroContactosSemanaActualINBI.find(el => el.usuario_asignado === usuario_asignado);
        const existeConteo2INBI = numeroContactosSemanaAnteriorINBI.find(el => el.usuario_asignado === usuario_asignado);

        const existeConteoFAST = numeroContactosSemanaActualFAST.find(el => el.usuario_asignado === usuario_asignado);
        const existeConteo2FAST = numeroContactosSemanaAnteriorFAST.find(el => el.usuario_asignado === usuario_asignado);

        if (existeConteo) {
            newData.conteo_semana_actual = existeConteo.conteo_semana_actual;
        } else {
            newData.conteo_semana_actual = 0;
        }

        if (existeConteo2) {
            newData.conteo_semana_anterior = existeConteo2.conteo_semana_actual;
        } else {
            newData.conteo_semana_anterior = 0;
        }
         
        ////////////////////////////////////////////////////////////////////////////////////INBI
        if (existeConteoINBI) {
          newData.conteo_semana_actualINBI = existeConteoINBI.conteo_semana_actual;
        } else {
          newData.conteo_semana_actualINBI = 0;
        }

        if (existeConteo2INBI) {
          newData.conteo_semana_anteriorINBI = existeConteo2INBI.conteo_semana_actual;
        } else {
            newData.conteo_semana_anteriorINBI = 0;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////FAST
        if (existeConteoFAST) {
          newData.conteo_semana_actualFAST = existeConteoFAST.conteo_semana_actual;
        } else {
          newData.conteo_semana_actualFAST = 0;
        }

        if (existeConteo2FAST) {
           newData.conteo_semana_anteriorFAST = existeConteo2FAST.conteo_semana_actual;
        } else {
            newData.conteo_semana_anteriorFAST = 0;
        }

        totalConteoSemanaActual += newData.conteo_semana_actual;
        totalConteoSemanaAnterior += newData.conteo_semana_anterior;

        totalConteoSemanaActualINBI += newData.conteo_semana_actualINBI;
        totalConteoSemanaAnteriorINBI += newData.conteo_semana_anteriorINBI;

        totalConteoSemanaActualFAST += newData.conteo_semana_actualFAST;
        totalConteoSemanaAnteriorFAST += newData.conteo_semana_anteriorFAST;

        newData.diferencia = newData.conteo_semana_actual - newData.conteo_semana_anterior;

        dataTabla.push(newData);
    }

    // Agregar el objeto de totales al final del array

    dataTabla.push({
      vendedora: 'Total',
      conteo_semana_actual: totalConteoSemanaActual,
      conteo_semana_anterior: totalConteoSemanaAnterior,
      diferencia: totalConteoSemanaActual - totalConteoSemanaAnterior
    });


    dataTabla.push({
      vendedora: 'Total FAST',
      conteo_semana_actual: totalConteoSemanaActualFAST,
      conteo_semana_anterior: totalConteoSemanaAnteriorFAST,
      diferencia: totalConteoSemanaActualFAST - totalConteoSemanaAnteriorFAST
    });


    dataTabla.push({
      vendedora: 'Total INBI',
      conteo_semana_actual: totalConteoSemanaActualINBI,
      conteo_semana_anterior: totalConteoSemanaAnteriorINBI,
      diferencia: totalConteoSemanaActualINBI - totalConteoSemanaAnteriorINBI
   });
  
      res.send({graficaContactos, getFechasUnicas, headerstablacontacto, dataTabla })
      
    } catch (error) {
      res.status(500).send({message:error.message})
    }
  };


  exports.reporte = async (req, res) => {
    try{

      const { infoTabla1, infoTabla2, infoTexto, infoTabla3, infoTabla4, imageData, imageData2, ventaGeneral, infoTablaEspecial, escuela } = req.body


      ////////////////////////////////////////////////////////Reporte Semanal Ventas/////////////////////////////////////////////////
      //Saca el porcentaje de subida o de bajada
      infoTabla1.forEach(obj => {
        if (obj.columna5 !== undefined && obj.columna6 !== undefined) {
          const porcentaje = ((obj.columna6 - obj.columna5) / obj.columna5) * 100;
          obj.resultado = porcentaje.toFixed(2);
        }
      });

      const tercerObjetoResultado = parseFloat(infoTabla1[2].resultado);

      if (tercerObjetoResultado > 0) {
        resultado1 = (`Las ventas subieron un ${tercerObjetoResultado.toFixed(2)}%`);
      } else if (tercerObjetoResultado < 0) {
        resultado1 = (`Las ventas bajaron un ${Math.abs(tercerObjetoResultado).toFixed(2)}%`);
      } else {
        resultado1 = ("Las ventas se mantuvieron igual");
      }


      ////////////////////////////////////////////////////////Ventas semana a semana Encargadas/////////////////////////////////////////////////


      infoTabla2.forEach(obj => {
        if (obj.matriculas_liquidadas !== undefined && obj.matriculas_liquidadasSemAnt !== undefined) {
          const porcentaje = (obj.matriculas_liquidadas- obj.matriculas_liquidadasSemAnt);
          obj.resultado = porcentaje;
        }
      });


      infoTabla2.forEach(obj => {
        const diff = obj.matriculas_liquidadas - obj.matriculas_liquidadasSemAnt;
        const porcentaje = (diff / obj.matriculas_liquidadasSemAnt) * 100;
        obj.porcentaje_resultado = porcentaje;
      });

      const infoTabla2Filtrados = infoTabla2.slice(0, -2);

      const objetoMenorResultado = infoTabla2Filtrados.reduce((minObjeto, objeto) => {
        if (objeto.resultado < minObjeto.resultado) {
          return objeto;
        }
        return minObjeto;
      }, infoTabla2Filtrados[0]);

      // Obtener el nombre_completo del objeto encontrado
      const resultado2 = objetoMenorResultado.nombre_completo;


      let resultado3 = null;
      for (const obj of infoTabla2) {
        if (obj.nombre_completo === resultado2) {
          resultado3 = obj.porcentaje_resultado;
          break;
        }
      }

      const resultadoporcentaje = parseFloat(resultado3);


      if (resultadoporcentaje > 0) {
        resultado3 = (`${resultado2} subió ${resultadoporcentaje.toFixed(2)}% sus ventas`);
      } else if (resultadoporcentaje < 0) {
        resultado3 = (`${resultado2} bajo ${resultadoporcentaje.toFixed(2)}% sus ventas`);
      } else {
        resultado3 = ("Las ventas se mantuvieron igual");
      }


      // Filtrar objetos con "matriculas_liquidadas" mayores a cero
      const objetosConValoresMayoresACero = infoTabla2.filter(obj => obj.matriculas_liquidadas > 0);

      // Ordenar el nuevo array de objetos según el valor de "matriculas_liquidadas"
      objetosConValoresMayoresACero.sort((a, b) => a.matriculas_liquidadas - b.matriculas_liquidadas);
 

      // Obtener los nombres completos de los dos valores más bajos de "matriculas_liquidadas"
        const nombre1 = objetosConValoresMayoresACero[0].nombre_completo;
        const nombre2 = objetosConValoresMayoresACero[1].nombre_completo;

      
      ////////////////////////////////////////////////////////Contactos por semana distribución de encargadas/////////////////////////////////////////////////

      
      // Agregar las columnas extras de sumas
      infoTabla3.forEach(objeto => {
        // Sumar los valores de las columnas 2 a 7
        const Semana_Anterior = Object.values(objeto).slice(1, 6 + 1).reduce((a, b) => a + b, 0);
      
        // Sumar los valores de las columnas 8 a 13
        const Semana_Siguiente = Object.values(objeto).slice(7, 12 + 1).reduce((a, b) => a + b, 0);
      
        // Agregar las sumas como propiedades en el objeto
        objeto['Semana_Anterior'] = Semana_Anterior;
        objeto['Semana_Siguiente'] = Semana_Siguiente;
        objeto['Diferencia'] = Semana_Siguiente - Semana_Anterior;
      });

      //Suma los objetos de infoTabla4
      infoTabla4.forEach(obj => {
        const keys = Object.keys(obj);
        keys.forEach(key => {
          if (Array.isArray(obj[key])) {
            obj[key] = obj[key].length; 
          }
        });
      });

      

      if (escuela == 1){
         totalItem = infoTablaEspecial.find(item => item.vendedora === 'Total INBI');
      } 
      
      if (escuela == 2){
         totalItem = infoTablaEspecial.find(item => item.vendedora === 'Total FAST');
      }
     
      const porcentajeCambio = 0
      if (totalItem) {
        const { conteo_semana_actual, conteo_semana_anterior } = totalItem;
        
        this.porcentajeCambio = ((conteo_semana_actual - conteo_semana_anterior) / conteo_semana_anterior) * 100;
      }

      
      const porcentaje_resultado2 = this.porcentajeCambio.toFixed(2)

      if(porcentaje_resultado2 > 0){
         porcentajeMensajeGrafica =  (`Los contactos subieron un ${porcentaje_resultado2}%`);  
      }else {
        porcentajeMensajeGrafica =  (`Los contactos bajaron un ${porcentaje_resultado2}%`);  
      }

      ////////////////////////////////////////////////////////Enviar Datos/////////////////////////////////////////////////
      generateReporteVentasSemanal({     
        infoTabla1,
        resultado1,
        infoTabla2,
        resultado2,
        resultado3,
        nombre1,
        nombre2,
        infoTexto,
        infoTabla3,
        infoTabla4,
        imageData,
        imageData2,
        porcentajeMensajeGrafica,
        ventaGeneral,
        infoTablaEspecial,
        escuela
      })

        res.send({ message: 'Datos generados' });
 
    } catch (error) {
      res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
    }
  };