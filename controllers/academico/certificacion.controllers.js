var   fs = require('fs');
const certificacion = require("../../models/academico/certificacion.model.js");
const Jimp = require('jimp');
const gm   = require('gm').subClass({imageMagick: true});
var request = require('request');

// Obtener el listado de codigos
exports.getCodigos = async(req, res) => {
	try {
		let { codigos } = req.body

    // Hay que obtener los codigos asignados
    const codigosFast       = await certificacion.getCodigosFast().then(response => response)
    const codigosInbi       = await certificacion.getCodigosInbi().then(response => response)

    // Ahora si, hay que ver si los codigos que se recibieron ya existen
    let codigosEncontrados = codigosFast.concat(codigosInbi)
    // Recorremos los codigos
    for(const i in codigos){
    	// Desestrucutramos el codigo para poder buscarlo más facilmente
    	let { codigo } = codigos[i]
    	// Lo buscamos para ver si ya existe
    	let codigoExiste = codigosEncontrados.find(element=> element.codigo == codigo )
    	// Si ya existe solo mostramos los datos del codigo para que vea el usuario que ya existe
    	if(codigoExiste){
    		codigos[i] = codigoExiste
    	}
    }

    res.send(codigos);
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addCodigo = async(req, res) => {
  try {
  	const payload = req.body
    // Primero sacamos a todos los grupos que están en ese ciclo y funcionando 
    const existeTeacher       = await certificacion.getTeacherGrupo(payload).then(response => response)
    // Validamos si existe
    if(existeTeacher.length > 0){
    	// Actualizamos
    	const updateTeacher     = await certificacion.updateTeacherGrupo(payload).then(response => response)
    }else{
    	// Agregamos
    	const addTeacher        = await certificacion.addTeacherGrupo(payload).then(response => response)
    }

    res.send({message:'Se actualizo el teacher'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getAlumnosCerti = async(req, res) => {
  try {
  	// Primero hay que obtener a los alumnos de fast y de inbi, donde: el grupo es certificación, esta activo y listo
    const getAlumnosCertiFast   = await certificacion.getAlumnosCertiFast().then(response => response)
    const getAlumnosCertiInbi   = await certificacion.getAlumnosCertiInbi().then(response => response)
    
    const alumnos = getAlumnosCertiFast.concat(getAlumnosCertiInbi)

    res.send(alumnos);
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.updateCodigo = async(req, res) => {
  try {
  	const { codigo, id_alumno, id_plantel, nombre } = req.body
  	 // Primero sacamos a todos los grupos que están en ese ciclo y funcionando 
  	if( id_plantel == 2){
	    const existeCodigo       = await certificacion.getCodigoFast(codigo).then(response => response)
	    // Validamos si existe
	    if(existeCodigo.length > 0){
	    	// Actualizamos
	    	const updateCodigoFast = await certificacion.updateCodigoFast(codigo, id_alumno, nombre).then(response => response)
	    }else{
	    	// Agregamos
	    	const addTeacherGrupo  = await certificacion.addCodigoFast(codigo, id_alumno, nombre).then(response => response)
	    }
  	}else{
  		const existeCodigo       = await certificacion.getCodigoInbi(codigo).then(response => response)
	    // Validamos si existe
	    if(existeCodigo.length > 0){
	    	// Actualizamos
	    	const updateCodigoInbi = await certificacion.updateCodigoInbi(codigo, id_alumno, nombre).then(response => response)
	    }else{
	    	// Agregamos
	    	const addTeacherGrupo  = await certificacion.addCodigoInbi(codigo, id_alumno, nombre).then(response => response)
	    }
  	}

    res.send({mensaje: 'Registrado correctamente'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getCertificadoAlumno = async(req, res) => {
  try {
    const { id } = req.params
    const existe       = await certificacion.getExisteSolicitudAlumno( id ).then(response => response)

    if(existe.length > 0){
      res.status(201).send(existe);
    }else{
      res.status(200).send({message:'No existe'});
    }

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getAlumnosUltimoNivel = async(req, res) => {
  try {
    // Hay que obtener los codigos asignados
    const alumnosFast       = await certificacion.getAlumnosUltimoNivel( 2 ).then(response => response)
    const alumnosInbi       = await certificacion.getAlumnosUltimoNivel( 1 ).then(response => response)

    const alumnos = alumnosFast.concat( alumnosInbi )

    res.send( alumnos );
  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

exports.addSolicitudCertificado = async(req, res) => {
  try {
    const { iderp } = req.body
    // Hay que obtener los codigos asignados
    const existe       = await certificacion.getExisteSolicitud( iderp ).then(response => response)

    if(existe.length > 0){
      res.status(201).send(existe);
    }else{
      const generarSolcitud = await certificacion.addSolicitudCertificado(req.body).then(response => response)
      res.send( generarSolcitud );
    }
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getCertificadosSolicitados = (req, res) => {
  certificacion.getCertificadosSolicitados((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

exports.getalumnosActivos = (req, res) => {
  certificacion.getalumnosActivos((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

exports.updateSolicitudCertificado = (req, res) => {

  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }

  certificacion.updateSolicitudCertificado(req.params.id, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro la solicitud con id : ${req.params.id}` });
      } else {
        res.status(500).send({ message: "Error al actualizar la solicitud con el id : " + req.params.id });
      }
    } else {
      res.status(200).send({ message: `La solicitud see ha actualizado correctamente.` })
    }
  })
}

exports.addImagenSolicitudCertificado = async( req, res ) => {
  try {
    // Obtenemos el archivo
    let EDFile = req.files.file

    let nombreArchivo = req.params.id + '.' + req.params.ext
    // Enivamos a guardar el archivo
    const imagenAgregada = await addImagen( EDFile, nombreArchivo ).then(response=> response)

    // Actualizamos la ruta de la imagen en la evaluación 
    const updateSolicitudCertificadoFoto = await certificacion.updateSolicitudCertificadoFoto( req.params.id, nombreArchivo ).then(response=> response)

    res.send({message:'Actualización correcta'});
  } catch ( error ){
    res.status(500).send({message:error})
  }
};

exports.crearCertificado = async( req, res ) => {
  try {
    const { nombre_completo, unidad_negocio, adultos } = req.body

    let nombreArchivo = `${ req.params.id }.jpg`
    const font  = await Jimp.loadFont(Jimp.FONT_SANS_128_BLACK);

    let image   = null
    let firma   = null
    let altura  = 0

    let largoTexto = Jimp.measureText(font, nombre_completo); 
    let inicioPosicion = ((2550 - largoTexto) / 2)


    if(unidad_negocio == 2 && adultos == 1){

      image = await Jimp.read('./../../imagenes-certificados/nivelesFAST.jpg')
      altura = 1200

      firma = await Jimp.read('./../../imagenes-certificados/firma_fast.png')

      // Agregando la firma al documento
      const newWidth = 850;
      const newHeight = Jimp.AUTO;

      firma.resize(newWidth, newHeight);

      image.composite(firma, 1120, 1980);
    
      image.print(font, inicioPosicion, altura, nombre_completo);
      image.quality(100).writeAsync(`./../../imagenes-certificados/${ nombreArchivo }`);


      const updateSolicitudCertificadoFoto = await certificacion.updateSolicitudCertificado2( req.params.id ).then(response=> response)
    }
    
    if(unidad_negocio == 1 && adultos == 1){

      image = await Jimp.read('./../../imagenes-certificados/nivelesINBI.jpg')
      altura = 1180

      firma = await Jimp.read('./../../imagenes-certificados/firma_inbi.png')

      // Agregando la firma al documento
      const newWidth = 450;
      const newHeight = Jimp.AUTO;

      firma.resize(newWidth, newHeight);

      image.composite(firma, 690, 2320);

      image.print(font, inicioPosicion, altura, nombre_completo);
      image.quality(100).writeAsync(`./../../imagenes-certificados/${ nombreArchivo }`);

      const updateSolicitudCertificadoFoto = await certificacion.updateSolicitudCertificado2( req.params.id ).then(response=> response)
    } 

    if(unidad_negocio == 1 && adultos == 0){

      image = await Jimp.read('./../../imagenes-certificados/nivelesINBITEENS.jpg')
      altura = 1180
      firma = await Jimp.read('./../../imagenes-certificados/firma_inbi.png')

      // Agregando la firma al documento
      const newWidth = 450;
      const newHeight = Jimp.AUTO;

      firma.resize(newWidth, newHeight);

      image.composite(firma, 690, 2320)

      image.print(font, inicioPosicion, altura, nombre_completo);
      image.quality(100).writeAsync(`./../../imagenes-certificados/${ nombreArchivo }`);

      const updateSolicitudCertificadoFoto = await certificacion.updateSolicitudCertificado2( req.params.id ).then(response=> response)
    } 


    res.send({message:'Actualización correcta'});
  } catch ( error ){
    res.status(500).send({message:error})
  }
};

addImagen = ( archivo, nombre ) => {
  return new Promise((resolve,reject)=>{
    let EDFile = archivo //EDFile.name
    EDFile.mv(`./../../imagenes-recepcion/${ nombre }`, err => {
      if (err) {
        reject( err )
      }else{
        resolve ( true )
      }
    })
  })
};


/**********************************************************************************************/
/********************************* C E R T I F I C A C I O N **********************************/
/**********************************************************************************************/

exports.getAlumnosCodigo = async(req, res) => {
  try {
    // Hay que obtener los codigos asignados
    const codigosFast       = await certificacion.getCodigosFast().then(response => response)
    const codigosInbi       = await certificacion.getCodigosInbi().then(response => response)
    
    let alumnos = codigosFast.concat(codigosInbi)

    const idAlumnos = alumnos.map((registro) => { return registro.iderp })
    const gruposAlumnos = await certificacion.gruposAlumnosPlantel( idAlumnos ).then(response => response)

    for( const i in alumnos ){
      const { iderp } = alumnos[i]

      const existePlantel = gruposAlumnos.find( el => el.id_alumno == iderp )
      alumnos[i]['plantel'] = existePlantel ? existePlantel.plantel : 'Sin plantel'
    }

    res.send(alumnos);
  } catch (error) {
    res.status(500).send({message: error ? error.message: 'Error en el servidor' })
  }
};

exports.getCertificadoCambridgeAlumno = async(req, res) => {
  try {
    const { idusuario, id_unidad_negocio } = req.body
    // Hay que obtener los codigos asignados
    if(id_unidad_negocio == 2){
      const codigosFast       = await certificacion.getCertificadoCambridgeFast(idusuario).then(response => response)
      if(codigosFast.length == 0){
        res.send({message: 'No tienes certificaciones'})
      }else{
        if(codigosFast[0].status == ''){
          res.send({message: 'Necesitas esperar a que tu resultado este listo'})
        }else{
          if(codigosFast[0].estatus == 0){
            res.send({message: 'Actualizar el nombre', datos: codigosFast[0]})
          }else{
            if(codigosFast[0].estatus >= 4){
              res.send({message: 'Certificado creado', datos: codigosFast[0]})
            }else{
              res.send({message: 'Pendiente', datos: codigosFast[0]})
            }
          }
        }
      }
    }else{
      const codigosInbi       = await certificacion.getCertificadoCambridgeInbi(idusuario, id_unidad_negocio).then(response => response)
      if(codigosInbi.length == 0){
        res.send({message: 'No tienes certificaciones', estatus: 0})
      }else{
        if(codigosInbi[0].status == ''){
          res.send({message: 'Necesitas esperar a que tu resultado este listo'})
        }else{
          if(codigosInbi[0].estatus == 0){
            res.send({message: 'Actualizar el nombre', datos: codigosInbi[0]})
          }else{
            if(codigosInbi[0].estatus >= 4){
              res.send({message: 'Certificado creado', datos: codigosInbi[0]})
            }else{
              res.send({message: 'Esperar', datos: codigosInbi[0]})
            }
          }
        }
      }
    }
    
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.updateCertificado = async(req, res) => {
  try {
    const { idcertificacion, id_unidad_negocio, nombre_completo, estatus } = req.body
     // Primero sacamos a todos los grupos que están en ese ciclo y funcionando 
    if( id_unidad_negocio == 2){
      const updateCodigoFast = await certificacion.updateCertificadoFast(idcertificacion, nombre_completo, estatus).then(response => response)
    }else{
      const updateCodigoInbi = await certificacion.updateCertificadoInbi(idcertificacion, nombre_completo, estatus).then(response => response)
    }

    res.send({mensaje: 'Registrado correctamente'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.updateCodigoNivel = async(req, res) => {
  try {
    const { idcertificacion, nivel, escuela, status } = req.body
     // Primero sacamos a todos los grupos que están en ese ciclo y funcionando 
    if( escuela == 2){
      const updateCodigoFast = await certificacion.updateCodigoNivelFast(idcertificacion, nivel, status).then(response => response)
    }else{
      const updateCodigoInbi = await certificacion.updateCodigoNivelInbi(idcertificacion, nivel, status).then(response => response)
    }

    res.send({mensaje: 'Registrado correctamente'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.crearCertificadoCambridge = async( req, res ) => {
  try {
    const { nombre, escuela, idcertificacion, nivel } = req.body

    let nombreArchivo   = idcertificacion + '.jpg'
    const font  = await Jimp.loadFont(Jimp.FONT_SANS_128_BLACK);
    let image   = null
    let firma   = null
    let altura  = 0


    if(escuela == 2 ){
      image = await Jimp.read(`./../../imagenes-cambridge/plantilla_fast_${ nivel }.jpg`)
      altura = 1200

      firma = await Jimp.read('./../../imagenes-certificados/firma_fast.png')

      // Agregando la firma al documento
      const newWidth = 850;
      const newHeight = Jimp.AUTO;

      firma.resize(newWidth, newHeight);

      image.composite(firma, 300, 1680);

      const update = await certificacion.updateCambridgeFotoFAST( idcertificacion, nombreArchivo ).then(response=> response)

    }else{
      
      altura = 1000
      image = await Jimp.read(`./../../imagenes-cambridge/plantilla_inbi_${ nivel }.jpg`)

      firma = await Jimp.read('./../../imagenes-certificados/firma_inbi.png')

      // Agregando la firma al documento
      const newWidth = 390;
      const newHeight = Jimp.AUTO;

      firma.resize(newWidth, newHeight);

      image.composite(firma, 1480, 1500);
      
      let archivo = idcertificacion + '.jpg' 
      const update = await certificacion.updateCambridgeFotoINBI( idcertificacion, nombreArchivo ).then(response=> response)
    }

    let largoTexto = Jimp.measureText(font, nombre); 
    let inicioPosicion = ((3300 - largoTexto) / 2)

    image.print(font, inicioPosicion, altura, nombre);
    image.quality(100).writeAsync(`./../../imagenes-cambridge/${ nombreArchivo }`);

    res.send({message: 'Datos actualizados correctamente'})

    
  } catch ( error ){
    res.status(500).send({message:error})
  }
};

exports.addImagenEntregaCambridge = async( req, res ) => {
  try {
    // Obtenemos el archivo
    let EDFile = req.files.file

    const { id, ext, escuela } = req.params
    if(escuela == 2){
      let nombreArchivo = id + '-FAST' + '.' + ext
      // Enivamos a guardar el archivo
      const imagenAgregada = await addImagenRecepcionCambridge( EDFile, nombreArchivo ).then(response=> response)

      // Actualizamos la ruta de la imagen en la evaluación 
      const updateSolicitudCertificadoFoto = await certificacion.updateCambridgeFotoFAST( id, nombreArchivo ).then(response=> response)

      res.send({message:'Actualización correcta'});

    }else{
      let nombreArchivo = id + '-INBI' + '.' + ext
      // Enivamos a guardar el archivo
      const imagenAgregada = await addImagenRecepcionCambridge( EDFile, nombreArchivo ).then(response=> response)

      // Actualizamos la ruta de la imagen en la evaluación 
      const updateSolicitudCertificadoFoto = await certificacion.updateCambridgeFotoINBI( id, nombreArchivo ).then(response=> response)

      res.send({message:'Actualización correcta'});
    }
  } catch ( error ){
    res.status(500).send({message:error})
  }
};

addImagenRecepcionCambridge = ( archivo, nombre ) => {
  return new Promise((resolve,reject)=>{
    let EDFile = archivo //EDFile.name
    EDFile.mv(`./../../imagenes-recepcion-cambridge/${ nombre }`, err => {
      if (err) {
        reject( err )
      }else{
        resolve ( true )
      }
    })
  })
};

exports.updateSolicitudCertificadoCambridge = (req, res) => {

  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }
  const { escuela } = req.body
  if(escuela == 2){
    certificacion.updateSolicitudCertificadoCambridgeFast(req.body, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({ message: `No se encontro la solicitud con id : ${req.body.idcertificacion}` });
        } else {
          res.status(500).send({ message: "Error al actualizar la solicitud con el id : " + req.body.idcertificacion });
        }
      } else {
        res.status(200).send({ message: `La solicitud see ha actualizado correctamente.` })
      }
    })
  }else{
    certificacion.updateSolicitudCertificadoCambridgeInbi( req.body, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({ message: `No se encontro la solicitud con id : ${req.body.idcertificacion}` });
        } else {
          res.status(500).send({ message: "Error al actualizar la solicitud con el id : " + req.body.idcertificacion });
        }
      } else {
        res.status(200).send({ message: `La solicitud see ha actualizado correctamente.` })
      }
    })
  }
}

/***********************************************************/
/***********************************************************/

exports.getAlumnoMatricula = async(req, res) => {
  try {
    const { escuela }   = req.params
    const { matricula } = req.body
    let alumno          = null
    let certificado     = null
    let catorceniveles  = null
    let estatusNiveles  = null
    let cambridge       = null

    // Validamos la escuela
    if( escuela == 2 ){
      // Buscamos en Fast
      alumno    = await certificacion.getAlumnoMatriculaFast( matricula ).then(response => response)
    }else{
      // Buscamos en Fast
      alumno   = await certificacion.getAlumnoMatriculaInbi( matricula ).then(response => response)
    }

    if(alumno){
      // Datos actualizados
      if(alumno.datos_actualizados == 1){
        // Verificar si el alumno tiene certificación o 14 niveles

        if(escuela == 2){
          certificado    = await certificacion.verificarExisteCertificacionFast(alumno.id).then(response => response)
          catorceniveles = await certificacion.verificarCatorceNivelesFast(alumno.id).then(response => response)
          estatusNiveles = await certificacion.getExisteSolicitudAlumno( alumno.iderp ).then(response => response)
        }else{
          certificado    = await certificacion.verificarExisteCertificacionInbi(alumno.id).then(response => response)
          catorceniveles = await certificacion.verificarCatorceNivelesInbi(alumno.id).then(response => response)
          estatusNiveles = await certificacion.getExisteSolicitudAlumno( alumno.iderp ).then(response => response)
        }

        res.status(200).send({ estatus: 1, alumno, certificado, catorceniveles, estatusNiveles})
      }else{
        // Datos no actualizados, actualizar
        res.status(200).send({ estatus: 0, alumno})
      }
    }else{
      res.status(500).send({ message: `La matricula ${ matricula } no existe` });
    }
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.updateAlumnoMatricula = async(req, res) => {
  try {
    const { escuela }   = req.params

    // Validamos la escuela
    if( escuela == 2 ){
      // Buscamos en Fast
      alumno   = await certificacion.updateAlumnoMatriculaFast( req.body ).then(response => response)
    }else{
      // Buscamos en Fast
      alumno   = await certificacion.updateAlumnoMatriculaInbi( req.body ).then(response => response)
    }

    if(alumno){
      res.status(200).send({ estatus: 1, message:'Actualización de datos exitosa'})
    }else{
      res.status(500).send({ message: `La matricula ${ req.body.matricula } no existe` });
    }
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.generarCopiaNiveles = async( req, res ) => {
  try {
    const { escuela } = req.params
    const { nombre, apellido_paterno, apellido_materno }  = req.body
    let nombre_completo = `${ nombre } ${ apellido_paterno } ${ apellido_materno }`
    let nombreArchivo   = nombre_completo.trim() + '.jpg'
    const font  = await Jimp.loadFont(Jimp.FONT_SANS_128_BLACK);
    let image   = null
    let altura  = 0
    
    let largoTexto = Jimp.measureText(font, nombre_completo); 
    let inicioPosicion = ((2550 - largoTexto) / 2)

    if(escuela == 2 ){
      image = await Jimp.read('./../../imagenes-certificados/nivelesFAST.jpg')
      altura = 1300
    }else{
      image = await Jimp.read('./../../imagenes-certificados/nivelesINBI.jpg')
      altura = 1400
    }
    
    image.print(font, inicioPosicion, altura, nombre_completo);
    image.quality(100).writeAsync(`./../../copias-certificados/${ nombreArchivo }`);

    res.send({message:'Actualización correcta', nombreArchivo});
    
  } catch ( error ){
    res.status(500).send({message:error})
  }
};


exports.generarCopiaNiveles2 = async( req, res ) => {
  try {
    const { escuela } = req.params
    const { nombre, apellido_paterno, apellido_materno }  = req.body
    let nombre_completo = `${ nombre } ${ apellido_paterno } ${ apellido_materno }`
    let nombreArchivo   = nombre_completo.trim() + '.jpg'
    const font  = await Jimp.loadFont(Jimp.FONT_SANS_128_BLACK);
    let image   = null
    let altura  = 0
    
    let largoTexto = Jimp.measureText(font, nombre_completo); 
    let inicioPosicion = ((2550 - largoTexto) / 2)

    if(escuela == 2 ){
      console.log('piubibpb')
      image = await Jimp.read('./../../imagenes-certificados/nivelesFAST.jpg')
      altura = 1210
    }else{
      image = await Jimp.read('./../../imagenes-certificados/nivelesINBI.jpg')
      altura = 1180
    }
    
    image.print(font, inicioPosicion, altura, nombre_completo);
    image.quality(100).writeAsync(`./../../copias-certificados/${ nombreArchivo }`);

    res.send({message:'Actualización correcta', nombreArchivo});
    
  } catch ( error ){
    res.status(500).send({message:error})
  }
};

exports.generarCopiaCambridge = async( req, res ) => {
  try {
    const { escuela } = req.params
    const { nombre_completo, idcertificacion, nivel } = req.body

    let nombreArchivo   = nombre_completo.trim() + '.jpg'
    const font  = await Jimp.loadFont(Jimp.FONT_SANS_128_BLACK);
    let image   = null
    let altura  = 0

    if(escuela == 2 ){
      image = await Jimp.read(`./../../imagenes-cambridge/plantilla_fast_${ nivel }.jpg`)
      altura = 1200
    }else{
      altura = 1000
      image = await Jimp.read(`./../../imagenes-cambridge/plantilla_inbi_${ nivel }.jpg`)
    }

    let largoTexto = Jimp.measureText(font, nombre_completo); 
    let inicioPosicion = ((3300 - largoTexto) / 2)

    image.print(font, inicioPosicion, altura, nombre_completo);
    image.quality(100).writeAsync(`./../../copias-certificados/${ nombreArchivo }`);

    res.send({message:'Actualización correcta', nombreArchivo});
  } catch ( error ){
    res.status(500).send({message:error})
  }
};
