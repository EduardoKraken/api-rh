const evaluacion_teacher = require("../../models/academico/evaluacion_teacher.model.js");


// Obtener el listado de codigos
exports.getEvaluacionClaseTeacher = async(req, res) => {
	try {
		let { idcicloFast, idcicloInbi, id_usuario } = req.body
		// Primero vamos a falidar si el teacher ya tiene una evaluacion terminada
		const calificacionFast = await evaluacion_teacher.getEvaluacionesCalificiacionFast( idcicloFast, idcicloInbi, id_usuario ).then(response => response)
		const calificacionInbi = await evaluacion_teacher.getEvaluacionesCalificiacionInbi( idcicloFast, idcicloInbi, id_usuario ).then(response => response)
		
		const { email } = await evaluacion_teacher.getUsuarioTeacherIderp( id_usuario ).then(response => response)
    // // Hay que obtener los codigos asignados
    const clasesTeachersFast       = await evaluacion_teacher.getClasesTeacherFast( idcicloFast, email ).then(response => response)
    const clasesTeachersInbi       = await evaluacion_teacher.getClasesTeacherInbi( idcicloInbi, email ).then(response => response)

    let leccionesFast = []
    let leccionesInbi = []
    let leccion = 1

    // Resultado de las clases o lecciones de Fast de ese usuario
    for(const i in clasesTeachersFast){
    	let payload = {}
    	// Desestructuramos el objeto para obtener los valores que necesitamos
    	const { id_grupo, grupo, total_dias_laborales, id_curso, diasdeFrecuencia, id_teacher, teacher1, id_teacher_2, teacher2, id_nivel, id_unidad_negocio, id_ciclo, email1, email2 } = clasesTeachersFast[i]

    	// Recorremos las 4 semanas
  		for( let j = 1; j <= 4; j++ ){

  			if(email1 == email){
  				payload = { id_unidad_negocio, id_nivel, id_teacher: '', teacher: '', num_teacher: '', grupo, id_grupo, id_leccion: 0, id_ciclo}
	  			// Llenamos 3 de lunes a miercoles
	  			payload.id_leccion      = j + 5
	  			payload.num_teacher     = 1
	    		payload.id_teacher      = id_teacher
	    		payload.teacher         = teacher1
	    		leccionesFast.push(payload)
	    		leccion += 1

	    		payload = { id_unidad_negocio, id_nivel, id_teacher: '', teacher: '', num_teacher: '', grupo, id_grupo, id_leccion: 0, id_ciclo}
	    		payload.id_leccion      = leccion
	  			payload.num_teacher     = 1
	    		payload.id_teacher      = id_teacher
	    		payload.teacher         = teacher1
	    		leccionesFast.push(payload)
	    		leccion += 1

	    		payload = { id_unidad_negocio, id_nivel, id_teacher: '', teacher: '', num_teacher: '', grupo, id_grupo, id_leccion: 0, id_ciclo}
	    		payload.id_leccion      = leccion
	  			payload.num_teacher     = 1
	    		payload.id_teacher      = id_teacher
	    		payload.teacher         = teacher1
	    		leccionesFast.push(payload)
	    		leccion += 1
  			}else{
  				leccion += 3
  			}

  			if(email2 == email){
  				payload = { id_unidad_negocio, id_nivel, id_teacher: '', teacher: '', num_teacher: '', grupo, id_grupo, id_leccion: 0, id_ciclo}
	    		payload.id_leccion      = leccion
	  			payload.num_teacher     = 2
	    		payload.id_teacher      = id_teacher_2
	    		payload.teacher         = teacher2
	    		leccionesFast.push(payload)
	    		leccion += 1

	    		payload = { id_unidad_negocio, id_nivel, id_teacher: '', teacher: '', num_teacher: '', grupo, id_grupo, id_leccion: 0, id_ciclo}
	    		payload.id_leccion      = leccion
	  			payload.num_teacher     = 2
	    		payload.id_teacher      = id_teacher_2
	    		payload.teacher         = teacher2
	    		leccionesFast.push(payload)
	    		leccion += 1
	    	}else{
	    		leccion += 2
	    	}
  		}

  		leccion = 1
    }

    // Resultado de las clases o lecciones de Inbi de ese usuario
    for(const i in clasesTeachersInbi){
    	let payload = {}
    	// Desestructuramos el objeto para obtener los valores que necesitamos
    	const { id_grupo, grupo, total_dias_laborales, id_curso, diasdeFrecuencia, id_teacher, teacher1, id_teacher_2, teacher2, id_nivel, id_unidad_negocio, id_ciclo, email1, email2 } = clasesTeachersInbi[i]

    	// Recorremos las 4 semanas
  		for( let j = 1; j <= 4; j++ ){

  			if(email1 == email){
  				payload = { id_unidad_negocio, id_nivel, id_teacher: '', teacher: '', num_teacher: '', grupo, id_grupo, id_leccion: 0, id_ciclo}
	  			// Llenamos 3 de lunes a miercoles
	  			payload.id_leccion      = j + 5
	  			payload.num_teacher     = 1
	    		payload.id_teacher      = id_teacher
	    		payload.teacher         = teacher1
	    		leccionesInbi.push(payload)
	    		leccion += 1

	    		payload = { id_unidad_negocio, id_nivel, id_teacher: '', teacher: '', num_teacher: '', grupo, id_grupo, id_leccion: 0, id_ciclo}
	    		payload.id_leccion      = leccion
	  			payload.num_teacher     = 1
	    		payload.id_teacher      = id_teacher
	    		payload.teacher         = teacher1
	    		leccionesInbi.push(payload)
	    		leccion += 1

	    		payload = { id_unidad_negocio, id_nivel, id_teacher: '', teacher: '', num_teacher: '', grupo, id_grupo, id_leccion: 0, id_ciclo}
	    		payload.id_leccion      = leccion
	  			payload.num_teacher     = 1
	    		payload.id_teacher      = id_teacher
	    		payload.teacher         = teacher1
	    		leccionesInbi.push(payload)
	    		leccion += 1
  			}else{
  				leccion += 3
  			}

  			if(email2 == email){
  				payload = { id_unidad_negocio, id_nivel, id_teacher: '', teacher: '', num_teacher: '', grupo, id_grupo, id_leccion: 0, id_ciclo}
	    		payload.id_leccion      = leccion
	  			payload.num_teacher     = 2
	    		payload.id_teacher      = id_teacher_2
	    		payload.teacher         = teacher2
	    		leccionesInbi.push(payload)
	    		leccion += 1

	    		payload = { id_unidad_negocio, id_nivel, id_teacher: '', teacher: '', num_teacher: '', grupo, id_grupo, id_leccion: 0, id_ciclo}
	    		payload.id_leccion      = leccion
	  			payload.num_teacher     = 2
	    		payload.id_teacher      = id_teacher_2
	    		payload.teacher         = teacher2
	    		leccionesInbi.push(payload)
	    		leccion += 1
	    	}else{
	    		leccion += 2
	    	}
  		}

  		leccion = 1
    }

    // // console.log(leccionesInbi)

    // Hasta aquí, resultadoFinal Ya tiene todas las lecciones por teacher

    // Obtener los niveles de los grupos de cada clase
    const nivelesTeacherClaseFast  = await evaluacion_teacher.getNivelesTeacherFast( idcicloFast, email ).then(response => response)
    const nivelesTeacherClaseInbi  = await evaluacion_teacher.getNivelesTeacherInbi( idcicloInbi, email ).then(response => response)

    // // console.log(nivelesTeacherClaseFast)
    // Generar los niveles de forma de array
    let nivelesFast = []
    for(const i in nivelesTeacherClaseFast){
    	nivelesFast.push(nivelesTeacherClaseFast[i].id_nivel)
    }
    
    // La declaramos vacia para no afectar los datos finales
    let preguntasFast = []
    let leccionesFastClases = []

    // consultar las lecciones por nivel y lecciones
    for(const i in nivelesFast){
	    for(const j in leccionesFast){
	    	if(leccionesFast[j].id_nivel == nivelesFast[i]){
	    		leccionesFastClases.push(leccionesFast[j].id_leccion)
	    	}
	    }
	    const preguntasFastNivel = await evaluacion_teacher.getEvaluacionesClasesFast( nivelesFast[i], leccionesFastClases ).then(response => response)
    	for(const k in preguntasFastNivel){
    		preguntasFast.push(preguntasFastNivel[k])
    	}
    	leccionesFastClases = []
    }

    /*****************************************************************/
  	let nivelesInbi = []
    for(const i in nivelesTeacherClaseInbi){
    	nivelesInbi.push(nivelesTeacherClaseInbi[i].id_nivel)
    }
    
    // La declaramos vacia para no afectar los datos finales
    let preguntasInbi = []
    let leccionesInbiClases = []

    // consultar las lecciones por nivel y lecciones
    for(const i in nivelesInbi){
	    for(const j in leccionesInbi){
	    	if(leccionesInbi[j].id_nivel == nivelesInbi[i]){
	    		leccionesInbiClases.push(leccionesInbi[j].id_leccion)
	    	}
	    }
    	const preguntasInbiNivel = await evaluacion_teacher.getEvaluacionesClasesInbi( nivelesInbi[i], leccionesInbiClases ).then(response => response)
    	for(const k in preguntasInbiNivel){
    		preguntasInbi.push(preguntasInbiNivel[k])
    	}
    	leccionesInbiClases = []
    }

    // Arreglar los objetos para enaviarlo ya con los datos completos
    for(const i in preguntasFast){
    	preguntasFast[i] = {
    		...preguntasFast[i],
    		eleccion:   0,
    		valor:      0,
    		id_teacher: id_usuario,
    		id_ciclo:   idcicloFast,
    	}
    }

    for(const i in preguntasInbi){
    	preguntasInbi[i] = {
    		...preguntasInbi[i],
    		eleccion:   0,
    		valor:      0,
    		id_teacher: id_usuario,
    		id_ciclo:   idcicloInbi,
    	}
    }

    const respuestas = await evaluacion_teacher.getRespuestaEvalaucionClaseTeacher( id_usuario, idcicloFast, idcicloInbi ).then(response => response)

    for(const i in preguntasFast){
    	const respuesta = respuestas.find(res => res.idpreguntas_evaluacion_leccion == preguntasFast[i].idpreguntas_evaluacion_leccion)
    	if( respuesta ){
    		preguntasFast[i].eleccion = respuesta.eleccion
    		preguntasFast[i].valor    = respuesta.valor
    	}
    }

    for(const i in preguntasInbi){
    	const respuesta = respuestas.find(res => res.idpreguntas_evaluacion_leccion == preguntasInbi[i].idpreguntas_evaluacion_leccion)
    	if( respuesta ){
    		preguntasInbi[i].eleccion = respuesta.eleccion
    		preguntasInbi[i].valor    = respuesta.valor
    	}
    }


    // Las enviamos 
    let resultadoFinal = {
    	usuario: email,
    	leccionesFast: leccionesFast,
    	leccionesInbi: leccionesInbi,
    	preguntasFast,
    	preguntasInbi,
    	calificacionFast,
    	calificacionInbi
    }

    res.send(resultadoFinal);
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addRespuestaEvalaucionClase = async(req, res) => {
  try {
  	const payload = req.body
    // Primero sacamos a todos los grupos que están en ese ciclo y funcionando 
    const existeRespuesta       = await evaluacion_teacher.getRespuestaEvalaucionClase(payload).then(response => response)

    // Validamos si existe
    if(existeRespuesta.length > 0){
    	// Actualizamos
    	const updateRespuesta     = await evaluacion_teacher.updateRespuestaEvalaucionClase(payload).then(response => response)
    }else{
    	// Agregamos
    	const addRespuesta        = await evaluacion_teacher.addRespuestaEvalaucionClase(payload).then(response => response)
    }

    res.send({message:'Se actualizo el teacher'});
  } catch (error) {
    res.status(500).send({message:error})
  }

};


exports.addCalificacionEvalaucionClase = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  evaluacion_teacher.addCalificacionEvalaucionClase(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send({ message: `El departamento se ha creado correctamente` });
  })
};