const { response } = require("express");
const { rest } = require("lodash");
const incidencias = require("../../models/academico/incidencias.model.js");


exports.getAlumnos = async (req, res,) => {
  try {
    const alumnos = await incidencias.getAlumnos( ).then(response => response) 
    const respuesta = alumnos;
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};


exports.getCiclos = async (req, res,) => {
    try {
      const ciclos = await incidencias.getCiclos( ).then(response => response) 
      const respuesta = ciclos;
      res.send(respuesta)
    } catch (error) {
      res.status(500).send({message: error ? error.message : "Error en el servidor"})
    }
  
  };


  exports.getIncidencias = async (req, res,) => {
    try {
      const { idcreo } = req.body
      const incidencia = await incidencias.getIncidencias( idcreo ).then(response => response) 
      const respuesta = incidencia;
      res.send(respuesta)
    } catch (error) {
      res.status(500).send({message: error ? error.message : "Error en el servidor"})
    }
  
  };


    exports.addIncidencia = (req, res) => {
        //validamos que tenga algo el req.body
        if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
        }

        incidencias.addIncidencia(req.body, (err, data) => {
        if (err)
            res.status(400).send({
            message: err.message || "Se produjo algún error al crear el curso"
            })
        else res.status(200).send({ message: `el curso se creo correctamente` });
        })
    };


  exports.updateIncidencia = (req, res) => {
    if (!req.body || Object.keys(req.body).length === 0) {
      res.status(400).send({ message: "El contenido no puede estar vacío" });
    }
    incidencias.updateIncidencia(req.params.id, req.body, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(400).send({ message: `No se encontro la regla con id : ${req.params.id}, ${ err }` });
        } else {
          res.status(400).send({ message: `Error al actualizar la regla con el id : ${req.params.id}, ${ err } `});
        }
      } else {
        res.status(200).send({ message: `el curso se ha actualizado correctamente.` })
      }
    })
  };


  exports.consultarMensajesRechazoIncidencias = async (req, res,) => {
    try {
      const { iderp, idincidencias } = req.body
  
      //Trae los datos para llenar la tabla de egresos
      const respuesta = await incidencias.consultarMensajesRechazoIncidencias( iderp, idincidencias ).then(response => response)
  
      res.send(respuesta)
    } catch (error) {
      res.status(500).send({message: error ? error.message : "Error en el servidor"})
    }
  
  };


  exports.addIncidenciasMensajeRespuesta = async(req, res) => {
    try {
      const { nota_respuesta, idincidencias, iderp, idincidencias2,  iderp2 } = req.body
      console.log(req.body)

        const updateEstatusPartidaRevision = await incidencias.addIncidenciasMensajeRespuesta( nota_respuesta, idincidencias, iderp, idincidencias2,  iderp2 ).then( response=> response )  
      
        res.send({ message: 'Éxito en el proceso' });
    } catch (error) {
      res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
    }
  };


  exports.updateEstatusIncidencias = async(req, res) => {
    try {
      const { estatus, id } = req.body

        const updateEstatusPartidaRevision = await incidencias.updateEstatusIncidencias( estatus, id ).then( response=> response )  
      
        res.send({ message: 'Éxito en el proceso' });
    } catch (error) {
      res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
    }
  };


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//ANGEL RODRIGUEZ -- TODO