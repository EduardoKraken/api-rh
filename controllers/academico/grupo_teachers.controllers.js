var fs = require('fs');
const grupoTeachers = require("../../models/academico/grupo_teachers.model.js");
const catalogos     = require("../../models/lms/catalogoslms.model.js");



// Obtener los grupos y sus teacher del ciclo actual
exports.getGrupoTeachers = (req, res) => {
	const { idciclo, idciclo2 } = req.params
  grupoTeachers.getGrupoTeachers(idciclo, idciclo2,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los grupos"
      });
    else res.send(data);
  });
};



// Obtener los teachers activos
exports.getTeachersActivos = (req, res) => {
  grupoTeachers.getTeachersActivos((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los teachers activos"
      });
    else res.send(data);
  });
};

/*
  GRUPO TEACHERS
  1-. Agregar al ERP VIEJITO los maestros
  2-. Agregar los teachers a los LMS
  3-. Actualizar los teacher en el grupo del LMS
*/

exports.addGrupoTeacher = async(req, res) => {
  try {
  	const payload = req.body
    // Primero sacamos a todos los grupos que están en ese ciclo y funcionando 
    const existeTeacher       = await grupoTeachers.getTeacherGrupo(payload).then(response => response)
    // Validamos si existe
    if(existeTeacher.length > 0){
    	// Actualizamos
    	const updateTeacher     = await grupoTeachers.updateTeacherGrupo(payload).then(response => response)
    }else{
    	// Agregamos
    	const addTeacher        = await grupoTeachers.addTeacherGrupo(payload).then(response => response)
    }


    const {  ciclo, id_grupo, id_maestro, numero_teacher, escuela } = req.body 

    // Guardamos el ciclo dependiendo de la escuela
    let cicloLMS = escuela == 2 ? ciclo.id_ciclo_relacionado : ciclo.id_ciclo

    // consultar los ciclos primero, para sacar de quien es cada ciclo
    const cicloErpFast = await catalogos.getCicloEscuela( cicloLMS, escuela ).then( response => response )

    // Un ciclo no existe hay que cargarlos
    if( !cicloErpFast ){
      /* 
        CARGAR HORARIOS CICLOS
      */
      const ciclos = await cronCiclos( ).then( response => response )
    }

    // Cargamos los teacher activos
    // Hacemos una peticion async para obtener los datos de ambas bases de datos
    const teachers = await catalogos.getTeachersLMSFAST( escuela ).then(response => response)

    // Cargamos los grupos
    const grupos   = await catalogos.getGruposLMSFAST( cicloLMS, escuela ).then(response => response)

    // Cargamos los grupo y los teacher del erp
    let gruposTeachersErp = await catalogos.getGruposTeacherLMSERP( cicloLMS ).then(response=> response)

    // cargar los grupo_teachers del LMS de FAST
    const gruposTeachersFast = await catalogos.getGruposTeacherLMSFAST( cicloLMS, escuela ).then(response => response)

    // Declaramos las variables a utilizar 
    let id_teacher1 = 0
    let id_teacher2 = 0
    let busqueda1   = 0
    let busqueda2   = 0


    // Verificamos que ciclo del ERP no esta en FAST
    id_teacher1 = 0
    id_teacher2 = 0

    const existeGrupo = gruposTeachersFast.find( el => el.iderp == id_grupo )

    // primero hay que buscar el primer teacher
    let teacher   = gruposTeachersErp.find(el => el.id_grupo == id_grupo && el.numero_teacher == 1)
    let teacher2  = gruposTeachersErp.find(el => el.id_grupo == id_grupo && el.numero_teacher == 2)

    // Buscar el id_usuario
    if( teacher ){   
      let busqueda1 = teachers.find( el=> el.iderp == teacher.id_usuario )
      if( busqueda1 ){ id_teacher1 = busqueda1.id }else{ id_teacher1 = 0 }
    }

    // Buscar el id_usuario2
    if( teacher2 ){   
      let busqueda2 = teachers.find( el=> el.iderp == teacher2.id_usuario )
      if( busqueda2 ){ id_teacher2 = busqueda2.id }else{ id_teacher2 = 0 }
    }

    // Buscar el grupo en el LMS
    const id_grupo_lms = grupos.find( el => el.iderp == id_grupo  )

    if( !id_grupo_lms ){ return res.status(500).send({message: 'El grupo no existe en el LMS' }) }

    // si no existe hay que agregarlo
    if(!existeGrupo){
      // Ahora si ya podemos agregar el grupo , ya que tenemos validado si existe o no
      const addGruposTeacherFast = await catalogos.addGruposTeacherFast(id_grupo_lms.id, id_teacher1, id_teacher2, escuela).then(response => response)
    }else{
      // Actualizamos el grupo
      const updateGruposTeacherFast = await catalogos.updateGruposTeacherFast(id_grupo_lms.id, id_teacher1, id_teacher2, escuela).then(response => response)
    }

    res.send({message:'Se actualizo el teacher'});
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};


