const { response } = require("express");
const { rest } = require("lodash");
const comentarios_maestros_calificaciones = require("../../models/academico/comentarios_maestros_calificaciones.model.js");


exports.getMensaje = async (req, res,) => {
  try {
    const INBI = await comentarios_maestros_calificaciones.getMensajeINBI( ).then(response => response) 
    const FAST = await comentarios_maestros_calificaciones.getMensajeFAST( ).then(response => response) 
    const respuesta = INBI.concat(FAST);
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};


exports.updateMensaje = async(req, res) => {
  try {
    const { escuela, idmaestro_mensaje, id_alumno} = req.body

    // Actualizar "finalizo"
    const respuesta = await comentarios_maestros_calificaciones.updateMensaje( escuela, idmaestro_mensaje, id_alumno).then(response => response) 
    res.send({message: 'Se acepto el mensaje'});
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};


exports.updateMensajeRechazo = async(req, res) => {
  try {
    const { escuela, idmaestro_mensaje, id_alumno } = req.body
  
    // Actualizar "finalizo"
    const respuesta = await comentarios_maestros_calificaciones.updateMensajeRechazo( escuela, idmaestro_mensaje, id_alumno).then(response => response) 
    res.send({message: 'Se rechazo el mensaje '});
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};


exports.updateKardex = async(req, res) => {
  try {
    
    const { escuela, id_alumno, id_grupo  } = req.body

    // const id = await comentarios_maestros_calificaciones.getIdUsuario( escuela, id_alumno ).then(response => response)
    // const id2 = await comentarios_maestros_calificaciones.getIdGrupo( escuela, id_grupo ).then(response => response)
    // const id_usuario = parseInt(id[0].id);
    // const id_grupos = parseInt(id2[0].id);

    // Consultar telefono del alumno
    const updateKardex  = await comentarios_maestros_calificaciones.updateKardex( escuela, id_alumno, id_grupo ).then( response => response )
    return res.send({ message: 'Datos actualizados correctamente' })

  } catch (error) {
    return res.status(500).send({message : error ? error.message : 'Error en el servidor '})
  }
};


 //Nuevo

 exports.consultarMensajesRechazo = async (req, res,) => {
  try {
    const { escuela, id_alumno, id_grupo } = req.body

    //Trae los datos para llenar la tabla de egresos
    const respuesta = await comentarios_maestros_calificaciones.consultarMensajesRechazo( escuela, id_alumno, id_grupo ).then(response => response)

    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};


exports.addIncidenciasMensajeRespuesta = async(req, res) => {
  try {
    const { escuela, respuesta, id_alumno, id_grupo, id_alumno2, id_grupo2} = req.body

      const updateEstatusPartidaRevision = await comentarios_maestros_calificaciones.addIncidenciasMensajeRespuesta( escuela, respuesta, id_alumno, id_grupo, id_alumno2, id_grupo2 ).then( response=> response ) 
    
      res.send({ message: 'Éxito en el proceso de envio' });
  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.updateEstatusIncidencias = async(req, res) => {
  try {
    const { escuela, estatus, id } = req.body

      const updateEstatusPartidaRevision = await comentarios_maestros_calificaciones.updateEstatusIncidencias( escuela, estatus, id ).then( response=> response )  
    
      res.send({ message: 'Éxito en el proceso de envio' });
  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};





//ANGEL RODRIGUEZ -- TODO