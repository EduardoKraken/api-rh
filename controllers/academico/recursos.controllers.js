const { response } = require("express");
const { rest } = require("lodash");
const recursos = require("../../models/academico/recursos.model.js");


exports.getCursosAlumnos = async (req, res,) => {
    try {
      const { escuela } = req.body
  
      //Trae los datos para llenar la tabla de egresos
      const respuesta = await recursos.getCursosAlumnos( escuela ).then(response => response)
  
      res.send(respuesta)
    } catch (error) {
      res.status(500).send({message: error ? error.message : "Error en el servidor"})
    }
  };

exports.getRecursosAlumnos = async (req, res,) => {
    try {
      const { escuela } = req.body
  
      //Trae los datos para llenar la tabla de egresos
      const respuesta = await recursos.getRecursosAlumnos( escuela ).then(response => response)
  
      res.send(respuesta)
    } catch (error) {
      res.status(500).send({message: error ? error.message : "Error en el servidor"})
    }
  
};  


exports.getPreguntasAlumnos = async (req, res,) => {
  try {
    const { escuela, idEvaluacion } = req.body

    //Trae los datos para llenar la tabla de egresos
    const respuesta = await recursos.getPreguntasAlumnos( escuela, idEvaluacion ).then(response => response)

    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};  


exports.getRespuestasAlumnos = async (req, res,) => {
  try {
    const { escuela, idEvaluacion } = req.body

    //Trae los datos para llenar la tabla de egresos
    const respuesta = await recursos.getRespuestasAlumnos( escuela, idEvaluacion ).then(response => response)

    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};  


exports.getRespuestasAlumnosPORpregunta = async (req, res,) => {
  try {
    const { escuela, idPregunta } = req.body

    //Trae los datos para llenar la tabla de egresos
    const respuesta = await recursos.getRespuestasAlumnosPORpregunta( escuela, idPregunta ).then(response => response)

    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};  


//ANGEL RODRIGUEZ -- TODO