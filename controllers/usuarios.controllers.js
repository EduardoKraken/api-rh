//declaramos en una const nuestro model
const Usuarios  = require("../models/usuarios.model.js");
const catalogos = require("../models/lms/catalogoslms.model.js");
const entradas  = require("../models/catalogos/entradas.model.js");
const { v4: uuidv4 }  = require('uuid')
// Importaciones
const AdmZip = require('adm-zip');
const fs = require('fs');
const path = require('path');

const { agregarArchivo } = require("../helpers/agregar_archivo.js");


exports.all_usuarios = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Usuarios.all_usuarios((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los usuarios"
            });
        else res.send(data);
    });
};

exports.getUsuarioId = async(req, res) => {
  try {
    // Cargamos todos las preguntas no borradas
    const getUsuario = await Usuarios.getUsuarioId( req.params.idusuarios ).then(response=> response)
    // Validamos que todo este bien y que si haya un usuarios
    if(getUsuario.length > 0){
      const getPlanteles = await Usuarios.getPlantelesUsuario( getUsuario[0].idusuario ).then(response=> response)
      const respuesta = {
        ...getUsuario[0],
        planteles: getPlanteles
      }
      res.status(200).send(respuesta)
    }else{
      res.status(500).send({message:'usuario no existe'})
    }
  } catch (error) {
    res.status(500).send({message:error})
  }
};


// Obtener la información completa del usuario
exports.getUsuarioInfo = async(req, res) => {
  try {
    const { id } = req.params
    // Cargamos todos las preguntas no borradas
    const getUsuario      = await Usuarios.getNuevoERPUsuario( id ).then(response=> response)
    // Si no existe, hay que mencionarlo
    if( !getUsuario ){
      return res.status( 400 ).send({ message:'Usuario no existente'})
    }

    // Consultamos la información de ese usuario pero en el nuevo ERP 
    const getUsuariosERPID = await Usuarios.getUsuariosERPID( id ).then(response=> response)
    // Si no existe, hay que mencionarlo
    if( !getUsuariosERPID ){
      return res.status( 400 ).send({ message:'Usuario no existente'})
    }

    const respuesta = {
      ...getUsuario,
      ...getUsuariosERPID
    }
    
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.add_usuarios = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    Usuarios.add_usuarios(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear el usuario"
            })
        else res.status(200).send({ message: `El usuario se ha creado correctamente` });
    })
};

exports.update_usuario = (req, res) => {
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Usuarios.update_usuario(req.params.idusuarios, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(400).send({ message: `No se encontro al usuario con id : ${req.params.idusuarios}` });
            } else {
                res.status(500).send({ message: "Error al actualizar el usuario con el id : " + req.params.idsucursales });
            }
        } else {
            res.status(200).send({ message: `El usuario se ha actualizado correctamente.` })
        }
    })
}

/******************************************************/
exports.all_usuariosERP = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Usuarios.all_usuariosERP((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los usuarios"
            });
        else res.send(data);
    });
};

exports.all_usuariosCAPA = async(req, res) => {
  try {
    // Cargamos todos las preguntas no borradas
    const getUsuario = await Usuarios.all_usuariosCAPA( req.params.id ).then(response=> response)
    res.send(getUsuario);
  } catch (error) {
    res.status(500).send({message:error})
  }
};


//usuario id erp
exports.usuario_id = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Usuarios.usuario_id(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los usuarios"
            });
        else res.send(data);
    });
};

///usuario por puesto
exports.getusuariosPuesto = (req, res) => {
    Usuarios.getusuariosPuesto(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los usuarios"
            });
        else res.send(data);
    });
};

//usuario por departamento 
exports.getusuariosDepto = (req, res) => {
    Usuarios.getusuariosDepto(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los usuarios"
            });
        else res.send(data);
    });
};

/******************************************************/
exports.getUsuariosNiveles = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Usuarios.getUsuariosNiveles((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los niveles del organigrama"
            });
        else res.send(data);
    });
};

exports.addUsuariosNiveles = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    Usuarios.addUsuariosNiveles(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear los niveles del organigrama"
            })
        else res.status(200).send({ message: `los niveles del organigrama se ha creado correctamente` });
    })
};

exports.updateUsuariosNiveles = (req, res) => {
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Usuarios.updateUsuariosNiveles(req.params.id, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(400).send({ message: `No se encontro el nivel del organigramacon con id : ${req.params.id}` });
            } else {
                res.status(500).send({ message: "Error al actualizar el nivel del organigramacon el id : " + req.params.id });
            }
        } else {
            res.status(200).send({ message: `el nivel del organigramacon se ha actualizado correctamente.` })
        }
    })
}

exports.loginCapa = async(req, res) => {

  try {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status(500).send({ message: "El Contenido no puede estar vacio" });
    }

    let getUsuario = await Usuarios.loginCapa( req.body ).then(response=> response)

    if( !getUsuario ){
      return res.status(500).send({ message: "El usuario no existe :(" });
    }

    let usuario = await Usuarios.loginCapa2( getUsuario.id_usuario ).then(response=> response)

    if( !usuario ){
      return res.status(500).send({ message: "El usuario no existe :(" });
    }

    const getPlanteles = await Usuarios.getPlantelesUsuario( usuario.idusuario ).then(response=> response)

    const respuesta = [{
      ...getUsuario,
      idpuesto               : usuario.idpuesto,
      puesto                 : usuario.puesto,
      idusuario              : usuario.idusuario,
      idniveles_capacitacion : usuario.idniveles_capacitacion,
      planteles              : getPlanteles
    }]

    res.send(respuesta);

  } catch (error) {
    res.status(500).send({message:error  ? error.message : 'Error en el servidor'})
  }
  
};

exports.getUsuariosActivos = async(req, res) => {
  try{
    const usuariosActivos = await Usuarios.getUsuariosActivos( ).then( response => response )

    res.send( usuariosActivos )
  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.getEntradasEmpleados = async(req, res) => {
  try{

    const { fecha, fechafin } = req.body

    const { dias } = await catalogos.getDiferenciaDias( fecha, fechafin ).then( response => response )
    let resultado = []

    for (var i = 0; i <= dias; i++) {
      const entradasEmpleados = await Usuarios.getEntradasEmpleados( fecha, i ).then( response => response )
      const idEmpleados = entradasEmpleados.map((registro) => { return registro.id })
      let   idtrabajadores = entradasEmpleados.map((registro) => { return registro.id_usuario })
      let result = idEmpleados.filter((item,index)=>{
        return idEmpleados.indexOf(item) === index;
      })

      // ver que puesto tiene cada usuario

      idtrabajadores = idtrabajadores.length ? idtrabajadores : [0]

      let puestosEmpledao = await Usuarios.getPuestosEmpleado( idtrabajadores ).then( response => response )

      for( const i in puestosEmpledao ){
        const existeEmpleado = entradasEmpleados.find( el => el.id_usuario == puestosEmpledao[i].iderp )

        puestosEmpledao[i]['id_empleado'] = existeEmpleado ? existeEmpleado.id : null 
      }

      let entradas = []

      for( const i in result ){
        const existeAsistencia = entradasEmpleados.find( el => el.id == result[i] )
        existeAsistencia['tipo'] = 1
        entradas.push( existeAsistencia )
      }

      const reversed = entradasEmpleados.reverse();

      let salidas = []

      for( const i in result ){
        const existeAsistencia = reversed.find( el => el.id == result[i] )
        if( existeAsistencia ){
          const { id_salida } = existeAsistencia
          const existeSalida = entradas.find( el => el.id_salida == id_salida )
          if( !existeSalida ){
            existeAsistencia['tipo'] = 2
            salidas.push( existeAsistencia )
          }
        }
      }
      
      for( const i in result ){
        let existeEntrada = entradas.find( el => el.id == result[i] )
        let existeSalida  = salidas.find( el => el.id == result[i] )
        let existeDatos   = entradasEmpleados.find( el => el.id == result[i] )
        let existeFecha   = entradasEmpleados.find( el => el.id == result[i] )
        let existePuesto  = puestosEmpledao.find( el => el.id_empleado == result[i] )

        if( existeDatos ) {
          const payload = {
            id:              existeDatos   ? existeDatos.id              : '',
            idcehcador:      existeDatos   ? existeDatos.idchecador      : uuidv4(),
            nombre_completo: existeDatos   ? existeDatos.nombre_completo : '',
            plantel:         existeDatos   ? existeDatos.plantel         : '',
            entrada:         existeEntrada ? existeEntrada.hora_registro : null ,
            salida:          existeSalida  ? existeSalida.hora_registro  : null,
            fecha:           existeFecha   ? existeFecha.fecha           : null,
            id_usuario:      existeDatos   ? existeFecha.id_usuario      : null,
            puesto:          existePuesto  ? existePuesto.puesto         : 'Sin puesto',
          }
          resultado.push( payload )
        }
      }
    }


    res.send( resultado )
  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.generarCodigoAsistenciaTeacher = async(req, res) => {
  try{

    const { usuario, password } = req.body

    const existeUsuario = await Usuarios.getExisteUsuario( usuario ).then( response => response )
    if( !existeUsuario ){ return res.status( 500 ).send({ message: 'El usuario no existe' }) }

    const { email, id_usuario, id_trabajador } = existeUsuario

    const contraseniaCorrecta = await Usuarios.valdiarContrasenia( email, password ).then( response => response )
    if( !contraseniaCorrecta ){ return res.status( 500 ).send({ message: 'La contraseña es incorrecta' }) }

    // Validar si el teacher tiene que dar clase hoy
    const clasesFast = await entradas.clasesPorIniciar( 2 ).then( response => response )
    const clasesInbi = await entradas.clasesPorIniciar( 1 ).then( response => response )

    const existeClaseFast = clasesFast.find( el => el.iderp ==  id_usuario )
    const existeClaseInbi = clasesInbi.find( el => el.iderp ==  id_usuario )

    if( !existeClaseFast && !existeClaseInbi){ return res.status( 500 ).send({ message: 'Para solicitar el código, deben ser máximo 15 minutos antes y 10 minutos después de iniciar la clase' }) }

    const id_grupo       = existeClaseFast ? existeClaseFast.id_grupo : existeClaseInbi.id_grupo
    const unidad_negocio = existeClaseFast ? 2                        : 1


    // Antes de generar otro codigo, hay que validar si ya tiene uno asignado para el día de hoy
    const existeCodigo = await Usuarios.existeCodigo( id_usuario, id_grupo, unidad_negocio ).then( response => response )
    if( existeCodigo ){ return res.send({ codigo: existeCodigo.codigo }) }

    const codigo = generarCodigo(6)

    // Insertar el codigo del teacher para que pueda registrar su asistencia
    const addCodigoAsistencia = await Usuarios.addCodigoAsistencia( id_trabajador, id_usuario, codigo, id_grupo, unidad_negocio ).then( response => response )

    res.send({ codigo })

  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


generarCodigo = (length) => {
  let characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

  let pass = "";
  for (i=0; i < length; i++){
    pass += characters.charAt(Math.floor(Math.random()*characters.length));   
  }
  return pass;
}


/***** Archivos *****/

// Agregar archivo
exports.addArchivo = async (req, res) => {
  try{

    // Validación de que todos los datos hayan llegado
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // Desestructurar los campos necesarios
    const { archivo } = req.body;

    // Validacion que no exista un registro identico
    const archivoEncontrado = await Usuarios.getArchivoNombre({ archivo }).then( response => response );

    if( archivoEncontrado ) return res.status( 400 ).send({ message: `El archivo con nombre ${archivo} ya existe en base de datos` });

    // Agregar archivo
    const archivoNuevo = await Usuarios.addArchivo({ archivo }).then( response => response );

    // Mensaje de respuesta al usuario
    res.status(200).send({ message: 'Archivo generado correctamente', archivo: archivoNuevo });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};

// Actualizar archivo
exports.updateArchivo = async (req, res) => {
  try{

    // Validación de que todos los datos hayan llegado
    if (!req.body || Object.keys(req.body).length === 0 || !req.params.id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    const { id } = req.params;
    
    // Validar que exista el archivo con el id proporcionado
    const archivoEncontrado = await Usuarios.getArchivo( id ).then( response => response );
    if ( !archivoEncontrado ) return res.status( 400 ).send({ message: `El archivo con id ${id} no ha sido encontrado` });


    // Objeto para insertar los campos válidos
    const datosActualizar = { ...archivoEncontrado };

    // Si el campo proporcionado del body existe en archivo_encontrado se agrega al objeto con su respectivo valor
    for (const campo in req.body) {
      if( archivoEncontrado.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }

    // Actualizacion con los datos validos proporcionados
    const archivo = await Usuarios.updateArchivo( datosActualizar, id ).then( response=> response );

    // Respuesta al usuario
    res.status( 200 ).send({ message: 'Archivo actualizado correctamente', archivo });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};

// Obtener archivo
exports.getArchivo = async (req, res) => {
  try{

    // Validación de que los parametros necesarios hayan llegado
    if (!req.params || !req.params.id ) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });
    
    const { id } = req.params;

    // Obtener archivo mediante id
    const archivo = await Usuarios.getArchivo( id ).then( response => response );

    if ( !archivo ) return res.status( 400 ).send({ message: `El archivo con id ${id} no ha sido encontrado` });

    // Respuesta al usuario
    res.send({ message: 'Archivos encontrado correctamente', archivo });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};

// Obtener archivos
exports.getArchivos = async (req, res) => {
  try{
    
    // Obtener archivos
    const archivos = await Usuarios.getArchivos(  ).then( response => response );

    // Obtener el total de archivos
    const total = archivos.length;

    // Respuesta al usuario
    res.status( 200 ).send({ message: 'Archivos obtenidos', total_archivos: total, archivos });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


/********** Archivos Empleados **********/

// Agregar archivo empleado
exports.addArchivoEmpleado = async (req, res) => {
  try{

    // Validación de que todos los datos hayan llegado
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // Desestructurar los campos necesarios
    const { idarchivos, idempleados, documento } = req.body;

    // Agregar archivo
    const archivoNuevo = await Usuarios.addArchivoEmpleado({ idarchivos, idempleados, documento }).then( response => response );

    // Mensaje de respuesta al usuario
    res.status(200).send({ message: 'Archivo de empleado generado correctamente', archivo: archivoNuevo });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};

// Actualizar archivo empleado
exports.updateArchivoEmpleado = async (req, res) => {
  try{

    // Validación de que todos los datos hayan llegado
    if (!req.body || Object.keys(req.body).length === 0 || !req.params.id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    const { id } = req.params;
    
    // Validar que exista el archivo empleado con el id proporcionado
    const archivoEncontrado = await Usuarios.getArchivoEmpleado( id ).then( response => response );
    if ( !archivoEncontrado ) return res.status( 400 ).send({ message: `El archivo de empleado con id ${id} no ha sido encontrado` });


    // Objeto para insertar los campos válidos
    const datosActualizar = { ...archivoEncontrado };

    // Si el campo proporcionado del body existe en archivo_encontrado se agrega al objeto con su respectivo valor
    for (const campo in req.body) {
      if( archivoEncontrado.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }

    // Actualizacion con los datos validos proporcionados
    const archivo = await Usuarios.updateArchivoEmpleado( datosActualizar, id ).then( response=> response );

    // Respuesta al usuario
    res.status( 200 ).send({ message: 'Archivo empleado actualizado correctamente', archivo });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// Obtener archivo empleado
exports.getArchivoEmpleado = async (req, res) => {
  try{

    // Validación de que los parametros necesarios hayan llegado
    if (!req.params || !req.params.id ) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });
    
    const { id } = req.params;

    // Obtener archivo empleado mediante id
    const archivo = await Usuarios.getArchivoEmpleado( id ).then( response => response );

    if ( !archivo ) return res.status( 400 ).send({ message: `El archivo con id ${id} no ha sido encontrado` });

    // Respuesta al usuario
    res.send({ message: 'Archivos encontrado correctamente', archivo });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};

// Obtener los archivos de un empleado
exports.getArchivosEmpleado = async (req, res) => {
  try{

    // Validación de que todos los datos hayan llegado
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // Desestructurar los campos necesarios
    const { idempleados  } = req.body;
    
    // Obtener nombre empleado
    const nombre = await Usuarios.getNombreEmpleado( idempleados  ).then( response => response );

    // Validar que exista el usuario
    if ( !nombre ) return res.status( 400 ).send({ message: `El empleado con id ${idempleados } no ha sido encontrado` });

    // Obtener archivos
    const archivos = await Usuarios.getArchivosEmpleado( idempleados  ).then( response => response );

    for( const i in archivos ){
      archivos[i]['nombre_completo'] = nombre.nombre_completo
    }

    // Obtener el total de archivos
    const total = archivos.length;

    // Respuesta al usuario
    res.status( 200 ).send({ message: 'Archivos de empleado obtenidos', nombre_empleado: nombre, total_archivos: total, archivos });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// Descargar archivos del servidor
exports.downloadArchivosServidor = async (req, res) => {
  try{

    // Validación de que todos los datos hayan llegado
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // Desestructurar los campos necesarios
    const { archivos, idempleados } = req.body;

    // Obtener archivos
    // const archivos = await Usuarios.getArchivosNombre( idsarchivos ).then( response => response );

    // Aquí puedes colocar el código que deseas ejecutar después de la espera de 5 segundos
    // Guardar todo en un ZIP
    const zip = new AdmZip();

    for (const i in archivos) {

      const { idempleados, documento } = archivos[i]

      // Agregar un archivo a la compresión
      zip.addLocalFile(`../../archivos_empleados/${documento}`);
      // Puedes agregar más archivos si es necesario
    }
    
    // Guardar el archivo ZIP en disco
    zip.writeZip(`../../archivos_empleados/${idempleados}.zip`);


    return res.send({ message: 'Datos generados' });

    // Ruta de la carpeta donde se almacenan los documentos
    const rutaArchivos = './../../archivos_empleados';

    // Agregar los archivos al archivo .zip
    for (const archivo of archivos) {
      const archivoPath = path.join( rutaArchivos, archivo.documento );

      if (fs.existsSync(archivoPath)) {
        zip.addFile( archivo.documento, fs.readFileSync(archivoPath) );
      }
    }

    // Generar el contenido del archivo .zip
    const contenidoZip = zip.toBuffer();

    // Configurar las cabeceras de la respuesta
    res.setHeader('Content-Type', 'application/zip');
    res.setHeader('Content-Disposition', 'attachment; filename=archivos.zip');

    // Respuesta al usuario
    res.status( 200 ).send( contenidoZip );

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


/********** Archivos Servidor **********/

// Agregar archivo al servidor
exports.addArchivoServidor = async (req, res) => {
  try{

    // Validacion de que exista un archivo en el req.files llamado file
    if( !req.files || !req.files.file ){
      return res.status( 400 ).send({ message : 'El contenido file no puede ir vacio' });
    }
    
    // Desestructurar los campos necesarios
    const { file } = req.files;

    // Extensiones válidas para cargar archivos e imagenes
    const extensiones = [ 'pdf', 'PDF', 'jpg', 'JPG', 'jpeg','JPEG', 'png', 'PNG' ];

    // Ruta de insercion al servidor
    const PATH = './../../archivos_empleados';

    // Agregar el archivo
    const archivo = await agregarArchivo( file, extensiones, PATH );

    // Validacion de que fue creado el archivo
    if( !archivo ) return res.status( 400 ).send({ message : 'El archivo no pudo ser creado' });

    const { nombreUuid } = archivo;

    // Mensaje de respuesta al usuario
    res.send({ message: 'Archivo de empleado agregado correctamente', nombre: nombreUuid });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};



// Eliminar archivo del servidor
exports.deleteArchivoServidor = async (req, res) => {
  try{

    // Validacion de que los datos necesarios hayan llegado
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // Obtener nombre del archivo
    const { nombre_archivo } = req.body;

    if( !nombre_archivo ) return res.status( 400 ).send({ message: 'Es necesario un nombre_archivo' });

    // Ruta de eliminación del servidor
    const PATH = './../../archivos_empleados';

    // Respuesta al usuario
    try {
      fs.unlinkSync(`${ PATH }/${ nombre_archivo }`);
      res.send({ message: 'Archivo de empleado eliminado correctamente' });
    } catch (error) {
      if( error.code === 'ENOENT' ) 
        return res.status(400).send({ message: 'El archivo no se encuentra en el servidor' });
      throw error;
    }

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al eliminar el archivo, comunícate con sistemas' } );
  }
};



// Mostrar archivos del servidor
exports.getArchivoServidor = async (req, res) => {
  try{

    // Ruta de la carpeta de servidor
    const PATH = './../../archivos_empleados';

    // Leer archivos de la ruta o del servidor
    let filenames = fs.readdirSync(`${ PATH }`);
    
    // Se crea un array
    const archivos = [];

    // Para cada archivo se hace una insercion al arreglo
    filenames.forEach((file) => {
      archivos.push({ nombre_archivo: file });
    });

    // Respuesta al usuario
    return res.send(archivos);

  }catch( error ){
    res.status( 500 ).send( { message: 'Error al eliminar el fichero', error } );
  }
};