const Zonas = require("../models/zonas.model.js");

exports.all_zonas = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Zonas.all_zonas((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar las zonas"
            });
        else res.send(data);
    });
};

exports.add_zonas = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    Zonas.add_zonas(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la zona"
            })
        else res.status(200).send({ message: `La zona se ha creado correctamente` });
    })
};

exports.update_zona = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Zonas.update_zona(req.params.idzonas, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro la zona con id : ${req.params.idzonas}` });
            } else {
                res.status(500).send({ message: "Error al actualizar la zona con el id : " + req.params.idzonas });
            }
        } else {
            res.status(200).send({ message: `La zona se ha actualizado correctamente.` })
        }
    })
}