const contratos       = require("../models/operaciones/contratos.model.js");
const path            = require("path");
const { v4: uuidv4 }  = require('uuid')
const ffmpegPath      = require('ffmpeg-static');
const ffmpeg          = require('fluent-ffmpeg');
ffmpeg.setFfmpegPath(ffmpegPath);

// Importar librerias
const { jsPDF } = require("jspdf");
const { PDFDocument, degrees, rgb, StandardFonts  } = require('pdf-lib');
const fs = require('fs');


exports.addImagen = (req, res) => {
  let EDFile = req.files.file //EDFile.name
  EDFile.mv(`./../../imagenes-tienda/${EDFile.name}`, err => {
    if (err) return res.status(500).send({ message: err })
    return res.status(200).send({ message: 'File upload' })
  })
};


exports.addArchivo = (req, res) => {
  let EDFile = req.files.file

  var pathToPdf = `../archivos/${EDFile.name}`
  EDFile.mv(`../archivos/${EDFile.name}`, err => {
    if (err) return res.status(500).send({ message: err })
    return res.status(200).send({ message: 'File upload' })
  })
};

exports.addPdf = (req, res) => {
  let EDFile = req.files.file
  var pathToPdf = `./../pdf/${EDFile.name}`
  EDFile.mv(`./../pdf/${EDFile.name}`, err => {
    if (err) return res.status(500).send({ message: err })
    return res.status(200).send({ message: 'File upload' })
  })
};


addImagenContratoTeacher = ( archivo ) => {
  return new Promise((resolve,reject)=>{
    let EDFile = archivo //EDFile.name
    EDFile.mv(`./../../imagenes-contratos/${EDFile.name}`, err => {
      if (err) {
        reject( err )
      }else{
        resolve ( true )
      }
    })
  })
};

exports.crearContraro = async(req, res) => {
  let EDFile = req.files.file
  let { id } = req.params

  // Guardamos la image
  const addImagen = await addImagenContratoTeacher( EDFile ).then(response=> response)

  // Obtenemos los datos del contrato
  const { nombre_completo, direccion, dia, mes, anio, folio }   = await contratos.getContrato( id ).then(response => response)

  // Create a new document and add a new page
  let doc = await PDFDocument.create();
  const helveticaFont = await doc.embedFont(StandardFonts.Helvetica)
  // Generamos la primera Pagina
  let page = doc.addPage();

  // Leemos la imagen
  let img = fs.readFileSync('./../../contratos/contrato1.png');
  // colocamos la imegeb
  img = await doc.embedPng(img);

  // Draw the image on the center of the page
  // Sacamos las medidas
  const { width, height } = img.scale(1);
  // Colocamos la imagen su lugar
  page.drawImage(img, {
    x: page.getWidth() / 2 - width / 4 + 10,
    y: page.getHeight() / 2 - height / 4 + 15,
    width: width / 2.1,
    height: height / 2.1,
  });

  // Escribirmos el texo 
  // Agregar el nombre
  page.drawText(nombre_completo, {
    x: 85,
    y: 721,
    size: 10,
    font: helveticaFont,
  })

  // Agregar la dirección
  page.drawText(direccion, {
    x: 35,
    y: 555,
    size: 10,
    font: helveticaFont,
  })


  /***********************************/
  page = doc.addPage();

  // // Load the image and store it as a Node.js buffer in memory
  imgPage2 = fs.readFileSync('./../../contratos/contrato2.png');
  imgPage2 = await doc.embedPng(imgPage2);

  // // // Draw the image on the center of the page
  
  const { width2, height2 } = imgPage2.scale(1);
  
  page.drawImage(imgPage2, {
    x: page.getWidth() / 2 - width / 4,
    y: page.getHeight() / 2 - height / 4,
    width: width / 2,
    height: height / 2,
  });

  // Agregar la fecha
  page.drawText(`${dia}            ${mes}                   ${anio}`, {  
    x: 280,
    y: 557,
    size: 9,
    font: helveticaFont,
  })

  // Agregar La firma
  // Leemos la firma
  let imagenFirma = fs.readFileSync(`./../../imagenes-contratos/${folio}.png`);
  // colocamos la imegeb
  imagenFirma = await doc.embedPng(imagenFirma);
  const { width3, height3 } = imagenFirma.scale(1);
  let rotate = 0

  if(height3 > width3){
    rotate = -90
  }
  page.drawImage(imagenFirma, {
    x: 350,
    y: 330,
    width: 250,
    height: 110,
    rotate: degrees(rotate),
  });

  try{
    fs.writeFileSync(`./../../contratos/${folio}.pdf`, await doc.save());

    const updateEstatusContrato = await contratos.updateEstatusContrato( id ).then(response => response)

    return res.status(200).send({ message: 'File upload' })
  }catch(err){
    return res.status(500).send({ message: 'File NO upload' })
  }
};

exports.parseAudio = (req, res) => {

  let BASE_IMAGE_PATH = path.resolve(__dirname, "..", "..", "whatsapp-imagenes");


  const uidFile = uuidv4()
  const nombreFileMpeg = uidFile + '.mpeg'
  const nombreFileMp3  = uidFile + '.mp3'


  const fullPathMpeg     = path.join(
    path.relative(__dirname, BASE_IMAGE_PATH),
    nombreFileMpeg
  );

  const fullPathMp3     = path.join(
    path.relative(__dirname, BASE_IMAGE_PATH),
    nombreFileMp3
  );

  console.log( req.body )

  fs.writeFile(fullPathMpeg, req.body, error => {
    if (error) {
      res.status(500).send({ message: error ? error.message : 'Error en el servidor'});
    } else {

      // ruta al archivo de entrada MPEG
      const inputFile = fullPathMpeg;

      // ruta al archivo de salida MP3
      const outputFile = fullPathMp3


      const command = ffmpeg(inputFile)
        .setFfmpegPath(ffmpegPath)
        .noVideo()
        .audioCodec('libmp3lame')
        .output(outputFile);

      // iniciar la conversión
      command.on('end', function() {
        console.log('Finished processing');
        // console.log(`File saved to ${fullPathMpeg}`);
        res.send( nombreFileMp3 );

      }).run()

    }
  });

};