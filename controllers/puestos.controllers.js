const Puestos = require("../models/puestos.model.js");

exports.all_puestos = async (req, res) => {

  try{

    const puestosAll = await Puestos.all_puestos( ).then( response => response )

    res.send( puestosAll )

  }catch( error ){
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

exports.add_puestos = (req, res) => {
    //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Puestos.add_puestos(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el puesto"
      })
    else res.status(200).send({ message: `El puesto se ha creado correctamente` });
  })
};

exports.update_puesto = (req, res) => {

  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }

  Puestos.update_puesto(req.params.idpuestos, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro el puesto con id : ${req.params.idpuestos}` });
      } else {
        res.status(500).send({ message: "Error al actualizar el puesto con el id : " + req.params.idpuestos });
      }
    } else {
      res.status(200).send({ message: `El puesto se ha actualizado correctamente.` })
    }
  })
}

exports.getPuestosDepto = (req, res) => {
  Puestos.getPuestosDepto(req.params.id, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los puestos"
      });
    else res.send(data);
  });
}