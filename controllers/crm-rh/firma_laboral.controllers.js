const firma_laboral = require("../../models/crm-rh/firma_laboral.model.js");

const { agregarArchivo } = require('../../helpers/agregar_archivo.js');
const fs = require('fs');


// AGREGAR FIRMA_LABORAL
exports.addFirma = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }


    // VALIDAR CAMPOS REQUERIDOS
    const { idformulario, propuesta_economica, horario_agendado } = req.body;

    if ( !idformulario ) return res.status( 400 ).send({ message: 'Es necesario el campo idformulario' });


    // VALIDAR SI VIENE FIRMA_LABORAL O LE ASIGNAMOS UNA CADENA VACIA
    horario_agendado = horario_agendado || '';


    // VALIDAR QUE NO SE ENCUENTRE UNA FIRMA_LABORAL AL PROSPECTO
    const firmaEncontrada = await firma_laboral.getPropuestaProspecto( idformulario ).then( response => response );

    if( firmaEncontrada ) return res.status( 403 ).send({ message: 'Ya hay una firma asociada al prospecto' });


    // AGREGAR EXAMEN
    const firma = await firma_laboral.addFirma({ idformulario, propuesta_economica, horario_agendado }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Firma laboral generada correctamente', firma_laboral: firma });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR FIRMA_LABORAL O BIEN ELIMINARLO
exports.updateFirma = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EXISTA FIRMA_LABORAL CON ESE ID
    const firmaEncontrada = await firma_laboral.getFirma( id ).then( response => response );

    if ( !firmaEncontrada ) return res.status( 404 ).send({ message: `La firma con id ${id} no pudo ser encontrada` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...firmaEncontrada };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN FIRMA_LABORAL SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( firmaEncontrada.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const firma = await firma_laboral.updateFirma( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Firma laboral actualizada correctamente', firma_laboral: firma });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER FIRMA_LABORAL POR ID
exports.getFirma = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.params) return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });

    const { id } = req.params;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !id ) return res.status( 400 ).send({ message: 'El id no puede estar vacio' });


    // OBTENER FIRMA_LABORAL
    const firma = await firma_laboral.getFirma( id ).then( response => response );

    if ( !firma ) return res.status( 404 ).send({ message: `La firma laboral con id ${id} no pudo ser encontrada` });


    // RESPUESTA AL USUARIO
    res.send({ message: 'Firma laboral encontrada correctamente', firma_laboral: firma });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER FIRMAS_LABORALES
exports.getFirmas = async (req, res) => {
  try{

    // OBTENER FIRMA_LABORAL
    const firmas = await firma_laboral.getFirmas( ).then( response => response );

    const total = firmas.length;

    // RESPUESTA AL USUARIO
    res.send({ message: 'Firmas laborales encontradas correctamente', total_firmas: total, firmas_laborales: firmas });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// -----SUBIR PROPUESTA ECONOMICA-----


// AGREGAR ARCHIVO
exports.addPropuesta = async (req, res) => {
  try{

    
    // VALIDACIÓN DE QUE HAYA UN ARCHIVO EN EL REQ.FILES
    if( !req.files || !req.files.file ){
      return res.status( 400 ).send({ message : 'El contenido file no puede ir vacio' });
    }
    
    const { file } = req.files;

    // EXTENSIONES VÁLIDAS PARA CARGAR ARCHIVOS PDF
    const extensiones = [ 'pdf', 'PDF' ];


    // RUTA DE INSERCIÓN AL SERVIDOR
    const PATH = './propuesta_economica';

    // AGREGAR EL ARCHIVO
    const archivo = await agregarArchivo( file, extensiones, PATH );

    // VALIDACION DE ARCHIVO
    if( !archivo ) return res.status( 400 ).send({ message : 'El archivo no pudo ser creado' });

    const { nombreUuid } = archivo;

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Archivo agregado correctamente', nombre: nombreUuid });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};



// ELIMINAR EL ARCHIVO
exports.deletePropuesta = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });

    // OBTENER NOMBRE DEL ARCHIVO
    const { nombre_archivo } = req.body;

    if( !nombre_archivo ) return res.status( 400 ).send({ message: 'Es necesario un nombre_archivo' });

    // RUTA DE ELIMINACIÓN DEL SERVIDOR
    const PATH = './propuesta_economica';

    try {
      fs.unlinkSync(`${ PATH }/${ nombre_archivo }`);
      res.send({ message: 'Archivo eliminado correctamente' });
    } catch (error) {
      if( error.code === 'ENOENT' ) 
        return res.status(404).send({ message: 'El archivo no se encuentra en el servidor' });
      throw error;
    }

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al eliminar el archivo, comunícate con sistemas' } );
  }
};



// MOSTRAR ARCHIVOS
exports.getPropuesta = async (req, res) => {
  try{

    // RUTA DE LECTURA AL SERVIDOR
    const PATH = './propuesta_economica';

    // LEER ARCHIVOS DE LA RUTA O SERVIDOR
    let filenames = fs.readdirSync(`${ PATH }`);
    
    const archivos = [];

    filenames.forEach((file) => {
      archivos.push({ nombre_archivo: file });
    });

    return res.send(archivos);

  }catch( error ){
    res.status( 500 ).send( { message: 'Error al eliminar el fichero', error } );
  }
};



// -----SUBIR PROPUESTAS FIRMADAS-----


// AGREGAR ARCHIVO
exports.addPropuestaFirmada = async (req, res) => {
  try{

    
    // VALIDACIÓN DE QUE HAYA UN ARCHIVO EN EL REQ.FILES
    if( !req.files || !req.files.file ){
      return res.status( 400 ).send({ message : 'El contenido file no puede ir vacio' });
    }
    
    const { file } = req.files;

    // EXTENSIONES VÁLIDAS PARA CARGAR ARCHIVOS PDF
    const extensiones = [ 'pdf', 'PDF' ];


    // RUTA DE INSERCIÓN AL SERVIDOR
    const PATH = './propuesta_firmada';

    // AGREGAR EL ARCHIVO
    const archivo = await agregarArchivo( file, extensiones, PATH );

    // VALIDACION DE ARCHIVO
    if( !archivo ) return res.status( 400 ).send({ message : 'El archivo no pudo ser creado' });

    const { nombreUuid } = archivo;

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Archivo agregado correctamente', nombre: nombreUuid });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};



// ELIMINAR EL ARCHIVO
exports.deletePropuestaFirmada = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });

    // OBTENER NOMBRE DEL ARCHIVO
    const { nombre_archivo } = req.body;

    if( !nombre_archivo ) return res.status( 400 ).send({ message: 'Es necesario un nombre_archivo' });

    // RUTA DE ELIMINACIÓN DEL SERVIDOR
    const PATH = './propuesta_firmada';

    try {
      fs.unlinkSync(`${ PATH }/${ nombre_archivo }`);
      res.send({ message: 'Archivo eliminado correctamente' });
    } catch (error) {
      if( error.code === 'ENOENT' ) 
        return res.status(404).send({ message: 'El archivo no se encuentra en el servidor' });
      throw error;
    }

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al eliminar el archivo, comunícate con sistemas' } );
  }
};



// MOSTRAR ARCHIVOS
exports.getPropuestaFirmada = async (req, res) => {
  try{

    // RUTA DE LECTURA AL SERVIDOR
    const PATH = './propuesta_firmada';

    // LEER ARCHIVOS DE LA RUTA O SERVIDOR
    let filenames = fs.readdirSync(`${ PATH }`);
    
    const archivos = [];

    filenames.forEach((file) => {
      archivos.push({ nombre_archivo: file });
    });

    return res.send(archivos);

  }catch( error ){
    res.status( 500 ).send( { message: 'Error al eliminar el fichero', error } );
  }
};



// ----- AGENDA -----


// OBTENER FIRMAS LABORALES POR FECHA
exports.getAgenda = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // VALIDAR QUE VENGAN LOS CAMPOS NECESARIOS
    const { fecha_inicio, fecha_final } = req.body;

    // SE ESPERA EN FORMATO 'YYYY-MM-DD'
    if ( !fecha_inicio || fecha_final ) return res.status( 400 ).send({ message: 'Es necesaria una fecha inicio y fecha final' });


    // AGREGAR HORAS A LAS FECHAS
    const fechaInicio = new Date(fecha_inicio).toISOString().split('T')[0] + ' 00:00:00';
    const fechaFinal = new Date(fecha_final).toISOString().split('T')[0] + ' 23:59:59';


    // OBTENER AGENDA DE FIRMAS LABORALES
    const firmas = await firma_laboral.getAgenda( fechaInicio, fechaFinal ).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Firmas laborales por rango de fechas', firmas });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};