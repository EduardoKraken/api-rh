const reclutar_puestos = require("../../models/crm-rh/reclutar_puestos.model.js");


// AGREGAR RECLUTAR_PUESTOS
exports.addReclutadora = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    const { idreclutadora, idpuestos } = req.body;

    if ( !idreclutadora && !idpuestos ) return res.status( 400 ).send({ message: 'Son necesarios los campos idreclutadora e idpuestos' });

    // AGREGAR RECLUTAR_PUESTOS
    const reclutar_puesto  = await reclutar_puestos.addReclutadora({ idreclutadora, idpuestos }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Reclutar puestos generado correctamente', reclutar_puesto });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR RECLUTAR_PUESTOS O BIEN ELIMINARLO
exports.updateReclutadora = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EXISTA RECLUTAR_PUESTOS CON ESE ID
    const reclutadoraPuestos = await reclutar_puestos.getReclutadora( id ).then( response => response );

    if ( !reclutadoraPuestos ) return res.status( 404 ).send({ message: `La reclutadora de puestos con id ${id} no pudo ser encontrada` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...reclutadoraPuestos };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN RECLUTADORA_PUESTOS SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( reclutadoraPuestos.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const reclutarPuestos = await reclutar_puestos.updateReclutadora( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Reclutadora de puestos actualizada correctamente', reclutarPuestos });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER TODOS LOS RECLUTAR_PUESTOS
exports.getReclutadoras = async (req, res) => {
  try{
    
    // OBTENER TODOS LOS RECLUTAR_PUESTOS
    const reclutadoras  = await reclutar_puestos.getReclutadoras().then( response => response );

    const total = reclutadoras.length;

    // RESPUESTA AL USUARIO
    res.send({ message: 'Reclutadoras obtenidas', total_reclutadoras: total, reclutadoras });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER RECLUTAR_PUESTOS POR ID
exports.getReclutadora = async (req, res) => {
  try{

    const { idreclutadora } = req.params;

    if ( !idreclutadora ) return res.status( 400 ).send({ message: 'El id no puede estar vacio' });

    // OBTENER RECLUTAR_PUESTOS
    const reclutadora  = await reclutar_puestos.getReclutadora( idreclutadora ).then( response => response );

    if ( !reclutadora ) return res.status( 404 ).send({ message: `La reclutadora de puestos con id ${idreclutadora} no pudo ser encontrada` });

    // RESPUESTA AL USUARIO
    res.send({ message: 'La reclutadora de puestos fue encontrada correctamente', reclutadora });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


  // ----- ACCIONES EXTRA -----


// OBTENER RECLUTAR_PUESTOS POR ID_PUESTOS
exports.getReclutadoraPuesto = async (req, res) => {
  try{

    const { idpuesto } = req.params;

    if ( !idpuesto ) return res.status( 400 ).send({ message: 'El idpuesto no puede estar vacio' });

    // OBTENER RECLUTAR_PUESTOS
    const reclutadoraPuesto = await reclutar_puestos.getReclutadoraPuesto( idpuesto ).then( response => response );

    const total = reclutadoraPuesto.length;

    // RESPUESTA AL USUARIO
    res.send({ message: 'Formularios por prospecto asociado a su puesto', total, reclutadoraPuesto });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR PROSPECTO EN CAMPO ACEPTADO
exports.updateProspectoFormulario = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EL ID CORRESPONDA A UN ID
    const formulario = await reclutar_puestos.getPropectoFormulario( id ).then( response => response );

    if ( !formulario ) return res.status( 404 ).send({ message: `EL formulario con id ${id} no pudo ser encontrado` });


    const { aceptado, motivo_rechazo } = req.body;

    if ( !aceptado ) return res.status( 400 ).send({ message: `Es necesario el valor de aceptado` });


    // SI FUE RECHAZADO ES NECESARIO ESCRIBIR UN MOTIVO_RECHAZO
    if( aceptado === 2 ){
      if( !motivo_rechazo ) return res.status( 400 ).send({ message: `Es necesario el valor de motivo_rechazo` });
    } else {
      motivo_rechazo = '';
    }

    // ACTUALIZAR ESTATUS DE ACEPTADO EN BD
    const prospecto = await reclutar_puestos.updateProspectoFormulario({ aceptado, motivo_rechazo }).then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Prospecto actualizado correctamente', prospecto });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


  // ----- AGENDA -----


// AGREGAR AGENDA
exports.addAgenda = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // HORARIO EN FORMATO YYYY/MM/DD
    const { idreclutadora, idformulario, horario } = req.body;

    if ( !idreclutadora && !horario ) return res.status( 400 ).send({ message: 'Son necesarios los campos idreclutadora y horario' });


    // VALIDAR DISPONIBILIDAD
    const noDisponibilidad = await reclutar_puestos.getDisponibilidad({ idreclutadora, horario }).then( response => response );

    if( noDisponibilidad ) return res.status( 403 ).send({ message: 'No hay disponibilidad en el horario requerido' });


    // AGREGAR RECLUTAR_PUESTOS
    const agenda = await reclutar_puestos.addAgenda({ idreclutadora, idformulario, horario }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Agenda generada correctamente', agenda });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR AGENDA O BIEN ELIMINARLO
exports.updateAgenda = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EXISTA AGENDA CON ESE ID
    const reclutadoraAgenda = await reclutar_puestos.getAgenda( id ).then( response => response );

    if ( !reclutadoraAgenda ) return res.status( 404 ).send({ message: `La agenda con id ${id} no pudo ser encontrada` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...reclutadoraAgenda };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN RECLUTADORA_PUESTOS SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( reclutadoraAgenda.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const agenda = await reclutar_puestos.updateAgenda( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Agenda actualizada correctamente', agenda });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR DURACION DE LA LLAMADA
exports.updateAgendaHorario = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EL ID CORRESPONDA A UN ID
    const agenda = await reclutar_puestos.getAgenda( id ).then( response => response );

    if ( !agenda ) return res.status( 404 ).send({ message: `La agenda con id ${id} no pudo ser encontrada` });


    const { duracion } = req.body;

    if ( !duracion ) return res.status( 400 ).send({ message: `Es necesario el valor de duracion` });


    // ACTUALIZAR ESTATUS DE ACEPTADO EN BD
    const agendaDuracion = await reclutar_puestos.updateAgendaHorario({ duracion }).then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Duracion actualizada correctamente', agenda: agendaDuracion });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};