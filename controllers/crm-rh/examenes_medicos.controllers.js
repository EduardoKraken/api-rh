const examenes_medicos = require("../../models/crm-rh/examenes_medicos.model.js");

const { agregarArchivo } = require('../../helpers/agregar_archivo.js');
const fs = require('fs');


// AGREGAR EXAMEN
exports.addExamen = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }


    // VALIDAR CAMPOS REQUERIDOS
    let { idformulario, examen, comentario } = req.body;

    if ( !idformulario ) return res.status( 400 ).send({ message: 'Es necesario el campo idformulario' });


    // VALIDAR SI VIENE EXAMEN O LE ASIGNAMOS UNA CADENA VACIA
    examen = examen || '';


    // VALIDAR QUE NO SE ENCUENTRE UNa EXAMEN IGUAL (NOMBRE)
    const examenEncontrado = await examenes_medicos.getExamenProspecto( idformulario ).then( response => response );

    if( examenEncontrado ) return res.status( 400 ).send({ message: 'Ya hay un examen ligado a ese prospecto' });

    
    // AGREGAR EXAMEN
    let examenNuevo = await examenes_medicos.addExamen({ idformulario, examen, comentario }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Examen médico generado correctamente', examen: examenNuevo });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR EXAMEN O BIEN ELIMINARLO
exports.updateExamen = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EXISTA EXAMEN CON ESE ID
    const examenEncontrado = await examenes_medicos.getExamen( id ).then( response => response );

    if ( !examenEncontrado ) return res.status( 404 ).send({ message: `La investigacion con id ${id} no pudo ser encontrada` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...examenEncontrado };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN EXAMEN_ENCONTRADO SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( examenEncontrado.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const examen = await examenes_medicos.updateExamen( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Examen medico actualizado correctamente', examen });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER EXAMEN POR ID
exports.getExamen = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.params) return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });

    const { id } = req.params;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !id ) return res.status( 400 ).send({ message: 'El id no puede estar vacio' });


    // OBTENER EXAMEN
    const examen  = await examenes_medicos.getExamen( id ).then( response => response );

    if ( !examen ) return res.status( 404 ).send({ message: `El examen medico con id ${id} no pudo ser encontrado` });


    // RESPUESTA AL USUARIO
    res.send({ message: 'Examen medico encontrado correctamente', examen });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER EXAMEN POR ID
exports.getExamenProspecto = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.params) return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });

    const { idprospecto } = req.params;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !idprospecto ) return res.status( 400 ).send({ message: 'El idprospecto no puede estar vacio' });


    // OBTENER EXAMEN
    const examen  = await examenes_medicos.getExamenProspecto( idprospecto ).then( response => response );

    if ( !examen ) return res.status( 404 ).send({ message: `El examen medico con idprospecto ${idprospecto} no pudo ser encontrado` });


    // RESPUESTA AL USUARIO
    res.send({ message: 'Examen medico encontrado correctamente', examen });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// -----SUBIR ARCHIVOS-----


// AGREGAR ARCHIVO
exports.addArchivo = async (req, res) => {
  try{

    
    // VALIDACIÓN DE QUE HAYA UN ARCHIVO EN EL REQ.FILES
    if( !req.files || !req.files.file ){
      return res.status( 400 ).send({ message : 'El contenido file no puede ir vacio' });
    }
    
    const { file } = req.files;

    // EXTENSIONES VÁLIDAS PARA CARGAR ARCHIVOS IMAGENES Y PDF
    const extensiones = [ 'pdf', 'PDF', 'jpg', 'JPG', 'jpeg','JPEG', 'png', 'PNG' ];


    // RUTA DE INSERCIÓN AL SERVIDOR
    const PATH = './examenes_medicos';

    // AGREGAR EL ARCHIVO
    const archivo = await agregarArchivo( file, extensiones, PATH );

    // VALIDACION DE ARCHIVO
    if( !archivo ) return res.status( 400 ).send({ message : 'El archivo no pudo ser creado' });

    const { nombreUuid } = archivo;

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Archivo agregado correctamente', nombre: nombreUuid });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};



// ELIMINAR EL ARCHIVO
exports.deleteArchivo = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });

    // OBTENER NOMBRE DEL ARCHIVO
    const { nombre_archivo } = req.body;

    if( !nombre_archivo ) return res.status( 400 ).send({ message: 'Es necesario un nombre_archivo' });

    // RUTA DE ELIMINACIÓN DEL SERVIDOR
    const PATH = './examenes_medicos';

    try {
      fs.unlinkSync(`${ PATH }/${ nombre_archivo }`);
      res.send({ message: 'Archivo eliminado correctamente' });
    } catch (error) {
      if( error.code === 'ENOENT' ) 
        return res.status(404).send({ message: 'El archivo no se encuentra en el servidor' });
      throw error;
    }

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al eliminar el archivo, comunícate con sistemas' } );
  }
};



// MOSTRAR ARCHIVOS
exports.getArchivos = async (req, res) => {
  try{

    // RUTA DE LECTURA AL SERVIDOR
    const PATH = './examenes_medicos';

    // LEER ARCHIVOS DE LA RUTA O SERVIDOR
    let filenames = fs.readdirSync(`${ PATH }`);
    
    const archivos = [];

    filenames.forEach((file) => {
      archivos.push({ nombre_archivo: file });
    });

    return res.send(archivos);

  }catch( error ){
    res.status( 500 ).send( { message: 'Error al eliminar el fichero', error } );
  }
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Agregado por Angel Rdz

exports.getExamenes = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.params) return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });

    // OBTENER EXAMEN
    const examen  = await examenes_medicos.getExamenes(  ).then( response => response );

    // RESPUESTA AL USUARIO
    res.send(examen);

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};