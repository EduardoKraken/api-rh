const formularios = require("../../models/crm-rh/formularios.model");

// IMPORTACIONES
const fs = require('fs');

const { agregarArchivo } = require('../../helpers/agregar_archivo');


// AGREGAR FORMULARIOS
exports.addFormularios = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // Ver si ya existe algún formulario con ese correo y/o numero 

    const { correo, numero_wa } = req.body

    const existeFormulario = await formularios.existeFormulario( correo, numero_wa ).then( response => response );

    if( existeFormulario ){ return res.status(500).send({ message: 'No se puede guardar el formulario, ya cuentas con un registro, saludos!' }) }

    // AGREGAR ENCARGADA
    const formularioNuevo = await formularios.addFormularios( req.body ).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Formulario generado correctamente', formulario: formularioNuevo });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR FORMULARIOS O BIEN ELIMINARLO
exports.updateFormularios = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EXISTA FORMULARIO CON EL ID PROPORCIONADO
    const formularioEncontrado = await formularios.getFormulario( id ).then( response => response );

    if ( !formularioEncontrado ) return res.status( 404 ).send({ message: `El formulario con id ${id} no pudo ser encontrado` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...formularioEncontrado };

    console.log( formularioEncontrado )

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN FORMULARIO_ENCONTRADO SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( formularioEncontrado.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }

    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const formularioActualizado = await formularios.updateFormularios( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Formulario actualizado correctamente', formulario: formularioActualizado });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER TODAS LOS FORMULARIOS
exports.getFormularios = async (req, res) => {
  try{
    
    // OBTENER TODOS LOS FORMULARIOS
    const formulariosTodos  = await formularios.getFormularios().then( response => response );

    const total = formulariosTodos.length;

    // RESPUESTA AL USUARIO
    res.send({ message: 'Formularios obtenidos', total_formularios: total, formularios: formulariosTodos });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER FORMULARIO
exports.getFormulario = async (req, res) => {
  try{

    const { id } = req.params;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !id ) return res.status( 400 ).send({ message: 'El id no puede estar vacio' });


    // OBTENER FORMULARIO
    const formulario  = await formularios.getFormulario( id ).then( response => response );

    if ( !formulario ) return res.status( 404 ).send({ message: `El formulario con id ${id} no pudo ser encontrado` });


    // RESPUESTA AL USUARIO
    res.send({ message: 'Formulario encontrado correctamente', formulario });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// -----SUBIR ARCHIVOS-----


// AGREGAR ARCHIVO
exports.addArchivo = async (req, res) => {
  try{

    
    // VALIDACIÓN DE QUE HAYA UN ARCHIVO EN EL REQ.FILES
    if( !req.files || !req.files.file ){
      return res.status( 400 ).send({ message : 'El contenido file no puede ir vacio' });
    }
    
    const { file } = req.files;

    // EXTENSIONES VÁLIDAS PARA CARGAR ARCHIVOS CV
    const extensiones = [ 'pdf', 'PDF', 'doc', 'DOC', 'docx','DOCX', 'png', 'jpg', 'jpeg', 'JPEG' ];


    const nombreSplit = file.name.split('.')
    
    // Obtener la extensión del archivo 
    const extensioArchivo = nombreSplit[ nombreSplit.length - 1 ]

    // validar que la extensión del archivo recibido sea valido
    if( !extensiones.includes( extensioArchivo) )
      return res.status( 400 ).send({message: `Hola, la extensión: ${ extensioArchivo } no es valida, solo se aceptan: ${ extensiones }`})

    // RUTA DE INSERCIÓN AL SERVIDOR
    const PATH = './archivos';

    // AGREGAR EL ARCHIVO
    const archivo = await agregarArchivo( file, extensiones, PATH );

    // VALIDACION DE ARCHIVO
    if( !archivo ) return res.status( 400 ).send({ message : 'El archivo no pudo ser creado' });

    const { nombreUuid } = archivo;

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Archivo agregado correctamente', nombre: nombreUuid });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};



// ELIMINAR EL ARCHIVO
exports.deleteArchivo = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });

    // OBTENER NOMBRE DEL ARCHIVO
    const { nombre_archivo } = req.body;

    if( !nombre_archivo ) return res.status( 400 ).send({ message: 'Es necesario un nombre a eliminar' });

    // RUTA DE ELIMINACIÓN DEL SERVIDOR
    const PATH = './archivos';

    try {
      fs.unlinkSync(`${ PATH }/${ nombre_archivo }`);
      res.send({ message: 'Archivo eliminado correctamente' });
    } catch (error) {
      if( error.code === 'ENOENT' ) 
        return res.status(404).send({ message: 'El archivo no se encuentra en el servidor' });
      throw error;
    }

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al eliminar el archivo, comunícate con sistemas' } );
  }
};



// MOSTRAR ARCHIVOS
exports.getArchivos = async (req, res) => {
  try{

    // RUTA DE LECTURA AL SERVIDOR
    const PATH = './archivos';

    // LEER ARCHIVOS DE LA RUTA O SERVIDOR
    let filenames = fs.readdirSync(`${ PATH }`);
    
    const archivos = [];

    filenames.forEach((file) => {
      archivos.push({ nombre_archivo: file });
    });

    return res.send(archivos);

  }catch( error ){
    res.status( 500 ).send( { message: 'Error al eliminar el fichero', error } );
  }
};


// -----VISTAS-----



// OBTENER FORMULARIO POR PUESTOS
exports.getPuestos = async (req, res) => {
  try{

    const { idpuesto } = req.params;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !idpuesto ) return res.status( 400 ).send({ message: 'El idpuesto no puede estar vacio' });


    if ( !idpuesto ) return res.status( 400 ).send({ message: 'Es necesario un idpuesto' });

    // VLIDAR QUE EXISTE PUESTO
    const puestoEncontrado = await formularios.getPuesto({ idpuesto }).then( response => response );
    
    if( !puestoEncontrado ) return res.status( 404 ).send({ message: `El puesto con id ${idpuesto} no existe` });

    const { puesto } = puestoEncontrado;
    

    // OBTENER FORMULARIOS
    const formulario = await formularios.getPuestos({ idpuesto }).then( response => response );


    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Formularios por puesto', puesto, formularios: formulario });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};

exports.getPuestosFormularios = async (req, res) => {
  try{


    // VLIDAR QUE EXISTE PUESTO
    const getPuestosFormularios = await formularios.getPuestosFormularios( ).then( response => response );
    
    // MENSAJE DE RESPUESTA AL USUARIO
    res.send( getPuestosFormularios );

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};

// OBTENER FORMULARIO POR ESTATUS
exports.getEstatus = async (req, res) => {
  try{

    // OBTENER FORMULARIOS
    const formulario = await formularios.getFormularios().then( response => response );
    
    const total = formulario.length;

    const aceptados  = formulario.filter( f => f.aceptado === 2 );
    const rechazados = formulario.filter( f => f.aceptado === 1 );
    const procesando = formulario.filter( f => f.aceptado === 0 );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Formularios por estatus', total, aceptados, rechazados, procesando });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER FORMULARIO POR FECHAS
exports.getFechas = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // VALIDAR QUE VENGAN LOS CAMPOS NECESARIOS
    const { fecha_inicio, fecha_final } = req.body;

    // SE ESPERA EN FORMATO 'YYYY-MM-DD'
    if ( !fecha_inicio || !fecha_final ) return res.status( 400 ).send({ message: 'Es necesaria una fecha inicio y fecha final' });


    // AGREGAR HORAS A LAS FECHAS
    const fechaInicio = new Date(fecha_inicio).toISOString().split('T')[0] + ' 00:00:00';
    const fechaFinal = new Date(fecha_final).toISOString().split('T')[0] + ' 23:59:59';


    // OBTENER FORMULARIOS
    const formulario = await formularios.getFechas( fechaInicio, fechaFinal ).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Formulario por rango de fechas', formulario });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER FORMULARIO POR ENCARGADAS
exports.getEncargada = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    const { idencargada } = req.body;

    if ( !idencargada ) return res.status( 400 ).send({ message: 'Es necesario un campo idencargada' });

    // OBTENER FORMULARIOS
    const formulario = await formularios.getEncargada({ idencargada }).then( response => response );

    const { iderp, ...data } = formulario;

    // OBTENER NOMBRE ENCARGADA
    const nombreEncargada = await formularios.getEncargadaNombre({ iderp }).then( response => response );


    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Formularios por encargada', nombre: nombreEncargada, formularios: data });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};

// OBTENER FORMULARIOS POR ENCARGADAS
exports.getEncargadas = async (req, res) => {
  try{

    // OBTENER FORMULARIOS
    const formularioEncargadas = await formularios.getEncargadas().then( response => response );

    // OBTENER IDERP DE USUARIO
    let idsEncargadas = formularioEncargadas.map( ids => ids.iderp );

    // MAP DE IDSERP UNICOS
    idsEncargadas = idsEncargadas.length ? [...new Set(idsEncargadas)] : [0];


    // OBTENER NOMBRES DE ENCARGADAS
    const nombresEncargadas = await formularios.getEncargadasNombres( idsEncargadas ).then( response => response );


    // INSERTAMOS EL NOMBRE DESDE LA OTRA BASE DE DATOS
    for( const i in formularioEncargadas ){
      const { iderp } = formularioEncargadas[i];
      formularioEncargadas[i]['nombre_encargada'] = nombresEncargadas.filter( n => n.id_usuario === iderp );
    }


    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Formularios por encargada', formularios: formularioEncargadas });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};

exports.getAllMedioVacante = async (req, res) => {
  try{


    // VLIDAR QUE EXISTE PUESTO
    const getAllMedioVacante = await formularios.getAllMedioVacante( ).then( response => response );
    
    // MENSAJE DE RESPUESTA AL USUARIO
    res.send( getAllMedioVacante );

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};