const eventos = require("../../models/crm-rh/eventos.model");


// AGREGAR EVENTOS
exports.addEvento = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    const { tipo_evento, idformulario, idreclutadora, etapa, detalle } = req.body;

    if ( !tipo_evento || !idformulario ) return res.status( 400 ).send({ message: 'Son necesarios los campos tipo_evento y idformulario' });
    
    if ( !idreclutadora || !etapa || !detalle  ) return res.status( 400 ).send({ message: 'Son necesarios los campos idreclutadora, etapa y detalle' });


    // AGREGAR EVENTOS
    const evento = await eventos.addEvento({ tipo_evento, idformulario, idreclutadora, etapa, detalle }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Evento generado correctamente', evento });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR EVENTOS O BIEN ELIMINARLA
exports.updateEvento = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EXISTA EVENTOS CON ESE ID
    const eventoEncontrado = await eventos.getEvento( id ).then( response => response ); {}

    if ( !eventoEncontrado ) return res.status( 404 ).send({ message: `El evento con id ${id} no pudo ser encontrado` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...eventoEncontrado };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN EVENTO_ENCONTRADO SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( eventoEncontrado.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const evento = await eventos.updateEvento( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Evento actualizado correctamente', evento });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER TODOS LOS EVENTOS
exports.getEventos = async (req, res) => {
  try{
    
    // OBTENER EVENTOS
    const evento = await eventos.getEventos().then( response => response );

    const total = evento.length;

    // RESPUESTA AL USUARIO
    res.send({ message: 'Eventos obtenidos', total_eventos: total, eventos: evento });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER EVENTOS
exports.getEvento = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS PARAMETROS HAYAN LLEGADO
    if (!req.params || !req.params.id ) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });
    
    const { id } = req.params;

    // OBTENER EVENTOSES
    const evento  = await eventos.getEvento( id ).then( response => response );

    if ( !evento ) return res.status( 404 ).send({ message: `El evento con id ${id} no pudo ser encontrado` });

    // RESPUESTA AL USUARIO
    res.send({ message: 'Evento encontrado correctamente', evento });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER EVENTOS POR FORMULARIO
exports.getEventosFormulario = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS PARAMETROS HAYAN LLEGADO
    if (!req.params || !req.params.id ) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });
    
    const { id } = req.params;

    // OBTENER EVENTOS
    const evento  = await eventos.getEventosFormulario( id ).then( response => response );

    const total = evento.length;

    // RESPUESTA AL USUARIO
    res.send({ message: 'Eventos por prospecto obtenidos', total_eventos: total, eventos: evento });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};