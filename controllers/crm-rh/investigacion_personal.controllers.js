const investigacion_personal = require("../../models/crm-rh/investigacion_personal.model");


// AGREGAR INVESTIGACION
exports.addInvestigacion = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }


    // VALIDAR CAMPOS REQUERIDOS
    const { investigar } = req.body;

    if ( !investigar ) return res.status( 400 ).send({ message: 'Es necesario el campo investigar' });


    // VALIDAR QUE NO SE ENCUENTRE UNa INVESTIGACION IGUAL (NOMBRE)
    const investigacionEncontrada = await investigacion_personal.getInvestigacionNombre( investigar ).then( response => response );

    if( investigacionEncontrada ) return res.status( 403 ).send({ message: 'Esa investigacion ya se encuentra en uso' });


    // AGREGAR INVESTIGACION
    const investigacion = await investigacion_personal.addInvestigacion({ investigar }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Investigacion generada correctamente', investigacion });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR INVESTIGACION O BIEN ELIMINARLA
exports.updateInvestigacion = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }


    // VALIDAR CAMPOS REQUERIDOS
    const { investigar } = req.body;

    if ( investigar ){
      const investigarNombre = await investigacion_personal.getInvestigacionNombre( investigar ).then( response => response );
      if ( investigarNombre ) return res.status( 403 ).send({ message: 'La investigacion ya se encuentra en uso' });
    }

    
    // VALIDAR QUE EXISTA INVESTIGACION CON ESE ID
    const investigacionEncontrada = await investigacion_personal.getInvestigacion( id ).then( response => response );

    if ( !investigacionEncontrada ) return res.status( 404 ).send({ message: `La investigacion con id ${id} no pudo ser encontrada` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...investigacionEncontrada };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN INVESTIGACION_ENCONTRADA SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( investigacionEncontrada.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const investigacion = await investigacion_personal.updateInvestigacion( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Investigacion actualizada correctamente', investigacion });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER TODAS LAS INVESTIGACIONES
exports.getInvestigaciones = async (req, res) => {
  try{
    
    // OBTENER INVESTIGACIONES
    const investigaciones = await investigacion_personal.getInvestigaciones().then( response => response );

    const total = investigaciones.length;

    // RESPUESTA AL USUARIO
    res.send({ message: 'Investigaciones personales obtenidas', total_investigaciones: total, investigaciones });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER INVESTIGACION POR ID
exports.getInvestigacion = async (req, res) => {
  try{

    const { id } = req.params;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !id ) return res.status( 400 ).send({ message: 'El id no puede estar vacio' });


    // OBTENER INVESTIGACION
    const investigacion  = await investigacion_personal.getInvestigacion( id ).then( response => response );

    if ( !investigacion ) return res.status( 404 ).send({ message: `La investigacion con id ${id} no pudo ser encontrada` });


    // RESPUESTA AL USUARIO
    res.send({ message: 'Investigacion encontrada correctamente', investigacion });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};



// ----- INVESTIGACIONES COMENTARIOS -----


// AGREGAR COMENTARIO A INVESTIGACION
exports.addComentario = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }


    // VALIDAR CAMPOS REQUERIDOS
    const { idreclutadora, idformulario, idinvestigacion_personal, comentarios } = req.body;

    if ( !idreclutadora || !idformulario ) return res.status( 400 ).send({ message: 'Son necesarios los campos idreclutadora y idformulario' });
    
    if ( !idinvestigacion_personal || !comentarios ) return res.status( 400 ).send({ message: 'Son necesarios los campos idinvestigacion_personal y comentarios' });


    // AGREGAR SPEECH
    const investigacionComentario = await investigacion_personal.addComentario({ idpuesto, idformulario, idspeech, comentarios }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Comentario generado correctamente', comentario: investigacionComentario });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR COMENTARIO DE INVESTIGACION O BIEN ELIMINARLO
exports.updateComentario = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EXISTA SPEECH CON ESE ID
    const comentarioEncontrado = await investigacion_personal.getComentario( id ).then( response => response );

    if ( !comentarioEncontrado ) return res.status( 404 ).send({ message: `El comentario con id ${id} no pudo ser encontrado` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...comentarioEncontrado };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN COMENTARIO_ENCONTRADO SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( comentarioEncontrado.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const comentarioActualizado = await investigacion_personal.updateComentario( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Comentario actualizado correctamente', comentario: comentarioActualizado });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER INVESTIGACIONES_COMENTARIOS POR IDFORMULARIO (PROSPECTO)
exports.getProspecto = async (req, res) => {
  try{

    const { idformulario } = req.params;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !idformulario ) return res.status( 400 ).send({ message: 'El idformulario no puede estar vacio' });


    // OBTENER SPEECH
    const comentarios = await investigacion_personal.getProspecto( idformulario ).then( response => response );


    // RESPUESTA AL USUARIO
    res.send({ message: 'Comentarios de prospecto' , comentarios });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};