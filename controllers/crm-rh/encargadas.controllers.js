const encargadas = require("../../models/crm-rh/encargadas.model");
const puestos    = require("../../models/puestos.model.js");


// AGREGAR ENCARGADA
exports.addEncargada = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    const { idpuesto, idusuarios } = req.body;

    if ( !idpuesto && !idusuarios ) return res.status( 400 ).send({ message: 'Son necesarios los campos idpuesto e idusuarios' });

    // AGREGAR ENCARGADA
    const encargada  = await encargadas.addEncargada({ idpuesto, idusuarios }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Encargada generada correctamente', encargada });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR ENCARGADA O BIEN ELIMINARLA
exports.updateEncargada = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // // VALIDAR QUE EXISTA ENCARGADA CON ESE ID
    // const encargadaEncontrada = await encargadas.getEncargada( id ).then( response => response ); {}

    // if ( !encargadaEncontrada ) return res.status( 404 ).send({ message: `La encargada con id ${id} no pudo ser encontrada` });


    // // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    // const datosActualizar = { ...encargadaEncontrada };
    

    // // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN ENCARGADA_ENCONTRADA SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    // for (const campo in req.body) {
    //   if( encargadaEncontrada.hasOwnProperty(campo) ){
    //     datosActualizar[ campo ] = req.body[campo];
    //   }
    // }

    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const encargada = await encargadas.updateEncargada( id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Encargada actualizada correctamente', encargada });
    

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER TODAS LAS ENCARGADAS
exports.getEncargadas = async (req, res) => {
  try{

    // Obtener todos los puestos
    const puestosAll = await puestos.all_puestos( ).then( response => response )

    
    // OBTENER TODAS LOS ENCARGADAS
    const encargadasTodas  = await encargadas.getEncargadas().then( response => response );

    const usuariosTodas  = await encargadas.getUsuarios().then( response => response );

    const total = encargadasTodas.length;


    for (const i in encargadasTodas){

      //Obtenemos el iderp de todos
      const { idusuarios } = encargadasTodas[i]

      //Guardamos en una varible el resultado de la consulta cuando el idusuario y el iderp coincidan (depende del nombre del id)

      let existeUsuario = usuariosTodas.find(el=>el.id_usuario == idusuarios)

      encargadasTodas[i]['usuario'] = existeUsuario ? existeUsuario.nombre_completo : 'Sin nombre'

    }

    for( const i in puestosAll ){

      const { idpuesto } = puestosAll[i]

      puestosAll[i].reclutadoras = encargadasTodas.filter( el => { return el.idpuesto == idpuesto })
    
    }


    // RESPUESTA AL USUARIO
    res.send({ message: 'Encargadas obtenidas', total_encargadas: total, encargadas: puestosAll });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER ENCARGADA
exports.getEncargada = async (req, res) => {
  try{

    const { id } = req.params;

    if ( !id ) return res.status( 400 ).send({ message: 'El id no puede estar vacio' });

    // OBTENER ENCARGADA
    const encargada  = await encargadas.getEncargada( id ).then( response => response );

    if ( !encargada ) return res.status( 404 ).send({ message: `La encargada con id ${id} no pudo ser encontrada` });

    // RESPUESTA AL USUARIO
    res.send({ message: 'Encargada encontrada correctamente', encargada });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Agregado por Angel Rdz

exports.getUsuarios = async (req, res) => {
  try{
    
    // OBTENER TODAS LOS ENCARGADAS
    const usuariosTodas  = await encargadas.getUsuarios().then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Encargadas obtenidas', usuarios: usuariosTodas });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};

exports.getDepartamentos = async (req, res) => {
  try{
    
    // OBTENER TODAS LOS ENCARGADAS
    const departamentosTodas  = await encargadas.getDepartamentos().then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Encargadas obtenidas', departamentos: departamentosTodas });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};