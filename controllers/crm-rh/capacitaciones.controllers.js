const capacitaciones = require("../../models/crm-rh/capacitaciones.model.js");

const { agregarArchivo } = require('../../helpers/agregar_archivo.js');
const fs = require('fs');


// AGREGAR CAPACITACIONES
exports.addCapacitacion = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }


    // VALIDAR CAMPOS REQUERIDOS
    const { idpuesto, actividad } = req.body;

    if ( !idpuesto || actividad ) return res.status( 400 ).send({ message: 'Son necesarios los campos idpuesto y actividad' });


    // VALIDAR QUE NO SE ENCUENTRE UNA CAPACITACION IGUAL (ACTIVIDAD)
    const actividadCapacitacion = await capacitaciones.getActividadPuesto({ idpuesto, actividad }).then( response => response );

    if( actividadCapacitacion ) return res.status( 403 ).send({ message: 'Ya hay una actividad ligada a ese idpuesto' });


    // AGREGAR CAPACITACION
    const capacitacion = await capacitaciones.addCapacitacion({ idpuesto, actividad }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Capacitacion generada correctamente', capacitacion });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR CAPACITACION O BIEN ELIMINARLO
exports.updateCapacitacion = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EXISTA CAPACITACION CON ESE ID
    const capacitacionEncontrada = await capacitaciones.getCapacitacion( id ).then( response => response );

    if ( !capacitacionEncontrada ) return res.status( 404 ).send({ message: `La capacitacion con id ${id} no pudo ser encontrada` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...capacitacionEncontrada };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN CAPACITACION_ENCONTRADO SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( capacitacionEncontrada.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const capacitacion = await capacitaciones.updateCapacitacion( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Capacitacion actualizada correctamente', capacitacion });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER CAPACITACION POR ID
exports.getCapacitacion = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.params || !req.params.id ) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });

    
    const { id } = req.params;

    // OBTENER CAPACITACION
    const capacitacion  = await capacitaciones.getCapacitacion( id ).then( response => response );

    if ( !capacitacion ) return res.status( 404 ).send({ message: `La capacitacion con id ${id} no pudo ser encontrada` });


    // RESPUESTA AL USUARIO
    res.send({ message: 'Capacitacion encontrada correctamente', capacitacion });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER CAPACITACIONES
exports.getCapacitaciones = async (req, res) => {
  try{

    // OBTENER CAPACITACIONES
    const capacitacion  = await capacitaciones.getCapacitaciones( idprospecto ).then( response => response );

    const total = capacitacion.length;

    // RESPUESTA AL USUARIO
    res.send({ message: 'Capacitaciones encontradas', total_capacitaciones: total, capacitaciones: capacitacion });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER CAPACITACIONES POR ID_PUESTO
exports.getCapacitacionesPuesto = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.params || !req.params.id ) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });

    
    const { id } = req.params;

    // OBTENER CAPACITACIONES POR PUESTO
    const capacitacion = await capacitaciones.getCapacitacionesPuesto( id ).then( response => response );

    const total = capacitacion.length;

    // RESPUESTA AL USUARIO
    res.send({ message: 'Capacitaciones por puesto', total_capacitaciones: total, capacitaciones: capacitacion });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};



// ----- CAPACITACIONES COMENTARIOS -----


// AGREGAR COMENTARIO A CAPACITACIONES
exports.addComentario = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }


    // VALIDAR CAMPOS REQUERIDOS
    const { idcapacitaciones, idformulario, comentarios } = req.body;

    if ( !idcapacitaciones || !idformulario || !comentarios ) return res.status( 400 ).send({ message: 'Son necesarios los campos idcapacitaciones y idformulario' });

    // AGREGAR SPEECH
    const comentario = await capacitaciones.addComentario({ idcapacitaciones, idformulario, comentarios }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Comentario generado correctamente', comentario });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR COMENTARIO DE CAPACITACIONES O BIEN ELIMINARLO
exports.updateComentario = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EXISTA SPEECH CON ESE ID
    const comentarioEncontrado = await capacitaciones.getComentario( id ).then( response => response );

    if ( !comentarioEncontrado ) return res.status( 404 ).send({ message: `El comentario con id ${id} no pudo ser encontrado` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...comentarioEncontrado };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN COMENTARIO_ENCONTRADO SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( comentarioEncontrado.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const comentarioActualizado = await capacitaciones.updateComentario( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Comentario actualizado correctamente', comentario: comentarioActualizado });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER CAPACITACIONES_COMENTARIOS POR IDFORMULARIO (PROSPECTO)
exports.getComentariosProspecto = async (req, res) => {
  try{

    
    if (!req.params || !req.params.idformulario ) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });
    
    const { idformulario } = req.params;


    // OBTENER COMENTARIOS
    const comentarios = await capacitaciones.getComentariosProspecto( idformulario ).then( response => response );


    // RESPUESTA AL USUARIO
    res.send({ message: 'Comentarios de prospecto', comentarios });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// -----SUBIR ARCHIVOS-----


// AGREGAR ARCHIVO
exports.addArchivo = async (req, res) => {
  try{

    
    // VALIDACIÓN DE QUE HAYA UN ARCHIVO EN EL REQ.FILES
    if( !req.files || !req.files.file ){
      return res.status( 400 ).send({ message : 'El contenido file no puede ir vacio' });
    }
    
    const { file } = req.files;

    // EXTENSIONES VÁLIDAS PARA CARGAR ARCHIVOS PDF
    const extensiones = [ 'pdf', 'PDF' ];


    // RUTA DE INSERCIÓN AL SERVIDOR
    const PATH = './capacitaciones';

    // AGREGAR EL ARCHIVO
    const archivo = await agregarArchivo( file, extensiones, PATH );

    // VALIDACION DE ARCHIVO
    if( !archivo ) return res.status( 400 ).send({ message : 'El archivo no pudo ser creado' });

    const { nombreUuid } = archivo;

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Archivo agregado correctamente', nombre: nombreUuid });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};



// ELIMINAR EL ARCHIVO
exports.deleteArchivo = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });

    // OBTENER NOMBRE DEL ARCHIVO
    const { nombre_archivo } = req.body;

    if( !nombre_archivo ) return res.status( 400 ).send({ message: 'Es necesario un nombre_archivo' });

    // RUTA DE ELIMINACIÓN DEL SERVIDOR
    const PATH = './capacitaciones';

    try {
      fs.unlinkSync(`${ PATH }/${ nombre_archivo }`);
      res.send({ message: 'Archivo eliminado correctamente' });
    } catch (error) {
      if( error.code === 'ENOENT' ) 
        return res.status(404).send({ message: 'El archivo no se encuentra en el servidor' });
      throw error;
    }

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al eliminar el archivo, comunícate con sistemas' } );
  }
};



// MOSTRAR ARCHIVOS
exports.getArchivos = async (req, res) => {
  try{

    // RUTA DE LECTURA AL SERVIDOR
    const PATH = './capacitaciones';

    // LEER ARCHIVOS DE LA RUTA O SERVIDOR
    let filenames = fs.readdirSync(`${ PATH }`);
    
    const archivos = [];

    filenames.forEach((file) => {
      archivos.push({ nombre_archivo: file });
    });

    return res.send(archivos);

  }catch( error ){
    res.status( 500 ).send( { message: 'Error al eliminar el fichero', error } );
  }
};