const incorporacion_actividades = require("../../models/crm-rh/incorporacion_actividades.model");


// AGREGAR ACTIVIDAD_INCORPORACION
exports.addActividades = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    const { idpuesto, actividad } = req.body;

    if ( !idpuesto && !actividad ) return res.status( 400 ).send({ message: 'Son necesarios los campos idpuesto y actividad' });

    // VALIDAR QUE NO HAYA ACTIVIDAD POR PUESTO REPETIDA
    const actividadEncontrada = await incorporacion_actividades.getActividadPuesto({ idpuesto, actividad }).then( response => response );

    if ( actividadEncontrada ) return res.status( 400 ).send({ message: 'Ya hay una actividad con ese puesto creada' });

    // AGREGAR ACTIVIDAD_INCORPORACION
    const actividadNueva = await incorporacion_actividades.addActividades({ idpuesto, actividad }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Actividad generada correctamente', actividad: actividadNueva });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR ACTIVIDAD_INCORPORACION O BIEN ELIMINARLA
exports.updateActividades = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EXISTA ACTIVIDAD_INCORPORACION CON ESE ID
    const actividadEncontrada = await incorporacion_actividades.getActividad( id ).then( response => response ); {}

    if ( !actividadEncontrada ) return res.status( 404 ).send({ message: `La actividad con id ${id} no pudo ser encontrada` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...actividadEncontrada };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN ACTIVIDAD_ENCONTRADA SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( actividadEncontrada.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const actividad = await incorporacion_actividades.updateActividades( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Actividad actualizada correctamente', actividad });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER TODAS LAS ACTIVIDADES_INCORPORACIONES
exports.getActividades = async (req, res) => {
  try{
    
    // OBTENER TODAS LAS ACTIVIDADES
    const actividades = await incorporacion_actividades.getActividades().then( response => response );

    const total = actividades.length;

    // RESPUESTA AL USUARIO
    res.send({ message: 'Actividades obtenidas', total_actividades: total, actividades });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER ACTIVIDAD_INCORPORACIONES
exports.getActividad = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS PARAMETROS HAYAN LLEGADO
    if (!req.params || !req.params.id ) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });
    
    const { id } = req.params;

    // OBTENER ACTIVIDAD_INCORPORACIONES
    const actividad  = await incorporacion_actividades.getActividad( id ).then( response => response );

    if ( !actividad ) return res.status( 404 ).send({ message: `La actividad con id ${id} no pudo ser encontrada` });

    // RESPUESTA AL USUARIO
    res.send({ message: 'Actividad encontrada correctamente', actividad });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER ACTIVIDADES_INCORPORACIONES POR FORMULARIO
exports.getActividadesPuesto = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS PARAMETROS HAYAN LLEGADO
    if (!req.params || !req.params.id ) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });
    
    const { id } = req.params;

    // OBTENER ACTIVIDAD_INCORPORACIONES
    const actividades  = await incorporacion_actividades.getActividadesPuesto( id ).then( response => response );

    const total = actividades.length;

    // RESPUESTA AL USUARIO
    res.send({ message: 'Actividades por prospecto obtenidas', total_actividades: total, actividades });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};