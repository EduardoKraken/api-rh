const speeches = require("../../models/crm-rh/speeches.model.js");


// AGREGAR SPEECH
exports.addSpeech = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }


    // VALIDAR CAMPOS REQUERIDOS
    const { idpuesto, speech } = req.body;

    if ( !idpuesto || !speech ) return res.status( 400 ).send({ message: 'Son necesarios los campos idpuesto y speech' });


    // VALIDAR QUE NO SE ENCUENTRE UN SPEECH IGUAL (NOMBRE)
    const speechEncontrado = await speeches.getSpeechNombre( speech ).then( response => response );

    if( speechEncontrado ) return res.status( 403 ).send({ message: 'El speech ya se encuentra en uso' });


    // AGREGAR SPEECH
    const speechNuevo = await speeches.addSpeech({ idpuesto, speech }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Speech generado correctamente', speech: speechNuevo });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR SPEECH O BIEN ELIMINARLO
exports.updateSpeech = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }


    // VALIDAR CAMPOS REQUERIDOS
    const { speech } = req.body;

    if ( speech ){
      const speechNombre = await speeches.getSpeechNombre( speech ).then( response => response );
      if ( speechNombre ) return res.status( 403 ).send({ message: 'El speech ya se encuentra en uso' });
    }

    
    // VALIDAR QUE EXISTA SPEECH CON ESE ID
    const speechEncontrado = await speeches.getSpeech( id ).then( response => response );

    if ( !speechEncontrado ) return res.status( 404 ).send({ message: `El Speech con id ${id} no pudo ser encontrado` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...speechEncontrado };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN SPEECH_ENCONTRADO SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( speechEncontrado.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const speechActualizado = await speeches.updateSpeech( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Speech actualizada correctamente', speech: speechActualizado });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER TODOS LOS SPEECHES
exports.getSpeeches = async (req, res) => {
  try{
    
    // OBTENER SPEECHES
    const speechTodos  = await speeches.getSpeeches().then( response => response );

    const total = speechTodos.length;

    // RESPUESTA AL USUARIO
    res.send({ message: 'Speeches obtenidos', total_speech: total, speeches: speechTodos });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER SPEECH POR ID
exports.getSpeech = async (req, res) => {
  try{

    const { id } = req.params;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !id ) return res.status( 400 ).send({ message: 'El id no puede estar vacio' });


    // OBTENER SPEECH
    const speech  = await speech.getSpeech( id ).then( response => response );

    if ( !speech ) return res.status( 404 ).send({ message: `El Speech con id ${id} no pudo ser encontrada` });


    // RESPUESTA AL USUARIO
    res.send({ message: 'Speech encontrado correctamente', speech });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ----- VISTAS EXTRA -----


// OBTENER SPEECHES POR IDPUESTO
exports.getSpeechPuesto = async (req, res) => {
  try{

    const { idpuesto } = req.params;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !idpuesto ) return res.status( 400 ).send({ message: 'El idpuesto no puede estar vacio' });


    // OBTENER SPEECH
    const speech = await speeches.getSpeechPuesto( idpuesto ).then( response => response );

    const total = speech.length;

    const { puesto, ...speechData } = speech;

    // RESPUESTA AL USUARIO
    res.send({ message: `Speech por puesto ${ puesto }` , total, speeches: speechData });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ----- SPEECH COMENTARIOS -----


// AGREGAR COMENTARIO A SPEECH
exports.addComentario = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }


    // VALIDAR CAMPOS REQUERIDOS
    const { idreclutadora, idformulario, idspeech, comentarios } = req.body;

    if ( !idreclutadora || !idformulario ) return res.status( 400 ).send({ message: 'Son necesarios los campos idreclutadora y idformulario' });
    
    if ( !idspeech || !comentarios ) return res.status( 400 ).send({ message: 'Son necesarios los campos idspeech y comentarios' });


    // AGREGAR SPEECH
    const speechComentarios = await speeches.addComentario({ idpuesto, idformulario, idspeech, comentarios }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Comentario generado correctamente', comentario: speechComentarios });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR COMENTARIO DE SPEECH O BIEN ELIMINARLO
exports.updateComentario = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EXISTA SPEECH CON ESE ID
    const comentarioEncontrado = await speeches.getComentario( id ).then( response => response );

    if ( !comentarioEncontrado ) return res.status( 404 ).send({ message: `El comentario con id ${id} no pudo ser encontrado` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...comentarioEncontrado };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN COMENTARIO_ENCONTRADO SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( comentarioEncontrado.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const comentarioActualizado = await speeches.updateComentario( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Comentario actualizado correctamente', comentario: comentarioActualizado });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER SPEECHES_COMENTARIOS POR IDFORMULARIO (PROSPECTO)
exports.getProspecto = async (req, res) => {
  try{

    const { idformulario } = req.params;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !idformulario ) return res.status( 400 ).send({ message: 'El idformulario no puede estar vacio' });


    // OBTENER SPEECH
    const comentarios = await speeches.getProspecto( idformulario ).then( response => response );


    // RESPUESTA AL USUARIO
    res.send({ message: 'Comentarios de prospecto' , comentarios });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};