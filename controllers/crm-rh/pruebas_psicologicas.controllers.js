const pruebas_psicologicas = require("../../models/crm-rh/pruebas_psicologicas.model.js");

const { agregarArchivo } = require("../../helpers/agregar_archivo.js");
const fs = require('fs');


// AGREGAR PRUEBA_PSICOLOGICA
exports.addPrueba = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }


    // VALIDAR CAMPOS REQUERIDOS
    const { idpuesto, prueba_psicologica } = req.body;

    if ( !idpuesto ) return res.status( 400 ).send({ message: 'Es necesario el campo idpuesto' });


    // AGREGAR SPEECH
    const prueba = await pruebas_psicologicas.addPrueba({ idpuesto, prueba_psicologica }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Prueba psicologica generada correctamente', prueba });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR PRUEBA_PSICOLOGICA O BIEN ELIMINARLA
exports.updatePrueba = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EXISTA SPEECH CON ESE ID
    const pruebaEncontrada = await pruebas_psicologicas.getPrueba( id ).then( response => response );

    if ( !pruebaEncontrada ) return res.status( 404 ).send({ message: `La prueba con id ${id} no pudo ser encontrada` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...pruebaEncontrada };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN SPEECH_ENCONTRADO SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( pruebaEncontrada.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const prueba = await pruebas_psicologicas.updatePrueba( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Prueba psicologica actualizada correctamente', prueba });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER TODOS LAS PRUEBAS_PSICOLOGICAS
exports.getPruebas = async (req, res) => {
  try{
    
    // OBTENER PRUEBAS_TECNICA
    const pruebasTodas = await pruebas_psicologicas.getPruebas().then( response => response );

    const total = pruebasTodas.length;

    // RESPUESTA AL USUARIO
    res.send({ message: 'Pruebas psicologicas obtenidas', total_pruebas: total, pruebas: pruebasTodas });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER PRUEBA_PSICOLOGICA POR ID
exports.getPrueba = async (req, res) => {
  try{

    const { id } = req.params;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !id ) return res.status( 400 ).send({ message: 'El id no puede estar vacio' });


    // OBTENER PRUEBA_PSICOLOGICA
    const prueba  = await pruebas_psicologicas.getPrueba( id ).then( response => response );

    if ( !prueba ) return res.status( 404 ).send({ message: `La prueba tecnica con id ${id} no pudo ser encontrada` });


    // RESPUESTA AL USUARIO
    res.send({ message: 'Prueba psicologica encontrada correctamente', prueba });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER PRUEBA_PSICOLOGICA POR IDPUESTO
exports.getPruebaPuesto = async (req, res) => {
  try{

    const { idpuesto } = req.params;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !idpuesto ) return res.status( 400 ).send({ message: 'El idpuesto no puede estar vacio' });


    // OBTENER SPEECH
    const prueba = await pruebas_psicologicas.getPruebaPuesto( idpuesto ).then( response => response );


    // RESPUESTA AL USUARIO
    res.send({ message: 'Prueba psicologica por puesto encontrada correctamente', prueba });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ----- SUBIR PRUEBAS -----


// AGREGAR ARCHIVO PRUEBA
exports.addArchivo = async (req, res) => {
  try{

    
    // VALIDACIÓN DE QUE HAYA UN ARCHIVO EN EL REQ.FILES
    if( !req.files || !req.files.file ){
      return res.status( 400 ).send({ message : 'El contenido file no puede ir vacio' });
    }
    
    const { file } = req.files;

    // EXTENSIONES VÁLIDAS PARA CARGAR ARCHIVOS CV
    const extensiones = [ 'pdf', 'PDF' ];


    // RUTA DE INSERCIÓN AL SERVIDOR
    const PATH = './pruebas_psicologicas';

    // AGREGAR EL ARCHIVO
    const archivo = await agregarArchivo( file, extensiones, PATH );

    // VALIDACION DE ARCHIVO
    if( !archivo ) return res.status( 400 ).send({ message : 'El archivo no pudo ser creado' });

    const { nombreUuid } = archivo;

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Archivo agregado correctamente', nombre: nombreUuid });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};



// ELIMINAR EL ARCHIVO PRUEBA
exports.deleteArchivo = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });

    // OBTENER NOMBRE DEL ARCHIVO
    const { nombre_archivo } = req.body;

    if( !nombre_archivo ) return res.status( 400 ).send({ message: 'Es necesario un nombre a eliminar' });

    // RUTA DE ELIMINACIÓN DEL SERVIDOR
    const PATH = './pruebas_psicologicas';

    try {
      fs.unlinkSync(`${ PATH }/${ nombre_archivo }`);
      res.send({ message: 'Archivo eliminado correctamente' });
    } catch (error) {
      if( error.code === 'ENOENT' ) 
        return res.status(404).send({ message: 'El archivo no se encuentra en el servidor' });
      throw error;
    }

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al eliminar el archivo, comunícate con sistemas' } );
  }
};



// MOSTRAR ARCHIVOS PRUEBA
exports.getArchivos = async (req, res) => {
  try{

    // RUTA DE LECTURA AL SERVIDOR
    const PATH = './pruebas_psicologicas';

    // LEER ARCHIVOS DE LA RUTA O SERVIDOR
    let filenames = fs.readdirSync(`${ PATH }`);
    
    const archivos = [];

    filenames.forEach((file) => {
      archivos.push({ nombre_archivo: file });
    });

    return res.send(archivos);

  }catch( error ){
    res.status( 500 ).send( { message: 'Error al obtener el fichero', error } );
  }
};



// ----- AGENDA -----


// AGREGAR AGENDA
exports.addAgenda = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // HORARIO EN FORMATO YYYY/MM/DD
    const { idreclutadora, idformulario, idprueba_psicologica, horario_agendado } = req.body;

    if ( !idreclutadora && !idformulario ) return res.status( 400 ).send({ message: 'Son necesarios los campos idreclutadora y idformulario' });
    
    if ( !idprueba_psicologica && !horario_agendado ) return res.status( 400 ).send({ message: 'Son necesarios los campos idprueba_psicologica y horario_agendado' });


    // OBTENER HORARIOS DE RECLUTADORA
    const disponibilidad = await pruebas_psicologicas.getAgendaReclutadora({ idreclutadora }).then( response => response );


    // VALIDAR DISPONIBILIDAD POR HORARIO
    if (horarios.includes( disponibilidad.horario_agendado )) return res.status( 400 ).send({ message: 'No hay disponibilidad en el horario requerido' });


    // AGREGAR AGENDA
    const agenda = await pruebas_psicologicas.addAgenda({ idreclutadora, idformulario, idprueba_psicologica, horario_agendado }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Agenda generada correctamente', agenda });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR AGENDA O BIEN ELIMINARLO
exports.updateAgenda = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EXISTA AGENDA CON ESE ID
    const reclutadoraAgenda = await pruebas_psicologicas.getAgenda( id ).then( response => response );

    if ( !reclutadoraAgenda ) return res.status( 404 ).send({ message: `La agenda con id ${id} no pudo ser encontrada` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...reclutadoraAgenda };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN RECLUTADORA_PUESTOS SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( reclutadoraAgenda.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const agenda = await pruebas_psicologicas.updateAgenda( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Agenda actualizada correctamente', agenda });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER AGENDA POR RECLUTADORA
exports.getAgendaReclutadora = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.params) return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });

    const { id } = req.params;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !id ) return res.status( 400 ).send({ message: 'El id no puede estar vacio' });

    // OBTENER SPEECH
    const agenda = await pruebas_psicologicas.getAgendaReclutadora( id ).then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Agenda por reclutadora encontrada correctamente', agenda });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};



// ----- PRUEBAS PSICOLOGICAS NOTAS -----


// AGREGAR NOTA
exports.addNota = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }


    // VALIDAR CAMPOS REQUERIDOS
    const { idprueba_psicologica_agenda, notas } = req.body;

    if ( !idprueba_psicologica_agenda || !notas ) return res.status( 400 ).send({ message: 'Son necesarios los campos idprueba_psicologica_agenda y notas' });


    // AGREGAR NOTA
    const pruebaNota = await pruebas_psicologicas.addNota({ idprueba_psicologica_agenda, notas }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Nota generada correctamente', nota: pruebaNota });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR NOTA
exports.updateNota = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EXISTA NOTA CON ESE ID
    const notaEncontrada = await pruebas_psicologicas.getNota( id ).then( response => response );

    if ( !notaEncontrada ) return res.status( 404 ).send({ message: `La nota con id ${id} no pudo ser encontrada` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...notaEncontrada };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN COMENTARIO_ENCONTRADO SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( notaEncontrada.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const notaActualizada = await pruebas_psicologicas.updateNota( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Comentario actualizado correctamente', nota: notaActualizada });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER NOTAS POR IDAGENDA
exports.getNotasAgenda = async (req, res) => {
  try{

    if (!req.params) return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });

    const { idprueba_psicologica_agenda } = req.params;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !idprueba_psicologica_agenda ) return res.status( 400 ).send({ message: 'El idprueba_psicologica_agenda no puede estar vacio' });


    // OBTENER NOTAS
    const notas = await pruebas_psicologicas.getNotasAgenda( idprueba_psicologica_agenda ).then( response => response );


    // RESPUESTA AL USUARIO
    res.send({ message: 'Notas de agenda', notas });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};