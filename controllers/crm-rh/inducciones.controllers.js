const inducciones = require("../../models/crm-rh/inducciones.model.js");

const { agregarArchivo } = require('../../helpers/agregar_archivo.js');
const fs = require('fs');


// AGREGAR INDUCCION
exports.addInduccion = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }


    // VALIDAR CAMPOS REQUERIDOS
    const { idpuesto, actividad } = req.body;

    if ( !idpuesto || actividad ) return res.status( 400 ).send({ message: 'Son necesarios los campos idpuesto y actividad' });


    // VALIDAR QUE NO SE ENCUENTRE UNA INDUCCION IGUAL (ACTIVIDAD)
    const actividadInduccion = await inducciones.getActividadPuesto({ idpuesto, actividad }).then( response => response );

    if( actividadInduccion ) return res.status( 403 ).send({ message: 'Ya hay una actividad ligada a ese idpuesto' });


    // AGREGAR INDUCCION
    const induccion = await inducciones.addInduccion({ idpuesto, actividad }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Induccion generada correctamente', induccion });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR INDUCCION O BIEN ELIMINARLO
exports.updateInduccion = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !req.params || !req.params.id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    
    // VALIDAR QUE EXISTA INDUCCION CON ESE ID
    const induccionEncontrada = await inducciones.getInduccion( id ).then( response => response );

    if ( !induccionEncontrada ) return res.status( 404 ).send({ message: `La induccion con id ${id} no pudo ser encontrada` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...induccionEncontrada };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN INDUCCION_ENCONTRADA SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( induccionEncontrada.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const induccion = await inducciones.updateInduccion( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Induccion actualizada correctamente', induccion });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER INDUCCION POR ID
exports.getInduccion = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if ( !req.params || !req.params.id ) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });

    
    const { id } = req.params;

    // OBTENER INDUCCION
    const induccion  = await inducciones.getInduccion( id ).then( response => response );

    if ( !induccion ) return res.status( 404 ).send({ message: `La induccion con id ${id} no pudo ser encontrada` });


    // RESPUESTA AL USUARIO
    res.send({ message: 'Induccion encontrada correctamente', induccion });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER INDUCCION
exports.getInducciones = async (req, res) => {
  try{

    // OBTENER INDUCCION
    const induccion  = await inducciones.getInducciones( idprospecto ).then( response => response );

    const total = induccion.length;

    // RESPUESTA AL USUARIO
    res.send({ message: 'Inducciones encontradas', total_inducciones: total, inducciones: induccion });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER INDUCCION POR ID_PUESTO
exports.getInduccionesPuesto = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.params || !req.params.id ) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });

    
    const { id } = req.params;

    // OBTENER INDUCCION POR PUESTO
    const induccion = await inducciones.getInduccionesPuesto( id ).then( response => response );

    const total = induccion.length;

    // RESPUESTA AL USUARIO
    res.send({ message: 'Inducciones por puesto', total_inducciones: total, inducciones: induccion });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};



// ----- INDUCCION COMENTARIOS -----


// AGREGAR COMENTARIO A INDUCCION
exports.addComentario = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }


    // VALIDAR CAMPOS REQUERIDOS
    const { idinducciones, idformulario, comentarios } = req.body;

    if ( !idinducciones || !idformulario || !comentarios ) return res.status( 400 ).send({ message: 'Son necesarios los campos idinducciones y idformulario' });

    // AGREGAR INDUCCION
    const comentario = await inducciones.addComentario({ idinducciones, idformulario, comentarios }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Comentario generado correctamente', comentario });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR INDUCCION DE CAPACITACIONES O BIEN ELIMINARLO
exports.updateComentario = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EXISTA SPEECH CON ESE ID
    const comentarioEncontrado = await inducciones.getComentario( id ).then( response => response );

    if ( !comentarioEncontrado ) return res.status( 404 ).send({ message: `El comentario con id ${id} no pudo ser encontrado` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...comentarioEncontrado };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN COMENTARIO_ENCONTRADO SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( comentarioEncontrado.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const comentarioActualizado = await inducciones.updateComentario( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Comentario actualizado correctamente', comentario: comentarioActualizado });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER INDUCCIONES_COMENTARIOS POR IDFORMULARIO (PROSPECTO)
exports.getComentariosProspecto = async (req, res) => {
  try{
    
    if (!req.params || !req.params.idformulario ) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });
    
    const { idformulario } = req.params;

    // OBTENER COMENTARIOS
    const comentarios = await inducciones.getComentariosProspecto( idformulario ).then( response => response );


    // RESPUESTA AL USUARIO
    res.send({ message: 'Comentarios de prospecto', comentarios });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// -----SUBIR ARCHIVOS-----


// AGREGAR ARCHIVO
exports.addArchivo = async (req, res) => {
  try{

    
    // VALIDACIÓN DE QUE HAYA UN ARCHIVO EN EL REQ.FILES
    if( !req.files || !req.files.file ){
      return res.status( 400 ).send({ message : 'El contenido file no puede ir vacio' });
    }
    
    const { file } = req.files;

    // EXTENSIONES VÁLIDAS PARA CARGAR ARCHIVOS PDF
    const extensiones = [ 'pdf', 'PDF' ];


    // RUTA DE INSERCIÓN AL SERVIDOR
    const PATH = './inducciones';

    // AGREGAR EL ARCHIVO
    const archivo = await agregarArchivo( file, extensiones, PATH );

    // VALIDACION DE ARCHIVO
    if( !archivo ) return res.status( 400 ).send({ message : 'El archivo no pudo ser creado' });

    const { nombreUuid } = archivo;

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Archivo agregado correctamente', nombre: nombreUuid });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};



// ELIMINAR EL ARCHIVO
exports.deleteArchivo = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body) return res.status( 400 ).send({ message: 'El contenido no puede estar vacio' });

    // OBTENER NOMBRE DEL ARCHIVO
    const { nombre_archivo } = req.body;

    if( !nombre_archivo ) return res.status( 400 ).send({ message: 'Es necesario un nombre_archivo' });

    // RUTA DE ELIMINACIÓN DEL SERVIDOR
    const PATH = './inducciones';

    try {
      fs.unlinkSync(`${ PATH }/${ nombre_archivo }`);
      res.send({ message: 'Archivo eliminado correctamente' });
    } catch (error) {
      if( error.code === 'ENOENT' ) 
        return res.status(404).send({ message: 'El archivo no se encuentra en el servidor' });
      throw error;
    }

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al eliminar el archivo, comunícate con sistemas' } );
  }
};



// MOSTRAR ARCHIVOS
exports.getArchivos = async (req, res) => {
  try{

    // RUTA DE LECTURA AL SERVIDOR
    const PATH = './inducciones';

    // LEER ARCHIVOS DE LA RUTA O SERVIDOR
    let filenames = fs.readdirSync(`${ PATH }`);
    
    const archivos = [];

    filenames.forEach((file) => {
      archivos.push({ nombre_archivo: file });
    });

    return res.send(archivos);

  }catch( error ){
    res.status( 500 ).send( { message: 'Error al eliminar el fichero', error } );
  }
};