const llamadas_tecnicas = require("../../models/crm-rh/llamadas_tecnicas.model.js");


// AGREGAR LLAMADA_TECNICA
exports.addLlamada = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }


    // VALIDAR CAMPOS REQUERIDOS
    const { idformulario, liga, horario_agendado } = req.body;

    if ( !idformulario ) return res.status( 400 ).send({ message: 'Es necesario el campo idformulario' });


    // VALIDAR SI VIENE HORARIO_AGENDADO O LE ASIGNAMOS UNA CADENA VACIA
    horario_agendado = horario_agendado || '';


    // VALIDAR QUE NO SE ENCUENTRE UNA LLAMADA_TECNICA AL PROSPECTO
    const llamadaEncontrada = await llamadas_tecnicas.getLlamadaProspecto( idformulario ).then( response => response );

    if( llamadaEncontrada ) return res.status( 403 ).send({ message: 'Ya hay una llamada asociada al prospecto' });


    // AGREGAR EXAMEN
    const llamada = await llamadas_tecnicas.addLlamada({ idformulario, liga, horario_agendado }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Llamada tecnica generada correctamente', llamada_tecnica: llamada });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR LLAMADA_TECNICA O BIEN ELIMINARLO
exports.updateLlamada = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EXISTA LLAMADA_TECNICA CON ESE ID
    const llamadaEncontrada = await llamadas_tecnicas.getLlamada( id ).then( response => response );

    if ( !llamadaEncontrada ) return res.status( 404 ).send({ message: `La llamada con id ${id} no pudo ser encontrada` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...llamadaEncontrada };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN LLAMADA_TECNICA SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( llamadaEncontrada.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const llamada = await llamadas_tecnicas.updateLlamada( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Llamada tecnica actualizada correctamente', llamada_tecnica: llamada });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER LLAMADA_TECNICA POR ID
exports.getLlamada = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.params|| Object.keys(req.params).length === 0){
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    } 

    const { id } = req.params;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !id ) return res.status( 400 ).send({ message: 'El id no puede estar vacio' });


    // OBTENER LLAMADA_TECNICA
    const llamada = await llamadas_tecnicas.getLlamada( id ).then( response => response );

    if ( !llamada ) return res.status( 404 ).send({ message: `La llamada tecnica con id ${id} no pudo ser encontrada` });


    // RESPUESTA AL USUARIO
    res.send({ message: 'Llamada tecnica encontrada correctamente', llamadas_tecnicas: llamada });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER LLAMADAS_TECNICAS POR FECHAS
exports.getFecha = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // VALIDAR QUE VENGAN LOS CAMPOS NECESARIOS
    const { fecha_inicio, fecha_final } = req.body;

    // SE ESPERA EN FORMATO 'YYYY-MM-DD'
    if ( !fecha_inicio ) return res.status( 400 ).send({ message: 'Es necesaria una fecha inicio' });


    // SI NO VIENE FECHA FINAL ASIGNARLE EL DIA DE HOY EN FORMATO 'YYYY-MM-DD'
    const fechaFinal = fecha_final ? fecha_final : new Date().toISOString().slice(0,10);


    // AGREGAR HORAS A LAS FECHAS
    const fechaInicio = new Date(fecha_inicio).toISOString().split('T')[0] + ' 00:00:00';
    const fechaFinalCompleta = new Date(fechaFinal).toISOString().split('T')[0] + ' 23:59:59';


    // OBTENER LLAMADAS_TECNICAS
    const llamadas = await llamadas_tecnicas.getFecha( fechaInicio, fechaFinalCompleta ).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Llamadas por rango de fechas', llamadas_tecnicas: llamadas });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ----- PARTICIPANTES -----


// AGREGAR PARTICIPANTES
exports.addParticipantes = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // OBTENER CAMPOS NECESARIOS DEL BODY
    const { idllamadas_tecnicas, idempleado } = req.body;

    if ( !idllamadas_tecnicas || !idempleado ) return res.status( 400 ).send({ message: 'Son necesarios los campos idllamadas_tecnicas y idempleado' });


    // OBTENER HORARIOS DE EMPLEADO
    const disponibilidad = await llamadas_tecnicas.getParticipantesLlamada({ idempleado }).then( response => response );


    // VALIDAR DISPONIBILIDAD POR HORARIO
    if (disponibilidad.some(item => item.horario_agendado === horario_agendado)){
      return res.status( 400 ).send({ message: 'No hay disponibilidad en el horario requerido' });
    }


    // AGREGAR PARTICIPANTE
    const participante = await llamadas_tecnicas.addParticipantes({ idllamadas_tecnicas, idempleado }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Participante agregado correctamente', participante });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR AGENDA O BIEN ELIMINARLO
exports.updateParticipante = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    
    // VALIDAR QUE EXISTA AGENDA CON ESE ID
    const participanteEncontrado = await llamadas_tecnicas.getParticipante( id ).then( response => response );

    if ( !participanteEncontrado ) return res.status( 404 ).send({ message: `Participante de llamada con id ${id} no pudo ser encontrado` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...participanteEncontrado };


    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN PARTICIPANTE_ENCONTRADO SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( participanteEncontrado.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const participante = await llamadas_tecnicas.updateParticipante( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Participante actualizado correctamente', participante });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER PARTICIPANTES POR ID_LLAMADAS_PARTICIPANTES
exports.getParticipanteLLamada = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // VALIDAR CAMPOS REQUERIDOS
    if ( !id ) return res.status( 400 ).send({ message: 'El id no puede estar vacio' });

    // OBTENER SPEECH
    const participanteLlamada = await llamadas_tecnicas.getParticipanteLLamada( id ).then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Participante de llamada tecnica encontrado correctamente', participanteLlamada });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER PARTICIPANTES POR ID_LLAMADA_TECNICA
exports.getParticipantesLlamada = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // OBTENER CAMPOS NECESARIOS DEL BODY
    const { idllamadas_tecnicas } = req.body;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !idllamadas_tecnicas ) return res.status( 400 ).send({ message: 'El campo idllamadas_tecnicas no puede estar vacio' });

    // OBTENER SPEECH
    const participantes = await llamadas_tecnicas.getParticipantesLlamada( idllamadas_tecnicas ).then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Participantes de llamada tecnica encontrados correctamente', participantes });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER LLAMADAS POR IDEMPLEADO
exports.getParticipantesAgenda = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // OBTENER CAMPOS NECESARIOS DEL BODY
    const { idempleado } = req.body;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !idempleado ) return res.status( 400 ).send({ message: 'El campo idempleado no puede estar vacio' });

    // OBTENER SPEECH
    const participantes = await llamadas_tecnicas.getParticipantesAgenda( idempleado ).then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Participantes de llamada tecnica encontrados correctamente', participantes });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};