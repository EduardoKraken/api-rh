const publicidades = require("../../models/crm-rh/publicidades.model.js");


// AGREGAR PUBLICIDAD
exports.addPublicidad = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // VALIDAR CAMPOS REQUERIDOS
    const { publicidad } = req.body;

    if ( !publicidad ) return res.status( 400 ).send({ message: 'Es necesario el campo publicidad' });


    // VALIDAR QUE NO SE ENCUENTRE UNA PUBLICIDAD IGUAL (NOMBRE)
    const publicidadEncontrada = await publicidades.getPublicidadNombre( publicidad ).then( response => response );

    if( publicidadEncontrada ) return res.status( 403 ).send({ message: 'La publicidad ya se encuentra en uso' });


    // AGREGAR ENCARGADA
    const publicidadNueva = await publicidades.addPublicidad({ publicidad }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Publicidad generada correctamente', publicidad: publicidadNueva });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR PUBLICIDAD O BIEN ELIMINARLA
exports.updatePublicidad = async (req, res) => {
  try{

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }


    // VALIDAR CAMPOS REQUERIDOS
    const { publicidad } = req.body;

    // if ( publicidad ){
    //   const publicidadNombre = await publicidades.getPublicidadNombre( publicidad ).then( response => response );
    //   if ( publicidadNombre ) return res.status( 403 ).send({ message: 'La publicidad ya se encuentra en uso' });
    // }

    
    // VALIDAR QUE EXISTA PUBLICIDAD CON ESE ID
    const publicidadEncontrada = await publicidades.getPublicidad( id ).then( response => response );

    if ( !publicidadEncontrada ) return res.status( 404 ).send({ message: `La publicidad con id ${id} no pudo ser encontrada` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...publicidadEncontrada };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN PUBLICIDAD_ENCONTRADA SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( publicidadEncontrada.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const publicidadActualizada = await publicidades.updatePublicidad( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Publicidad actualizada correctamente', publicidad: publicidadActualizada });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER TODAS LAS PUBLICIDADES

exports.getPublicidades = async (req, res) => {
  try{
    
    // OBTENER TODAS LOS ENCARGADAS
    const publicidadesTodas  = await publicidades.getPublicidades().then( response => response );

    const total = publicidadesTodas.length;


    // RESPUESTA AL USUARIO
    res.send({ message: 'Publicidades obtenidas', total_publicidades: total, publicidades: publicidadesTodas });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};



// OBTENER PUBLICIDAD
exports.getPublicidad = async (req, res) => {
  try{

    const { id } = req.params;

    // VALIDAR CAMPOS REQUERIDOS
    if ( !id ) return res.status( 400 ).send({ message: 'El id no puede estar vacio' });


    // OBTENER PUBLICIDAD
    const publicidad  = await publicidades.getPublicidad( id ).then( response => response );

    if ( !publicidad ) return res.status( 404 ).send({ message: `La publicidad con id ${id} no pudo ser encontrada` });


    // RESPUESTA AL USUARIO
    res.send({ message: 'Publicidad encontrada correctamente', publicidad });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


