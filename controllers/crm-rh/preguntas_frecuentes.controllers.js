const preguntas_frecuentes = require("../../models/crm-rh/preguntas_frecuentes.model.js");


// AGREGAR PREGUNTA
exports.addPregunta = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // VALIDAR CAMPOS REQUERIDOS
    const { pregunta } = req.body;

    if ( !pregunta ) return res.status( 400 ).send({ message: 'Es necesario el campo pregunta' });


    // VALIDAR QUE NO SE ENCUENTRE UNA PREGUNTA IGUAL (NOMBRE)
    const preguntaEncontrada = await preguntas_frecuentes.getpreguntaNombre({ pregunta }).then( response => response );

    if( preguntaEncontrada ) return res.status( 403 ).send({ message: 'La pregunta ya se encuentra en uso' });

    // AGREGAR PREGUNTA
    const preguntaNueva = await preguntas_frecuentes.addPregunta({ pregunta }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Pregunta generada correctamente', pregunta: preguntaNueva });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR PREGUNTA O BIEN ELIMINARLA
exports.updatePregunta = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !req.params.id) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;

    
    // VALIDAR QUE EXISTA PREGUNTA CON ESE ID
    const preguntaEncontrada = await preguntas_frecuentes.getPregunta( id ).then( response => response );

    if ( !preguntaEncontrada ) return res.status( 404 ).send({ message: `La pregunta con id ${id} no pudo ser encontrada` });


    // OBJETO PARA INSERTAR LOS CAMPOS VÁLIDOS
    const datosActualizar = { ...preguntaEncontrada };

    // SI EL CAMPO POROPORCIONADO DEL BODY EXISTE EN PREGUNTA_ENCONTRADA SE AGREGA AL OBJETO CON SU RESPECTIVO VALOR
    for (const campo in req.body) {
      if( preguntaEncontrada.hasOwnProperty(campo) ){
        datosActualizar[ campo ] = req.body[campo];
      }
    }


    // PROCEDEMOS A ACTUALIZAR CON LOS DATOS PROPORCIONADOS
    const preguntaActualizada = await preguntas_frecuentes.updatePregunta( datosActualizar, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Pregunta actualizada correctamente', pregunta: preguntaActualizada });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// OBTENER TODAS LAS PREGUNTAS
exports.getPreguntas = async (req, res) => {
  try{
    
    // OBTENER TODAS LAS PREGUNTAS
    const preguntasTodas  = await preguntas_frecuentes.getPreguntas().then( response => response );

    const total = preguntasTodas.length;

    // RESPUESTA AL USUARIO
    res.send({ message: 'Preguntas obtenidas', total_preguntas: total, preguntas: preguntasTodas });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};



// OBTENER PREGUNTA
exports.getPregunta = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if ( !req.params.id ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // DESESTRUCTURAMOS LOS DATOS
    const { id } = req.params;


    // OBTENER PREGUNTA
    const pregunta  = await preguntas_frecuentes.getPregunta( id ).then( response => response );

    if ( !pregunta ) return res.status( 404 ).send({ message: `La pregunta con id ${id} no pudo ser encontrada` });


    // RESPUESTA AL USUARIO
    res.send({ message: 'Pregunta encontrada correctamente', pregunta });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


