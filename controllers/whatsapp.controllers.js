//declaramos en una const nuestro model
const wpp = require("../models/whatsapp.model");
const usuariosModel = require("../models/usuarios.model");
exports.getAllMessage = async (req, res) => {
  if (typeof req.params.id == "undefined") {
    res.status(400).send({
      message: "debes enviar el id del chat",
    });
  }

  wpp.getAllMessage(req.params.id, (err, data) => {
    if (err) {
      res.status(500).send({ message: "Hubo un errro al traer los datos" });
    }
    res.status(200).send(data);
  });
};

exports.getAllChats = async (req, res) => {
  const idEmpleado = req.params.idEmpleado;
  const puesto = await usuariosModel
    .getPuestoUsuario(idEmpleado)
    .then((response) => response)
    .catch((err) => 323212);

  try {
    if (
      puesto[0].idpuesto == 25 ||
      puesto[0].idpuesto == 3 ||
      puesto[0].idpuesto == 7 ||
      puesto[0].idpuesto == 43 ||
      puesto[0].idpuesto == 12
    ) {
      await wpp
        .getAllChats()
        .then((response) => res.status(200).send({ chats: response }))
        .catch((err) => {
          res.status(500).send({
            message:
              err.message || "Se produjo algún error al recuperar los chats",
          });
        });
    } else {
      await wpp
        .getAllChatsEmpleado(idEmpleado)
        .then((response) => res.status(200).send({ chats: response }))
        .catch((err) => {
          res.status(500).send({
            message:
              err.message || "Se produjo algún error al recuperar los chats",
          });
        });
    }
  } catch (error) {
    res.status(500).send({
      message:
        "Se produjo algún error al recuperar los chats. o no se encontro el empleado",
    });
  }
};
// exports.createChatWithClient = async(req,res)=>{

// }

// exports.all_usuarios = (req, res) => {
//   //mandamos a llamar al model y le enviamos dos parametros err=para errores
//   //y data= los datos que estamos recibiendo
//   Usuarios.all_usuarios((err, data) => {
//     if (err)
//       res.status(500).send({
//         message:
//           err.message || "Se produjo algún error al recuperar los usuarios",
//       });
//     else res.send(data);
//   });
// };

// exports.getUsuarioId = (req, res) => {
//   //mandamos a llamar al model y le enviamos dos parametros err=para errores
//   //y data= los datos que estamos recibiendo
//   Usuarios.getUsuarioId(req.params.idusuarios, (err, data) => {
//     if (err)
//       res.status(500).send({
//         message:
//           err.message || "Se produjo algún error al recuperar los usuarios",
//       });
//     else res.send(data);
//   });
// };

// exports.update_usuario = (req, res) => {
//   if (!req.body || Object.keys(req.body).length === 0) {
//     res.status(400).send({ message: "El contenido no puede estar vacío" });
//   }

//   Usuarios.update_usuario(req.params.idusuarios, req.body, (err, data) => {
//     if (err) {
//       if (err.kind === "not_found") {
//         res.status(404).send({
//           message: `No se encontro al usuario con id : ${req.params.idusuarios}`,
//         });
//       } else {
//         res.status(500).send({
//           message:
//             "Error al actualizar el usuario con el id : " +
//             req.params.idsucursales,
//         });
//       }
//     } else {
//       res
//         .status(200)
//         .send({ message: `El usuario se ha actualizado correctamente.` });
//     }
//   });
// };

// /******************************************************/
// exports.all_usuariosERP = (req, res) => {
//   //mandamos a llamar al model y le enviamos dos parametros err=para errores
//   //y data= los datos que estamos recibiendo
//   Usuarios.all_usuariosERP((err, data) => {
//     if (err)
//       res.status(500).send({
//         message:
//           err.message || "Se produjo algún error al recuperar los usuarios",
//       });
//     else res.send(data);
//   });
// };

// exports.all_usuariosCAPA = (req, res) => {
//   //mandamos a llamar al model y le enviamos dos parametros err=para errores
//   //y data= los datos que estamos recibiendo
//   Usuarios.all_usuariosCAPA(req.params.id, (err, data) => {
//     if (err)
//       res.status(500).send({
//         message:
//           err.message || "Se produjo algún error al recuperar los usuarios",
//       });
//     else res.send(data);
//   });
// };

// //usuario id erp
// exports.usuario_id = (req, res) => {
//   //mandamos a llamar al model y le enviamos dos parametros err=para errores
//   //y data= los datos que estamos recibiendo
//   Usuarios.usuario_id(req.params.id, (err, data) => {
//     if (err)
//       res.status(500).send({
//         message:
//           err.message || "Se produjo algún error al recuperar los usuarios",
//       });
//     else res.send(data);
//   });
// };

// ///usuario por puesto
// exports.getusuariosPuesto = (req, res) => {
//   Usuarios.getusuariosPuesto(req.params.id, (err, data) => {
//     if (err)
//       res.status(500).send({
//         message:
//           err.message || "Se produjo algún error al recuperar los usuarios",
//       });
//     else res.send(data);
//   });
// };

// //usuario por departamento
// exports.getusuariosDepto = (req, res) => {
//   Usuarios.getusuariosDepto(req.params.id, (err, data) => {
//     if (err)
//       res.status(500).send({
//         message:
//           err.message || "Se produjo algún error al recuperar los usuarios",
//       });
//     else res.send(data);
//   });
// };

// /******************************************************/
// exports.getUsuariosNiveles = (req, res) => {
//   //mandamos a llamar al model y le enviamos dos parametros err=para errores
//   //y data= los datos que estamos recibiendo
//   Usuarios.getUsuariosNiveles((err, data) => {
//     if (err)
//       res.status(500).send({
//         message:
//           err.message ||
//           "Se produjo algún error al recuperar los niveles del organigrama",
//       });
//     else res.send(data);
//   });
// };

// exports.addUsuariosNiveles = (req, res) => {
//   //validamos que tenga algo el req.body
//   if (!req.body || Object.keys(req.body).length === 0) {
//     res.status(400).send({ message: "El Contenido no puede estar vacio" });
//   }

//   Usuarios.addUsuariosNiveles(req.body, (err, data) => {
//     if (err)
//       res.status(500).send({
//         message:
//           err.message ||
//           "Se produjo algún error al crear los niveles del organigrama",
//       });
//     else
//       res.status(200).send({
//         message: `los niveles del organigrama se ha creado correctamente`,
//       });
//   });
// };

// exports.updateUsuariosNiveles = (req, res) => {
//   if (!req.body || Object.keys(req.body).length === 0) {
//     res.status(400).send({ message: "El contenido no puede estar vacío" });
//   }

//   Usuarios.updateUsuariosNiveles(req.params.id, req.body, (err, data) => {
//     if (err) {
//       if (err.kind === "not_found") {
//         res.status(404).send({
//           message: `No se encontro el nivel del organigramacon con id : ${req.params.id}`,
//         });
//       } else {
//         res.status(500).send({
//           message:
//             "Error al actualizar el nivel del organigramacon el id : " +
//             req.params.id,
//         });
//       }
//     } else {
//       res.status(200).send({
//         message: `el nivel del organigramacon se ha actualizado correctamente.`,
//       });
//     }
//   });
// };
