const dashboardPersonal    = require("../../models/kpi/dashboardPersonal.model.js");
const prospectos           = require("../../models/prospectos/prospectos.model.js");
const erpviejo             = require("../../models/prospectos/erpviejo.model.js");
const registro_actividades = require("../../models/kpi/registro_actividades.model.js");
const personal             = require("../../models/kpi/dashboardPersonal.model.js");

exports.gatDashboardPersonal = async(req, res) => {
  try {

  	/************ CALCULAR RI POR RECEPCIONISTA ******/
  	// BUSCAR LAS RECPEIONISTAS Y SUS PLANTELES
  	/*
			1-. SACAR LAS RECEPCIONISTAS
			2-. SACAR LOS PLANTELES
  	*/
  	const recepcionistas   = await dashboardPersonal.getRecepcionistas( ).then(response=> response)
  	const plantelesUsuario = await dashboardPersonal.getPlantelesUsuario( ).then(response=> response)
  	let idRecepcionistas = []
  	for(const i in recepcionistas){
  		const { iderp } = recepcionistas[i]
  		idRecepcionistas.push(iderp)
  	}

  	// Consultamos los nombres de esas recepcionistas 
  	const nombresRecepcionistas = await dashboardPersonal.getNombresRecepcionistas( idRecepcionistas ).then(response => response)
  	// Creamos a las recepcionitas
  	for(const i in recepcionistas){
  		const { iderp, idusuario } = recepcionistas[i]
  		// Buscamos el nombre
  		const nombre     = nombresRecepcionistas.find(el=> el.id_usuario == iderp)
  		// Filtarmos sus sucursales 
  		const sucursales = plantelesUsuario.filter(el=> { return  el.idusuario == idusuario })
  		// Creamos el objeto de la recepcionista
  		recepcionistas[i] = {
  			...recepcionistas[i],
  			nombre_completo: nombre ? nombre.nombre_completo : '',
  			sucursales
  		}
  	}

    let id_alumnos         = [0]
    let mes_ciclo          = ''
    let anio_ciclo         = ''
    let siguienteCiclo     = ''

    let diferencia         = 0
    let semana             = 1


    // consultamos los planteles
    let planteles = await dashboardPersonal.getPlanteles().then(response=> response)
    
    // consultamos los ciclos actuales
    const cicloActual = await dashboardPersonal.cicloActual( ).then(response=> response)

    // Hay que calcular la semana en la que estanmos
    if(!cicloActual){
    	// Retornamos el error 
    	return res.status(400).send({message:'No se encontro un ciclo actual'})
    }

    diferencia      = cicloActual.diferencia

    if([1,2,3,4,5,6,7].includes(diferencia)){
      semana = 1
    }

    if([8,9,10,11,12,13,14].includes(diferencia)){
      semana = 2
    }

    if([15,16,17,18,19,20,21].includes(diferencia)){
      semana = 3
    }

    if(diferencia >= 22){
      semana = 4
    }

    // Sacar el ciclo siguiente
    // Primero el mes
    mes_ciclo  = parseInt(cicloActual.ciclo.substr(5,3)) == 12 ? 1 : parseInt(cicloActual.ciclo.substr(5,3)) + 1
    // Lo escribimos de manera correcta ya que no podemos poner 1_22 tiene que ser 01_22
    mes_ciclo = mes_ciclo < 10 ? `0${mes_ciclo}` : mes_ciclo
    anio_ciclo = parseInt(cicloActual.ciclo.substr(9,2))
    // Creamos la variable del ciclo
    siguienteCiclo = `CICLO ${mes_ciclo}_${anio_ciclo}`
    // Lo buscamos en la base de datos
    let cicloSiguiente = await dashboardPersonal.getCiclo(siguienteCiclo).then(response=>response)
    // Vaidamos que exista
    if(!cicloSiguiente){
    	// Retornamos el error 
    	return res.status(400).send({message:'No se encontro el siguiente ciclo'})
    }

    // Consultar los alumnos del ciclo Actual
    const alumnosCicloActual = await dashboardPersonal.getAlumnosCicloActual( cicloActual.id_ciclo, cicloActual.id_ciclo_relacionado ).then(response=> response)

    // // consultamos los alumnos inscritos actualmente
    const alumnosSiguientes = await dashboardPersonal.getAlumnosCicloSiguiente( cicloActual.id_ciclo, cicloActual.id_ciclo_relacionado, cicloSiguiente.id_ciclo, cicloSiguiente.id_ciclo_relacionado ).then(response=>response)

    /******** CALCULANDO RI ********/
    // Calular la cantidad de alumnos inscritos por plantel
    for(const i in planteles){
      // sacamos el total de alumnos en el ciclo
      const totalAlumnosActuales    =  alumnosCicloActual.filter(el=> { return el.id_plantel == planteles[i].id_plantel }).length
      const totalAlumnosSiguientes  =  alumnosSiguientes.filter(el=> { return el.id_plantel == planteles[i].id_plantel }).length

      const porcentajeRi    = totalAlumnosSiguientes > 0 ? ((totalAlumnosSiguientes/totalAlumnosActuales)*100).toFixed(2) : 0

      planteles[i] = {
        id_plantel: planteles[i].id_plantel,
        plantel:    planteles[i].plantel,
        porcentajeRi,
      }
    }

    // Ahoraaaa vamos a ponerle a cada recepcionista su sucursal
    for(const i in recepcionistas){
    	const { sucursales } = recepcionistas[i]
    	for( const j in sucursales ){
	  		const { idplantel } = sucursales[j]
	  		const riPlantel     = planteles.find( el=> el.id_plantel == idplantel) 
	  		// Creamos el objeto de la recepcionista
	  		sucursales[j] = {
	  			...sucursales[j],
	  			riPlantel
	  		}
	  	}
  	}

    /******** CALCULANDO VENTAS ********/
    let alumnosNI = await dashboardPersonal.getAlumnosNI( ).then( response=> response )
    // Guardamos los id de los alumnos
    let idAlumnosNI = []
    for(const i in alumnosNI){
      idAlumnosNI.push(alumnosNI[i].id_alumno)
    }

    // consultar vendedoras
    let alumnosInscritosCRM = []
    if(idAlumnosNI.length){
      // Obtener los alumnos inscritos desde el CRM 
      alumnosInscritosCRM = await dashboardPersonal.getAlumnosInscritosCRM( idAlumnosNI ).then( response=> response )
      // Hay que validar si en el crm si están esos contactos, si no, no se hace nada
      if( alumnosInscritosCRM ){
        // Agregar los nombres de las vendedoras del CRM y reemplazar la "vendedora" del ERP viejito
        for(const i in alumnosNI){
          const vendedora = alumnosInscritosCRM.find( el=> el.usuario_asignado == alumnosNI[i].id_usuario_ultimo_cambio )
          if( vendedora ){ alumnosNI[i].id_usuario_ultimo_cambio = vendedora.usuario_asignado }
        }
      }
    }

    // Agreagarlos los nombres de las vendedoras a la vendedora
    const usuariosERP    = await erpviejo.getUsuariosERP( ).then(response=> response)
    for(const i in alumnosNI){
      const { id_usuario_ultimo_cambio } = alumnosNI[i]
      // Buscar el usuario del ERP 
      const vend     = usuariosERP.find(el=> el.id_usuario == id_usuario_ultimo_cambio)
      // Agregar los usuarios y nombres
      alumnosNI[i] = {
        ...alumnosNI[i],
        vendedora: vend  ? vend.nombre_completo : '',
      }
    }

    let hash = {};
    const ventas = alumnosNI.filter((vendedora) => {
      let exists = !hash[vendedora.id_usuario_ultimo_cambio];
      hash[vendedora.id_usuario_ultimo_cambio] = true;
      return exists;
    });

    for(const i in ventas){
      const inscritos = alumnosNI.filter(el=> { return el.id_usuario_ultimo_cambio == ventas[i].id_usuario_ultimo_cambio })
      ventas[i] = {
        vendedora      : ventas[i].vendedora,
        id_usuario     : ventas[i].id_usuario_ultimo_cambio,
        totalInscritos : inscritos.length,
        inscritos
      }
    }

    /******** CALCULANDO CONTACTOS ********/
    let contactosVendedora = await dashboardPersonal.getProspectosVendedoraCantidad( ).then( response=> response )
    // Guardamos los id de los alumnos
    for(const i in contactosVendedora){
      const { usuario_asignado } = contactosVendedora[i]
      // Buscar el nombre de la vendedora
      const vend     = usuariosERP.find(el=> el.id_usuario == usuario_asignado)

      contactosVendedora[i] = {
        ...contactosVendedora[i],
        vendedora: vend ? vend.nombre_completo : ''
      }
    }

    
    /******** CALCULANDO PROGRESO DE LOS USUARIOS (DESARROLLADOR) ********/
    let desarrolladores   = await dashboardPersonal.getUsuariosDesarrollo( ).then(response => response); // consultar los Ni de estos ciclos
    let actividades       = await dashboardPersonal.getActividadesUsuario( ).then(response => response); // consultar los Ni de estos ciclos
    
    for(const i in desarrolladores){
      // Buscar el nombre de la vendedora
      const usuario            = usuariosERP.find(el=> el.id_usuario   == desarrolladores[i].iderp)
      const actividadesUsuario = actividades.find(el=> el.idusuarioerp == desarrolladores[i].iderp)

      desarrolladores[i] = {
        ...desarrolladores[i],
        nombre_completo: usuario ? usuario.nombre_completo : '',
        actividades: actividadesUsuario ? actividadesUsuario : { realizadas: 0, numero_actividades: 0}
      }
    }

    /******** CALCULANDO PROGRESO DE LOS USUARIOS (ADMINISTRACION) ********/
    let administradores   = await dashboardPersonal.getUsuariosAdministracion( ).then(response => response); // consultar los Ni de estos ciclos
    actividades           = await dashboardPersonal.getActividadesUsuario( ).then(response => response); // consultar los Ni de estos ciclos
    
    for(const i in administradores){
      // Buscar el nombre de la vendedora
      const usuario            = usuariosERP.find(el=> el.id_usuario   == administradores[i].iderp)
      const actividadesUsuario = actividades.find(el=> el.idusuarioerp == administradores[i].iderp)

      administradores[i] = {
        ...administradores[i],
        nombre_completo: usuario ? usuario.nombre_completo : '',
        actividades: actividadesUsuario ? actividadesUsuario : { realizadas: 0, numero_actividades: 0}
      }
    }


    /******** OBTENER EL AVANCE DE VACANTES ********/
    const getVacantesDisponibles = await registro_actividades.getVacantesDisponibles( ).then(response=> response)
  

    // Respuesta a enviar
    res.send({
      semana,
      vacantesDisponibles: getVacantesDisponibles,
    	recepcionistas,
      ventas,
      contactosVendedora,
      desarrolladores,
      administradores 
    });
    
  } catch (error) {
    res.status(500).send({message:error})
  }

};
