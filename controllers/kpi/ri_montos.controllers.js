const ri_montos    = require("../../models/kpi/ri_montos.model.js");
const prospectos   = require("../../models/prospectos/prospectos.model.js");
const Kpi          = require("../../models/kpi/kpi.model.js");

exports.getRiMontos = async(req, res) => {
  try {
  	// Sacamos los ID
  	const { id_inicio_inbi, id_inicio_fast, id_fin_inbi, id_fin_fast, escuela } = req.body

  	/************** RI MONTOS **********************/
  	// Ingresos del ciclo actual
  	const ingresosCicloInicio = await ri_montos.getIngresosCicloInicio( id_inicio_inbi, id_inicio_fast ).then(response=> response)
  	// Ingresos del RI
  	const ingresosCicloRi     = await ri_montos.getIngresosCicloRi( id_fin_inbi, id_fin_fast, id_inicio_inbi, id_inicio_fast ).then(response=> response)
  	// Ahora vamos a sacar los Ri de montos, ocupamos el % más bajao de cada escuela
		// Declaramos las variables iniciales
		let plantelInbi = ''
		let plantelFast = ''
		let riMontoInbi = 100
		let riMontoFast = 100

  	// Calcular los % de RI
  	for(const i in ingresosCicloInicio){
  		const { total, id_plantel, plantel } = ingresosCicloInicio[i]
  		// Buscamos el monto de RI 
  		const montoTotalRi = ingresosCicloRi.find(el=> el.id_plantel == id_plantel )
  		ingresosCicloInicio[i] = {
  			...ingresosCicloInicio[i],
  			riMonto: montoTotalRi ? ((montoTotalRi.total / total)*100).toFixed(2) : 0 // se calcula el %
  		}
  	}

		for(const i in ingresosCicloInicio){
			const { plantel, escuela, riMonto } = ingresosCicloInicio[i]
			// buscar quien tiene menor RI y escuela de inbi
			if(parseFloat(riMonto) < riMontoInbi && escuela == 1){
				riMontoInbi = riMonto // se guarda el RI
				plantelInbi = plantel // se guarda el nombre del plantel
			}
			// buscar quien tiene menor RI y escuela de FAST
			if(parseFloat(riMonto) < riMontoFast && escuela == 2){
				riMontoFast = riMonto // se guarda el RI
				plantelFast = plantel // se guarda el nombre del plantel
			}
		}

  	/************** DATA TABLE **********************/
  	// Alumnos actuales
  	const alumnosActuales   = await ri_montos.getAlumnosCicloActual( id_inicio_inbi, id_inicio_fast ).then(response=> response)
  	// Alumnos siguientes
  	const alumnosSiguientes = await ri_montos.getAlumnosCicloSiguiente( id_inicio_inbi, id_inicio_fast, id_fin_inbi, id_fin_fast ).then(response=> response)

  	// Sacar los alumnos por sucursal
  	const planteles         = await ri_montos.getPlanteles().then(response=> response)
  	for( const i in planteles){
  		const { id_plantel } = planteles[i]
  		// Necesitamos sacar los alumnos que hay actuales y cuantos en RI y cuantos Faltan al igual que el %
  		let cantAlumnos   = alumnosActuales.filter(el=> { return el.id_plantel == id_plantel }).length
  		let cantAlumnosRI = alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel }).length
  		let faltan        = cantAlumnos - cantAlumnosRI
  		let porcentaje    = ((faltan / cantAlumnos)*100).toFixed(2)
  		planteles[i] = {
  			...planteles[i],
  			cantAlumnos,
				cantAlumnosRI,
				faltan,
				porcentaje,
  		}
  	}

  	// Necesitamos sacar a quien le faltan más alumnos
  	let numAlumPlantelInbi     = ''
  	let numAlumPlantelCantInbi = 0
  	let numAlumPlantelFast     = ''
  	let numAlumPlantelCantFast = 0

  	for(const i in planteles){
			const { id_plantel, faltan, plantel, escuela } = planteles[i]
			// buscar quien tiene menor cantidad de alumnos y escuela de inbi
			if(faltan > numAlumPlantelCantInbi && escuela == 1){
				numAlumPlantelCantInbi = faltan // se guarda lo que falta
				numAlumPlantelInbi     = plantel // se guarda el nombre del plantel
			}
			// buscar quien tiene menor RI y escuela de FAST
			if(faltan > numAlumPlantelCantFast && escuela == 2){
				numAlumPlantelCantFast = faltan // se guarda el RI
				numAlumPlantelFast     = plantel // se guarda el nombre del plantel
			}
		}

		// Necesitamos sacar a quien le faltan más alumnos
  	let percentAlumPlantelInbi     = ''
  	let percentAlumPlantelCantInbi = 0
  	let percentAlumPlantelFast     = ''
  	let percentAlumPlantelCantFast = 0

  	for(const i in planteles){
			const { id_plantel, porcentaje, plantel, escuela } = planteles[i]
			// buscar quien tiene menor cantidad de alumnos y escuela de inbi
			if(porcentaje > percentAlumPlantelCantInbi && escuela == 1){
				percentAlumPlantelCantInbi = porcentaje // se guarda lo que falta
				percentAlumPlantelInbi     = plantel // se guarda el nombre del plantel
			}
			// buscar quien tiene menor RI y escuela de FAST
			if(porcentaje > percentAlumPlantelCantFast && escuela == 2){
				percentAlumPlantelCantFast = porcentaje // se guarda el RI
				percentAlumPlantelFast     = plantel // se guarda el nombre del plantel
			}
		}

		const leads     = await ri_montos.getLeadsAyer().then(response => response) // leads registrados el día de ayer
		const contactos = await ri_montos.getContactosAyer().then(response => response) // contactos registrados el día de ayer

		// Calular los NI
	  const niAlumnosInbi = await Kpi.TotalInscritosPorCiclo(id_fin_inbi).then(response => response); // consultar los Ni de estos ciclos
	  const niAlumnosFast = await Kpi.TotalInscritosPorCiclo(id_fin_fast).then(response => response); // consultar los Ni de estos ciclos

		const cantAlumnos       = alumnosActuales.filter(el=> { return el.escuela == escuela }).length
		const cantAlumnosRI     = alumnosSiguientes.filter(el=> { return el.escuela == escuela }).length
		const cantAlumnosFaltan = (cantAlumnos - cantAlumnosRI)
		const porcentajeAlFanta = (((cantAlumnos - cantAlumnosRI)/ cantAlumnos) * 100).toFixed(2)
    
    // Respuesta a enviar
    res.send({
    	riMontos:{
    		inbi:{
    			plantel: plantelInbi,
    			riMonto: riMontoInbi
    		},

    		fast:{
    			plantel: plantelFast,
    			riMonto: riMontoFast
    		},
    	},

    	cantAlumnos:{
    		inbi:{
    			plantel: numAlumPlantelInbi,
    			cant:    numAlumPlantelCantInbi
    		},

    		fast:{
    			plantel: numAlumPlantelFast,
    			cant:    numAlumPlantelCantFast
    		},
    	},


    	porcentajeAlumnos:{
    		inbi:{
    			plantel: percentAlumPlantelInbi,
    			cant:    percentAlumPlantelCantInbi
    		},

    		fast:{
    			plantel: percentAlumPlantelFast,
    			cant:    percentAlumPlantelCantFast
    		},
    	},

    	dataTable:[
    		{
    			cantAlumnos,
    			cantAlumnosRI,
    			cantAlumnosFaltan,
    			porcentajeAlFanta,
    			cantAlumnosNI    : escuela == 1 ? niAlumnosInbi : niAlumnosFast,
    			contactos        : contactos.filter(el=> { return el.escuela == escuela }).length,
    			leads            : leads.filter(el=> { return el.escuela == escuela }).length
    		}
    	]
    });
    
  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }

};

