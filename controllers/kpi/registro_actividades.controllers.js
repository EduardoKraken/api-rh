const registro_actividades = require("../../models/kpi/registro_actividades.model.js");
const erpviejo             = require("../../models/prospectos/erpviejo.model.js");


exports.getRegistroActividades = async (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  try {
  	// Consultando todos los usuarios
  	const usuariosERP    = await erpviejo.getUsuariosERP( ).then(response=> response)
  	// Obtener el registro de actividades
  	let registroActividades = await registro_actividades.getRegistrosActividades( ).then( response=> response )

  	// obtener el mes y el añio
  	const { semana, anio }     = await registro_actividades.getMesAnio( ).then( response=> response )

  	for(const i in registroActividades){
  		const { idusuarioerp, usuario_registro } = registroActividades[i]
  		// Buscamos el nombre del usuario
  		const usuario      = usuariosERP.find(el=> el.id_usuario == idusuarioerp)
  		const registro     = usuariosERP.find(el=> el.id_usuario == usuario_registro)

  		// Guardamos el nombre
  		registroActividades[i] = {
  			...registroActividades[i],
  			nombre:   usuario  ? usuario.nombre_completo : '',
  			registro: registro ? registro.nombre_completo : '' 
  		}
  	}

    res.send({
    	semana,
			anio,
    	registroActividades
    });
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addRegistroActividades = async (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  try {
    const { idusuarioerp, fecha_inicio, numero_actividades, idpuesto, usuario_registro } =  req.body

    // Consultar semana y añio de la fecha de inici
    const existenActividades = await registro_actividades.existenActividades( idusuarioerp, fecha_inicio ).then(response=>response)
    // Validar si existe información
    if( existenActividades ){
      let id = existenActividades.idactividades_personal
      // Existe, hay que editar las actividades
      const updateActividades = await registro_actividades.updateAddActividades( numero_actividades, id ).then(response=>response)
      // Agregar el detalle actividades
      const payload = {
        idactividades_personal: id,
        numero_actividades,
        usuario_registro,
        suma_resta: 1
      }
      const addRegistroDetalleActividad = await registro_actividades.addRegistroDetalleActividad( payload ).then(response=>response)
      return res.send({message: 'Ok', idusuarioerp, existenActividades });
    }else{
      // No existe
      // Agregar actividades
      const addRegistroActividades = await registro_actividades.addRegistroActividades( req.body ).then(response=>response)
      // Agregar el detalle de actividades
      if(addRegistroActividades){
        const payload2 = {
          idactividades_personal: addRegistroActividades.id,
          numero_actividades,
          usuario_registro,
          suma_resta: 1
        }
        const addRegistroDetalleActividad = await registro_actividades.addRegistroDetalleActividad( payload2 ).then(response=>response)
        return res.send({message: 'Ok', idusuarioerp, existenActividades });
      }else{
        return res.status(400).send({message: 'Error al registrar las actividades' });
      }
    }

    
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getRegistroActividadesUsuario = async (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  try {
    const { id } = req.params
    // Consultando todos los usuarios
    const usuariosERP    = await erpviejo.getUsuariosERP( ).then(response=> response)
    // Obtener el registro de actividades
    let registroActividadesUsuario = await registro_actividades.getRegistroActividadesUsuario( id ).then( response=> response )

    // // obtener el mes y el añio
    const { semana, anio }     = await registro_actividades.getMesAnio( ).then( response=> response )

    res.send({
      semana,
      anio,
      registroActividadesUsuario
    });
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addRegistroMiActividades = async (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  try {
    const { idusuarioerp, fecha_inicio, realizadas, idpuesto, usuario_registro } =  req.body

    // Consultar semana y añio de la fecha de inici
    const existenActividades = await registro_actividades.existenActividades( idusuarioerp, fecha_inicio ).then(response=>response)
    
    // Validar si existe información
    if( existenActividades ){
      
      let id = existenActividades.idactividades_personal
      
      // Existe, hay que editar las actividades
      const updateActividades = await registro_actividades.updateMisActividades( realizadas, id ).then(response=>response)
      
      // Agregar el detalle actividades
      const payload = {
        idactividades_personal: id,
        numero_actividades: realizadas,
        usuario_registro,
        suma_resta: 2
      }
      const addRegistroDetalleActividad = await registro_actividades.addRegistroDetalleActividad( payload ).then(response=>response)
      
      return res.send({message: 'Ok', idusuarioerp, existenActividades });
    }

    // }else{
    //   // No existe
    //   // Agregar actividades
    //   const addRegistroActividades = await registro_actividades.addRegistroActividades( req.body ).then(response=>response)
    //   // Agregar el detalle de actividades
    //   if(addRegistroActividades){
    //     const payload2 = {
    //       idactividades_personal: addRegistroActividades.id,
    //       numero_actividades,
    //       usuario_registro,
    //       suma_resta: 1
    //     }
    //     const addRegistroDetalleActividad = await registro_actividades.addRegistroDetalleActividad( payload2 ).then(response=>response)
    //     return res.send({message: 'Ok', idusuarioerp, existenActividades });
    //   }else{
    //     return res.status(400).send({message: 'Error al registrar las actividades' });
    //   }
    // }

    
  } catch (error) {
    res.status(500).send({message:error})
  }
};

// Función para agregar vacantes disponibles
exports.addVacantesDisponibles = async (req, res) => {
  try {
    const { vacantes, vacantes_disponibles  } = req.body
    // Calculamos el porcentaje
    const procentaje = ((vacantes_disponibles / vacantes) * 100 ).toFixed(2)
    // Guardamosss
    const addVacantesDisponibles = await registro_actividades.addVacantesDisponibles( req.body, procentaje).then(response=> response)

    res.send({
      addVacantesDisponibles
    });
  } catch (error) {
    res.status(500).send({message:error})
  }
};

// Función para obtener los últimos registros de las vacantes disponibles
exports.getVacantesDisponibles = async (req, res) => {
  try {
    const getVacantesDisponibles = await registro_actividades.getVacantesDisponibles( ).then(response=> response)
    // Validar si hay información
    if(getVacantesDisponibles){
      return res.send(getVacantesDisponibles);
    }else{
      return res.status(400).send({message: 'Sin datos'});
    }
  } catch (error) {
    res.status(500).send({message:error})
  }
};