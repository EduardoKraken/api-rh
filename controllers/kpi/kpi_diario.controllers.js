const kpi_diario    = require("../../models/kpi/kpi_diario.model.js");
const prospectos    = require("../../models/prospectos/prospectos.model.js");
const dashboardKpi  = require("../../models/kpi/dashboardKpi.model.js");
const personal      = require("../../models/kpi/dashboardPersonal.model.js");
const erpviejo      = require("../../models/prospectos/erpviejo.model.js");

exports.getKpiDiario = async(req, res) => {
  try {
    let mes_ciclo          = ''
    let anio_ciclo         = ''
    let siguienteCiclo     = ''

    

    // consultar vendedoras
    let vendedorasActuales = await personal.getUsuariosVentas( ).then(response => response)
    
    // Agregarles el nombre
    const usuariosERP    = await erpviejo.getUsuariosERP( ).then(response=> response)
    for(const i in vendedorasActuales){
      const { iderp } = vendedorasActuales[i]
      
      // Buscar el usuario del ERP 
      const nombre    = usuariosERP.find(el=> el.id_usuario == iderp)
      
      // Agregar los usuarios y nombres
      vendedorasActuales[i] = {
        id_usuario: iderp,
        nombre_completo: nombre  ? nombre.nombre_completo : '',
      }
    }

    // consultamos los ciclos actuales
    const cicloActual = await kpi_diario.cicloActual( ).then(response=> response)

    if(!cicloActual){
    	// Retornamos el error 
    	return res.status(400).send({message:'No se encontro un ciclo actual'})
    }

    // Sacar el ciclo siguiente
    // Primero el mes
    mes_ciclo  = parseInt(cicloActual.ciclo.substr(5,3)) == 12 ? 1 : parseInt(cicloActual.ciclo.substr(5,3)) + 1
    // Lo escribimos de manera correcta ya que no podemos poner 1_22 tiene que ser 01_22
    mes_ciclo = mes_ciclo < 10 ? `0${mes_ciclo}` : mes_ciclo
    anio_ciclo = parseInt(cicloActual.ciclo.substr(9,2))
    // Creamos la variable del ciclo
    siguienteCiclo = `CICLO ${mes_ciclo}_${anio_ciclo}`
    // Lo buscamos en la base de datos
    let cicloSiguiente = await kpi_diario.getCiclo(siguienteCiclo).then(response=>response)
    // Vaidamos que exista
    if(!cicloSiguiente){
    	// Retornamos el error 
    	return res.status(400).send({message:'No se encontro el siguiente ciclo'})
    }

    // Ventas de ayer del ciclo
    const ventasDiarias = await kpi_diario.getInscritosAyer( cicloSiguiente.id_ciclo, cicloSiguiente.id_ciclo_relacionado ).then(response => response)

    // VENTAS TOTALES DEL CICLO 
    const ventasTotales = await kpi_diario.getInscritosTotalesCiclo( cicloSiguiente.id_ciclo, cicloSiguiente.id_ciclo_relacionado ).then(response => response)

    // CONTACTOS POR VENDEDORA
    let contactos      = await kpi_diario.getContactosVendedoraAyer( ).then(response => response)

		// Primero vamos a sacar los id de las vendedoras
		let idVendedoras = [0]
		// Sacamos los id de las vendedoras
		for(const i in vendedorasActuales){ idVendedoras.push(vendedorasActuales[i].id_usuario) }

		// Le agregamos a cada vendedora sus ventas
		for(const i in vendedorasActuales){
      const { id_usuario } = vendedorasActuales[i]
      // Agregamos los inscritos con y sin adeudo y el total
      vendedorasActuales[i] = {
      	...vendedorasActuales[i],
        conAdeudo: ventasDiarias.filter(el=> { return  el.adeudo > 0 && el.id_usuario == id_usuario}).length,
        sinAdeudo: ventasDiarias.filter(el=> { return  el.adeudo <= 0 && el.id_usuario == id_usuario}).length,
        cantidad:  ventasDiarias.filter(el=> { return  el.id_usuario == id_usuario}).length,
        ciclo:     ventasTotales.filter(el=> { return  el.adeudo <= 0 && el.id_usuario == id_usuario}).length,
        contactos_totales: contactos.filter(el=> { return  el.usuario_asignado == id_usuario}).length,
        contactos_mmk:     contactos.filter(el=> { return  el.usuario_asignado == id_usuario && el.idpuesto == 25 }).length,
        contactos_ventas:  contactos.filter(el=> { return  el.usuario_asignado == id_usuario && el.idpuesto == 18 }).length,
        contactos_recep:   contactos.filter(el=> { return  el.usuario_asignado == id_usuario && el.idpuesto == 19 }).length,
        contactos_super:   contactos.filter(el=> { return  el.usuario_asignado == id_usuario && el.idpuesto == 17 }).length,
        contactos_otros:   contactos.filter(el=> { return  el.usuario_asignado == id_usuario && ![25,18,19,17].includes(el.idpuesto) }).length
      }
    }

    // Agregaremos una nueva vendedora que será la que nos ayude a guardar los alumnos sin vendedora
		vendedorasActuales.push({
			id_usuario: 0,
			nombre_completo: 'Z - SIN VENDEDORA',
      conAdeudo: ventasDiarias.filter(el=> { return  el.adeudo > 0 && !idVendedoras.includes(el.id_usuario) }).length,
      sinAdeudo: ventasDiarias.filter(el=> { return  el.adeudo <= 0 && !idVendedoras.includes(el.id_usuario)}).length,
      cantidad:  ventasDiarias.filter(el=> { return  !idVendedoras.includes(el.id_usuario)}).length,
      ciclo:     ventasTotales.filter(el=> { return  el.adeudo > 0 && !idVendedoras.includes(el.id_usuario)}).length,

		})

		// Ordenar las fuentes para de mayor a menor de leads
    vendedorasActuales.sort((a, b) => {
		  if (a.nombre_completo > b.nombre_completo) {
		    return 1;
		  }
		  if (a.nombre_completo < b.nombre_completo) {
		    return -1;
		  }
		  return 0;
		});


    /**************** ALUMNO SIN VENDEDORA ****************/

    // Vamos a guardar los alumnos que no tienen vendedora
		let alumnosSinVendedora = []
		// Recorremos las ventas
		for(const i in ventasDiarias){
			const { id_usuario } = ventasDiarias[i]

			// Validamos si tiene vendedora 
			if(!idVendedoras.includes(id_usuario)){
				// Lo agremosa los alumnos sin vendedora
				alumnosSinVendedora.push({
					matricula: ventasDiarias[i].matricula,
					nombre:    ventasDiarias[i].alumno
				})
			}
		}

		/************ VENTAS TOTALES ****************/

		/* CALCULAR EL RI*/

    // consultamos los alumnos inscritos actualmente
    const alumnosActuales   = await kpi_diario.getAlumnosCicloActual( cicloActual.id_ciclo, cicloActual.id_ciclo_relacionado ).then(response=>response)

    // consultamos los alumnos inscritos actualmente
    const alumnosSiguientes = await kpi_diario.getAlumnosCicloSiguiente( cicloActual.id_ciclo, cicloActual.id_ciclo_relacionado, cicloSiguiente.id_ciclo, cicloSiguiente.id_ciclo_relacionado ).then(response=>response)

    /******** CALCULANDO RI ********/
		// Consultamos los planteles
		let planteles = await dashboardKpi.getPlanteles().then(response=> response)
    
    // Calular la cantidad de alumnos inscritos por plantel
    let riPlanteles = []
    
    // Recorremos los planteles para agregarles lo del RI 
    for(const i in planteles){
      const { id_plantel, plantel } = planteles[i]
      // sacamos el total de alumnos en el ciclo
      const totalAlumnosActuales    =  alumnosActuales.filter(el=> { return el.id_plantel == id_plantel }).length
      // sacamos el total de alumnos en el siguiente ciclo
      const totalAlumnosSiguientes  =  alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel }).length
      // Total de alumnos en el siguiente ciclo que se registraron ayer
      const totalAlumnosAyer        =  alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel && el.fecha_alta_nuevo_grupo }).length

      const porcentajeRi    = totalAlumnosSiguientes > 0 ? ((totalAlumnosSiguientes/totalAlumnosActuales)*100).toFixed(2) : 0
      let payload = {
        id_plantel,
        plantel   ,
        totalAlumnosActuales,
        totalAlumnosSiguientes,
        totalAlumnosAyer,
        faltantes: totalAlumnosActuales - totalAlumnosSiguientes,
        porcentajeRi,
      }
      if(totalAlumnosActuales > 0){
      	riPlanteles.push( payload )
      }
    }

    // Respuesta a enviar
    res.send({
    	alumnosSinVendedora,
    	vendedorasActuales,
    	riPlanteles
    });
    
  } catch (error) {
    res.status(500).send({message:error})
  }

};

