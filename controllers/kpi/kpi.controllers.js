const Kpi           = require("../../models/kpi/kpi.model.js");
const exci          = require("../../models/exci/exci.model.js");

/* */
const kpi_diario    = require("../../models/kpi/kpi_diario.model.js");
const dashboardKpi  = require("../../models/kpi/dashboardKpi.model.js");
const prospectos    = require("../../models/prospectos/prospectos.model.js");
const personal      = require("../../models/kpi/dashboardPersonal.model.js");
const erpviejo      = require("../../models/prospectos/erpviejo.model.js");
const entradas      = require("../../models/catalogos/entradas.model.js");

const newReporteVentas = require('../../helpers/reporteVentas');

const options = { style: 'currency', currency: 'USD' };
const AdmZip  = require('adm-zip');

exports.getCantActual = async(req, res) => {
  try {

    /* CALCULAR EL RI*/
    const { cicloPre, cicloAct, cicloSig } = req.body

    /****************************************/
    /*                E X C I               */
    /****************************************/

    let registros = await exci.getRegistrosExciBeca( ).then( response => response )
    // Solo los que ya tienen grupo
    const alumnosExciGrupo = registros.filter( el=> { return el.id_grupo && [cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado].includes(el.id_ciclo)})
    // Sacamos los idAlumnosExci
    const idAlumnosExci = alumnosExciGrupo.map((registro)=>{ return registro.id_alumno })
    const idsGrupos     = alumnosExciGrupo.map((registro)=>{ return registro.id_grupo_encuesta })
    // Consultadmos las becas
    let grupos = []
    if( idAlumnosExci.length > 0 ){ grupos  = await exci.gruposExci( idsGrupos ).then( response => response ) }

    for( const i in alumnosExciGrupo ){
      const { id_grupo_encuesta, id_alumno } = alumnosExciGrupo[i]
      const existeGrupo = grupos.find( el=> el.id_grupo == id_grupo_encuesta )
      alumnosExciGrupo[i].id_plantel = existeGrupo ? existeGrupo.id_plantel : null 
    }

    /****************************************/
    /****************************************/

    // Esta línea se ocupa aquí, por que alugumos becados no se reinscriben, entonces... vale queso 
    // consultamos los alumnos inscritos actualmente
    const alumnosActuales   = await Kpi.getAlumnosInscritos( cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado ).then(response=>response)
    const idAlumnosActuales = alumnosActuales.map((registro)=>{ return registro.id_alumno })


    /****************************************/
    /*      C I C L O  A N T E R I O R      */
    /****************************************/

    // alumnos inscritos en el ciclo anteriro
    let alumnosInscritosPre   = await Kpi.getAlumnosInscritos( cicloPre.id_ciclo, cicloPre.id_ciclo_relacionado ).then(response=>response)
    // Sacar los alumnos becados en el ciclo actual, becados que son por que ya terminaron su plan de estudios ( ciclo anterior = ciclo actual, ciclo act = ciclo siguiente)
    let alumnosBecadosAct     = await Kpi.getAlumnosFuturaBeca( cicloPre.id_ciclo, cicloPre.id_ciclo_relacionado, cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado ).then(response=>response)
    // Sacamos los id, se van a necesitar para saber que alumnos son los reinscribibles
    let idBecadosAct          = alumnosBecadosAct.map((registro)=>{ return registro.id_alumno })
    // Alumnos reinscribibles para el ciclo anterior
    let alumnosRegularesPre     = alumnosInscritosPre.filter( el => { return el.adeudo == 0 && el.pagado > 0 && el.id_maestro > 0 && el.certificacion == 0 && !idBecadosAct.includes(el.id_alumno)})
    // Obtener los id de los alumnos regulares
    let idAlumnosRegularesPre = alumnosRegularesPre.map((registro)=>{ return registro.id_alumno })
    // Alumnos becados reinscribibles
    let alumnosBecadosReinscribibles = alumnosInscritosPre.filter(el=> { return el.adeudo == 0 && el.pagado == 0  && el.certificacion == 0 && el.id_maestro > 0})
    // Validar que los becados se hayan reinscrito de nuevo
    alumnosBecadosReinscribibles = alumnosBecadosReinscribibles.filter( el=> { return idAlumnosActuales.includes( el.id_alumno )})
    // Alumnos inscritos en el ciclo siguiente
    let alumnosRIAct          = await Kpi.getAlumnosInscritosCicloSiguiente( cicloPre.id_ciclo, cicloPre.id_ciclo_relacionado, cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado ).then(response=>response)
    // Sacamos los alumnos acumulados
    let alumnosAcumuladoPre  = alumnosRIAct.filter(el=> { return idAlumnosRegularesPre.includes(el.id_alumno) })

    // Sacar los NI
    let acumuladoPorSucursalFast     = await Kpi.acumuladoPorSucursalAll( cicloAct.id_ciclo_relacionado ).then(response => response)
    let acumuladoPorSucursalINBI     = await Kpi.acumuladoPorSucursalAll( cicloAct.id_ciclo ).then(response => response)
    let niPre = acumuladoPorSucursalFast.concat( acumuladoPorSucursalINBI )


    let idAlumnosBecados    = alumnosBecadosReinscribibles.map((registro)=>{ return registro.id_alumno })
    let idAlumnosAcumulados = alumnosAcumuladoPre.map((registro)=>{ return registro.id_alumno })
    let idAlumnosNi         = niPre.map((registro)=>{ return registro.id_alumno })
    let idAlumnosResagados    = idAlumnosBecados.concat( idAlumnosAcumulados ).concat( idAlumnosNi )
    // Sacar los alumnos resgados
    let alumnosActualNoPre  = await Kpi.alumnosActualNoPre( cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado, idAlumnosResagados ).then( response=> response )


    let alumnosCicloAnterior = niPre.concat(alumnosAcumuladoPre).concat(alumnosBecadosReinscribibles).concat(alumnosActualNoPre)
    /****************************************/
    /****************************************/

    // Aquíii vamos a limpiar los alumnos del ciclo anterior, por que no todosss se vuelven a inscribir
    alumnosCicloAnterior = alumnosCicloAnterior.filter( el=> { return idAlumnosActuales.includes(el.id_alumno)})

    // sacamos el id del alumno de los reinscribibles
    let alumnosRegulares  = alumnosActuales.filter( el => { return el.adeudo == 0 && el.pagado > 0 && el.id_maestro > 0 && el.certificacion == 0 })
    const idRegulares     = alumnosRegulares.map((registro)=>{ return registro.id_alumno })

    /****************************************/
    /*        C I C L O  A C T U A L        */
    /****************************************/

    // consultamos los alumnos inscritos actualmente
    const alumnosSiguientes = await Kpi.getAlumnosInscritosCicloSiguiente( cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado, cicloSig.id_ciclo, cicloSig.id_ciclo_relacionado ).then(response=>response)
    let alumnosBecaSiguiente = await Kpi.getAlumnosFuturaBeca( cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado, cicloSig.id_ciclo, cicloSig.id_ciclo_relacionado ).then(response=>response)
    let alumnosBecaSiguiente2 = await Kpi.getAlumnosBecaAutomatica( cicloSig.id_ciclo, cicloSig.id_ciclo_relacionado ).then(response=>response)

    if( alumnosBecaSiguiente2.length ){
      alumnosBecaSiguiente = alumnosBecaSiguiente.concat( alumnosBecaSiguiente2 )
    }
    
    const idBecadosSiguiente = alumnosBecaSiguiente.map((registro)=>{ return registro.id_alumno })

    /******** CALCULANDO RI ********/
    // Consultamos los planteles
    let planteles = await dashboardKpi.getPlanteles().then(response=> response)
    
    // Calular la cantidad de alumnos inscritos por plantel
    let riPlantelesFast = []
    let riPlantelesInbi = []

    let becadosBecados = []


    // Recorremos los planteles para agregarles lo del RI 
    for(const i in planteles){
      const { id_plantel, plantel, escuela } = planteles[i]
      const becadoCerti = alumnosBecaSiguiente.filter( el => el.id_plantel == id_plantel ).length

      let idAlumnosBecadosCerti = alumnosBecaSiguiente.filter( el => el.id_plantel == id_plantel ).map((registro) => { return registro.id_alumno })
      idAlumnosBecadosCerti = idAlumnosBecadosCerti.length ? idAlumnosBecadosCerti : [0]

      const alumnosPlantel          = alumnosActuales.filter(el=> { return el.id_plantel == id_plantel })
      // sacamos el total de alumnos en el ciclo
      const totalAlumnosActuales    = alumnosPlantel.length
      // Total de alumnos en el siguiente ciclo que se registraron ayer
      const totalAlumnosAyer        = alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel && el.fecha_alta_nuevo_grupo }).length
      // Alumnos en certificación sin beca
      const certificacion           = alumnosActuales.filter(el=> { return el.id_plantel == id_plantel && el.certificacion == 1  && el.pagado > 0 && el.adeudo == 0 }).length
      // Becados certificados
      const becadoCertificacion     = alumnosActuales.filter(el=> { return el.id_plantel == id_plantel && el.certificacion == 1  && el.pagado == 0 && el.adeudo == 0 }).length
      // Alumnos que no son de certificacion
      const alumnosNoCertificacion  = alumnosPlantel.filter(el=> { return el.id_plantel == id_plantel && el.certificacion == 0 && !idAlumnosBecadosCerti.includes( el.id_alumno ) })

      // Regularessss # REINSCRIBIBLES ( con maestro, sin adeudo y sin beca y no certificacion )
      let   regulares               = alumnosNoCertificacion.filter(el=> { return el.adeudo == 0 && el.pagado > 0 && el.id_maestro > 0 }).length

      // IRREGULARES ( sin maestro, o con adeudo y sin beca y no certificacion )
      const irregulares             = alumnosNoCertificacion.filter(el=> { return el.adeudo > 0 || !el.id_maestro }).length
      const alumnosIrregualres      = alumnosNoCertificacion.filter(el=> { return el.adeudo != 0 || !el.id_maestro })

      // alumnos becados
      let alumnosBecados          = alumnosNoCertificacion.filter(el=> { return el.adeudo == 0 && el.pagado == 0 && !idAlumnosExci.includes(el.id_alumno) })


      // Becados del exci
      let   becadosExci             = alumnosExciGrupo.filter(el=> { return el.id_plantel == id_plantel }).length
      const idBecaExci              = alumnosExciGrupo.filter(el=> { return el.id_plantel == id_plantel })
      const idBecaExciAlumnos       = idBecaExci.map((registro)=>{ return registro.id_alumno })
      

      // sacamos el total de alumnos en el siguiente ciclo los cuales es con respecto a los reinscribibles
      const idAlumnosPosibles = idRegulares.concat( idAlumnosExci )
      const totalAlumnosSiguientes  = alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel && idAlumnosPosibles.includes(el.id_alumno) && !idBecadosSiguiente.includes( el.id_alumno ) }).length

      // Sacar el % de alumnos que faltan por reinscribirse
      const porcentajeFaltantes     = totalAlumnosSiguientes > 0 ? (100-((totalAlumnosSiguientes/(regulares + becadosExci))*100).toFixed(2)).toFixed(2) : 100
      const porcentajeAvance        = totalAlumnosSiguientes > 0 ? ((totalAlumnosSiguientes/(regulares + becadosExci)) * 100).toFixed(2) : 0
 
      // console.log("2",porcentajeAvance)
 

      // Becados ( sin pagado y no certificacion)
      const becados                 = alumnosBecados.length

      let alumnosBecaCerti        = alumnosBecaSiguiente.filter( el => el.id_plantel == id_plantel )

      alumnosBecados = alumnosBecados.concat( alumnosBecaCerti )
      
      becadosBecados = becadosBecados.concat( alumnosNoCertificacion.filter(el=> { return el.adeudo == 0 && el.pagado == 0 }) )

      let alumnosBecaCertificacion = alumnosActuales.filter(el=> { return el.id_plantel == id_plantel && el.certificacion == 1  && el.pagado == 0 })
      alumnosBecaCertificacion     = alumnosBecaCertificacion.concat(alumnosBecaSiguiente.filter( el => el.id_plantel == id_plantel))

      // Acumulado anterior
      const acumuladoant           = alumnosAcumuladoPre.filter( el=> { return el.id_plantel == id_plantel }).length
      const becadosant             = alumnosBecadosReinscribibles.filter( el=> { return el.id_plantel == id_plantel }).length
      const niant                  = niPre.filter( el=> { return el.id_plantel == id_plantel }).length
      const resagados              = alumnosActualNoPre.filter( el=> { return el.id_plantel == id_plantel }).length

      let alumnosAreinscribir  = alumnosNoCertificacion.filter(el=> { return el.adeudo == 0 && el.pagado > 0 && el.id_maestro > 0 }).concat(idBecaExci)
      let alumnosReincribibles = alumnosAreinscribir.filter( el => { return !idBecaExciAlumnos.includes(el.id_alumno) })

      let idAlumnosSiguientes  = alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel && idRegulares.includes(el.id_alumno) }).map((registro) => { return registro.id_alumno })

      let alumnosFaltantes = alumnosReincribibles.filter(el=> { return el.id_plantel == id_plantel && !idAlumnosSiguientes.includes( el.id_alumno )})

      let payload = {
        id_plantel,
        plantel   ,

        acumuladoant,
        becadosant,
        niant,
        resagados,

        reinscribibles: (regulares + becadosExci),
        irregulares,
        becados,
        becadosExci,
        becadoCertificacion: becadoCertificacion + becadoCerti,
        certificacion,

        totalAlumnosActuales,
        totalAlumnosSiguientes,
        totalAlumnosAyer,

        faltantes: (regulares + becadosExci) - totalAlumnosSiguientes,
        porcentajeFaltantes,
        porcentajeAvance,
        alumnosIrregualres,
        alumnosBecados,
        alumnosPlantel, 
        alumnosReincribibles,
        alumnosSiguientes: alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel && idRegulares.includes(el.id_alumno) && !idBecadosSiguiente.includes( el.id_alumno ) }),
        alumnosBecaCertificacion,
        alumnosCertificacion: alumnosActuales.filter(el=> { return el.id_plantel == id_plantel && el.certificacion == 1  && el.pagado > 0 }),
        alumnosAcumulados: alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel && idRegulares.includes(el.id_alumno) }),
        escuela,
        alumnosFaltantes
      }

      if( escuela == 2 && ![19,20].includes(id_plantel)){
        riPlantelesFast.push( payload )
      }
      if( escuela == 1 && ![19,20].includes(id_plantel)){
        riPlantelesInbi.push( payload )
      }
    }

    let allReinscribibles = []
    let allAcumulado      = []
    let allFaltantes      = []

    for( const i in riPlantelesFast ){
      allReinscribibles = allReinscribibles.concat(riPlantelesFast[i].alumnosReincribibles )
      allAcumulado      = allAcumulado.concat(riPlantelesFast[i].alumnosAcumulados )
      allFaltantes      = allFaltantes.concat(riPlantelesFast[i].alumnosFaltantes )
    }

    let reinscribibles         = riPlantelesFast.map(item => item.reinscribibles).reduce((prev, curr) => prev + curr, 0)
    let irregulares            = riPlantelesFast.map(item => item.irregulares).reduce((prev, curr) => prev + curr, 0)
    let becados                = riPlantelesFast.map(item => item.becados).reduce((prev, curr) => prev + curr, 0)
    let becadoCertificacion    = riPlantelesFast.map(item => item.becadoCertificacion).reduce((prev, curr) => prev + curr, 0)
    let certificacion          = riPlantelesFast.map(item => item.certificacion).reduce((prev, curr) => prev + curr, 0)
    
    let totalAlumnosActuales   = riPlantelesFast.map(item => item.totalAlumnosActuales).reduce((prev, curr) => prev + curr, 0)
    let totalAlumnosSiguientes = riPlantelesFast.map(item => item.totalAlumnosSiguientes).reduce((prev, curr) => prev + curr, 0)
    let totalAlumnosAyer       = riPlantelesFast.map(item => item.totalAlumnosAyer).reduce((prev, curr) => prev + curr, 0)
    let faltantes              = riPlantelesFast.map(item => item.faltantes).reduce((prev, curr) => prev + curr, 0)
    let porcentajeAvance       = (100 - ((faltantes / reinscribibles)*100)).toFixed(2)
    
    let acumuladoant           = riPlantelesFast.map(item => item.acumuladoant).reduce((prev, curr) => prev + curr, 0)
    let becadosant             = riPlantelesFast.map(item => item.becadosant).reduce((prev, curr) => prev + curr, 0)
    let niant                  = riPlantelesFast.map(item => item.niant).reduce((prev, curr) => prev + curr, 0)
    let resagados              = riPlantelesFast.map(item => item.resagados).reduce((prev, curr) => prev + curr, 0)

    /* AGREGAR LOS TOTALES DE CADA TABLA */
    riPlantelesFast.push({
      id_plantel             : 0,
      plantel                : 'TOTAL',

      acumuladoant,
      becadosant,
      niant,
      resagados,

      reinscribibles,
      irregulares,
      becados,
      becadoCertificacion,
      certificacion,

      totalAlumnosActuales,
      totalAlumnosSiguientes,
      totalAlumnosAyer,
      faltantes: reinscribibles - totalAlumnosSiguientes,
      porcentajeAvance,
      escuela: 2,
      alumnosReincribibles: allReinscribibles,
      alumnosAcumulados   : allAcumulado,
      alumnosFaltantes    : allFaltantes
    })

    let regularesInbi              = riPlantelesInbi.map(item => item.reinscribibles).reduce((prev, curr) => prev + curr, 0)
    let irregularesInbi            = riPlantelesInbi.map(item => item.irregulares).reduce((prev, curr) => prev + curr, 0)
    let becadosInbi                = riPlantelesInbi.map(item => item.becados).reduce((prev, curr) => prev + curr, 0)
    let becadoCertificacionInbi    = riPlantelesInbi.map(item => item.becadoCertificacion).reduce((prev, curr) => prev + curr, 0)
    let certificacionInbi          = riPlantelesInbi.map(item => item.certificacion).reduce((prev, curr) => prev + curr, 0)
    
    let totalAlumnosActualesInbi   = riPlantelesInbi.map(item => item.totalAlumnosActuales).reduce((prev, curr) => prev + curr, 0)
    let totalAlumnosSiguientesInbi = riPlantelesInbi.map(item => item.totalAlumnosSiguientes).reduce((prev, curr) => prev + curr, 0)
    let totalAlumnosAyerInbi       = riPlantelesInbi.map(item => item.totalAlumnosAyer).reduce((prev, curr) => prev + curr, 0)
    let faltantesInbi              = riPlantelesInbi.map(item => item.faltantes).reduce((prev, curr) => prev + curr, 0)
    let porcentajeAvanceInbi       = (100 - ((faltantesInbi / regularesInbi)*100)).toFixed(2)
    let acumuladoantInbi           = riPlantelesInbi.map(item => item.acumuladoant).reduce((prev, curr) => prev + curr, 0)
    let becadosantInbi             = riPlantelesInbi.map(item => item.becadosant).reduce((prev, curr) => prev + curr, 0)
    let niantInbi                  = riPlantelesInbi.map(item => item.niant).reduce((prev, curr) => prev + curr, 0)
    let resagadosInbi              = riPlantelesInbi.map(item => item.resagados).reduce((prev, curr) => prev + curr, 0)

    allReinscribibles = []
    allAcumulado      = []
    allFaltantes      = []

    for( const i in riPlantelesInbi ){
      allReinscribibles = allReinscribibles.concat(riPlantelesInbi[i].alumnosReincribibles )
      allAcumulado      = allAcumulado.concat(riPlantelesInbi[i].alumnosAcumulados )
      allFaltantes      = allFaltantes.concat(riPlantelesInbi[i].alumnosFaltantes )
    }

    /* AGREGAR LOS TOTALES DE CADA TABLA */
    riPlantelesInbi.push({
      id_plantel             : 0,
      plantel                : 'TOTAL',
      acumuladoant           : acumuladoantInbi,
      becadosant             : becadosantInbi,
      niant                  : niantInbi,
      resagados              : resagadosInbi,

      reinscribibles         : regularesInbi,
      irregulares            : irregularesInbi,
      becados                : becadosInbi,
      becadoCertificacion    : becadoCertificacionInbi,
      certificacion          : certificacionInbi,

      totalAlumnosActuales   : totalAlumnosActualesInbi,
      totalAlumnosSiguientes : totalAlumnosSiguientesInbi,
      totalAlumnosAyer       : totalAlumnosAyerInbi,
      faltantes              : regularesInbi - totalAlumnosSiguientesInbi,
      porcentajeAvance       : porcentajeAvanceInbi,
      escuela: 1,
      alumnosReincribibles   : allReinscribibles,
      alumnosAcumulados      : allAcumulado,
      alumnosFaltantes       : allFaltantes
    })
    
    const totales = [{
      id_plantel             : 0,
      plantel                : 'TOTAL',
      acumuladoant           : acumuladoant  + acumuladoantInbi,
      becadosant             : becadosant    + becadosantInbi,
      niant                  : niant         + niantInbi,
      resagados              : resagados     + resagadosInbi,

      reinscribibles         : regularesInbi + reinscribibles,
      irregulares            : irregularesInbi + irregulares,
      becados                : becadosInbi + becados,
      becadoCertificacion    : becadoCertificacionInbi + becadoCertificacion,
      certificacion          : certificacionInbi + certificacion,

      totalAlumnosActuales   : totalAlumnosActualesInbi + totalAlumnosActuales,
      totalAlumnosSiguientes : totalAlumnosSiguientesInbi + totalAlumnosSiguientes,
      totalAlumnosAyer       : totalAlumnosAyerInbi + totalAlumnosAyer,
      faltantes              : (totalAlumnosActualesInbi - totalAlumnosSiguientesInbi) + (totalAlumnosActuales - totalAlumnosSiguientes),
      porcentajeAvance       : ((parseFloat( porcentajeAvanceInbi ) + parseFloat( porcentajeAvance )) / 2 ).toFixed(2),
      escuela: 3
    }]

    // Respuesta a enviar
    res.send({riPlantelesFast, riPlantelesInbi, totales, becadosBecados, alumnosCicloAnterior, alumnosActuales });
    
  } catch (error) {
    res.status(500).send({message: error ? error.message: 'Error :p'})
  }
};

exports.kpiInscripcionesInduccion = async(req, res) => {
  try {
    /* CALCULAR EL RI*/
    const { ciclo_inicial, semana_inicial, ciclo_final, semana_final, escuela } = req.body
    // Crearemos el arreglo a recorrer para realizar las consultas necesariassss
    const { fecha_inicio_ciclo } = ciclo_inicial
    const { fecha_fin_ciclo }    = ciclo_final

    const ciclosInduccion = await Kpi.getCiclosInduccionRango( escuela, fecha_inicio_ciclo, fecha_fin_ciclo ).then( response => response )
    const semanas = [1,2,3,4]

    let ciclosSemana = []

    // Recorremos los ciclos para ir agregando las semanas
    if( ciclosInduccion.length > 1 ){
      for( const i in ciclosInduccion){
        const { id_ciclo } = ciclosInduccion[i]
        for( const j in semanas ){

          // Primer ciclo y la semana no es número 1 
          if( i == 0 && (parseInt(j) + 1) >= semana_inicial ){
            const semana = ( parseInt( j ) + 1 )
            ciclosSemana.push({ id_ciclo, semana })
          }

          // Ciclos de en medio
          if( i > 0  && i  < (ciclosInduccion.length - 1) ){
            const semana = ( parseInt( j ) + 1 )
            ciclosSemana.push({ id_ciclo, semana })
          }

          // Ciclo final
          if( i  == ( ciclosInduccion.length - 1 ) && (parseInt(j) + 1) <= semana_final){
            const semana = ( parseInt( j ) + 1 )
            ciclosSemana.push({ id_ciclo, semana })
          }

        }
      }
    }else{
      const { id_ciclo } = ciclosInduccion[0]
      for( const j in semanas ){
        // Primer ciclo y la semana no es número 1 
        if( (parseInt(j) + 1) >= semana_inicial && (parseInt(j) + 1) <= semana_final ){
          const semana = ( parseInt( j ) + 1 )
          ciclosSemana.push({ id_ciclo, semana })
        }
      }
    }


    // Recorremos los ciclos para ir agregando las semanas
    let listadoAlumnos = []
    for( const i in ciclosSemana){
      const { id_ciclo, semana } = ciclosSemana[i]

      // Aquí hay que consultar la información de ese ciclo con esa semana
      const alumnos = await Kpi.getCiclosInduccionCicloSemana( id_ciclo, semana ).then( response => response )
      listadoAlumnos = listadoAlumnos.concat( alumnos )
    }

    // Hay que sacar los números de celular del alumno para poder ver la vendedora
    const celulares       = listadoAlumnos.map((registro) => { return registro.celular })
    const telefonos       = listadoAlumnos.map((registro) => { return registro.telefono })
    const idAlumnos       = listadoAlumnos.map((registro) => { return registro.id_alumno })
    const telefonoTutor   = listadoAlumnos.map((registro) => { return registro.telefonoTutor })
    const celularTutor    = listadoAlumnos.map((registro) => { return registro.celularTutor })

    // Consultar los alumnos con esos teléfonos en los prospectos
    const prospectosVendedora = await prospectos.getProspectosxTelefonoOrIdAlumno( celulares, telefonos, idAlumnos, telefonoTutor, celularTutor ).then( response => response )
    const idVendedoras        = prospectosVendedora.map(( registro ) => { return registro.usuario_asignado })
    // Obtener el nombre de las vendedoras
    const nombreVendedoras    = idVendedoras.length ? await Kpi.getUsuariosxId( idVendedoras ).then( response => response ) : []

    // Vamos a agregar el estatus a los alumnos
    for( const i in listadoAlumnos ){
      const { adeudo, pagado, nuevo_grupo, id_alumno, celular, pago_induccion, unidad_negocio, telefono, telefonoTutor, celularTutor } = listadoAlumnos[i]
      // Buscar vendedora
      const existeVendedora = prospectosVendedora.find( el => el.idalumno == id_alumno && el.escuela == unidad_negocio 
        || el.telefono == celular && el.escuela == unidad_negocio
        || el.telefono == telefono && el.escuela == unidad_negocio
        || el.telefono == telefonoTutor && el.escuela == unidad_negocio
        || el.telefono == celularTutor && el.escuela == unidad_negocio )
      if( existeVendedora ){
        // Ahora validamos que exista el nombre de esa vendedora 
        const existeNombre = nombreVendedoras.find( el => el.id_usuario == existeVendedora.usuario_asignado )
        listadoAlumnos[i]['vendedora']      = existeNombre ? existeNombre.nombre_completo : 'Sin vendedora'
        listadoAlumnos[i]['fuente']         = existeVendedora ? existeVendedora.fuente : 'NA'
        listadoAlumnos[i]['detalle_fuente'] = existeVendedora ? existeVendedora.detalle_fuente : 'NA'


      }else{ listadoAlumnos[i]['vendedora'] = 'Sin vendedora' }

      // Parcial aquel que no tiene grupo o adeudo
      // Validamos que no tenga grupo asignado
      if( !nuevo_grupo ){
        listadoAlumnos[i]['estatus'] = 1 // Parcial
        // Si el alumno pago solo su cuso de induccion <= 200, entonces sería sin liquidación
        if( pago_induccion <= 200 ){ listadoAlumnos[i]['motivo'] = 'Sin liquidación' }
        // Validamos el pago de inducción que se dió para saber si liquido
        const pagos = [1399,1400,925,1540,849,850]
        if( pagos.includes(pago_induccion) ){ listadoAlumnos[i]['motivo'] = 'Liquidado sin grupo' }

        // Validamos si es un pago parcial
        if( !pagos.includes(pago_induccion) && pago_induccion > 200 ){ listadoAlumnos[i]['motivo'] = 'Pago parcial' }
      }

      // Parcial, con grupo pero con adeudo
      if( nuevo_grupo && adeudo > 0 ){
        listadoAlumnos[i]['estatus'] = 1 // Parcial
        // Si el alumno pago solo su cuso de induccion <= 200, entonces sería sin liquidación
        if( adeudo > 0 ){ listadoAlumnos[i]['motivo'] = 'Con adeudo' }
      }

      // Final aquel sin adeudo, hizo un pago mayor a 0 y tiene un grupo
      if( adeudo == 0 && pagado > 0 && nuevo_grupo ){
        listadoAlumnos[i]['estatus'] = 2 // Final
        listadoAlumnos[i]['motivo']  = 'Todo correcto' // Parcial
      }

      // Becado, aquel que no tiene adeudo y no pago nada
      if( adeudo == 0 && pagado == 0 ){
        listadoAlumnos[i]['estatus'] = 3 // Becado
        listadoAlumnos[i]['motivo']  = 'Sin pago y sin adeudo' // Becado
      }

    }


    const parciales = listadoAlumnos.filter( el => { return el.estatus == 1 }).length
    const finales   = listadoAlumnos.filter( el => { return el.estatus == 2 }).length
    const becados   = listadoAlumnos.filter( el => { return el.estatus == 3 }).length
    const total     = listadoAlumnos.length

    // Respuesta a enviar
    res.send( {listadoAlumnos, parciales, becados, finales, total } );
    
  } catch (error) {
    res.status(500).send({message: error ? error.message: 'Error :p'})
  }
};

exports.getCantActualTeachers = async(req, res) => {

  try {
    /* CALCULAR EL RI*/
    const { cicloPre, cicloAct, cicloSig } = req.body


    /****************************************/
    /*                E X C I               */
    /****************************************/
    let registros = await exci.getRegistrosExciBeca( ).then( response => response )
    // Solo los que ya tienen grupo
    const alumnosExciGrupo = registros.filter( el=> { return el.id_grupo && [cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado].includes(el.id_ciclo)})
    // Sacamos los idAlumnosExci
    const idAlumnosExci = alumnosExciGrupo.map((registro)=>{ return registro.id_alumno })
    const idsGrupos     = alumnosExciGrupo.map((registro)=>{ return registro.id_grupo_encuesta })
    // Consultadmos las becas
    let grupos = []
    if( idAlumnosExci.length > 0 ){ grupos  = await exci.gruposExci( idsGrupos ).then( response => response ) }

    for( const i in alumnosExciGrupo ){
      const { id_grupo_encuesta, id_alumno } = alumnosExciGrupo[i]
      const existeGrupo = grupos.find( el=> el.id_grupo == id_grupo_encuesta )
      alumnosExciGrupo[i].id_plantel   = existeGrupo ? existeGrupo.id_plantel   : null 
      alumnosExciGrupo[i].id_maestro   = existeGrupo ? existeGrupo.id_maestro   : null 
      alumnosExciGrupo[i].id_maestro_2 = existeGrupo ? existeGrupo.id_maestro_2 : null 
    }

    /****************************************/
    /****************************************/

    // Esta línea se ocupa aquí, por que alugumos becados no se reinscriben, entonces... vale queso 
    // consultamos los alumnos inscritos actualmente
    const alumnosActuales   = await Kpi.getAlumnosInscritos( cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado ).then(response=>response)
    const idAlumnosActuales = alumnosActuales.map((registro)=>{ return registro.id_alumno })


    /****************************************/
    /*      C I C L O  A N T E R I O R      */
    /****************************************/

    // alumnos inscritos en el ciclo anteriro
    let alumnosInscritosPre   = await Kpi.getAlumnosInscritos( cicloPre.id_ciclo, cicloPre.id_ciclo_relacionado ).then(response=>response)
    // Sacar los alumnos becados en el ciclo actual, becados que son por que ya terminaron su plan de estudios ( ciclo anterior = ciclo actual, ciclo act = ciclo siguiente)
    let alumnosBecadosAct     = await Kpi.getAlumnosFuturaBeca( cicloPre.id_ciclo, cicloPre.id_ciclo_relacionado, cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado ).then(response=>response)
    // Sacamos los id, se van a necesitar para saber que alumnos son los reinscribibles
    let idBecadosAct          = alumnosBecadosAct.map((registro)=>{ return registro.id_alumno })
    // Alumnos reinscribibles para el ciclo anterior
    let alumnosRegularesPre     = alumnosInscritosPre.filter( el => { return el.adeudo == 0 && el.pagado > 0 && el.id_maestro > 0 && el.certificacion == 0 && !idBecadosAct.includes(el.id_alumno)})
    // Obtener los id de los alumnos regulares
    let idAlumnosRegularesPre = alumnosRegularesPre.map((registro)=>{ return registro.id_alumno })
    // Alumnos becados reinscribibles
    let alumnosBecadosReinscribibles = alumnosInscritosPre.filter(el=> { return el.adeudo == 0 && el.pagado == 0  && el.certificacion == 0 && el.id_maestro > 0})
    // Validar que los becados se hayan reinscrito de nuevo
    alumnosBecadosReinscribibles = alumnosBecadosReinscribibles.filter( el=> { return idAlumnosActuales.includes( el.id_alumno )})
    // Alumnos inscritos en el ciclo siguiente
    let alumnosRIAct          = await Kpi.getAlumnosInscritosCicloSiguiente( cicloPre.id_ciclo, cicloPre.id_ciclo_relacionado, cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado ).then(response=>response)
    // Sacamos los alumnos acumulados
    let alumnosAcumuladoPre  = alumnosRIAct.filter(el=> { return idAlumnosRegularesPre.includes(el.id_alumno) })

    // Sacar los NI
    let acumuladoPorSucursalFast     = await Kpi.acumuladoPorSucursalAll( cicloAct.id_ciclo_relacionado ).then(response => response)
    let acumuladoPorSucursalINBI     = await Kpi.acumuladoPorSucursalAll( cicloAct.id_ciclo ).then(response => response)
    let niPre = acumuladoPorSucursalFast.concat( acumuladoPorSucursalINBI )


    let idAlumnosBecados    = alumnosBecadosReinscribibles.map((registro)=>{ return registro.id_alumno })
    let idAlumnosAcumulados = alumnosAcumuladoPre.map((registro)=>{ return registro.id_alumno })
    let idAlumnosNi         = niPre.map((registro)=>{ return registro.id_alumno })
    let idAlumnosResagados    = idAlumnosBecados.concat( idAlumnosAcumulados ).concat( idAlumnosNi )
    // Sacar los alumnos resgados
    let alumnosActualNoPre  = await Kpi.alumnosActualNoPre( cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado, idAlumnosResagados ).then( response=> response )

    // console.log( niPre.length, alumnosAcumuladoPre.length, alumnosBecadosReinscribibles.length, alumnosActualNoPre.length )

    let alumnosCicloAnterior = niPre.concat(alumnosAcumuladoPre).concat(alumnosBecadosReinscribibles).concat(alumnosActualNoPre)
    // console.log( alumnosCicloAnterior.length )


    // console.log( alumnosBecadosReinscribibles.filter( el => { return [7772,8054,8648].includes(el.id_alumno) }))
    /****************************************/
    /****************************************/

    // Aquíii vamos a limpiar los alumnos del ciclo anterior, por que no todosss se vuelven a inscribir
    alumnosCicloAnterior = alumnosCicloAnterior.filter( el=> { return idAlumnosActuales.includes(el.id_alumno)})

    // sacamos el id del alumno de los reinscribibles
    let alumnosRegulares  = alumnosActuales.filter( el => { return el.adeudo == 0 && el.pagado > 0 && el.id_maestro > 0 && el.certificacion == 0 })
    const idRegulares     = alumnosRegulares.map((registro)=>{ return registro.id_alumno })

    // consultamos los alumnos inscritos actualmente
    const alumnosSiguientes = await Kpi.getAlumnosInscritosCicloSiguiente( cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado, cicloSig.id_ciclo, cicloSig.id_ciclo_relacionado ).then(response=>response)

    const alumnosBecaSiguiente = await Kpi.getAlumnosFuturaBeca( cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado, cicloSig.id_ciclo, cicloSig.id_ciclo_relacionado ).then(response=>response)
    const idBecadosSiguiente = alumnosBecaSiguiente.map((registro)=>{ return registro.id_alumno })

    /******** CALCULANDO RI ********/
    // Consultamos los planteles
    let planteles = await dashboardKpi.getPlanteles().then(response=> response)

    let teachers = await Kpi.getTeachersAll( cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado ).then(response=> response)

    // console.log( 'teachers', teachers )
    
    // Calular la cantidad de alumnos inscritos por plantel
    let riPlantelesFast = []
    let riPlantelesInbi = []

    let riTeachers = []

    let becadosBecados = []

    for(const i in teachers){
      const { id_maestro, nombre_completo } = teachers[i]
      const becadoCerti      = alumnosBecaSiguiente.filter( el =>{ return el.id_maestro == id_maestro ||  el.id_maestro_2 == id_maestro }).length
      const alumnosPlantel   = alumnosActuales.filter(el=> { return el.id_maestro == id_maestro || el.id_maestro_2 == id_maestro })
      
      // sacamos el total de alumnos en el ciclo
      const totalAlumnosActuales    = alumnosPlantel.length

      // Total de alumnos en el siguiente ciclo que se registraron ayer
      const totalAlumnosAyer  = alumnosSiguientes.filter(el=> { return el.id_maestro == id_maestro && el.fecha_alta_nuevo_grupo || el.id_maestro_2 == id_maestro && el.fecha_alta_nuevo_grupo }).length

      // Alumnos en certificación sin beca
      const certificacion   = alumnosActuales.filter(el=> { return
                              el.id_maestro == id_maestro && el.certificacion == 1 && el.pagado > 0 && el.adeudo == 0 || el.id_maestro_2 == id_maestro && el.certificacion == 1 && el.pagado > 0 && el.adeudo == 0 }).length

      // Becados certificados
      const becadoCertificacion   = alumnosActuales.filter(el=> { return el.id_maestro == id_maestro && el.certificacion == 1  && el.pagado == 0 && el.adeudo == 0 || el.id_maestro_2 == id_maestro && el.certificacion == 1  && el.pagado == 0 && el.adeudo == 0 }).length
      
      // Alumnos que no son de certificacion
      const alumnosNoCertificacion  = alumnosPlantel.filter(el=> { return el.id_maestro == id_maestro && el.certificacion == 0 || el.id_maestro_2 == id_maestro && el.certificacion == 0 })

      // Regularessss # REINSCRIBIBLES ( con maestro, sin adeudo y sin beca y no certificacion )
      const regulares               = alumnosNoCertificacion.filter(el=> { return el.adeudo == 0 && el.pagado > 0 && el.id_maestro > 0 }).length

      // IRREGULARES ( sin maestro, o con adeudo y sin beca y no certificacion )
      const irregulares             = alumnosNoCertificacion.filter(el=> { return el.adeudo > 0 || !el.id_maestro }).length
      const alumnosIrregualres      = alumnosNoCertificacion.filter(el=> { return el.adeudo != 0 || !el.id_maestro })

      // alumnos becados
      const alumnosBecados          = alumnosNoCertificacion.filter(el=> { return el.adeudo == 0 && el.pagado == 0 && !idAlumnosExci.includes(el.id_alumno) })

      // Becados del exci
      const becadosExci             = alumnosExciGrupo.filter(el=> { return el.id_maestro == id_maestro ||  el.id_maestro_2 == id_maestro }).length
      const idBecaExci              = alumnosExciGrupo.filter(el=> { return el.id_maestro == id_maestro ||  el.id_maestro_2 == id_maestro })
      const idBecaExciAlumnos       = idBecaExci.map((registro)=>{ return registro.id_alumno })
      

      // sacamos el total de alumnos en el siguiente ciclo los cuales es con respecto a los reinscribibles
      const idAlumnosPosibles = idRegulares.concat( idAlumnosExci )
      const totalAlumnosSiguientes  = alumnosSiguientes.filter(el=> { return el.id_maestro == id_maestro && idAlumnosPosibles.includes(el.id_alumno) && !idBecadosSiguiente.includes( el.id_alumno ) || el.id_maestro_2 == id_maestro && idAlumnosPosibles.includes(el.id_alumno) && !idBecadosSiguiente.includes( el.id_alumno ) }).length

      // Sacar el % de alumnos que faltan por reinscribirse
      const porcentajeFaltantes     = totalAlumnosSiguientes > 0 ? (100-((totalAlumnosSiguientes/(regulares + becadosExci))*100).toFixed(2)).toFixed(2) : 100

      // Becados ( sin pagado y no certificacion)
      const becados                 = alumnosBecados.length
      becadosBecados = becadosBecados.concat( alumnosNoCertificacion.filter(el=> { return el.adeudo == 0 && el.pagado == 0 }) )

      let alumnosBecaCertificacion = alumnosActuales.filter(el=> { return el.id_maestro == id_maestro && el.certificacion == 1  && el.pagado == 0 || el.id_maestro_2 == id_maestro && el.certificacion == 1  && el.pagado == 0 })

      alumnosBecaCertificacion     = alumnosBecaCertificacion.concat(alumnosBecaSiguiente.filter( el => { return el.id_maestro == id_maestro ||  el.id_maestro_2 == id_maestro }))

      // Acumulado anterior
      const acumuladoant           = alumnosAcumuladoPre.filter( el => { return el.id_maestro == id_maestro ||  el.id_maestro_2 == id_maestro }).length
      const becadosant             = alumnosBecadosReinscribibles.filter( el => { return el.id_maestro == id_maestro ||  el.id_maestro_2 == id_maestro }).length
      const niant                  = niPre.filter( el => { return el.id_maestro == id_maestro ||  el.id_maestro_2 == id_maestro }).length
      const resagados              = alumnosActualNoPre.filter( el => { return el.id_maestro == id_maestro ||  el.id_maestro_2 == id_maestro }).length

      let alumnosAreinscribir      = alumnosNoCertificacion.filter(el=> { return el.adeudo == 0 && el.pagado > 0 && el.id_maestro > 0 }).concat(idBecaExci)
      let alumnosReincribibles     = alumnosAreinscribir.filter( el => { return !idBecaExciAlumnos.includes(el.id_alumno) })

      let payload = {
        id_maestro,
        nombre_completo   ,

        acumuladoant,
        becadosant,
        niant,
        resagados,

        reinscribibles: (regulares + becadosExci) - becadoCerti,
        irregulares,
        becados,
        becadosExci,
        becadoCertificacion: becadoCertificacion + becadoCerti,
        certificacion,

        totalAlumnosActuales,
        totalAlumnosSiguientes,
        totalAlumnosAyer,

        faltantes: (regulares + becadosExci) - totalAlumnosSiguientes - becadoCerti,
        porcentajeFaltantes,
        alumnosIrregualres,
        alumnosBecados,
        alumnosPlantel, 
        alumnosReincribibles,
        alumnosSiguientes: alumnosSiguientes.filter(el=> { return el.id_maestro == id_maestro && idRegulares.includes(el.id_alumno) && !idBecadosSiguiente.includes( el.id_alumno ) || el.id_maestro_2 == id_maestro && idRegulares.includes(el.id_alumno) && !idBecadosSiguiente.includes( el.id_alumno ) }),

        alumnosBecaCertificacion,
        alumnosCertificacion: alumnosActuales.filter(el=> { return el.id_maestro == id_maestro && el.certificacion == 1  && el.pagado > 0 || el.id_maestro_2 == id_maestro && el.certificacion == 1  && el.pagado > 0 }),

        alumnosAcumulados: alumnosSiguientes.filter(el=> { return el.id_maestro == id_maestro && idRegulares.includes(el.id_alumno) || el.id_maestro_2 == id_maestro && idRegulares.includes(el.id_alumno) }),
      }

      riTeachers.push( payload )
    }

    let reinscribiblesA         = riTeachers.map(item => item.reinscribibles).reduce((prev, curr) => prev + curr, 0)
    let irregularesA            = riTeachers.map(item => item.irregulares).reduce((prev, curr) => prev + curr, 0)
    let becadosA                = riTeachers.map(item => item.becados).reduce((prev, curr) => prev + curr, 0)
    let becadoCertificacionA    = riTeachers.map(item => item.becadoCertificacion).reduce((prev, curr) => prev + curr, 0)
    let certificacionA          = riTeachers.map(item => item.certificacion).reduce((prev, curr) => prev + curr, 0)
    
    let totalAlumnosActualesA   = riTeachers.map(item => item.totalAlumnosActuales).reduce((prev, curr) => prev + curr, 0)
    let totalAlumnosSiguientesA = riTeachers.map(item => item.totalAlumnosSiguientes).reduce((prev, curr) => prev + curr, 0)
    let totalAlumnosAyerA       = riTeachers.map(item => item.totalAlumnosAyer).reduce((prev, curr) => prev + curr, 0)
    let faltantesA              = riTeachers.map(item => item.faltantes).reduce((prev, curr) => prev + curr, 0)
    let porcentajeFaltantesA    = ((faltantesA / reinscribiblesA)*100).toFixed(2)

    let acumuladoantA           = riTeachers.map(item => item.acumuladoant).reduce((prev, curr) => prev + curr, 0)
    let becadosantA             = riTeachers.map(item => item.becadosant).reduce((prev, curr) => prev + curr, 0)
    let niantA                  = riTeachers.map(item => item.niant).reduce((prev, curr) => prev + curr, 0)
    let resagadosA              = riTeachers.map(item => item.resagados).reduce((prev, curr) => prev + curr, 0)

    /* AGREGAR LOS TOTALES DE CADA TABLA */
    riTeachers.push({
      id_maestro      : 100000,
      nombre_completo : 'TOTAL',

      acumuladoant:acumuladoantA,
      becadosant:becadosantA,
      niant:niantA,
      resagados:resagadosA,

      reinscribibles:reinscribiblesA,
      irregulares:irregularesA,
      becados:becadosA,
      becadoCertificacion:becadoCertificacionA,
      certificacion:certificacionA,

      totalAlumnosActuales:totalAlumnosActualesA,
      totalAlumnosSiguientes:totalAlumnosSiguientesA,
      totalAlumnosAyer:totalAlumnosAyerA,
      faltantes: reinscribiblesA - totalAlumnosSiguientesA,
      porcentajeFaltantes: porcentajeFaltantesA,
      escuela: 2
    })

    // Recorremos los planteles para agregarles lo del RI 
    for(const i in planteles){
      const { id_plantel, plantel, escuela } = planteles[i]
      const becadoCerti = alumnosBecaSiguiente.filter( el => el.id_plantel == id_plantel ).length

      const alumnosPlantel          = alumnosActuales.filter(el=> { return el.id_plantel == id_plantel })
      // sacamos el total de alumnos en el ciclo
      const totalAlumnosActuales    = alumnosPlantel.length
      // Total de alumnos en el siguiente ciclo que se registraron ayer
      const totalAlumnosAyer        = alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel && el.fecha_alta_nuevo_grupo }).length
      // Alumnos en certificación sin beca
      const certificacion           = alumnosActuales.filter(el=> { return el.id_plantel == id_plantel && el.certificacion == 1  && el.pagado > 0 && el.adeudo == 0 }).length
      // Becados certificados
      const becadoCertificacion     = alumnosActuales.filter(el=> { return el.id_plantel == id_plantel && el.certificacion == 1  && el.pagado == 0 && el.adeudo == 0 }).length
      // Alumnos que no son de certificacion
      const alumnosNoCertificacion  = alumnosPlantel.filter(el=> { return el.id_plantel == id_plantel && el.certificacion == 0 })

      // Regularessss # REINSCRIBIBLES ( con maestro, sin adeudo y sin beca y no certificacion )
      const regulares               = alumnosNoCertificacion.filter(el=> { return el.adeudo == 0 && el.pagado > 0 && el.id_maestro > 0 }).length

      // IRREGULARES ( sin maestro, o con adeudo y sin beca y no certificacion )
      const irregulares             = alumnosNoCertificacion.filter(el=> { return el.adeudo > 0 || !el.id_maestro }).length
      const alumnosIrregualres      = alumnosNoCertificacion.filter(el=> { return el.adeudo != 0 || !el.id_maestro })

      // alumnos becados
      const alumnosBecados          = alumnosNoCertificacion.filter(el=> { return el.adeudo == 0 && el.pagado == 0 && !idAlumnosExci.includes(el.id_alumno) })

      // Becados del exci
      const becadosExci             = alumnosExciGrupo.filter(el=> { return el.id_plantel == id_plantel }).length
      const idBecaExci              = alumnosExciGrupo.filter(el=> { return el.id_plantel == id_plantel })
      const idBecaExciAlumnos       = idBecaExci.map((registro)=>{ return registro.id_alumno })
      

      // sacamos el total de alumnos en el siguiente ciclo los cuales es con respecto a los reinscribibles
      const idAlumnosPosibles = idRegulares.concat( idAlumnosExci )
      const totalAlumnosSiguientes  = alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel && idAlumnosPosibles.includes(el.id_alumno) && !idBecadosSiguiente.includes( el.id_alumno ) }).length

      // Sacar el % de alumnos que faltan por reinscribirse
      const porcentajeFaltantes     = totalAlumnosSiguientes > 0 ? (100-((totalAlumnosSiguientes/(regulares + becadosExci))*100).toFixed(2)).toFixed(2) : 100

      // Becados ( sin pagado y no certificacion)
      const becados                 = alumnosBecados.length
      becadosBecados = becadosBecados.concat( alumnosNoCertificacion.filter(el=> { return el.adeudo == 0 && el.pagado == 0 }) )

      let alumnosBecaCertificacion = alumnosActuales.filter(el=> { return el.id_plantel == id_plantel && el.certificacion == 1  && el.pagado == 0 })
      alumnosBecaCertificacion     = alumnosBecaCertificacion.concat(alumnosBecaSiguiente.filter( el => el.id_plantel == id_plantel))

      // Acumulado anterior
      const acumuladoant           = alumnosAcumuladoPre.filter( el=> { return el.id_plantel == id_plantel }).length
      const becadosant             = alumnosBecadosReinscribibles.filter( el=> { return el.id_plantel == id_plantel }).length
      const niant                  = niPre.filter( el=> { return el.id_plantel == id_plantel }).length
      const resagados              = alumnosActualNoPre.filter( el=> { return el.id_plantel == id_plantel }).length

      let alumnosAreinscribir      = alumnosNoCertificacion.filter(el=> { return el.adeudo == 0 && el.pagado > 0 && el.id_maestro > 0 }).concat(idBecaExci)
      let alumnosReincribibles     = alumnosAreinscribir.filter( el => { return !idBecaExciAlumnos.includes(el.id_alumno) })

      let payload = {
        id_plantel,
        plantel   ,

        acumuladoant,
        becadosant,
        niant,
        resagados,

        reinscribibles: (regulares + becadosExci) - becadoCerti,
        irregulares,
        becados,
        becadosExci,
        becadoCertificacion: becadoCertificacion + becadoCerti,
        certificacion,

        totalAlumnosActuales,
        totalAlumnosSiguientes,
        totalAlumnosAyer,

        faltantes: (regulares + becadosExci) - totalAlumnosSiguientes - becadoCerti,
        porcentajeFaltantes,
        alumnosIrregualres,
        alumnosBecados,
        alumnosPlantel, 
        alumnosReincribibles,
        alumnosSiguientes: alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel && idRegulares.includes(el.id_alumno) && !idBecadosSiguiente.includes( el.id_alumno ) }),
        alumnosBecaCertificacion,
        alumnosCertificacion: alumnosActuales.filter(el=> { return el.id_plantel == id_plantel && el.certificacion == 1  && el.pagado > 0 }),
        alumnosAcumulados: alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel && idRegulares.includes(el.id_alumno) }),
        escuela,
      }

      if(regulares > 0 && escuela == 2){
        riPlantelesFast.push( payload )
      }
      if(regulares > 0 && escuela == 1){
        riPlantelesInbi.push( payload )
      }
    }

    let reinscribibles         = riPlantelesFast.map(item => item.reinscribibles).reduce((prev, curr) => prev + curr, 0)
    let irregulares            = riPlantelesFast.map(item => item.irregulares).reduce((prev, curr) => prev + curr, 0)
    let becados                = riPlantelesFast.map(item => item.becados).reduce((prev, curr) => prev + curr, 0)
    let becadoCertificacion    = riPlantelesFast.map(item => item.becadoCertificacion).reduce((prev, curr) => prev + curr, 0)
    let certificacion          = riPlantelesFast.map(item => item.certificacion).reduce((prev, curr) => prev + curr, 0)
    
    let totalAlumnosActuales   = riPlantelesFast.map(item => item.totalAlumnosActuales).reduce((prev, curr) => prev + curr, 0)
    let totalAlumnosSiguientes = riPlantelesFast.map(item => item.totalAlumnosSiguientes).reduce((prev, curr) => prev + curr, 0)
    let totalAlumnosAyer       = riPlantelesFast.map(item => item.totalAlumnosAyer).reduce((prev, curr) => prev + curr, 0)
    let faltantes              = riPlantelesFast.map(item => item.faltantes).reduce((prev, curr) => prev + curr, 0)
    let porcentajeFaltantes    = ((faltantes / reinscribibles)*100).toFixed(2)

    let acumuladoant           = riPlantelesFast.map(item => item.acumuladoant).reduce((prev, curr) => prev + curr, 0)
    let becadosant             = riPlantelesFast.map(item => item.becadosant).reduce((prev, curr) => prev + curr, 0)
    let niant                  = riPlantelesFast.map(item => item.niant).reduce((prev, curr) => prev + curr, 0)
    let resagados              = riPlantelesFast.map(item => item.resagados).reduce((prev, curr) => prev + curr, 0)

    /* AGREGAR LOS TOTALES DE CADA TABLA */
    riPlantelesFast.push({
      id_plantel             : 0,
      plantel                : 'TOTAL',

      acumuladoant,
      becadosant,
      niant,
      resagados,

      reinscribibles,
      irregulares,
      becados,
      becadoCertificacion,
      certificacion,

      totalAlumnosActuales,
      totalAlumnosSiguientes,
      totalAlumnosAyer,
      faltantes: reinscribibles - totalAlumnosSiguientes,
      porcentajeFaltantes,
      escuela: 2
    })

    let regularesInbi              = riPlantelesInbi.map(item => item.reinscribibles).reduce((prev, curr) => prev + curr, 0)
    let irregularesInbi            = riPlantelesInbi.map(item => item.irregulares).reduce((prev, curr) => prev + curr, 0)
    let becadosInbi                = riPlantelesInbi.map(item => item.becados).reduce((prev, curr) => prev + curr, 0)
    let becadoCertificacionInbi    = riPlantelesInbi.map(item => item.becadoCertificacion).reduce((prev, curr) => prev + curr, 0)
    let certificacionInbi          = riPlantelesInbi.map(item => item.certificacion).reduce((prev, curr) => prev + curr, 0)
    
    let totalAlumnosActualesInbi   = riPlantelesInbi.map(item => item.totalAlumnosActuales).reduce((prev, curr) => prev + curr, 0)
    let totalAlumnosSiguientesInbi = riPlantelesInbi.map(item => item.totalAlumnosSiguientes).reduce((prev, curr) => prev + curr, 0)
    let totalAlumnosAyerInbi       = riPlantelesInbi.map(item => item.totalAlumnosAyer).reduce((prev, curr) => prev + curr, 0)
    let faltantesInbi              = riPlantelesInbi.map(item => item.faltantes).reduce((prev, curr) => prev + curr, 0)
    let porcentajeFaltantesInbi    = ((faltantesInbi / regularesInbi)*100).toFixed(2)
    let acumuladoantInbi           = riPlantelesInbi.map(item => item.acumuladoant).reduce((prev, curr) => prev + curr, 0)
    let becadosantInbi             = riPlantelesInbi.map(item => item.becadosant).reduce((prev, curr) => prev + curr, 0)
    let niantInbi                  = riPlantelesInbi.map(item => item.niant).reduce((prev, curr) => prev + curr, 0)
    let resagadosInbi              = riPlantelesInbi.map(item => item.resagados).reduce((prev, curr) => prev + curr, 0)


    /* AGREGAR LOS TOTALES DE CADA TABLA */
    riPlantelesInbi.push({
      id_plantel             : 0,
      plantel                : 'TOTAL',
      acumuladoant           : acumuladoantInbi,
      becadosant             : becadosantInbi,
      niant                  : niantInbi,
      resagados              : resagadosInbi,

      reinscribibles         : regularesInbi,
      irregulares            : irregularesInbi,
      becados                : becadosInbi,
      becadoCertificacion    : becadoCertificacionInbi,
      certificacion          : certificacionInbi,

      totalAlumnosActuales   : totalAlumnosActualesInbi,
      totalAlumnosSiguientes : totalAlumnosSiguientesInbi,
      totalAlumnosAyer       : totalAlumnosAyerInbi,
      faltantes              : regularesInbi - totalAlumnosSiguientesInbi,
      porcentajeFaltantes    : porcentajeFaltantesInbi,
      escuela: 1
    })

    const totales = [{
      id_plantel             : 0,
      plantel                : 'TOTAL',
      acumuladoant           : acumuladoant  + acumuladoantInbi,
      becadosant             : becadosant    + becadosantInbi,
      niant                  : niant         + niantInbi,
      resagados              : resagados     + resagadosInbi,

      reinscribibles         : regularesInbi + reinscribibles,
      irregulares            : irregularesInbi + irregulares,
      becados                : becadosInbi + becados,
      becadoCertificacion    : becadoCertificacionInbi + becadoCertificacion,
      certificacion          : certificacionInbi + certificacion,

      totalAlumnosActuales   : totalAlumnosActualesInbi + totalAlumnosActuales,
      totalAlumnosSiguientes : totalAlumnosSiguientesInbi + totalAlumnosSiguientes,
      totalAlumnosAyer       : totalAlumnosAyerInbi + totalAlumnosAyer,
      faltantes              : (totalAlumnosActualesInbi - totalAlumnosSiguientesInbi) + (totalAlumnosActuales - totalAlumnosSiguientes),
      porcentajeFaltantes    : porcentajeFaltantesInbi + porcentajeFaltantes,
      escuela: 3
    }]

    // Respuesta a enviar
    res.send({riPlantelesFast, riPlantelesInbi, totales, becadosBecados, alumnosCicloAnterior, alumnosActuales, riTeachers });
    
  } catch (error) {
    res.status(500).send({message: error ? error.message: 'Error :p'})
  }
};

exports.getCantActualMontos = async(req, res) => {
  try {
    /* CALCULAR EL RI*/
    const { cicloPre, cicloAct, cicloSig } = req.body

    /****************************************/
    /*                E X C I               */
    /****************************************/

    let registros = await exci.getRegistrosExciBeca( ).then( response => response )
    // Solo los que ya tienen grupo
    const alumnosExciGrupo = registros.filter( el=> { return el.id_grupo && [cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado].includes(el.id_ciclo)})

    // Sacamos los idAlumnosExci
    const idAlumnosExci = alumnosExciGrupo.map((registro)=>{ return registro.id_alumno })
    const idsGrupos     = alumnosExciGrupo.map((registro)=>{ return registro.id_grupo_encuesta })
    // Consultadmos las becas
    let grupos = []
    if( idAlumnosExci.length > 0 ){ grupos  = await exci.gruposExci( idsGrupos ).then( response => response ) }

    let pagosExci = []
    if( idAlumnosExci.length > 0 ){ pagosExci  = await exci.pagosExci( idsGrupos, idAlumnos ).then( response => response ) }


    for( const i in alumnosExciGrupo ){
      const { id_grupo_encuesta, id_alumno } = alumnosExciGrupo[i]
      const existeGrupo = grupos.find( el=> el.id_grupo == id_grupo_encuesta )
      alumnosExciGrupo[i].id_plantel = existeGrupo ? existeGrupo.id_plantel : null

      const existePago = grupos.find( el=> el.id_grupo == id_grupo_encuesta && el.id_alumno == id_alumno )
      alumnosExciGrupo[i]['pagado'] = existePago ? existePago.pagado : 0 
    }

    /****************************************/
    /****************************************/

    // Esta línea se ocupa aquí, por que alugumos becados no se reinscriben, entonces... vale queso 
    // consultamos los alumnos inscritos actualmente
    const alumnosActuales          = await Kpi.getAlumnosInscritos( cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado ).then(response=>response)
    const idAlumnosActuales        = alumnosActuales.map((registro)=>{ return registro.id_alumno })

    const alumnosSiguientesPagos   = await Kpi.getAlumnosInscritos( cicloSig.id_ciclo, cicloSig.id_ciclo_relacionado ).then(response=>response)
    const idAlumnosSiguientes      = alumnosSiguientesPagos.map((registro)=>{ return registro.id_alumno })

    const alumnosRezagados         = await Kpi.getAlumnosRezagados( cicloAct.fecha_inicio_ciclo, idAlumnosSiguientes, idAlumnosActuales ).then(response=>response)
    const idAlumnosRezagados       = alumnosRezagados.map((registro)=>{ return registro.id_alumno })

    const alumnosNIMontos          = alumnosSiguientesPagos.filter( el => { return !idAlumnosRezagados.includes( el.id_alumno ) && !idAlumnosActuales.includes( el.id_alumno ) })

    /****************************************/
    /*      C I C L O  A N T E R I O R      */
    /****************************************/

    // alumnos inscritos en el ciclo anteriro
    let alumnosInscritosPre   = await Kpi.getAlumnosInscritos( cicloPre.id_ciclo, cicloPre.id_ciclo_relacionado ).then(response=>response)

    // Sacar los alumnos becados en el ciclo actual, becados que son por que ya terminaron su plan de estudios ( ciclo anterior = ciclo actual, ciclo act = ciclo siguiente)
    let alumnosBecadosAct     = await Kpi.getAlumnosFuturaBeca( cicloPre.id_ciclo, cicloPre.id_ciclo_relacionado, cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado ).then(response=>response)
    
    // Sacamos los id, se van a necesitar para saber que alumnos son los reinscribibles
    let idBecadosAct          = alumnosBecadosAct.map((registro)=>{ return registro.id_alumno })
    
    // Alumnos reinscribibles para el ciclo anterior
    let alumnosRegularesPre     = alumnosInscritosPre.filter( el => { return el.adeudo == 0 && el.pagado > 0 && el.certificacion == 0 && !idBecadosAct.includes(el.id_alumno)})
    
    // Obtener los id de los alumnos regulares
    let idAlumnosRegularesPre = alumnosRegularesPre.map((registro)=>{ return registro.id_alumno })
    
    // Alumnos becados reinscribibles
    let alumnosBecadosReinscribibles = alumnosInscritosPre.filter(el=> { return el.adeudo == 0 && el.pagado == 0  && el.certificacion == 0 && el.id_maestro > 0})
    
    // Validar que los becados se hayan reinscrito de nuevo
    alumnosBecadosReinscribibles = alumnosBecadosReinscribibles.filter( el=> { return idAlumnosActuales.includes( el.id_alumno )})
    
    // Alumnos inscritos en el ciclo siguiente
    let alumnosRIAct          = await Kpi.getAlumnosInscritosCicloSiguiente( cicloPre.id_ciclo, cicloPre.id_ciclo_relacionado, cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado ).then(response=>response)
    // Sacamos los alumnos acumulados
    let alumnosAcumuladoPre  = alumnosRIAct.filter(el=> { return idAlumnosRegularesPre.includes( el.id_alumno ) })

    // Sacar los NI
    let acumuladoPorSucursalFast     = await Kpi.acumuladoPorSucursalAll( cicloAct.id_ciclo_relacionado ).then(response => response)
    let acumuladoPorSucursalINBI     = await Kpi.acumuladoPorSucursalAll( cicloAct.id_ciclo ).then(response => response)
    let niPre = acumuladoPorSucursalFast.concat( acumuladoPorSucursalINBI )

    let idAlumnosBecados    = alumnosBecadosReinscribibles.map((registro)=>{ return registro.id_alumno })
    let idAlumnosAcumulados = alumnosAcumuladoPre.map((registro)=>{ return registro.id_alumno })
    let idAlumnosNi         = niPre.map((registro)=>{ return registro.id_alumno })
    let idAlumnosRegualres  = idAlumnosBecados.concat( idAlumnosAcumulados ).concat( idAlumnosNi ).concat(idAlumnosActuales)
    
    // Sacar los alumnos resgados
    let alumnosActualNoPre  = await Kpi.alumnosActualNoPre( cicloSig.id_ciclo, cicloSig.id_ciclo_relacionado, idAlumnosRegualres ).then( response=> response )

    let alumnosCicloAnterior = niPre.concat(alumnosAcumuladoPre).concat(alumnosBecadosReinscribibles).concat(alumnosActualNoPre)
    /****************************************/
    /****************************************/

    // Aquíii vamos a limpiar los alumnos del ciclo anterior, por que no todosss se vuelven a inscribir
    alumnosCicloAnterior = alumnosCicloAnterior.filter( el=> { return idAlumnosActuales.includes(el.id_alumno)})

    // sacamos el id del alumno de los reinscribibles
    const idRegulares     = alumnosActuales.filter( el => { return el.certificacion == 0 }).map((registro)=>{ return registro.id_alumno })

    // consultamos los alumnos inscritos actualmente
    const alumnosSiguientes = await Kpi.getAlumnosInscritosCicloSiguiente( cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado, cicloSig.id_ciclo, cicloSig.id_ciclo_relacionado ).then(response=>response)
    
    for( const i in alumnosSiguientes ){
      alumnosSiguientes[i].pagado = alumnosSiguientes[i].nuevoPago
    }

    const alumnosBecaSiguiente = await Kpi.getAlumnosFuturaBeca( cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado, cicloSig.id_ciclo, cicloSig.id_ciclo_relacionado ).then(response=>response)
    const idBecadosSiguiente = alumnosBecaSiguiente.map((registro)=>{ return registro.id_alumno })

    /******** CALCULANDO RI ********/
    // Consultamos los planteles
    let planteles = await dashboardKpi.getPlanteles().then(response=> response)
    
    // Calular la cantidad de alumnos inscritos por plantel
    let riPlantelesFast = []
    let riPlantelesInbi = []

    let becadosBecados = []

    // NUEVO NI
    let niFastCicloActual = await Kpi.acumuladoPorSucursalAll( cicloSig.id_ciclo_relacionado ).then(response => response)
    let niINBICicloActual = await Kpi.acumuladoPorSucursalAll( cicloSig.id_ciclo ).then(response => response)
    let niActualAll       = niFastCicloActual.concat( niINBICicloActual )

    let alumnosDobles = []

    for( const i in alumnosSiguientesPagos ){
      const { id_alumno } = alumnosSiguientesPagos[i]

      const doblePago = alumnosSiguientesPagos.filter( el=> { return el.id_alumno == id_alumno })

      if( doblePago.length > 1 ){
        // VAlidar que no este registrado en el arreglo de alumnos duplicados
        const existeRegistroPagoDoble = alumnosDobles.find( el => el.id_alumno == id_alumno )

        if( !existeRegistroPagoDoble ){ alumnosDobles = alumnosDobles.concat( doblePago ) }
      }
    }


    // Recorremos los planteles para agregarles lo del RI 
    for(const i in planteles){

      const { id_plantel, plantel, escuela } = planteles[i]

      const becadoCerti = alumnosBecaSiguiente.filter( el => el.id_plantel == id_plantel ).map(item => item.pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)

      const alumnosPlantel          = alumnosActuales.filter(el=> { return el.id_plantel == id_plantel })

      // Total de alumnos en el siguiente ciclo que se registraron ayer
      const totalAlumnosAyer        = alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel && el.fecha_alta_nuevo_grupo }).length
      // Alumnos en certificación sin beca
      const certificacion           = alumnosActuales.filter(el=> { return el.id_plantel == id_plantel && el.certificacion == 1  && el.pagado > 0 && el.adeudo == 0 }).map(item => item.pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)
      // Becados certificados
      const becadoCertificacion     = alumnosActuales.filter(el=> { return el.id_plantel == id_plantel && el.certificacion == 1  && el.pagado == 0 && el.adeudo == 0 }).map(item => item.pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)
      // Alumnos que no son de certificacion
      const alumnosNoCertificacion  = alumnosPlantel.filter(el=> { return el.id_plantel == id_plantel && el.certificacion == 0 })

      // Regularessss # REINSCRIBIBLES ( con maestro, sin adeudo y sin beca y no certificacion )
      let   regulares               = alumnosNoCertificacion.filter(el=> { return el.adeudo == 0 && el.pagado > 0 && el.id_maestro > 0 }).length


      const alumnosIrregualres      = alumnosNoCertificacion.filter(el=> { return el.adeudo != 0 || !el.id_maestro })

      // alumnos becados
      const alumnosBecados          = alumnosNoCertificacion.filter(el=> { return el.adeudo == 0 && el.pagado == 0 && !idAlumnosExci.includes(el.id_alumno) })


      // Becados del exci
      let   becadosExci             = alumnosExciGrupo.filter(el=> { return el.id_plantel == id_plantel }).length
      const idBecaExci              = alumnosExciGrupo.filter(el=> { return el.id_plantel == id_plantel })
      const idBecaExciAlumnos       = idBecaExci.map((registro)=>{ return registro.id_alumno })
      

      // sacamos el total de alumnos en el siguiente ciclo los cuales es con respecto a los reinscribibles
      const totalAlumnosSiguientes  = alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel && idRegulares.includes(el.id_alumno) && !idBecadosSiguiente.includes( el.id_alumno ) }).length


      // Becados ( sin pagado y no certificacion)
      const becados                 = alumnosNoCertificacion.filter(el=> { return el.adeudo == 0 && el.pagado == 0 && !idAlumnosExci.includes(el.id_alumno) }).map(item => item.pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)
      becadosBecados = becadosBecados.concat( alumnosNoCertificacion.filter(el=> { return el.adeudo == 0 && el.pagado == 0 }) )

      // Acumulado anterior
      const acumuladoant           = alumnosAcumuladoPre.filter( el=> { return el.id_plantel == id_plantel }).map(item => item.pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)
      const becadosant             = alumnosBecadosReinscribibles.filter( el=> { return el.id_plantel == id_plantel }).map(item => item.pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)
      const niant                  = niPre.filter( el=> { return el.id_plantel == id_plantel }).map(item => item.pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)

      let montoActual = alumnosActuales.filter(el=> { return el.id_plantel == id_plantel }).map(item => item.pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)

      // BUSCAR AL ALUMNO CON ID 9743
      // alumnosBecadosReinscribibles
      // alumnosAcumuladoPre
      // niPre

      const irregulares             = alumnosNoCertificacion.filter(el=> { return el.adeudo > 0 || !el.id_maestro  }).map(item => item.pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)

      let idAlumnosNiNi             = niActualAll.map((registro)=>{ return registro.id_alumno })
      let alumnosNoResagados        = idAlumnosRezagados.concat(idAlumnosNiNi)
      const resagados               = alumnosSiguientesPagos.filter( el=> { return el.id_plantel == id_plantel && idAlumnosRezagados.includes(el.id_alumno)}).map(item => item.pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)

      
      let alumnosAcumulados = alumnosSiguientesPagos.filter(el=> { return el.id_plantel == id_plantel && !idAlumnosRezagados.includes(el.id_alumno) && idRegulares.includes(el.id_alumno) })
      let montoAcumulado    = alumnosAcumulados.map(item => item.pagado ).reduce((prev, curr) => prev + curr, 0)

      // MONTOS REINSCRIBIBLES
      let montoRegulares    = alumnosNoCertificacion.filter(el=> { return el.adeudo <= 0 && el.pagado > 0 && el.id_maestro > 0 && !alumnosNoResagados.includes(el.id_alumno) }).map(item => item.pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)
      let montoBecadosExci  = alumnosExciGrupo.filter(el=> { return el.id_plantel == id_plantel }).map(item => item.pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)

      let montoNI     = alumnosNIMontos.filter( el=> { return el.id_plantel == id_plantel }).map(item => item.pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)

      let totalMonto = ( parseFloat( montoAcumulado ) + parseFloat(resagados) + parseFloat(montoNI) ).toFixed(2)

      // Sacar el % de alumnos que faltan por reinscribirse
      let faltantes = (parseFloat( montoRegulares ) + parseFloat( montoBecadosExci )) - parseFloat( montoAcumulado ) - parseFloat( becadoCerti )
      
      let reinscribibles = (parseFloat( montoRegulares ) + parseFloat( montoBecadosExci )) - parseFloat( becadoCerti )

      const porcentajeFaltantes = ( ( 1 - (faltantes / reinscribibles) ) * 100).toFixed(2)

      let alumnosPagoMismoCiclo = alumnosDobles.filter(el=> { return el.id_plantel == id_plantel }).map(( registro ) => { return registro.id_alumno })

      // console.log( plantel, irregulares, montoActual,  montoRegulares  )

      let payload = {
        id_plantel,
        plantel   ,

        acumuladoant,
        becadosant,
        niant,

        totalAlumnosActuales: montoActual,
        totalAlumnosSiguientes: montoAcumulado,
        noRI: parseFloat(irregulares),
        resagados,
        niActual: montoNI,
        totalMonto,

        diferencia: (parseFloat( totalMonto ) - parseFloat( montoActual )).toFixed(2),


        reinscribibles: reinscribibles,
        irregulares,
        becados,
        becadosExci,
        becadoCertificacion: (parseFloat( becadoCertificacion ) + parseFloat( becadoCerti )).toFixed(2),
        certificacion,

        totalAlumnosAyer:       alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel && el.fecha_alta_nuevo_grupo }).map(item => item.pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2),

        faltantes: faltantes.toFixed(2),
        porcentajeFaltantes,
        alumnosBecados,
        alumnosPlantel, 
        alumnosSiguientes: alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel && idRegulares.includes(el.id_alumno) && !idBecadosSiguiente.includes( el.id_alumno ) }),
        alumnosCertificacion: alumnosActuales.filter(el=> { return el.id_plantel == id_plantel && el.certificacion == 1  && el.pagado > 0 }),
        alumnosAcumulados: alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel && idRegulares.includes(el.id_alumno) }),
        escuela,
        cantPagoDoble   : alumnosDobles.filter( el => { return alumnosPagoMismoCiclo.includes( el.id_alumno ) }).length,
        alumnosPagoDoble: alumnosDobles.filter( el => { return alumnosPagoMismoCiclo.includes( el.id_alumno ) })
      }
      if( escuela == 2 && ![19,20].includes(id_plantel)){
        riPlantelesFast.push( payload )
      }
      if( escuela == 1 && ![19,20].includes(id_plantel)){
        riPlantelesInbi.push( payload )
      }
    }

    let reinscribibles         = riPlantelesFast.map(item => item.reinscribibles).reduce((prev, curr) => prev + curr, 0)
    let irregulares            = riPlantelesFast.map(item => parseFloat( item.irregulares )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let becados                = riPlantelesFast.map(item => parseFloat( item.becados )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let becadoCertificacion    = riPlantelesFast.map(item => parseFloat( item.becadoCertificacion )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let certificacion          = riPlantelesFast.map(item => parseFloat( item.certificacion )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    
    let totalAlumnosActuales   = riPlantelesFast.map(item => parseFloat( item.totalAlumnosActuales )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let totalAlumnosSiguientes = riPlantelesFast.map(item => parseFloat( item.totalAlumnosSiguientes )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let totalAlumnosAyer       = riPlantelesFast.map(item => parseFloat( item.totalAlumnosAyer )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let faltantes              = riPlantelesFast.map(item => parseFloat( item.faltantes )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let porcentajeFaltantes    = ( ( 1 - (parseFloat( faltantes ) / parseFloat( reinscribibles) )) *100).toFixed(2)

    let acumuladoant           = riPlantelesFast.map(item => parseFloat( item.acumuladoant )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let becadosant             = riPlantelesFast.map(item => parseFloat( item.becadosant )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let niant                  = riPlantelesFast.map(item => parseFloat( item.niant )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let niActual               = riPlantelesFast.map(item => parseFloat( item.niActual )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let resagados              = riPlantelesFast.map(item => parseFloat( item.resagados )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let totalMonto             = riPlantelesFast.map(item => parseFloat( item.totalMonto )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let diferencia             = riPlantelesFast.map(item => parseFloat( item.diferencia )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let noRI                   = riPlantelesFast.map(item => parseFloat( item.noRI )).reduce((prev, curr) => prev + curr, 0).toFixed(2)

    /* AGREGAR LOS TOTALES DE CADA TABLA */
    riPlantelesFast.push({
      id_plantel             : 0,
      plantel                : 'TOTAL',

      acumuladoant,
      becadosant,
      niant,
      niActual,
      totalMonto,
      resagados,

      diferencia,

      reinscribibles,
      irregulares,
      becados,
      becadoCertificacion,
      certificacion,

      totalAlumnosActuales,
      totalAlumnosSiguientes,
      totalAlumnosAyer,
      faltantes: reinscribibles - totalAlumnosSiguientes,
      porcentajeFaltantes,
      escuela: 2,
      noRI
    })

    let regularesInbi              = riPlantelesInbi.map(item => item.reinscribibles).reduce((prev, curr) => prev + curr, 0)
    let irregularesInbi            = riPlantelesInbi.map(item => parseFloat( item.irregulares )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let becadosInbi                = riPlantelesInbi.map(item => parseFloat( item.becados )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let becadoCertificacionInbi    = riPlantelesInbi.map(item => parseFloat( item.becadoCertificacion )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let certificacionInbi          = riPlantelesInbi.map(item => parseFloat( item.certificacion )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    
    let totalAlumnosActualesInbi   = riPlantelesInbi.map(item => parseFloat( item.totalAlumnosActuales )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let totalAlumnosSiguientesInbi = riPlantelesInbi.map(item => parseFloat( item.totalAlumnosSiguientes )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let totalAlumnosAyerInbi       = riPlantelesInbi.map(item => parseFloat( item.totalAlumnosAyer )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let faltantesInbi              = riPlantelesInbi.map(item => parseFloat( item.faltantes )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let porcentajeFaltantesInbi    = ( ( 1 - (faltantesInbi / regularesInbi))*100).toFixed(2)
    let acumuladoantInbi           = riPlantelesInbi.map(item => parseFloat( item.acumuladoant )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let becadosantInbi             = riPlantelesInbi.map(item => parseFloat( item.becadosant )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let niantInbi                  = riPlantelesInbi.map(item => parseFloat( item.niant )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let niActualInbi               = riPlantelesInbi.map(item => parseFloat( item.niActual )).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let resagadosInbi              = riPlantelesInbi.map(item => parseFloat( item.resagados )).reduce((prev, curr) => prev + curr, 0).toFixed(2) 
    let totalMontoInbi             = riPlantelesInbi.map(item => parseFloat( item.totalMonto )).reduce((prev, curr) => prev + curr, 0).toFixed(2) 
    let diferenciaInbi             = riPlantelesInbi.map(item => parseFloat( item.diferencia )).reduce((prev, curr) => prev + curr, 0).toFixed(2) 
    let noRIInbi                       = riPlantelesInbi.map(item => parseFloat( item.noRI )).reduce((prev, curr) => prev + curr, 0).toFixed(2) 

    /* AGREGAR LOS TOTALES DE CADA TABLA */
    riPlantelesInbi.push({
      id_plantel             : 0,
      plantel                : 'TOTAL',
      acumuladoant           : acumuladoantInbi,
      becadosant             : becadosantInbi,
      niant                  : niantInbi,
      niActual               : niActualInbi,
      resagados              : resagadosInbi,
      totalMonto             : totalMontoInbi,
      diferencia             : diferenciaInbi,

      reinscribibles         : regularesInbi,
      irregulares            : irregularesInbi,
      becados                : becadosInbi,
      becadoCertificacion    : becadoCertificacionInbi,
      certificacion          : certificacionInbi,

      totalAlumnosActuales   : totalAlumnosActualesInbi,
      totalAlumnosSiguientes : totalAlumnosSiguientesInbi,
      totalAlumnosAyer       : totalAlumnosAyerInbi,
      faltantes              : regularesInbi - totalAlumnosSiguientesInbi,
      porcentajeFaltantes    : porcentajeFaltantesInbi,
      escuela: 1,
      noRI: noRIInbi
    })

    const totales = [{
      id_plantel             : 0,
      plantel                : 'TOTAL',
      acumuladoant           : ( parseFloat( acumuladoant )  + parseFloat( acumuladoantInbi ) ).toFixed(2),
      becadosant             : ( parseFloat( becadosant ) + parseFloat( becadosantInbi ) ).toFixed(2),
      niant                  : ( parseFloat( niant ) + parseFloat( niantInbi ) ).toFixed(2),
      niActual               : ( parseFloat( niActual ) + parseFloat( niActualInbi ) ).toFixed(2),
      resagados              : ( parseFloat( resagados ) + parseFloat( resagadosInbi ) ).toFixed(2),
      totalMonto             : ( parseFloat( totalMonto ) + parseFloat( totalMontoInbi ) ).toFixed(2),
      reinscribibles         : regularesInbi + reinscribibles,
      irregulares            : ( parseFloat( irregularesInbi ) + parseFloat( irregulares ) ).toFixed(2),
      becados                : ( parseFloat( becadosInbi ) + parseFloat( becados ) ).toFixed(2),
      becadoCertificacion    : ( parseFloat( becadoCertificacionInbi ) + parseFloat( becadoCertificacion ) ).toFixed(2),
      certificacion          : ( parseFloat( certificacionInbi ) + parseFloat( certificacion ) ).toFixed(2),

      diferencia             : ( parseFloat( diferencia ) + parseFloat( diferenciaInbi ) ).toFixed(2),

      totalAlumnosActuales   : ( parseFloat( totalAlumnosActualesInbi ) + parseFloat( totalAlumnosActuales )),
      totalAlumnosSiguientes : ( parseFloat( totalAlumnosSiguientesInbi ) + parseFloat( totalAlumnosSiguientes )),
      totalAlumnosAyer       : ( parseFloat( totalAlumnosAyerInbi ) + parseFloat( totalAlumnosAyer )),
      faltantes              : (totalAlumnosActualesInbi - totalAlumnosSiguientesInbi) + (totalAlumnosActuales - totalAlumnosSiguientes),
      porcentajeFaltantes    : (parseFloat( porcentajeFaltantesInbi ) + parseFloat( porcentajeFaltantes )).toFixed(2),
      escuela: 3,
      noRI                   : ( parseFloat( noRI ) + parseFloat( noRIInbi )), 
    }]

    // Respuesta a enviar
    res.send({riPlantelesFast, riPlantelesInbi, totales, becadosBecados, alumnosCicloAnterior, alumnosActuales });
    
  } catch (error) {
    res.status(500).send({message: error ? error.message: 'Error :p'})
  }
};


// Obtener todos los ciclos  activos
exports.getCantSiguiente = (req, res) => {
  Kpi.getCantSiguiente(req.params.id,req.params.idfast, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

exports.kpiInscripcionesGeneral = async (req, res) => {
  try {
    const{ id_ciclo, id_ciclo_relacionado } = req.body

    const dataFast = await Kpi.kpiInscripcionesGeneral( id_ciclo_relacionado ).then( response => response )
    const dataInbi = await Kpi.kpiInscripcionesGeneral( id_ciclo ).then( response => response )

    const respuesta = {
      fast: dataFast,
      inbi: dataInbi
    }

    res.send( respuesta )

  } catch (error) {
    res.status(500).send({message: error ? error.message: 'Error :p'})
  }
};

/**/
exports.kpiInscripcionesAlumnosCicloRango = async(req, res) => {
  try {
    const{ ciclo, fechaini, fechafin } = req.body

    const dataFast = await Kpi.kpiInscripcionesAlumnosCicloRango( ciclo.id_ciclo_relacionado, fechaini, fechafin ).then( response => response )
    const dataInbi = await Kpi.kpiInscripcionesAlumnosCicloRango( ciclo.id_ciclo, fechaini, fechafin ).then( response => response )

    const respuesta = {
      fast: dataFast,
      inbi: dataInbi
    }

    res.send( respuesta )

  } catch (error) {
    res.status(500).send({message: error ? error.message: 'Error :p'})
  }
};

exports.kpiInscripcionesAlumnosCicloRangoINBI = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesAlumnosCicloRangoINBI(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};

exports.kpiInscripcionesAlumnosRangoFAST = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Kpi.kpiInscripcionesAlumnosRangoFAST(req.body,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

exports.kpiInscripcionesAlumnosRangoINBI = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Kpi.kpiInscripcionesAlumnosRangoINBI(req.body,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

/***/

exports.kpiInscripcionesAlumnosDiaFAST = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Kpi.kpiInscripcionesAlumnosDiaFAST(req.body,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

exports.kpiInscripcionesAlumnosDiaINBI = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Kpi.kpiInscripcionesAlumnosDiaINBI(req.body,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};


/************************************************************************************/
/************************************************************************************/
/************************************************************************************/

exports.kpiInscripcionesTotalFAST = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Kpi.kpiInscripcionesTotalFAST(req.params.actual,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

exports.kpiInscripcionesTotalINBI = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Kpi.kpiInscripcionesTotalINBI(req.params.actual,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

exports.kpiInscripcionesCicloDiaFAST = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesCicloDiaFAST(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};

exports.kpiInscripcionesCicloDiaINBI = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesCicloDiaINBI(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};

exports.kpiInscripcionesCicloRangoFAST = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesCicloRangoFAST(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};

exports.kpiInscripcionesCicloRangoINBI = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesCicloRangoINBI(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};

exports.getPlanteles = (req, res) => {
  Kpi.getPlanteles((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

// Obtener todos los ciclos  activos
exports.getCiclos = async (req, res) => {
  try {
    const{ id_ciclo, id_ciclo_relacionado } = req.body

    const ciclos = await Kpi.getCiclos( ).then( response => response )

    res.send( ciclos )

  } catch (error) {
    res.status(500).send({message: error ? error.message: 'Error :p'})
  }
};

exports.getCiclosExci = (req, res) => {
  Kpi.getCiclosExci((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

exports.getCiclosInduccion = (req, res) => {
  Kpi.getCiclosInduccion((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

// Obtener todos los ciclos  activos
exports.getCantSiguienteAvance = (req, res) => {
  Kpi.getCantSiguienteAvance(req.params.id,req.params.idfast, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

// TEEEEEENS

// Obtener todos los planteles activos
exports.getCantActualTeens = (req, res) => {
  Kpi.getCantActualTeens(req.params.id,req.params.idfast, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

// Obtener todos los ciclos  activos
exports.getCantSiguienteTeens = (req, res) => {
  Kpi.getCantSiguienteTeens(req.params.id,req.params.idfast, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

/************************************************************************************/
/************************************************************************************/
/************************************************************************************/

exports.kpiInscripcionesVendedoraCicloGeneral = async (req, res) => {
  try{

    const { id_ciclo_relacionado, id_ciclo } = req.body

    let inscritosFast = await Kpi.kpiInscripcionesVendedoraCicloGeneral( id_ciclo_relacionado ).then( response => response )
    let inscritosInbi = await Kpi.kpiInscripcionesVendedoraCicloGeneral( id_ciclo ).then( response => response )

    let vendedoraFast = await Kpi.kpiInscripcionesVendedoraCicloGeneralGroup( id_ciclo_relacionado ).then( response => response )
    let vendedoraInbi = await Kpi.kpiInscripcionesVendedoraCicloGeneralGroup( id_ciclo ).then( response => response )


    // AGREGAR LOS TOTALES
    let total = 0 
    let sin_adeudo = 0
    let con_adeudo = 0
    let becados    = 0

    for(const i in vendedoraFast){
      const { nombre_completo } = vendedoraFast[i]

      vendedoraFast[i]['sin_adeudo'] = inscritosFast.filter( el => { return el.adeudo == 0 && el.pagado > 1 && el.nombre_completo == nombre_completo}).length
      sin_adeudo += vendedoraFast[i]['sin_adeudo']

      vendedoraFast[i]['con_adeudo'] = inscritosFast.filter( el => { return el.adeudo > 0 && el.pagado > 1 && el.nombre_completo == nombre_completo}).length
      vendedoraFast[i]['adeudos']    = inscritosFast.filter( el => { return el.adeudo > 0 && el.pagado > 1 && el.nombre_completo == nombre_completo})
      con_adeudo += vendedoraFast[i]['con_adeudo']

      vendedoraFast[i]['becados']    = inscritosFast.filter( el => { return el.adeudo == 0 && el.pagado <= 1 && el.nombre_completo == nombre_completo}).length
      becados    += vendedoraFast[i]['becados']

      vendedoraFast[i]['total']    = inscritosFast.filter( el => { return el.nombre_completo == nombre_completo}).length

      total += vendedoraFast[i].total
    }

    vendedoraFast.push({ nombre_completo: 'Total', total, sin_adeudo, con_adeudo, becados})

    total = 0
    sin_adeudo = 0
    con_adeudo = 0
    becados    = 0

    for(const i in vendedoraInbi){
      const { nombre_completo} = vendedoraInbi[i]

      vendedoraInbi[i]['sin_adeudo'] = inscritosInbi.filter( el => { return el.adeudo == 0 && el.pagado > 1 && el.nombre_completo == nombre_completo }).length
      sin_adeudo += vendedoraInbi[i]['sin_adeudo']
      
      vendedoraInbi[i]['con_adeudo'] = inscritosInbi.filter( el => { return el.adeudo > 0 && el.pagado > 1 && el.nombre_completo == nombre_completo }).length
      vendedoraInbi[i]['adeudos']    = inscritosInbi.filter( el => { return el.adeudo > 0 && el.pagado > 1 && el.nombre_completo == nombre_completo})
      con_adeudo += vendedoraInbi[i]['con_adeudo']
      
      vendedoraInbi[i]['becados']    = inscritosInbi.filter( el => { return el.adeudo == 0 && el.pagado <= 1 && el.nombre_completo == nombre_completo }).length
      becados    += vendedoraInbi[i]['becados']
      
      vendedoraInbi[i]['total']      = inscritosInbi.filter( el => { return el.nombre_completo == nombre_completo}).length
      
      total += vendedoraInbi[i].total
    }

    vendedoraInbi.push({ nombre_completo: 'Total', total, sin_adeudo, con_adeudo, becados})

    const respuesta = {
      fast: vendedoraFast,
      inbi: vendedoraInbi
    }

    res.send( respuesta )

  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor contacta a sistemas' })
  }
};

exports.kpiInscripcionesVendedoraCicloRangoGeneral = async (req, res) => {
  try{

    const { cicloFast, cicloInbi, fechaini, fechafin } = req.body

    let inscritosFast = await Kpi.kpiInscripcionesVendedoraCicloRangoGeneral( cicloFast, fechaini, fechafin ).then( response => response )
    let inscritosInbi = await Kpi.kpiInscripcionesVendedoraCicloRangoGeneral( cicloInbi, fechaini, fechafin ).then( response => response )

    let vendedoraFast = await Kpi.kpiInscripcionesVendedoraCicloRangoGeneralGroup( cicloFast, fechaini, fechafin ).then( response => response )
    let vendedoraInbi = await Kpi.kpiInscripcionesVendedoraCicloRangoGeneralGroup( cicloInbi, fechaini, fechafin ).then( response => response )


    // AGREGAR LOS TOTALES
    let total = 0 
    let sin_adeudo = 0
    let con_adeudo = 0
    let becados    = 0

    for(const i in vendedoraFast){
      const { nombre_completo } = vendedoraFast[i]

      vendedoraFast[i]['sin_adeudo'] = inscritosFast.filter( el => { return el.adeudo == 0 && el.pagado > 1 && el.nombre_completo == nombre_completo}).length
      sin_adeudo += vendedoraFast[i]['sin_adeudo']

      vendedoraFast[i]['con_adeudo'] = inscritosFast.filter( el => { return el.adeudo > 0 && el.pagado > 1 && el.nombre_completo == nombre_completo}).length
      con_adeudo += vendedoraFast[i]['con_adeudo']

      vendedoraFast[i]['becados']    = inscritosFast.filter( el => { return el.adeudo == 0 && el.pagado <= 1 && el.nombre_completo == nombre_completo}).length
      becados    += vendedoraFast[i]['becados']

      vendedoraFast[i]['total']    = inscritosFast.filter( el => { return el.nombre_completo == nombre_completo}).length

      total += vendedoraFast[i].total
    }

    vendedoraFast.push({ nombre_completo: 'Total', total, sin_adeudo, con_adeudo, becados})

    total = 0
    sin_adeudo = 0
    con_adeudo = 0
    becados    = 0

    for(const i in vendedoraInbi){
      const { nombre_completo} = vendedoraInbi[i]

      vendedoraInbi[i]['sin_adeudo'] = inscritosInbi.filter( el => { return el.adeudo == 0 && el.pagado > 1 && el.nombre_completo == nombre_completo }).length
      sin_adeudo += vendedoraInbi[i]['sin_adeudo']
      
      vendedoraInbi[i]['con_adeudo'] = inscritosInbi.filter( el => { return el.adeudo > 0 && el.pagado > 1 && el.nombre_completo == nombre_completo }).length
      con_adeudo += vendedoraInbi[i]['con_adeudo']
      
      vendedoraInbi[i]['becados']    = inscritosInbi.filter( el => { return el.adeudo == 0 && el.pagado <= 1 && el.nombre_completo == nombre_completo }).length
      becados    += vendedoraInbi[i]['becados']
      
      vendedoraInbi[i]['total']      = inscritosInbi.filter( el => { return el.nombre_completo == nombre_completo}).length
      
      total += vendedoraInbi[i].total
    }

    vendedoraInbi.push({ nombre_completo: 'Total', total, sin_adeudo, con_adeudo, becados})

    const respuesta = {
      fast: vendedoraFast,
      inbi: vendedoraInbi
    }

    res.send( respuesta )

  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor contacta a sistemas' })
  }
};

exports.kpiInscripcionesVendedoraRangoGeneral = async (req, res) => {
  try{

    const { fechaini, fechafin } = req.body

    let  nuevasMatriculas = await Kpi.getNuevasMatriculas(  fechaini, fechafin ).then(response=> response)

    const matriculasFast = nuevasMatriculas.filter( el => { return el.unidad_negocio == 2 })
    const matriculasInbi = nuevasMatriculas.filter( el => { return el.unidad_negocio == 1 })

    const totalFast = nuevasMatriculas.filter( el => { return el.unidad_negocio == 2 }).length
    const totalInbi = nuevasMatriculas.filter( el => { return el.unidad_negocio == 1 }).length


    // SACAR LAS VENDEDORAS UNICAS DE INBI
    let arrayInbi = matriculasInbi.map(item=>{ return [item.vendedora,item] });
    // Creamos un map de los alumnos
    var inbi = new Map(arrayInbi); // Pares de clave y valor
    // y ahora si, extraemos los valores unicos
    let vendedorasInbi = [...inbi.values()]; // Conversión a un array


    // SACAR LAS VENDEDORAS UNICAS DE FAST
    let arrayFast = matriculasFast.map(item=>{ return [item.vendedora,item] });
    // Creamos un map de los alumnos
    var fast = new Map(arrayFast); // Pares de clave y valor
    // y ahora si, extraemos los valores unicos
    let vendedorasFast = [...fast.values()]; // Conversión a un array


    for( const i in vendedorasInbi ){
      const { vendedora } = vendedorasInbi[i]
      vendedorasInbi[i]['total']       = matriculasInbi.filter( el => { return el.vendedora == vendedora }).length
    }


    for( const i in vendedorasFast ){
      const { vendedora } = vendedorasFast[i]
      vendedorasFast[i]['total']   = matriculasFast.filter( el => { return el.vendedora == vendedora }).length
    }


    // RESPONDEMOS
    res.send({ vendedorasInbi, vendedorasFast, totalFast, totalInbi, nuevasMatriculas});

  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor contacta a sistemas' })
  }
};

exports.kpiInscripcionesVendedoraDiaFAST = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesVendedoraDiaFAST(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};


exports.kpiInscripcionesVendedoraDiaINBI = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesVendedoraDiaINBI(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};

exports.kpiInscripcionesPorCiclo = async (req, res)=>{
  try {

    const { id_ciclo, id_ciclo_relacionado } = req.body
    
    const inscritosFast          = await Kpi.TotalInscritosPorCiclo(id_ciclo_relacionado).then(response => response);
    const inscritosFinalesFast   = inscritosFast.filter( el=> { return el.adeudo == 0 && el.pagado > 0 }).length
    const conInduccionFast       = inscritosFast.filter( el=> { return el.adeudo == 0 && el.pagado > 0 && el.id_grupo }).length
    const sinInduccionFast       = inscritosFast.filter( el=> { return el.adeudo == 0 && el.pagado > 0 && !el.id_grupo}).length
    const acumuladoPorSucursal   = await Kpi.acumuladoPorSucursal(id_ciclo_relacionado).then(response => response)
    const semanaPorSucursal      = await Kpi.semanaPorSucursal(id_ciclo_relacionado).then(response => response)
    const ayerPorSucursal        = await Kpi.ayerPorSucursal(id_ciclo_relacionado).then(response => response)

    acumuladoPorSucursal.forEach(element => {
      let sucursal     = semanaPorSucursal.find(el => el.plantel == element.plantel);
      let sucursalAyer = ayerPorSucursal.find(el => el.plantel == element.plantel);
      element.semanal  = sucursal ? sucursal.cantidad : 0;
      element.ayer     = sucursalAyer ? sucursalAyer.cantidad : 0;
    });

    const inscritosInbi              = await Kpi.TotalInscritosPorCiclo(id_ciclo).then(response => response);
    const inscritosFinalesInbi       = inscritosInbi.filter( el=> { return el.adeudo == 0 && el.pagado > 0 }).length
    const conInduccionInbi           = inscritosInbi.filter( el=> { return el.adeudo == 0 && el.pagado > 0 && el.id_grupo }).length
    const sinInduccionInbi           = inscritosInbi.filter( el=> { return el.adeudo == 0 && el.pagado > 0 && !el.id_grupo}).length
    const acumuladoPorSucursalInbi   = await Kpi.acumuladoPorSucursal(id_ciclo).then(response => response)
    const semanaPorSucursalInbi      = await Kpi.semanaPorSucursal(id_ciclo).then(response => response)
    const ayerPorSucursalInbi        = await Kpi.ayerPorSucursal(id_ciclo).then(response => response)

    acumuladoPorSucursalInbi.forEach(element => {
      let sucursal     = semanaPorSucursalInbi.find(el => el.plantel == element.plantel);
      let sucursalAyer = ayerPorSucursalInbi.find(el => el.plantel == element.plantel);
      element.semanal  = sucursal ? sucursal.cantidad : 0;
      element.ayer     = sucursalAyer ? sucursalAyer.cantidad : 0;
    });


    res.send({
      fast:{
        inscritosFinales: inscritosFinalesFast,
        inscritosTotales: inscritosFast.length,
        faltantes:        ( inscritosFast.length - inscritosFinalesFast ),
        conInduccion:     conInduccionFast,
        sinInduccion:     sinInduccionFast,
        tabla:acumuladoPorSucursal
      },

      inbi:{
        inscritosFinales: inscritosFinalesInbi,
        inscritosTotales: inscritosInbi.length,
        faltantes:        ( inscritosInbi.length - inscritosFinalesInbi ),
        conInduccion:     conInduccionInbi,
        sinInduccion:     sinInduccionInbi,
        tabla:            acumuladoPorSucursalInbi
      },
    });


  } catch (error) {
    res.status(500).send({message: error ? error.message : ''})
  }

}

/***********************************************************************************************************/
/***************************************** K P I -- G E N E R A L ******************************************/
/***********************************************************************************************************/

exports.kpiNiPorCicloGeneralFAST = async (req, res)=>{
  let ciclo = req.params.ciclo;
  const TotalInscritosPorCicloFast = await Kpi.TotalInscritosPorCiclo(ciclo).then(response => response);

  try {
    
    const acumuladoPorSucursal     = await Kpi.acumuladoPorSucursal(ciclo).then(response => response)
    const acumuladoPorSucursalAyer = await Kpi.acumuladoPorSucursalAyer(ciclo).then(response => response)

    acumuladoPorSucursal.forEach(element => {
      // Buscamos primero el plantel, para ver si esta en los del ciclo actual
      let plantel = acumuladoPorSucursalAyer.find(el => el.plantel == element.plantel);
      // Si si esta, entonces sacamos la diferencia
      element.avance = plantel ? (plantel.acumulado) : 0;
    });

    res.send({totalFAST:TotalInscritosPorCicloFast,tabla:acumuladoPorSucursal});
  } catch (error) {
    res.status(500).send({message:"Hubo un error"})
  }
}

exports.kpiNiPorCicloGeneralINBI = async (req, res)=>{
  let ciclo = req.params.ciclo;
  const TotalInscritosPorCicloINBI = await Kpi.TotalInscritosPorCiclo(ciclo).then(response => response);

  try {
    
    const acumuladoPorSucursal     = await Kpi.acumuladoPorSucursal(ciclo).then(response => response)
    const acumuladoPorSucursalAyer = await Kpi.acumuladoPorSucursalAyer(ciclo).then(response => response)

    acumuladoPorSucursal.forEach(element => {
      // Buscamos primero el plantel, para ver si esta en los del ciclo actual
      let plantel = acumuladoPorSucursalAyer.find(el => el.plantel == element.plantel);
      // Si si esta, entonces sacamos la diferencia
      element.avance = plantel ? (plantel.acumulado) : 0;
    });

    res.send({totalINBI:TotalInscritosPorCicloINBI,tabla:acumuladoPorSucursal});
  } catch (error) {
    res.status(500).send({message:"Hubo un error"})
  }
}

exports.kpiNiPorCicloGeneralTEENS = async (req, res)=>{
  let ciclo = req.params.ciclo;
  const TotalInscritosPorCicloTEENS = await Kpi.TotalInscritosPorCicloTEENS(ciclo).then(response => response);

  try {
    
    const acumuladoPorSucursalTeens      = await Kpi.acumuladoPorSucursalTeens(ciclo).then(response => response)
    const acumuladoPorSucursalAyerTeens  = await Kpi.acumuladoPorSucursalAyerTeens(ciclo).then(response => response)

    acumuladoPorSucursalTeens.forEach(element => {
      // Buscamos primero el plantel, para ver si esta en los del ciclo actual
      let plantel = acumuladoPorSucursalAyerTeens.find(el => el.plantel == element.plantel);
      // Si si esta, entonces sacamos la diferencia
      element.avance = plantel ? (plantel.acumulado + element.acumulado) : 0;
    });

    res.send({totalINBI:TotalInscritosPorCicloTEENS,tabla:acumuladoPorSucursalTeens});
  } catch (error) {
    res.status(500).send({message:"Hubo un error"})
  }
}

/**************************************************************************************************************/
/**************************************************************************************************************/
/**************************************************************************************************************/

exports.kpiNiVendedoraCicloFAST = async (req, res)=>{
  let ciclo = req.params.ciclo;
  const TotalInscritosPorCicloFast = await Kpi.kpiInscripcionesVendedoraCiclo(ciclo).then(response => response);
  let totalFast = 0
  for(const i in TotalInscritosPorCicloFast){
    totalFast += TotalInscritosPorCicloFast[i].total
  }

  try {
    
    const acumuladoPorVendedora     = await Kpi.kpiInscripcionesVendedoraCiclo(ciclo).then(response => response)
    const acumuladoPorVendedoraAyer = await Kpi.acumuladoPorVendedoraAyer(ciclo).then(response => response)
    acumuladoPorVendedora.forEach(element => {
      // Buscamos primero la vendedora, para ver si esta en los del ciclo actual
      let vendedora = acumuladoPorVendedoraAyer.find(el => el.nombre_completo == element.nombre_completo);
      // Si si esta, entonces sacamos la diferencia
      element.avance = vendedora ? (vendedora.total) : 0;
    });

    res.send({totalFAST:totalFast,tabla:acumuladoPorVendedora});
  } catch (error) {
    res.status(500).send({message:"Hubo un error" + error})
  }
}

exports.kpiNiVendedoraCicloTEENS = async (req, res)=>{
  let ciclo = req.params.ciclo;
  const TotalInscritosPorCicloTeens = await Kpi.kpiInscripcionesVendedoraCicloTEENS(ciclo).then(response => response);
  let totalFast = 0
  for(const i in TotalInscritosPorCicloTeens){
    totalFast += TotalInscritosPorCicloTeens[i].total
  }

  try {
    
    const acumuladoPorVendedoraTeens     = await Kpi.kpiInscripcionesVendedoraCicloTEENS(ciclo).then(response => response)
    const acumuladoPorVendedoraAyerTeens = await Kpi.acumuladoPorVendedoraAyerTeens(ciclo).then(response => response)
    acumuladoPorVendedoraTeens.forEach(element => {
      // Buscamos primero la vendedora, para ver si esta en los del ciclo actual
      let vendedora = acumuladoPorVendedoraAyerTeens.find(el => el.nombre_completo == element.nombre_completo);
      // Si si esta, entonces sacamos la diferencia
      element.avance = vendedora ? (vendedora.total) : 0;
    });

    res.send({totalFAST:totalFast,tabla:acumuladoPorVendedoraTeens});
  } catch (error) {
    res.status(500).send({message:"Hubo un error" + error})
  }
}

exports.kpiNiVendedoraCiclo = async (req, res)=>{
  let ciclo = req.params.ciclo;

  const TotalInscritosPorCicloInbi = await Kpi.kpiInscripcionesVendedoraCiclo(ciclo).then(response => response);
  let totalFast = 0
  for(const i in TotalInscritosPorCicloInbi){
    totalInbi += TotalInscritosPorCicloInbi[i].total
  }

  try {
    
    const acumuladoPorVendedora     = await Kpi.kpiInscripcionesVendedoraCiclo(ciclo).then(response => response)
    const acumuladoPorVendedoraAyer = await Kpi.acumuladoPorVendedoraAyer(ciclo).then(response => response)

    acumuladoPorVendedora.forEach(element => {
      // Buscamos primero la vendedora, para ver si esta en los del ciclo actual
      let vendedora = acumuladoPorVendedoraAyer.find(el => el.nombre_completo == element.nombre_completo);
      // Si si esta, entonces sacamos la diferencia
      element.avance = vendedora ? (vendedora.total) : 0;
    });

    res.send({totalINBI:totalInbi,tabla:acumuladoPorVendedora});
  } catch (error) {
    res.status(500).send({message:"Hubo un error"})
  }
}

/*********************************************************************************************************/
/*********************************************************************************************************/
// RI por grupos

exports.kpiRiGrupos = async(req, res) => {
  try {
    // Primero sacamos a todos los grupos que están en ese ciclo y funcionando 
    const getGruposActual       = await Kpi.getGruposActual(req.params.inbi,req.params.fast).then(response => response)

    // Ahora a los alumnos de esos grupos
    const alumosCicloActual     = await Kpi.alumosCicloActual(req.params.inbi,req.params.fast).then(response => response)

    // Consultamos los alumnos que están en
    const alumosCicloSiguiente  = await Kpi.alumosCicloSiguiente(req.params.inbi,req.params.fast,req.params.sigInbi,req.params.sigFast).then(response => response)

    let resultadoFinal = []
    // Creado el arreglo list para recibir las cantidades
    for(const i in getGruposActual){
      let payload = {
        id_grupo                : getGruposActual[i].id_grupo,
        id_plantel              : getGruposActual[i].id_plantel,
        grupo                   : getGruposActual[i].grupo,
        alumnos_ciclo_actual    : getGruposActual[i].alumnos_ciclo_actual,
        alumnos_siguiente_ciclo : 0,
        faltantes               : 0
      }
      resultadoFinal.push(payload)
    }


    // Sacamos los alumnos del siguiente ciclo
    for(const i in resultadoFinal){
      for(const j in alumosCicloSiguiente){
        // Solo hay que valdiar que el alumno sea de ese mismo grupo
        if(resultadoFinal[i].id_grupo == alumosCicloSiguiente[j].id_grupo){
          resultadoFinal[i].alumnos_siguiente_ciclo = alumosCicloSiguiente[j].alumnos_siguiente_ciclo
        }
      }
    }

    // Sacamos los alumnos faltantes
    for(const i in resultadoFinal){
      resultadoFinal[i].faltantes = resultadoFinal[i].alumnos_ciclo_actual - resultadoFinal[i].alumnos_siguiente_ciclo
    }

    res.send(resultadoFinal);
  } catch (error) {
    res.status(500).send({message:error})
  }
};

/*********************************************************************************************************/
/*********************************************************************************************************/
// Horarios

exports.getHorariosClases = async(req, res) => {
  try {

    const { cicloFast, cicloInbi } = req.params

    // Primero sacamos a todos los grupos que están en ese ciclo y funcionando 
    let getHorarioClaseFast       = await Kpi.getHorarioClaseFast( cicloFast ).then(response => response)

    // Ahora a los alumnos de esos grupos
    let getHorarioClaseInbi       = await Kpi.getHorarioClaseInbi( cicloInbi ).then(response => response)

    let resultadoFinal = getHorarioClaseFast.concat(getHorarioClaseInbi)

    res.send(resultadoFinal);
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getClasesHoraDia = async (req, res) => {
  try{
    // Desestructuramos los ciclos
    const { hora_inicio, cicloFast, cicloInbi, fecha } = req.body

    // Primero debemos obtener el dia en que se esta llevamos a  cabo la clase
    const { diaEspecifico } = await Kpi.getNumberDiaEspecificoBD2( fecha ).then(response => response)

    // Ahora se tiene que consultar
    const clasesFast = await Kpi.getClasesFast( cicloFast, diaEspecifico, hora_inicio ).then(response => response)
    const clasesInbi = await Kpi.getClasesInbi( cicloInbi, diaEspecifico, hora_inicio ).then(response => response)

    // Preparar los datos antes de enviarlos
    for(const i in clasesFast){
      clasesFast[i] = {
        ...clasesFast[i],
        idindicador_clases:0,
        inicio_tiempo     :0,
        maestro_asignado  :0,
        contenido_correcto:0,
        finaliza_tiempo   :0,
        ninguna           :0
      }
    }

    for(const i in clasesInbi){
      clasesInbi[i] = {
        ...clasesInbi[i],
        idindicador_clases:0,
        inicio_tiempo     :0,
        maestro_asignado  :0,
        contenido_correcto:0,
        finaliza_tiempo   :0,
        ninguna           :0
      }
    }

    const clasesGrabadasFast = await Kpi.getClasesGrabadasFast ( ).then(response => response)
    const clasesGrabadasInbi = await Kpi.getClasesGrabadasInbi ( ).then(response => response)

    for(const i in clasesFast){
      const { dia, id_ciclo, hora_inicio, id_grupo } = clasesFast[i]
      const clase = clasesGrabadasFast.find(el => el.dia == dia && el.id_ciclo == id_ciclo && el.hora_inicio == hora_inicio && el.id_grupo == id_grupo )
      if( clase ){
        clasesFast[i].idindicador_clases = clase.idindicador_clases
        clasesFast[i].inicio_tiempo      = clase.inicio_tiempo
        clasesFast[i].maestro_asignado   = clase.maestro_asignado
        clasesFast[i].finaliza_tiempo    = clase.finaliza_tiempo
        clasesFast[i].contenido_correcto = clase.contenido_correcto
        clasesFast[i].ninguna            = clase.ninguna
      }
    }

    for(const i in clasesInbi){
      const { dia, id_ciclo, hora_inicio, id_grupo } = clasesInbi[i]
      const clase = clasesGrabadasInbi.find(el => el.dia == dia && el.id_ciclo == id_ciclo && el.hora_inicio == hora_inicio && el.id_grupo == id_grupo )
      if( clase ){
        clasesInbi[i].idindicador_clases = clase.idindicador_clases
        clasesInbi[i].inicio_tiempo      = clase.inicio_tiempo
        clasesInbi[i].maestro_asignado   = clase.maestro_asignado
        clasesInbi[i].finaliza_tiempo    = clase.finaliza_tiempo
        clasesInbi[i].contenido_correcto = clase.contenido_correcto
        clasesInbi[i].ninguna            = clase.ninguna
      }
    }


    let resultadoFinal = clasesFast.concat(clasesInbi)
    // RESPONDEMOS
    res.send(resultadoFinal);
  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};

exports.getClasesHoraDiaUpdate = async (req, res) => {
  try{
    // Desestructuramos los ciclos
    const { idindicador_clases, id_unidad_negocio } = req.body

    // Validamos la escuela si es FAST o INBI
    if( id_unidad_negocio == 2){
      // Varificamos si la clase ya tiene un registro o no
      if( idindicador_clases == 0 ){
        const addClaseFast = await Kpi.addClaseFast ( req.body ).then(response => response)
      }else{
        const updateClaseFast = await Kpi.updateClaseFast ( req.body ).then(response => response)
      }
    // Escuela de inbi
    }else{
      // Verificamos si ya existe una clase grabada o no 
      if( idindicador_clases == 0 ){
        const addClaseInbi = await Kpi.addClaseInbi ( req.body ).then(response => response)
      }else{
        const updateClaseInbi = await Kpi.updateClaseInbi ( req.body ).then(response => response)
      }
    }

    // RESPONDEMOS
    res.send({ message : 'Datos grabados correctamente'});
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getClasesHoraDiaReporte = async (req, res) => {
  try{
    // Primero debemos obtener el dia en que se esta llevamos a  cabo la clase
    const { dia } = await Kpi.getNumberDiaBD( ).then(response => response)
    let clasesTotalesFast = null
    let clasesTotalesInbi = null
    let totalFast = 0
    let totalInbi = 0
    // Desestructuramos los ciclos
    const { fecha, fecha2, cicloFast, cicloInbi, tipo } = req.body
    // Primero obtenemos las clases buenas de FAST
    const clasesFast = await Kpi.getReporteClasesBuenasFast( fecha, fecha2, cicloFast ).then(response=> response)
    // Y ahora obtenemos las clases buenas de INBI
    const clasesInbi = await Kpi.getReporteClasesBuenasInbi( fecha, fecha2, cicloInbi ).then(response=> response)

    // si las fechas son iguales, eso quiere decir que hay que sacar el total de clases por día
    if(tipo == 1){
      const { diaClase } = await Kpi.getNumberDiaBDEspecifico( fecha ).then(response => response)
      // Mandamos como parameto el ciclo, el dia, y el multiplicador
      // Clases totales FAST
      let concatDia = ` AND ${ diaClase } = 1` 
      let select    = '*'
      let group     = ''
      clasesTotalesFast = await Kpi.getReporteClasesTotalesFast( cicloFast, concatDia, group, select ).then(response=> response)
      totalFast = clasesTotalesFast.length
      // // Clases totales INBI
      clasesTotalesInbi = await Kpi.getReporteClasesTotalesInbi( cicloInbi, concatDia, group, select).then(response=> response)
      totalInbi = clasesTotalesInbi.length
    }

    if( tipo == 2){
      // aquí trabajamos con un rango de fechas

      // Convertimos las fechas a tipo fecha
      let inicio   = new Date(fecha); //Fecha inicial
      let fin      = new Date(fecha2); //Fecha final
      // Sacamos la diferencia de días
      let timeDiff = Math.abs(fin.getTime() - inicio.getTime());
      // Sacamos el total de días entre las fechas con un claculo de timepo
      let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); //Días entre las dos fechas

      // iniciamos en -1 ¿por qué? por que si no, solo recorreríamos el segundo día, y debemos recorrer desde el día de inicio
      for (var i=-1; i < diffDays; i++){
        // Agregamos un día
        inicio.setDate(inicio.getDate() + 1);
        // Recuperamos el dia de la semana de esa fecha
        const { diaClase } = await Kpi.getNumberDiaBDEspecifico( inicio ).then(response => response)
        // Creamos lo qe vamos a concatenar a la fecha
        let concatDia = ` AND ${ diaClase } = 1` 
        let select    = '*'
        let group     = ''
        // Calculamos las clases de ese día
        clasesTotalesFast = await Kpi.getReporteClasesTotalesFast( cicloFast, concatDia, group, select ).then(response=> response)
        // Hacemos la sumatoria
        totalFast += clasesTotalesFast.length
        // // Clases totales INBI
        clasesTotalesInbi = await Kpi.getReporteClasesTotalesInbi( cicloInbi, concatDia, group, select).then(response=> response)
        totalInbi += clasesTotalesInbi.length
      }

    }

    if(tipo == 3){
      let concatDia = ``
      let select    = ' SUM(cu.diasdeFrecuencia * 4 ) AS dias '
      let group     = ' GROUP BY id_ciclo '
      // Clases totales FAST
      clasesTotalesFast = await Kpi.getReporteClasesTotalesFast( cicloFast, concatDia, group, select ).then(response=> response)
      totalFast = clasesTotalesFast[0].dias
      // Rellenmos el arreglo, ya que en este caso, se agrupan las clases
      // // Clases totales INBI
      clasesTotalesInbi = await Kpi.getReporteClasesTotalesInbi( cicloInbi, concatDia, group, select ).then(response=> response)
      totalInbi = clasesTotalesInbi[0].dias
    }


    // Clases totales FAST
    const clasesMalasFast = await Kpi.getReporteClasesMalasFast( fecha, fecha2, cicloFast ).then(response=> response)
    // // Clases totales INBI
    const clasesMalasInbi = await Kpi.getReporteclasesMalasInbi( fecha, fecha2, cicloInbi ).then(response=> response)


    let resultadoFinal = {
      totales:{
        clases_totales:    totalFast + totalInbi,
        clases_malas:      (clasesMalasFast.length + clasesMalasInbi.length ) , 
        clases_buenas:     clasesFast.length + clasesInbi.length,
        no_evaluadas:      (totalFast + totalInbi) - (clasesFast.length + clasesInbi.length + clasesMalasFast.length + clasesMalasInbi.length),
        porcentajesBuenas: (((clasesFast.length + clasesInbi.length) / (totalFast + totalInbi)) * 100).toFixed(2).toString() + ' %'
      },

      fast:{
        clases_totales:    totalFast,
        clases_malas:      clasesMalasFast.length,
        clases_buenas:     clasesFast.length,
        no_evaluadas:      totalFast - ( clasesMalasFast.length + clasesFast.length ) ,
        porcentajesBuenas: ((clasesFast.length  / totalFast) * 100).toFixed(2).toString() + ' %'
      },

      inbi:{
        clases_totales:    totalInbi,
        clases_malas:      clasesMalasInbi.length,
        clases_buenas:     clasesInbi.length,
        no_evaluadas:      totalInbi - ( clasesMalasInbi.length + clasesInbi.length ) ,
        porcentajesBuenas: ((clasesInbi.length  / totalInbi) * 100).toFixed(2).toString() + ' %'
      }
    }

    // RESPONDEMOS
    res.send(resultadoFinal);
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getTeacherActivos = async (req, res) => {
  try{
    // Desestructuramos los parametros
    const { cicloFast, cicloInbi } = req.params

    // Obtener los teacher del bloque 1 osea lunes, miercoles y viernes
    const teacherFast  = await Kpi.getTeacherFAST( cicloFast, 1 ).then(response=> response)
    // Obtener los teacher del bloque 2 osea lunes, miercoles y viernes
    const teacherFast2 = await Kpi.getTeacherFAST( cicloFast, 2 ).then(response=> response)
    // Obtener los teacher del bloque 1 osea lunes, miercoles y viernes
    const teacherInbi  = await Kpi.getTeacherINBI( cicloInbi, 1 ).then(response=> response)
    // Obtener los teacher del bloque 2 osea lunes, miercoles y viernes
    const teacherInbi2 = await Kpi.getTeacherINBI( cicloInbi, 2 ).then(response=> response)

    let teachers = teacherFast.concat(teacherFast2)
    teachers = teachers.concat(teacherInbi)
    teachers = teachers.concat(teacherInbi2)
    // RESPONDEMOS
    res.send(teachers);
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getClasesHoraDiaReporteTeacher = async (req, res) => {
  try{
    // Desestructuramos los ciclos
    const { fecha, fecha2, cicloFast, cicloInbi, email } = req.body

    // Primero debemos obtener el dia en que se esta llevamos a  cabo la clase
    const { diaEspecifico } = await Kpi.getNumberDiaEspecificoBD( fecha ).then(response => response)
    const { dia } = await Kpi.getNumberDiaBD( fecha ).then(response => response)

    // Primero obtenemos las clases buenas de FAST
    const clasesFast = await Kpi.getReporteClasesBuenasFastTeacher( fecha, fecha2, cicloFast, email ).then(response=> response)
    // Y ahora obtenemos las clases buenas de INBI
    const clasesInbi = await Kpi.getReporteClasesBuenasInbiTeacher( fecha, fecha2, cicloInbi, email ).then(response=> response)

    let number_teacer = 0
    let number_teacer2 = 0
    // Clases totales FAST
    if(diaEspecifico <= 3 || diaEspecifico >=6){ 
      number_teacer  = 1
      number_teacer2 = 1
    }else{ 
      number_teacer  = 2
      number_teacer2 = 2
    }

    if(fecha != fecha2){
      number_teacer  = 1
      number_teacer2 = 2 
    }

    // console.log(diaEspecifico, number_teacer,number_teacer2)
    const clasesTotalesFast = await Kpi.getReporteClasesTotalesFastTeacher( dia, cicloFast, email, number_teacer, number_teacer2 ).then(response=> response)
    // // Clases totales INBI
    const clasesTotalesInbi = await Kpi.getReporteClasesTotalesInbiTeacher( dia, cicloInbi, email, number_teacer, number_teacer2 ).then(response=> response)


    // Clases totales FAST
    const clasesMalasFast = await Kpi.getReporteClasesMalasFastTeacher( fecha, fecha2, cicloFast, email ).then(response=> response)
    // // Clases totales INBI
    const clasesMalasInbi = await Kpi.getReporteclasesMalasInbiTeacher( fecha, fecha2, cicloInbi, email ).then(response=> response)


    let resultadoFinal = {
      totales:{
        clases_totales:    clasesTotalesFast.length + clasesTotalesInbi.length,
        clases_malas:      (clasesMalasFast.length + clasesMalasInbi.length ) , 
        clases_buenas:     clasesFast.length + clasesInbi.length,
        no_evaluadas:      (clasesTotalesFast.length + clasesTotalesInbi.length) - (clasesFast.length + clasesInbi.length + clasesMalasFast.length + clasesMalasInbi.length),
        porcentajesBuenas: (((clasesFast.length + clasesInbi.length) / (clasesTotalesFast.length + clasesTotalesInbi.length)) * 100).toFixed(2).toString() + ' %'
      },

      fast:{
        clases_totales:    clasesTotalesFast.length,
        clases_malas:      clasesMalasFast.length,
        clases_buenas:     clasesFast.length,
        no_evaluadas:      clasesTotalesFast.length - ( clasesMalasFast.length + clasesFast.length ) ,
        porcentajesBuenas: ((clasesFast.length  / clasesTotalesFast.length) * 100).toFixed(2).toString() + ' %'
      },

      inbi:{
        clases_totales:    clasesTotalesInbi.length,
        clases_malas:      clasesMalasInbi.length,
        clases_buenas:     clasesInbi.length,
        no_evaluadas:      clasesTotalesInbi.length - ( clasesMalasInbi.length + clasesInbi.length ) ,
        porcentajesBuenas: ((clasesInbi.length  / clasesTotalesInbi.length) * 100).toFixed(2).toString() + ' %'
      }
    }

    // RESPONDEMOS
    res.send(resultadoFinal);
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getClasesReporteTeacher = async (req, res) => {
  try{
    // Desestructuramos los ciclos
    const { fecha, fecha2, cicloFast, cicloInbi, tipo } = req.body

    // Inicializamos las variables
    let totalFast = []
    let totalInbi = []
    let clasesTotalesFast = null


    // Primero debemos obtener el dia en que se esta llevamos a  cabo la clase
    const { diaEspecifico } = await Kpi.getNumberDiaEspecificoBD( fecha ).then(response => response)
    const { dia } = await Kpi.getNumberDiaBD( fecha ).then(response => response)

    // Consultar los maestros que están dando clase
    const teachersFast = await Kpi.getTeachersFast( cicloFast ).then(response=> response)
    // Y ahora obtenemos las clases buenas de INBI
    const teachersInbi = await Kpi.getTeachersInbi( cicloInbi ).then(response=> response)


    let number_teacer = 0
    let number_teacer2 = 0
    
    // Se verifica el día, si es 1,2,3,6,7 son de teacher 1
    if(diaEspecifico <= 3 || diaEspecifico >=6){ 
      number_teacer  = 1
      number_teacer2 = 1
    }else{ 
      number_teacer  = 2
      number_teacer2 = 2
    }

    if(fecha != fecha2){
      number_teacer  = 1
      number_teacer2 = 2 
    }

    let resultadoFinal = []


    if( tipo == 1){
      const { diaClase } = await Kpi.getNumberDiaBDEspecifico( fecha ).then(response => response)
      // Mandamos como parameto el ciclo, el dia, y el multiplicador
      // Clases totales FAST
      let concatDia = ` AND ${ diaClase } = 1` 

      clasesTotalesFast       = await Kpi.getTotalClasesFAST( concatDia, cicloFast, fecha ).then(response=> response)
      clasesTotalesBuenasFast = await Kpi.getTotalClasesBuenasFAST(  fecha, fecha2, cicloFast ).then(response=> response)
      clasesTotalesMalasFast  = await Kpi.getTotalClasesMalasFAST(  fecha, fecha2, cicloFast ).then(response=> response)

      clasesTotalesInbi       = await Kpi.getTotalClasesINBI( concatDia, cicloInbi, fecha ).then(response=> response)
      clasesTotalesBuenasInbi = await Kpi.getTotalClasesBuenasINBI(  fecha, fecha2, cicloInbi ).then(response=> response)
      clasesTotalesMalasInbi  = await Kpi.getTotalClasesMalasINBI(  fecha, fecha2, cicloInbi ).then(response=> response)

      
      // Concatenamos los maestros
      let teachers = teachersFast.concat(teachersInbi)

      let hash = {};
      let teachersResult = teachers.filter((teacher) => {
        let exists = !hash[teacher.email];
        hash[teacher.email] = true;
        return exists;
      });

      // Recorremos los maestros
      for(const i in teachersResult){
        // Calculamos las clases de FAST
        const clases  = clasesTotalesFast.filter(grupo => grupo.email == teachersResult[i].email)
        const clasesB = clasesTotalesBuenasFast.filter(grupo => grupo.email == teachersResult[i].email)
        const clasesM = clasesTotalesMalasFast.filter(grupo => grupo.email == teachersResult[i].email)

        // Calculamos las clases de INBI
        const clasesI  = clasesTotalesInbi.filter(grupo => grupo.email == teachersResult[i].email)
        const clasesBI = clasesTotalesBuenasInbi.filter(grupo => grupo.email == teachersResult[i].email)
        const clasesMI = clasesTotalesMalasInbi.filter(grupo => grupo.email == teachersResult[i].email)

        if(clases.length > 0 || clasesI.length > 0){
          resultadoFinal.push({
            ...teachersResult[i],
            total_clases:  clases.length + clasesI.length,
            clases_buenas: clasesB.length + clasesBI.length,
            clases_malas:  clasesM.length + clasesMI.length,
            sin_evaluar:   (clases.length + clasesI.length- ( (clasesB.length + clasesBI.length) + (clasesM.length + clasesMI.length) )),
            porcentaje:    (( (clasesB.length + clasesBI.length)  / (clases.length + clasesI.length) ) * 100).toFixed(0)
          })
        }
      }
    }

    if( tipo == 2){
      const { diaClase } = await Kpi.getNumberDiaBDEspecifico( fecha ).then(response => response)
      // aquí trabajamos con un rango de fechas

      // Convertimos las fechas a tipo fecha
      let inicio   = new Date(fecha); //Fecha inicial
      let fin      = new Date(fecha2); //Fecha final
      // Sacamos la diferencia de días
      let timeDiff = Math.abs(fin.getTime() - inicio.getTime());
      // Sacamos el total de días entre las fechas con un claculo de timepo
      let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); //Días entre las dos fechas

      // iniciamos en -1 ¿por qué? por que si no, solo recorreríamos el segundo día, y debemos recorrer desde el día de inicio
      for (var i=-1; i < diffDays; i++){
        // Agregamos un día
        inicio.setDate(inicio.getDate() + 1);
        // Recuperamos el dia de la semana de esa fecha
        const { diaClase } = await Kpi.getNumberDiaBDEspecifico( inicio ).then(response => response)
        // Creamos lo qe vamos a concatenar a la fecha
        let concatDia = ` AND ${ diaClase } = 1` 
        // Calculamos las clases de ese día
        clasesTotalesFast       = await Kpi.getTotalClasesFAST( concatDia, cicloFast, inicio ).then(response=> response)
        // Hacemos la sumatoria
        totalFast = totalFast.concat(clasesTotalesFast) 
        // // Clases totales INBI
        clasesTotalesInbi       = await Kpi.getTotalClasesINBI( concatDia, cicloInbi, inicio ).then(response=> response)
        totalInbi = totalInbi.concat(clasesTotalesInbi) 
      }

      clasesTotalesBuenasFast = await Kpi.getTotalClasesBuenasFAST(  fecha, fecha2, cicloFast ).then(response=> response)
      clasesTotalesMalasFast  = await Kpi.getTotalClasesMalasFAST(  fecha, fecha2, cicloFast ).then(response=> response)
      
      clasesTotalesBuenasInbi = await Kpi.getTotalClasesBuenasINBI(  fecha, fecha2, cicloInbi ).then(response=> response)
      clasesTotalesMalasInbi  = await Kpi.getTotalClasesMalasINBI(  fecha, fecha2, cicloInbi ).then(response=> response)

      
      // Concatenamos los maestros
      let teachers = teachersFast.concat(teachersInbi)

      let hash = {};
      let teachersResult = teachers.filter((teacher) => {
        let exists = !hash[teacher.email];
        hash[teacher.email] = true;
        return exists;
      });

      // Recorremos los maestros
      for(const i in teachersResult){

        // Calculamos las clases de FAST
        const clases  = totalFast.filter(grupo => grupo.email == teachersResult[i].email)
        const clasesB = clasesTotalesBuenasFast.filter(grupo => grupo.email == teachersResult[i].email)
        const clasesM = clasesTotalesMalasFast.filter(grupo => grupo.email == teachersResult[i].email)

        // Calculamos las clases de INBI
        const clasesI  = totalInbi.filter(grupo => grupo.email == teachersResult[i].email)
        const clasesBI = clasesTotalesBuenasInbi.filter(grupo => grupo.email == teachersResult[i].email)
        const clasesMI = clasesTotalesMalasInbi.filter(grupo => grupo.email == teachersResult[i].email)

        if(clases.length > 0 || clasesI.length ){
          resultadoFinal.push({
            ...teachersResult[i],
            total_clases:  clases.length + clasesI.length,
            clases_buenas: clasesB.length + clasesBI.length,
            clases_malas:  clasesM.length + clasesMI.length,
            sin_evaluar:   (clases.length + clasesI.length- ( (clasesB.length + clasesBI.length) + (clasesM.length + clasesMI.length) )),
            porcentaje:    (( (clasesB.length + clasesBI.length)  / (clases.length + clasesI.length) ) * 100).toFixed(0)
          })
        }
      }
    }

    if( tipo == 3){
      const { diaClase } = await Kpi.getNumberDiaBDEspecifico( fecha ).then(response => response)

      const { fecha_inicio, fecha_fin } = await Kpi.getFechasCiclo( cicloFast ).then(response => response)
      // aquí trabajamos con un rango de fechas

      // Convertimos las fechas a tipo fecha
      let inicio   = new Date(fecha_inicio); //Fecha inicial
      let fin      = new Date(fecha_fin); //Fecha final
      // Sacamos la diferencia de días
      let timeDiff = Math.abs(fin.getTime() - inicio.getTime());
      // Sacamos el total de días entre las fechas con un claculo de timepo
      let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); //Días entre las dos fechas

      // iniciamos en -1 ¿por qué? por que si no, solo recorreríamos el segundo día, y debemos recorrer desde el día de inicio
      for (var i=-1; i < diffDays; i++){
        // Agregamos un día
        inicio.setDate(inicio.getDate() + 1);
        // Recuperamos el dia de la semana de esa fecha
        const { diaClase } = await Kpi.getNumberDiaBDEspecifico( inicio ).then(response => response)
        // Creamos lo qe vamos a concatenar a la fecha
        let concatDia = ` AND ${ diaClase } = 1` 
        // Calculamos las clases de ese día
        clasesTotalesFast       = await Kpi.getTotalClasesFAST( concatDia, cicloFast, inicio ).then(response=> response)
        // Hacemos la sumatoria
        totalFast = totalFast.concat(clasesTotalesFast) 
        // // Clases totales INBI
        clasesTotalesInbi       = await Kpi.getTotalClasesINBI( concatDia, cicloInbi, inicio ).then(response=> response)
        totalInbi = totalInbi.concat(clasesTotalesInbi) 
      }

      clasesTotalesBuenasFast = await Kpi.getTotalClasesBuenasFAST( fecha_inicio, fecha_fin, cicloFast ).then(response=> response)
      clasesTotalesMalasFast  = await Kpi.getTotalClasesMalasFAST(  fecha_inicio, fecha_fin, cicloFast ).then(response=> response)
      
      clasesTotalesBuenasInbi = await Kpi.getTotalClasesBuenasINBI( fecha_inicio, fecha_fin, cicloInbi ).then(response=> response)
      clasesTotalesMalasInbi  = await Kpi.getTotalClasesMalasINBI(  fecha_inicio, fecha_fin, cicloInbi ).then(response=> response)

      
      // Concatenamos los maestros
      let teachers = teachersFast.concat(teachersInbi)

      let hash = {};
      let teachersResult = teachers.filter((teacher) => {
        let exists = !hash[teacher.email];
        hash[teacher.email] = true;
        return exists;
      });

      // Recorremos los maestros
      for(const i in teachersResult){

        // Calculamos las clases de FAST
        const clases  = totalFast.filter(grupo => grupo.email == teachersResult[i].email)
        const clasesB = clasesTotalesBuenasFast.filter(grupo => grupo.email == teachersResult[i].email)
        const clasesM = clasesTotalesMalasFast.filter(grupo => grupo.email == teachersResult[i].email)

        // Calculamos las clases de INBI
        const clasesI  = totalInbi.filter(grupo => grupo.email == teachersResult[i].email)
        const clasesBI = clasesTotalesBuenasInbi.filter(grupo => grupo.email == teachersResult[i].email)
        const clasesMI = clasesTotalesMalasInbi.filter(grupo => grupo.email == teachersResult[i].email)

        if(clases.length > 0 || clasesI.length ){
          resultadoFinal.push({
            ...teachersResult[i],
            total_clases:  clases.length + clasesI.length,
            clases_buenas: clasesB.length + clasesBI.length,
            clases_malas:  clasesM.length + clasesMI.length,
            sin_evaluar:   (clases.length + clasesI.length- ( (clasesB.length + clasesBI.length) + (clasesM.length + clasesMI.length) )),
            porcentaje:    (( (clasesB.length + clasesBI.length)  / (clases.length + clasesI.length) ) * 100).toFixed(0)
          })
        }
      }
    }

    // RESPONDEMOS
    res.send(resultadoFinal);
  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getNuevasMatriculas = async (req, res) => {
  try{
    // Desestructuramos los ciclos
    const { fechaini, fechafin } = req.body

    let  nuevasMatriculas = await Kpi.getNuevasMatriculas(  fechaini, fechafin ).then(response=> response)

    // Sacar los alumnos unicos
    // SACAR LAS VENDEDORAS UNICAS DE FAST
    let arrayAlumnos = nuevasMatriculas.map(item=>{ return [item.id_alumno,item] });
    // Creamos un map de los alumnos
    var alumnos = new Map(arrayAlumnos); // Pares de clave y valor
    // y ahora si, extraemos los valores unicos
    let alumnosUnicos = [...alumnos.values()]; // Conversión a un array


    const matriculasFast = alumnosUnicos.filter( el => { return el.unidad_negocio == 2 })
    const matriculasInbi = alumnosUnicos.filter( el => { return el.unidad_negocio == 1 })

    const totalFast = alumnosUnicos.filter( el => { return el.unidad_negocio == 2 }).length
    const totalInbi = alumnosUnicos.filter( el => { return el.unidad_negocio == 1 }).length


    // SACAR LAS VENDEDORAS UNICAS DE INBI
    let arrayInbi = matriculasInbi.map(item=>{ return [item.vendedora,item] });
    // Creamos un map de los alumnos
    var inbi = new Map(arrayInbi); // Pares de clave y valor
    // y ahora si, extraemos los valores unicos
    let vendedorasInbi = [...inbi.values()]; // Conversión a un array


    // SACAR LAS VENDEDORAS UNICAS DE FAST
    let arrayFast = matriculasFast.map(item=>{ return [item.vendedora,item] });
    // Creamos un map de los alumnos
    var fast = new Map(arrayFast); // Pares de clave y valor
    // y ahora si, extraemos los valores unicos
    let vendedorasFast = [...fast.values()]; // Conversión a un array


    const pagos = [1399,1400,925,1540,849,850,930,765,770,800]

    for( const i in vendedorasInbi ){
      const { vendedora } = vendedorasInbi[i]
      vendedorasInbi[i]['con_adeudo']  = matriculasInbi.filter( el => { return el.vendedora == vendedora && el.adeudo > 0 || el.induccion == 1 && !pagos.includes( el.pagado ) }).length
      vendedorasInbi[i]['sin_adeudo']  = matriculasInbi.filter( el => { return el.vendedora == vendedora && el.adeudo <= 0 && el.induccion == 1 && pagos.includes( el.pagado ) }).length
      vendedorasInbi[i]['completos']   = matriculasInbi.filter( el => { return el.vendedora == vendedora && el.adeudo <= 0 && el.induccion == 0 }).length
      vendedorasInbi[i]['total']       = matriculasInbi.filter( el => { return el.vendedora == vendedora }).length
    }


    for( const i in vendedorasFast ){
      const { vendedora } = vendedorasFast[i]
      vendedorasFast[i]['con_adeudo'] = matriculasFast.filter( el => { return el.vendedora == vendedora && el.adeudo > 0 || el.induccion == 1 && !pagos.includes( el.pagado ) }).length
      vendedorasFast[i]['sin_adeudo'] = matriculasFast.filter( el => { return el.vendedora == vendedora && el.adeudo <= 0 && el.induccion == 1  && pagos.includes( el.pagado ) }).length
      vendedorasFast[i]['completos']  = matriculasFast.filter( el => { return el.vendedora == vendedora && el.adeudo <= 0 && el.induccion == 0 }).length
      vendedorasFast[i]['total']      = matriculasFast.filter( el => { return el.vendedora == vendedora }).length
    }


    // RESPONDEMOS
    res.send({ vendedorasInbi, vendedorasFast, totalFast, totalInbi, nuevasMatriculas: alumnosUnicos});
  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getNuevasMatriculasCiclo = async (req, res) => {
  try{
    // Desestructuramos los ciclos
    const { ciclo, fecha_inicio_ciclo, id_ciclo, id_ciclo_relacionado } = req.body

    const cicloAnterior = await Kpi.getCicloAnterior( fecha_inicio_ciclo, ciclo ).then( response => response )
    if( !cicloAnterior ){ return res.status( 500 ).send({ message: 'Ciclo anterior no existe' }) }

    const nombreCicloAnterior = cicloAnterior.nombreCicloAnterior
    const nombreCicloActual   = cicloAnterior.nombreCicloActual

    let alumnosInduccion = await Kpi.getAlumnosInducción( nombreCicloAnterior, nombreCicloActual ).then(response=> response)

    // SACAR A LOS ALUMNOS UNICOS
    let idAlumnos = alumnosInduccion.map((registro)=> { return registro.id_alumno })

    idAlumnos = idAlumnos.length ? idAlumnos : [0]
    let alumnosInduccionConGrupo = await Kpi.getAlumnosGrupoNormal( idAlumnos ).then(response=> response)


    for( const i in alumnosInduccion ){
      const { id_alumno, adeudo, grupo, pagado } = alumnosInduccion[i]

      // Verificar si tiene un grupo normal estos alumnos
      const existeGrupoNormal = alumnosInduccionConGrupo.find( el => el.id_alumno == id_alumno )

      alumnosInduccion[i]['induccion'] = existeGrupoNormal ? 0 : 1
      alumnosInduccion[i]['adeudo']    = existeGrupoNormal ? existeGrupoNormal.adeudo : adeudo
      alumnosInduccion[i]['grupo']     = existeGrupoNormal ? existeGrupoNormal.grupo  : grupo
      alumnosInduccion[i]['pagado']    = existeGrupoNormal ? existeGrupoNormal.pagado : pagado
    }

    const alumnosNICicloFast = await Kpi.getAlumnosNI( id_ciclo_relacionado, idAlumnos ).then(response=> response)
    const alumnosNICicloInbi = await Kpi.getAlumnosNI( id_ciclo, idAlumnos ).then(response=> response)

    alumnosInduccion = alumnosInduccion.concat( alumnosNICicloFast ).concat( alumnosNICicloInbi )

    const matriculasFast = alumnosInduccion.filter( el => { return el.unidad_negocio == 2 })
    const matriculasInbi = alumnosInduccion.filter( el => { return el.unidad_negocio == 1 })

    const totalFast = alumnosInduccion.filter( el => { return el.unidad_negocio == 2 && el.adeudo <= 0 && el.induccion == 0 }).length
    const totalInbi = alumnosInduccion.filter( el => { return el.unidad_negocio == 1 && el.adeudo <= 0 && el.induccion == 0 }).length

    // SACAR LAS VENDEDORAS UNICAS DE INBI
    let arrayInbi = matriculasInbi.map(item=>{ return [item.vendedora,item] });
    // Creamos un map de los alumnos
    var inbi = new Map(arrayInbi); // Pares de clave y valor
    // y ahora si, extraemos los valores unicos
    let vendedorasInbi = [...inbi.values()]; // Conversión a un array


    // SACAR LAS VENDEDORAS UNICAS DE FAST
    let arrayFast = matriculasFast.map(item=>{ return [item.vendedora,item] });
    // Creamos un map de los alumnos
    var fast = new Map(arrayFast); // Pares de clave y valor
    // y ahora si, extraemos los valores unicos
    let vendedorasFast = [...fast.values()]; // Conversión a un array

    const pagos = [1399,1400,925,1540,749,849,850,930,765,770,800,1255]

    const alumno = matriculasInbi.find( el => { return el.id_alumno == 9965})

    for( const i in vendedorasInbi ){
      const { vendedora } = vendedorasInbi[i]
      const matriculasAdeudo = matriculasInbi.filter( el => { return el.vendedora == vendedora && el.adeudo > 0 })


      vendedorasInbi[i] = {
        ...vendedorasInbi[i],
        con_adeudo:   matriculasInbi.filter( el => { return el.vendedora == vendedora && el.adeudo > 0  || el.vendedora == vendedora && el.induccion == 1 && !pagos.includes( el.pagado ) }).length,
        con_adeudoA:  matriculasInbi.filter( el => { return el.vendedora == vendedora && el.adeudo > 0  || el.vendedora == vendedora && el.induccion == 1 && !pagos.includes( el.pagado ) }),
        sin_adeudo:   matriculasInbi.filter( el => { return el.vendedora == vendedora && el.adeudo <= 0 && el.induccion == 1 && pagos.includes( el.pagado ) }).length,
        sin_adeudoA:  matriculasInbi.filter( el => { return el.vendedora == vendedora && el.adeudo <= 0 && el.induccion == 1 && pagos.includes( el.pagado ) }),
        completos:    matriculasInbi.filter( el => { return el.vendedora == vendedora && el.adeudo <= 0 && el.induccion == 0 }).length,
        completosA:   matriculasInbi.filter( el => { return el.vendedora == vendedora && el.adeudo <= 0 && el.induccion == 0 }),
        total:        matriculasInbi.filter( el => { return el.vendedora == vendedora }).length,
        totalA:       matriculasInbi.filter( el => { return el.vendedora == vendedora }),
      }
    }

    // Totales
    vendedorasInbi.push({
      vendedora:   'TOTAL',
      con_adeudo:  matriculasInbi.filter( el => { return el.adeudo > 0  || el.induccion == 1 && !pagos.includes( el.pagado ) }).length,
      con_adeudoA: matriculasInbi.filter( el => { return el.adeudo > 0  || el.induccion == 1 && !pagos.includes( el.pagado ) }),
      sin_adeudo:  matriculasInbi.filter( el => { return el.adeudo <= 0 && el.induccion == 1 && pagos.includes( el.pagado ) }).length,
      sin_adeudoA: matriculasInbi.filter( el => { return el.adeudo <= 0 && el.induccion == 1 && pagos.includes( el.pagado ) }),
      completos:   matriculasInbi.filter( el => { return el.adeudo <= 0 && el.induccion == 0 }).length,
      completosA:  matriculasInbi.filter( el => { return el.adeudo <= 0 && el.induccion == 0 }),
      total:       matriculasInbi.length,
      totalA:      matriculasInbi
    })

    for( const i in vendedorasFast ){
      const { vendedora } = vendedorasFast[i]
      vendedorasFast[i] = {
        ...vendedorasFast[i],
        con_adeudo:   matriculasFast.filter( el => { return el.vendedora == vendedora && el.adeudo > 0 || el.vendedora == vendedora && el.induccion == 1 && !pagos.includes( el.pagado ) }).length,
        con_adeudoA:  matriculasFast.filter( el => { return el.vendedora == vendedora && el.adeudo > 0 || el.vendedora == vendedora && el.induccion == 1 && !pagos.includes( el.pagado ) }),
        sin_adeudo:   matriculasFast.filter( el => { return el.vendedora == vendedora && el.adeudo <= 0 && el.induccion == 1 && pagos.includes( el.pagado ) }).length,
        sin_adeudoA:  matriculasFast.filter( el => { return el.vendedora == vendedora && el.adeudo <= 0 && el.induccion == 1 && pagos.includes( el.pagado ) }),
        completos:    matriculasFast.filter( el => { return el.vendedora == vendedora && el.adeudo <= 0 && el.induccion == 0 }).length,
        completosA:   matriculasFast.filter( el => { return el.vendedora == vendedora && el.adeudo <= 0 && el.induccion == 0 }),
        total:        matriculasFast.filter( el => { return el.vendedora == vendedora }).length,
        totalA:       matriculasFast.filter( el => { return el.vendedora == vendedora }),
      }
    }

    // Totales
    vendedorasFast.push({
      vendedora:   'TOTAL',
      con_adeudo:  matriculasFast.filter( el => { return el.adeudo > 0  || el.induccion == 1 && !pagos.includes( el.pagado ) }).length,
      con_adeudoA: matriculasFast.filter( el => { return el.adeudo > 0  || el.induccion == 1 && !pagos.includes( el.pagado ) }),
      sin_adeudo:  matriculasFast.filter( el => { return el.adeudo <= 0 && el.induccion == 1 && pagos.includes( el.pagado ) }).length,
      sin_adeudoA: matriculasFast.filter( el => { return el.adeudo <= 0 && el.induccion == 1 && pagos.includes( el.pagado ) }),
      completos:   matriculasFast.filter( el => { return el.adeudo <= 0 && el.induccion == 0 }).length,
      completosA:  matriculasFast.filter( el => { return el.adeudo <= 0 && el.induccion == 0 }),
      total:       matriculasFast.length,
      totalA:      matriculasFast
    })

    // SACAR LOS TELEFONOS Y NIVELES DE LOS EXAMENES DE FAST
    const examenesInduccionCEU   = await Kpi.examenesInduccionCEU( ).then( response => response )
    const examenesInduccionCEU2  = await Kpi.examenesInduccionCEU2( ).then( response => response )
    const examenesInduccionCEU3  = await Kpi.examenesInduccionCEU3( ).then( response => response )
    const examenesInduccionCEI   = await Kpi.examenesInduccionCEI( ).then( response => response )

    const matriculasFastCompletas = matriculasFast.filter( el => { return el.adeudo <= 0 && el.induccion == 0 })


    for( const i in matriculasFastCompletas ){
      let { telefono,  celular, telefonoTutor, celularTutor } = matriculasFastCompletas[i]

      telefono      = telefono      ? telefono      : 'SIN TELEFONO' 
      celular       = celular       ? celular       : 'SIN TELEFONO' 
      telefonoTutor = telefonoTutor ? telefonoTutor : 'SIN TELEFONO' 
      celularTutor  = celularTutor  ? celularTutor  : 'SIN TELEFONO' 
      
      // VAlidar si existen los datos

      const existe1 = examenesInduccionCEU.find( el => [telefono,  celular, telefonoTutor, celularTutor].includes( el.telefono )) 
      const existe2 = examenesInduccionCEU2.find( el => [telefono,  celular, telefonoTutor, celularTutor].includes( el.telefono )) 
      const existe3 = examenesInduccionCEU3.find( el => [telefono,  celular, telefonoTutor, celularTutor].includes( el.telefono )) 
      const existe4 = examenesInduccionCEI.find( el => [telefono,  celular, telefonoTutor, celularTutor].includes( el.telefono )) 

      if( existe1 ){
        matriculasFastCompletas[i]['nivel']    = existe1.nivel
        matriculasFastCompletas[i]['nombre_i'] = existe1.nombre
        matriculasFastCompletas[i]['diploma']  = `https://www.fastenglish.com.mx/english-level-test/diploma.php?id=${existe1.id}`
      }else if( existe2 ){
        matriculasFastCompletas[i]['nivel']    = existe2.nivel
        matriculasFastCompletas[i]['nombre_i'] = existe2.nombre
        matriculasFastCompletas[i]['diploma']  = `https://www.fastenglish.com.mx/conoce-tu-nivel-de-ingles/diploma.php?id=${existe2.id}`
      }else if( existe3 ){
        matriculasFastCompletas[i]['nivel']    = existe3.nivel
        matriculasFastCompletas[i]['nombre_i'] = existe3.nombre
        matriculasFastCompletas[i]['diploma']  = `https://www.fastenglish.com.mx/conoce-tu-nivel-de-ingles-ceaa/diploma.php?id=${existe3.id}`
      }else if( existe4 ){
        matriculasFastCompletas[i]['nivel']    = existe4.nivel
        matriculasFastCompletas[i]['nombre_i'] = existe4.nombre
        matriculasFastCompletas[i]['diploma']  = `https://www.fastenglish.com.mx/examen-interno/diploma.php?id=${existe4.id}`
      }else{
        matriculasFastCompletas[i]['nivel']    = 'SIN NIVEL'
        matriculasFastCompletas[i]['nombre_i'] = 'SIN NOMBRE'
        matriculasFastCompletas[i]['diploma']  = 'SIN DIPLOMA'
      }

    }


    // INBIIIIIIIIIIIIIIIIIIIIIIIIIIII
    // SACAR LOS TELEFONOS Y NIVELES DE LOS EXAMENES DE FAST
    const examenesInduccionEI = await Kpi.examenesInduccionEI( ).then( response => response )
    const examenesInduccionET = await Kpi.examenesInduccionET( ).then( response => response )
    const examenesInduccionR1 = await Kpi.examenesInduccionR1( ).then( response => response )
    const examenesInduccionR2 = await Kpi.examenesInduccionR2( ).then( response => response )

    const matriculasInbiCompletas = matriculasInbi.filter( el => { return el.adeudo <= 0 && el.induccion == 0 })


    for( const i in matriculasInbiCompletas ){
      let { telefono,  celular, telefonoTutor, celularTutor } = matriculasInbiCompletas[i]

      telefono      = telefono      ? telefono      : 'SIN TELEFONO' 
      celular       = celular       ? celular       : 'SIN TELEFONO' 
      telefonoTutor = telefonoTutor ? telefonoTutor : 'SIN TELEFONO' 
      celularTutor  = celularTutor  ? celularTutor  : 'SIN TELEFONO' 
      
      // VAlidar si existen los datos

      const existe1 = examenesInduccionEI.find( el => [telefono,  celular, telefonoTutor, celularTutor].includes( el.telefono )) 
      const existe2 = examenesInduccionET.find( el => [telefono,  celular, telefonoTutor, celularTutor].includes( el.telefono )) 
      const existe3 = examenesInduccionR1.find( el => [telefono,  celular, telefonoTutor, celularTutor].includes( el.telefono )) 
      const existe4 = examenesInduccionR2.find( el => [telefono,  celular, telefonoTutor, celularTutor].includes( el.telefono )) 

      if( existe1 ){
        matriculasInbiCompletas[i]['nivel']    = existe1.nivel
        matriculasInbiCompletas[i]['nombre_i'] = existe1.nombre
        matriculasInbiCompletas[i]['diploma']  = `https://www.inbi.mx/examen-interno/diploma.php?id=${existe1.id}`
      }else if( existe2 ){
        matriculasInbiCompletas[i]['nivel']    = existe2.nivel
        matriculasInbiCompletas[i]['nombre_i'] = existe2.nombre
        matriculasInbiCompletas[i]['diploma']  = `https://www.inbi.mx/examen-teens/diploma.php?id=${existe2.id}`
      }else if( existe3 ){
        matriculasInbiCompletas[i]['nivel']    = existe3.nivel
        matriculasInbiCompletas[i]['nombre_i'] = existe3.nombre
        matriculasInbiCompletas[i]['diploma']  = `https://www.inbi.mx/examen-ubicacion/diploma.php?id=${existe3.id}`
      }else if( existe4 ){
        matriculasInbiCompletas[i]['nivel']    = existe4.nivel
        matriculasInbiCompletas[i]['nombre_i'] = existe4.nombre
        matriculasInbiCompletas[i]['diploma']  = `https://www.inbi.mx/examen-ubicacion/diploma.php?id=${existe4.id}`
      }else{
        matriculasInbiCompletas[i]['nivel']    = 'SIN NIVEL'
        matriculasInbiCompletas[i]['nombre_i'] = 'SIN NOMBRE'
        matriculasInbiCompletas[i]['diploma']  = 'SIN DIPLOMA'
      }

    }


    // RESPONDEMOS
    res.send({ vendedorasInbi, vendedorasFast, totalFast, totalInbi, nuevasMatriculas: alumnosInduccion});

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getInscritosMarketingRecepcion = async (req, res) => {
  try{

    // consultar los ciclos

    let ciclos = await Kpi.getCiclos( ).then( response => response )

    const primerCiclo = ciclos[0]
    let respuesta = []
    let respuesta2 = []

    const marketing = [25,48,49,47,23,55,54,21]

    ciclos.shift()

    for( const i in ciclos ){
      const {  id_ciclo, id_ciclo_relacionado, fecha_inicio_ciclo, ciclo } = ciclos[i]

      const cicloAnterior = await Kpi.getCicloAnterior( fecha_inicio_ciclo, ciclo ).then( response => response )
      if( !cicloAnterior ){ return res.status( 500 ).send({ message: 'Ciclo anterior no existe' }) }

      const nombreCicloAnterior = cicloAnterior.nombreCicloAnterior
      const nombreCicloActual   = cicloAnterior.nombreCicloActual

      let alumnosInduccion = await Kpi.getAlumnosInducción( nombreCicloAnterior, nombreCicloActual ).then(response=> response)

      // SACAR A LOS ALUMNOS UNICOS
      let idAlumnos = alumnosInduccion.map((registro)=> { return registro.id_alumno })
      idAlumnos = idAlumnos.length ? idAlumnos : [0]

      let alumnosInduccionConGrupo = await Kpi.getAlumnosGrupoNormal( idAlumnos ).then(response=> response)

      for( const i in alumnosInduccion ){
        const { id_alumno, adeudo, grupo, pagado } = alumnosInduccion[i]

        // Verificar si tiene un grupo normal estos alumnos
        const existeGrupoNormal = alumnosInduccionConGrupo.find( el => el.id_alumno == id_alumno )

        alumnosInduccion[i]['induccion'] = existeGrupoNormal ? 0 : 1
        alumnosInduccion[i]['adeudo']    = existeGrupoNormal ? existeGrupoNormal.adeudo : adeudo
        alumnosInduccion[i]['grupo']     = existeGrupoNormal ? existeGrupoNormal.grupo  : grupo
        alumnosInduccion[i]['pagado']    = existeGrupoNormal ? existeGrupoNormal.pagado : pagado
      }

      const alumnosNICicloFast = await Kpi.getAlumnosNI( id_ciclo_relacionado, idAlumnos ).then(response=> response)
      const alumnosNICicloInbi = await Kpi.getAlumnosNI( id_ciclo, idAlumnos ).then(response=> response)

      alumnosInduccion = alumnosInduccion.concat( alumnosNICicloFast ).concat( alumnosNICicloInbi )

      //  alumnosInduccion -> de aquí vamos a sacar ese desglose
      let celulares         = alumnosInduccion.map((registro) => { return registro.celular })
      let telefonos         = alumnosInduccion.map((registro) => { return registro.telefono })
      let idAlumnosAll      = alumnosInduccion.map((registro) => { return registro.id_alumno })
      let telefonoTutor     = alumnosInduccion.map((registro) => { return registro.telefonoTutor })
      let celularTutor      = alumnosInduccion.map((registro) => { return registro.celularTutor })

      celulares      = celulares.length     ? celulares     : [0]
      telefonos      = telefonos.length     ? telefonos     : [0]
      telefonoTutor  = telefonoTutor.length ? telefonoTutor : [0]
      celularTutor   = celularTutor.length  ? celularTutor  : [0]
      idAlumnosAll   = idAlumnosAll.length  ? idAlumnosAll  : [0]

      // Consultar los alumnos con esos teléfonos en los prospectos
      const prospectosVendedora = await prospectos.getProspectosxTelefonoOrIdAlumno( celulares, telefonos, idAlumnosAll, telefonoTutor, celularTutor ).then( response => response )

      let mmkt = []
      let mmkt2 = []
      let ventas = []
      let ventas2 = []

      for( const i in alumnosInduccion ){

        const { id_alumno, celular, unidad_negocio, telefono, telefonoTutor, celularTutor, adeudo, induccion, pagado } = alumnosInduccion[i]
        // Buscar vendedora
        const existeVendedora = prospectosVendedora.find( el => el.idalumno == id_alumno && el.escuela == unidad_negocio || el.telefono == celular  && el.escuela == unidad_negocio || el.telefono == telefono && el.escuela == unidad_negocio || el.telefono == telefonoTutor && el.escuela == unidad_negocio || el.telefono == celularTutor  && el.escuela == unidad_negocio )

        alumnosInduccion['idpuesto'] = existeVendedora ? existeVendedora.idpuesto : 0

        /* FAST */
        if( adeudo <= 0 && induccion == 0 && pagado > 0 && unidad_negocio == 2 ){
          if( marketing.includes(alumnosInduccion['idpuesto']) ){
            mmkt.push(alumnosInduccion)
          }else{
            ventas.push(alumnosInduccion)
          }
        }

        /* INBI */
        if( adeudo <= 0 && induccion == 0 && pagado > 0 && unidad_negocio == 1 ){
          if( marketing.includes(alumnosInduccion['idpuesto']) ){
            mmkt2.push(alumnosInduccion)
          }else{
            ventas2.push(alumnosInduccion)
          }
        }
      }

      const payload = {
        total:     alumnosInduccion.filter( el => { return el.adeudo <= 0 && el.induccion == 0 && el.pagado > 0 && el.unidad_negocio == 2 }).length,
        ventas:    ventas.length,
        marketing: mmkt.length,
        ciclo
      }

      const payload2 = {
        total:     alumnosInduccion.filter( el => { return el.adeudo <= 0 && el.induccion == 0 && el.pagado > 0 && el.unidad_negocio == 1 }).length,
        ventas:    ventas2.length,
        marketing: mmkt2.length,
        ciclo
      }

      respuesta.push( payload )
      respuesta2.push( payload2 )
    }

    res.send({fast: respuesta, inbi:respuesta2 });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.reporteSemanalVentas = async (req, res) => {
  try{


    const fechaEspaniol    = await dashboardKpi.fechaEspaniolsqlERP( ).then( response => response )
    
    // 1-. Sacar la fecha
    const { fechaini, escuela } = req.body

    // 2-. Obtener las fechas obtenidas de la semana
    const fechas = await Kpi.getFechasReporteSemanal( fechaini ).then( response => response )

    const { fecha_inicio, fecha_final } = fechas
    
    const fechasFormatos   = await Kpi.fechasFormatos( fecha_inicio, fecha_final ).then( response => response )
    const { fecha_inicio_format, fecha_final_format } = fechasFormatos

    // 3-. Obtener las fecahs de la semana anterios a la elegida
    const fechasSemanaAnterior   = await Kpi.getFechaAnterior( fecha_inicio ).then( response => response )
    const fechasFormatosAnterior = await Kpi.fechasFormatos( fechasSemanaAnterior.fecha_inicio, fechasSemanaAnterior.fecha_final ).then( response => response )


    // Sacaremos 2 datos, 
    // Vendedoras
    let vendedorasActuales = await personal.getUsuariosVentas( ).then(response => response)


    let iderps = vendedorasActuales.map(( registro ) => { return registro.iderp })

    // Agregarles el nombre
    const usuariosSistema = await entradas.getUsuariosERP( iderps ).then( response => response )

    for(const i in vendedorasActuales){
      const { iderp, nombre_completo } = vendedorasActuales[i]

      
      // Buscar el usuario del ERP 
      const nombre    = usuariosSistema.find(el=> el.id_usuario == iderp)

      // Agregar los usuarios y nombres
      vendedorasActuales[i] = {
        id_usuario      : iderp,
        nombre_completo : nombre  ? nombre.nombre_completo : '',
        escuela         : nombre ? nombre.escuela : '0'
      }
    }

    vendedorasActuales = vendedorasActuales.filter( el => { return el.escuela == escuela && !el.nombre_completo.match("VACANTE")})

    // Planteles
    let plantelesActivos = await entradas.getPlantelesApertura( ).then( response => response) 
    plantelesActivos = plantelesActivos.filter( el => { return el.idunidad_negocio == escuela })

    // Sacamos todas las matriculas nuevas entre esas fechas
    let nuevasMatriculas = await Kpi.getNuevasMatriculas( fecha_inicio, fecha_final ).then( response => response) 
    // FIltamos por escuela
    nuevasMatriculas = nuevasMatriculas.filter( el => { return el.unidad_negocio == escuela })

    // Sacamos todas las matriculas nuevas entre esas fechas
    let nuevasMatriculasSemanaAnterior = await Kpi.getNuevasMatriculas( fechasSemanaAnterior.fecha_inicio, fechasSemanaAnterior.fecha_final ).then( response => response) 
    // FIltamos por escuela
    nuevasMatriculasSemanaAnterior = nuevasMatriculasSemanaAnterior.filter( el => { return el.unidad_negocio == escuela })

    // matriculas anteriores
    let nuevasMatriculasAnteriores = await Kpi.getNuevasMatriculasAnteriores( fecha_inicio, fecha_final ).then( response => response) 
    // FIltamos por escuela
    nuevasMatriculasAnteriores = nuevasMatriculasAnteriores.filter( el => { return el.unidad_negocio == escuela && el.cant_grupos == 0 })

    // Empezamos con el registro
    const encabezados = {
      matriculas_nuevas     : nuevasMatriculas.length,
      matriculas_liquidadas : nuevasMatriculas.filter( el => { return el.adeudo <= 0 }).length,
      matriculas_parciales  : nuevasMatriculas.filter( el => { return el.adeudo > 0 }).length,

      matriculas_liquidadas_ant       : nuevasMatriculasAnteriores.filter( el => { return el.adeudo <= 0 }).length,
      matriculas_liquidadas_ant_monto : nuevasMatriculasAnteriores.filter( el => { return el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),

      matriculas_nuevas_monto     : nuevasMatriculas.map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
      matriculas_liquidadas_monto : nuevasMatriculas.filter( el => { return el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
      matriculas_parciales_monto  : nuevasMatriculas.filter( el => { return el.adeudo > 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
      nuevasMatriculas,

      fecha_inicio_format,
      fecha_final_format,

      fecha_inicio_formatAnterior       : fechasFormatosAnterior.fecha_inicio_format,
      fecha_final_formatAnterior        : fechasFormatosAnterior.fecha_final_format,
      matriculas_nuevasSemAnt           : nuevasMatriculasSemanaAnterior.length,
      matriculas_liquidadasSemAnt       : nuevasMatriculasSemanaAnterior.filter( el => { return el.adeudo <= 0 }).length,
      matriculas_parcialesSemAnt        : nuevasMatriculasSemanaAnterior.filter( el => { return el.adeudo > 0 }).length,
      matriculas_nuevas_montoSemAnt     : nuevasMatriculasSemanaAnterior.map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
      matriculas_liquidadas_montoSemAnt : nuevasMatriculasSemanaAnterior.filter( el => { return el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
      matriculas_parciales_montoSemAnt  : nuevasMatriculasSemanaAnterior.filter( el => { return el.adeudo > 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
    }

    for( const i in plantelesActivos ){

      // Sacar el id del plantel o el plantel
      const { id_plantel, plantel } =  plantelesActivos[i]

      // y ahora si, sacamos el desglose de matriculas
      plantelesActivos[i]['matriculas_nuevas']     = nuevasMatriculas.filter( el => { return el.plantel == plantel }).length
      plantelesActivos[i]['matriculas_liquidadas'] = nuevasMatriculas.filter( el => { return el.plantel == plantel && el.adeudo <= 0 }).length
      plantelesActivos[i]['matriculas_parciales']  = nuevasMatriculas.filter( el => { return el.plantel == plantel && el.adeudo > 0 }).length
      plantelesActivos[i]['nuevasMatriculas']      = nuevasMatriculas.filter( el => { return el.plantel == plantel })
      plantelesActivos[i]['matriculas_ant_liqu']   = nuevasMatriculasAnteriores.filter( el => { return el.plantel == plantel && el.adeudo <= 0 }).length

      plantelesActivos[i]['matriculas_nuevas_monto']     = nuevasMatriculas.filter( el => { return el.plantel == plantel }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
      plantelesActivos[i]['matriculas_liquidadas_monto'] = nuevasMatriculas.filter( el => { return el.plantel == plantel && el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
      plantelesActivos[i]['matriculas_parciales_monto']  = nuevasMatriculas.filter( el => { return el.plantel == plantel && el.adeudo > 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
      plantelesActivos[i]['matriculas_ant_liqu_monto']   = nuevasMatriculasAnteriores.filter( el => { return el.plantel == plantel && el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)

    }

    // AGREGAR LOS TOTALES FINALES
    plantelesActivos.push({
      plantel: 'TOTAL',
      matriculas_nuevas     : plantelesActivos.map(item => item.matriculas_nuevas).reduce((prev, curr) => prev + curr, 0),
      matriculas_liquidadas : plantelesActivos.map(item => item.matriculas_liquidadas).reduce((prev, curr) => prev + curr, 0),
      matriculas_parciales  : plantelesActivos.map(item => item.matriculas_parciales).reduce((prev, curr) => prev + curr, 0),
      matriculas_ant_liqu   : plantelesActivos.map(item => item.matriculas_ant_liqu).reduce((prev, curr) => prev + curr, 0),

      matriculas_nuevas_monto     : plantelesActivos.map(item => item.matriculas_nuevas_monto).reduce((prev, curr) => prev + curr, 0),
      matriculas_liquidadas_monto : plantelesActivos.map(item => item.matriculas_liquidadas_monto).reduce((prev, curr) => prev + curr, 0),
      matriculas_parciales_monto  : plantelesActivos.map(item => item.matriculas_parciales_monto).reduce((prev, curr) => prev + curr, 0),
      matriculas_ant_liqu_monto   : plantelesActivos.map(item => item.matriculas_ant_liqu_monto).reduce((prev, curr) => prev + curr, 0),
      nuevasMatriculas: []
    })


    // Ahora siguen las vendedoras
    for( const i in vendedorasActuales ){

      // Sacar el id del plantel o el plantel
      const { id_usuario } =  vendedorasActuales[i]

      // y ahora si, sacamos el desglose de matriculas
      vendedorasActuales[i]['matriculas_nuevas']     = nuevasMatriculas.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario }).length
      vendedorasActuales[i]['matriculas_liquidadas'] = nuevasMatriculas.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario && el.adeudo <= 0 }).length
      vendedorasActuales[i]['matriculas_parciales']  = nuevasMatriculas.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario && el.adeudo > 0 }).length
      vendedorasActuales[i]['nuevasMatriculas']      = nuevasMatriculas.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario })
      vendedorasActuales[i]['matriculas_ant_liqu']   = nuevasMatriculasAnteriores.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario && el.adeudo <= 0 }).length

      vendedorasActuales[i]['matriculas_nuevas_monto']     = nuevasMatriculas.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
      vendedorasActuales[i]['matriculas_liquidadas_monto'] = nuevasMatriculas.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario && el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
      vendedorasActuales[i]['matriculas_parciales_monto']  = nuevasMatriculas.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario && el.adeudo > 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
      vendedorasActuales[i]['matriculas_ant_liqu_monto']   = nuevasMatriculasAnteriores.filter( el => { return el.id_usuario_ultimo_cambio == id_usuario && el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)

    }

    // comentado por angel  console.log( nuevasMatriculas.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) }))

    // VENDEDORA INACTIVA
    vendedorasActuales.push({
      id_usuario: 0,
      nombre_completo: "INACTIVOS",
      matriculas_nuevas     : nuevasMatriculas.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) }).length,
      matriculas_liquidadas : nuevasMatriculas.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) && el.adeudo <= 0 }).length,
      matriculas_parciales  : nuevasMatriculas.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) && el.adeudo > 0 }).length,
      nuevasMatriculas      : nuevasMatriculas.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) }),
      matriculas_ant_liqu   : nuevasMatriculasAnteriores.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) && el.adeudo <= 0 }).length,

      matriculas_nuevas_monto     : nuevasMatriculas.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
      matriculas_liquidadas_monto : nuevasMatriculas.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) && el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
      matriculas_parciales_monto  : nuevasMatriculas.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) && el.adeudo > 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
      matriculas_ant_liqu_monto   : nuevasMatriculasAnteriores.filter( el => { return !iderps.includes( el.id_usuario_ultimo_cambio ) && el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
    })

    // AGREGAR LOS TOTALES FINALES
    vendedorasActuales.push({
      nombre_completo       : 'TOTAL',
      matriculas_nuevas     : vendedorasActuales.map(item => item.matriculas_nuevas).reduce((prev, curr) => prev + curr, 0),
      matriculas_liquidadas : vendedorasActuales.map(item => item.matriculas_liquidadas).reduce((prev, curr) => prev + curr, 0),
      matriculas_parciales  : vendedorasActuales.map(item => item.matriculas_parciales).reduce((prev, curr) => prev + curr, 0),
      matriculas_ant_liqu   : vendedorasActuales.map(item => item.matriculas_ant_liqu).reduce((prev, curr) => prev + curr, 0),

      matriculas_nuevas_monto     : vendedorasActuales.map(item => item.matriculas_nuevas_monto).reduce((prev, curr) => prev + curr, 0),
      matriculas_liquidadas_monto : vendedorasActuales.map(item => item.matriculas_liquidadas_monto).reduce((prev, curr) => prev + curr, 0),
      matriculas_parciales_monto  : vendedorasActuales.map(item => item.matriculas_parciales_monto).reduce((prev, curr) => prev + curr, 0),
      matriculas_ant_liqu_monto   : vendedorasActuales.map(item => item.matriculas_ant_liqu_monto).reduce((prev, curr) => prev + curr, 0),
      nuevasMatriculas: []
    })


    res.send({ message: 'Ok', fechaini, fecha_inicio, fecha_final, vendedorasActuales, plantelesActivos, nuevasMatriculas, encabezados });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getReporteVendedora = async (req, res) => {
  try{

    const excluir = [501,13,184] 

    // Consultar recepcionistas y encargadas
    let   personal   = await entradas.getPersonalApertura( ).then( response => response )

    personal = personal.filter( el => { return el.idpuesto == 18 && !excluir.includes(el.iderp)  })

    const idPersonal = personal.map((registro) => { return registro.iderp })

    // Consultar los nombres
    let usuariosERP = await entradas.getUsuariosTrabajadores( idPersonal ).then( response => response )

    // Agregar a cada personal su usuario y su id_trabajador
    for( const i in personal ){
      const { iderp } = personal[i]

      const existeUsuario    = usuariosERP.find( el => el.id_usuario == iderp )
    
      personal[i]['nombre_completo'] = existeUsuario    ? existeUsuario.nombre_completo    : null   
      personal[i]['id_plantel']      = existeUsuario    ? existeUsuario.id_plantel         : 0  
      personal[i]['escuela']         = existeUsuario    ? existeUsuario.escuela            : 0  
      personal[i]['id_usuario']      = iderp  
    }

    personal = personal.filter( el => { return el.nombre_completo }).sort((a, b) => a.nombre_completo.localeCompare(b.nombre_completo));

    res.send(personal);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.reporteSemanalVendedora = async (req, res) => {
  try{

    const fechaEspaniol    = await dashboardKpi.fechaEspaniolsqlERP( ).then( response => response )

    // 1-. Sacar la fecha
    const { fechaini, vendedora } = req.body


    // 2-. Obtener las fechas obtenidas de la semana
    const fechas = await Kpi.getFechasReporteSemanal( fechaini ).then( response => response )
    const { fecha_inicio, fecha_final } = fechas

    const fechasFormatos   = await Kpi.fechasFormatos( fecha_inicio, fecha_final ).then( response => response )
    const { fecha_inicio_format, fecha_final_format } = fechasFormatos

    // Sacamos todas las matriculas nuevas entre esas fechas
    let nuevasMatriculas = await Kpi.getNuevasMatriculas( fecha_inicio, fecha_final ).then( response => response) 
    // FIltamos por escuela
    nuevasMatriculas = nuevasMatriculas.filter( el => { return el.id_usuario_ultimo_cambio == vendedora })

    // matriculas anteriores
    let nuevasMatriculasAnteriores = await Kpi.getNuevasMatriculasAnteriores( fecha_inicio, fecha_final ).then( response => response) 
    // FIltamos por escuela
    nuevasMatriculasAnteriores = nuevasMatriculasAnteriores.filter( el => { return el.id_usuario_ultimo_cambio == vendedora && el.cant_grupos == 0 })


    // prospectos
    const prospectosUsuario = await prospectos.prospectosUsuario( vendedora, fecha_inicio, fecha_final ).then( response => response) 

    let prospectosAll                    = prospectosUsuario.length
    let prospectos_vendedora             = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) }).length
    let prospectos_mkt                   = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) }).length
    let prospectos_vendedora_percent     = (( prospectos_vendedora / prospectosAll ) * 100 ).toFixed( 2 )
    let prospectos_mkt_percent           = (( prospectos_mkt / prospectosAll ) * 100 ).toFixed( 2 )
    
    let prospectosv_seguimiento          = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.finalizo == 0 }).length
    let prospectosm_seguimiento          = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.finalizo == 0 }).length
    let prospectosv_seguimiento_percent  = (( prospectosv_seguimiento / prospectosAll ) * 100 ).toFixed( 2 )
    let prospectosm_seguimiento_percent  = (( prospectosm_seguimiento / prospectosAll ) * 100 ).toFixed( 2 )

    let prospectosv_seguimiento_buenos   = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.clasificacion == 1 && el.finalizo == 0 }).length
    let prospectosv_seguimiento_malos    = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.clasificacion == 2 && el.finalizo == 0 }).length
    let prospectosv_seguimiento_ambos    = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.clasificacion == 0 && el.finalizo == 0 }).length
    let prospectosm_seguimiento_buenos   = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.clasificacion == 1 && el.finalizo == 0 }).length
    let prospectosm_seguimiento_malos    = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.clasificacion == 2 && el.finalizo == 0 }).length
    let prospectosm_seguimiento_ambos    = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.clasificacion == 0 && el.finalizo == 0 }).length

    let prospectosv_cerrados_buenos      = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.clasificacion == 1 && el.finalizo == 1 && el.idalumno == 0 }).length
    let prospectosv_cerrados_malos       = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.clasificacion == 2 && el.finalizo == 1 && el.idalumno == 0 }).length
    let prospectosv_cerrados_ambos       = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.clasificacion == 0 && el.finalizo == 1 && el.idalumno == 0 }).length
    let prospectosm_cerrados_buenos      = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.clasificacion == 1 && el.finalizo == 1 && el.idalumno == 0 }).length
    let prospectosm_cerrados_malos       = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.clasificacion == 2 && el.finalizo == 1 && el.idalumno == 0 }).length
    let prospectosm_cerrados_ambos       = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.clasificacion == 0 && el.finalizo == 1 && el.idalumno == 0 }).length

    let prospectosv_cerrados_no_exito          = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.finalizo == 1 && el.idalumno == 0 }).length
    let prospectosv_cerrados_no_exito_percent  = (( prospectosv_cerrados_no_exito / prospectosAll ) * 100 ).toFixed( 2 )
    let prospectosm_cerrados_no_exito          = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.finalizo == 1 && el.idalumno == 0 }).length
    let prospectosm_cerrados_no_exito_percent  = (( prospectosm_cerrados_no_exito / prospectosAll ) * 100 ).toFixed( 2 )

    let prospectosv_cerrados_si_exito          = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.finalizo == 1 && el.idalumno > 0 }).length
    let prospectosv_cerrados_si_exito_percent  = (( prospectosv_cerrados_si_exito / prospectosAll ) * 100 ).toFixed( 2 )
    let prospectosm_cerrados_si_exito          = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.finalizo == 1 && el.idalumno > 0 }).length
    let prospectosm_cerrados_si_exito_percent  = (( prospectosm_cerrados_si_exito / prospectosAll ) * 100 ).toFixed( 2 )

    // SACAR LAS MATRICULAS QUE SE INSCRIBIERON PERO NO ESTÁN EN ESTOS PROSPECTOS

    let idsV = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.finalizo == 1 && el.idalumno > 0 }).map((registro)=>{ return registro.idalumno })
    let idsM = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.finalizo == 1 && el.idalumno > 0 }).map((registro)=>{ return registro.idalumno })

    let otrasMatriculas =  nuevasMatriculas.filter( el => { return !idsV.includes( el.id_alumno ) && !idsM.includes( el.id_alumno ) }).map(( registro ) => { return registro.id_alumno })
    otrasMatriculas = otrasMatriculas.length ? otrasMatriculas : [0]

    //comentado por angel console.log( idsM, otrasMatriculas )

    // Hay que buscar esas matriculas en prospectos
    let prospectosAlumnos = await prospectos.prospectosAlumnos( otrasMatriculas ).then( response => response )

    let ventasExtrasV = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.finalizo == 1 && el.idalumno == 0 }).length
    let ventasExtrasM = otrasMatriculas.length - ventasExtrasV

    // comentado por angel console.log( ventasExtrasV, ventasExtrasM )


    // Empezamos con el registro
    const encabezados = {
      matriculas_nuevas     : nuevasMatriculas.length,
      matriculas_liquidadas : nuevasMatriculas.filter( el => { return el.adeudo <= 0 }).length,
      matriculas_parciales  : nuevasMatriculas.filter( el => { return el.adeudo > 0 }).length,

      matriculas_liquidadas_ant       : nuevasMatriculasAnteriores.filter( el => { return el.adeudo <= 0 }).length,
      matriculas_liquidadas_ant_monto : nuevasMatriculasAnteriores.filter( el => { return el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),

      matriculas_nuevas_monto     : nuevasMatriculas.map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
      matriculas_liquidadas_monto : nuevasMatriculas.filter( el => { return el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
      matriculas_parciales_monto  : nuevasMatriculas.filter( el => { return el.adeudo > 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
      nuevasMatriculas,

      prospectosAll,
      prospectos_vendedora,
      prospectos_vendedora_percent,
      prospectos_mkt,
      prospectos_mkt_percent,
      prospectosv_cerrados_no_exito,
      prospectosv_cerrados_no_exito_percent,
      prospectosm_cerrados_no_exito,
      prospectosm_cerrados_no_exito_percent,
      prospectosv_cerrados_si_exito,
      prospectosv_cerrados_si_exito_percent,
      prospectosm_cerrados_si_exito,
      prospectosm_cerrados_si_exito_percent,

      prospectosv_seguimiento,
      prospectosm_seguimiento,
      prospectosv_seguimiento_percent,
      prospectosm_seguimiento_percent,

      prospectosv_seguimiento_buenos,
      prospectosv_seguimiento_malos,
      prospectosv_seguimiento_ambos,
      prospectosm_seguimiento_buenos,
      prospectosm_seguimiento_malos,
      prospectosm_seguimiento_ambos,

      prospectosv_cerrados_buenos,
      prospectosv_cerrados_malos,
      prospectosv_cerrados_ambos,
      prospectosm_cerrados_buenos,
      prospectosm_cerrados_malos,
      prospectosm_cerrados_ambos,
      
      fecha_inicio, 
      fecha_final,
      fecha_inicio_format,
      fecha_final_format
    }


    res.send({ encabezados });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.reporteSemanalVendedoraZip = async (req, res) => {
  try{

    const fechaEspaniol    = await dashboardKpi.fechaEspaniolsqlERP( ).then( response => response )

    // 1-. Sacar la fecha
    const { fechaini, vendedoras, infoRI } = req.body


    // 2-. Obtener las fechas obtenidas de la semana
    const fechas = await Kpi.getFechasReporteSemanal( fechaini ).then( response => response )
    const { fecha_inicio, fecha_final } = fechas

    const fechasFormatos   = await Kpi.fechasFormatos( fecha_inicio, fecha_final ).then( response => response )
    const { fecha_inicio_format, fecha_final_format } = fechasFormatos

    // Sacamos todas las matriculas nuevas entre esas fechas
    let nuevasMatriculas = await Kpi.getNuevasMatriculas( fecha_inicio, fecha_final ).then( response => response) 

    let dato1 = []

    riPorciento = ""
    reinscribibles = ""
    acumulado = ""

    for( const i in vendedoras ){

      const { iderp, nombre_completo, id_plantel } = vendedoras[i]
      
      dato1 = infoRI.find( el=> el.id_plantel == id_plantel )

      riPorciento = dato1.porcentajeAvance
      reinscribibles = dato1.totalAlumnosAnteriores
      acumulado = dato1.totalAlumnosSiguientes
      

      // FIltamos por escuela
      let nuevasMatriculas2 = nuevasMatriculas.filter( el => { return el.id_usuario_ultimo_cambio == iderp })

      // matriculas anteriores
      let nuevasMatriculasAnteriores = await Kpi.getNuevasMatriculasAnteriores( fecha_inicio, fecha_final ).then( response => response) 
      // FIltamos por escuela
      nuevasMatriculasAnteriores = nuevasMatriculasAnteriores.filter( el => { return el.id_usuario_ultimo_cambio == iderp && el.cant_grupos == 0 })


      // prospectos
      const prospectosUsuario = await prospectos.prospectosUsuario( iderp, fecha_inicio, fecha_final ).then( response => response) 


      let prospectosAll                    = prospectosUsuario.length
      let prospectos_vendedora             = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) }).length
      let prospectos_mkt                   = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) }).length
      let prospectos_vendedora_percent     = prospectos_vendedora ? (( prospectos_vendedora / prospectosAll ) * 100 ).toFixed( 2 ):0
      let prospectos_mkt_percent           = prospectos_mkt ? (( prospectos_mkt / prospectosAll ) * 100 ).toFixed( 2 ):0
      
      let prospectosv_seguimiento          = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.finalizo == 0 }).length
      let prospectosm_seguimiento          = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.finalizo == 0 }).length
      let prospectosv_seguimiento_percent  = prospectosv_seguimiento ? (( prospectosv_seguimiento / prospectosAll ) * 100 ).toFixed( 2 ):0
      let prospectosm_seguimiento_percent  = prospectosm_seguimiento ? (( prospectosm_seguimiento / prospectosAll ) * 100 ).toFixed( 2 ):0

      let prospectosv_seguimiento_buenos   = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.clasificacion == 1 && el.finalizo == 0 }).length
      let prospectosv_seguimiento_malos    = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.clasificacion == 2 && el.finalizo == 0 }).length
      let prospectosv_seguimiento_ambos    = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.clasificacion == 0 && el.finalizo == 0 }).length
      let prospectosm_seguimiento_buenos   = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.clasificacion == 1 && el.finalizo == 0 }).length
      let prospectosm_seguimiento_malos    = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.clasificacion == 2 && el.finalizo == 0 }).length
      let prospectosm_seguimiento_ambos    = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.clasificacion == 0 && el.finalizo == 0 }).length

      let prospectosv_cerrados_buenos      = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.clasificacion == 1 && el.finalizo == 1 && el.idalumno == 0 }).length
      let prospectosv_cerrados_malos       = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.clasificacion == 2 && el.finalizo == 1 && el.idalumno == 0 }).length
      let prospectosv_cerrados_ambos       = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.clasificacion == 0 && el.finalizo == 1 && el.idalumno == 0 }).length
      let prospectosm_cerrados_buenos      = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.clasificacion == 1 && el.finalizo == 1 && el.idalumno == 0 }).length
      let prospectosm_cerrados_malos       = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.clasificacion == 2 && el.finalizo == 1 && el.idalumno == 0 }).length
      let prospectosm_cerrados_ambos       = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.clasificacion == 0 && el.finalizo == 1 && el.idalumno == 0 }).length

      let prospectosv_cerrados_no_exito          = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.finalizo == 1 && el.idalumno == 0 }).length
      let prospectosv_cerrados_no_exito_percent  = prospectosv_cerrados_no_exito ? (( prospectosv_cerrados_no_exito / prospectosAll ) * 100 ).toFixed( 2 ):0
      let prospectosm_cerrados_no_exito          = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.finalizo == 1 && el.idalumno == 0 }).length
      let prospectosm_cerrados_no_exito_percent  = prospectosm_cerrados_no_exito ? (( prospectosm_cerrados_no_exito / prospectosAll ) * 100 ).toFixed( 2 ):0

      let prospectosv_cerrados_si_exito          = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.finalizo == 1 && el.idalumno > 0 }).length
      let prospectosv_cerrados_si_exito_percent  = prospectosv_cerrados_si_exito ? (( prospectosv_cerrados_si_exito / prospectosAll ) * 100 ).toFixed( 2 ):0
      let prospectosm_cerrados_si_exito          = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.finalizo == 1 && el.idalumno > 0 }).length
      let prospectosm_cerrados_si_exito_percent  = prospectosm_cerrados_si_exito ? (( prospectosm_cerrados_si_exito / prospectosAll ) * 100 ).toFixed( 2 ):0

      // SACAR LAS MATRICULAS QUE SE INSCRIBIERON PERO NO ESTÁN EN ESTOS PROSPECTOS

      let idsV = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.finalizo == 1 && el.idalumno > 0 }).map((registro)=>{ return registro.idalumno })
      let idsM = prospectosUsuario.filter( el => { return ![18,19].includes( el.idpuesto ) && el.finalizo == 1 && el.idalumno > 0 }).map((registro)=>{ return registro.idalumno })

      let otrasMatriculas =  nuevasMatriculas2.filter( el => { return !idsV.includes( el.id_alumno ) && !idsM.includes( el.id_alumno ) }).map(( registro ) => { return registro.id_alumno })
      otrasMatriculas = otrasMatriculas.length ? otrasMatriculas : [0]

      // Hay que buscar esas matriculas en prospectos
      let prospectosAlumnos = await prospectos.prospectosAlumnos( otrasMatriculas ).then( response => response )

      let ventasExtrasV = prospectosUsuario.filter( el => { return [18,19].includes( el.idpuesto ) && el.finalizo == 1 && el.idalumno == 0 }).length
      let ventasExtrasM = otrasMatriculas.length - ventasExtrasV

      newReporteVentas({
        fechaini,
        nombre_completo,
        fecha_inicio_format,
        fecha_final_format,
        matriculas_nuevas     : nuevasMatriculas2.length,
        matriculas_liquidadas : nuevasMatriculas2.filter( el => { return el.adeudo <= 0 }).length,
        matriculas_parciales  : nuevasMatriculas2.filter( el => { return el.adeudo > 0 }).length,
        matriculas_nuevas_monto         : nuevasMatriculas2.map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('es-MX', { style: 'currency', currency: 'MXN' }),
        matriculas_liquidadas_monto     : nuevasMatriculas2.filter( el => { return el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('es-MX', { style: 'currency', currency: 'MXN' }),
        matriculas_parciales_monto      : nuevasMatriculas2.filter( el => { return el.adeudo > 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('es-MX', { style: 'currency', currency: 'MXN' }),
        matriculas_liquidadas_ant       : nuevasMatriculasAnteriores.filter( el => { return el.adeudo <= 0 }).length,
        matriculas_liquidadas_ant_monto : nuevasMatriculasAnteriores.filter( el => { return el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('es-MX', { style: 'currency', currency: 'MXN' }),
        prospectosAll,
        prospectos_vendedora,
        prospectos_vendedora_percent,
        prospectos_mkt,
        prospectos_mkt_percent,
        prospectosv_cerrados_no_exito,
        prospectosv_cerrados_no_exito_percent,
        prospectosm_cerrados_no_exito,
        prospectosm_cerrados_no_exito_percent,
        prospectosv_cerrados_si_exito,
        prospectosv_cerrados_si_exito_percent,
        prospectosm_cerrados_si_exito,
        prospectosm_cerrados_si_exito_percent,

        prospectosv_seguimiento,
        prospectosm_seguimiento,
        prospectosv_seguimiento_percent,
        prospectosm_seguimiento_percent,

        prospectosv_seguimiento_buenos,
        prospectosv_seguimiento_malos,
        prospectosv_seguimiento_ambos,
        prospectosm_seguimiento_buenos,
        prospectosm_seguimiento_malos,
        prospectosm_seguimiento_ambos,

        prospectosv_cerrados_buenos,
        prospectosv_cerrados_malos,
        prospectosv_cerrados_ambos,
        prospectosm_cerrados_buenos,
        prospectosm_cerrados_malos,
        prospectosm_cerrados_ambos,

        riPorciento,  //Es el %RI para imprimir en el pdf Angel Rodriguez
        reinscribibles,
        acumulado,
      })
      
      // Empezamos con el registro
      const encabezados = {
        matriculas_nuevas     : nuevasMatriculas2.length,
        matriculas_liquidadas : nuevasMatriculas2.filter( el => { return el.adeudo <= 0 }).length,
        matriculas_parciales  : nuevasMatriculas2.filter( el => { return el.adeudo > 0 }).length,

        matriculas_liquidadas_ant       : nuevasMatriculasAnteriores.filter( el => { return el.adeudo <= 0 }).length,
        matriculas_liquidadas_ant_monto : nuevasMatriculasAnteriores.filter( el => { return el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),

        matriculas_nuevas_monto     : nuevasMatriculas2.map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
        matriculas_liquidadas_monto : nuevasMatriculas2.filter( el => { return el.adeudo <= 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
        matriculas_parciales_monto  : nuevasMatriculas2.filter( el => { return el.adeudo > 0 }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0),
        nuevasMatriculas: nuevasMatriculas2,

        prospectosAll,
        prospectos_vendedora,
        prospectos_vendedora_percent,
        prospectos_mkt,
        prospectos_mkt_percent,
        prospectosv_cerrados_no_exito,
        prospectosv_cerrados_no_exito_percent,
        prospectosm_cerrados_no_exito,
        prospectosm_cerrados_no_exito_percent,
        prospectosv_cerrados_si_exito,
        prospectosv_cerrados_si_exito_percent,
        prospectosm_cerrados_si_exito,
        prospectosm_cerrados_si_exito_percent,

        prospectosv_seguimiento,
        prospectosm_seguimiento,
        prospectosv_seguimiento_percent,
        prospectosm_seguimiento_percent,

        prospectosv_seguimiento_buenos,
        prospectosv_seguimiento_malos,
        prospectosv_seguimiento_ambos,
        prospectosm_seguimiento_buenos,
        prospectosm_seguimiento_malos,
        prospectosm_seguimiento_ambos,

        prospectosv_cerrados_buenos,
        prospectosv_cerrados_malos,
        prospectosv_cerrados_ambos,
        prospectosm_cerrados_buenos,
        prospectosm_cerrados_malos,
        prospectosm_cerrados_ambos,
        
        fecha_inicio, 
        fecha_final,
        fecha_inicio_format,
        fecha_final_format
      }
    }

    setTimeout(() => {
      // Aquí puedes colocar el código que deseas ejecutar después de la espera de 5 segundos
      // Guardar todo en un ZIP
      const zip = new AdmZip();

      for( const i in vendedoras ){

        const { iderp, nombre_completo } = vendedoras[i]

        // Agregar un archivo a la compresión
        zip.addLocalFile(`../../recibos-pagos/${nombre_completo}-${fechaini}.pdf`);
        // Puedes agregar más archivos si es necesario
        // Guardar el archivo ZIP en disco
        zip.writeZip(`../../recibos-pagos/${fechaini}.zip`);
      }


      res.send({ message: 'Datos generados' });
    }, 5000);



  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getAniosCiclos = async (req, res) => {
  try{

    // 1-. Consultar todos los ciclos
    const ciclosAll = await Kpi.getCiclosAll( ).then( response => response )

    // 2-. Separar solo los años para hacer un mejor filtro de todo
    let anios = ciclosAll.filter((ciclo, index, self) =>
      index === self.findIndex((p) => (
        p.anio === ciclo.anio
      ))
    );

    // Y ahora lo limpimos
    anios = anios.map(ciclo => ( ciclo.anio ));

    res.send({ ciclosAll, anios });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getHistorialVentas = async (req, res) => {
  try{

    // 1-. Sacar el tipo de venta
    const { ventas } = req.body

    // 1-. Consultar todos los ciclos despues del seleccionado
    const ciclosFinales = await Kpi.getCiclosFecha( req.body.fecha_inicio_ciclo ).then( response => response )

    // 2-. Sacamos el ciclo final solo para poder mostrarlo en el FRONT y listo 
    let cicloFinal = ciclosFinales[ ciclosFinales.length - 1 ]

    // HEADERS
    let headers = [
      { text: 'Vendedora' , value: 'vendedora' }
    ]

    for( const i in ciclosFinales ){

      let ciclo = ciclosFinales[i].ciclo.replace(' ','_')
      headers.push({ text: ciclosFinales[i].ciclo, value: 'cantidad_'+ciclo, align: 'right' })
    }

    // Agregamos el total
    headers.push({ text: 'Total' , value: 'total' })

    let vendedorasInbiAll = []
    let vendedorasFastAll = []


    for( const i in ciclosFinales ){

      const { fecha_inicio_ciclo, ciclo, id_ciclo_relacionado, id_ciclo } = ciclosFinales[i]

      // Sacamos un ciclo anterior para poder sacar a los de inducción
      const cicloAnterior = await Kpi.getCicloAnterior( fecha_inicio_ciclo, ciclo ).then( response => response )
      if( !cicloAnterior ){ return res.status( 500 ).send({ message: 'Ciclo anterior no existe' }) }

      // Desestructuramos el ciclo anterior para sacar a los de inducción
      const { nombreCicloAnterior , nombreCicloActual } = cicloAnterior

      // Sacamos a los alumnos de inducción
      let alumnosInduccion = await Kpi.getAlumnosInducción( nombreCicloAnterior, nombreCicloActual ).then(response=> response)

      // SACAR A LOS ALUMNOS UNICOS
      let idAlumnos = alumnosInduccion.map((registro)=> { return registro.id_alumno })

      // Sacar a los de inducción pero ya con su grupo si es que tienen 
      idAlumnos = idAlumnos.length ? idAlumnos : [0]
      let alumnosInduccionConGrupo = await Kpi.getAlumnosGrupoNormal( idAlumnos ).then(response=> response)

      // Recorremos a los alumnos de inducción
      for( const i in alumnosInduccion ){
        const { id_alumno, adeudo, grupo, pagado, activo_sn } = alumnosInduccion[i]

        // Verificar si tiene un grupo normal estos alumnos
        const existeGrupoNormal = alumnosInduccionConGrupo.find( el => el.id_alumno == id_alumno )

        alumnosInduccion[i]['induccion'] = existeGrupoNormal ? 0 : 1
        alumnosInduccion[i]['adeudo']    = existeGrupoNormal ? existeGrupoNormal.adeudo    : adeudo
        alumnosInduccion[i]['pagado']    = existeGrupoNormal ? existeGrupoNormal.pagado    : pagado
        alumnosInduccion[i]['activo_sn'] = existeGrupoNormal ? existeGrupoNormal.activo_sn : activo_sn
      }

      const alumnosNICicloFast = await Kpi.getAlumnosNI( id_ciclo_relacionado, idAlumnos ).then(response=> response)
      const alumnosNICicloInbi = await Kpi.getAlumnosNI( id_ciclo, idAlumnos ).then(response=> response)

      alumnosInduccion         = alumnosInduccion.concat( alumnosNICicloFast ).concat( alumnosNICicloInbi )

      const matriculasFast     = alumnosInduccion.filter( el => { return el.unidad_negocio == 2 })
      const matriculasInbi     = alumnosInduccion.filter( el => { return el.unidad_negocio == 1 })


      // Vendedoras inbi, sin duplicados
      const vendedorasInbi = matriculasInbi.filter((persona, index, self) =>
        index === self.findIndex((p) => (
          p.vendedora === persona.vendedora
        ))
      ).map(persona => ({ id_usuario: persona.id_usuario_ultimo_cambio, vendedora: persona.vendedora, ciclo, activo_sn: persona.activo_sn, pagado: persona.pagado }));

      // Vendedoras fast, sin duplicados
      const vendedorasFast = matriculasFast.filter((persona, index, self) =>
        index === self.findIndex((p) => (
          p.vendedora === persona.vendedora
        ))
      ).map(persona => ({ id_usuario: persona.id_usuario_ultimo_cambio, vendedora: persona.vendedora, ciclo, activo_sn: persona.activo_sn, pagado: persona.pagado }));

      vendedorasInbiAll = vendedorasInbiAll.concat( vendedorasInbi )
      vendedorasFastAll = vendedorasFastAll.concat( vendedorasFast )

      ciclosFinales[i]['datos_fast'] = matriculasFast.filter( el => { return el.adeudo <= 0 && el.induccion == 0 })
      ciclosFinales[i]['datos_inbi'] = matriculasInbi.filter( el => { return el.adeudo <= 0 && el.induccion == 0 })
    }

    // Sacamos los duplicados de las vendedoras de FAST e INBI
    vendedorasInbiAll = vendedorasInbiAll.filter((persona, index, self) =>
      index === self.findIndex((p) => (
        p.vendedora === persona.vendedora
      ))
    )

    vendedorasFastAll = vendedorasFastAll.filter((persona, index, self) =>
      index === self.findIndex((p) => (
        p.vendedora === persona.vendedora
      ))
    )

    // Ordenar los nombres
    vendedorasInbiAll = vendedorasInbiAll.sort((a, b) => a.vendedora.localeCompare(b.vendedora));
    vendedorasFastAll = vendedorasFastAll.sort((a, b) => a.vendedora.localeCompare(b.vendedora));

    vendedorasInbiAll.push({ id_usuario: 1000, vendedora: 'TOTAL', ciclo: 'Sin ciclo', align: 'right' })
    vendedorasFastAll.push({ id_usuario: 1000, vendedora: 'TOTAL', ciclo: 'Sin ciclo', align: 'right' })

    let totalFinal  = 0

    // Llenemos ahora el headers con la siguiente información
    for( const i in vendedorasInbiAll ){

      // Desestrucutramos la vendedora
      const{ vendedora } = vendedorasInbiAll[i]

      let total  = 0

      // Ahora, tenemos que recorrer los ciclossss
      for( const j in ciclosFinales ){

        let ciclo = 'cantidad_' + ciclosFinales[j].ciclo.replace(' ','_')

        if( ventas == 'Alumnos' )
          total  += ciclosFinales[j].datos_inbi.filter( el => { return el.vendedora == vendedora }).length
        else
          total  += ciclosFinales[j].datos_inbi.filter( el => { return el.vendedora == vendedora }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
        
        let totalVentas = 0

        if( ventas == 'Alumnos' )
          totalVentas = vendedora == 'TOTAL' ? ciclosFinales[j].datos_inbi.length : ciclosFinales[j].datos_inbi.filter( el => { return el.vendedora == vendedora }).length
        else
          totalVentas = vendedora == 'TOTAL' ? ciclosFinales[j].datos_inbi.map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options) : ciclosFinales[j].datos_inbi.filter( el => { return el.vendedora == vendedora }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options)

        vendedorasInbiAll[i][ciclo] = totalVentas

      }

      totalFinal  += total

      if( ventas == 'Alumnos' )
        vendedorasInbiAll[i]['total']  = vendedora == 'TOTAL' ? totalFinal  : total
      else
        vendedorasInbiAll[i]['total']  = vendedora == 'TOTAL' ? totalFinal.toLocaleString('en-US', options)  : total.toLocaleString('en-US', options)

    }


    let totalFinalFast  = 0

    // Llenemos ahora el headers con la siguiente información
    for( const i in vendedorasFastAll ){

      // Desestrucutramos la vendedora
      const{ vendedora } = vendedorasFastAll[i]

      let total  = 0

      // Ahora, tenemos que recorrer los ciclossss
      for( const j in ciclosFinales ){

        let ciclo = 'cantidad_' + ciclosFinales[j].ciclo.replace(' ','_')

        if( ventas == 'Alumnos' )
          total  += ciclosFinales[j].datos_fast.filter( el => { return el.vendedora == vendedora }).length
        else
          total  += ciclosFinales[j].datos_fast.filter( el => { return el.vendedora == vendedora }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
        
        let totalVentas = 0

        if( ventas == 'Alumnos' )
          totalVentas = vendedora == 'TOTAL' ? ciclosFinales[j].datos_fast.length : ciclosFinales[j].datos_fast.filter( el => { return el.vendedora == vendedora }).length
        else
          totalVentas = vendedora == 'TOTAL' ? ciclosFinales[j].datos_fast.map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options) : ciclosFinales[j].datos_fast.filter( el => { return el.vendedora == vendedora }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options)

        vendedorasFastAll[i][ciclo] = totalVentas

      }

      totalFinalFast  += total

      if( ventas == 'Alumnos' )
        vendedorasFastAll[i]['total']  = vendedora == 'TOTAL' ? totalFinalFast  : total
      else
        vendedorasFastAll[i]['total']  = vendedora == 'TOTAL' ? totalFinalFast.toLocaleString('en-US', options)  : total.toLocaleString('en-US', options)

    }

    const vendedorasAll = vendedorasInbiAll.concat( vendedorasFastAll )
    res.send({ ciclosFinales, cicloFinal, headers, vendedorasInbiAll, vendedorasFastAll, vendedorasAll });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getHistorialVentasPlantel = async (req, res) => {
  try{

    // 1-. Sacar el tipo de venta
    const { ventas } = req.body

    // 1-. Consultar todos los ciclos despues del seleccionado
    const ciclosFinales = await Kpi.getCiclosFecha( req.body.fecha_inicio_ciclo ).then( response => response )

    // 2-. Sacamos el ciclo final solo para poder mostrarlo en el FRONT y listo 
    let cicloFinal = ciclosFinales[ ciclosFinales.length - 1 ]

    // HEADERS
    let headers = [
      { text: 'Plantel' , value: 'plantel' }
    ]

    for( const i in ciclosFinales ){
      let ciclo = ciclosFinales[i].ciclo.replace(' ','_')
      headers.push({ text: ciclosFinales[i].ciclo, value: 'cantidad_'+ciclo })
    }

    // Agregamos el total
    headers.push({ text: 'Total', value: 'total' })

    let vendedorasInbiAll = []
    let vendedorasFastAll = []


    for( const i in ciclosFinales ){

      const { fecha_inicio_ciclo, ciclo, id_ciclo_relacionado, id_ciclo } = ciclosFinales[i]

      // Sacamos un ciclo anterior para poder sacar a los de inducción
      const cicloAnterior = await Kpi.getCicloAnterior( fecha_inicio_ciclo, ciclo ).then( response => response )
      if( !cicloAnterior ){ return res.status( 500 ).send({ message: 'Ciclo anterior no existe' }) }

      // Desestructuramos el ciclo anterior para sacar a los de inducción
      const { nombreCicloAnterior , nombreCicloActual } = cicloAnterior

      // Sacamos a los alumnos de inducción
      let alumnosInduccion = await Kpi.getAlumnosInducción( nombreCicloAnterior, nombreCicloActual ).then(response=> response)

      // SACAR A LOS ALUMNOS UNICOS
      let idAlumnos = alumnosInduccion.map((registro)=> { return registro.id_alumno })

      // Sacar a los de inducción pero ya con su grupo si es que tienen 
      idAlumnos = idAlumnos.length ? idAlumnos : [0]
      let alumnosInduccionConGrupo = await Kpi.getAlumnosGrupoNormal( idAlumnos ).then(response=> response)

      // Recorremos a los alumnos de inducción
      for( const i in alumnosInduccion ){
        const { id_alumno, adeudo, grupo, pagado, activo_sn } = alumnosInduccion[i]

        // Verificar si tiene un grupo normal estos alumnos
        const existeGrupoNormal = alumnosInduccionConGrupo.find( el => el.id_alumno == id_alumno )

        alumnosInduccion[i]['induccion'] = existeGrupoNormal ? 0 : 1
        alumnosInduccion[i]['adeudo']    = existeGrupoNormal ? existeGrupoNormal.adeudo : adeudo
        alumnosInduccion[i]['pagado']    = existeGrupoNormal ? existeGrupoNormal.pagado : pagado
        alumnosInduccion[i]['activo_sn'] = existeGrupoNormal ? existeGrupoNormal.activo_sn : activo_sn
      }

      const alumnosNICicloFast = await Kpi.getAlumnosNI( id_ciclo_relacionado, idAlumnos ).then(response=> response)
      const alumnosNICicloInbi = await Kpi.getAlumnosNI( id_ciclo, idAlumnos ).then(response=> response)

      alumnosInduccion         = alumnosInduccion.concat( alumnosNICicloFast ).concat( alumnosNICicloInbi )

      const matriculasFast     = alumnosInduccion.filter( el => { return el.unidad_negocio == 2 })
      const matriculasInbi     = alumnosInduccion.filter( el => { return el.unidad_negocio == 1 })

      // Vendedoras inbi, sin duplicados
      const vendedorasInbi = matriculasInbi.filter((plantel2, index, self) =>
        index === self.findIndex((p) => (
          p.plantel === plantel2.plantel
        ))
      ).map(plantel2 => ({ plantel: plantel2.plantel, ciclo, activo_sn: 1, pagado: plantel2.pagado }));

      // Vendedoras fast, sin duplicados
      const vendedorasFast = matriculasFast.filter((plantel2, index, self) =>
        index === self.findIndex((p) => (
          p.plantel === plantel2.plantel
        ))
      ).map(plantel2 => ({ plantel: plantel2.plantel, ciclo, activo_sn: 1, pagado: plantel2.pagado }));

      vendedorasInbiAll = vendedorasInbiAll.concat( vendedorasInbi )
      vendedorasFastAll = vendedorasFastAll.concat( vendedorasFast )

      ciclosFinales[i]['datos_fast'] = matriculasFast.filter( el => { return el.adeudo <= 0 && el.induccion == 0 })
      ciclosFinales[i]['datos_inbi'] = matriculasInbi.filter( el => { return el.adeudo <= 0 && el.induccion == 0 })
    }

    // Sacamos los duplicados de las vendedoras de FAST e INBI
    vendedorasInbiAll = vendedorasInbiAll.filter((plantel2, index, self) =>
      index === self.findIndex((p) => (
        p.plantel === plantel2.plantel
      ))
    )

    vendedorasFastAll = vendedorasFastAll.filter((plantel2, index, self) =>
      index === self.findIndex((p) => (
        p.plantel === plantel2.plantel
      ))
    )

    // Ordenar los nombres
    vendedorasInbiAll = vendedorasInbiAll.sort((a, b) => a.plantel.localeCompare(b.plantel));
    vendedorasFastAll = vendedorasFastAll.sort((a, b) => a.plantel.localeCompare(b.plantel));

    vendedorasInbiAll.push({ id_plantel: 1000, plantel: 'TOTAL', ciclo: 'Sin ciclo'})
    vendedorasFastAll.push({ id_plantel: 1000, plantel: 'TOTAL', ciclo: 'Sin ciclo'})

    let totalFinal  = 0
    let pagadoTotal = 0

    // Llenemos ahora el headers con la siguiente información
    for( const i in vendedorasInbiAll ){

      // Desestrucutramos la vendedora
      const{ plantel } = vendedorasInbiAll[i]

      let total  = 0
      let pagado = 0

      // Ahora, tenemos que recorrer los ciclossss
      for( const j in ciclosFinales ){

        let ciclo = 'cantidad_' + ciclosFinales[j].ciclo.replace(' ','_')

        if( ventas == 'Alumnos' )
          total  += ciclosFinales[j].datos_inbi.filter( el => { return el.plantel == plantel }).length
        else
          total  += ciclosFinales[j].datos_inbi.filter( el => { return el.plantel == plantel }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
        
        let totalVentas = 0

        if( ventas == 'Alumnos' )
          totalVentas = plantel == 'TOTAL' ? ciclosFinales[j].datos_inbi.length : ciclosFinales[j].datos_inbi.filter( el => { return el.plantel == plantel }).length
        else
          totalVentas = plantel == 'TOTAL' ? ciclosFinales[j].datos_inbi.map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options) : ciclosFinales[j].datos_inbi.filter( el => { return el.plantel == plantel }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options)

        vendedorasInbiAll[i][ciclo] = totalVentas

      }

      totalFinal  += total

      if( ventas == 'Alumnos' )
        vendedorasInbiAll[i]['total']  = plantel == 'TOTAL' ? totalFinal  : total
      else
        vendedorasInbiAll[i]['total']  = plantel == 'TOTAL' ? totalFinal.toLocaleString('en-US', options)  : total.toLocaleString('en-US', options)

    }


    let totalFinalFast  = 0

    // Llenemos ahora el headers con la siguiente información
    for( const i in vendedorasFastAll ){

      // Desestrucutramos la vendedora
      const{ plantel } = vendedorasFastAll[i]

      let total  = 0

      // Ahora, tenemos que recorrer los ciclossss
      for( const j in ciclosFinales ){

        let ciclo = 'cantidad_' + ciclosFinales[j].ciclo.replace(' ','_')

        if( ventas == 'Alumnos' )
          total  += ciclosFinales[j].datos_fast.filter( el => { return el.plantel == plantel }).length
        else
          total  += ciclosFinales[j].datos_fast.filter( el => { return el.plantel == plantel }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
        
        let totalVentas = 0

        if( ventas == 'Alumnos' )
          totalVentas = plantel == 'TOTAL' ? ciclosFinales[j].datos_fast.length : ciclosFinales[j].datos_fast.filter( el => { return el.plantel == plantel }).length
        else
          totalVentas = plantel == 'TOTAL' ? ciclosFinales[j].datos_fast.map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options) : ciclosFinales[j].datos_fast.filter( el => { return el.plantel == plantel }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options)

        vendedorasFastAll[i][ciclo] = totalVentas

      }

      totalFinalFast  += total

      if( ventas == 'Alumnos' )
        vendedorasFastAll[i]['total']  = plantel == 'TOTAL' ? totalFinalFast  : total
      else
        vendedorasFastAll[i]['total']  = plantel == 'TOTAL' ? totalFinalFast.toLocaleString('en-US', options)  : total.toLocaleString('en-US', options)
    }

    const vendedorasAll = vendedorasInbiAll.concat( vendedorasFastAll )
    res.send({ ciclosFinales, cicloFinal, headers, vendedorasInbiAll, vendedorasFastAll, vendedorasAll });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getHistorialVentasPlantelRi = async (req, res) => {
  try{

    // 1-. Sacar el tipo de venta
    const { ventas } = req.body

    // 1-. Consultar todos los ciclos despues del seleccionado
    const ciclosFinales = await Kpi.getCiclosFecha( req.body.fecha_inicio_ciclo ).then( response => response )

    // 2-. Sacamos el ciclo final solo para poder mostrarlo en el FRONT y listo 
    let cicloFinal = ciclosFinales[ ciclosFinales.length - 1 ]

    const ciclosDespuesUltimo = await Kpi.getCicloFinalUltimo( cicloFinal.fecha_inicio_ciclo ).then( response => response )

    // Consultamos los planteles
    let planteles = await dashboardKpi.getPlanteles().then(response=> response)

    planteles = planteles.map(p => ({ id_plantel: p.id_plantel, plantel: p.plantel, ciclo: '', escuela: p.escuela }));

    let plantelesInbi = planteles.filter( el => { return el.escuela == 1 && ![19,20].includes(el.id_plantel) })
    let plantelesFast = planteles.filter( el => { return el.escuela == 2 && ![19,20].includes(el.id_plantel) })


    // HEADERS
    let headers = [
      { text: 'Plantel' , value: 'plantel' }
    ]

    for( const i in ciclosFinales ){
      let ciclo = ciclosFinales[i].ciclo.replace(' ','_')
      headers.push({ text: ciclosFinales[i].ciclo, value: 'cantidad_'+ciclo })
    }

    // Agregamos el total
    headers.push({ text: 'Total', value: 'total' })

    let vendedorasInbiAll = []
    let vendedorasFastAll = []


    for( const i in ciclosFinales ){

      let cicloAct = ciclosFinales[i]
      let cicloSig = null

      if( i == ciclosFinales.length - 1 )
        cicloSig = ciclosDespuesUltimo ? ciclosDespuesUltimo : ciclosFinales[i]
      else
        cicloSig = ciclosFinales[parseInt( i ) + 1]


      /****************************************/
      /*                E X C I               */
      /****************************************/

      let registros = await exci.getRegistrosExciBeca( ).then( response => response )
      // Solo los que ya tienen grupo
      const alumnosExciGrupo = registros.filter( el=> { return el.id_grupo && [cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado].includes(el.id_ciclo)})

      // Sacamos los idAlumnosExci
      const idAlumnosExci = alumnosExciGrupo.map((registro)=>{ return registro.id_alumno })
      const idsGrupos     = alumnosExciGrupo.map((registro)=>{ return registro.id_grupo_encuesta })
      // Consultadmos las becas
      let grupos = []
      if( idAlumnosExci.length > 0 ){ grupos  = await exci.gruposExci( idsGrupos ).then( response => response ) }

      for( const i in alumnosExciGrupo ){
        const { id_grupo_encuesta, id_alumno } = alumnosExciGrupo[i]
        const existeGrupo = grupos.find( el=> el.id_grupo == id_grupo_encuesta )
        alumnosExciGrupo[i].id_plantel = existeGrupo ? existeGrupo.id_plantel : null 
      }

      /****************************************/
      /*        C I C L O  A C T U A L        */
      /****************************************/
      // Esta línea se ocupa aquí, por que alugumos becados no se reinscriben, entonces... vale queso 
      // consultamos los alumnos inscritos actualmente
      const alumnosActuales   = await Kpi.getAlumnosInscritos( cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado ).then(response=>response)
      const idAlumnosActuales = alumnosActuales.map((registro)=>{ return registro.id_alumno })

      // consultamos los alumnos inscritos actualmente
      const alumnosSiguientes = await Kpi.getAlumnosInscritosCicloSiguienteRI( cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado, cicloSig.id_ciclo, cicloSig.id_ciclo_relacionado ).then(response=>response)

      // Alumnos que están becados para el siguiente ciclo
      const alumnosBecaSiguiente = await Kpi.getAlumnosFuturaBeca( cicloAct.id_ciclo, cicloAct.id_ciclo_relacionado, cicloSig.id_ciclo, cicloSig.id_ciclo_relacionado ).then(response=>response)

      // Id de los alumnos becados
      const idBecadosSiguiente = alumnosBecaSiguiente.map((registro)=>{ return registro.id_alumno })
      
      // Calular la cantidad de alumnos inscritos por plantel
      let riPlantelesFast = []
      let riPlantelesInbi = []

      let becadosBecados = []

      // Recorremos los planteles para agregarles lo del RI 
      // sacamos el total de alumnos en el siguiente ciclo los cuales es con respecto a los reinscribibles
      let alumnosRegulares  = alumnosActuales.filter( el => { return el.adeudo == 0 && el.pagado > 0 && el.certificacion == 0 })
      const idRegulares     = alumnosRegulares.map((registro)=>{ return registro.id_alumno })
      
      let acumulado         = alumnosSiguientes.filter(el=> { return idRegulares.includes(el.id_alumno) })

      ciclosFinales[i]['datos_fast'] = acumulado
      ciclosFinales[i]['datos_inbi'] = acumulado
    }

    plantelesInbi = plantelesInbi.sort((a, b) => a.plantel.localeCompare(b.plantel));


    // Agregamos la columna del total
    plantelesInbi.push({ id_plantel: 1000, plantel: 'TOTAL', ciclo: 'Sin ciclo', escuela: 1})

    let totalFinal = 0

    for( const i in plantelesInbi ){

      // Desestrucutramos la vendedora
      const { id_plantel, plantel } = plantelesInbi[i]

      let total  = 0

      // Ahora, tenemos que recorrer los ciclossss
      for( const j in ciclosFinales ){

        let ciclo = 'cantidad_' + ciclosFinales[j].ciclo.replace(' ','_')

        if( ventas == 'Alumnos' )
          total  += ciclosFinales[j].datos_inbi.filter( el => { return el.id_plantel == id_plantel }).length
        else
          total  += ciclosFinales[j].datos_inbi.filter( el => { return el.id_plantel == id_plantel }).map(item => item.nuevoPago).reduce((prev, curr) => prev + curr, 0)
        
        let totalVentas = 0

        if( ventas == 'Alumnos' )
          totalVentas = plantel == 'TOTAL' ? ciclosFinales[j].datos_inbi.filter( el => { return el.escuela == 1 }).length : ciclosFinales[j].datos_inbi.filter( el => { return el.id_plantel == id_plantel }).length
        else
          totalVentas = plantel == 'TOTAL' ? ciclosFinales[j].datos_inbi.filter( el => { return el.escuela == 1 }).map(item => item.nuevoPago).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options) : ciclosFinales[j].datos_inbi.filter( el => { return el.id_plantel == id_plantel }).map(item => item.nuevoPago).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options)

        plantelesInbi[i][ciclo] = totalVentas
        
      }


      totalFinal  += total

      if( ventas == 'Alumnos' )
        plantelesInbi[i]['total']  = plantel == 'TOTAL' ? totalFinal  : total
      else
        plantelesInbi[i]['total']  = plantel == 'TOTAL' ? totalFinal.toLocaleString('en-US', options)  : total.toLocaleString('en-US', options)

    }


    // FAST
    plantelesFast = plantelesFast.sort((a, b) => a.plantel.localeCompare(b.plantel));
    
    // Agregamos la columna del total
    plantelesFast.push({ id_plantel: 1000, plantel: 'TOTAL', ciclo: 'Sin ciclo'})

    let totalFinalFast = 0

    for( const i in plantelesFast ){

      // Desestrucutramos la vendedora
      const { id_plantel, plantel } = plantelesFast[i]

      let total  = 0

      // Ahora, tenemos que recorrer los ciclossss
      for( const j in ciclosFinales ){

        let ciclo = 'cantidad_' + ciclosFinales[j].ciclo.replace(' ','_')

        if( ventas == 'Alumnos' )
          total  += ciclosFinales[j].datos_fast.filter( el => { return el.id_plantel == id_plantel }).length
        else
          total  += ciclosFinales[j].datos_fast.filter( el => { return el.id_plantel == id_plantel }).map(item => item.nuevoPago).reduce((prev, curr) => prev + curr, 0)
        
        let totalVentas = 0

        if( ventas == 'Alumnos' )
          totalVentas = plantel == 'TOTAL' ? ciclosFinales[j].datos_fast.filter( el => { return el.escuela == 1 }).length : ciclosFinales[j].datos_fast.filter( el => { return el.id_plantel == id_plantel }).length
        else
          totalVentas = plantel == 'TOTAL' ? ciclosFinales[j].datos_fast.filter( el => { return el.escuela == 2 }).map(item => item.nuevoPago).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options) : ciclosFinales[j].datos_fast.filter( el => { return el.id_plantel == id_plantel }).map(item => item.nuevoPago).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options)

        plantelesFast[i][ciclo] = totalVentas

      }

      totalFinalFast  += total

      if( ventas == 'Alumnos' )
        plantelesFast[i]['total']  = plantel == 'TOTAL' ? totalFinalFast  : total
      else
        plantelesFast[i]['total']  = plantel == 'TOTAL' ? totalFinalFast.toLocaleString('en-US', options)  : total.toLocaleString('en-US', options)

    }

    res.send({ ciclosFinales, cicloFinal, headers, vendedorasInbiAll: plantelesInbi, vendedorasFastAll: plantelesFast, vendedorasAll: planteles });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getHistorialVentasFecha = async (req, res) => {
  try{

    // 1-. Sacar el tipo de venta
    const { ventas, fecha_inicio, fecha_final } = req.body

    // 2-. Consultar las fechas habiles de inscritos
    const fechasUnicas = await Kpi.getFechasUnicas( fecha_inicio, fecha_final ).then( response => response )
    const fechas       = await Kpi.getFechasUnicas( fecha_inicio, fecha_final ).then( response => response )

    // HEADERS
    let headers = [
      { text: 'Vendedora' , value: 'vendedora' }
    ]

    for( const i in fechasUnicas ){

      let fecha = fechasUnicas[i].fecha_creacion.replace('-','_')

      headers.push({ text: fechasUnicas[i].fecha_creacion, value: 'cantidad_'+fecha, align: 'center' })

    }

    // Agregamos el total
    headers.push({ text: 'Total' , value: 'total' })

    let vendedorasInbiAll = []
    let vendedorasFastAll = []

    for( const i in fechasUnicas ){

      const { fecha_creacion } = fechasUnicas[i]

      let nuevasMatriculas = await Kpi.getNuevasMatriculas( fecha_creacion, fecha_final ).then( response => response) 

      const matriculasFast = nuevasMatriculas.filter( el => { return el.unidad_negocio == 2 })
      const matriculasInbi = nuevasMatriculas.filter( el => { return el.unidad_negocio == 1 })

      // Vendedoras inbi, sin duplicados
      const vendedorasInbi = matriculasInbi.filter((persona, index, self) =>
        index === self.findIndex((p) => (
          p.vendedora === persona.vendedora
        ))
      ).map(persona => ({ id_usuario: persona.id_usuario_ultimo_cambio, vendedora: persona.vendedora, activo_sn: persona.activo_sn, pagado: persona.pagado }));

      // Vendedoras fast, sin duplicados
      const vendedorasFast = matriculasFast.filter((persona, index, self) =>
        index === self.findIndex((p) => (
          p.vendedora === persona.vendedora
        ))
      ).map(persona => ({ id_usuario: persona.id_usuario_ultimo_cambio, vendedora: persona.vendedora, activo_sn: persona.activo_sn, pagado: persona.pagado }));


      // Concatenamos los datos, para al final poder sacar solo las vendedoras sin duplicados
      vendedorasInbiAll = vendedorasInbiAll.concat( vendedorasInbi )
      vendedorasFastAll = vendedorasFastAll.concat( vendedorasFast )

      fechasUnicas[i]['datos_fast'] = matriculasFast.filter( el => { return el.adeudo <= 0 && el.induccion == 0 })
      fechasUnicas[i]['datos_inbi'] = matriculasInbi.filter( el => { return el.adeudo <= 0 && el.induccion == 0 })
    }

    // Sacamos los duplicados de las vendedoras de FAST e INBI
    vendedorasInbiAll = vendedorasInbiAll.filter((persona, index, self) =>
      index === self.findIndex((p) => (
        p.vendedora === persona.vendedora
      ))
    )

    vendedorasFastAll = vendedorasFastAll.filter((persona, index, self) =>
      index === self.findIndex((p) => (
        p.vendedora === persona.vendedora
      ))
    )

    // Ordenar los nombres
    vendedorasInbiAll = vendedorasInbiAll.sort((a, b) => a.vendedora.localeCompare(b.vendedora));
    vendedorasFastAll = vendedorasFastAll.sort((a, b) => a.vendedora.localeCompare(b.vendedora));

    vendedorasInbiAll.push({ id_usuario: 1000, vendedora: 'TOTAL', ciclo: 'Sin ciclo', align: 'center' })
    vendedorasFastAll.push({ id_usuario: 1000, vendedora: 'TOTAL', ciclo: 'Sin ciclo', align: 'center' })

    let totalFinal  = 0

    // Llenemos ahora el headers con la siguiente información
    for( const i in vendedorasInbiAll ){

      // Desestrucutramos la vendedora
      const{ vendedora } = vendedorasInbiAll[i]

      let total  = 0

      // Ahora, tenemos que recorrer los ciclossss
      for( const j in fechasUnicas ){

        let fecha = 'cantidad_'+fechasUnicas[j].fecha_creacion.replace('-','_')

        if( ventas == 'Alumnos' )
          total  += fechasUnicas[j].datos_inbi.filter( el => { return el.vendedora == vendedora }).length
        else
          total  += fechasUnicas[j].datos_inbi.filter( el => { return el.vendedora == vendedora }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
        
        let totalVentas = 0

        if( ventas == 'Alumnos' )
          totalVentas = vendedora == 'TOTAL' ? fechasUnicas[j].datos_inbi.length : fechasUnicas[j].datos_inbi.filter( el => { return el.vendedora == vendedora }).length
        else
          totalVentas = vendedora == 'TOTAL' ? fechasUnicas[j].datos_inbi.map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options) : fechasUnicas[j].datos_inbi.filter( el => { return el.vendedora == vendedora }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options)

        vendedorasInbiAll[i][fecha] = totalVentas

      }

      totalFinal  += total

      if( ventas == 'Alumnos' )
        vendedorasInbiAll[i]['total']  = vendedora == 'TOTAL' ? totalFinal  : total
      else
        vendedorasInbiAll[i]['total']  = vendedora == 'TOTAL' ? totalFinal.toLocaleString('en-US', options)  : total.toLocaleString('en-US', options)

    }


    let totalFinalFast  = 0

    // Llenemos ahora el headers con la siguiente información
    for( const i in vendedorasFastAll ){

      // Desestrucutramos la vendedora
      const{ vendedora } = vendedorasFastAll[i]

      let total  = 0

      // Ahora, tenemos que recorrer los ciclossss
      for( const j in fechasUnicas ){

        let fecha = 'cantidad_'+fechasUnicas[j].fecha_creacion.replace('-','_')

        if( ventas == 'Alumnos' )
          total  += fechasUnicas[j].datos_fast.filter( el => { return el.vendedora == vendedora }).length
        else
          total  += fechasUnicas[j].datos_fast.filter( el => { return el.vendedora == vendedora }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
        
        let totalVentas = 0

        if( ventas == 'Alumnos' )
          totalVentas = vendedora == 'TOTAL' ? fechasUnicas[j].datos_fast.length : fechasUnicas[j].datos_fast.filter( el => { return el.vendedora == vendedora }).length
        else
          totalVentas = vendedora == 'TOTAL' ? fechasUnicas[j].datos_fast.map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options) : fechasUnicas[j].datos_fast.filter( el => { return el.vendedora == vendedora }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options)

        vendedorasFastAll[i][fecha] = totalVentas

      }

      totalFinalFast  += total

      if( ventas == 'Alumnos' )
        vendedorasFastAll[i]['total']  = vendedora == 'TOTAL' ? totalFinalFast  : total
      else
        vendedorasFastAll[i]['total']  = vendedora == 'TOTAL' ? totalFinalFast.toLocaleString('en-US', options)  : total.toLocaleString('en-US', options)

    }

    const vendedorasAll = vendedorasInbiAll.concat( vendedorasFastAll )
    res.send({ fechasUnicas, headers, vendedorasInbiAll, vendedorasFastAll, vendedorasAll, fechas });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getHistorialVentasPlantelFecha = async (req, res) => {
  try{

    // 1-. Sacar el tipo de venta
    const { ventas, fecha_inicio, fecha_final } = req.body

    // 2-. Consultar las fechas habiles de inscritos
    const fechasUnicas = await Kpi.getFechasUnicas( fecha_inicio, fecha_final ).then( response => response )
    const fechas       = await Kpi.getFechasUnicas( fecha_inicio, fecha_final ).then( response => response )

    // HEADERS
    let headers = [
      { text: 'Plantel' , value: 'plantel' }
    ]
    
    for( const i in fechasUnicas ){

      let fecha = fechasUnicas[i].fecha_creacion.replace('-','_')

      headers.push({ text: fechasUnicas[i].fecha_creacion, value: 'cantidad_'+fecha, align: 'center' })

    }

    // Agregamos el total
    headers.push({ text: 'Total' , value: 'total' })

    let vendedorasInbiAll = []
    let vendedorasFastAll = []

    for( const i in fechasUnicas ){

      const { fecha_creacion } = fechasUnicas[i]

      let nuevasMatriculas = await Kpi.getNuevasMatriculas( fecha_creacion, fecha_final ).then( response => response) 

      const matriculasFast = nuevasMatriculas.filter( el => { return el.unidad_negocio == 2 })
      const matriculasInbi = nuevasMatriculas.filter( el => { return el.unidad_negocio == 1 })

      // Vendedoras inbi, sin duplicados
      const vendedorasInbi = matriculasInbi.filter((plantel2, index, self) =>
        index === self.findIndex((p) => (
          p.plantel === plantel2.plantel
        ))
      ).map(plantel2 => ({ plantel: plantel2.plantel, activo_sn: 1, pagado: plantel2.pagado }));

      // Vendedoras fast, sin duplicados
      const vendedorasFast = matriculasFast.filter((plantel2, index, self) =>
        index === self.findIndex((p) => (
          p.plantel === plantel2.plantel
        ))
      ).map(plantel2 => ({ plantel: plantel2.plantel, activo_sn: 1, pagado: plantel2.pagado }));

      vendedorasInbiAll = vendedorasInbiAll.concat( vendedorasInbi )
      vendedorasFastAll = vendedorasFastAll.concat( vendedorasFast )

      fechasUnicas[i]['datos_fast'] = matriculasFast.filter( el => { return el.adeudo <= 0 && el.induccion == 0 })
      fechasUnicas[i]['datos_inbi'] = matriculasInbi.filter( el => { return el.adeudo <= 0 && el.induccion == 0 })
    }

    // Sacamos los duplicados de las vendedoras de FAST e INBI
    vendedorasInbiAll = vendedorasInbiAll.filter((plantel2, index, self) =>
      index === self.findIndex((p) => (
        p.plantel === plantel2.plantel
      ))
    )

    vendedorasFastAll = vendedorasFastAll.filter((plantel2, index, self) =>
      index === self.findIndex((p) => (
        p.plantel === plantel2.plantel
      ))
    )

    // Ordenar los nombres
    vendedorasInbiAll = vendedorasInbiAll.sort((a, b) => a.plantel.localeCompare(b.plantel));
    vendedorasFastAll = vendedorasFastAll.sort((a, b) => a.plantel.localeCompare(b.plantel));

    vendedorasInbiAll.push({ id_plantel: 1000, plantel: 'TOTAL', ciclo: 'Sin ciclo'})
    vendedorasFastAll.push({ id_plantel: 1000, plantel: 'TOTAL', ciclo: 'Sin ciclo'})

    let totalFinal  = 0
    let pagadoTotal = 0

    // Llenemos ahora el headers con la siguiente información
    for( const i in vendedorasInbiAll ){

      // Desestrucutramos la vendedora
      const{ plantel } = vendedorasInbiAll[i]

      let total  = 0
      let pagado = 0

      // Ahora, tenemos que recorrer los ciclossss
      for( const j in fechasUnicas ){

        let fecha = 'cantidad_'+fechasUnicas[j].fecha_creacion.replace('-','_')

        if( ventas == 'Alumnos' )
          total  += fechasUnicas[j].datos_inbi.filter( el => { return el.plantel == plantel }).length
        else
          total  += fechasUnicas[j].datos_inbi.filter( el => { return el.plantel == plantel }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
        
        let totalVentas = 0

        if( ventas == 'Alumnos' )
          totalVentas = plantel == 'TOTAL' ? fechasUnicas[j].datos_inbi.length : fechasUnicas[j].datos_inbi.filter( el => { return el.plantel == plantel }).length
        else
          totalVentas = plantel == 'TOTAL' ? fechasUnicas[j].datos_inbi.map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options) : fechasUnicas[j].datos_inbi.filter( el => { return el.plantel == plantel }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options)

        vendedorasInbiAll[i][fecha] = totalVentas

      }

      totalFinal  += total

      if( ventas == 'Alumnos' )
        vendedorasInbiAll[i]['total']  = plantel == 'TOTAL' ? totalFinal  : total
      else
        vendedorasInbiAll[i]['total']  = plantel == 'TOTAL' ? totalFinal.toLocaleString('en-US', options)  : total.toLocaleString('en-US', options)

    }


    let totalFinalFast  = 0

    // Llenemos ahora el headers con la siguiente información
    for( const i in vendedorasFastAll ){

      // Desestrucutramos la vendedora
      const{ plantel } = vendedorasFastAll[i]

      let total  = 0

      // Ahora, tenemos que recorrer los ciclossss
      for( const j in fechasUnicas ){

        let fecha = 'cantidad_'+fechasUnicas[j].fecha_creacion.replace('-','_')

        if( ventas == 'Alumnos' )
          total  += fechasUnicas[j].datos_fast.filter( el => { return el.plantel == plantel }).length
        else
          total  += fechasUnicas[j].datos_fast.filter( el => { return el.plantel == plantel }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
        
        let totalVentas = 0

        if( ventas == 'Alumnos' )
          totalVentas = plantel == 'TOTAL' ? fechasUnicas[j].datos_fast.length : fechasUnicas[j].datos_fast.filter( el => { return el.plantel == plantel }).length
        else
          totalVentas = plantel == 'TOTAL' ? fechasUnicas[j].datos_fast.map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options) : fechasUnicas[j].datos_fast.filter( el => { return el.plantel == plantel }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options)

        vendedorasFastAll[i][fecha] = totalVentas

      }

      totalFinalFast  += total

      if( ventas == 'Alumnos' )
        vendedorasFastAll[i]['total']  = plantel == 'TOTAL' ? totalFinalFast  : total
      else
        vendedorasFastAll[i]['total']  = plantel == 'TOTAL' ? totalFinalFast.toLocaleString('en-US', options)  : total.toLocaleString('en-US', options)
    }

    const vendedorasAll = vendedorasInbiAll.concat( vendedorasFastAll )
    res.send({ fechasUnicas, headers, vendedorasInbiAll, vendedorasFastAll, vendedorasAll, fechas });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getHistorialVentasPlantelRiFecha = async (req, res) => {
  try{

    // 1-. Sacar el tipo de venta
    const { ventas, fecha_inicio, fecha_final } = req.body

    // 2-. Consultar las fechas habiles de inscritos
    const fechasUnicas = await Kpi.getFechasUnicas( fecha_inicio, fecha_final ).then( response => response )
    const fechas       = await Kpi.getFechasUnicas( fecha_inicio, fecha_final ).then( response => response )

    // HEADERS
    let headers = [
      { text: 'Plantel' , value: 'plantel' }
    ]
    
    for( const i in fechasUnicas ){

      let fecha = fechasUnicas[i].fecha_creacion.replace('-','_')

      headers.push({ text: fechasUnicas[i].fecha_creacion, value: 'cantidad_'+fecha, align: 'center' })

    }

    // Agregamos el total
    headers.push({ text: 'Total' , value: 'total' })

    let vendedorasInbiAll = []
    let vendedorasFastAll = []

    for( const i in fechasUnicas ){

      const { fecha_creacion } = fechasUnicas[i]

      let nuevasMatriculas = await Kpi.getNuevasMatriculas( fecha_creacion, fecha_final ).then( response => response) 

      let idMatriculas = nuevasMatriculas.map((registro) => { return registro.id_alumno })

      idMatriculas = idMatriculas.length ? idMatriculas : [0]

      let matriculasAll    = await Kpi.getMatriculasAll( idMatriculas, fecha_creacion, fecha_final ).then( response => response) 

      const matriculasFast = matriculasAll.filter( el => { return el.unidad_negocio == 2 })
      const matriculasInbi = matriculasAll.filter( el => { return el.unidad_negocio == 1 })

      // Vendedoras inbi, sin duplicados
      const vendedorasInbi = matriculasInbi.filter((plantel2, index, self) =>
        index === self.findIndex((p) => (
          p.plantel === plantel2.plantel
        ))
      ).map(plantel2 => ({ plantel: plantel2.plantel, activo_sn: 1, pagado: plantel2.pagado }));

      // Vendedoras fast, sin duplicados
      const vendedorasFast = matriculasFast.filter((plantel2, index, self) =>
        index === self.findIndex((p) => (
          p.plantel === plantel2.plantel
        ))
      ).map(plantel2 => ({ plantel: plantel2.plantel, activo_sn: 1, pagado: plantel2.pagado }));

      vendedorasInbiAll = vendedorasInbiAll.concat( vendedorasInbi )
      vendedorasFastAll = vendedorasFastAll.concat( vendedorasFast )

      fechasUnicas[i]['datos_fast'] = matriculasFast.filter( el => { return el.adeudo <= 0 && el.induccion == 0 })
      fechasUnicas[i]['datos_inbi'] = matriculasInbi.filter( el => { return el.adeudo <= 0 && el.induccion == 0 })
    }

    // Sacamos los duplicados de las vendedoras de FAST e INBI
    vendedorasInbiAll = vendedorasInbiAll.filter((plantel2, index, self) =>
      index === self.findIndex((p) => (
        p.plantel === plantel2.plantel
      ))
    )

    vendedorasFastAll = vendedorasFastAll.filter((plantel2, index, self) =>
      index === self.findIndex((p) => (
        p.plantel === plantel2.plantel
      ))
    )

    // Ordenar los nombres
    vendedorasInbiAll = vendedorasInbiAll.sort((a, b) => a.plantel.localeCompare(b.plantel));
    vendedorasFastAll = vendedorasFastAll.sort((a, b) => a.plantel.localeCompare(b.plantel));

    vendedorasInbiAll.push({ id_plantel: 1000, plantel: 'TOTAL', ciclo: 'Sin ciclo'})
    vendedorasFastAll.push({ id_plantel: 1000, plantel: 'TOTAL', ciclo: 'Sin ciclo'})

    let totalFinal  = 0
    let pagadoTotal = 0

    // Llenemos ahora el headers con la siguiente información
    for( const i in vendedorasInbiAll ){

      // Desestrucutramos la vendedora
      const{ plantel } = vendedorasInbiAll[i]

      let total  = 0
      let pagado = 0

      // Ahora, tenemos que recorrer los ciclossss
      for( const j in fechasUnicas ){

        let fecha = 'cantidad_'+fechasUnicas[j].fecha_creacion.replace('-','_')

        if( ventas == 'Alumnos' )
          total  += fechasUnicas[j].datos_inbi.filter( el => { return el.plantel == plantel }).length
        else
          total  += fechasUnicas[j].datos_inbi.filter( el => { return el.plantel == plantel }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
        
        let totalVentas = 0

        if( ventas == 'Alumnos' )
          totalVentas = plantel == 'TOTAL' ? fechasUnicas[j].datos_inbi.length : fechasUnicas[j].datos_inbi.filter( el => { return el.plantel == plantel }).length
        else
          totalVentas = plantel == 'TOTAL' ? fechasUnicas[j].datos_inbi.map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options) : fechasUnicas[j].datos_inbi.filter( el => { return el.plantel == plantel }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options)

        vendedorasInbiAll[i][fecha] = totalVentas

      }

      totalFinal  += total

      if( ventas == 'Alumnos' )
        vendedorasInbiAll[i]['total']  = plantel == 'TOTAL' ? totalFinal  : total
      else
        vendedorasInbiAll[i]['total']  = plantel == 'TOTAL' ? totalFinal.toLocaleString('en-US', options)  : total.toLocaleString('en-US', options)

    }


    let totalFinalFast  = 0

    // Llenemos ahora el headers con la siguiente información
    for( const i in vendedorasFastAll ){

      // Desestrucutramos la vendedora
      const{ plantel } = vendedorasFastAll[i]

      let total  = 0

      // Ahora, tenemos que recorrer los ciclossss
      for( const j in fechasUnicas ){

        let fecha = 'cantidad_'+fechasUnicas[j].fecha_creacion.replace('-','_')

        if( ventas == 'Alumnos' )
          total  += fechasUnicas[j].datos_fast.filter( el => { return el.plantel == plantel }).length
        else
          total  += fechasUnicas[j].datos_fast.filter( el => { return el.plantel == plantel }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0)
        
        let totalVentas = 0

        if( ventas == 'Alumnos' )
          totalVentas = plantel == 'TOTAL' ? fechasUnicas[j].datos_fast.length : fechasUnicas[j].datos_fast.filter( el => { return el.plantel == plantel }).length
        else
          totalVentas = plantel == 'TOTAL' ? fechasUnicas[j].datos_fast.map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options) : fechasUnicas[j].datos_fast.filter( el => { return el.plantel == plantel }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0).toLocaleString('en-US', options)

        vendedorasFastAll[i][fecha] = totalVentas

      }

      totalFinalFast  += total

      if( ventas == 'Alumnos' )
        vendedorasFastAll[i]['total']  = plantel == 'TOTAL' ? totalFinalFast  : total
      else
        vendedorasFastAll[i]['total']  = plantel == 'TOTAL' ? totalFinalFast.toLocaleString('en-US', options)  : total.toLocaleString('en-US', options)
    }

    const vendedorasAll = vendedorasInbiAll.concat( vendedorasFastAll )
    res.send({ fechasUnicas, headers, vendedorasInbiAll, vendedorasFastAll, vendedorasAll, fechas });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};



exports.getRIFecha = async (req, res) => {
  try{


    const {fechaini, vendedora} = req.body

    const infoVendedoraFechaini = await Kpi.getinfoVendedoraFechaIni( fechaini, vendedora ).then(response=>response)

    const objetoDentroDelArray = infoVendedoraFechaini[0]; 

    const id_usuario = objetoDentroDelArray.id_usuario;
    const id_plantel = objetoDentroDelArray.id_plantel;
    const plantel = objetoDentroDelArray.plantel;
    const id_ciclo = objetoDentroDelArray.id_ciclo;
    const id_ciclo_relacionado = objetoDentroDelArray.ciclo_relacionado;
    const id_ciclo_siguiente = objetoDentroDelArray.id_ciclo_siguiente;
    const id_ciclo_relacionado_siguiente = objetoDentroDelArray.ciclo_relacionado_siguiente;

    ////1.-alumnosSiguientes
    const alumnosSiguientes = await Kpi.getAlumnosInscritosCicloSiguiente( id_ciclo, id_ciclo_relacionado, id_ciclo_siguiente, id_ciclo_relacionado_siguiente ).then(response=>response)

    //3.-idAlumnosPosibles
       //Exci
       let registros = await exci.getRegistrosExciBeca( ).then( response => response )
       const alumnosExciGrupo = registros.filter( el=> { return el.id_grupo && [id_ciclo, id_ciclo_relacionado].includes(el.id_ciclo)})
       const idAlumnosExci = alumnosExciGrupo.map((registro)=>{ return registro.id_alumno })

    const alumnosActuales   = await Kpi.getAlumnosInscritos( id_ciclo, id_ciclo_relacionado  ).then(response=>response)
    let alumnosRegulares  = alumnosActuales.filter( el => { return el.adeudo == 0 && el.pagado > 0 && el.id_maestro > 0 && el.certificacion == 0 })
    const idRegulares     = alumnosRegulares.map((registro)=>{ return registro.id_alumno })
 

    //4.- idBecadosSiguiente

    let alumnosBecaSiguiente = await Kpi.getAlumnosFuturaBeca( id_ciclo, id_ciclo_relacionado, id_ciclo_siguiente, id_ciclo_relacionado_siguiente ).then(response=>response)
    const idBecadosSiguiente = alumnosBecaSiguiente.map((registro)=>{ return registro.id_alumno })
 
    //2.- id_plantel
    let planteles = await dashboardKpi.getPlanteles().then(response=> response)

    let riPlantelesTodos = []
    let riPlantelesFast = []
    let riPlantelesInbi = []
    let riFinal = []


    //FOOOOOORRRRRRRR
    for(const i in planteles){
    const { id_plantel, plantel, escuela } = planteles[i]

    let idAlumnosBecadosCerti = alumnosBecaSiguiente.filter( el => el.id_plantel == id_plantel ).map((registro) => { return registro.id_alumno })
   
    const idAlumnosPosibles = idRegulares.concat( idAlumnosExci )
    const totalAlumnosSiguientes  = alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel && idAlumnosPosibles.includes(el.id_alumno) && !idBecadosSiguiente.includes( el.id_alumno ) }).length

    //5.-regulares
    const alumnosPlantel          = alumnosActuales.filter(el=> { return el.id_plantel == id_plantel })
    const alumnosNoCertificacion  = alumnosPlantel.filter(el=> { return el.id_plantel == id_plantel && el.certificacion == 0 && !idAlumnosBecadosCerti.includes( el.id_alumno ) })
    let   regulares               = alumnosNoCertificacion.filter(el=> { return el.adeudo == 0 && el.pagado > 0 && el.id_maestro > 0 }).length

    //becadosExci
    let   becadosExci             = alumnosExciGrupo.filter(el=> { return el.id_plantel == id_plantel }).length

    let totalAlumnosAnteriores = (regulares + becadosExci)

    const porcentajeAvance        = totalAlumnosSiguientes > 0 ? ((totalAlumnosSiguientes/(regulares + becadosExci)) * 100).toFixed(2) : 0
 
    let payload = {
      id_plantel,
      totalAlumnosAnteriores,
      totalAlumnosSiguientes,
      porcentajeAvance,
      escuela
    }

    riPlantelesTodos.push( payload )

    if( escuela == 2 && ![19,20].includes(id_plantel)){
      riPlantelesFast.push( payload )
    }
    if( escuela == 1 && ![19,20].includes(id_plantel)){
      riPlantelesInbi.push( payload )
    }

   }

   if (plantel.includes("FAST")) {
     riFinal = riPlantelesFast.find( el=> el.id_plantel == id_plantel )
   } else {
     riFinal = riPlantelesInbi.find( el=> el.id_plantel == id_plantel )
   }


  res.send({ riFinal, riPlantelesTodos });

  console.log("data", riFinal)

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};