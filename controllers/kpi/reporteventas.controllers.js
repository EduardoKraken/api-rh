const reporteVentas = require("../../models/kpi/reporteventas.model.js");
const personal      = require("../../models/kpi/dashboardPersonal.model.js");
const erpviejo      = require("../../models/prospectos/erpviejo.model.js");

exports.getReporteVentasCiclos = async (req, res) => {
 
  try {

  	// desmenuzar un objeto
  	const { ciclo, fechaini, fechafin, tipo } = req.body
  	let contactos          = []
  	let contactosInscritos = []
  	let inscritos          = []
  	// Sacamos las vendedoras
    let vendedorasActuales = await personal.getUsuariosVentas( ).then(response => response)
    
    // Agregarles el nombre
    const usuariosERP    = await   erpviejo.getUsuariosERP( ).then(response=> response)

    // Sacar los contactos que hubo en esas fechas de ese ciclo
  	// 1-. Con fechas
  	if( tipo == 1 ){
  		contactos             = await   reporteVentas.getContactosVendedora( fechaini, fechafin ).then( response => response )
  		inscritos             = await   reporteVentas.getInscritosRango( fechaini, fechafin ).then( response => response )
  		contactosInscritos    = await   reporteVentas.getContactosInscritosVendedora( fechaini, fechafin ).then( response => response )
  	}

  	// 2-. Sin fechas
  	if( tipo == 2 ){
  		// const contactos
  		contactos             = await   reporteVentas.getContactosVendedora( ciclo.fecha_inicio_ciclo, ciclo.fecha_fin_ciclo ).then( response => response )
  		inscritos             = await   reporteVentas.getInscritosRango( ciclo.fecha_inicio_ciclo, ciclo.fecha_fin_ciclo ).then( response => response )
  		contactosInscritos    = await   reporteVentas.getContactosInscritosVendedora( ciclo.fecha_inicio_ciclo, ciclo.fecha_fin_ciclo ).then( response => response )
  	}

  	// Recorremos el arreglo de vendedoras
    for(const i in vendedorasActuales){
      const { iderp } = vendedorasActuales[i]
      
      // Buscar el usuario del ERP 
      const nombre              = usuariosERP.find(el=> el.id_usuario == iderp)
      const contactos_vendedora = contactos.filter( el=> { return el.usuario_asignado == iderp }).length
      const inscritos_vendedora = inscritos.filter( el=> { return el.id_usuario_ultimo_cambio == iderp }).length
      const contactos_inscritos = contactosInscritos.filter( el=> { return el.usuario_asignado == iderp }).length

      const alumnos  = contactosInscritos.filter( el=> { return el.usuario_asignado == iderp })
      let mapIdUsers = alumnos.map((registro) => registro.idalumno);

      const extemporales = inscritos.filter( el=> { return el.id_usuario_ultimo_cambio == iderp && !mapIdUsers.includes( el.id_alumno ) }).length
      const temporales   = inscritos.filter( el=> { return el.id_usuario_ultimo_cambio == iderp && mapIdUsers.includes( el.id_alumno ) }).length


      // Agregar los usuarios y nombres
      vendedorasActuales[i] = {
        id_usuario:      iderp,
        nombre_completo: nombre  ? nombre.nombre_completo : '',
        contactos:       contactos_vendedora,
        inscritos:       inscritos_vendedora,
        temporales:      temporales,
        extemporales:    extemporales
      }
    }

    // AGREGAR A LOS INSCRITOS SINNN VENDEDORA, O CON COTRA VENDEDORA
    let idVends        = vendedorasActuales.map((registro) => registro.id_usuario);
    const temporales   = inscritos.filter( el=> { return !idVends.includes( el.id_usuario_ultimo_cambio ) }).length

    vendedorasActuales.push({
      id_usuario:      0,
      nombre_completo: 'SIN VENDEDORA',
      contactos:       0,
      inscritos:       temporales,
      temporales:      0,
      extemporales:    0
    })


    const totales = {
    	inscritos:    vendedorasActuales.map(item => item.inscritos).reduce((prev, curr) => prev + curr, 0),
    	temporales:   vendedorasActuales.map(item => item.temporales).reduce((prev, curr) => prev + curr, 0),
    	extemporales: vendedorasActuales.map(item => item.extemporales).reduce((prev, curr) => prev + curr, 0)
    }

    res.send({vendedorasActuales, totales });

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getReporteVentasFechas = async (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  try {
  	// Primero consultamos los ciclos activos
    const ciclos    = await ingresos_egresos.getCiclos().then(response => response).catch(error=> error)

    // Ahora consultamos los egresos y los ingresos
    const egresos   = await ingresos_egresos.getEgresos().then(response => response).catch(error=> error)
    const ingresos  = await ingresos_egresos.getIngresos().then(response => response).catch(error=> error)
    
    // Recorremos los ciclos para saber el egreso y el ingreso da cada ciclo
    let sumaEgresoTotal = 0
    let sumaIngresoTotal = 0
    for(const i in ciclos){

    	const { id_ciclo } =  ciclos[i]
    	
      for(const j in egresos){
    		if(id_ciclo == egresos[j].id_ciclo){
          sumaEgresoTotal += egresos[j].costo_total
        }
      }

      ciclos[i] = {...ciclos[i], sumaegreso: sumaEgresoTotal}


    	const ingreso = ingresos.find(ingreso => ingreso.id_ciclo == id_ciclo)

    	if(ingreso){
    		ciclos[i] = {...ciclos[i], sumaingreso: ingreso.suma}
    		sumaIngresoTotal += ingreso.suma
    	}else{
    		ciclos[i] = {...ciclos[i], sumaingreso: 0}
    	}
      
      sumaEgresoTotal = 0
      sumaIngresoTotal = 0
    }

    let sumaEgresoTotal2 = 0
    let sumaIngresoTotal2 = 0
    for(const i in ciclos){
      sumaEgresoTotal2  += ciclos[i].sumaegreso
      sumaIngresoTotal2 += ciclos[i].sumaingreso
    }

    // Ahora tenemos que agregarle el registro de totales a la tabla
    let total = {
    	id_ciclo:1000,
    	ciclo: 'TOTAL',
    	sumaegreso: sumaEgresoTotal2,
    	sumaingreso: sumaIngresoTotal2
    }


    ciclos.push(total)

    res.send(ciclos);
  } catch (error) {
    res.status(500).send({message:error})
  }
};