const reportes = require("../../models/kpi/reportes.model.js");

exports.reporteTeacherAsistenciaFast = async (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  try {
    const habilitarGroup = await reportes.habilitarGroup().then(response => response)
    const teacher1       = await reportes.reporteTeacherAsistenciaFast1(req.params.actual).then(response => response)
    const teacher2       = await reportes.reporteTeacherAsistenciaFast2(req.params.actual).then(response => response)
    let teachers = teacher1.concat(teacher2)

    res.send(teachers);
  } catch (error) {
    res.status(500).send({message:error})
  }
};


exports.reporteTeacherAsistenciaInbi = async (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  try {
    const habilitarGroupInbi = await reportes.habilitarGroupInbi().then(response => response)
    const teacher1           = await reportes.reporteTeacherAsistenciaInbi1(req.params.actual).then(response => response)
    const teacher2           = await reportes.reporteTeacherAsistenciaInbi2(req.params.actual).then(response => response)
    let teachers = teacher1.concat(teacher2)

    res.send(teachers);
  } catch (error) {
    res.status(500).send({message:error})
  }
};


/*********************************************************************************************************/
/*********************************************************************************************************/
// RI por grupos

exports.kpiRiGrupos = async(req, res) => {
  try {
    // Primero sacamos a todos los grupos que están en ese ciclo y funcionando 
    const getGruposActual       = await reportes.getGruposActual(req.params.inbi,req.params.fast).then(response => response)

    // Ahora a los alumnos de esos grupos
    const alumosCicloActual     = await reportes.alumosCicloActual(req.params.inbi,req.params.fast).then(response => response)

    // Consultamos los alumnos que están en
    const alumosCicloSiguiente  = await reportes.alumosCicloSiguiente(req.params.inbi,req.params.fast,req.params.sigInbi,req.params.sigFast).then(response => response)

    let resultadoFinal = []
    // Creado el arreglo list para recibir las cantidades
    for(const i in getGruposActual){
      let payload = {
        id_grupo                : getGruposActual[i].id_grupo,
        grupo                   : getGruposActual[i].grupo,
        alumnos_ciclo_actual    : 0,
        alumnos_siguiente_ciclo : 0,
        faltantes               : 0
      }
      resultadoFinal.push(payload)
    }

    // Sacamos los alumnos actuales
    for(const i in resultadoFinal){
      for(const j in alumosCicloActual){
        // Solo hay que valdiar que el alumno sea de ese mismo grupo
        if(resultadoFinal[i].id_grupo == alumosCicloActual[j].id_grupo){
          resultadoFinal[i].alumnos_ciclo_actual += 1
        }
      }
    }

    // Sacamos los alumnos del siguiente ciclo
    for(const i in resultadoFinal){
      for(const j in alumosCicloSiguiente){
        // Solo hay que valdiar que el alumno sea de ese mismo grupo
        if(resultadoFinal[i].id_grupo == alumosCicloSiguiente[j].id_grupo){
          resultadoFinal[i].alumnos_siguiente_ciclo += 1
        }
      }
    }

    // Sacamos los alumnos faltantes
    for(const i in resultadoFinal){
      resultadoFinal[i].faltantes = resultadoFinal[i].alumnos_ciclo_actual - resultadoFinal[i].alumnos_siguiente_ciclo
    }

    res.send(resultadoFinal);
  } catch (error) {
    res.status(500).send({message:error})
  }
};