const ingresos_egresos = require("../../models/kpi/ingresos_egresos.model.js");

exports.getEgresosIngresos = async (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  try {
  	// Primero consultamos los ciclos activos
    const ciclos    = await ingresos_egresos.getCiclos().then(response => response).catch(error=> error)


    let idCiclos = ciclos.map((registro=> { return registro.id_ciclo }))

    idCiclos = idCiclos.length ? idCiclos : [0]

    // Ahora consultamos los egresos y los ingresos
    const egresos   = await ingresos_egresos.getEgresos( idCiclos ).then(response => response).catch(error=> error)
    const ingresos  = await ingresos_egresos.getIngresos( idCiclos ).then(response => response).catch(error=> error)
    
    // Recorremos los ciclos para saber el egreso y el ingreso da cada ciclo

    for(const i in ciclos){

    	const { id_ciclo } =  ciclos[i]
    	
      ciclos[i]['sumaegreso'] = egresos.filter( el => { return el.id_ciclo == id_ciclo }).map(item => item.costo_total).reduce((prev, curr) => prev + curr, 0);
      
      ciclos[i]['detalleEgreso'] = egresos.filter( el => { return el.id_ciclo == id_ciclo })

      ciclos[i]['sumaingreso'] = ingresos.filter( el => { return el.id_ciclo == id_ciclo }).map(item => item.suma).reduce((prev, curr) => prev + curr, 0);
      
    }


    let sumaEgresoTotal2 = 0
    let sumaIngresoTotal2 = 0

    for(const i in ciclos){
      sumaEgresoTotal2  += ciclos[i].sumaegreso
      sumaIngresoTotal2 += ciclos[i].sumaingreso
    }

    // Ahora tenemos que agregarle el registro de totales a la tabla
    let total = {
    	id_ciclo:1000,
    	ciclo: 'TOTAL',
    	sumaegreso: sumaEgresoTotal2,
    	sumaingreso: sumaIngresoTotal2
    }


    ciclos.push(total)

    res.send(ciclos);
  } catch (error) {
    res.status(500).send({message:error})
  }
};