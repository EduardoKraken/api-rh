const dashboardKpi   = require("../../models/kpi/dashboardKpi.model.js");
const prospectos     = require("../../models/prospectos/prospectos.model.js");
const organigrama    = require("../../models/prospectos/organigrama.model.js");
const entradas       = require("../../models/catalogos/entradas.model.js");
const calificaciones = require("../../models/capacitacion/calificaciones.model.js");
const articulos      = require("../../models/tienda/articulos.model.js");


exports.getDashboardKpiGeneral = async(req, res) => {
  try {
    let id_ciclos          = [0]  
    let id_ciclosSiguiente = [0]  
    let id_alumnos         = [0]
    let mes_ciclo          = ''
    let anio_ciclo         = ''
    let siguienteCiclo     = ''

    let anio_cicloAnt      = ''
    let mes_cicloAnt       = ''
    let anteriorCiclo      = ''
    let id_ciclosAnterior  = [0] 
    let fechaCicloAnt      = ''
    let fechaCicloActual   = ''
    let fechaFinCicloActual = ''

    let fechaInicioAnterior = ''
    let fechaFinAnterior    = ''
    // getDiasTranscurridos 

    // consultamos los planteles
    let planteles = await dashboardKpi.getPlanteles().then(response=> response)
    
    // consultamos los ciclos actuales
    const cicloActual = await dashboardKpi.cicloActual( ).then(response=> response)
    // Guardamos los id de los ciclos
    for(const i in cicloActual){
      /********** DATOS CICLO ACTUAL ************/
      fechaCicloActual = cicloActual[i].inicio
      fechaFinCicloActual = cicloActual[i].final


      id_ciclos.push(cicloActual[i].id_ciclo)
      // Sacamos los valores para el siguiente cicloo
      // Primero el mes
      mes_ciclo  = parseInt(cicloActual[i].ciclo.substr(5,3)) == 12 ? 1 : parseInt(cicloActual[i].ciclo.substr(5,3)) + 1
      mes_ciclo = mes_ciclo < 10 ? `0${mes_ciclo}` : mes_ciclo
      // Despues el año
      anio_ciclo = parseInt(cicloActual[i].ciclo.substr(9,2))
      if ((cicloActual[i].ciclo.search('FE') != -1)) {
        siguienteCiclo = `CICLO ${mes_ciclo}_${anio_ciclo} FE`
      }else{
        siguienteCiclo = `CICLO ${mes_ciclo}_${anio_ciclo}`
      }
      

      // Ahora hay que buscar el id de este ciclo
      let getCiclo = await dashboardKpi.getCiclo(siguienteCiclo).then(response=>response)
      // Valdiamos que exista ese ciclo
      if(getCiclo){
        // Lo agregamos a los ciclos a buscar
        id_ciclosSiguiente.push(getCiclo.id_ciclo)
      }

      /********** DATOS CICLO ANTERIOR ************/
      // Calcularemos los datos del ciclo anterior
      // Primero el mes
      mes_cicloAnt  = parseInt(cicloActual[i].ciclo.substr(5,3)) == 1 ? 12 : parseInt(cicloActual[i].ciclo.substr(5,3)) - 1
      mes_cicloAnt  = mes_cicloAnt < 10 ? `0${mes_cicloAnt}` : mes_cicloAnt
      // Despues el año
      anio_cicloAnt = parseInt(cicloActual[i].ciclo.substr(9,2))
      if ((cicloActual[i].ciclo.search('FE') != -1)) {
        anteriorCiclo = `CICLO ${mes_cicloAnt}_${anio_cicloAnt} FE`
      }else{
        anteriorCiclo = `CICLO ${mes_cicloAnt}_${anio_cicloAnt}`
      }

      // Ahora hay que buscar el id de este ciclo
      let getCiclo2 = await dashboardKpi.getCiclo(anteriorCiclo).then(response=>response)
      // Valdiamos que exista ese ciclo
      if(getCiclo2){
        fechaCicloAnt = getCiclo2.inicio
        // Guardamos las fechas del ciclo anterior
        fechaInicioAnterior = getCiclo.inicio
        fechaFinAnterior    = getCiclo.fin
        
        // Lo agregamos a los ciclos a buscar
        id_ciclosAnterior.push(getCiclo2.id_ciclo)
      }
    }

    // Calular los días transcurridos del ciclo
    const getDiasTranscurridos = await dashboardKpi.getDiasTranscurridos(fechaCicloAnt, fechaCicloActual).then(response=> response)

    // consultamos los alumnos inscritos actualmente
    const alumnosActuales   = await dashboardKpi.getAlumnosCicloActual( id_ciclos ).then(response=>response)

    // consultamos los alumnos inscritos actualmente
    console.log( id_ciclos, id_ciclosSiguiente )
    const alumnosSiguientes = await dashboardKpi.getAlumnosCicloSiguiente( id_ciclos, id_ciclosSiguiente ).then(response=>response)

    /********** ALUMNOS CICLO ANTERIOR **************/
    let fechaLimite = getDiasTranscurridos.dias_transcurridos
    // consultamos los alumnos inscritos anteriormente
    const alumnosAnteriores = await dashboardKpi.getAlumnosCicloActual( id_ciclosAnterior ).then(response=>response)
    // consultamos los alumnos inscritos de ese ciclo anterior contra el actual
    const alumnosAnterioresActuales = await dashboardKpi.getAlumnosCicloSiguienteAnterior( id_ciclosAnterior, id_ciclos, fechaLimite ).then(response=>response)

    /******** CALCULANDO RI ********/
    // Calular la cantidad de alumnos inscritos por plantel
    let riPlanteles = []
    for(const i in planteles){
      const { id_plantel, plantel } = planteles[i]
      // sacamos el total de alumnos en el ciclo
      const totalAlumnosActuales    =  alumnosActuales.filter(el=> { return el.id_plantel == id_plantel }).length
      const totalAlumnosActualesAnt =  alumnosAnteriores.filter(el=> { return el.id_plantel == id_plantel }).length
      // sacamos el total de alumnos en el siguiente ciclo
      const totalAlumnosSiguientes  =  alumnosSiguientes.filter(el=> { return el.id_plantel == id_plantel }).length
      const totalAlumnosAnteriores  =  alumnosAnterioresActuales.filter(el=> { return el.id_plantel == id_plantel }).length

      const porcentajeRi    = totalAlumnosSiguientes > 0 ? ((totalAlumnosSiguientes/totalAlumnosActuales)*100).toFixed(2) : 0
      const porcentajeRiAnt = totalAlumnosAnteriores > 0 ? ((totalAlumnosAnteriores/totalAlumnosActualesAnt)*100).toFixed(2) : 0
      let payload = {
        id_plantel,
        plantel   ,
        porcentajeRi,
        porcentajeRiAnt,
        diferenciaPorcentaje: (porcentajeRi - porcentajeRiAnt).toFixed(2)
      }
      riPlanteles.push( payload )
    }

    // CALCULO GENERAL DE FAST
    let totalAlumnosActuales    =  alumnosActuales.filter(el=> { return el.escuela == 2 }).length
    let totalAlumnosActualesAnt =  alumnosAnteriores.filter(el=> { return el.escuela == 2 }).length
    // sacamos el total de alumnos en el siguiente ciclo
    let totalAlumnosSiguientes  =  alumnosSiguientes.filter(el=> { return el.escuela == 2 }).length
    let totalAlumnosAnteriores  =  alumnosAnterioresActuales.filter(el=> { return el.escuela == 2 }).length


    // Agregar el plantel de todos
    let porcentajeRi    = totalAlumnosSiguientes > 0 ? ((totalAlumnosSiguientes/totalAlumnosActuales)*100).toFixed(1) : 0
    let porcentajeRiAnt = totalAlumnosAnteriores > 0 ? ((totalAlumnosAnteriores/totalAlumnosActualesAnt)*100).toFixed(1) : 0

    let payload = {
      id_plantel      : 0,
      plantel         : 'TODOS',
      escuela         : 2,
      porcentajeRi,
      porcentajeRiAnt,
      diferenciaPorcentaje: (porcentajeRi - porcentajeRiAnt).toFixed(1)
    }
    riPlanteles.push( payload )

    // CALCULO GENERAL DE INBI
    totalAlumnosActuales    =  alumnosActuales.filter(el=> { return el.escuela == 1 }).length
    totalAlumnosActualesAnt =  alumnosAnteriores.filter(el=> { return el.escuela == 1 }).length
    // sacamos el total de alumnos en el siguiente ciclo
    totalAlumnosSiguientes  =  alumnosSiguientes.filter(el=> { return el.escuela == 1 }).length
    totalAlumnosAnteriores  =  alumnosAnterioresActuales.filter(el=> { return el.escuela == 1 }).length


    // Agregar el plantel de todos
    porcentajeRi    = totalAlumnosSiguientes > 0 ? ((totalAlumnosSiguientes/totalAlumnosActuales)*100).toFixed(1) : 0
    porcentajeRiAnt = totalAlumnosAnteriores > 0 ? ((totalAlumnosAnteriores/totalAlumnosActualesAnt)*100).toFixed(1) : 0

    payload = {
      id_plantel      : 100,
      plantel         : 'TODOS',
      escuela         : 1,
      porcentajeRi,
      porcentajeRiAnt,
      diferenciaPorcentaje: (porcentajeRi - porcentajeRiAnt).toFixed(1)
    }
    riPlanteles.push( payload )



    /******** CALCULANDO EL NI ********/
    // Calular los NI
    const niAlumnosActual   = await dashboardKpi.getNiAlumnos(id_ciclosSiguiente).then(response => response); // consultar los Ni de estos ciclos
    const niAlumnosAnterior = await dashboardKpi.getNiAlumnosSiguientes(id_ciclos, fechaLimite).then(response => response); // consultar los Ni de estos ciclos anteriores
    let ventas = []
    for(const i in planteles){
      const { id_plantel, plantel } = planteles[i]
      const actuales   = niAlumnosActual.find( el=> el.id_plantel == id_plantel )
      const anteriores = niAlumnosAnterior.filter( el=> { return el.id_plantel == id_plantel }).length
      let payload = {
        id_plantel,
        plantel   ,
        inscritos           : actuales   ? actuales.total : 0,
        inscritosAnteriores : anteriores ? anteriores : 0,
      }
      ventas.push( payload )
    }

    // CALCULO GENERAL DE FAST
    let actuales   = niAlumnosActual.filter( el=> { return el.escuela == 2 })
    let anteriores = niAlumnosAnterior.filter( el=> { return el.escuela == 2 }).length
    let totalActuales = 0
    for(const i in actuales){ totalActuales+= actuales[i].total }
    payload = {
      id_plantel          : 0,
      plantel             : 'TODOS',
      escuela             : 2,
      inscritos           : totalActuales,
      inscritosAnteriores : anteriores ? anteriores : 0,
    }
    ventas.push( payload )

    // CALCULO GENERAL DE INBI
    actuales   = niAlumnosActual.filter( el=> { return el.escuela == 1 })
    anteriores = niAlumnosAnterior.filter( el=> { return el.escuela == 1 }).length
    totalActuales = 0
    for(const i in actuales){ totalActuales+= actuales[i].total }
    payload = {
      id_plantel          : 100,
      plantel             : 'TODOS',
      escuela             : 1,
      inscritos           : actuales   ? totalActuales : 0,
      inscritosAnteriores : anteriores ? anteriores : 0,
    }
    ventas.push( payload )



    /******** CALCULANDO CANTIDAD CONTACTOS ********/
    // 
    const contactosActuales   = await dashboardKpi.getContactosPorFecha(fechaCicloActual,fechaFinCicloActual).then(response => response); // consultar los Ni de estos ciclos
    const contactosAnteriores = await dashboardKpi.getContactosPorFecha(fechaCicloAnt,fechaLimite).then(response => response); // consultar los Ni de estos ciclos anteriores
    let contactos = []
    for(const i in planteles){
      const { id_plantel, plantel } = planteles[i]
      const actuales   = contactosActuales.filter( el=> { return el.sucursal_interes == id_plantel }).length
      const anteriores = contactosAnteriores.filter( el=> { return el.sucursal_interes == id_plantel }).length
      let payload = {
        id_plantel,
        plantel   ,
        contactos       : actuales   ? actuales : 0,
        contactosAnteriores : anteriores ? anteriores : 0,
      }
      contactos.push( payload )
    }

    // CALCULO GENERAL DE FAST
    actuales   = contactosActuales.filter( el=> { return el.escuela == 2 }).length
    anteriores = contactosAnteriores.filter( el=> { return el.escuela == 2 }).length
    payload = {
      id_plantel          : 0,
      plantel             : 'TODOS',
      escuela             : 2,
      contactos           : actuales,
      contactosAnteriores : anteriores,
    }
    contactos.push( payload )

    // CALCULO GENERAL DE INBI
    actuales   = contactosActuales.filter( el=> { return el.escuela == 1 }).length
    anteriores = contactosAnteriores.filter( el=> { return el.escuela == 1 }).length
    totalActuales = 0
    for(const i in actuales){ totalActuales+= actuales[i].total }
    payload = {
      id_plantel          : 100,
      plantel             : 'TODOS',
      escuela             : 1,
      contactos           : actuales,
      contactosAnteriores : anteriores,
    }
    contactos.push( payload )

    /******** CALCULAR LA CALIDAD DE LA CLASE ********/
    // sacaro el día de ayer 
    const { dia, dia_numero } = await dashboardKpi.getDiaClase( ).then(response => response) // retorna el día de ayerrrrr
    // Ahora se tiene que consultar
    let clasesFast = await dashboardKpi.getClasesFast( id_ciclos, dia ).then(response => response)
    let clasesInbi = await dashboardKpi.getClasesInbi( id_ciclos, dia ).then(response => response)
    const clasesEscuelas = clasesFast.concat(clasesInbi)
    // Sacar las cantidad de clases buenas
    let clasesBuenasFast = await dashboardKpi.getClasesBuenasFast( ).then(response => response)
    let clasesBuenasInbi = await dashboardKpi.getClasesBuenasInbi( ).then(response => response)
    const clasesBuenasEscuelas = clasesBuenasFast.concat(clasesBuenasInbi)

    // Concatenamos las clases
    let clases = []
    for(const i in planteles){
      const { id_plantel, plantel } = planteles[i]
      const actuales   = clasesEscuelas.filter( el=> { return el.id_plantel == id_plantel }).length // 
      const anteriores = clasesBuenasEscuelas.filter( el=> { return el.id_plantel == id_plantel }).length
      let payload = {
        id_plantel,
        plantel   ,
        actuales,
        anteriores,
        porcentaje: anteriores == 0 ? 0 : (( anteriores / actuales ) * 100 ).toFixed(0) 
      }
      clases.push( payload )
    }

    // CALCULO GENERAL DE FAST
    actuales   = clasesFast.length
    anteriores = clasesBuenasFast.length

    payload = {
      id_plantel : 0,
      plantel    : 'TODOS',
      escuela    : 2,
      actuales   : actuales,
      anteriores : anteriores,
      porcentaje : anteriores == 0 ? 0 : (( anteriores / actuales ) * 100 ).toFixed(2) 
    }
    clases.push( payload )

    // CALCULO GENERAL DE INBI
    actuales   = clasesInbi.length
    anteriores = clasesBuenasInbi.length
    payload = {
      id_plantel : 100,
      plantel    : 'TODOS',
      escuela    : 1,
      actuales   : actuales,
      anteriores : anteriores,
      porcentaje : anteriores == 0 ? 0 : (( anteriores / actuales ) * 100 ).toFixed(2) 
    }
    clases.push( payload )

    // /******** CALCULAR LA CALIDAD DEL TEACHER ********/
    // Primero sacamos las clases evaluadas 
    let clasesEvaluadas = await dashboardKpi.getClasesEvaluadas( ).then( response => response )
    // Ahora que ya las tenemos hay que buscar el platel de esos grupos
    // clasesEvaluadas
    let gruposIdInbi = []
    let gruposIdFast = [] 
    for(const i in clasesEvaluadas){
      if(clasesEvaluadas[i].unidad_negocio == 1){
        gruposIdInbi.push(clasesEvaluadas[i].id_grupo)
      }else{
        gruposIdFast.push(clasesEvaluadas[i].id_grupo)
      }
    }
    let gruposInbi = []
    let gruposFast = []
    if( gruposIdInbi.length ) { gruposInbi = await dashboardKpi.getGruposInbi( gruposIdInbi ).then( response=> response) } 
    if( gruposIdFast.length ) { gruposFast = await dashboardKpi.getGruposFast( gruposIdFast ).then( response=> response) } 

    for(const i in clasesEvaluadas){
      let grupo = null
      if(clasesEvaluadas[i].unidad_negocio == 1){
        grupo = gruposInbi.find(el => el.id == clasesEvaluadas[i].id_grupo)
      }else{
        grupo = gruposFast.find(el => el.id == clasesEvaluadas[i].id_grupo)
      }
      clasesEvaluadas[i] = {
        ...clasesEvaluadas[i],
        id_plantel: grupo ? grupo.id_plantel : 0
      }
    }

    let clasesTeacher = []
    for(const i in planteles){
      const { id_plantel, plantel } = planteles[i]
      const actuales   = clasesEvaluadas.filter( el=> { return el.id_plantel == id_plantel }).length // 
      const anteriores = clasesEvaluadas.filter( el=> { return el.id_plantel == id_plantel && el.calificacion == 100 }).length
      let payload = {
        id_plantel,
        plantel,
        actuales,
        anteriores,
        porcentaje: anteriores == 0 ? 0 : (( anteriores / actuales ) * 100 ).toFixed(0) 
      }
      clasesTeacher.push( payload )
    }

    // CALCULO GENERAL DE FAST
    actuales   = clasesEvaluadas.filter( el=> { return el.unidad_negocio == 2 }).length
    anteriores = clasesEvaluadas.filter( el=> { return el.unidad_negocio == 2 && el.calificacion == 100 }).length

    payload = {
      id_plantel : 0,
      plantel    : 'TODOS',
      escuela    : 2,
      actuales   : actuales,
      anteriores : anteriores,
      porcentaje : anteriores == 0 ? 0 : (( anteriores / actuales ) * 100 ).toFixed(0) 
    }
    clasesTeacher.push( payload )

    // CALCULO GENERAL DE INBI
    actuales   = clasesEvaluadas.filter( el=> { return el.unidad_negocio == 1 }).length
    anteriores = clasesEvaluadas.filter( el=> { return el.unidad_negocio == 1 && el.calificacion == 100 }).length

    payload = {
      id_plantel : 100,
      plantel    : 'TODOS',
      escuela    : 1,
      actuales   : actuales,
      anteriores : anteriores,
      porcentaje : anteriores == 0 ? 0 : (( anteriores / actuales ) * 100 ).toFixed(0) 
    }
    clasesTeacher.push( payload )


    /******** CALCULAR EL NUMERO DE VACANTES ********/
    // Primero sacamos las clases evaluadas 
    let vacantesNumero = await dashboardKpi.getNumeroVacantes( ).then( response => response )
    let vacantes = {
      numero_vacantes: vacantesNumero.numero_vacantes,
      contratados:     vacantesNumero.contratados,
      porcentaje:      (100 - (vacantesNumero.contratados / vacantesNumero.numero_vacantes) * 100).toFixed(0)
    }

    /******** CALCULAR INGRESOS Y EGRESOS ********/
    // Ahora consultamos los egresos y los ingresos
    const egresos    = await dashboardKpi.getEgresos( id_ciclos ).then(response => response).catch(error=> error)
    const ingresos   = await dashboardKpi.getIngresos( id_ciclos ).then(response => response).catch(error=> error)

    let montosEgresos = []
    for(const i in planteles){
      const { id_plantel, plantel } = planteles[i]
      const egreso     = egresos.find( el=> el.id_plantel == id_plantel ) 
      const ingreso    = ingresos.find( el=> el.id_plantel == id_plantel ) 
      let egresoVal    = egreso ? egreso.suma : 0
      let ingresoVal   = ingreso ? ingreso.suma : 0
      let payload = {
        id_plantel,
        plantel,
        ingreso: new Intl.NumberFormat("en-US",{style: "currency", currency: "MXN"}).format(ingresoVal),
        egreso: new Intl.NumberFormat("en-US",{style: "currency", currency: "MXN"}).format(egresoVal),
        porcentaje: (( egresoVal / ingresoVal ) * 100).toFixed(0)
      }

      montosEgresos.push( payload )
    }

    // CALCULO GENERAL DE FAST
    let egreso     = egresos.filter( el=>{ return  el.escuela == 2 }) 
    let ingreso    = ingresos.filter( el=>{ return  el.escuela == 2 }) 
    let totalEgreso   = 0
    let totalIngreso  = 0
    for(const i in egreso){ totalEgreso += egreso[i].suma }
    for(const i in ingreso){ totalIngreso += ingreso[i].suma }
    payload = {
      id_plantel : 0,
      plantel    : 'TODOS',
      escuela    : 2,
      ingreso    : new Intl.NumberFormat("en-US",{style: "currency", currency: "MXN"}).format(totalIngreso),
      egreso     : new Intl.NumberFormat("en-US",{style: "currency", currency: "MXN"}).format(totalEgreso),
      porcentaje : (( totalEgreso / totalIngreso ) * 100).toFixed(0)
    }
    montosEgresos.push( payload )

    // CALCULO GENERAL DE INBI
    egreso     = egresos.filter( el=>{ return  el.escuela == 1 }) 
    ingreso    = ingresos.filter( el=>{ return  el.escuela == 1 }) 
    totalEgreso   = 0
    totalIngreso  = 0
    for(const i in egreso){ totalEgreso += egreso[i].suma }
    for(const i in ingreso){ totalIngreso += ingreso[i].suma }
    payload = {
      id_plantel : 100,
      plantel    : 'TODOS',
      escuela    : 1,
      ingreso    : new Intl.NumberFormat("en-US",{style: "currency", currency: "MXN"}).format(totalIngreso),
      egreso     : new Intl.NumberFormat("en-US",{style: "currency", currency: "MXN"}).format(totalEgreso),
      porcentaje : (( totalEgreso / totalIngreso ) * 100).toFixed(0)
    }
    montosEgresos.push( payload )


    // Respuesta a enviar
    res.send({
      montosEgresos,
      vacantes,
      clasesTeacher,
      clases,
      contactos,
      porcentajeRi: riPlanteles,
      ventas
    });
    
  } catch (error) {
    res.status(500).send({message:error})
  }

};

exports.addNumeroVacantes = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  dashboardKpi.addNumeroVacantes(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la etapa"
      })
    else res.status(200).send({ message: `La etapa se creo correctamente` });
  })
};

exports.getnumeroVacantesList = (req, res) => {
  dashboardKpi.getnumeroVacantesList((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las anuncios"
      });
    else res.send(data);
  });
};

exports.getMetasList = (req, res) => {
  dashboardKpi.getMetasList((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las anuncios"
      });
    else res.send(data);
  });
};

exports.getMetasSemanaList = (req, res) => {
  dashboardKpi.getMetasSemanaList((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las anuncios"
      });
    else res.send(data);
  });
};

exports.updateMeta = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }
  dashboardKpi.updateMeta(req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro la etapa con id : ${req.body.idmetas_dashboard_general }, ${ err }` });
      } else {
        res.status(500).send({ message: `Error al actualizar la etapa con el id : ${req.body.idmetas_dashboard_general }, ${ err } `});
      }
    } else {
      res.status(200).send({ message: `La etapa se ha actualizado correctamente.` })
    }
  })
}


exports.updateMetaSemana = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }
  dashboardKpi.updateMetaSemana(req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro la etapa con id : ${req.body.idmetas_semanales }, ${ err }` });
      } else {
        res.status(500).send({ message: `Error al actualizar la etapa con el id : ${req.body.idmetas_semanales }, ${ err } `});
      }
    } else {
      res.status(200).send({ message: `La etapa se ha actualizado correctamente.` })
    }
  })
}

// OBTENER MIS COLABORADORES
exports.getResultadosUsuario = async(req, res) => {
  try {

    // Sacamos el id del usuario 
    const { id } = req.params

    // consultamos a tosos los usuarios del organigrama
    let organigramaUsuarios       = await organigrama.getOrganigrama( ).then(response => response)

    // Sacar los que dependen de mi
    // y esos son los que yo voy a evaluar

    // sacar mi idormanigrama
    const existeIdOrganigrama = organigramaUsuarios.filter( el => { return el.iderp == id })

    // Si no existen colaboradores, hay que indicarlo
    if( !existeIdOrganigrama.length ){ return res.status(400).send({ message: 'No cunetas don colaboradores' }); }

    // consultar mi idorganigrama y luego saber quienes dependen de mi 
    const idorganigramas = existeIdOrganigrama.map((registro) => { return registro.idorganigrama })

    // Sacar los colabores pid = idorganigrama
    const colaboradores = organigramaUsuarios.filter( el => { return idorganigramas.includes( el.pid ) && el.iderp < 10000 && el.iderp != id })

    // Sacamos los iderp de todos (let respuesta)
    let iderps = colaboradores.map((registro)=>{ return registro.iderp })

    iderps = iderps.length ? iderps : [0]

    // Obtener los usuarios del sistema
    const usuariosSistema = await entradas.getUsuariosERP( iderps ).then( response => response )

    // Obtener el plantel desde IDERP
    const plantel = await organigrama.getPlantel( iderps ).then( response => response ) 

    // Obtener los usuarios del Nuevo ERP
    const usuariosNuevoERP = await calificaciones.usuariosNuevoERP( ).then( response => response )

    //Recorremos la tabla organigrama
    for (const i in colaboradores){

      //Obtenemos el iderp de todos
      const { iderp } = colaboradores[i]
      //Guardamos en una varible el resultado de la consulta cuando el idusuario y el iderp coincidan (depende del nombre del id)
      const existeUsuario = usuariosSistema.find(el=>el.id_usuario == iderp)    
      const existePlantel = plantel.find(el=>el.id_usuario == iderp)
  
      let nombre             = existeUsuario   ? existeUsuario.nombre_completo : "SIN NOMBRE"
      const plantelUsuario   = existePlantel   ? existePlantel.plantel : "SIN NOMBRE"
      let existePuesto       = usuariosNuevoERP.find(el=>el.iderp == iderp)
      existePuesto           = existePuesto ? existePuesto.puesto : 'Sin puesto'

      colaboradores[i]['nombre']  = nombre + ' / ' + existePuesto + ' / ' + plantelUsuario
      colaboradores[i]['plantel'] = plantelUsuario
      colaboradores[i]['puesto']  = existePuesto
      
    }    

    //Ordenarlo alfabeticamente por "puesto"
    colaboradores.sort(function (a, b) {
      if (a.nombre.toUpperCase() > b.nombre.toUpperCase()) {
        return 1;
      }
      if (a.nombre.toUpperCase() < b.nombre.toUpperCase()) {
        return -1;
      }
      return 0;
    });

    // Verificar que hoy sea sábado
    let diaHabilResultados     = await articulos.diaActual(  ).then( response => response )
    
    const { dia } = diaHabilResultados


    // La encuesta de hoy
    let resultadosLlenos = await dashboardKpi.existeResultadoUsuario( id ).then( response => response )

    if( resultadosLlenos ){
      // Consultar las actividades
      const getActividadesResultado = await dashboardKpi.getActividadesResultado( resultadosLlenos.idresultados_personal ).then( response => response )
      resultadosLlenos['actividades'] = getActividadesResultado
    }

    // Sacar el último registro del usuario
    let ultimoRegistro   = await dashboardKpi.getUltimoResultado( id ).then( response => response )

    if( ultimoRegistro ){
      // Consultar las actividades
      const getActividadesResultado2 = await dashboardKpi.getActividadesResultado( ultimoRegistro.idresultados_personal ).then( response => response )
      ultimoRegistro['actividades'] = getActividadesResultado2
    }

    // Formato en español 
    const fechaEspaniol    = await dashboardKpi.fechaEspaniol( ).then( response => response )

    const fechas_resultados = await dashboardKpi.fechasResultados( ).then( response => response )

    // enviar los niveles
    res.send({
      habil:            dia == 6         ? true             : false,
      ultimoRegistro:   ultimoRegistro   ? ultimoRegistro   : null,
      resultadosLlenos: resultadosLlenos ? resultadosLlenos : null,
      colaboradores,
      fechas_resultados
    });

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor'})
  }
};

// AGREGAR MI RESULTADO SEMANAL
exports.addResultadoUsuario = async(req, res) => {
  try {

    const { idcolaborador, actividades } = req.body

    // Validar que no haya más registros el día de hoy de este usuario
    const existeResultadoUsuario = await dashboardKpi.existeResultadoUsuario( idcolaborador ).then( response => response )

    if( existeResultadoUsuario ){ return res.status( 400 ).send({ message: 'Ya cuentas con un registro' })  }

    // AGREGAR EL RESULTADO
    //  2-. Ya validado, agregar
    const addResultadoUsuario = await dashboardKpi.addResultadoUsuario( req.body ).then( response => response )

    // 3-. AGREGAR LAS ACTIVIDADES
    for( const i in actividades ){

      if( actividades[i].actividad != '' ){
        // Agregando las actividades
        const addResultadoActividad = await dashboardKpi.addResultadoActividad( addResultadoUsuario.id, actividades[i] ).then( response => response )
      }
    }

    // 3-. Responder
    res.send({ message: 'Resultados agregados correctamente'});

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor'})
  }
};

// Obtener el listado de niveles
exports.getResultadoUsuario = async(req, res) => {
  try {

    const { colaboradores, fecha } = req.body

    let idcolaboradores = colaboradores.map((registro) => { return registro.iderp })

    // Validar que no haya más registros el día de hoy de este usuario
    const getResultadosUsuarioFecha = await dashboardKpi.getResultadosUsuarioFecha( idcolaboradores, fecha ).then( response => response )

    if( !getResultadosUsuarioFecha.length ){ return res.status( 400 ).send({ message: 'No cuenta con registro' })  }

    const ids = getResultadosUsuarioFecha.map((registro) => { return registro.idresultados_personal })

    // Consultar las actividades
    const getActividadesResultado = await dashboardKpi.getActividadesResultado( ids ).then( response => response )

    // Recorremos todo el historial 
    for( const i in getResultadosUsuarioFecha ){

      // Desestrucutruamos para sacaer el id del resultado
      const { idresultados_personal } = getResultadosUsuarioFecha[i] 

      // Ahora le agregamos las actividades a cada uno de los registros
      getResultadosUsuarioFecha[i]['actividades'] = getActividadesResultado.filter( el => { return el.idresultados_personal == idresultados_personal })

    }

    // Obtener los usuarios del sistema
    const usuariosSistema = await entradas.getUsuariosERP( idcolaboradores ).then( response => response )

    // Obtener el plantel desde IDERP
    const plantel = await organigrama.getPlantel( idcolaboradores ).then( response => response ) 

    // Obtener los usuarios del Nuevo ERP
    const usuariosNuevoERP = await calificaciones.usuariosNuevoERP( ).then( response => response )

    //Recorremos la tabla organigrama
    for (const i in getResultadosUsuarioFecha){

      //Obtenemos el iderp de todos
      const { idcolaborador } = getResultadosUsuarioFecha[i]
      //Guardamos en una varible el resultado de la consulta cuando el idusuario y el idcolaborador coincidan (depende del nombre del id)
      const existeUsuario = usuariosSistema.find(el=>el.id_usuario == idcolaborador)    
      const existePlantel = plantel.find(el=>el.id_usuario == idcolaborador)
  
      let nombre             = existeUsuario   ? existeUsuario.nombre_completo : "SIN NOMBRE"
      const plantelUsuario   = existePlantel   ? existePlantel.plantel : "SIN NOMBRE"
      const existePuesto     = usuariosNuevoERP.find(el=>el.iderp == idcolaborador)

      getResultadosUsuarioFecha[i]['nombre']  = nombre    
      getResultadosUsuarioFecha[i]['plantel'] = plantelUsuario
      getResultadosUsuarioFecha[i]['puesto']  = existePuesto ? existePuesto.puesto : 'Sin puesto'

      getResultadosUsuarioFecha[i]['clase_row'] = ''
      
    }  


    for( const i in colaboradores ){
      const { iderp, nombre, puesto, plantel } = colaboradores[i]


      const existeRegistro = getResultadosUsuarioFecha.find( el => el.idcolaborador == iderp )

      if( !existeRegistro ){
        console.log( colaboradores[i] )
        getResultadosUsuarioFecha.push({
          deleted: 0,
          estatus: 0,
          fecha_creacion: null,
          fecha_formato: 'Sin fecha',
          idcolaborador: iderp,
          idfechas: fecha,
          idfeje: 0,
          idresultados_personal: 0,
          nombre,
          objetivo: "NO APLICA",
          plantel,
          puesto,
          clase_row: 'usuario_no_registro'
        })

      }
    }

    // Veemos si existe, si no existe, hay que ponerle la clase de error




    // 3-. Responder
    res.send(getResultadosUsuarioFecha);

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor'})
  }
};

exports.getResultadoHistorial = async(req, res) => {
  try {

    const { id } = req.params

    // Validar que no haya más registros el día de hoy de este usuario
    const getResultadoHistorial = await dashboardKpi.getResultadoHistorial( id ).then( response => response )

    if( !getResultadoHistorial.length ){ return res.status( 400 ).send({ message: 'No cuenta con registros' })  }

    const ids = getResultadoHistorial.map((registro) => { return registro.idresultados_personal })

    // Consultar las actividades
    const getActividadesResultado = await dashboardKpi.getActividadesResultado( ids ).then( response => response )

    // Recorremos todo el historial 
    for( const i in getResultadoHistorial ){

      // Desestrucutruamos para sacaer el id del resultado
      const { idresultados_personal } = getResultadoHistorial[i] 

      // Ahora le agregamos las actividades a cada uno de los registros
      getResultadoHistorial[i]['actividades'] = getActividadesResultado.filter( el => { return el.idresultados_personal == idresultados_personal })

    }

    // 3-. Responder
    res.send(getResultadoHistorial);

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor'})
  }
};


// Actualizar los datos
exports.updateResultado = async(req, res) => {
  try {

    const { id } = req.params
    const { actividades, idjefe } = req.body

    // Validar que no haya más registros el día de hoy de este usuario
    const updateResultado = await dashboardKpi.updateResultado( idjefe, id ).then( response => response )

    // Actualizar las actividades
    for( const i in actividades ){

      const { retroalimentacion, idresultados_actividades } = actividades[i]

      const updateActididadResultado = await dashboardKpi.updateActididadResultado( retroalimentacion, idresultados_actividades ).then( response => response )
    }

    // 3-. Responder
    res.send({ message: 'Datos actualizados correctamente' });

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor'})
  }
};