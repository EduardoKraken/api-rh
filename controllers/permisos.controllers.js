//declaramos en una const nuestro model
const Permisos = require("../models/permisos.model.js");

exports.getPermisos = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Permisos.getPermisos((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los permisos"
      });
    else res.send(data);
  });
};

exports.addPermisos = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Permisos.addPermisos(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el permiso"
      })
    else res.status(200).send({ message: `El permiso se ha creado correctamente` });
  })
};

exports.updatePermisos = (req, res) => {
    if (!req.body || Object.keys(req.body).length === 0) {
      res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Permisos.updatePermisos(req.params.idpermisos, req.body, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({ message: `No se encontro al permiso con id : ${req.params.idpermisos}` });
        } else {
          res.status(500).send({ message: "Error al actualizar el permiso con el id : " + req.params.idpermisos });
        }
      } else {
        res.status(200).send({ message: `El permiso se ha actualizado correctamente.` })
      }
    })
}