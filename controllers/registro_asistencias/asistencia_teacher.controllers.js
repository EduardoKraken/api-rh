const asistencia_teacher  = require("../../models/registro_asistencias/asistencia_teacher.model.js");
const md5 = require('md5');


exports.validarAsistenciaTeacher = async(req, res) => {
  try {
		const { usuario, password, escuela, suplente, id_suplente } = req.body

		let acceso = null
		let grupo = null
		let validaAsistencia = null

		if( escuela == 2 ){
			// Validar acceso
			acceso = await asistencia_teacher.validarAccesoFast( usuario, md5(password) ).then( response => response )

		}else{
			// Validar acceso
			acceso = await asistencia_teacher.validarAccesoInbi( usuario, md5(password) ).then( response => response )
		}

		// Validar la respues
		if( !acceso ){
    	return res.status(400).send({ message: 'Datos incorrectos' });
		}

		// Aquí ahora hay que valdiar si tiene clases el día de hoy y más o menos a esta hora, 5 min antes de iniciar clase y 15 minutos después de hacerla

		const id_teacher = acceso.id

		if( escuela == 2 ){
			// Validar acceso
			grupo = await asistencia_teacher.validarGruposFast( id_teacher ).then( response => response )

		}else{
			// Validar acceso
			grupo = await asistencia_teacher.validarGruposInbi( id_teacher ).then( response => response )
		}

		// Validar si tiene grupo Activo a esta hora
		if( !grupo ){
    	return res.status(400).send({ message: ' No tienes grupos activos, lo siento :( ' });
		}


		// Ahoraaaa hay que validar que no este registrado ya en esta clase este teacher este día
		validaAsistencia = await asistencia_teacher.validaAsistencia( id_teacher, grupo.id_grupo ).then( response => response )

		if( validaAsistencia ){
			return res.status(400).send({ message: ' Hola, ya cuentas con un registro para este grupo el día de hoy' });
		}
		
		// Registrar los datos
		const registroar = await asistencia_teacher.addAsistenciaTeacher(grupo, escuela, suplente, id_suplente).then( response => response )
    
    return res.send({ message: 'Registrado correctamente', grupo: grupo.grupo, validaAsistencia });
    
  } catch (error) {
    res.status(500).send({message:error})
  }

};



exports.getNoAsistencias = async(req, res) => {
  try {

			// Validar acceso
		const	gruposFast = await asistencia_teacher.getGruposHoyFast( ).then( response => response )

		// Consultar las asistencias de los usuarios el día de hoyyy
		const asistencias_hoy = await asistencia_teacher.getAsistenciasHoy().then(response => response)

		const gruposPrescenciales = gruposFast.filter( el=> { return el.online == 0 })
		let gruposSinAsistencia   =  []

		for(const i in gruposFast){
			const { id_plantel, grupo, plantel } = gruposFast[i]
			// Buscaremos el plantel en las asistencias de las recepcionistas para ver si si haya asistido la recepcionista
			const existeAsistencia = asistencias_hoy.find(el=> el.id_plantel == id_plantel )
			// Validamos si no hay asistencia
			if(!existeAsistencia){
				// Guardarlo y mostrar que ese grupo no ha llegado la recepcionista
				gruposSinAsistencia.push({ grupo, plantel })
			}
		}


		// Validar que grupo no tiene asistencias
		// ui3hvorivnvr

		// Recorrer solo a los grupos que no tiene asistencia
   	req.app.io.emit('asistencias-teacher', { gruposFast })

    return res.send({ gruposSinAsistencia, gruposPrescenciales, gruposFast, asistencias_hoy });
    
  } catch (error) {
    res.status(500).send({message:error})
  }

};