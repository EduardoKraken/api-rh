const lecciones = require("../../models/sistemaweb/lecciones.model.js");

// AGREGAR LECCION
exports.addLeccion = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
    return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }
    
    // AGREGAR LA LECCION
    const leccion = await lecciones.addLeccion( req.body ).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Leccion generada correctamente', leccion });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR LA LEECCION BIEN ELIMINARLA
exports.updateLeccion = async (req, res) => {
  try{

    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // ACTUALIZAR LA LECCION
    const leccionActualizada = await lecciones.updateLeccion( req.body, id ).then( response=> response );

    res.send({ message: 'Leccion actualizada correctamente', leccionActualizada });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};
  
  
// OBTENER LECCIONES POR NIVEL
exports.getLecciones = async (req, res) => {
  try{

    const { idnivel } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!idnivel) return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    
    // OBTENER LECCIONES
    const leccionesAll = await lecciones.getLecciones( idnivel ).then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Todas las lecciones por nivel', leccionesAll });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};
  
  
// OBTENER LECCION
exports.getLeccion = async (req, res) => {
  try{

    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!id) return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    
    // OBTENER LECCION MEDIANTE ID
    const leccion = await lecciones.getLeccion( id ).then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Leccion obtenida', leccion });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};