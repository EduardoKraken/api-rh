const escuelas = require("../../models/sistemaweb/escuelas.model.js");

// AGREGAR ESCUELA
exports.addEscuela = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
    return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }
    
    // AGREGAR LA ESCUELA
    const escuela = await escuelas.addEscuela( req.body ).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Escuela generada correctamente', escuela });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR LA ESCUELA O BIEN ELIMINARLA
exports.updateEscuela = async (req, res) => {
    try{
  
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // ACTUALIZAR ESCUELA
    const escuelaActualizada = await escuelas.updateEscuela( req.body, id ).then( response=> response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Escuela actualizada correctamente', escuelaActualizada });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};
  
  
// OBTENER TODAS LAS ESCUELAS
exports.getEscuelas = async (req, res) => {
  try{
      
    // OBTENER ESCUELAS
    const escuelasAll = await escuelas.getEscuelas().then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Todas las escuelas', escuelasAll });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};
  
  
// OBTENER ESCUELA
exports.getEscuela = async (req, res) => {
  try{
    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!id) return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    
    // OBTENER ESCUELA MEDIANTE ID
    const escuela = await escuelas.getEscuela( id ).then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Escuela obtenida', escuela });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


exports.getEscuelasAcceso = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    const { idescuela, idequipo } = req.body;

    // VALIDACION DE LOS ELEMENTOS REQUERIDOS
    if( !idescuela ) return res.status( 400 ).send({ message: 'Se necesita idescuela' });
    if( !idequipo ) return res.status( 400 ).send({ message: 'Se necesita idequipo' });
    
    // OBTENER ACCCESO MEDIANTE ESCUELA Y ACCESO
    const accesoEscuelas = await escuelas.getEscuelasAcceso( idescuela, idequipo ).then( response => response );

    if( !accesoEscuelas ) return res.status( 400 ).send({ message: 'No se encontro relación' });

    // PETICIONES A BASE DE DATOS
    const accesoNiveles = await escuelas.getNivelesEscuela( idescuela ).then( response => response );

    let idsNiveles = accesoNiveles.map( n => n.idniveles );
    idsNiveles = idsNiveles.length ? idsNiveles : [0];

    const accesoLecciones = await escuelas.getLeccionesNiveles( idsNiveles ).then( response => response );

    let idLecciones = accesoLecciones.map( l => l.idlecciones );
    idLecciones = idLecciones.length ? idLecciones : [0];

    const accesoDiapositivas = await escuelas.getDiapositivasLecciones( idLecciones ).then( response => response );

    const accesoArchivos = await escuelas.getArchivosEscuela( idescuela ).then( response => response );

    // IMPRESION DE RESPUESTA AL USUARIO

    for( const i in accesoDiapositivas ){
      const { idarchivos, portada } = accesoDiapositivas[i]
      accesoDiapositivas[i]['archivos'] = accesoArchivos.filter( f => f.idarchivos === idarchivos );
      accesoDiapositivas[i]['portada'] = accesoArchivos.filter( f => f.idarchivos === portada );
    }


    for( const i in accesoLecciones ){
      const { idlecciones, idarchivos } = accesoLecciones[i]
      accesoLecciones[i]['diapositivas'] = accesoDiapositivas.filter( d => d.idlecciones === idlecciones );
      accesoLecciones[i]['archivos'] = accesoArchivos.filter( f => f.idarchivos === idarchivos );
    }

    for( const i in accesoNiveles ){
      const { idniveles, idarchivos } = accesoNiveles[i]
      accesoNiveles[i]['lecciones'] = accesoLecciones.filter( l => l.idniveles === idniveles );
      accesoNiveles[i]['archivos'] = accesoArchivos.filter( f => f.idarchivos === idarchivos );
    }

    accesoEscuelas['niveles'] = accesoNiveles

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ escuela: accesoEscuelas });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};