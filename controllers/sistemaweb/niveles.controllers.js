const niveles = require("../../models/sistemaweb/niveles.model.js");

// AGREGAR NIVEL
exports.addNivel = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
    return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }
    
    // AGREGAR EL NIVEL
    const nivel = await niveles.addNivel( req.body ).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Nivel generado correctamente', nivel });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR EL NIVEL O BIEN ELIMINARLO
exports.updateNivel = async (req, res) => {
  try{

    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // ACTUALIZAR EL NIVEL
    const nivelActualizado = await niveles.updateNivel( req.body, id ).then( response=> response );

    res.send({ message: 'Nivel actualizado correctamente', nivelActualizado });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};
  
  
// OBTENER TODOS LOS NIVELES
exports.getNiveles = async (req, res) => {
  try{

    const { id } = req.params
    
    // OBTENER NIVELES
    const nivelesAll = await niveles.getNiveles( id ).then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Todos los niveles', nivelesAll });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};
  
  
// OBTENER NIVEL
exports.getNivel = async (req, res) => {
  try{

    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!id) return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    
    // OBTENER NIVEL MEDIANTE ID
    const nivel = await niveles.getNivel( id ).then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Nivel obtenido', nivel });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};