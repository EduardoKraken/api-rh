// IMPORTACIONES
const { response } = require('express');
const fs = require('fs');

const { agregarArchivo } = require('../../helpers/subir_archivos');
const archivos = require("../../models/sistemaweb/archivos.model.js");

// AGREGAR ARCHIVO
exports.addArchivo = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if( !req.body ){
      return res.status( 400 ).send({ message : 'El contenido del body no puede ir vacio' });
    }

    const { idescuelas, descripcion, url, tipo_reproduccion } = req.body;

    // ASIGNACION AUTOMATICA DE INTERNO VALIDADNDO POR SI URL EXISTE
    const interno = url && url.trim() !== '' ? 0 : 1;

    // SI VIENE UN URL INSERTAR CON ESTOS DATOS
    if( url ){
      const payload = { idescuelas, url, tipo_reproduccion, interno };
      const ArchivoUrl = await archivos.addArchivo( payload );
      return res.send({ message: 'Archivo agregado correctamente', id: ArchivoUrl.id, url });
    }

    // VALIDACIÓN DE QUE HAYA UN ARCHIVO EN EL REQ.FILES
    if( !req.files || !req.files.file ){
      return res.status( 400 ).send({ message : 'El contenido no puede ir vacio' });
    }

    const { file } = req.files;
    
    // EXTENSIONES VÁLIDAS PARA CARGAR ARCHIVOS
    const extensiones = ['BMP', 'GIF', 'jpg', 'jpeg', 'png', 'webp', 'PNG', 'JPG', 'JPEG', 'gif', 'bmp', 'mp4', 'avi', 'mov','mp3'];

    // AGREGAR EL ARCHIVO
    const archivo = await agregarArchivo( file, extensiones );

    // VALIDACION DE ARCHIVO
    if( !archivo ) return res.status( 400 ).send({ message : 'El archivo no pudo ser creado' });

    const { nombreUuid, extensionArchivo } = archivo;

    const payload = { idescuelas, nombreUuid, descripcion, extensionArchivo, url, tipo_reproduccion, interno }

    const archivoAgregado = await archivos.addArchivo( payload );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Archivo generado correctamente', id: archivoAgregado.id, nombre: nombreUuid });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ELIMINARLO EL ARCHIVO
exports.deleteArchivo = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body) return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });

    // OBTENER NOMBRE DEL ARCHIVO
    const { nombre_archivo, url } = req.body;

    if( !nombre_archivo && !url ) return res.status( 400 ).send({ message: 'Es necesario un nombre o un url a eliminar' });

    if( url ) {
      const urlEliminado = await archivos.deleteArchivoUrl( url );
      return res.send({ message: 'Url eliminado correctamente', url: urlEliminado.url });
    }

    // ELIMINAR EL ARCHIVO
    const archivoEliminado = await archivos.deleteArchivo( nombre_archivo );

    if( !archivoEliminado ) return res.send({ message: 'El archivo no se pudo eliminar correctamente de la base de datos' });

    try {
      fs.unlinkSync(`./galeria/${ nombre_archivo }`);
      res.send({ message: 'Archivo eliminado correctamente', archivo: archivoEliminado.nombre });
    } catch (error) {
      if( error.code === 'ENOENT' ) 
        return res.status(404).send({ message: 'El archivo no se encuentra en la galeria' });
      throw error;
    }

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al eliminar el archivo, comunícate con sistemas' } );
  }
};


// MOSTRAR LA GALERÍA
exports.getArchivos = async (req, res) => {
  try{

    // LEER ARCHIVOS DE LA RUTA O SERVIDOR
    let filenames = fs.readdirSync(`./galeria/`);
    
    const archivos = [];

    filenames.forEach((file) => {
      archivos.push({ nombre_archivo: file });
    });

    return res.send(archivos);

  }catch( error ){
    res.status( 500 ).send( { message: 'Error al eliminar el fichero', error } );
  }
};


// AGREGAR ARCHIVO
exports.addArchivoGaleria = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE HAYA UN ARCHIVO EN EL REQ.FILES
    if( !req.files || !req.files.file ){
      return res.status( 400 ).send({ message : 'El contenido no puede ir vacio' });
    }

    const { file } = req.files;
    
    // EXTENSIONES VÁLIDAS PARA CARGAR ARCHIVOS
    const extensiones = ['BMP', 'GIF', 'jpg', 'jpeg', 'png', 'webp', 'PNG', 'JPG', 'JPEG', 'gif', 'bmp', 'mp4', 'avi', 'mov'];

    // AGREGAR EL ARCHIVO
    const archivo = await agregarArchivo( file, extensiones );

    // VALIDACION DE ARCHIVO
    if( !archivo ) return res.status( 400 ).send({ message : 'El archivo no pudo ser creado' });

    const { nombreUuid, extensionArchivo } = archivo;

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Archivo generado correctamente', nombre: nombreUuid });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};