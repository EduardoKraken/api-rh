const diapositivas = require("../../models/sistemaweb/diapositivas.model.js");

// AGREGAR DIAPOSITIVA
exports.addDiapositiva = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    const { idarchivos, idlecciones, diapositiva, portada } = req.body;

    const portadaDefault = 0;
    const portadaUsar = portada ?? portadaDefault;
    
    // AGREGAR LA DIAPOSITIVA
    const diapositivaNueva = await diapositivas.addDiapositiva({ idarchivos, idlecciones, diapositiva, portada: portadaUsar }).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Diapositiva generada correctamente', diapositiva: diapositivaNueva });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR LA DIAPOSITIVA O BIEN ELIMINARLA
exports.updateDiapositiva = async (req, res) => {
  try{

    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // ACTUALIZAR LA DIAPOSITIVA
    const diapositivaActualizada = await diapositivas.updateDiapositiva( req.body, id ).then( response=> response );

    res.send({ message: 'Diapositiva actualizada correctamente', diapositivaActualizada });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};
  
  
// OBTENER TODAS LAS DIAPOSITIVAS POR LECCION
exports.getDiapositivas = async (req, res) => {
  try{

    const { idleccion } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!idleccion) return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    
    // OBTENER DIAPOSITIVAS
    const diapositivasAll = await diapositivas.getDiapositivas( idleccion ).then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Todas las diapositivas por leccion', diapositivas: diapositivasAll });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};
  
  
// OBTENER DIAPOSITIVA
exports.getDiapositiva = async (req, res) => {
  try{

    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!id) return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    
    // OBTENER DIAPOSITIVA MEDIANTE ID
    const diapositiva = await diapositivas.getDiapositiva( id ).then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Diapositiva obtenida', diapositiva });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};