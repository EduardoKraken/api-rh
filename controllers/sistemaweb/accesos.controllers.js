const accesos = require("../../models/sistemaweb/accesos.model.js");

// AGREGAR ACCESO
exports.addAcceso = async (req, res) => {
  try{

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
    return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // DESESTRUCTURAMOS LOS DATOS A INSERTAR
    const { idequipo, idescuela } = req.body;

    if( !idequipo ) return res.status( 400 ).send({ message: 'idequipo no puede estar vacio' });

    const payload = { idequipo, idescuela }
    
    // AGREGAR ACCESO
    const acceso = await accesos.addAcceso( payload ).then( response => response );

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Acceso generado correctamente', acceso });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};


// ACTUALIZAR EL ACCESO
exports.updateAcceso = async (req, res) => {
  try{

    const { id } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // ACTUALIZAR LA ACCESO
    const accesoActualizado = await accesos.updateAcceso( req.body, id ).then( response=> response );

    res.send({ message: 'Acceso actualizado correctamente', accesoActualizado });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};
  
  
// OBTENER ACCESO POR ESCUELA
exports.getAccesos = async (req, res) => {
  try{

    const { idescuela } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!idescuela) return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    
    // OBTENER ACCESOS
    const allAccesos = await accesos.getAccesos( idescuela ).then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Todos los accesos', allAccesos });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};
  
  
// OBTENER ACCESO
exports.getAcceso = async (req, res) => {
  try{

    const { idacceso } = req.params;

    // VALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!idacceso) return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    
    // OBTENER ACCESO
    const acceso = await accesos.getAcceso( idacceso ).then( response => response );

    // RESPUESTA AL USUARIO
    res.send({ message: 'Acceso obtenido', acceso });

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } );
  }
};