var fs = require('fs');
const riesgo         = require("../../models/operaciones/riesgo.model.js");
const calificaciones = require("../../models/operaciones/calificaciones.model.js");
const catalogos      = require("../../models/lms/catalogoslms.model.js");
const conmutador     = require("../../models/prospectos/conmutador.model.js");

exports.getCiclosFast = (req,res) => {
  riesgo.getCiclosFast((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los clientes"
      });
    else res.send(data);
  });
};

exports.getCiclosInbi = (req,res) => {
  riesgo.getCiclosInbi((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los clientes"
      });
    else res.send(data);
  });
};


exports.getRiesgoAlumno = async (req, res) => {
  const idCiclo = req.body.ciclo
  let data = []

  const alumnosActivos = await riesgo
    .getAlumnosFAST(idCiclo)
    .then((response) => response)

  const alumnos = await riesgo
    .getRiesgoAlumnosFast(idCiclo)
    .then((response) => response)

  const asistencias = await riesgo
    .asistenciasFast(idCiclo)
    .then((response) => response)

  for(const i in alumnos){

    // Crear el payload a cargar
    let payload = {
      ...alumnos[i],
      asistencia:   0,
      falta:        0,
      retardo:      0,
      justificada:  0
    }
      
    let countAsistencia = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 1 
    })


    let countRetardo = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 3
    })

    let countFalta = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo&&
      alumno.valor_asistencia  == 2
    })

    let countJustificada =  asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 4
    })

    payload.asistencia  = countAsistencia.length
    payload.falta       = countFalta.length
    payload.retardo     = countRetardo.length
    payload.justificada = countJustificada.length

    data.push(payload)
  }

  // Ahora revisamos los que nooo tienen actividades
  for(const i in alumnosActivos){
    let payload = {}
    const alumno = alumnos.find(el=> el.id_alumno == alumnosActivos[i].id_alumno)
    
    if(!alumno){
      // Crear el payload a cargar
      payload = {
        matricula:               alumnosActivos[i].matricula,
        nombre:                  alumnosActivos[i].nombre,
        id_alumno:               alumnosActivos[i].id_alumno,
        valor_asistencia:        alumnosActivos[i].valor_asistencia,
        telefono:                0,
        celular:                 0,
        id_curso:                alumnosActivos[i].id_curso,
        id_plantel:              alumnosActivos[i].id_plantel,
        valor_ejercicios:        alumnosActivos[i].valor_ejercicios,
        valor_examenes:          alumnosActivos[i].valor_examenes,
        id_grupo:                alumnosActivos[i].id_grupo,
        grupo:                   alumnosActivos[i].grupo,
        calificacion:            0,
        total_dias_laborales:    alumnosActivos[i].total_dias_laborales,
        idevaluacion:            0,
        num_examenes:            alumnosActivos[i].num_examenes,
        num_ejercicios:          alumnosActivos[i].num_ejercicios,
        tipo_evaluacion:         0,
        idCategoriaEvaluacion:   0,
        diferencia:              0,
        asistencia:              0,
        falta:                   0,
        retardo:                 0,
        justificada:             0,
        primera:                 '',
        segunda:                 '',
      }
    }

    let countAsistencia = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 1 
    })


    let countRetardo = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 3
    })

    let countFalta = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo&&
      alumno.valor_asistencia  == 2
    })

    let countJustificada =  asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 4
    })

    payload.asistencia  = countAsistencia.length
    payload.falta       = countFalta.length
    payload.retardo     = countRetardo.length
    payload.justificada = countJustificada.length

    data.push(payload)
  }

  res.send(data);
};

exports.getRiesgoAlumnoInbi = async (req, res) => {
  const idCiclo = req.body.ciclo
  let data = []

  const alumnosActivos = await riesgo
    .getAlumnosINBI(idCiclo)
    .then((response) => response)

  const alumnos = await riesgo
    .getRiesgoAlumnosInbi(idCiclo)
    .then((response) => response)

  const asistencias = await riesgo
    .asistenciasInbi(idCiclo)
    .then((response) => response)


  for(const i in alumnos){

    // Crear el payload a cargar
    let payload = {
      ...alumnos[i],
      asistencia:   0,
      falta:        0,
      retardo:      0,
      justificada:  0
    }
      
    let countAsistencia = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 1 
    })


    let countRetardo = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 3
    })

    let countFalta = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo&&
      alumno.valor_asistencia  == 2
    })

    let countJustificada =  asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 4
    })

    payload.asistencia  = countAsistencia.length
    payload.falta       = countFalta.length
    payload.retardo     = countRetardo.length
    payload.justificada = countJustificada.length

    data.push(payload)
  }

  // Ahora revisamos los que nooo tienen actividades
  for(const i in alumnosActivos){
    let payload = {}
    const alumno = alumnos.find(el=> el.id_alumno == alumnosActivos[i].id_alumno)
    
    if(!alumno){
      // Crear el payload a cargar
      payload = {
        nombre:                  alumnosActivos[i].nombre,
        id_alumno:               alumnosActivos[i].id_alumno,
        valor_asistencia:        alumnosActivos[i].valor_asistencia,
        telefono:                0,
        celular:                 0,
        id_curso:                alumnosActivos[i].id_curso,
        id_plantel:              alumnosActivos[i].id_plantel,
        valor_ejercicios:        alumnosActivos[i].valor_ejercicios,
        valor_examenes:          alumnosActivos[i].valor_examenes,
        id_grupo:                alumnosActivos[i].id_grupo,
        grupo:                   alumnosActivos[i].grupo,
        calificacion:            0,
        total_dias_laborales:    alumnosActivos[i].total_dias_laborales,
        idevaluacion:            0,
        num_examenes:            alumnosActivos[i].num_examenes,
        num_ejercicios:          alumnosActivos[i].num_ejercicios,
        tipo_evaluacion:         0,
        idCategoriaEvaluacion:   0,
        diferencia:              0,
        asistencia:              0,
        falta:                   0,
        retardo:                 0,
        justificada:             0,
      }
    }

    let countAsistencia = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 1 
    })


    let countRetardo = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 3
    })

    let countFalta = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo&&
      alumno.valor_asistencia  == 2
    })

    let countJustificada =  asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 4
    })

    payload.asistencia  = countAsistencia.length
    payload.falta       = countFalta.length
    payload.retardo     = countRetardo.length
    payload.justificada = countJustificada.length

    data.push(payload)
  }

  res.send(data);
};


exports.getRiesgoAlumnoRegistro = async (req, res) => {
	try{
		const{ escuela } = req.body

		const alumnosRegistro = await riesgo.getRiesgoAlumnoRegistro( escuela ).then( response => response )

		res.send(alumnosRegistro)

	}catch( error ){
		res.status( 500 ).send({message : error ? error.message : 'Error en el servidor' })
	}
};

// Crear un Historial_ticket
exports.getAlumnosRiesgoPago = async(req, res) => {
	try{
		const{ iderp } = req.body

		const alumnoRiesgoPago = await riesgo.getAlumnosRiesgoPago( iderp ).then( response => response )

		res.send(alumnoRiesgoPago)

	}catch( error ){
		res.status( 500 ).send({message : error ? error.message : 'Error en el servidor' })
	}
}

exports.getAlumnosRiesgo = async (req, res) => {
	try{
		const{ ciclo, escuela } = req.body

		const idCiclo = escuela == 1 ? ciclo.id_ciclo : ciclo.id_ciclo_relacionado

		let alumnos       = await riesgo.getAlumnos( idCiclo, escuela ).then( response => response )
		
		let asistencias   = await riesgo.getAsistencias( idCiclo, escuela ).then( response => response )
		
		let ejercicios    = await riesgo.getEjercicios( idCiclo, escuela ).then( response => response )
		
		let examenes      = await riesgo.getExamenes( idCiclo, escuela ).then( response => response )

		let respuesta = []
		// const alumnosRiesgo = await riesgo.getAlumnosRiesgo( ciclo, escuela ).then( response => response )
		var inicio    = new Date(alumnos[0].inicio); //Fecha inicial
    var fin       = new Date(alumnos[0].hoy); //Fecha final
    var timeDiff  = Math.abs(fin.getTime() - inicio.getTime());
    var diffDays  = Math.ceil(timeDiff / (1000 * 3600 * 24)); //Días entre las dos fechas
    var cuentaDom = 0; //Número de  Domingos
    var cuentaSab = 0; //Número de Sábados 
    var cuentaLMV = 0; //Número de lunes a viernes
    var cuentaLV  = 0; //Número de lunes a viernes
    var cuentaMJ  = 0; //Número de martes y jueves
    var array     = new Array(diffDays);

    // Cantidad de días que han transcurrido entre 
    for (var i=0; i < diffDays; i++){
      //0 => Domingo
      if (inicio.getDay() == 0) {
          cuentaDom++;
      }
      //0 => Domingo - 6 => Sábado
      if (inicio.getDay() == 6) {
          cuentaSab++;
      }
      if (inicio.getDay() == 1 || inicio.getDay() == 3 || inicio.getDay() == 5) {
          cuentaLMV++;
      }
      if (inicio.getDay() == 2 || inicio.getDay() == 4) {
          cuentaMJ++;
      }

      if (inicio.getDay() == 1 || inicio.getDay() == 2 || inicio.getDay() == 3 || inicio.getDay() == 4 || inicio.getDay() == 5) {
          cuentaLV++;
      }
      inicio.setDate(inicio.getDate() + 1);
    }

    for(const i in alumnos){

      let { id_curso, id_alumno, matricula } = alumnos[i]

      const diferencia_global = alumnos[i].diferencia

	    var enRiesgo = false
	    var countFalta = 0
	    var countAsistencia = 0
      var asistencias_totales = 0
      var faltas_totales = 0

      var arrayFalta = []
	    // No hubo error, si trajo a los alumnos
	    // Hacemos un ciclo para ver si tiene 2 asistencias seguidas y mandarlo al panel de riesgo
	    for(const j in asistencias){
        // Comparamos que sea el mismo usuario
        if(asistencias[j].id_usuario == alumnos[i].id){

		    	if(asistencias[j].valor_asistencia == 2){
            arrayFalta.push(asistencias[j].fecha_asistencia)
		    		countFalta += 1
            faltas_totales += 1
		    	}else{
            if(asistencias[j].valor_asistencia > 0 && asistencias[j].valor_asistencia < 4){
              asistencias_totales += 1
		    		  countFalta = 0
            }
		    	}

        }
	    }

      // Ciclo para ejercicios
      var estatus_ejercicios = 'NULO'

      var diferencia = 0
      // For para ejercicios
      for(const k in ejercicios){

        const { contestado } = ejercicios[k]

        let curso = 0

        if( escuela == 1 ){
          switch( id_curso ){
            case 7: 
              id_curso = 6
            break;

            case 10: 
              id_curso = 8
            break;

            case 11: 
              id_curso = 8
            break;

            case 13: 
              id_curso = 8
            break;
          }
        }

        // Comparamos que sea el mismo usuario
        if(ejercicios[k].id_alumno == alumnos[i].id){

          // Sacar el curso Lunes a viernes
          switch( id_curso ){

            case 2: // Lunes a viernes
              diferencia = cuentaLV - contestado

              if(diferencia_global >=  3){
                if(diferencia_global >=  15){
                  if(diferencia > 3){estatus_ejercicios='ALTO'}
                }else if(diferencia > 2){estatus_ejercicios='ALTO'}
              }
            break;

            case 4: //Lunes miercoles viernes
              diferencia = cuentaLMV - contestado
              if(diferencia > 1){estatus_ejercicios='ALTO'}
            break;

            case 6: //Sabatino
              if( diferencia_global >= 6 ){
                diferencia = cuentaSab - contestado
                if(diferencia >= 1){ estatus_ejercicios='ALTO' }
              }
            break;

            case 7: //Dominical
              if(diferencia_global >= 7){
                diferencia = cuentaDom - contestado
                if(diferencia >= 1){estatus_ejercicios='ALTO'}
              }
            break;

            case 8: //Martes y jueves
              if(diferencia_global >= 2){
                diferencia = cuentaMJ - contestado
                if(diferencia > 1){estatus_ejercicios='ALTO'}
              }
            break;
          }
        }
      }

      const found = ejercicios.find( id => id.id_alumno == alumnos[i].id  );

      if(!found){
        estatus_ejercicios = 'ALTO'
      }

      var estatus = 'NULO'
      switch(alumnos[i].id_curso){
        case 2: // Lunes a viernes
          if(diferencia_global >= 2){
            // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
            if(asistencias_totales == 0){
              estatus = 'ALTO'
            }else{
              if(faltas_totales == 0){
                estatus = 'NULO'
              }else{estatus = 'MEDIO'}
            }
          }else{
            estatus = 'NULO'
          }
        break;

        case 4: //Lunes miercoles viernes
          if(diferencia_global >= 2){
            // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
            if(asistencias_totales == 0){
              estatus = 'ALTO'
            }else{
              if(faltas_totales == 0){
                estatus = 'NULO'
              }else{estatus = 'MEDIO'}
            }
          }else{
            estatus = 'NULO'
          }
        break;

        case 6: //Sabatino
          if(diferencia_global >= 6){
            // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
            if(asistencias_totales == 0){
              estatus = 'ALTO'
            }else{
              if(faltas_totales == 0){
                estatus = 'NULO'
              }else{estatus = 'MEDIO'}
            }
          }else{
            estatus = 'NULO'
          }
        break;

        case 7: //Dominical
          if(diferencia_global >= 7){
            // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
            if(asistencias_totales == 0){
              estatus = 'ALTO'
            }else{
              if(faltas_totales == 0){
                estatus = 'NULO'
              }else{estatus = 'MEDIO'}
            }
          }else{
            estatus = 'NULO'
          }
        break;

        case 8: //Martes y jueves
          if(diferencia_global >= 4){
            // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
            if(asistencias_totales == 0){
              estatus = 'ALTO'
            }else{
              if(faltas_totales == 0){
                estatus = 'NULO'
              }else{estatus = 'MEDIO'}
            }
          }else{
            estatus = 'NULO'
          }
        break;
      }// fin del switch

      // Ciclo para examenes
      var count = 0
      var estatus_examenes  = 'NULO'
      
      if(diferencia_global >= 15){
        var existeExamen = examenes.find(exa => exa.id == alumnos[i].id && exa.calificacion > 0)
        if(existeExamen){
          estatus_examenes = 'NULO'
        }else{
          estatus_examenes = 'ALTO'
        }
      }

	    // validamos el estatus para dos faltas seguidas
	    // Lunes a viernes
      respuesta.push({
        matricula:              alumnos[i].matricula,
        id_alumno:              alumnos[i].id,
        grupo:                  alumnos[i].grupo,
        nombre:                 alumnos[i].nombre,
        curso:                  alumnos[i].id_curso,
        nivel:                  alumnos[i].id_nivel,
        estatus:                estatus,
        estatus_ejercicios:     estatus_ejercicios,
        diferencia:             diferencia,
        estatus_examenes:       estatus_examenes,
        estatus_calificaciones: 'NULO',
        estatus_registro:       'NULO',
        id_grupo:               alumnos[i].id_grupo,
        id_plantel:             alumnos[i].id_plantel,
        faltas:                 arrayFalta,
        iderp:                  alumnos[i].iderp,
        monto:                  'NI',
      })

    }

		res.send(respuesta)

	}catch( error ){
		res.status( 500 ).send({message : error ? error.message : 'Error en el servidor' })
	}
}

exports.getRecursoFalta = async (req, res) => {

  try{
    const{ iderp } = req.body

    const ejerciciosFaltantes = await riesgo.getRecursoFaltaEjercicios( req.body ).then( response => response )
    const examenesFaltantes   = await riesgo.getRecursoFaltaExamenes( req.body ).then( response => response )

    res.send({ejerciciosFaltantes, examenesFaltantes})

  }catch( error ){
    res.status( 500 ).send({message : error ? error.message : 'Error en el servidor' })
  }
}

exports.getPuntosAlumnos = async (req, res) => {
  try{
    const { idCiclo, escuela } = req.body
    let data = []

    const alumnosActivos = await riesgo.getAlumnosPuntos(idCiclo, escuela).then( response => response )

    const alumnos        = await riesgo.getRiesgoAlumnosPuntos(idCiclo, escuela).then( response => response )

    const asistencias    = await riesgo.asistenciasPuntos(idCiclo, escuela).then( response => response )

    for(const i in alumnos){

      // Crear el payload a cargar
      let payload = {
        ...alumnos[i],
        asistencia:   0,
        falta:        0,
        retardo:      0,
        justificada:  0,
      }
        
      let countAsistencia = asistencias.filter((alumno)=>{
        return alumno.id_usuario == payload.id_alumno &&
        alumno.id_grupo          == payload.id_grupo &&
        alumno.valor_asistencia  == 1 
      })


      let countRetardo = asistencias.filter((alumno)=>{
        return alumno.id_usuario == payload.id_alumno &&
        alumno.id_grupo          == payload.id_grupo &&
        alumno.valor_asistencia  == 3
      })

      let countFalta = asistencias.filter((alumno)=>{
        return alumno.id_usuario == payload.id_alumno &&
        alumno.id_grupo          == payload.id_grupo&&
        alumno.valor_asistencia  == 2
      })

      let countJustificada =  asistencias.filter((alumno)=>{
        return alumno.id_usuario == payload.id_alumno &&
        alumno.id_grupo          == payload.id_grupo &&
        alumno.valor_asistencia  == 4
      })

      payload.asistencia  = countAsistencia.length
      payload.falta       = countFalta.length
      payload.retardo     = countRetardo.length
      payload.justificada = countJustificada.length

      data.push(payload)
    }

    // Ahora revisamos los que nooo tienen actividades
    for(const i in alumnosActivos){
      let payload = {}
      const alumno = alumnos.find(el=> el.id_alumno == alumnosActivos[i].id_alumno)
      
      if(!alumno){
        // Crear el payload a cargar
        payload = {
          matricula:               alumnosActivos[i].matricula,
          nombre:                  alumnosActivos[i].nombre,
          id_alumno:               alumnosActivos[i].id_alumno,
          valor_asistencia:        alumnosActivos[i].valor_asistencia,
          telefono:                0,
          celular:                 0,
          id_curso:                alumnosActivos[i].id_curso,
          id_plantel:              alumnosActivos[i].id_plantel,
          valor_ejercicios:        alumnosActivos[i].valor_ejercicios,
          valor_examenes:          alumnosActivos[i].valor_examenes,
          id_grupo:                alumnosActivos[i].id_grupo,
          grupo:                   alumnosActivos[i].grupo,
          calificacion:            0,
          total_dias_laborales:    alumnosActivos[i].total_dias_laborales,
          idevaluacion:            0,
          num_examenes:            alumnosActivos[i].num_examenes,
          num_ejercicios:          alumnosActivos[i].num_ejercicios,
          tipo_evaluacion:         0,
          idCategoriaEvaluacion:   0,
          diferencia:              0,
          asistencia:              0,
          falta:                   0,
          retardo:                 0,
          justificada:             0,
          primera:                 '',
          segunda:                 '',
          id_nivel:                alumnosActivos[i].id_nivel,
          estado_comentario:       alumnosActivos[i].estado_comentario,
        }
      }

      let countAsistencia = asistencias.filter((alumno)=>{
        return alumno.id_usuario == payload.id_alumno &&
        alumno.id_grupo          == payload.id_grupo &&
        alumno.valor_asistencia  == 1 
      })


      let countRetardo = asistencias.filter((alumno)=>{
        return alumno.id_usuario == payload.id_alumno &&
        alumno.id_grupo          == payload.id_grupo &&
        alumno.valor_asistencia  == 3
      })

      let countFalta = asistencias.filter((alumno)=>{
        return alumno.id_usuario == payload.id_alumno &&
        alumno.id_grupo          == payload.id_grupo&&
        alumno.valor_asistencia  == 2
      })

      let countJustificada =  asistencias.filter((alumno)=>{
        return alumno.id_usuario == payload.id_alumno &&
        alumno.id_grupo          == payload.id_grupo &&
        alumno.valor_asistencia  == 4
      })

      payload.asistencia  = countAsistencia.length
      payload.falta       = countFalta.length
      payload.retardo     = countRetardo.length
      payload.justificada = countJustificada.length
      
      data.push(payload)
      
    }
    res.send(data);

  }catch( error ){
    res.status( 500 ).send({message : error ? error.message : 'Error en el servidor' })
  }
}

exports.subirCalificaciones = async (req, res) => {
  try{
    const { escuela, calificaciones } = req.body

    let idsAlumnos = calificaciones.map((registro) => { return registro.id_alumno })
    let idsGrupos  = calificaciones.map((registro) => { return registro.id_grupo })

    const calificacionesKardex = await riesgo.existeCalificacionKardex( escuela, idsAlumnos, idsGrupos ).then( response => response )

    for( const i in calificaciones ){
      let { id_curso, id_grupo, id_nivel, id_alumno, total, extra, grupo } = calificaciones[i]


      // Validar que si haya un alumno
      if( id_alumno ){
        const matchCerti = grupo.match('CERTI')
        
        if( !matchCerti ){
          // hacer validaciones completas

          total = parseFloat(total) >= 69 && parseFloat( total ) <= 70 ? 70 : parseFloat( total )

          extra = total >= 70 ? "" : extra == 0 ? '' : extra

          // VALIDAR SI YA EXISTE UNA CALIFICACION
          const existeCalificacionKardex = calificacionesKardex.find( el => el.id_grupo == id_grupo && el.id_alumno == id_alumno )

          if( existeCalificacionKardex ){
            const { id } = existeCalificacionKardex
            // Actualizar calificaciones
            const updateCalifKardex = await riesgo.updateCalifKardex( escuela, total, extra, id ).then( response => response )
          }else{
            // Agregar calificación
            const addCalifKardex = await riesgo.addCalifKardex( escuela, id_curso, id_grupo, id_nivel, id_alumno, total, extra ).then( response => response )
          }
        } 
      }
    }

    res.send({ message: 'Calificaciones cargadas correctamente '});

  }catch( error ){
    res.status( 500 ).send({message : error ? error.message : 'Error en el servidor' })
  }
}

exports.panelSeguimientoRI = async (req, res) => {
  try{
    // Caputramos los datos
    const { cicloFast, cicloInbi } = req.body

    let alumnosERPVIEJO = await riesgo.alumnosERPVIEJO( cicloFast, cicloInbi ).then( response => response )

    console.log( 'riesgo', alumnosERPVIEJO.length )

    const idAlumnosRecibor = alumnosERPVIEJO.map((registro) => { return registro.id_alumno })
    const recibosAlumnos   = await riesgo.getCantRecibos( idAlumnosRecibor ).then( response => response )
    
    let fechaEspaniol    = await calificaciones.fechaEspaniol( 1 ).then( response => response )
    fechaEspaniol        = await calificaciones.fechaEspaniol( 2 ).then( response => response )

    // Hacemos una peticion async para obtener los datos de ambas bases de datos
    const gruposAlumnosFast = await riesgo.getAlumnosPorCiclo( cicloFast, 2 ).then(response => response)
    const gruposAlumnosInbi = await riesgo.getAlumnosPorCiclo( cicloInbi, 1 ).then(response => response)

    /*********************************************
     * SACAR LO DE WHATSAPP PARA SABER SI SE LE ESTA DANDO SEGUIMIENTO AL ALUMNO O NO
    *********************************************/

    // SACAR LO DEL WHATSAPP
    const mensajesWhats  = await riesgo.mensajesWhaAlumnos( ).then(response => response)

    for( const i in alumnosERPVIEJO ){
      const { telefono } = alumnosERPVIEJO[i]

      if( telefono ){
        const existeWhatsApp  = mensajesWhats.find(  el => el.tos.match( telefono ) )

        alumnosERPVIEJO[i]['configurado']          = telefono
        alumnosERPVIEJO[i]['whatsapp']             = telefono
        alumnosERPVIEJO[i]['seguimiento_whatsApp'] = existeWhatsApp ? existeWhatsApp.diferencia_dias : 10000
        alumnosERPVIEJO[i]['ultimo_mensaje']       = existeWhatsApp ? existeWhatsApp.max_fecha       : null

      }else{
        alumnosERPVIEJO[i]['configurado']          = null
        alumnosERPVIEJO[i]['whatsapp']             = null
        alumnosERPVIEJO[i]['seguimiento_whatsApp'] = 10000
        alumnosERPVIEJO[i]['ultimo_mensaje']       = null
      }

    }


    /*********************************************
     * CALCULAR EL SI HACE O NO EJERCICIOS
    *********************************************/

    // OBTENER LOS EJERCICIOS
    let ejerciciosFast  = await riesgo.getEjerciciosCant( cicloFast, 2 ).then( response => response )
    let ejerciciosInbi  = await riesgo.getEjerciciosCant( cicloInbi, 1 ).then( response => response )

    /*********************************************
     * CALULAR LA PARTICIPACION DE LOS ALUMNOS DE FAST
    *********************************************/

    // OBTNER LAS ASISTENCIAS DE LOS ALUMNOS
    let participacionesFast   = await riesgo.getParticipacionCiclo( cicloFast, 2 ).then( response => response )
    let participacionesInbi   = await riesgo.getParticipacionCiclo( cicloInbi, 1 ).then( response => response )

    for( const i in gruposAlumnosFast ){
      const { id_grupo, id_alumno, id_curso, seguimiento_acceso_plataforma } = gruposAlumnosFast[i]


      // DATOS VACIOS
      gruposAlumnosFast[i]['datos_vacios'] = 0

      // SIN ACCESO A LA PLATAFORMA
      gruposAlumnosFast[i].datos_vacios = seguimiento_acceso_plataforma


      const participacionAlumno = participacionesFast.filter( el => el.id_alumno == id_alumno && el.id_grupo == id_grupo )

      /*
        2  Lunes a Viernes           --- desde 3
        4  Lunes miercoles viernes   --- desde 1
        6  Sabatino                  --- desde 1
        7  Dominical                 --- desde 1
        8  Martes y jueves           --- desde 1
      */
      let estatus  = 0
      let contador = 0
      let limite   = 0

      switch( id_curso ){
        case 2: limite = 3; break ;
        case 4: limite = 2; break ; 
        case 6: limite = 1; break ; 
        case 7: limite = 1; break ; 
        case 8: limite = 1; break ; 
      }

      // EVALUAR LAS PARTICIPACIONES
      let participacion = 0

      for( const i in participacionAlumno ){
        const { valor_asistencia } = participacionAlumno[i]

        // EVALUAMOS EL TIPO DE CURSO
        contador      = valor_asistencia == 2 ? contador + 1 : 0
        participacion = contador >= limite ? 1 : 0
      }

      gruposAlumnosFast[i]['participacion']       = participacion
      gruposAlumnosFast[i]['participaciones']     = participacionAlumno.length
      gruposAlumnosFast[i]['dias_sin_participar'] = contador
      gruposAlumnosFast[i].datos_vacios           = contador >= limite ? 1 : gruposAlumnosFast[i].datos_vacios


      // EVALUAR LOS EJERCICIOS
      const existenEjercicios = ejerciciosFast.find( el => el.id_alumno == id_alumno && id_grupo == id_grupo )

      if( existenEjercicios ){
        const { cant_actividades, cantidad_actual } = existenEjercicios

        let faltantes = cantidad_actual - cant_actividades
        gruposAlumnosFast[i]['faltantes']  = faltantes
        gruposAlumnosFast[i]['ejercicios'] = faltantes >= limite ? 1 : 0
        gruposAlumnosFast[i].datos_vacios  = faltantes >= limite ? 1 : gruposAlumnosFast[i].datos_vacios

      }else{
        gruposAlumnosFast[i]['faltantes']  = 'TODOS'
        gruposAlumnosFast[i]['ejercicios'] = 1
        gruposAlumnosFast[i].datos_vacios  = 1

      }


    }

    for( const i in gruposAlumnosInbi ){
      const { id_grupo, id_alumno, id_curso, seguimiento_acceso_plataforma } = gruposAlumnosInbi[i]

      // DATOS VACIOS
      gruposAlumnosInbi[i]['datos_vacios'] = 0

      // SIN ACCESO A LA PLATAFORMA
      gruposAlumnosInbi[i].datos_vacios = seguimiento_acceso_plataforma

      const participacionAlumno = participacionesInbi.filter( el => el.id_alumno == id_alumno && el.id_grupo == id_grupo )

      /*
        2   Lunes a Viernes            --- desde 3
        3   Sabatino semi-intensivo a  --- desde 1
        4   Lunes miercoles viernes    --- desde 2
        7   Sabatino                   --- desde 1
        8   Sabatino semi-intensivo b  --- desde 1
        9   Lunes a jueves             --- desde 2
        10  Teens Lunes y Miercoles    --- desde 1
        11  Teens Lunes Miercoles y Viernes --- desde 2
        12  Teens sabatino             --- desde 1
        13  Teens Martes y Jueves      --- desde 1
      */
      let estatus  = 0
      let contador = 0
      let limite   = 0

      switch( id_curso ){
        case 2: limite = 3; break ;
        case 3: limite = 1; break ; 
        case 4: limite = 2; break ; 
        case 7: limite = 1; break ; 
        case 8: limite = 1; break ; 
        case 9: limite = 2; break ; 
        case 10: limite = 1; break ; 
        case 11: limite = 2; break ; 
        case 12: limite = 1; break ; 
        case 13: limite = 1; break ; 
      }

      // EVALUAR LAS PARTICIPACIONES
      let participacion = 0

      for( const i in participacionAlumno ){
        const { valor_asistencia } = participacionAlumno[i]

        // EVALUAMOS EL TIPO DE CURSO
        contador      = valor_asistencia == 2 ? contador + 1 : 0
        participacion = contador >= limite ? 1 : 0
      }

      gruposAlumnosInbi[i]['participacion']       = participacion
      gruposAlumnosInbi[i]['participaciones']     = participacionAlumno.length
      gruposAlumnosInbi[i]['dias_sin_participar'] = contador
      gruposAlumnosInbi[i].datos_vacios           = contador >= limite ? 1 : gruposAlumnosInbi[i].datos_vacios


      // EVALUAR LOS EJERCICIOS
      const existenEjercicios = ejerciciosInbi.find( el => el.id_alumno == id_alumno && id_grupo == id_grupo )

      if( existenEjercicios ){
        const { cant_actividades, cantidad_actual } = existenEjercicios

        let faltantes = cantidad_actual - cant_actividades
        gruposAlumnosInbi[i]['ejercicios'] = faltantes >= limite ? 1 : 0
        gruposAlumnosInbi[i]['faltantes']  = faltantes
        gruposAlumnosInbi[i].datos_vacios  = faltantes >= limite ? 1 : gruposAlumnosInbi[i].datos_vacios
      }else{
        gruposAlumnosInbi[i]['ejercicios'] = 1
        gruposAlumnosInbi[i]['faltantes']  = 'TODOS'
        gruposAlumnosInbi[i].datos_vacios  = 1
      }

    }

    let respuesta = gruposAlumnosFast.concat( gruposAlumnosInbi )

    // CONMUTADOR
    let prospectosTelefonos  = alumnosERPVIEJO.map((registro) => { return registro.telefono });

    const llamadasRealizadas = await conmutador.getLlamadasTelefonosAll( prospectosTelefonos ).then( response => response )

    
    for(const i in  alumnosERPVIEJO){

      const {  id_alumno, plantel, adeudo, telefono, seguimiento_whatsApp, whatsapp, ultimo_mensaje, configurado } = alumnosERPVIEJO[i]
      
      /*
        iderp
        matricula
        nombre
        id_grupo
        id_alumno
        id_plantel
        plantel

        id_curso
        ultimo_movimiento
        escuela
        curso
        seguimiento_acceso_plataforma
        deleted
        whatsapp
        seguimiento_whatsApp
        ultimo_mensaje
        configurado
        datos_vacios
        participacion
        participaciones
        dias_sin_participar
        faltantes
        ejercicios
      */

      const existeAlumnoLMS = respuesta.find( el => el.iderp == id_alumno )


      if( existeAlumnoLMS ){
        alumnosERPVIEJO[i]                         = existeAlumnoLMS
        alumnosERPVIEJO[i]['adeudo']               = adeudo
        alumnosERPVIEJO[i]['plantel']              = plantel
        alumnosERPVIEJO[i]['seguimiento_whatsApp'] = seguimiento_whatsApp
        alumnosERPVIEJO[i]['whatsapp']             = whatsapp
        alumnosERPVIEJO[i]['telefono']             = telefono
        alumnosERPVIEJO[i]['ultimo_mensaje']       = ultimo_mensaje
        alumnosERPVIEJO[i]['configurado']          = configurado
      }else{
        let escuela2                                        = alumnosERPVIEJO[i].grupo.match('FAST') ? 2 : 1
        alumnosERPVIEJO[i]['id_curso']                      = 0
        alumnosERPVIEJO[i]['iderp']                         = alumnosERPVIEJO[i]['id_alumno']
        alumnosERPVIEJO[i]['id_alumno']                     = 0
        alumnosERPVIEJO[i]['ultimo_movimiento']             = 'SIN DATO'
        alumnosERPVIEJO[i]['escuela']                       = escuela2
        alumnosERPVIEJO[i]['curso']                         = 'NO LMS'
        alumnosERPVIEJO[i]['seguimiento_acceso_plataforma'] = 0
        alumnosERPVIEJO[i]['deleted']                       = 0
        alumnosERPVIEJO[i]['datos_vacios']                  = 1
        alumnosERPVIEJO[i]['participacion']                 = 1
        alumnosERPVIEJO[i]['participaciones']               = 0
        alumnosERPVIEJO[i]['dias_sin_participar']           = 1000
        alumnosERPVIEJO[i]['faltantes']                     = 'TODOS'
        alumnosERPVIEJO[i]['ejercicios']                    = 1
        alumnosERPVIEJO[i]['plantel']                       = plantel
      }
      
      // VER SI HAY llamda realizada y el tiempo de llamada
      const existeConmutador   = llamadasRealizadas.find( el => el.telefono == telefono )
      const totalLlamadas      = llamadasRealizadas.filter( el =>{ return el.telefono == telefono })
      const contestadas        = llamadasRealizadas.filter( el =>{ return el.telefono == telefono && el.disposition == 'ANSWERED'}).length
      const rechazadas         = llamadasRealizadas.filter( el =>{ return el.telefono == telefono && el.disposition == 'NO ANSWER'}).length
      const ocupadas           = llamadasRealizadas.filter( el =>{ return el.telefono == telefono && el.disposition == 'BUSY'}).length
      const fallidas           = llamadasRealizadas.filter( el =>{ return el.telefono == telefono && el.disposition == 'FAILED'}).length
      const mastresmin         = llamadasRealizadas.filter( el =>{ return el.telefono == telefono && el.talk_time >= 180 }).length

      alumnosERPVIEJO[i]['conmutador']  = totalLlamadas
      alumnosERPVIEJO[i]['contestadas'] = contestadas
      alumnosERPVIEJO[i]['rechazadas']  = rechazadas
      alumnosERPVIEJO[i]['ocupadas']    = ocupadas
      alumnosERPVIEJO[i]['fallidas']    = fallidas
      alumnosERPVIEJO[i]['mastresmin']  = mastresmin

      const recibosEnviados = recibosAlumnos.filter( el=> { return el.id_alumno == id_alumno })
      alumnosERPVIEJO[i]['recibos_enviados']  = recibosEnviados.length
    }


    const planteles = [...new Set( alumnosERPVIEJO.map((row) => { return row.plantel }) )]

    let mensaje  = ''
    let mensaje2 = ''

    let arregloPlantelesFast = []
    let arregloPlantelesINBI = []

    for( const i in planteles ){

      let contador       = alumnosERPVIEJO.filter( el => { return el.plantel == planteles[i] }).length

      let sinseguimiento = alumnosERPVIEJO.filter( el => { return el.plantel == planteles[i] &&  el.seguimiento_whatsApp > 3 }).length

      if( planteles[i].match('FAST')){
        arregloPlantelesFast.push({
          plantel: planteles[i],
          contador,
          sinseguimiento
        })
      }else{
        arregloPlantelesINBI.push({
          plantel: planteles[i],
          contador,
          sinseguimiento
        })
      }

    }
    
    arregloPlantelesFast.sort((a,b)=> (b.sinseguimiento - a.sinseguimiento ));
    arregloPlantelesINBI.sort((a,b)=> (b.sinseguimiento - a.sinseguimiento ));


    alumnosERPVIEJO = alumnosERPVIEJO.sort((a,b)=> ( b.seguimiento_whatsApp - a.seguimiento_whatsApp ));

    console.log( 'riesgo 2', alumnosERPVIEJO.length )


    // Envíamos la respuesta
    res.send({alumnosERPVIEJO, arregloPlantelesFast, arregloPlantelesINBI});

  }catch( error ){
    res.status( 500 ).send({message : error ? error.message : 'Error en el servidor' })
  }
}