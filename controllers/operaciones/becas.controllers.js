const becas       = require("../../models/operaciones/becas.model.js");
const { v4: uuidv4 } = require('uuid')

/*********************************************************************************************************/
/*********************************************************************************************************/


// Habilitar plataforma
exports.getBecasAll = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const listarBecas  = await becas.getBecasAll(  ).then( response => response )

    res.send(listarBecas)
  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al consultar las asistencias', error: error.message } )
  }
};



exports.getPermisosArea = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const { id } = req.params

    const listaPermisos = await becas.getPermisosArea( id ).then( response => response )

    res.send(listaPermisos)
  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al consultar las asistencias', error: error.message } )
  }
};

exports.getPermisosRh = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const listaPermisos = await becas.getPermisosRh( ).then( response => response )

    res.send(listaPermisos)
  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al consultar las asistencias', error: error.message } )
  }
};

exports.addPermiso = async(req, res) => {
  try{

  	// Si no tiene archivo se debe agregar como quiera
  	if( !req.files ){
	    const addPermisos = await becas.addPermisos( req.body ).then( response => response )
      return res.send({ message: 'Datos grabados correctamente' })
    }
    
    // desestrucutramos los arvhios cargados
    const { file } = req.files

    const nombreSplit = file.name.split('.')
    
    // Obtener la extensión del archivo 
    const extensioArchivo = nombreSplit[ nombreSplit.length - 1 ]
    
    // modificar el nombre del archivo con un uuid unico
    const nombreUuid = uuidv4() + '.' + extensioArchivo
    
    // extensiones validas
    let extensiones  = [ 'BMP', 'GIF', 'jpg', 'jpeg', 'png', 'webp' ]
    let ruta         = './../../justificante/' + nombreUuid

    // validar que la extensión del archivo recibido sea valido
    if( !extensiones.includes( extensioArchivo) )
      return res.status( 400 ).send({message: `Hola, la extensión: ${ extensioArchivo } no es valida, solo se aceptan: ${ extensiones }`})
    

    file.mv(`${ruta}`, async ( err ) => {
      if( err )
        return res.status( 500 ).send({ message: err ? err : 'Error' })

	    // Desestrucutramos los datos
	    // Haty que guardar el nombre del archivo
      try{
  	    req.body.archivo_adjunto = nombreUuid
  	    const addPermisos = await becas.addPermisos( req.body ).then( response => response )
        
        console.log( 'llegue bien')
        return res.send({ message: 'Datos grabados correctamente' })
      }catch( error ){
        return res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor', error: error.message } )   
      }
    })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor', error: error.message } )
  }
};




exports.updatePermisoRH = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const updatePermisoRH = await becas.updatePermisoRH( req.body ).then( response => response )

    return res.send({ message: 'Datos grabados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al consultar las asistencias', error: error.message } )
  }
};

exports.updatePermisoJefe = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const updatePermisoJefe = await becas.updatePermisoJefe( req.body ).then( response => response )

    return res.send({ message: 'Datos grabados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al consultar las asistencias', error: error.message } )
  }
};