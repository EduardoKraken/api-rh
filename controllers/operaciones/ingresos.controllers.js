const inscripciones = require("../../models/operaciones/inscripciones.model.js");
const ingresos      = require("../../models/operaciones/ingresos.model.js");

/*********************************************************************************************************/
/*********************************************************************************************************/

exports.ingresosPorCiclo = async(req, res) => {
  try{

  	const { id } = req.params

  	if( !id ){ return res.status( 400 ).send({ message: 'El parametro es incorrecto' }) }
    
    // Consultamos las inscripciones
    const inscripcionesAlumnos = await inscripciones.getinscripcionesCiclo( id ).then(response=> response)

  	// Consultamos los movimientos de los alumnos por el id_ciclo y los alumnos
  	const movimientos = await ingresos.getIngresosAlumnoGrupo( id ).then(response=> response)

  	// vamos a agregar a cada inscripcion sus movimientos
  	for( const i in inscripcionesAlumnos ){
  		// DESESTRUCTURACIÓN DE DATOS
  		const { id_alumno, id_grupo } = inscripcionesAlumnos[i]

  		// Agregamos los movimientos a cada inscripción
  		inscripcionesAlumnos[i]['movimientos'] = movimientos.filter( el => { return el.id_alumno == id_alumno && el.id_grupo == id_grupo })

  	}

    res.send({
    	inscripcionesAlumnos,
    	movimientos
    })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};


// Eliminar un ingreso
exports.eliminarIngreso = async(req, res) => {
  try{

    const { idingresos_reporte, id_ingreso, id_autoriza } = req.body

    if( !idingresos_reporte || !id_ingreso ){ return res.status( 400 ).send({ message: 'El parametro es incorrecto' }) }

    // Actualizar el estatus del reporte solicitado aceptado
    let estatus = 1
    const updateReporteAjuste = await ingresos.updateReporteAjuste( estatus, id_autoriza, idingresos_reporte ).then(response=> response)


    // Eliminar ahora si, el movimiento
    const eliminarIngreso = await ingresos.eliminarIngreso( id_ingreso ).then(response=> response)
    
    res.send({ message: 'El ingreso se eliminó correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.updateIngresoMetodo = async(req, res) => {
  try{

    const { idingresos_reporte, id_forma_pago, id_ingreso, id_autoriza } = req.body

    if( !id_forma_pago || !id_ingreso ){ return res.status( 400 ).send({ message: 'El parametro es incorrecto' }) }

    // Actualizar el estatus del reporte solicitado aceptado
    let estatus = 1
    const updateReporteAjuste = await ingresos.updateReporteAjuste( estatus, id_autoriza, idingresos_reporte ).then(response=> response)


    // Eliminar ahora si, el movimiento
    const updateMetodoIngreso = await ingresos.updateMetodoIngreso( id_ingreso, id_forma_pago ).then(response=> response)
    
    res.send({ message: 'El ingreso se actualizó correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};


exports.eliminarReporte = async(req, res) => {
  try{

    const { id } = req.params

    if( !id ){ return res.status( 400 ).send({ message: 'El parametro es incorrecto' }) }
    
    // Consultamos las inscripciones
    const eliminarReporte = await ingresos.eliminarReporte( id ).then(response=> response)

    res.send({ message: 'El ingreso se eliminó correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.reporteAjustePagos = async(req, res) => {
  try{

    // Consultamos las inscripciones
    const reporteAjustePagos = await ingresos.reporteAjustePagos( req.body ).then(response=> response)

    res.send({ message: 'El reporte se a generado correctamente'})

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.getReporteAjustePagosUsuario = async(req, res) => {
  try{

    const { id } = req.params 

    // Consultamos las inscripciones
    const getReporte = await ingresos.getReporteAjustePagosUsuario( id ).then(response=> response)

    res.send(getReporte)

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};


exports.getPagosDuplicados = async(req, res) => {
  try{

    // Consultamos las inscripciones
    const getPagoDuplicado = await ingresos.getPagosDuplicados( req.body  ).then(response=> response)

    res.send(getPagoDuplicado)

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.updateMontoPago = async(req, res) => {
  try{
    // Actualizar grupos alumnos
    const { id_grupo, id_alumno, monto_pagado_total, monto_saldo_favor, pago_completado_sn } = req.body
    const updateGrupoAlumno = await ingresos.updateGrupoAlumno( id_grupo, id_alumno, monto_pagado_total, monto_saldo_favor, pago_completado_sn ).then(response=> response)
 
    // Actualizar el ingreso
    const { id_ingreso, monto_pagado } = req.body
    const updateMontoPago = await ingresos.updateMontoPago( monto_pagado, id_ingreso ).then(response=> response)

    // Actualizar alumnos_grupos_especiales_carga
    const { pagado, adeudo } = req.body
    const updateAlumnosCarga = await ingresos.updateAlumnosCarga( pagado, adeudo, id_grupo, id_alumno ).then(response=> response)

    // VALIDAR SI TIENE SALDO A FAVOR
    const { nuevoSaldoFavor } = req.body
    if ( nuevoSaldoFavor > 0 ){
      const addSaldoFavor      = await inscripciones.addSaldoFavor(  id_alumno, id_grupo, nuevoSaldoFavor, 'Saldo a favor', 1  ).then( response => response )
    }

    // Actualizar el estatus del reporte solicitado aceptado
    const { id_autoriza, idingresos_reporte } = req.body
    let estatus = 1
    const updateReporteAjuste = await ingresos.updateReporteAjuste( estatus, id_autoriza, idingresos_reporte ).then(response=> response)

    res.send({ message: 'Datos actualizados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.updateMontoDescuento = async(req, res) => {
  try{
    // Actualizar grupos alumnos
    const { id_grupo, id_alumno, monto_saldo_favor, pago_completado_sn, monto_descuento_grupo } = req.body
    const updateGrupoAlumno = await ingresos.updateGrupoAlumnoDescuento( id_grupo, id_alumno, monto_saldo_favor, pago_completado_sn, monto_descuento_grupo ).then(response=> response)

    // Actualizar alumnos_grupos_especiales_carga
    const { descuento, adeudo, precio_con_descuento } = req.body
    const updateAlumnosCarga = await ingresos.updateAlumnosCargaDescuento( descuento, adeudo, precio_con_descuento, id_grupo, id_alumno ).then(response=> response)

    // VALIDAR SI TIENE SALDO A FAVOR
    const { nuevoSaldoFavor } = req.body
    if ( nuevoSaldoFavor > 0 ){
      const addSaldoFavor      = await inscripciones.addSaldoFavor(  id_alumno, id_grupo, nuevoSaldoFavor, 'Saldo a favor', 1  ).then( response => response )
    }

    // Actualizar el estatus del reporte solicitado aceptado
    const { id_autoriza, idingresos_reporte } = req.body
    let estatus = 1
    const updateReporteAjuste = await ingresos.updateReporteAjuste( estatus, id_autoriza, idingresos_reporte ).then(response=> response)

    res.send({ message: 'Datos actualizados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.reporteBajaAlumno = async(req, res) => {
  try{
    // Actualizar grupos alumnos
    let { id_grupo, id_alumno, idingresos_reporte, id_autoriza, id_ingreso, pago_anterior, id_plantel } = req.body

    id_plantel = id_plantel  ? id_plantel : 0

    // DESACTIVAR EL GRUPO ALUMNOS
    const bajaGrupoAlumnos    = await ingresos.bajaGrupoAlumnos( id_grupo, id_alumno ).then(response=> response)

    // DESACTIVAR EL ALUMNOS_CARGA_ESPECIAL
    const bajaCargaEspecial   = await ingresos.bajaCargaEspecial( id_grupo, id_alumno ).then(response=> response)

    // ELIMINAR EL SALDO A FAVOR
    const deleteSaldoFavor    = await ingresos.deleteSaldoFavor( id_grupo, id_alumno ).then(response=> response)

    // AGREGAR LA DEVOLUCIÓN
    const addEgresoDevolucion = await ingresos.addEgresoDevolucion( pago_anterior, id_autoriza, id_grupo, id_alumno, id_plantel ).then(response=> response)

    // Actualizar el estatus del reporte solicitado aceptado
    let estatus = 1
    const updateReporteAjuste = await ingresos.updateReporteAjuste( estatus, id_autoriza, idingresos_reporte ).then(response=> response)

    res.send({ message: 'Datos actualizados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};