const contratos = require("../../models/operaciones/contratos.model.js");

exports.addContrato = async(req, res) => {
  try {

  	const { id_usuario } = req.body

    // Primero sacamos a todos los grupos que están en ese ciclo y funcionando 
    const busquedaContratoTeacher       = await contratos.busquedaContratoTeacher( id_usuario ).then(response => response)

    if(busquedaContratoTeacher.length > 0){
    	res.send({message:'Usuario con contrato activo', estatus: 1});
    }else{
    	let folio = 'SCHOOL-' + id_usuario.toString().padStart(5,'0')
    	const addContratoTeacher       = await contratos.addContratoTeacher( folio, id_usuario ).then(response => response)

    	// Usuario sin contrato
    	res.send({message:'Contrato generado correctamente', estatus: 2});
    }

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getContratos = async(req, res) => {
  try {
    const getContratos       = await contratos.getContratos( ).then(response => response)
    res.send( getContratos );
  } catch (error) {
    res.status(500).send({message:error})
  }
};


exports.validaContrato = async(req, res) => {
  try {

  	const { folio } = req.body

    // Primero sacamos a todos los grupos que están en ese ciclo y funcionando 
    const existeContrato   = await contratos.existeContrato( folio ).then(response => response)

    if(existeContrato.length > 0){
    	if(existeContrato[0].estatus == 0){
    		res.send({message:'Contrato', estatus: 1, id: existeContrato[0].idfolios_contratos  });
    	}else{
    		res.send({message:'Contrago ya generado', estatus: 2});
    	}
    }else{
			res.send({message:'Folio no existe', estatus: 3});
    }

  } catch (error) {
    res.status(500).send({message:error})
  }
}


exports.actualizaDatosContrato = async(req, res) => {
  try {

  	const { nombre_completo, direccion, folio } = req.body

    // Primero sacamos a todos los grupos que están en ese ciclo y funcionando 
    const actualizaDatosContrato   = await contratos.actualizaDatosContrato( nombre_completo, direccion, folio ).then(response => response)
		res.send({message:'Actualizado', estatus: 1});

  } catch (error) {
    res.status(500).send({message:error})
  }
}
