const permisos       = require("../../models/operaciones/permisos.model.js");
const { v4: uuidv4 } = require('uuid')
const organigrama    = require("../../models/prospectos/organigrama.model.js");
const entradas       = require("../../models/catalogos/entradas.model.js");
const calificaciones = require("../../models/capacitacion/calificaciones.model.js");

/*********************************************************************************************************/
/*********************************************************************************************************/


// Habilitar plataforma
exports.getPermisosUsuario = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const { id } = req.params

    const listaPermisos  = await permisos.getPermisosUsuario( id ).then( response => response )

    res.send(listaPermisos)
  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al consultar las asistencias', error: error.message } )
  }
};


exports.getPermisosRh = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const listaPermisos = await permisos.getPermisosRh( ).then( response => response )

    res.send(listaPermisos)
  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al consultar las asistencias', error: error.message } )
  }
};

// OBTENER MIS COLABORADORES
exports.getPermisosArea = async(req, res) => {
  try {

    // Sacamos el id del usuario 
    const { id } = req.params

    // consultamos a tosos los usuarios del organigrama
    let organigramaUsuarios       = await organigrama.getOrganigrama( ).then(response => response)

    // Sacar los que dependen de mi
    // y esos son los que yo voy a evaluar

    // sacar mi idormanigrama
    const existeIdOrganigrama = organigramaUsuarios.filter( el => { return el.iderp == id })

    // Si no existen colaboradores, hay que indicarlo
    if( !existeIdOrganigrama.length ){ return res.status(400).send({ message: 'No cunetas don colaboradores' }); }

    // consultar mi idorganigrama y luego saber quienes dependen de mi 
    const idorganigramas = existeIdOrganigrama.map((registro) => { return registro.idorganigrama })

    // Sacar los colabores pid = idorganigrama
    const colaboradores = organigramaUsuarios.filter( el => { return idorganigramas.includes( el.pid ) && el.iderp < 10000 && el.iderp != id })

    // Sacamos los iderp de todos (let respuesta)
    let iderps = colaboradores.map((registro)=>{ return registro.iderp })

    iderps = iderps.length ? iderps : [0]

    // Obtener los usuarios del sistema
    const usuariosSistema = await entradas.getUsuariosERP( iderps ).then( response => response )

    // Obtener el plantel desde IDERP
    const plantel = await organigrama.getPlantel( iderps ).then( response => response ) 

    // Obtener los usuarios del Nuevo ERP
    const usuariosNuevoERP = await calificaciones.usuariosNuevoERP( ).then( response => response )

    //Recorremos la tabla organigrama
    for (const i in colaboradores){

      //Obtenemos el iderp de todos
      const { iderp } = colaboradores[i]
      //Guardamos en una varible el resultado de la consulta cuando el idusuario y el iderp coincidan (depende del nombre del id)
      const existeUsuario = usuariosSistema.find(el=>el.id_usuario == iderp)    
      const existePlantel = plantel.find(el=>el.id_usuario == iderp)
  
      let nombre             = existeUsuario   ? existeUsuario.nombre_completo : "SIN NOMBRE"
      const plantelUsuario   = existePlantel   ? existePlantel.plantel : "SIN NOMBRE"
      let existePuesto       = usuariosNuevoERP.find(el=>el.iderp == iderp)
      existePuesto           = existePuesto ? existePuesto.puesto : 'Sin puesto'

      colaboradores[i]['nombre']  = nombre + ' / ' + existePuesto + ' / ' + plantelUsuario
      colaboradores[i]['plantel'] = plantelUsuario
      colaboradores[i]['puesto']  = existePuesto
      
    }    

    // Desestrucutramos los datos
    const listaPermisos = await permisos.getPermisosArea( iderps ).then( response => response )

    // enviar los niveles
    res.send(listaPermisos);

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor'})
  }
};

exports.addPermiso = async(req, res) => {
  try{

  	// Si no tiene archivo se debe agregar como quiera
  	if( !req.files ){
	    const addPermisos = await permisos.addPermisos( req.body ).then( response => response )
      return res.send({ message: 'Datos grabados correctamente' })
    }
    
    // desestrucutramos los arvhios cargados
    const { file } = req.files

    const nombreSplit = file.name.split('.')
    
    // Obtener la extensión del archivo 
    const extensioArchivo = nombreSplit[ nombreSplit.length - 1 ]
    
    // modificar el nombre del archivo con un uuid unico
    const nombreUuid = uuidv4() + '.' + extensioArchivo
    
    // extensiones validas
    let extensiones  = [ 'BMP', 'GIF', 'jpg', 'jpeg', 'png', 'webp' ]
    let ruta         = './../../justificante/' + nombreUuid

    // validar que la extensión del archivo recibido sea valido
    if( !extensiones.includes( extensioArchivo) )
      return res.status( 400 ).send({message: `Hola, la extensión: ${ extensioArchivo } no es valida, solo se aceptan: ${ extensiones }`})
    

    file.mv(`${ruta}`, async ( err ) => {
      if( err )
        return res.status( 500 ).send({ message: err ? err : 'Error' })

	    // Desestrucutramos los datos
	    // Haty que guardar el nombre del archivo
      try{
  	    req.body.archivo_adjunto = nombreUuid
  	    const addPermisos = await permisos.addPermisos( req.body ).then( response => response )
        
        console.log( 'llegue bien')
        return res.send({ message: 'Datos grabados correctamente' })
      }catch( error ){
        return res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor', error: error.message } )   
      }
    })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor', error: error.message } )
  }
};


exports.updatePermisoRH = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const updatePermisoRH = await permisos.updatePermisoRH( req.body ).then( response => response )

    return res.send({ message: 'Datos grabados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al consultar las asistencias', error: error.message } )
  }
};

exports.updatePermisoJefe = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const updatePermisoJefe = await permisos.updatePermisoJefe( req.body ).then( response => response )

    return res.send({ message: 'Datos grabados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al consultar las asistencias', error: error.message } )
  }
};