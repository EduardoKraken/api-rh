const reprobados = require("../../models/operaciones/reprobados.model.js");
const kpi        = require("../../models/kpi/reportes.model.js");

exports.getReprobados = async(req, res) => {
  try {

  	const { cicloInbi, cicloFast } = req.params
  	
  	// Habilitamos el group
		const kpiHabilitarGroup = await kpi.habilitarGroup( ).then(response=> response)
		// Consultamos los alumnos
  	const alumnosFast = await reprobados.getAlumnosFAST( cicloFast ).then(response=> response)

  	// Consultamos la calificacion de ejercicios
  	const asistenciasFast = await reprobados.getCalifAsistencias( cicloFast ).then(response=> response)

  	// consultamos las calificaciones dde los aluimnos en los ejercicios
  	const ejercicosFast = await reprobados.getCalifEjerciciosFast( cicloFast ).then(response => response)

  	// consultamos las calificaciones dde los aluimnos en los examenes
  	const examenesFast = await reprobados.getCalifExamenesFast( cicloFast ).then(response => response)

  	// consultamos las calificaciones dde los aluimnos en los examenes
  	const examenesExtraFast = await reprobados.getCalifExtraordinarioFast( cicloFast ).then(response => response)

  	// Ahora llenamos los datos de los alumnos
  	for(const i in alumnosFast){
  		alumnosFast[i] = alumnosFast[i] 
  		const { id_alumno } = alumnosFast[i]

  		// Agregamos los puntos de las asistencias
  		const puntosAsistencia = asistenciasFast.find(el => el.id_usuario == id_alumno)
  		// Validamos que haya 
  		if(puntosAsistencia){
  			alumnosFast[i] = { ...alumnosFast[i], puntosAsistencia: puntosAsistencia.inasistencias }
  		}else{
  			alumnosFast[i] = { ...alumnosFast[i], puntosAsistencia: 30 }
  		}

  		// Agregamos los puntos de los valor_asistencia
  		const puntosEjercicios = ejercicosFast.find(el => el.id_alumno == id_alumno)
  		// Validamos que haya 
  		if(puntosEjercicios){
  			alumnosFast[i] = { 
  				...alumnosFast[i], 
  				puntosEjercicios:   puntosEjercicios.puntos, 
  				posiblesEjercicios: puntosEjercicios.posibles ,
  				cantEjercicios:     puntosEjercicios.cantEjercicios
  			}
  		}else{
  			alumnosFast[i] = { 
  				...alumnosFast[i], 
  				puntosEjercicios:   0, 
  				posiblesEjercicios: 20 ,
  				cantEjercicios:     1000
  			}
  		}

  		// Agregamos los puntos de los examenes
  		const puntosExamanes = examenesFast.find(el => el.id_alumno == id_alumno)
  		// Validamos que haya 
  		if(puntosExamanes){
  			alumnosFast[i] = { 
  				...alumnosFast[i], 
  				puntosExamanes:   puntosExamanes.puntos, 
  				posiblesExamenes: puntosExamanes.posibles ,
  				cantExamenes:     puntosExamanes.cantExamenes
  			}
  		}else{
  			alumnosFast[i] = { 
  				...alumnosFast[i], 
  				puntosExamanes:   0, 
  				posiblesExamenes: 50 ,
  				cantExamenes:     1000
  			}
  		}


  		// Agregamos los puntos de los examenes
  		const extraordinario = examenesExtraFast.find(el => el.id_alumno == id_alumno)
  		// Validamos que haya 
  		if(extraordinario){
  			alumnosFast[i] = { 
  				...alumnosFast[i], 
  				extraordinario: extraordinario.calificacion
  			}
  		}else{
  			alumnosFast[i] = { 
  				...alumnosFast[i], 
  				extraordinario: 0
  			}
  		}
  	}

  	const respuestaFinal = alumnosFast.filter(el => {
  		let totalCalif = parseFloat(el.puntosAsistencia) + parseFloat(el.puntosExamanes) + parseFloat(el.puntosEjercicios)
			if( totalCalif < 70 && el.extraordinario < 70 || totalCalif < 70 && !el.extraordinario ){
  			return true
  		}
  	})



  	/*********************************************************************************************************************************/
  	const kpiHabilitarGroupInbi = await kpi.habilitarGroupInbi( ).then(response=> response)

		// Consultamos los alumnos
  	const alumnosInbi = await reprobados.getAlumnosInbi( cicloInbi ).then(response=> response)

  	// Consultamos la calificacion de ejercicios
  	const asistenciasInbi = await reprobados.getCalifAsistenciasInbi( cicloInbi ).then(response=> response)

  	// consultamos las calificaciones dde los aluimnos en los ejercicios
  	const ejercicosInbi = await reprobados.getCalifEjerciciosInbi( cicloInbi ).then(response => response)

  	// consultamos las calificaciones dde los aluimnos en los examenes
  	const examenesInbi = await reprobados.getCalifExamenesInbi( cicloInbi ).then(response => response)

  	// consultamos las calificaciones dde los aluimnos en los examenes
  	const examenesExtraInbi = await reprobados.getCalifExtraordinarioInbi( cicloInbi ).then(response => response)

  	// Ahora llenamos los datos de los alumnos
  	for(const i in alumnosInbi){
  		alumnosInbi[i] = alumnosInbi[i] 
  		const { id_alumno } = alumnosInbi[i]

  		// Agregamos los puntos de las asistencias
  		const puntosAsistencia = asistenciasInbi.find(el => el.id_usuario == id_alumno)
  		// Validamos que haya 
  		if(puntosAsistencia){
  			alumnosInbi[i] = { ...alumnosInbi[i], puntosAsistencia: puntosAsistencia.inasistencias }
  		}else{
  			alumnosInbi[i] = { ...alumnosInbi[i], puntosAsistencia: 30 }
  		}

  		// Agregamos los puntos de los ejercios
  		const puntosEjercicios = ejercicosInbi.find(el => el.id_alumno == id_alumno)
  		// Validamos que haya 
  		if(puntosEjercicios){
  			alumnosInbi[i] = { 
  				...alumnosInbi[i], 
  				puntosEjercicios:   puntosEjercicios.puntos, 
  				posiblesEjercicios: puntosEjercicios.posibles ,
  				cantEjercicios:     puntosEjercicios.cantEjercicios
  			}
  		}else{
  			alumnosInbi[i] = { 
  				...alumnosInbi[i], 
  				puntosEjercicios:   0, 
  				posiblesEjercicios: 20 ,
  				cantEjercicios:     1000
  			}
  		}

  		// Agregamos los puntos de los examenes
  		const puntosExamanes = examenesInbi.find(el => el.id_alumno == id_alumno)
  		// Validamos que haya 
  		if(puntosExamanes){
  			alumnosInbi[i] = { 
  				...alumnosInbi[i], 
  				puntosExamanes:   puntosExamanes.puntos, 
  				posiblesExamenes: puntosExamanes.posibles ,
  				cantExamenes:     puntosExamanes.cantExamenes
  			}
  		}else{
  			alumnosInbi[i] = { 
  				...alumnosInbi[i], 
  				puntosExamanes:   0, 
  				posiblesExamenes: 50 ,
  				cantExamenes:     1000
  			}
  		}

  		// Agregamos los puntos de los examenes
  		const extraordinario = examenesExtraInbi.find(el => el.id_alumno == id_alumno)
  		// Validamos que haya 
  		if(extraordinario){
  			alumnosInbi[i] = { 
  				...alumnosInbi[i], 
  				extraordinario: extraordinario.calificacion
  			}
  		}else{
  			alumnosInbi[i] = { 
  				...alumnosInbi[i], 
  				extraordinario: 0
  			}
  		}
  	}

  	const h = alumnosInbi.find(el => el.id_alumno == 9563)
  	// console.log(h )

  	const respuestaFinalInbi = alumnosInbi.filter(el => {
			if((el.puntosAsistencia + el.puntosExamanes + el.puntosEjercicios ) < 70 && el.extraordinario < 70){
  			return true
  		}
  	})

  	const respuesta = respuestaFinal.concat(respuestaFinalInbi)
  	// Usuario sin contrato
  	res.send( respuesta );

  } catch (error) {
    res.status(500).send({message:error})
  }
};



exports.getNotasAlumnos = async(req, res) => {
  try {

  	const { id_unidad_negocio, id_grupo, id_alumno } = req.body

  	// Consultamos los usuarios activos del ERP 
  	const usuarios = await reprobados.getUsuariosERP( ).then(response=> response)
		
		if(id_unidad_negocio==2){
	  	// Consultamos la calificacion de ejercicios
	  	const notas = await reprobados.getNotasALumnoFast( id_grupo, id_alumno ).then(response=> response)
	  	for(const i in notas){
	  		const usuario = usuarios.find(el=> el.id_usuario == notas[i].id_usuario)
	  		if(usuario){
	  			notas[i] = {
	  				...notas[i],
	  				usuario: usuario.nombre_completo
	  			}
	  		}
	  	}
	  	res.send( notas );
		}else{
		 	// consultar las notas 
	  	const notas = await reprobados.getNotasALumnoInbi( id_grupo, id_alumno ).then(response=> response)
	  	for(const i in notas){
	  		const usuario = usuarios.find(el=> el.id_usuario == notas[i].id_usuario)
	  		if(usuario){
	  			notas[i] = {
	  				...notas[i],
	  				usuario: usuario.nombre_completo
	  			}
	  		}
	  	}
	  	// Usuario sin contrato
	  	res.send( notas );
		}

  } catch (error) {
    res.status(500).send({message:error})
  }
};


exports.addNotasAlumnos = async(req, res) => {
  try {
  	const { id_usuario, id_grupo, id_alumno, id_unidad_negocio, nota, tipo_usuario } = req.body
		
		if(id_unidad_negocio==2){
	  	// Consultamos la calificacion de ejercicios
	  	const notas = await reprobados.addNotaFast( id_usuario, id_grupo, id_alumno, nota, tipo_usuario ).then(response=> response)
	  	res.send( notas );
		}else{
		 	// consultar las notas 
	  	const notas = await reprobados.addNotaInbi( id_usuario, id_grupo, id_alumno, nota, tipo_usuario ).then(response=> response)
	  	// Usuario sin contrato
	  	res.send( notas );
		}

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getGruposTeacher = async(req, res) => {
  try {

  	const { email, inbi, fast } = req.body
		
  	// Consultamos la calificacion de ejercicios
  	const gruposFast = await reprobados.getGruposFastTeacher( email, fast ).then(response=> response)
  	const gruposInbi = await reprobados.getGruposInbiTeacher( email, inbi ).then(response=> response)

  	const respuesta = gruposFast.concat(gruposInbi)
  	// Usuario sin contrato
  	res.send( respuesta );

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getAlumnoProximoPago = async(req, res) => {
  try {

  	const { idfast, idinbi } = req.params
		
  	// Consultamos la calificacion de ejercicios
  	let gruposFast = await reprobados.getAlumnosFast2( idfast ).then(response=> response)
  	let gruposInbi = await reprobados.getAlumnosInbi2( idinbi ).then(response=> response)

  	let mapIdFast  = gruposFast.map((registro) => registro.id);
  	let mapIdInbi  = gruposInbi.map((registro) => registro.id);

		const alumnosFast = await reprobados.getAlumnosFechaPagoFast( mapIdFast, idfast ).then(response=> response)
  	const alumnosInbi = await reprobados.getAlumnosFechaPagoInbi( mapIdInbi, idinbi ).then(response=> response)

  	for( const i in gruposFast){
  		const { id } = gruposFast[i]
  		gruposFast[i]['fechas'] = alumnosFast.filter( el=> { return el.id_usuario == id }).map((registro) => registro.fecha_pago);
  	}

  	for( const i in gruposInbi){
  		const { id } = gruposInbi[i]
  		gruposInbi[i]['fechas'] = alumnosInbi.filter( el=> { return el.id_usuario == id }).map((registro) => registro.fecha_pago);
  	}
  	// Consultar a ver si tienen alguna fecha de pago 

  	const respuesta = {
  		gruposFast,
  		gruposInbi
  	}

  	// Usuario sin contrato
  	res.send(respuesta);

  } catch (error) {
    res.status(500).send({message:error})
  }
};