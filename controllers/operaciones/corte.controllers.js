const corte           = require("../../models/operaciones/corte.model.js");
const inscripciones   = require("../../models/operaciones/inscripciones.model.js");
const { v4: uuidv4 }  = require('uuid')
const reciboCorte     = require('../../helpers/generarReciboCorte');


/*********************************************************************************************************/
/*********************************************************************************************************/

exports.getCorteSucursal = async(req, res) => {
  try{

  	const { fecha_corte, id_plantel } = req.body
    // Desestrucutramos los datos
    let corteDiario = await corte.getCorteSucursal( fecha_corte, id_plantel ).then( response => response )

    // Consultar los saldos utilizados en ese día
    let saldosFavorUtilizados = await corte.saldosFavorUtilizados( fecha_corte ).then( response => response )

    // Vamos a sacar los idAlumnos que están en el corte
    let idAlumnosS = corteDiario.map(( registro ) => { return registro.id_alumno })

    // Ahora regresamos solo los saldos a favor de esos alumns
    saldosFavorUtilizados = saldosFavorUtilizados.filter( el => { return idAlumnosS.includes( el.id_alumno ) })

    // Ahora, en el corte diario hay que ir eliminando todos esos pagos que aparezcan ahí
    for( const i in corteDiario ){

      // sacamos el id_grupo, id_alumno y valídamos que el pago si sea
      const { id_alumno, id_grupo, monto_pagado } = corteDiario[i]


      // validamos si existe un saldo a favor utilizado o agregado
      const existeRegistro = saldosFavorUtilizados.find( el => el.id_alumno == id_alumno && el.id_grupo == id_grupo )

      let saldoUtilizado = existeRegistro ? existeRegistro.saldoFavor : 0

      corteDiario[i].monto_pagado =  monto_pagado + saldoUtilizado // es +  por que es negativo el sado a favor
    }

    // Ahora quitamos los pagos que estén en 0 para que no salgan en el corte
    corteDiario = corteDiario.filter( el => el.monto_pagado > 0 )


    for( const i in corteDiario ){ corteDiario[i]['cambio'] = '' }

    /*
			1	EFECTIVO
			2	DEPOSITO
			3	SALDAZO
			4	TARJETA
    */

    // SACAR LOS PLANTELES UNICOS

    // SACAR LAS VENDEDORAS UNICAS DE INBI
    let arrayIdPlanteles = corteDiario.map(item=>{ return [item.id_plantel,item] });
    // Creamos un map de los alumnos
    var planteles = new Map(arrayIdPlanteles); // Pares de clave y valor
    // y ahora si, extraemos los valores unicos
    let plantelesUnicos = [...planteles.values()]; // Conversión a un array
    let tablaPagosSucursal = []


    // Recorrer los planteles unicos
    for( const i in plantelesUnicos ){

    	// DESESTRUCUTRAR LOS DATOS
    	const { id_plantel, plantel } = plantelesUnicos[i]

    	// PREPARAMOS EL PAYLOD, ESTE ES LO QUE VAMOS A ENVIAR AL FRONT PERO YA BONITOS
	    const payload = {
	    	total   :      corteDiario.filter( el => { return el.id_plantel == id_plantel }).map(item => item.monto_pagado ).reduce((prev, curr) => prev + curr, 0),
	    	efectivo:      corteDiario.filter( el => { return el.id_forma_pago == 1 && el.id_plantel == id_plantel }).map(item => item.monto_pagado).reduce((prev, curr) => prev + curr, 0),
	    	transferencia: 0,
	    	depositos:     corteDiario.filter( el => { return el.id_forma_pago == 2 && el.id_plantel == id_plantel }).map(item => item.monto_pagado).reduce((prev, curr) => prev + curr, 0),
	    	tarjeta:       corteDiario.filter( el => { return el.id_forma_pago == 4 && el.id_plantel == id_plantel }).map(item => item.monto_pagado).reduce((prev, curr) => prev + curr, 0),
	    	plantel,
	    	id_plantel
	    }

	    tablaPagosSucursal.push( payload )
	    // LO AGREGAMOS A LA TABLA DE LOS PLANTELES PARA SABER SI RECIBIÓ PAGOS DE OTRO LADO
    }


    tablaPagosSucursal.push({
    // AGREGAR EL TOTAL A LA TABLA
    	total   :      corteDiario.map(item => item.monto_pagado ).reduce((prev, curr) => prev + curr, 0),
    	efectivo:      corteDiario.filter( el => { return el.id_forma_pago == 1 }).map(item => item.monto_pagado).reduce((prev, curr) => prev + curr, 0),
    	transferencia: 0,
    	depositos:     corteDiario.filter( el => { return el.id_forma_pago == 2 }).map(item => item.monto_pagado).reduce((prev, curr) => prev + curr, 0),
    	tarjeta:       corteDiario.filter( el => { return el.id_forma_pago == 4 }).map(item => item.monto_pagado).reduce((prev, curr) => prev + curr, 0),
    	plantel:       'TOTAL',
    	id_plantel:    1000
    })


    //****************** PROCESO PARA VER LO DE LOS CAMBIOS ***********************************//

    // CONSULTAMOS LOS CAMBIOS EN GENERAL QUE SE HICIERON EN ESE DÍA, INDEPENDIENTEMENTE 
    const cambiosEgresos = await corte.getCambiosCorte( fecha_corte ).then( response => response )

    // RECORREMOS LOS CAMBIOS REALIZADOS
    for( const i in cambiosEgresos ){

      // SACAMOS EL COMENTARIO, AHÍ DICE DE QUÉ GRUPO A QUÉ GRUPO SE MOVIO
      const { comentarios } = cambiosEgresos[i]

      // CONVERTIMOS EL COMENTARIO EN UN ARRAY 
      const datosGrupos = comentarios.split('->')

      // ELIMINARMOS TODO LO QUE NO SEA NÚMERO Y DEJAMOS SOLO EL ID DEL NUEVO GRUPO
      cambiosEgresos[i]['id_nuevo_grupo'] = datosGrupos[0].replace(/[^0-9.]/g, '').trim() 

      // DEJAMOS SOLO EL GRUPO VIEJITO, ELIMINAMOS TODO LO QUE NO SEA NUMEROS
      cambiosEgresos[i]['id_grupo_viejo'] = datosGrupos[1].replace(/[^0-9.]/g, '').trim() 

    }

    // VAMOS A SACAR LOS IDS DE ESOS GRUPOS
    let idsNuevoGrupo = cambiosEgresos.map((registro) => { return registro.id_nuevo_grupo })
    let idsViejoGrupo = cambiosEgresos.map((registro) => { return registro.id_grupo_viejo })

    // Los validamos para no llevar arreglos vacios
    idsNuevoGrupo = idsNuevoGrupo.length ? idsNuevoGrupo : [0]
    idsViejoGrupo = idsViejoGrupo.length ? idsViejoGrupo : [0]

    // Vamos a consultar ahora los nombres de esos grupos
    const gruposCambio = await corte.getGruposCambio( idsNuevoGrupo, idsViejoGrupo ).then( response => response )

    // CONSULTAR LOS ALUMNOS EN EL CORTE QUE TUVIERON UN CAMBIO DE GRUPO
    let idAlumnos = cambiosEgresos.map(( row ) => { return row.id_alumno })
    idAlumnos = idAlumnos.length ? idAlumnos : [0]

    // CONSULTAMOS EL CORTE DE ESOS ALUMNOS Y DE ESE PLANTEL Y.... DEL GRUPO NUEVO
    const cambiosAlumnos = await corte.getCorteSucursalCambios( idAlumnos, id_plantel, idsNuevoGrupo ).then( response => response )

    // Recorremos el corteee
    for( const i in cambiosAlumnos ){
      const { id_alumno, id_grupo } = cambiosAlumnos[i]

      // VERIFICAR LOS GRUPOS, EL VIEJO Y EL NUEVO, PONERLES EL CICLO PLANTEL DE CADA GRUPO 
      const existeCambio     = cambiosEgresos.find( el => el.id_alumno == id_alumno && el.id_nuevo_grupo == id_grupo )

      // Ahora que nos aseguramos que ya existe, es hora de validar cada grupo
      if( existeCambio ){

        const { id_nuevo_grupo, id_grupo_viejo } = existeCambio

        const existeNuevoGrupo = gruposCambio.find( el => el.id_grupo == id_nuevo_grupo )
        const existeViejoGrupo = gruposCambio.find( el => el.id_grupo == id_grupo_viejo )

        // Ahora llenamos los datossss

        // Nuevos datos
        cambiosAlumnos[i]['nuevoPlantel'] = existeNuevoGrupo ? existeNuevoGrupo.plantel : ''
        cambiosAlumnos[i]['nuevoCiclo']   = existeNuevoGrupo ? existeNuevoGrupo.ciclo   : ''

        // Datos anteriores
        cambiosAlumnos[i]['viejoPlantel'] = existeViejoGrupo ? existeViejoGrupo.plantel : ''
        cambiosAlumnos[i]['viejoCiclo']   = existeViejoGrupo ? existeViejoGrupo.ciclo   : ''

      }

      cambiosAlumnos[i]['cambio'] = cambiosAlumnos ? 'cambio_grupo' : ''
    }

    const cambiosGrupo = cambiosAlumnos.filter( el => { return el.cambio })

    // Ya tenemos a los alumnos que realizaron el cambio de grupo de ese plantelll
    /*
			C I F R A S 
    */

    // CIFRAS DEL DÍA DE HOY
    const cifrasActuales = await corte.getCifrasActuales( fecha_corte, id_plantel ).then( response => response )

    // Obtener el saldo a favor de los ciclos
    const saldosFavorCorte = await corte.getSaldosFavorCiclo( fecha_corte ).then( response => response )

    // Saldo a favor del corte del día de ayer
    const saldosFavorCorteAyer = await corte.getSaldosFavorCicloAyer( fecha_corte ).then( response => response )

    
    // CIFRAS HASTA EL DÍA DE AYER
    const cifrasAyer     = await corte.getCifrasAyer( fecha_corte, id_plantel ).then( response => response )

    // RECORREMOS LAS CIFRAS ACTUALES, PARA PODER SACAR POR PLANTELLLL LA CIFRA INICIAR, LA CIFRA FINAL Y LA DIFERENCIA, PARA PODER HACER EL CORTE
    for( const i in cifrasActuales ){
    	const { id_ciclo, suma, id_plantel } = cifrasActuales[i]

      // VER SI EXISTE UNA CIFRA EL DÍA DE AYER POR PLANTEL
    	const existeCifra = cifrasAyer.find( el => el.id_ciclo == id_ciclo && el.id_plantel == id_plantel )

      // SUMA DE SALDOS A FAVOR 
      const saldoFavor      = saldosFavorCorte.filter( el => el.id_ciclo == id_ciclo && el.id_plantel == id_plantel ).map(item => item.saldoFavor ).reduce((prev, curr) => prev + curr, 0).toFixed( 2 ) 

      // SUMA DE SALDOS A FAVOR DE AYER
      const saldoFavorAyer  = saldosFavorCorteAyer.filter( el => el.id_ciclo == id_ciclo && el.id_plantel == id_plantel ).map(item => item.saldoFavor ).reduce((prev, curr) => prev + curr, 0).toFixed( 2 ) 

      // SACAMOS LA SUMNA, SI LA CIFRA NO EXISTE, LA CIFRA INICIARL, ´SERÍA 0
      const cifra_inicial = existeCifra ? ( existeCifra.suma ) : 0
    	// const cifra_inicial = existeCifra ? ( existeCifra.suma + parseFloat( saldoFavorAyer ) ) : 0

    	cifrasActuales[i]['cifra_inicial'] = cifra_inicial 

    	cifrasActuales[i]['cifra_final']   = ( suma + parseFloat( saldoFavor ) )

    	cifrasActuales[i]['diferencia']    = ( suma + parseFloat( saldoFavor ) ) - cifra_inicial 

    }

    /*
			DIFERENCIA GLOBAL
    */

    // ESTAS CIFRAS SON LAS QUE SE LE MUESTRAN A LA VENDEDORA
    const corteDiferencias  = await corte.getDiferenciasCorte( fecha_corte, id_plantel ).then( response => response )

    // LA DIFERENCIA TOTAL, ES LA DIFERENCIA ENTRE LO QUE HABÍA AYER Y LO QUE HAY EL DÍA DE HOY
    const diferencia_total  = cifrasActuales.map(item => item.diferencia).reduce((prev, curr) => prev + curr, 0).toFixed( 2 )

    const corte_plantel     = corteDiario.map(item => item.monto_pagado ).reduce((prev, curr) => prev + curr, 0).toFixed( 2 )
    const pagos_otra_suc    = corteDiario.filter( el => { return el.id_plantel != id_plantel }).map(item => item.monto_pagado ).reduce((prev, curr) => prev + curr, 0).toFixed( 2 )
    const pagos_propios     = corteDiario.filter( el => { return el.id_plantel == id_plantel }).map(item => item.monto_pagado ).reduce((prev, curr) => prev + curr, 0).toFixed( 2 )
    const pagos_de_otra_suc = corteDiferencias.filter( el => { return el.id_plantel != id_plantel }).map(item => item.monto_pagado ).reduce((prev, curr) => prev + curr, 0).toFixed( 2 )

    // Desestrucutramos los datos

    /*
			1	EFECTIVO
			2	DEPOSITO
			3	SALDAZO
			4	TARJETA
    */

    // SACAR LOS PLANTELES UNICOS

    // SACAR LAS VENDEDORAS UNICAS DE INBI
    let arrayIdPlanteles2 = corteDiferencias.map(item=>{ return [item.id_plantel,item] });
    // Creamos un map de los alumnos
    var planteles2 = new Map(arrayIdPlanteles2); // Pares de clave y valor
    // y ahora si, extraemos los valores unicos
    let plantelesUnicos2 = [...planteles2.values()]; // Conversión a un array

    let pagosDesdeOtraSucursal = []
    // Recorrer los planteles unicos
    for( const i in plantelesUnicos2 ){

    	// DESESTRUCUTRAR LOS DATOS
    	const { id_plantel, plantel } = plantelesUnicos2[i]

    	// PREPARAMOS EL PAYLOD, ESTE ES LO QUE VAMOS A ENVIAR AL FRONT PERO YA BONITOS
	    const payload = {
	    	total   :      corteDiferencias.filter( el => { return el.id_plantel == id_plantel }).map(item => item.monto_pagado ).reduce((prev, curr) => prev + curr, 0),
	    	efectivo:      corteDiferencias.filter( el => { return el.id_forma_pago == 1 && el.id_plantel == id_plantel }).map(item => item.monto_pagado).reduce((prev, curr) => prev + curr, 0),
	    	transferencia: 0,
	    	depositos:     corteDiferencias.filter( el => { return el.id_forma_pago == 2 && el.id_plantel == id_plantel }).map(item => item.monto_pagado).reduce((prev, curr) => prev + curr, 0),
	    	tarjeta:       corteDiferencias.filter( el => { return el.id_forma_pago == 4 && el.id_plantel == id_plantel }).map(item => item.monto_pagado).reduce((prev, curr) => prev + curr, 0),
	    	plantel,
	    	id_plantel
	    }

	    // LO AGREGAMOS A LA TABLA DE LOS PLANTELES PARA SABER SI RECIBIÓ PAGOS DE OTRO LADO
	    pagosDesdeOtraSucursal.push( payload )
    }


    // AGREGAR EL TOTAL A LA TABLA
    pagosDesdeOtraSucursal.push({
    	total   :      corteDiferencias.map(item => item.monto_pagado ).reduce((prev, curr) => prev + curr, 0),
    	efectivo:      corteDiferencias.filter( el => { return el.id_forma_pago == 1 }).map(item => item.monto_pagado).reduce((prev, curr) => prev + curr, 0),
    	transferencia: 0,
    	depositos:     corteDiferencias.filter( el => { return el.id_forma_pago == 2 }).map(item => item.monto_pagado).reduce((prev, curr) => prev + curr, 0),
    	tarjeta:       corteDiferencias.filter( el => { return el.id_forma_pago == 4 }).map(item => item.monto_pagado).reduce((prev, curr) => prev + curr, 0),
    	plantel:       'TOTAL',
    	id_plantel:    1000
    })

    // VALIDAR SI LAS DIFERENCIAS ESTAN CORRECTAS O NO
    const diferencia_global = parseFloat(diferencia_total) - ( parseFloat(pagos_propios) +  parseFloat(pagos_de_otra_suc) )

    const cifra_inicial = cifrasActuales.map(item => item.cifra_inicial ).reduce((prev, curr) => prev + curr, 0)
    const cifra_final   = cifrasActuales.map(item => item.cifra_final ).reduce((prev, curr) => prev + curr, 0)


    // Sacar los alumnos unicos
    // SACAR LAS VENDEDORAS UNICAS DE FAST
    let arrayCiclos = cifrasActuales.map(item=>{ return [item.id_ciclo,item] });
    // Creamos un map de los alumnos
    var ciclos = new Map(arrayCiclos); // Pares de clave y valor
    // y ahora si, extraemos los valores unicos
    let ciclosUnicos = [...ciclos.values()]; // Conversión a un array


    // VAMOS A DIVIDIR LOS PAGOS EN CICLOS PARA QUE SEA MÁS FÁCIL VERLOS

    // SACAMOS LOS CICLOS ÚNICOS DEL CORTE
    const ciclosPorCorte = corteDiario.filter((c, index, self) =>
      index === self.findIndex((p) => (
        p.ciclo === c.ciclo
      ))
    ).map(c => ({ id: c.ciclo, desglose: [], total: 0 }));

    // Ahora lo llenamos
    for( const i in ciclosPorCorte ){

      // Desestrucutramos y samos nuestro identificador
      const { id } = ciclosPorCorte[i]

      // Llenamos el arreglo vacio que sacamos anteriormente
      ciclosPorCorte[i].desglose = corteDiario.filter( el  => { return el.ciclo == id })
      ciclosPorCorte[i].total    = corteDiario.filter( el  => { return el.ciclo == id }).map(item => item.monto_pagado).reduce((prev, curr) => prev + curr, 0)

    }

    // PREPARAMOS LA RESPUESTA
    const respuesta = {
      cambiosEgresos,
    	cifra_inicial,
			cifra_final,
    	diferencia_total,
			corte_plantel,
			pagos_otra_suc,
			pagos_propios,
			pagos_de_otra_suc,
			diferencia_global,
      ciclosUnicos,
    	cifras:        cifrasActuales,

    	pagosDesdeOtraSucursal,
    	tablaPagosSucursal,
      tablaMovimientos: corteDiario,

      cambios:       cambiosGrupo,
    	desglose:      ciclosPorCorte,
      num_pagos:     corteDiario.length
    }

    res.send( respuesta )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.getMovimientosCorte = async(req, res) => {
  try{

    const { fecha_inicio, fecha_final, id_ciclo } = req.body

    if( fecha_inicio && !id_ciclo ){
      let corteDiario = await corte.getMovimientosCorteFecha( fecha_inicio, fecha_final ).then( response => response )
      res.send( corteDiario )
    }

    if( !fecha_inicio && id_ciclo ){
      let corteDiario = await corte.getMovimientosCorteCiclo( id_ciclo ).then( response => response )
      res.send( corteDiario )
    }

    if( fecha_inicio && id_ciclo ){
      let corteDiario = await corte.getMovimientosCorteCicloFecha( fecha_inicio, fecha_final, id_ciclo ).then( response => response )
      res.send( corteDiario ) 
    }


  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.generarRecibo = async(req, res) => {
  try{

    const { desglose, montos, usuario, fecha, plantel } = req.body

    const folio = plantel + '-' + fecha

    reciboCorte({ 
      folio,
      usuario,
      fecha,
      plantel,
      desglose,
      montos
    })

    res.send({ message: 'Recibo generado correctamente', folio })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};


exports.getReporteIngresosCiclo = async(req, res) => {
  try{

    const { id } = req.params

    const reporte = await corte.getReporteIngresosCiclo( id ).then( response => response )

    res.send( reporte )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.updateClaveRastreo = async(req, res) => {
  try{

    const { id_ingreso, aut_clave_rastreo, folio_operacion, cuenta } = req.body

    const reporte = await corte.updateClaveRastreo( id_ingreso, aut_clave_rastreo, folio_operacion, cuenta ).then( response => response )

    res.send({ message: 'Datos actualizados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};


exports.updateFolioOperacion = async(req, res) => {
  try{

    const { id_ingreso, folio_operacion } = req.body

    const reporte = await corte.updateFolioOperacion( id_ingreso, folio_operacion ).then( response => response )

    res.send({ message: 'Datos actualizados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};


exports.updateCuentaIngreso = async(req, res) => {
  try{

    const { id_ingreso, cuenta } = req.body

    const reporte = await corte.updateCuentaIngreso( id_ingreso, cuenta ).then( response => response )

    res.send({ message: 'Datos actualizados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.updateAceptado = async(req, res) => {
  try{

    const { id_ingreso, aceptado } = req.body

    const reporte = await corte.updateAceptado( id_ingreso, aceptado ).then( response => response )

    res.send({ message: 'Datos actualizados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};