const cambios = require("../../models/operaciones/cambios.model.js");

// General
// Obtener los tipos de cambio
exports.getTipoCambios = (req, res) => {
  cambios.getTipoCambios((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};


// Recepcionista
// Obtener a los grupos del ciclo actual
exports.cambiosGruposxCiclo = (req, res) => {
  const { tipociclo } = req.body

  cambios.cambiosGruposxCiclo(tipociclo,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

exports.cambiosNuevoGruposxAlumno = (req, res) => {
  const { tipociclo, id_ciclo } = req.body

  cambios.cambiosNuevoGruposxAlumno(tipociclo,id_ciclo,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

// Obtener a los alumnos del ciclo actual y su grupo
exports.getAlumnoxGrupo = (req, res) => {
	const { tipociclo } = req.body
  cambios.getAlumnoxGrupo(tipociclo,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

exports.addCambios = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  cambios.addCambios(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al solicitar el cambio"
      })
    else res.status(200).send({ message: `El cambio se solicito correctamente` });
  })
};

// Obtener los cambios solicitados por usuario
exports.getCambiosUsuario = (req, res) => {
	const { idusuario } = req.params
  cambios.getCambiosUsuario(idusuario,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

// Obtener los cambios solicitados por usuario
exports.getCambiosAll = (req, res) => {
  cambios.getCambiosAll((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

// Actualizar el cambio
exports.updateCambios = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }
  cambios.updateCambios(req.params.id, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro el cambio con id : ${req.params.id}` });
      } else {
        res.status(500).send({ message: "Error al actualizar el cambio con el id : " + req.params.id });
      }
    } else {
      res.status(200).send({ message: `El cambio se ha actualizado correctamente.` })
    }
  })
}


exports.cambiosMensaje = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  cambios.cambiosMensaje(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al enviar el mensaje"
      })
    else res.status(200).send({ message: `El mensaje se agrego correctamente` });
  })
};

// Obtener los cambios solicitados por usuario
exports.cambiosGetMensaje = (req, res) => {
  cambios.cambiosGetMensaje(req.params.id,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

// Obtener los grupos del alumno
exports.getGruposUsuario = (req, res) => {
  const { idusuario } = req.params

  cambios.getGruposUsuario(idusuario,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

/*********************************************************************************************************/
/*********************************************************************************************************/
// RI por grupos

exports.kpiRiGrupos = async(req, res) => {
  try {
    // Primero sacamos a todos los grupos que están en ese ciclo y funcionando 
    const getGruposActual       = await cambios.getGruposActual(req.params.inbi,req.params.fast).then(response => response)

    // Ahora a los alumnos de esos grupos
    const alumosCicloActual     = await cambios.alumosCicloActual(req.params.inbi,req.params.fast).then(response => response)

    // Consultamos los alumnos que están en
    const alumosCicloSiguiente  = await cambios.alumosCicloSiguiente(req.params.inbi,req.params.fast,req.params.sigInbi,req.params.sigFast).then(response => response)

    let resultadoFinal = []
    // Creado el arreglo list para recibir las cantidades
    for(const i in getGruposActual){
      let payload = {
        id_grupo                : getGruposActual[i].id_grupo,
        grupo                   : getGruposActual[i].grupo,
        alumnos_ciclo_actual    : 0,
        alumnos_siguiente_ciclo : 0,
        faltantes               : 0
      }
      resultadoFinal.push(payload)
    }

    // Sacamos los alumnos actuales
    for(const i in resultadoFinal){
      for(const j in alumosCicloActual){
        // Solo hay que valdiar que el alumno sea de ese mismo grupo
        if(resultadoFinal[i].id_grupo == alumosCicloActual[j].id_grupo){
          resultadoFinal[i].alumnos_ciclo_actual += 1
        }
      }
    }

    // Sacamos los alumnos del siguiente ciclo
    for(const i in resultadoFinal){
      for(const j in alumosCicloSiguiente){
        // Solo hay que valdiar que el alumno sea de ese mismo grupo
        if(resultadoFinal[i].id_grupo == alumosCicloSiguiente[j].id_grupo){
          resultadoFinal[i].alumnos_siguiente_ciclo += 1
        }
      }
    }

    // Sacamos los alumnos faltantes
    for(const i in resultadoFinal){
      resultadoFinal[i].faltantes = resultadoFinal[i].alumnos_ciclo_actual - resultadoFinal[i].alumnos_siguiente_ciclo
    }

    res.send(resultadoFinal);
  } catch (error) {
    res.status(500).send({message:error})
  }
};
/************************ P R U E B A ****************************/

exports.cambiosNuevoGruposxAlumnoPrueba = (req, res) => {
  const { tipociclo, id_ciclo } = req.body

  cambios.cambiosNuevoGruposxAlumnoPrueba(tipociclo,id_ciclo,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

exports.getAlumnoxGrupoPrueba = (req, res) => {
  const { tipociclo } = req.body
  cambios.getAlumnoxGrupoPrueba(tipociclo,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

exports.getGruposUsuarioPrueba = (req, res) => {
  const { idusuario } = req.params

  cambios.getGruposUsuarioPrueba(idusuario,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

exports.addCambioGrupo = async(req, res) => {
  try {

    let { diferencia, id_alumno, id_grupo, iderp, id_grupo_nuevo, pago_grupo_actual, id_plantel, id_ciclo_nuevo_grupo, descuento_grupo_actual, precio_grupo, precio_descuento  } = req.body

    diferencia  = diferencia < 0 ? ( diferencia * -1 ) : diferencia


    // Consultar la información del grupo actual de alumno grupo carga especial. 
    const getAlumnoCargaEspecialGrupo   = await cambios.getAlumnoCargaEspecialGrupo( id_alumno, id_grupo ).then(response => response)

    // ELIMINAR EL GRUPO ACTUAL DE alumnos_grupos_especiales_carga
    const eliminarAlumnoCargaEspecial   = await cambios.eliminarAlumnoCargaEspecial( id_grupo, id_alumno ).then(response => response)

    // ELIMINAR EL GRUPO DE GRUPOSALUMNOS
    const eliminarGrupoAlumnoActual     = await cambios.eliminarGrupoAlumnoActual( id_grupo, id_alumno ).then(response => response)

    // ACTUALIZAR LOS INGRESOS
    const updateIngresos                = await cambios.updateIngresos( id_alumno, id_grupo, id_grupo_nuevo, iderp ).then(response => response)

    // INSERTAR EL EGRESO YA QUE REALMENTE ESTAMOS SACANDO EL DINERO DE AHÍ
    const insertarEgreso                = await cambios.insertarEgreso( id_grupo_nuevo, id_grupo, iderp, pago_grupo_actual, id_alumno, id_plantel ).then(response => response)

    // INSERTAR GRUPO ALUMNO
    const insertarAlumnoGrupo           = await cambios.insertarGrupoAlumno( id_grupo_nuevo, id_alumno, precio_grupo, precio_descuento, iderp ).then(response => response)

    // INSERTAR EL GRUPOALUMNO AHORA SIIIII
    const insertarGrupoAlumno           = await cambios.insertarGruposEspeciales( id_alumno, id_grupo_nuevo, id_ciclo_nuevo_grupo, pago_grupo_actual, 0, descuento_grupo_actual, precio_grupo, precio_descuento, precio_grupo  ).then(response => response)

    res.send({ message: 'Cambio de grupo exitoso' });
  } catch (error) {
    res.status(500).send({message:error})
  }
};