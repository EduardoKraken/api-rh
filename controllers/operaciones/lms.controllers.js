const lms = require("../../models/operaciones/lms.model.js");

exports.getalumnosLMS = async(req, res) => {
  try {
  	const {  idciclo, idciclo_2 } = req.params
    // Primero sacamos a todos los grupos que están en ese ciclo y funcionando 
    const getAlumnosLMSFAST       = await lms.getAlumnosLMSFAST( idciclo_2 ).then(response => response)

    // Ahora a los alumnos de esos grupos
    const getAlumnosLMSINBI     = await lms.getAlumnosLMSINBI( idciclo ).then(response => response)

    let respuesa = getAlumnosLMSFAST.concat(getAlumnosLMSINBI)
    res.send(respuesa);
  } catch (error) {
    res.status(500).send({message:error})
  }
};