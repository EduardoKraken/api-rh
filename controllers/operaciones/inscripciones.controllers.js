const inscripciones      = require("../../models/operaciones/inscripciones.model");
const becas              = require("../../models/operaciones/becas.model");
const helperReciboPago   = require("../../helpers/reciboPago.helpers.js");
const mixinInscripciones = require("../../helpers/mixinInscripciones.js");
const descuentosEspe     = require("../../models/operaciones/descuentos.model.js");
const newPDF             = require('../../helpers/generarRecibo');
const whatsapp           = require("../../models/operaciones/whatsapp.model");
const helperBienvenida   = require("../../helpers/correoBienvenida.helper.js");


const { v4: uuidv4 } = require('uuid')
const numToWords     = require('numero-a-letras');


/******************************************/
/*            ENVIAR CORREOS              */
/******************************************/
const USUARIO_CORREO  = 'staff.fastenglish@gmail.com'
const PASSWORD_CORREO = 'jzxmdixwyauwxfbf'
const ESCUELA         = 'FAST ENGLISH'
const nodemailer      = require('nodemailer');

const user = USUARIO_CORREO
const pass = PASSWORD_CORREO

// Preparamos el transporte
let transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 465, 
  secure:true,
  pool: true,
  auth: {
    user,
    pass
  }
});

transporter.verify(function (error, success) {
  if (error) {
    console.log(error);
  } else {
    console.log("Server is ready to take our messages FAST");
  }
});

const userq = 'vinculacion@inbi.mx'
const passq = 'vincu14c10n@/'

// Preparamos el transporte
let transporter2 = nodemailer.createTransport({
  host: 'inbi.mx',
  port: 465, //25, 465, 587, 
  secure:true,
  auth: {
    user: userq,
    pass: passq
  }
});

transporter2.verify(function (error, success) {
  if (error) {
    console.log(error);
  } else {
    console.log("Server is ready to take our messages INBI");
  }
});

// Obtener a todos los alumnos inscritos
exports.getinscripciones = async(req, res) => {
  try {

    const { ciclo } = req.params

    let getinscripciones = []
    let ciclosParaRI     = []
    let alumnosParaRI    = []
    let mapCiclos        = [0]
    let mapAlumnos       = [0]

  	// consultar los alumnos inscritos
    if( ciclo != 'null' ){
      getinscripciones = await inscripciones.getinscripcionesCiclo( ciclo ).then(response=> response)
      ciclosParaRI     = await inscripciones.ciclosParaRI( ciclo ).then(response=> response)

      mapCiclos  = ciclosParaRI.map(( registro ) => { return registro.id_ciclo })
      mapAlumnos = getinscripciones.map(( registro ) => { return registro.id_alumno })

      mapCiclos = mapCiclos.length ? mapCiclos : [0]

      alumnosParaRI    = await inscripciones.alumnosParaRI( mapAlumnos, mapCiclos ).then(response=> response)
    }else{
		  getinscripciones = await inscripciones.getinscripciones( ).then(response=> response)
    }



    // SACAR SI EN ESE CICLO LE CORRESPONDE O NO EL SALDO A FAVOR
    let mapMatriculas = getinscripciones.map( alumnos => alumnos.matricula )

    mapMatriculas = mapMatriculas.length ? mapMatriculas : ['0']

    // CONSULTAR EL ÚLTIMO GRUPO 
    const ultimoGrupo = await inscripciones.getUltimoGruposCiclo( mapMatriculas, 1, ciclo ).then( response => response )

    // Recorremos los pagos del Nuevo ERP para saber si en ese grupo, existe un saldo a favor, por que podría ser de un grupo a fuuturo
    for( const i in getinscripciones ){

      // Desestructuramos y sacamos, matricula y grupo
      const { matricula, saldofavor } = getinscripciones[i]

      //comparamos y si la matricula y el grupo no existennn, se elimina el adeudo de ahí
      const saldoCorresponde = ultimoGrupo.find( el => el.matricula == matricula && el.id_ciclo == ciclo )

      // Si aparece, dejamos el saldo a favor, si no, lo eliminamos
      getinscripciones[i].saldofavor = saldoCorresponde ? saldofavor : 0
    }



    // Saber si el alumno es RI o no
    for( const i in getinscripciones ){

      // Desestructuramos y sacamos, matricula y grupo
      const { id_alumno, adeudo } = getinscripciones[i]

      const existeRIAlumno = alumnosParaRI.find( el => el.id_alumno == id_alumno && mapCiclos.includes( el.id_ciclo ) )

      // Si el alumno existe, hay que validar que no tenga adeudo en el siguiente ciclo y que tenga maestro asignado
      if( existeRIAlumno && adeudo <= 0 ){

        getinscripciones[i].clase_row = 'alumno_ri'
      }

    }

    console.log( getinscripciones.filter( el => { return el.clase_row == 'alumno_ri' }).length )

		// Enviar los inscripciones
		res.send(getinscripciones);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

// Obtener el listado de inscripciones
exports.getAlumnoInscrito = async(req, res) => {
  try {
  	const { id } = req.params
  	// Consultar solo los inscripciones activos
		const getAlumnoInscrito = await inscripciones.getAlumnoInscrito( id ).then(response=> response)

		// enviar los inscripciones
		res.send(getAlumnoInscrito);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

// Obtener el listado de inscripciones
exports.getGruposAlumnoInscrito = async(req, res) => {
  try {
  	const { id } = req.params
  	// Consultar solo los inscripciones activos
		const getGruposAlumnoInscrito = await inscripciones.getGruposAlumnoInscrito( id ).then(response=> response)

		// enviar los inscripciones
		res.send(getGruposAlumnoInscrito);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

// Obtener las becas existentes catalgos
exports.getBecasInscripcion = async(req, res) => {
  try {
  	const { id } = req.params
  	// Consultar solo los inscripciones activos
		const getBecas = await inscripciones.getBecas( id ).then(response=> response)

		// enviar los inscripciones
		res.send(getBecas);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

// Obtener las becas del alumno
exports.getBecasAlumno = async(req, res) => {
  try {
  	const { id } = req.params
  	// Consultar solo los inscripciones activos
		const getBecasAlumno = await inscripciones.getBecasAlumno( id ).then(response=> response)

		// enviar los inscripciones
		res.send(getBecasAlumno);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

// Obtener los alumnos inscritos
exports.getListadoAlumnos = async(req, res) => {
  try {
    // Consultar solo los inscripciones activos
    let getAlumnos         = await inscripciones.getListadoAlumnos( ).then(response=> response)
    // const getDatosProspectos = await inscripciones.getDatosProspectos( ).then(response=> response)

    // const respuesta = getAlumnos.concat( getDatosProspectos )

    // enviar los inscripciones
    res.send(getAlumnos);

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getListadoProspectos = async(req, res) => {
  try {
    // Consultar solo los inscripciones activos
    let getDatosProspectos         = await inscripciones.getDatosProspectos( ).then(response=> response)

    // enviar los inscripciones
    res.send(getDatosProspectos);

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

// Consultar la información de un solo alumno
exports.getListadoAlumnosId = async(req, res) => {
  try {
    const { id } = req.params

    // Consultar solo los inscripciones activos
    let alumno         = await inscripciones.getAlumnosId( id ).then(response=> response)

    if( !alumno ){ return res.status( 400 ).send({ message: 'El alumno no existe' }) }

    // enviar los inscripciones
    res.send({ alumno });

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

// Consultar el grupo actual y el grupo siguiente del alumno
exports.getGruposAlumno = async(req, res) => {
  try {
    if (!req.body || Object.keys(req.body).length === 0)
      return res.status( 400 ).send({ message: 'No puedes enviar datos vacios' })

    /*
      Tipos de alumnos ( tipoAlumno )
      1-. NI
      2-. Regular
      3-. Irregular
      4-. Adeudo
      5-. AL CORRIENTE PERO GRUPO NO ABIERTO
    */

    const { id_alumno, escuela } = req.body

    // Obtener el ciclo actual para inscripciones
    const cicloActual = await inscripciones.getCicloActualInscripciones( escuela ).then( response => response )

    if( !cicloActual )
      return res.status( 400 ).send({message: 'Ciclo no activo, envíar mensaje a administración para activar ciclo' })

    // consultar el grupo actual del alumno
    const getGrupoActual = await inscripciones.getGrupoActual( id_alumno ).then(response=> response)


    // CONSULTAR EL ÚLTIMO GRUPO QUE PAGÓ, PARA SABER SI TIENEN UN SALDO A FAVOR O NO
    const getUltimoGrupoPagado = await inscripciones.getUltimoGrupoPagado( id_alumno ).then( response => response )

    const diferencia = getUltimoGrupoPagado ? getUltimoGrupoPagado.diferencia : 0

    // Validar si la diferencia no es mayor a 0, si es mayor, entonces tiene un sado a favor
    if( diferencia > 3 )
      return res.send({ message: 'Alumno con saldo a favor, comunícate con administración para que haga el cambio de saldo', tipoAlumno: 6, getUltimoGrupoPagado })

    // Si no cuenta con grupo, es necesario indicar que no tiene grupo actual y hay que mostrar los grupos posibles pero solo los del examen de ubicación
    if( !getGrupoActual )
      return res.send({ message: 'Examen de ubicación 1', tipoAlumno: 1})

    // Qué pasa si cuenta con grupo actual pero tiene adeudo? 
    // Retornaremos un tipoAlumno 4 que será con adeudo
    const { adeudo, pagado } = getGrupoActual
    if( adeudo > 0 ){
      return res.send({ 
        grupoActual:    getGrupoActual    ? getGrupoActual : null,
        grupoSiguiente: getGrupoActual    ? getGrupoActual : null,
        cicloActual, 
        tipoAlumno: 4, hermanos: false
      })
    }

    const diferenciaCiclos = await inscripciones.diferenciaCiclos( cicloActual.fecha_inicio_ciclo , getGrupoActual.fecha_fin_ciclo ).then( response => response  )

    if( cicloActual.id_ciclo > getGrupoActual.id_ciclo && diferenciaCiclos.diferencia > 1){
      // Alumno irregularrrrrr
      // VALIDAR SI YA ESTA EN LA LISTA NEGRA, SI NO, AGREGARLO
      return res.send({ message: 'REINGRESO', tipoAlumno: 3, hermanos: false })
    }

    // Si no cuenta con grupo siguiente, es necesario indicar que no tiene y hay que mostrar los grupos posibles pero solo los del examen de ubicación
    if( getGrupoActual.id_grupo_consecutivo == 0 ){
      return res.status( 400 ).send({ 
        message: `Solicitar a administración abrir el grupo que le sigue a: ${getGrupoActual.grupo}`,
        grupoActual:    getGrupoActual    ? getGrupoActual    : null,
        grupoSiguiente: null,
        cicloActual, 
        tipoAlumno: 5, 
        hermanos: false
      })
    }


    // Si cuenta con grupo actual y consecutivo, hay que ver que ciclo tiene el consecutivo
    const getGrupoSiguiente = await inscripciones.getGrupoSiguiente( getGrupoActual.id_grupo_consecutivo ).then(response=> response)

    if( !getGrupoSiguiente )
      return res.status( 400 ).send({message: 'Grupo no activo, envíar mensaje a administración para activarlo, 1' })

    if( getGrupoSiguiente.inscritos >= getGrupoSiguiente.capacidad )
      return res.status( 400 ).send({message: 'El grupo ya no tiene capacidad' })


    // Existe grupo siguiente, hay que valodar los ciclos
    const { id_ciclo_grupo_siguiente }     = getGrupoSiguiente
    const { id_ciclo, dias_transcurridos } = cicloActual

    // Si el ciclo del grupo siguiente es mayor al actual, el alumno debe poder proseguir, esto a que son ciclos a futuro
    // si ambos ciclos son iguales, hay que validar los días transcurridos, para ver si aún se puede inscribir

    if( id_ciclo_grupo_siguiente > id_ciclo || id_ciclo_grupo_siguiente == id_ciclo && dias_transcurridos <= 14 ){
      return res.send({ 
        grupoActual:    getGrupoActual    ? getGrupoActual    : null,
        grupoSiguiente: getGrupoSiguiente ? getGrupoSiguiente : null,
        cicloActual, 
        tipoAlumno: 2, 
        hermanos: false
      })
    }

    if( id_ciclo_grupo_siguiente == id_ciclo && dias_transcurridos > 7 ){
      // Si no paso por ninguna de esas, significa que el grupo activo no nos sirve, y ya paso más de un ciclo de su último ciclo :p
      // VALIDAR SI YA ESTA EN LA LISTA NEGRA, SI NO, AGREGARLO
      return res.send({ message: 'Examen de ubicación 3', tipoAlumno: 3, hermanos: false })
    }

    return res.status(500).send({ message: 'Error en el servidor' })

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getGrupoAlumno = async(req, res) => {
  try {
    if (!req.body || Object.keys(req.body).length === 0)
      return res.status( 400 ).send({ message: 'No puedes enviar datos vacios' })

    /*
      Tipos de alumnos ( tipoAlumno )
      1-. NI
      2-. Regular
      3-. Irregular
      4-. Adeudo
      5-. AL CORRIENTE PERO GRUPO NO ABIERTO
      6-. Alumno con saldo a favor
    */

    const { id_alumno, escuela, id_grupo } = req.body

    // consultar el grupo actual del alumno
    const getGrupoActual = await inscripciones.pagoActual( id_alumno, id_grupo ).then(response=> response)

    // Si no cuenta con grupo, es necesario indicar que no tiene grupo actual y hay que mostrar los grupos posibles pero solo los del examen de ubicación
    if( !getGrupoActual )
      return res.send({ message: 'Examen de ubicación 1', tipoAlumno: 1})

    // Qué pasa si cuenta con grupo actual pero tiene adeudo? 
    // Retornaremos un tipoAlumno 4 que será con adeudo
    const { adeudo, pagado } = getGrupoActual
    if( adeudo > 0 ){
      return res.send({ 
        grupoActual:    getGrupoActual    ? getGrupoActual : null,
        grupoSiguiente: getGrupoActual    ? getGrupoActual : null,
        tipoAlumno: 4, hermanos: false
      })
    }

    // Si el ciclo del grupo siguiente es mayor al actual, el alumno debe poder proseguir, esto a que son ciclos a futuro
    // si ambos ciclos son iguales, hay que validar los días transcurridos, para ver si aún se puede inscribir

    return res.send({ 
      grupoActual:    getGrupoActual ? getGrupoActual    : null,
      grupoSiguiente: getGrupoActual ? getGrupoActual : null,
      tipoAlumno: 2, 
      hermanos: false
    })

    return res.status(500).send({ message: 'Error en el servidor' })

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

// Saber si el alumno es becado
exports.alumnosEsBecado = async(req, res) => {
  try {

    const { id } = req.params

    // Los alumnos becados son :
    /*
      1-. Empleados
      2-. Familiar empleado
      3-. 15 pagos y con seguro académicos
    */

    const getAlumnoEmpleado = await inscripciones.getAlumnoEmpleado( id ).then( response => response )

    // Desestructurar el resultado
    const { id_empleado, empleado_activo } = getAlumnoEmpleado

    if( id_empleado && empleado_activo )
      return res.send({ message: 'Es empleado'});  


    res.send({ message: 'Terminó de procesar '});

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

// Saber cuanto debe pagar el alumno
exports.calcularPrecioAlumno = async(req, res) => {
  try {
    /*************** CALCULAR EL PRECIO DEL CURSO ***************/
    
    /*
      Tipos de alumnos ( tipoAlumno )
      1-. NI
      2-. Regular
      3-. Irregular
      4-. Con adeudo
    */

    let { tipoAlumno, hermanos, fechapago, factura, unidad_negocio } = req.body

    // ************** ALUMNO NORMAL ****************************//
    if( !hermanos.length ){
      const { id_alumno, empleado_activo, grupoSiguiente } = req.body
      
      // DESCUENTOS POR RECOMENDACIÓN
      let descuentoRecomendados = await inscripciones.getDescuentoRecomendados( id_alumno, unidad_negocio ).then( response => response )
      let descRecomienda = descuentoRecomendados.map(item => item.monto).reduce((prev, curr) => prev + curr, 0)

      //****** 1-. Calular la semana en la que se esta inscribiendo el alumno ******//

      const descuentos = await inscripciones.getSemanaPago( grupoSiguiente.id_ciclo, fechapago ).then( response => response )
      if( !descuentos ){ return res.status( 400 ).send({ message: 'Solicitar a administración agregar los descuentos' }) }

      ///*****************************************///

      //****** 2-. Ver que curso le toca al alumno ******//
      const curso    = await inscripciones.getUltimoCursoAlumno( id_alumno ).then( response => response )

      let id_curso   = 0

      const { grupo } = grupoSiguiente

      let guardarCurso = false

      if( !grupo.toUpperCase().match('INDUCCI') && !grupo.toUpperCase().match('CAMBIOS')  ){
        guardarCurso = true
      }

      if( curso && curso.id_curso == 18 ){
        id_curso = 18
      }else{
        if( [1,2,4,5].includes(tipoAlumno) && curso && guardarCurso ){
          id_curso = curso.id_curso
        }else{
          id_curso = grupoSiguiente.id_curso
        }
      }

      ///******* 2-. SACAR SI TIENE SALDO A FAVOR**********///
      let saldoFavor = await mixinInscripciones.validaSaldoFavor( id_alumno ).then( response => response )

      if( saldoFavor > 10 ){ return res.status( 400 ).send({ message: 'El alumno cuenta con saldo a favor, comunícate con sistemas para resolver el detalle' } ) }
      ///**************************************************///

      ///*** 3-. SACAR PRECIO DE CURSO EN BASE AL CURSO ***///
      let descuentoPorAplicar =  await inscripciones.getDescuentosPorAplicar( id_curso ).then( response => response )
      
      // VALIDAMOS SI EXISTEN LOS DECUENSTOS DE ESE CURSO
      if( !descuentoPorAplicar.length ){ return res.status( 400 ).send({ message: 'Error, no existen descuentos' }) }

      // MIXIN PARA SABER QUE CURSO VA A PAGAR
      let descuentoAplicado = await mixinInscripciones.validaDescuentoAplicado( tipoAlumno, descuentos, descuentoPorAplicar, id_alumno, factura ).then( response => response )

      if( !descuentoAplicado ){ return res.status( 400 ).send({ message: 'Error, no existen descuentos' }) }

      // SEPARAMOS LOS PRECIOS DEL CURSO 
      let { precio_lista, beca_hermanos, precio_hermanos } = descuentoAplicado

      // SACAMOS EL PRECIO INICAL DEL CURSO
      const descuento3 = descuentoPorAplicar.find( el => el.numero_descuento == 3 )
      const precio_inicial = descuento3 ? descuento3.precio_lista : 0

      ///**************************************************///

      /*
        CALCULAR DESCUENTOS YA MÁS GENERALES O ESPECIALES
      */

      // consultar el grupo actual del alumno
      const getGrupoActual      = await inscripciones.getGrupoActual( id_alumno ).then(response=> response)

      let id_grupo_actual = getGrupoActual ? getGrupoActual.id_grupo : ''

      let descuentosEspeciales = await descuentosEspe.getDescuentosAlumno( id_alumno, id_grupo_actual ).then( response => response )

      const sumaDescuentosEspeciales = descuentosEspeciales.map(item => item.monto ).reduce((prev, curr) => prev + curr, 0)

      /*******************************************/


      ///* 4-. OBTENER SI HAY UNA BECA PARA APLICAR O NO *///
      const { id_grupo } = grupoSiguiente
      // BECA NORMAL
      // let aplicaBeca       = await becas.getBecaGrupoAlumno( id_alumno, id_grupo ).then( response => response )

      // VALIDAR BECA AUTOMÁTICA
      let aplicaBeca          = await becas.becasRestantes( id_alumno ).then( response => response )

      // BECAR POR EMPLEADo
      const validaEmpleado = await mixinInscripciones.validarEmpleado( id_alumno, id_grupo, aplicaBeca ).then( response => response )
      ///**************************************************///

      // VARIABLES INICIALES
      let descuento       = 0
      let total_a_pagar   = 0
      let beca            = null
      let pagado          = 0
      let tieneSaldoFavor = saldoFavor > 0 ? true : false
      let pago_anterior   = grupoSiguiente.pagado > 0 ? grupoSiguiente.pagado : 0

      // VERIFICAR QUE EL ALUMNO NO SEA IRREGULAR
      if( tipoAlumno != 3 ){

        /*
          VALIDAR SI ES EMPLEADO O TIENE BECA, SI ES EMPLEADO, NO VA A PAGAR
        */

        if( validaEmpleado || aplicaBeca ){
          // VALIDAR SI LA BECA ESTA ACEPTADA 
          const { aprobadoSN, becaTotal } = validaEmpleado ? validaEmpleado : aplicaBeca
          if( aprobadoSN ){
            let multiplo    = becaTotal < 100 ? (( 100 - becaTotal ) / 100) : 0 
            let totalPagar  = precio_lista * multiplo 
            descuento       = ( precio_inicial - totalPagar)
            total_a_pagar   = totalPagar > 0 ? ( totalPagar - saldoFavor ) : totalPagar
            beca            = validaEmpleado ? validaEmpleado : aplicaBeca
            pagado          = 0
          }

          // VALIDAR SI SE PUEDE AGREGAR EL DESCUENTO
          let pagoFake = total_a_pagar - sumaDescuentosEspeciales
          if( pagoFake > 0 ) {
            descuento += sumaDescuentosEspeciales
            total_a_pagar -= sumaDescuentosEspeciales
          }else{
            descuentosEspeciales = []
          }
        }

        ///*****************************************///

        /***** EL ALUMNO NO ES BECADO, NI EMPLEADO, ES UN ALUMNO NORMAL QUE PAGA NORMAL *****/
        if( [1,2,4,5].includes(tipoAlumno) &&  !validaEmpleado && !aplicaBeca ){

          let totalPagar    = descuentoAplicado.precio_lista

          // APLICAR DESCUENTO INDUCCIÓN
          const descuentoInduccion = await inscripciones.getDescuentoInduccion( id_alumno ).then( response => response )

          let descuentoInduccionAplicar = 0
          
          if( descuentoInduccion && [1].includes(tipoAlumno)){
            descuentoInduccionAplicar = descuentoInduccion.monto_pagado_total  
          }

          let pagoFake = totalPagar - descuentoInduccionAplicar

          if( pagoFake > 0 ){

            descuento     = ( precio_inicial - descuentoAplicado.precio_lista ) + descuentoInduccionAplicar + descRecomienda
            total_a_pagar = totalPagar - descuentoInduccionAplicar - descRecomienda

          }else{

            descuento             = ( precio_inicial - descuentoAplicado.precio_lista ) + descuentoInduccionAplicar
            total_a_pagar         = totalPagar - descuentoInduccionAplicar
            descuentoRecomendados = [] // VACIARLO

          }

          // AGREGAR DESCUENTOS ESPECIALES
          pagoFake = total_a_pagar - sumaDescuentosEspeciales
          if( pagoFake > 0 ) {
            descuento += sumaDescuentosEspeciales
            total_a_pagar -= sumaDescuentosEspeciales
          }else{
            descuentosEspeciales = []
          }


          beca              = null
          pagado            = grupoSiguiente.pagado > 0 ? grupoSiguiente.pagado : 0
        }

        ///*****************************************///

        // Es irregular, paga su curso normal
        return res.send({
          precio_inicial,
          descuento,
          total_a_pagar,
          beca,
          pagado,
          saldoFavor,
          tieneSaldoFavor,
          pago_anterior,
          descuentoRecomendados: total_a_pagar > 0 ? descuentoRecomendados : [],
          descRecomienda,
          descuentosEspeciales
        })

      }else{
        if( aplicaBeca ){
          // VALIDAR SI LA BECA ESTA ACEPTADA 
          const { aprobadoSN, becaTotal } = aplicaBeca
          if( aprobadoSN ){
            let multiplo    = becaTotal < 100 ? (( 100 - becaTotal ) / 100) : 0 
            let totalPagar  = precio_lista * multiplo 
            descuento       = ( precio_inicial - totalPagar)
            total_a_pagar   = totalPagar > 0 ? ( totalPagar - saldoFavor ) : totalPagar
            beca            = aplicaBeca
            pagado          = 0
          }

          // AGREGAR DESCUENTOS ESPECIALES
          let pagoFake = total_a_pagar - sumaDescuentosEspeciales
          if( pagoFake > 0 ) {
            descuento += sumaDescuentosEspeciales
            total_a_pagar -= sumaDescuentosEspeciales
          }else{
            descuentosEspeciales = []
          }

          return res.send({
            precio_inicial,
            descuento,
            total_a_pagar,
            beca,
            pagado,
            saldoFavor,
            tieneSaldoFavor,
            pago_anterior,
            descuentoRecomendados: total_a_pagar > 0 ? descuentoRecomendados : [],
            descRecomienda,
            descuentosEspeciales
          })
        }else{

          let pagoFake = descuentoAplicado.precio_lista - ( precio_inicial - descuentoAplicado.precio_lista)

          if( pagoFake > 0 ){

            descuento     = ( precio_inicial - descuentoAplicado.precio_lista) + descRecomienda
            total_a_pagar = descuentoAplicado.precio_lista - descRecomienda

          }else{

            descuento             = ( precio_inicial - descuentoAplicado.precio_lista)
            total_a_pagar         = descuentoAplicado.precio_lista
            descuentoRecomendados = [] // VACIARLO

          }

          // AGREGAR DESCUENTOS ESPECIALES
          pagoFake = total_a_pagar - sumaDescuentosEspeciales
          if( pagoFake > 0 ) {
            descuento += sumaDescuentosEspeciales
            total_a_pagar -= sumaDescuentosEspeciales
          }else{
            descuentosEspeciales = []
          }


          const respuesta = {
            precio_inicial,
            descuento,
            total_a_pagar,
            beca          : null,
            pagado,
            saldoFavor,
            tieneSaldoFavor,
            pago_anterior,
            descuentoRecomendados,
            descRecomienda,
            descuentosEspeciales
          }

          return res.send(respuesta)
        }
      }
    }else{

      // ************** HERMANOS  ****************//

      // ************************************
      //  HERMANOS
      // ************************************
      let precio_inicial  = 0
      let descuento       = 0
      let total_a_pagar   = 0
      let beca            = null
      let pagado          = 0
      let saldoFavor      = 0
      let tieneSaldoFavor = 0
      let pago_anterior   = 0
      let obtenerSaldoFavor = null

      let pagosCursos = []

      for( const i in hermanos ){

        const { grupoSiguiente, id_alumno, factura } = hermanos[i]

        //****** 1-. Calular la semana en la que se esta inscribiendo el alumno ******//
        const descuentos = await inscripciones.getSemanaPago( grupoSiguiente.id_ciclo, fechapago ).then( response => response )
        if( !descuentos ){ return res.status( 400 ).send({ message: 'Solicitar a administración agregar los descuentos' }) }

        //****** 2-. Ver que curso le toca al alumno ******//
        const curso    = await inscripciones.getUltimoCursoAlumno( id_alumno ).then( response => response )
        
        let id_curso   = 0

        let guardarCurso = false

        const { grupo } = grupoSiguiente

        if( !grupo.toUpperCase().match('EXCI') && !grupo.toUpperCase().match('INDUCCI') && !grupo.toUpperCase().match('CAMBIOS')  ){
          guardarCurso = true
        }

        if( [2,4].includes(tipoAlumno) && curso  ){
          id_curso = curso.id_curso
        }else{
          id_curso = grupoSiguiente.id_curso
        }

        ///******* 2-. SACAR SI TIENE SALDO A FAVOR**********///
        obtenerSaldoFavor = await mixinInscripciones.validaSaldoFavor( id_alumno ).then( response => response )
        ///**************************************************///

        ///*** 3-. SACAR PRECIO DE CURSO EN BASE AL CURSO ***///
        let descuentoPorAplicar =  await inscripciones.getDescuentosPorAplicar( id_curso ).then( response => response )


        // VALIDAMOS SI EXISTEN LOS DECUENSTOS DE ESE CURSO
        if( !descuentoPorAplicar.length ){ return res.status( 400 ).send({ message: 'Error, no existen descuentos' }) }

        // MIXIN PARA SABER QUE CURSO VA A PAGAR
        let descuentoAplicado = await mixinInscripciones.validaDescuentoAplicado( tipoAlumno, descuentos, descuentoPorAplicar, id_alumno, factura).then( response => response )

        if( !descuentoAplicado ){ return res.status( 400 ).send({ message: 'Error, no existen descuentos' }) }

        // SEPARAMOS LOS PRECIOS DEL CURSO 
        let { precio_lista, beca_hermanos, precio_hermanos } = descuentoAplicado

        // SACAMOS EL PRECIO INICAL DEL CURSO
        const descuento3 = descuentoPorAplicar.find( el => el.numero_descuento == 3 )
        precio_inicial += descuento3 ? descuento3.precio_lista : 0
        const precio_inicial_unico = descuento3 ? descuento3.precio_lista : 0

        ///* 4-. OBTENER SI HAY UNA BECA PARA APLICAR O NO *///
        const { id_grupo } = grupoSiguiente
        // BECA NORMAL
        let aplicaBeca       = await becas.getBecaGrupoAlumno( id_alumno, id_grupo ).then( response => response )

        // BECAR POR EMPLEADo
        const validaEmpleado = await mixinInscripciones.validarEmpleado( id_alumno, id_grupo ).then( response => response )
        ///**************************************************///

        tieneSaldoFavor = obtenerSaldoFavor > 0 ? true : false
        saldoFavor      += obtenerSaldoFavor

        if( tipoAlumno != 3 ){

          // APLICAR DESCUENTO INDUCCIÓN
          const descuentoInduccion = await inscripciones.getDescuentoInduccion( id_alumno ).then( response => response )

          let descuentoInduccionAplicar = 0
          
          if( descuentoInduccion && [1].includes(tipoAlumno)){
            descuentoInduccionAplicar = descuentoInduccion.monto_pagado_total  
          }

          /**** VALIDAR SI DESCUENTO HERAMNOS ****/
          // sacamos si el cruso tiene beca de hermano y si no pues pafa normal
          let precio_a_pagar = beca_hermanos ? precio_hermanos : precio_lista
          let totalPagar     = precio_a_pagar - descuentoInduccionAplicar

          pago_anterior      += grupoSiguiente.pagado > 0 ? grupoSiguiente.pagado : 0
          // Responder el precio del grupo 
          descuento          += ( precio_inicial_unico - precio_a_pagar) + descuentoInduccionAplicar
          total_a_pagar      += totalPagar
          beca               = null
          pagado             += grupoSiguiente.pagado > 0 ? grupoSiguiente.pagado : 0
          tieneSaldoFavor    = saldoFavor > 0 ? true : false

          pagosCursos.push({
            id_alumno,
            precio_inicial: descuento3 ? descuento3.precio_lista : 0,
            descuento     : ( precio_inicial_unico - precio_a_pagar) + descuentoInduccionAplicar,
            total_a_pagar : totalPagar,
            beca,
            pagado        : grupoSiguiente.pagado > 0 ? grupoSiguiente.pagado : 0,
            saldoFavor    : obtenerSaldoFavor,
            tieneSaldoFavor,
            pago_anterior : grupoSiguiente.pagado > 0 ? grupoSiguiente.pagado : 0
          })

        }else{
          const respuesta = {
            precio_inicial: precio_inicial,
            descuento     : ( precio_inicial - descuentoAplicado.precio_lista), 
            total_a_pagar : descuentoAplicado.precio_lista,
            beca          : null
          }
          return res.send(respuesta)
        }
      }

      for(const i in pagosCursos){
        pagosCursos[i]['porcentaje'] = pagosCursos[i].total_a_pagar / total_a_pagar
      }

      // Es irregular, paga su curso normal
      return res.send({
        precio_inicial,
        descuento,
        total_a_pagar,
        beca,
        pagado,
        saldoFavor,
        tieneSaldoFavor,
        pago_anterior,
        pagosCursos
      })
    }
    
  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};


exports.calcularFacturaAlumno = async(req, res) => {
  try {
    /*************** CALCULAR EL PRECIO DEL CURSO ***************/
    
    /*
      Tipos de alumnos ( tipoAlumno )
      1-. NI
      2-. Regular
      3-. Irregular
      4-. Con adeudo
    */

    let { hermanos, factura } = req.body

    // ************** ALUMNO NORMAL ****************************//
    if( !hermanos.length ){

      const { id_alumno, empleado_activo, grupoSiguiente } = req.body

      // Validar si es un empleado activo, si lo es, no puede pagar factura, ya que realmente esta becado
      if( empleado_activo ){ return res.status( 500 ).send({ message: 'El alumno cuenta con beca, no puede pagar factura' }) }
      
      ///******* 2-. SACAR SI TIENE SALDO A FAVOR**********///
      let saldoFavor = await mixinInscripciones.validaSaldoFavor( id_alumno ).then( response => response )

      // Obtener los ingresos que tiene en el grupo, y obtner alumnos_grupo_carga_especial
      const { id_grupo } =  grupoSiguiente

      // Obtner los ingresos de ese grupo del alumno
      const ingresosAlumno = await inscripciones.getIngresosAlumnos( id_alumno, id_grupo ).then( response => response )

      // Si no hay pagos anteriore, hay que marcar un error
      if( !ingresosAlumno ){ return res.status( 500 ).send({ message: 'No existen pagos del grupo' }) }

      const { monto_pagado } = ingresosAlumno

      // Obtener el alumnos_grupo_carga_especial
      const alumnosGrupoCarga = await inscripciones.alumnosGrupoCarga( id_alumno, id_grupo ).then( response => response ) 

      // SI no existe, es pro que hubo un error al ingresar el pago anterior
      if( !alumnosGrupoCarga ){ return res.status( 500 ).send({ message: 'El pago de este grupo, no se generó correctamente' }) }

      // Desestructuramos los datos
      const { pagado, adeudo, descuento, precio_grupo } = alumnosGrupoCarga

      /*
        CALCULAR DESCUENTOS YA MÁS GENERALES O ESPECIALES
      */

      // VARIABLES INICIALES

      return res.send({
        precio_inicial       : precio_grupo,
        descuento            : descuento,
        total_a_pagar        : (( pagado * 1.16 ) - pagado - saldoFavor ).toFixed( 2 ),
        iva                  : ( pagado * .16 ).toFixed( 2 ),
        beca                 : null,
        pagado               : 0,
        saldoFavor           : saldoFavor,
        tieneSaldoFavor      : saldoFavor > 0 ? true : false,
        pago_anterior        : pagado,
        descuentoRecomendados: [],
        descRecomienda       : [],
        descuentosEspeciales : []
      })
      
    }else{

      // ************** HERMANOS  ****************//

      // ************************************
      //  HERMANOS
      // ************************************
      let precio_inicial  = 0
      let descuento       = 0
      let total_a_pagar   = 0
      let beca            = null
      let pagado          = 0
      let saldoFavor      = 0
      let tieneSaldoFavor = 0
      let pago_anterior   = 0
      let obtenerSaldoFavor = null

      let pagosCursos = []

      for( const i in hermanos ){

        const { grupoSiguiente, id_alumno, factura } = hermanos[i]

        //****** 1-. Calular la semana en la que se esta inscribiendo el alumno ******//
        const descuentos = await inscripciones.getSemanaPago( grupoSiguiente.id_ciclo, fechapago ).then( response => response )
        if( !descuentos ){ return res.status( 400 ).send({ message: 'Solicitar a administración agregar los descuentos' }) }

        //****** 2-. Ver que curso le toca al alumno ******//
        const curso    = await inscripciones.getUltimoCursoAlumno( id_alumno ).then( response => response )
        
        let id_curso   = 0

        let guardarCurso = false

        const { grupo } = grupoSiguiente

        if( !grupo.toUpperCase().match('EXCI') && !grupo.toUpperCase().match('INDUCCI') && !grupo.toUpperCase().match('CAMBIOS')  ){
          guardarCurso = true
        }

        if( [2,4].includes(tipoAlumno) && curso  ){
          id_curso = curso.id_curso
        }else{
          id_curso = grupoSiguiente.id_curso
        }

        ///******* 2-. SACAR SI TIENE SALDO A FAVOR**********///
        obtenerSaldoFavor = await mixinInscripciones.validaSaldoFavor( id_alumno ).then( response => response )
        ///**************************************************///

        ///*** 3-. SACAR PRECIO DE CURSO EN BASE AL CURSO ***///
        let descuentoPorAplicar =  await inscripciones.getDescuentosPorAplicar( id_curso ).then( response => response )


        // VALIDAMOS SI EXISTEN LOS DECUENSTOS DE ESE CURSO
        if( !descuentoPorAplicar.length ){ return res.status( 400 ).send({ message: 'Error, no existen descuentos' }) }

        // MIXIN PARA SABER QUE CURSO VA A PAGAR
        let descuentoAplicado = await mixinInscripciones.validaDescuentoAplicado( tipoAlumno, descuentos, descuentoPorAplicar, id_alumno, factura).then( response => response )

        if( !descuentoAplicado ){ return res.status( 400 ).send({ message: 'Error, no existen descuentos' }) }

        // SEPARAMOS LOS PRECIOS DEL CURSO 
        let { precio_lista, beca_hermanos, precio_hermanos } = descuentoAplicado

        // SACAMOS EL PRECIO INICAL DEL CURSO
        const descuento3 = descuentoPorAplicar.find( el => el.numero_descuento == 3 )
        precio_inicial += descuento3 ? descuento3.precio_lista : 0
        const precio_inicial_unico = descuento3 ? descuento3.precio_lista : 0

        ///* 4-. OBTENER SI HAY UNA BECA PARA APLICAR O NO *///
        const { id_grupo } = grupoSiguiente
        // BECA NORMAL
        let aplicaBeca       = await becas.getBecaGrupoAlumno( id_alumno, id_grupo ).then( response => response )

        // BECAR POR EMPLEADo
        const validaEmpleado = await mixinInscripciones.validarEmpleado( id_alumno, id_grupo ).then( response => response )
        ///**************************************************///

        tieneSaldoFavor = obtenerSaldoFavor > 0 ? true : false
        saldoFavor      += obtenerSaldoFavor

        if( tipoAlumno != 3 ){

          // APLICAR DESCUENTO INDUCCIÓN
          const descuentoInduccion = await inscripciones.getDescuentoInduccion( id_alumno ).then( response => response )

          let descuentoInduccionAplicar = 0
          
          if( descuentoInduccion && [1].includes(tipoAlumno)){
            descuentoInduccionAplicar = descuentoInduccion.monto_pagado_total  
          }

          /**** VALIDAR SI DESCUENTO HERAMNOS ****/
          // sacamos si el cruso tiene beca de hermano y si no pues pafa normal
          let precio_a_pagar = beca_hermanos ? precio_hermanos : precio_lista
          let totalPagar     = precio_a_pagar - descuentoInduccionAplicar

          pago_anterior      += grupoSiguiente.pagado > 0 ? grupoSiguiente.pagado : 0
          // Responder el precio del grupo 
          descuento          += ( precio_inicial_unico - precio_a_pagar) + descuentoInduccionAplicar
          total_a_pagar      += totalPagar
          beca               = null
          pagado             += grupoSiguiente.pagado > 0 ? grupoSiguiente.pagado : 0
          tieneSaldoFavor    = saldoFavor > 0 ? true : false

          pagosCursos.push({
            id_alumno,
            precio_inicial: descuento3 ? descuento3.precio_lista : 0,
            descuento     : ( precio_inicial_unico - precio_a_pagar) + descuentoInduccionAplicar,
            total_a_pagar : totalPagar,
            beca,
            pagado        : grupoSiguiente.pagado > 0 ? grupoSiguiente.pagado : 0,
            saldoFavor    : obtenerSaldoFavor,
            tieneSaldoFavor,
            pago_anterior : grupoSiguiente.pagado > 0 ? grupoSiguiente.pagado : 0
          })

        }else{
          const respuesta = {
            precio_inicial: precio_inicial,
            descuento     : ( precio_inicial - descuentoAplicado.precio_lista), 
            total_a_pagar : descuentoAplicado.precio_lista,
            beca          : null
          }
          return res.send(respuesta)
        }
      }

      for(const i in pagosCursos){
        pagosCursos[i]['porcentaje'] = pagosCursos[i].total_a_pagar / total_a_pagar
      }

      // Es irregular, paga su curso normal
      return res.send({
        precio_inicial,
        descuento,
        total_a_pagar,
        beca,
        pagado,
        saldoFavor,
        tieneSaldoFavor,
        pago_anterior,
        pagosCursos
      })
    }
    
  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};
// Registar el alumno en el grupo correspondiente
exports.registrarAlumnoGrupo = async(req, res) => {
  try {

    const { id_alumno, id_grupo, id_usuario_ultimo_cambio, id_curso, tipoAlumno, solcitaFactura, guardarCurso, descuentoRecomendados, descuentosEspeciales, grupo, pago_final_grupo } = req.body

    // 1-. Validar si es alumno NI
    if( tipoAlumno == 1 ){

      //  3-. Validar si es grupo de FAST o INBI
      if( grupo.match('FAST') ){

        // 3.1-. Valida si ya existe un registro del alumno en el sistema de inducción
          
        // Si no existe... agregar
        //  TIPO = 2 == ADULTOS INBI
        const existeRegistro = await inscripciones.getAlumnoLMSInduccion( id_alumno, 2, 2 ).then( response => response )

        // Si no existe, hay que registraro, pero el pago... debe ser >= 99 ya que es TEENS
        //  5-. Validar el costo
        if( !existeRegistro && pago_final_grupo >= 199 ){

          const addAlumnoInduccion = await inscripciones.addAlumnoInduccion( id_alumno, id_grupo, pago_final_grupo, 1, 2 ).then( response => response )
        }

      }else{

        // 4-. Validar si es TEENS
        if( grupo.match('TEENS') || grupo.match('KIDS') ){

          // 3.1-. Valida si ya existe un registro del alumno en el sistema de inducción
          
          // Si no existe... agregar
          //  TIPO = 3 == TEENS
          const existeRegistro = await inscripciones.getAlumnoLMSInduccion( id_alumno, 1, 3 ).then( response => response )

          // Si no existe, hay que registraro, pero el pago... debe ser >= 99 ya que es TEENS
          //  5-. Validar el costo
          if( !existeRegistro && pago_final_grupo >= 99 ){

            const addAlumnoInduccion = await inscripciones.addAlumnoInduccion( id_alumno, id_grupo, pago_final_grupo, 3, 1 ).then( response => response )
          }

        }else{

          // 3.1-. Valida si ya existe un registro del alumno en el sistema de inducción
          
          // Si no existe... agregar
          //  TIPO = 2 == ADULTOS INBI
          const existeRegistro = await inscripciones.getAlumnoLMSInduccion( id_alumno, 1, 2 ).then( response => response )

          // Si no existe, hay que registraro, pero el pago... debe ser >= 99 ya que es TEENS
          //  5-. Validar el costo
          if( !existeRegistro && pago_final_grupo >= 199 ){

            const addAlumnoInduccion = await inscripciones.addAlumnoInduccion( id_alumno, id_grupo, pago_final_grupo, 2, 1 ).then( response => response )
          }

        }

      }


    }

    // VERIRIFICAR SI SOLICITÓ FACTURA EL ALUMNO
    if( solcitaFactura ){

      // Buscar si ya existe una factura
      const existeFactura = await inscripciones.existeFactura( id_alumno, id_grupo ).then( response => response )
      
      // Si no, la agregamos
      if( !existeFactura ){
        const solicitarFactura = await inscripciones.solicitarFactura(  id_alumno, id_grupo ).then( response => response )
      }

    }

    // VALIDAR QUE NO EXISTA YA UN CAMPO, DE SER ASÍ ACTUALIZAR 
    const existeGrupoAlumno = await inscripciones.existeGrupoAlumno( id_alumno, id_grupo ).then( response => response )
    
    // 1-. Registrar el alumno en el grupo
    if( existeGrupoAlumno ){
      const actualizar   = await inscripciones.updateGrupoAlumno( req.body ).then( response => response ) // Actualizzar
    } else {
      let gruposAlumno   = await inscripciones.addGrupoAlumno( req.body ).then( response => response ) // Agregar
    }

    
    // 2-. Registrar el ingreso
    const ingreso        = await inscripciones.addIngreso( req.body ).then( response => response )

    // 3-. Registrar en alumno carga especial

    // VALIDAR QUE NO EXISTA YA UN CAMPO, DE SER ASÍ ACTUALIZAR 
    const existeCargaAlumno = await inscripciones.existeCargaAlumno( id_alumno, id_grupo ).then( response => response )
    
    // 1-. Registrar el alumno en el grupo
    if( existeCargaAlumno ){
      const updateCargaespecial  = await inscripciones.updateCargaespecial( req.body ).then( response => response )
    } else {
      const cargaespecial        = await inscripciones.addalumnoCargaEspecial( req.body ).then( response => response )
    }

    // 4-. Registrar el último id_curso del alumno
    if( tipoAlumno == 1 && guardarCurso ){
      const registrarUltimoCurso  = await inscripciones.addUltimoCursoAplicado( id_alumno, id_curso ).then( response => response )
    }


    if( tipoAlumno == 3 && guardarCurso ){
      // Validar si ya existe un registro, si no, actualizarlo
      const existeUltimoCurso = await inscripciones.getUltimoCursoAlumno( id_alumno ).then( response => response )

      if( existeUltimoCurso ){
        const updateUltimoCursoAlumno  = await inscripciones.updateUltimoCursoAlumno( id_curso, existeUltimoCurso.idalumnos_id_curso ).then( response => response )
      }else{
        const registrarUltimoCurso  = await inscripciones.addUltimoCursoAplicado( id_alumno, id_curso ).then( response => response )
      }

    }

    for( const i in descuentoRecomendados ){
      const { escuela, id_recomienda } = descuentoRecomendados[i]
      const updateRecomienda = await inscripciones.updateRecomienda( id_grupo, id_recomienda, escuela ).then( response => response )
    }

    for( const i in descuentosEspeciales ){
      const { iddescuentos_especiales } = descuentosEspeciales[i]
      const updateDescuetoEspecial = await descuentosEspe.updateDescuetoEspecial( id_grupo, iddescuentos_especiales ).then( response => response )
    }


    // CONSULTAR PAGO 
    const datosReciboPago = await whatsapp.datosReciboPago( ingreso.id ).then( response => response )

    if( !datosReciboPago ){ return res.status( 400 ).send({ message: 'El pago no existe, favor de validarlo con sistemas' })}

    // Consultar telefono del alumno
    const datosAlumno     = await whatsapp.datosWhatsAlumno( id_alumno ).then( response => response )

    // Convertir el monto pagado en letra
    let pago_letra        = numToWords.NumerosALetras( datosReciboPago.monto_pagado );

    let payload = {
      folio:             ingreso.id,
      alumno:            datosAlumno.nombre + ' ' + datosAlumno.apellido_paterno + ' ' + datosAlumno.apellido_materno,
      matricula:         datosReciboPago.matricula,
      plantel:           datosReciboPago.plantel,
      ciclo:             datosReciboPago.ciclo,
      curso:             datosReciboPago.curso ,
      cantidad_recibida: datosReciboPago.monto_pagado,
      cantidad_letra:    pago_letra,
      monto_descuento:   datosReciboPago.descuento,
      adeudo:            datosReciboPago.adeudo,
      vendedora:         datosReciboPago.nombre_completo,
      fecha_pago:        datosReciboPago.fecha_pago
    }

    newPDF( payload );

    return res.send({ message: 'Terminar de procesar', ingreso })
  
  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

exports.registrarAlumnoGrupoHermanos = async(req, res) => {
  try {

    let ingreso = null
    for (const i in req.body ){
      const { id_alumno, id_grupo, id_usuario_ultimo_cambio, id_curso, tipoAlumno } = req.body[i]

      // VALIDAR QUE NO EXISTA YA UN CAMPO, DE SER ASÍ ACTUALIZAR 
      const existeGrupoAlumno = await inscripciones.existeGrupoAlumno( id_alumno, id_grupo ).then( response => response )
      
      // 1-. Registrar el alumno en el grupo
      if( existeGrupoAlumno ){
        const actualizar   = await inscripciones.updateGrupoAlumno( req.body[i] ).then( response => response ) // Actualizzar
      } else {
        let gruposAlumno   = await inscripciones.addGrupoAlumno( req.body[i] ).then( response => response ) // Agregar
      }

      
      // 2-. Registrar el ingreso
      ingreso        = await inscripciones.addIngreso( req.body[i] ).then( response => response )

      // 3-. Registrar en alumno carga especial

      // VALIDAR QUE NO EXISTA YA UN CAMPO, DE SER ASÍ ACTUALIZAR 
      const existeCargaAlumno = await inscripciones.existeCargaAlumno( id_alumno, id_grupo ).then( response => response )
      
      // 1-. Registrar el alumno en el grupo
      if( existeCargaAlumno ){
        const updateCargaespecial  = await inscripciones.updateCargaespecial( req.body[i] ).then( response => response )
      } else {
        const cargaespecial        = await inscripciones.addalumnoCargaEspecial( req.body[i] ).then( response => response )
      }

      // 4-. Registrar el último id_curso del alumno
      if( tipoAlumno == 1 ){
        const registrarUltimoCurso  = await inscripciones.addUltimoCursoAplicado( id_alumno, id_curso ).then( response => response )
      }


      if( tipoAlumno == 3 ){
        // Validar si ya existe un registro, si no, actualizarlo
        const existeUltimoCurso = await inscripciones.getUltimoCursoAlumno( id_alumno ).then( response => response )

        if( existeUltimoCurso ){
          const updateUltimoCursoAlumno  = await inscripciones.updateUltimoCursoAlumno( id_curso, existeUltimoCurso.idalumnos_id_curso ).then( response => response )
        }else{
          const registrarUltimoCurso  = await inscripciones.addUltimoCursoAplicado( id_alumno, id_curso ).then( response => response )
        }
      }
    }
    
    return res.send({ message: 'Terminar de procesar', ingreso })
  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

// Registar el alumno en el grupo correspondiente
exports.realizarTransferencia = async(req, res) => {
  try {

    const { id_alumno, id_grupo, id_usuario_ultimo_cambio, id_curso, tipoAlumno, nuevoSaldoFavor, saldoFavor, tieneSaldoFavor, solcitaFactura, id_grupo_anterior, transferir } = req.body


    /////////////////////////////////////////////////*//////////////////////////////////////////////////////

    // 1-. Obenter le gruposAlumnos del grupo anterior
    const grupoAlumno = await inscripciones.getGrupoAlumno( id_alumno, id_grupo_anterior ).then( response => response )

    // Validar errores
    if( !grupoAlumno ){ return res.status( 500 ).send({ message: 'Error al ingresar el pago anterior, favor de reviar los registros del pago anterior' }) }

    // 2-. Obtener todos los ingresos del grupo anterior
    const ingresoAlumno = await inscripciones.getIngresosAlumno( id_alumno, id_grupo_anterior ).then( response => response )

    // Validar errores
    if( !ingresoAlumno.length ){ return res.status( 500 ).send({ message: 'Error al ingresar el pago anterior, favor de reviar los registros del pago anterior' }) }

    // 3-. Obtener alumnos_grupo_Carga_Especial del grupo anteior
    const cargaEspecialAlumno = await inscripciones.GetCargaEspecialAlumno( id_alumno, id_grupo_anterior ).then( response => response )

    // Validar errores
    if( !cargaEspecialAlumno ){ return res.status( 500 ).send({ message: 'Error al ingresar el pago anterior, favor de reviar los registros del pago anterior' }) }

    // Desestructuración de datos
    const { id_gruposalumnos, monto_pagado_total } = grupoAlumno

    // Actualizamos el grupoAlumno
    const updateGrupoAlumnoMonto = await inscripciones.updateGrupoAlumnoMonto( id_gruposalumnos, transferir ).then( response => response )

    let saldoARestar = transferir
    for( const i in ingresoAlumno ){
      const { id_ingreso, monto_pagado } = ingresoAlumno[i]
      let saldo = saldoARestar >= monto_pagado ? monto_pagado : saldoARestar

      const updateIngresoAlumno = await inscripciones.updateIngresoAlumno( id_ingreso, saldo ).then( response => response )

      saldoARestar = saldo 
    }

    const { id, pagado } = cargaEspecialAlumno
    // Actualizamos el grupoAlumno
    const updateCargaEspecialMonto = await inscripciones.updateCargaEspecialMonto( id, transferir ).then( response => response )

    /////////////////////////////////////////////////*//////////////////////////////////////////////////////

    // VERIRIFICAR SI SOLICITÓ FACTURA EL ALUMNO
    if( solcitaFactura ){

      // Buscar si ya existe una factura
      const existeFactura = await inscripciones.existeFactura( id_alumno, id_grupo ).then( response => response )
      
      // Si no, la agregamos
      if( !existeFactura ){
        const solicitarFactura = await inscripciones.solicitarFactura(  id_alumno, id_grupo ).then( response => response )
      }

    }

    // VALIDAR QUE NO EXISTA YA UN CAMPO, DE SER ASÍ ACTUALIZAR 
    const existeGrupoAlumno = await inscripciones.existeGrupoAlumno( id_alumno, id_grupo ).then( response => response )
    
    // 1-. Registrar el alumno en el grupo
    if( existeGrupoAlumno ){
      const actualizar   = await inscripciones.updateGrupoAlumno( req.body ).then( response => response ) // Actualizzar
    } else {
      let gruposAlumno   = await inscripciones.addGrupoAlumno( req.body ).then( response => response ) // Agregar
    }

    
    // 2-. Registrar el ingreso
    const ingreso        = await inscripciones.addIngreso( req.body ).then( response => response )

    // 3-. Registrar en alumno carga especial

    // VALIDAR QUE NO EXISTA YA UN CAMPO, DE SER ASÍ ACTUALIZAR 
    const existeCargaAlumno = await inscripciones.existeCargaAlumno( id_alumno, id_grupo ).then( response => response )
    
    // 1-. Registrar el alumno en el grupo
    if( existeCargaAlumno ){
      const updateCargaespecial  = await inscripciones.updateCargaespecial( req.body ).then( response => response )
    } else {
      const cargaespecial        = await inscripciones.addalumnoCargaEspecial( req.body ).then( response => response )
    }


    // CONSULTAR PAGO 
    const datosReciboPago = await whatsapp.datosReciboPago( ingreso.id ).then( response => response )

    if( !datosReciboPago ){ return res.status( 400 ).send({ message: 'El pago no existe, favor de validarlo con sistemas' })}

    // Consultar telefono del alumno
    const datosAlumno     = await whatsapp.datosWhatsAlumno( id_alumno ).then( response => response )

    // Convertir el monto pagado en letra
    let pago_letra        = numToWords.NumerosALetras( datosReciboPago.monto_pagado );

    let payload = {
      folio:             ingreso.id,
      alumno:            datosAlumno.nombre + ' ' + datosAlumno.apellido_paterno + ' ' + datosAlumno.apellido_materno,
      matricula:         datosReciboPago.matricula,
      plantel:           datosReciboPago.plantel,
      ciclo:             datosReciboPago.ciclo,
      curso:             datosReciboPago.curso ,
      cantidad_recibida: datosReciboPago.monto_pagado,
      cantidad_letra:    pago_letra,
      monto_descuento:   datosReciboPago.descuento,
      adeudo:            datosReciboPago.adeudo,
      vendedora:         datosReciboPago.nombre_completo,
      fecha_pago:        datosReciboPago.fecha_pago
    }

    newPDF( payload );

    return res.send({ message: 'Terminar de procesar', ingreso })
  
  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

exports.grabarComprobante = async ( req, res) => {
  try{

    if( !req.files ){
      return res.status( 400 ).send({ message : 'El contenido no puede ir vacio' })
    }
    
    // desestrucutramos los arvhios cargados
    const { file } = req.files

    const nombreSplit = file.name.split('.')
    
    // Obtener la extensión del archivo 
    const extensioArchivo = nombreSplit[ nombreSplit.length - 1 ]
    
    // modificar el nombre del archivo con un uuid unico
    const nombreUuid = uuidv4() + '.' + extensioArchivo
    
    // extensiones validas
    let extensiones  = [ 'BMP', 'GIF', 'jpg', 'jpeg', 'png', 'webp', 'PNG', 'JPG', 'JPEG', 'gif', 'bmp' ]
    let ruta         = './../../comprobante-pago/' + nombreUuid

    // validar que la extensión del archivo recibido sea valido
    if( !extensiones.includes( extensioArchivo) )
      return res.status( 400 ).send({message: `Hola, la extensión: ${ extensioArchivo } no es valida, solo se aceptan: ${ extensiones }`})
    

    file.mv(`${ruta}`, err => {
      if( err )
        return res.status( 400 ).send({ message: err })
      
      return res.send({ message: 'Imagen cargada correctamente', nombre: nombreUuid, extension: extensioArchivo })
    })

  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor' })
  }
}

exports.getGruposDisponibles = async ( req, res) => {
  try{
    const { id } = req.params
    const gruposDisponibles  = await inscripciones.getGruposDisponibles( id ).then( response => response )

    return res.send( gruposDisponibles )

  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor' })
  }
}

// Función para validar si el alumno tiene hermanos y a la vez hay que validar si no están en lista negra y que sean continuos
exports.validarHermanos = async ( req, res) => {
  try{
    const { id_alumno, escuela } = req.body

    let getHermanos = await inscripciones.getHermanos( id_alumno ).then( response => response )

    /*
      VALIDAR SI EL ALUMNO TIENE HERMANOS, SI EL GETHERMANOS.LENGT ES MAYOR A 1, ESO SIGNIFICA QUE SON HERMANOS
    */
    if( !getHermanos.length ){
      return res.send({ hermanos: false }) // NO SON HERMANOS
    }
    
    // Obtener el ciclo actual para inscripciones
    const cicloActual = await inscripciones.getCicloActualInscripciones( escuela ).then( response => response )

    // Validar que exista un ciclo
    if( !cicloActual )
      return res.status( 400 ).send({message: 'Ciclo no activo, envíar mensaje a administración para activar ciclo' })


    /*
      Tipos de alumnos ( tipoAlumno )
      1-. NI
      2-. Regular
      3-. Irregular
      4-. Con adeudo
    */

    /*
      SI SON HERMANOS, AHORA HAY QUE VALIDAR SI SON NI, REGULARES O RI
    */
    let alumnosNI = true

    // RECORRE A LOS HERMANOS
    for( const i in  getHermanos ){
      
      const { id_alumno } = getHermanos[i] // DESESTRUCTURAR A LOS HERMANOS

      // CONSULTAR EL GRUPO ACTUAL DEL ALUMNO
      const getGrupoActual = await inscripciones.getGrupoActual( id_alumno ).then(response=> response)

      // SI TIENE GRUPO ESO SIGNIFICA QUE NO ES NI, ES RI
      if( getGrupoActual ){
        getHermanos[i]['getGrupoActual'] = getGrupoActual
        getHermanos[i]['grupoSiguiente'] = getGrupoActual
        // BUSCAR EL GRUPO SIGUIENTE A ESE
        alumnosNI = false
      }
    }

    // NO SON ALUMNO NI
    if( !alumnosNI ){
      let tipoAlumno = 0
      for( const i in  getHermanos ){
      
        const { getGrupoActual } = getHermanos[i]

        const { adeudo } = getGrupoActual

        // VALIDAMOS SI EL ALUMNO TIENE ADEUDO
        if( adeudo > 0 ){

          // AGREGAMOS LOS GRUPOS DEL ALUMNO
          getHermanos[i].getGrupoActual = getGrupoActual
          getHermanos[i].grupoSiguiente = getGrupoActual

          tipoAlumno = 4

        }else{
          const getGrupoSiguiente = await inscripciones.getGrupoSiguiente( getGrupoActual.id_grupo_consecutivo ).then(response=> response)

          if( !getGrupoSiguiente )
            return res.status( 400 ).send({message: 'Grupo no activo, envíar mensaje a administración para activarlo, 2' })

          // AGREGAMOS LOS GRUPOS DEL ALUMNO
          getHermanos[i].getGrupoActual = getGrupoActual
          getHermanos[i].grupoSiguiente = getGrupoSiguiente

          tipoAlumno = 2
        }

      }

      return res.send({ message: tipoAlumno == 4 ? 'ALUMNO CON ADEUDO' : 'ALUMNO REGULAR', tipoAlumno, hermanos: getHermanos })

    }else{
      for( const i in getHermanos ){
        getHermanos[i]['tipoAlumno'] = 1
      }
      // LOS ALUMNOS SON HERMANOS, PEROOOO SON NI
      return res.send({ message: 'ALUMNOS NI', tipoAlumno: 1, hermanos: getHermanos })
    }

  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor' })
  }
}

exports.validarTipoAlumno = async ( req, res) => {
  try{
    const { id_alumno, escuela } = req.body

    let getHermanos = await inscripciones.getHermanos( id_alumno ).then( response => response )

    /*
      VALIDAR SI EL ALUMNO TIENE HERMANOS, SI EL GETHERMANOS.LENGT ES MAYOR A 1, ESO SIGNIFICA QUE SON HERMANOS
    */
    if( !getHermanos.length ){
      return res.send({ hermanos: false }) // NO SON HERMANOS
    }
    
    // Obtener el ciclo actual para inscripciones
    const cicloActual = await inscripciones.getCicloActualInscripciones( escuela ).then( response => response )

    // Validar que exista un ciclo
    if( !cicloActual )
      return res.status( 400 ).send({message: 'Ciclo no activo, envíar mensaje a administración para activar ciclo' })


    /*
      Tipos de alumnos ( tipoAlumno )
      1-. NI
      2-. Regular
      3-. Irregular
      4-. Con adeudo
    */

    /*
      SI SON HERMANOS, AHORA HAY QUE VALIDAR SI SON NI, REGULARES O RI
    */
    let alumnosNI = true

    // RECORRE A LOS HERMANOS
    for( const i in  getHermanos ){
      
      const { id_alumno } = getHermanos[i] // DESESTRUCTURAR A LOS HERMANOS

      // CONSULTAR EL GRUPO ACTUAL DEL ALUMNO
      const getGrupoActual = await inscripciones.getGrupoActual( id_alumno ).then(response=> response)

      // SI TIENE GRUPO ESO SIGNIFICA QUE NO ES NI, ES RI
      if( getGrupoActual ){
        getHermanos[i]['getGrupoActual'] = getGrupoActual
        getHermanos[i]['grupoSiguiente'] = getGrupoActual
        // BUSCAR EL GRUPO SIGUIENTE A ESE
        alumnosNI = false
      }
    }

    // NO SON ALUMNO NI
    if( !alumnosNI ){
      let tipoAlumno = 0
      for( const i in  getHermanos ){
      
        const { getGrupoActual } = getHermanos[i]

        const { adeudo } = getGrupoActual

        // VALIDAMOS SI EL ALUMNO TIENE ADEUDO
        if( adeudo > 0 ){

          // AGREGAMOS LOS GRUPOS DEL ALUMNO
          getHermanos[i].getGrupoActual = getGrupoActual
          getHermanos[i].grupoSiguiente = getGrupoActual

          tipoAlumno = 4

        }else{
          const getGrupoSiguiente = await inscripciones.getGrupoSiguiente( getGrupoActual.id_grupo_consecutivo ).then(response=> response)

          if( !getGrupoSiguiente )
            return res.status( 400 ).send({message: 'Grupo no activo, envíar mensaje a administración para activarlo, 3' })

          // AGREGAMOS LOS GRUPOS DEL ALUMNO
          getHermanos[i].getGrupoActual = getGrupoActual
          getHermanos[i].grupoSiguiente = getGrupoSiguiente

          tipoAlumno = 2
        }

      }

      return res.send({ message: tipoAlumno == 4 ? 'ALUMNO CON ADEUDO' : 'ALUMNO REGULAR', tipoAlumno, hermanos: getHermanos })

    }else{
      for( const i in getHermanos ){
        getHermanos[i]['tipoAlumno'] = 1
      }
      // LOS ALUMNOS SON HERMANOS, PEROOOO SON NI
      return res.send({ message: 'ALUMNOS NI', tipoAlumno: 1, hermanos: getHermanos })
    }

  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor' })
  }
}

exports.getPagoGrupo = async(req, res) => {
  try {
    /*************** CALCULAR EL PRECIO DEL CURSO ***************/

    const { id_alumno, id_grupo, escuela } = req.body

    // DESCUENTOS POR RECOMENDACIÓN APLICADOS
    // let descuentoRecomendados = await inscripciones.getDescuentoRecomendados( id_alumno, escuela, id_grupo ).then( response => response )
    // let descRecomienda = descuentoRecomendados.map(item => item.monto).reduce((prev, curr) => prev + curr, 0)

    ///******* SACAR SI TIENE SALDO A FAVOR**********///

    let saldoFavor = await mixinInscripciones.obtenerSaldoFavorGrupo( id_alumno, id_grupo ).then( response => response )

    ///**************************************************///

    // CALCULAR DESCUENTOS YA MÁS GENERALES O ESPECIALES

    // consultar el grupo actual del alumno
    // let descuentosEspeciales = await descuentosEspe.getDescuentosAlumno( id_alumno, id_grupo ).then( response => response )
    // const sumaDescuentosEspeciales = descuentosEspeciales.map(item => item.monto ).reduce((prev, curr) => prev + curr, 0)

    // // APLICAR DESCUENTO INDUCCIÓN
    // const descuentoInduccion = await inscripciones.getDescuentoInduccion( id_alumno ).then( response => response )

    // let descuentoInduccionAplicar = 0
    
    // if( descuentoInduccion && [1].includes(tipoAlumno)){
    //   descuentoInduccionAplicar = descuentoInduccion.monto_pagado_total  
    // }

    /*******************************************/

    // Sacar cuanto es el pago final del alumno
    const pagoGrupoCargaEspecial = await inscripciones.pagoGrupoCargaEspecial( id_alumno, id_grupo ).then( response => response )
    const { descuento, pagado, total_pagado, precio_grupo, precio_grupo_con_descuento_x_alumno, adeudo } = pagoGrupoCargaEspecial

    // VARIABLES INICIALES
    let total_a_pagar   = precio_grupo_con_descuento_x_alumno
    let pago_anterior   = pagado
    // let totalPagar      = precio_grupo_con_descuento_x_alumno

    // VERIFICAR QUE EL ALUMNO NO SEA IRREGULAR
    ///*****************************************///

    // let pagoFake = precio_grupo_con_descuento_x_alumno - descuentoInduccionAplicar

    // if( pagoFake > 0 ){
      // total_a_pagar = totalPagar - descuentoInduccionAplicar - descRecomienda
    // }else{
      // total_a_pagar         = totalPagar - descuentoInduccionAplicar
      // descuentoRecomendados = [] // VACIARLO
    // }

    // AGREGAR DESCUENTOS ESPECIALES
    // pagoFake = total_a_pagar - sumaDescuentosEspeciales
    // if( pagoFake > 0 ) {
      // descuento += sumaDescuentosEspeciales
      // total_a_pagar -= sumaDescuentosEspeciales
    // }else{
      // descuentosEspeciales = []
    // }

    // Es irregular, paga su curso normal
    return res.send({
      precio_inicial: total_pagado,
      descuento,
      total_a_pagar: adeudo + pagado,
      beca: null,
      pagado,
      saldoFavor,
      tieneSaldoFavor: saldoFavor > 0 ? true : false,
      pago_anterior,
      descuentoRecomendados: [],
      descRecomienda:[],
      descuentosEspeciales:[]
    })

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getMatriculaViejita = async ( req, res) => {
  try{

    const { matricula_erp } = req.body

    const alumno  = await inscripciones.getMatriculaViejita( matricula_erp ).then( response => response )

    if( alumno ){
      return res.send( alumno )
    }

    return res.status( 400 ).send( alumno )

    

  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor' })
  }
}

exports.getIngresosComparaciones = async ( req, res) => {
  try{

    const { id_plantel, id_ciclo } = req.body

    let pagosErpNuevo  = await inscripciones.getPagosERP( id_plantel, id_ciclo, 1 ).then( response => response )
    let pagosERPViejo  = await inscripciones.getPagosERP( id_plantel, id_ciclo, 2 ).then( response => response )

    // SACAR SI EN ESE CICLO LE CORRESPONDE O NO EL SALDO A FAVOR
    let mapMatriculas = pagosErpNuevo.map( alumnos => alumnos.matricula )

    mapMatriculas = mapMatriculas.length ? mapMatriculas : ['0']

    // CONSULTAR EL ÚLTIMO GRUPO 
    const ultimoGrupo = await inscripciones.getUltimoGruposCiclo( mapMatriculas, 1 ).then( response => response )

    // Recorremos los pagos del Nuevo ERP para saber si en ese grupo, existe un saldo a favor, por que podría ser de un grupo a fuuturo
    for( const i in pagosErpNuevo ){

      // Desestructuramos y sacamos, matricula y grupo
      const { matricula, saldofavor } = pagosErpNuevo[i]

      //comparamos y si la matricula y el grupo no existennn, se elimina el adeudo de ahí
      const adeudoCorresponde = ultimoGrupo.find( el => el.matricula == matricula && el.id_ciclo == id_ciclo )

      // Si aparece, dejamos el saldo a favor, si no, lo eliminamos
      pagosErpNuevo[i].saldofavor = adeudoCorresponde ? saldofavor : 0
    }


    // Sacar las matriculas que están de más
    let idMatriculas = pagosERPViejo.map(( registro ) => { return registro.matricula })

    // Retonar las matriculas que están de más o alumnos que están de más
    const alumnosExtras = pagosErpNuevo.filter( el => { !idMatriculas.includes( el.matricula_erp ) })

    // Arregki oara obtener los pagos finales del Nuevo ERP 
    let pagosFinalesNuevoERP = [] 

    // Recorrer los pagos de el erp viejo para poder encontrar los pagos en el nuevo erp
    for( const i in pagosERPViejo ){

      // Desestructuramos los pagos
      const { matricula, monto_pagado, grupo, plantel, nombre_completo, alumno } = pagosERPViejo[i]

      // Le agregar este campo solo para diseño, la vdd es que no se ocupa
      pagosERPViejo[i]['matricula_erp'] = matricula


      // EXISTE EL PAGO EN EL NUEVO ERP?
      const existePagoAlumno = pagosErpNuevo.find( el => el.matricula_erp == matricula )

      if( !existePagoAlumno ){

        // NO EXISTE, DEBE AGREGARSE AL ARREGLO DE QUE FALTAAAA
        pagosFinalesNuevoERP.push({
          matricula,
          matricula_erp     : matricula,
          alumno            : 'FALTA',
          monto_pagado,
          saldofavor        : 0,
          grupo,
          plantel,
          nombre_completo,
          clase_pago: 'red white--text'
        })
      }else{

        // EL PAGO SI EXISTE, NECESITAMOS HACER MÁS VALIDACIONES

        // MISMOS MONTOS....
        const mismoMonto = pagosErpNuevo.find( el => el.matricula_erp == matricula && el.monto_pagado ==  monto_pagado || el.matricula_erp == matricula && ( el.monto_pagado + el.saldofavor ) ==  monto_pagado )
        
        // Validar si el pago tiene el mismo monto
        if( mismoMonto ){

          // Agregar normal, y la clase vacía
          pagosFinalesNuevoERP.push({ ...mismoMonto, clase_pago: '' })

        }else{

          // Son diferentes montos
          const pagoEncontrado     = pagosErpNuevo.find( el => el.matricula_erp == matricula )

          // Aquí, hay que validar si el pago, ya existe, en el array, si ya existem solo hay que poner de que pues... falta
          const existeEnNuevoArray = pagosFinalesNuevoERP.find( el => el.matricula_erp == matricula && el.monto_pagado == pagoEncontrado.monto_pagado )

          if( existeEnNuevoArray ){
            pagosFinalesNuevoERP.push({
              matricula         : existeEnNuevoArray.matricula,
              matricula_erp     : existeEnNuevoArray.matricula_erp,
              alumno            : alumno,
              monto_pagado      : monto_pagado,
              saldofavor        : existeEnNuevoArray.saldofavor,
              grupo             : existeEnNuevoArray.grupo ,
              plantel           : existeEnNuevoArray.plantel ,
              nombre_completo   : existeEnNuevoArray.nombre_completo ,
              clase_pago        : 'orange white--text'
            })
          }else{
            pagosFinalesNuevoERP.push({
              matricula         : pagoEncontrado.matricula,
              matricula_erp     : pagoEncontrado.matricula_erp,
              alumno            : pagoEncontrado.alumno,
              monto_pagado      : pagoEncontrado.monto_pagado,
              saldofavor        : pagoEncontrado.saldofavor,
              grupo             : pagoEncontrado.grupo ,
              plantel           : pagoEncontrado.plantel ,
              nombre_completo   : pagoEncontrado.nombre_completo ,
              clase_pago        : 'orange white--text'
            })
          }

        }
      }
    }

    let cifraERPVIEJO = pagosERPViejo.map(item => item.monto_pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let cifraERPNUEVO = pagosFinalesNuevoERP.filter( el => { return el.alumno != 'FALTA'}).map(item => item.monto_pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)
    let saldosFavor   = pagosFinalesNuevoERP.filter( el => { return el.alumno != 'FALTA'}).map(item => item.saldofavor ).reduce((prev, curr) => prev + curr, 0).toFixed(2)

    cifraERPNUEVO = parseFloat( cifraERPNUEVO ) + parseFloat ( saldosFavor )

    let diferenteMonto = ( parseFloat( cifraERPVIEJO ) -  parseFloat( cifraERPNUEVO ) ).toFixed(2)

    return res.send({ pagosERPViejo, pagosFinalesNuevoERP, cifraERPVIEJO, cifraERPNUEVO, diferenteMonto })

  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor' })
  }
}

exports.getIngresosComparacionesCiclo = async ( req, res) => {
  try{

    const { id_ciclo } = req.body

    let pagosErpNuevo  = await inscripciones.getPagosERPCiclo( id_ciclo, 1 ).then( response => response )
    let pagosERPViejo  = await inscripciones.getPagosERPCiclo( id_ciclo, 2 ).then( response => response )

    // SACAR SI EN ESE CICLO LE CORRESPONDE O NO EL SALDO A FAVOR
    let mapMatriculas = pagosErpNuevo.map( alumnos => alumnos.matricula )

    mapMatriculas = mapMatriculas.length ? mapMatriculas : ['0']

    // CONSULTAR EL ÚLTIMO GRUPO 
    const ultimoGrupo = await inscripciones.getUltimoGruposCiclo( mapMatriculas, 1 ).then( response => response )

    // Recorremos los pagos del Nuevo ERP para saber si en ese grupo, existe un saldo a favor, por que podría ser de un grupo a fuuturo
    for( const i in pagosErpNuevo ){

      // Desestructuramos y sacamos, matricula y grupo
      const { matricula, saldofavor } = pagosErpNuevo[i]

      //comparamos y si la matricula y el grupo no existennn, se elimina el adeudo de ahí
      const adeudoCorresponde = ultimoGrupo.find( el => el.matricula == matricula && el.id_ciclo == id_ciclo )

      // Si aparece, dejamos el saldo a favor, si no, lo eliminamos
      pagosErpNuevo[i].saldofavor = adeudoCorresponde ? saldofavor : 0
    }

    const planteles = [... new Set(pagosErpNuevo.map(( registro ) => { return registro.plantel }))];

    let respuesta = []
    
    for( const i in planteles ){

      // Filtar los pagos del ERP viejito por plantel
      let pagosERPViejoPlantel = pagosERPViejo.filter( el => { return el.plantel == planteles[i] })

      // Sacar las matriculas del erp viejito ya por plantel
      let idMatriculas = pagosERPViejoPlantel.map(( registro ) => { return registro.matricula })

      // alumnos que están de más
      const alumnosExtras = pagosErpNuevo.filter( el => { !idMatriculas.includes( el.matricula_erp ) })

      // Arreglo de pagos finales
      let pagosFinalesNuevoERP = []

      // Recorremos ahora si los datossss
      for( const i in pagosERPViejoPlantel ){

        // Desestructuramos los pagos
        const { matricula, monto_pagado, grupo, plantel, nombre_completo, alumno } = pagosERPViejoPlantel[i]

        // Le agregar la matricula del ERP viejo, solo por acomodo, es todo
        pagosERPViejoPlantel[i]['matricula_erp'] = matricula


        // EXISTE EL PAGO EN EL NUEVO ERP?
        const existePagoAlumno = pagosErpNuevo.find( el => el.matricula_erp == matricula )

        if( !existePagoAlumno ){
          // NO EXISTE, DEBE AGREGARSE AL ARREGLO DE QUE FALTAAAA
          pagosFinalesNuevoERP.push({
            matricula,
            matricula_erp     : matricula,
            alumno            : 'FALTA',
            monto_pagado,
            saldofavor        : 0,
            grupo,
            plantel,
            nombre_completo,
            clase_pago: 'red white--text'
          })
        }else{

          // EL PAGO SI EXISTE, NECESITAMOS HACER MÁS VALIDACIONES

          // MISMOS MONTOS....
          const mismoMonto = pagosErpNuevo.find( el => el.matricula_erp == matricula && el.monto_pagado ==  monto_pagado || el.matricula_erp == matricula && ( el.monto_pagado + el.saldofavor ) ==  monto_pagado )
          
          // Validar si el pago tiene el mismo monto
          if( mismoMonto ){

            // Agregar normal, y la clase vacía
            pagosFinalesNuevoERP.push({ ...mismoMonto, clase_pago: '' })

          }else{

            // Son diferentes montos
            const pagoEncontrado     = pagosErpNuevo.find( el => el.matricula_erp == matricula )

            // Aquí, hay que validar si el pago, ya existe, en el array, si ya existem solo hay que poner de que pues... falta
            const existeEnNuevoArray = pagosFinalesNuevoERP.find( el => el.matricula_erp == matricula && el.monto_pagado == pagoEncontrado.monto_pagado )

            if( existeEnNuevoArray ){
              pagosFinalesNuevoERP.push({
                matricula         : existeEnNuevoArray.matricula,
                matricula_erp     : existeEnNuevoArray.matricula_erp,
                alumno            : alumno,
                monto_pagado      : monto_pagado,
                saldofavor        : existeEnNuevoArray.saldofavor,
                grupo             : existeEnNuevoArray.grupo ,
                plantel           : existeEnNuevoArray.plantel ,
                nombre_completo   : existeEnNuevoArray.nombre_completo ,
                clase_pago        : 'orange white--text'
              })
            }else{
              pagosFinalesNuevoERP.push({
                matricula         : pagoEncontrado.matricula,
                matricula_erp     : pagoEncontrado.matricula_erp,
                alumno            : pagoEncontrado.alumno,
                monto_pagado      : pagoEncontrado.monto_pagado,
                saldofavor        : pagoEncontrado.saldofavor,
                grupo             : pagoEncontrado.grupo ,
                plantel           : pagoEncontrado.plantel ,
                nombre_completo   : pagoEncontrado.nombre_completo ,
                clase_pago        : 'orange white--text'
              })
            }

          }
        }
      }

      let cifraERPVIEJO = pagosERPViejoPlantel.map(item => item.monto_pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)
      let cifraERPNUEVO = pagosFinalesNuevoERP.filter( el => { return el.plantel == planteles[i] && el.alumno != 'FALTA'}).map(item => item.monto_pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)
      let saldosFavor   = pagosFinalesNuevoERP.filter( el => { return el.plantel == planteles[i] && el.alumno != 'FALTA'}).map(item => item.saldofavor ).reduce((prev, curr) => prev + curr, 0).toFixed(2)

      cifraERPNUEVO = parseFloat( cifraERPNUEVO ) + parseFloat ( saldosFavor )

      let diferenteMonto = ( parseFloat( cifraERPVIEJO ) -  parseFloat( cifraERPNUEVO ) ).toFixed(2)

      console.log( cifraERPVIEJO,cifraERPNUEVO, diferenteMonto  )

      respuesta.push({
        plantel: planteles[i],
        cifraERPVIEJO,
        cifraERPNUEVO,
        diferenteMonto
      })

    }

    // return res.send({ pagosERPViejo, pagosFinalesNuevoERP, cifraERPVIEJO, cifraERPNUEVO, diferenteMonto })
    return res.send( respuesta )

  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor' })
  }
}


//-------Angel Rodriguez----------------//
exports.getComparacionCicloERP = async ( req, res) => {
  try{


    let { escuela } = req.body
    let ciclos      = []
    let planteles   = []

    if(escuela === 1){
   
      ciclos = [
        { id_ciclo: 234, columna: 1},
        { id_ciclo: 248, columna: 2},
        { id_ciclo: 250, columna: 3},
        { id_ciclo: 252, columna: 4}
      ];
  
      planteles  = await inscripciones.getPlantelesINBI( ).then( response => response )

    }

    else if (escuela === 2){

      ciclos = [
        { id_ciclo: 233, columna: 1},
        { id_ciclo: 238, columna: 2},
        { id_ciclo: 249, columna: 3},
        { id_ciclo: 251, columna: 4}
      ];
      
      planteles  = await inscripciones.getPlantelesFAST( ).then( response => response )
    }


  
    for( const i in planteles ){

      for (const { id_ciclo, columna } of ciclos){
      
        let pagosErpNuevo  = await inscripciones.getPagosERPCiclo( id_ciclo, 1 ).then( response => response )
        let pagosERPViejo  = await inscripciones.getPagosERPCiclo( id_ciclo, 2 ).then( response => response )

        // SACAR SI EN ESE CICLO LE CORRESPONDE O NO EL SALDO A FAVOR
        let mapMatriculas = pagosErpNuevo.map( alumnos => alumnos.matricula )

        mapMatriculas = mapMatriculas.length ? mapMatriculas : ['0']

        // CONSULTAR EL ÚLTIMO GRUPO 
        const ultimoGrupo = await inscripciones.getUltimoGruposCiclo( mapMatriculas, 1 ).then( response => response )

        // Recorremos los pagos del Nuevo ERP para saber si en ese grupo, existe un saldo a favor, por que podría ser de un grupo a fuuturo
        for( const i in pagosErpNuevo ){

          // Desestructuramos y sacamos, matricula y grupo
          const { matricula, saldofavor } = pagosErpNuevo[i]

          //comparamos y si la matricula y el grupo no existennn, se elimina el adeudo de ahí
          const adeudoCorresponde = ultimoGrupo.find( el => el.matricula == matricula && el.id_ciclo == id_ciclo )

          // Si aparece, dejamos el saldo a favor, si no, lo eliminamos
          pagosErpNuevo[i].saldofavor = adeudoCorresponde ? saldofavor : 0
        }

        // Filtar los pagos del ERP viejito por plantel
        let pagosERPViejoPlantel = pagosERPViejo.filter( el => { return el.plantel == planteles[i].plantel })
     
        // Sacar las matriculas del erp viejito ya por plantel
        let idMatriculas = pagosERPViejoPlantel.map(( registro ) => { return registro.matricula })

        // alumnos que están de más
        const alumnosExtras = pagosErpNuevo.filter( el => { !idMatriculas.includes( el.matricula_erp ) })

        // Arreglo de pagos finales
        let pagosFinalesNuevoERP = []

        // Recorremos ahora si los datossss
        for( const i in pagosERPViejoPlantel ){

          // Desestructuramos los pagos
          const { matricula, monto_pagado, grupo, plantel, nombre_completo, alumno } = pagosERPViejoPlantel[i]
  
          // Le agregar la matricula del ERP viejo, solo por acomodo, es todo
          pagosERPViejoPlantel[i]['matricula_erp'] = matricula
  
  
          // EXISTE EL PAGO EN EL NUEVO ERP?
          const existePagoAlumno = pagosErpNuevo.find( el => el.matricula_erp == matricula )
  
          if( !existePagoAlumno ){
            // NO EXISTE, DEBE AGREGARSE AL ARREGLO DE QUE FALTAAAA
            pagosFinalesNuevoERP.push({
              matricula,
              matricula_erp     : matricula,
              alumno            : 'FALTA',
              monto_pagado,
              saldofavor        : 0,
              grupo,
              plantel,
              nombre_completo,
              clase_pago: 'red white--text'
            })
          }else{
  
            // EL PAGO SI EXISTE, NECESITAMOS HACER MÁS VALIDACIONES
  
            // MISMOS MONTOS....
            const mismoMonto = pagosErpNuevo.find( el => el.matricula_erp == matricula && el.monto_pagado ==  monto_pagado || el.matricula_erp == matricula && ( el.monto_pagado + el.saldofavor ) ==  monto_pagado )
            
            // Validar si el pago tiene el mismo monto
            if( mismoMonto ){
  
              // Agregar normal, y la clase vacía
              pagosFinalesNuevoERP.push({ ...mismoMonto, clase_pago: '' })
  
            }else{
  
              // Son diferentes montos
              const pagoEncontrado     = pagosErpNuevo.find( el => el.matricula_erp == matricula )
  
              // Aquí, hay que validar si el pago, ya existe, en el array, si ya existem solo hay que poner de que pues... falta
              const existeEnNuevoArray = pagosFinalesNuevoERP.find( el => el.matricula_erp == matricula && el.monto_pagado == pagoEncontrado.monto_pagado )
  
              if( existeEnNuevoArray ){
                pagosFinalesNuevoERP.push({
                  matricula         : existeEnNuevoArray.matricula,
                  matricula_erp     : existeEnNuevoArray.matricula_erp,
                  alumno            : alumno,
                  monto_pagado      : monto_pagado,
                  saldofavor        : existeEnNuevoArray.saldofavor,
                  grupo             : existeEnNuevoArray.grupo ,
                  plantel           : existeEnNuevoArray.plantel ,
                  nombre_completo   : existeEnNuevoArray.nombre_completo ,
                  clase_pago        : 'orange white--text'
                })
              }else{
                pagosFinalesNuevoERP.push({
                  matricula         : pagoEncontrado.matricula,
                  matricula_erp     : pagoEncontrado.matricula_erp,
                  alumno            : pagoEncontrado.alumno,
                  monto_pagado      : pagoEncontrado.monto_pagado,
                  saldofavor        : pagoEncontrado.saldofavor,
                  grupo             : pagoEncontrado.grupo ,
                  plantel           : pagoEncontrado.plantel ,
                  nombre_completo   : pagoEncontrado.nombre_completo ,
                  clase_pago        : 'orange white--text'
                })
              }
            }
          }
        }

        let cifraERPVIEJO = pagosERPViejoPlantel.map(item => item.monto_pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)
        let cifraERPNUEVO = pagosFinalesNuevoERP.filter( el => { return el.plantel == planteles[i].plantel }).map(item => item.monto_pagado ).reduce((prev, curr) => prev + curr, 0).toFixed(2)
        let saldosFavor   = pagosFinalesNuevoERP.map(item => item.saldofavor ).reduce((prev, curr) => prev + curr, 0).toFixed(2)
  
        cifraERPNUEVO = parseFloat( cifraERPNUEVO ) + parseFloat ( saldosFavor )
             
        let diferenteMonto = ( parseFloat( cifraERPVIEJO ) -  parseFloat( cifraERPNUEVO ) ).toFixed(2)

        planteles[i][`columna${columna}`] = diferenteMonto
      }
        
    }
    // return res.send({ pagosERPViejo, pagosFinalesNuevoERP, cifraERPVIEJO, cifraERPNUEVO, diferenteMonto })
    return res.send(planteles)

  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor' })
  }
}

exports.generarRecibo = async ( req, res) => {
  try{

    newPDF( req.body );

    return res.send({ message: 'Recibo generado correctamente'})

  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor' })
  }
}


exports.updateCorreoAlumno = async ( req, res) => {
  try{

    const { email, unidad_negocio, id_alumno, alumno, matricula  } = req.body
    
    const actualizarCorreoAlumno  = await inscripciones.updateCorreoAlumno( req.body ).then( response => response )
    
    const correoHtml     = await helperReciboPago.plantillaReciboPago(  req.body  ) 

    const correoEnviado  = await helperReciboPago.enviarCorreo( 'RECIBO DE PAGO', correoHtml, email, unidad_negocio ).then( response=> response ) 


     // 1-. Validar si es NI
    // Consultar solo los inscripciones activos
    const getGruposAlumnoInscrito = await inscripciones.getGruposAlumnoInscrito( id_alumno ).then(response=> response)

    // 2-. Validar si es su primer pago
    // si es NI, solo tienen un pago, eso quiere decir que es NI
    if( getGruposAlumnoInscrito.length == 1 ){

    
      // 3-. Validar la escuela en base al grupo
      const grupoAlumno = getGruposAlumnoInscrito[0]

      // Desestrucutramos al grupo
      const { grupo } = grupoAlumno  

      // Declaramos los archivos vacíos para poder llenarlos
      let archivos = []

      // Validamos si es fast
      if( grupo.match('FAST') ){

        // Llenamos los archivos 
        archivos = [
          { path: 'https://fastenglish.com.mx/public/bienvenida/fast_horarios.png'},
          { path: 'https://fastenglish.com.mx/public/bienvenida/manual_usuario.pdf'},
          { path: 'https://fastenglish.com.mx/public/bienvenida/tabla_fonetica.mp4'}
        ]
        
        const correoHTML   = helperBienvenida.getCorreoBienvenida( alumno, matricula, 2 ) // enviamos 2, por que es la escuela 

        const enviarCorreo = helperBienvenida.enviarCorreo( correoHTML, email, transporter, archivos, 'FAST ENGLISH', user ).then( response => response )

      }else{

        archivos = [
          { path: 'https://www.inbi.mx/public/images/calendario_inbi.jpeg'},
          { path: 'https://www.inbi.mx/public/images/manual_usario.pdf'}
        ]
        
        const correoHTML   = helperBienvenida.getCorreoBienvenida( alumno, matricula, 1 ) // enviamos 2, por que es la escuela 

        const enviarCorreo = helperBienvenida.enviarCorreo( correoHTML, email, transporter2, archivos, 'INBI SCHOOL', userq ).then( response => response )
      }                           

    }    

    return res.send({ message: 'Correo actualizado' })

  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor' })
  }
}




exports.enviarCorreoBienvenida = async ( req, res) => {
  try{
    const { email, id_alumno, alumno, matricula } = req.body 


    // 1-. Validar si es NI
    // Consultar solo los inscripciones activos
    const getGruposAlumnoInscrito = await inscripciones.getGruposAlumnoInscrito( id_alumno ).then(response=> response)

    console.log( getGruposAlumnoInscrito )

    // 2-. Validar si es su primer pago
    // si es NI, solo tienen un pago, eso quiere decir que es NI
    // if( getGruposAlumnoInscrito.length == 1 ){

    
      // 3-. Validar la escuela en base al grupo
      const grupoAlumno = getGruposAlumnoInscrito[0]

      // Desestrucutramos al grupo
      const { grupo } = grupoAlumno  

      // Declaramos los archivos vacíos para poder llenarlos
      let archivos = []

      // Validamos si es fast
      if( grupo.match('FAST') ){
        console.log(1)

        // Llenamos los archivos 
        archivos = [
          { path: 'https://fastenglish.com.mx/public/bienvenida/fast_horarios.png'},
          { path: 'https://fastenglish.com.mx/public/bienvenida/manual_usuario.pdf'},
          { path: 'https://fastenglish.com.mx/public/bienvenida/tabla_fonetica.mp4'}
        ]
        
        const correoHTML   = helperBienvenida.getCorreoBienvenida( alumno, matricula, 2 ) // enviamos 2, por que es la escuela 

        const enviarCorreo = helperBienvenida.enviarCorreo( correoHTML, email, transporter, archivos, 'FAST ENGLISH', user ).then( response => response )

        return res.send( enviarCorreo )

      }else{
        console.log(2)

        archivos = [
          { path: 'https://www.inbi.mx/public/images/calendario_inbi.jpeg'},
          { path: 'https://www.inbi.mx/public/images/manual_usario.pdf'}
        ]
        
        const correoHTML   = helperBienvenida.getCorreoBienvenida( alumno, matricula, 1 ) // enviamos 2, por que es la escuela 

        const enviarCorreo = helperBienvenida.enviarCorreo( correoHTML, email, transporter2, archivos, 'INBI SCHOOL', userq ).then( response => response )

        return res.send( enviarCorreo )
      }  


    // }    
    

    return res.send({ message: 'no se envío'})

  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor' })
  }
}


// Angel Rodriguez
exports.getTutorAlumno = async(req, res) => {
  try {
  	const { id } = req.params
  	// Consultar solo los inscripciones activos
		const getInfo = await inscripciones.getTutorAlumno( id ).then(response=> response)

		// enviar los inscripciones
		res.send(getInfo);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
}


exports.getPreciosAlumnos = async(req, res) => {
  try {
  	const { id } = req.params
  	// Consultar solo los inscripciones activos
		const getInfo = await inscripciones.getPreciosAlumno( id ).then(response=> response)

		// enviar los inscripciones
		res.send(getInfo);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
}


exports.getAllPrecios = async(req, res) => {
  try {

  	// Consultar solo los inscripciones activos
		const getInfo = await inscripciones.getAllPrecios(  ).then(response=> response)

		// enviar los inscripciones
		res.send(getInfo);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
}


exports.updatePrecioAlumno = async(req, res) => {
  try{

    const { id, id_curso, id_alumno} = req.body

    const getInfo = await inscripciones.getPreciosAlumno( id ).then(response=> response)

    if(getInfo && getInfo.length > 0){

      const updateInfo= await inscripciones.updatePrecioAlumno( id_curso, id_alumno ).then(response=> response)

    } else {

      const registrarUltimoCurso  = await inscripciones.insertPrecioAlumno( id_alumno, id_curso ).then( response => response )

    }
    res.send({ message: 'La información se ha actualizado correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};
 

