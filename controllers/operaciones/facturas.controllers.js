const facturas = require("../../models/operaciones/facturas.model.js");
const inscripciones = require("../../models/operaciones/inscripciones.model.js");
const { v4: uuidv4 } = require('uuid')

/*********************************************************************************************************/
/*********************************************************************************************************/

exports.getDatosFiscales = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const datosFiscales = await facturas.getDatosFiscales( ).then( response => response )

    res.send( datosFiscales )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};


exports.getDatosAlumnos = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const datosAlumnos = await facturas.getDatosAlumnos( ).then( response => response )

    res.send( datosAlumnos )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};


exports.getMunicipios = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const municipios = await facturas.getMunicipios( ).then( response => response )

    res.send( municipios )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.getRegimenFiscal = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const regimen = await facturas.getRegimenFiscal( ).then( response => response )

    res.send( regimen )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};



exports.getActaAdministrativaUsuario = async(req, res) => {
  try{
  	const { id } = req.params
    // Desestrucutramos los datos
    const actasList = await facturas.getActaAdministrativaUsuario( id ).then( response => response )

    res.send( actasList )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};


exports.addDatosFiscales = async(req, res) => {
  try{

  	if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

    // Desestrucutramos los datos
    const actasList = await facturas.addDatosFiscales( req.body ).then( response => response )

    res.send({ message: 'Datos agregados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};


exports.updateDatosFiscales = async(req, res) => {
  try{

  	if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

    // Desestrucutramos los datos
    const actasList = await facturas.updateDatosFiscales( req.body ).then( response => response )

    res.send({ message: 'Datos actualizados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};


/****************************************/
/****************************************/
/****************************************/

exports.getFacturasSolicitadas = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const datosFiscales = await facturas.getFacturasSolicitadas( ).then( response => response )

    res.send( datosFiscales )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.getFacturasSolicitadasUsuario = async(req, res) => {
  try{
    const { id } = req.params
    // Desestrucutramos los datos
    const datosFiscales = await facturas.getFacturasSolicitadasUsuario( id ).then( response => response )

    res.send( datosFiscales )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.getUsoCFDI = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const datosFiscales = await facturas.getUsoCFDI( ).then( response => response )

    res.send( datosFiscales )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.getSucursalesFactura = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const datosFiscales = await facturas.getSucursalesFactura( ).then( response => response )

    res.send( datosFiscales )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.getCiclosFactura = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const datosFiscales = await facturas.getCiclosFactura( ).then( response => response )

    res.send( datosFiscales )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};



exports.solicitarFactura = async(req, res) => {
  try{

  	if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

    // Desestrucutramos los datos
    const actasList = await facturas.solicitarFactura( req.body ).then( response => response )

    res.send({ message: 'Datos agregados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};


exports.subirFactura = async(req, res) => {
  try{

  	if( !req.files ){
      return res.status( 400 ).send({ message : 'El contenido no puede ir vacio' })
    }

    const { id } = req.params

    // desestrucutramos los arvhios cargados
    const { files } = req.files

    const nombreSplit = files.name.split('.')
    
    // Obtener la extensión del archivo 
    const extensioArchivo = nombreSplit[ nombreSplit.length - 1 ]
    
    // modificar el nombre del archivo con un uuid unico
    const nombreUuid = uuidv4() + '.' + extensioArchivo
    
    // extensiones validas
    let extensiones  = [ 'XML', 'xml', 'PDF', 'pdf' ]
    let ruta         = './../../facturas/' + nombreUuid

    // validar que la extensión del archivo recibido sea valido
    if( !extensiones.includes( extensioArchivo ) )
      return res.status( 400 ).send({message: `Hola, la extensión: ${ extensioArchivo } no es valida, solo se aceptan: ${ extensiones }`})

		if( ['XML','xml'].includes( extensioArchivo ) ){
			const subirXML = facturas.subirXMLFactura( id, nombreUuid ).then( response => response )
		}else{
			const subirXML = facturas.subirPDFFactura( id, nombreUuid ).then( response => response )
		}

    files.mv(`${ruta}`, err => {
      if( err )
        return res.status( 400 ).send({ message: err })
      
      return res.send({ message: 'Imagen cargada correctamente', nombre: nombreUuid, extension: extensioArchivo })
    })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

		

exports.eliminarFactura = async(req, res) => {
  try{

    const { id } = req.params 
    // Desestrucutramos los datos
    const actasList = await facturas.eliminarFactura( id ).then( response => response )

    res.send({ message: 'Factura eliminada correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};
