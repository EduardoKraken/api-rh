const descuentos     = require("../../models/operaciones/descuentos.model.js");
const inscripciones  = require("../../models/operaciones/inscripciones.model");


exports.addDescuentoAlumno = async(req, res) => {
  try {

  	if (!req.body || Object.keys(req.body).length === 0)
  		return res.status( 400 ).send({ message: 'No puedes enviar datos vacios' })

    const addDescuentoAlumno = await descuentos.addDescuentoAlumno( req.body ).then(response => response)
    
    res.send({ message: 'Descuento generado correctamente' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.addDescuentoGrupo = async(req, res) => {
  try {

    let { opciones, alumnos } = req.body

    if (!req.body || Object.keys(req.body).length === 0)
      return res.status( 400 ).send({ message: 'No puedes enviar datos vacios' })

    for( const i in alumnos ){
      const { id_grupo, id_alumno } = alumnos[i]

      opciones.id_grupo_actual  = id_grupo
      opciones.id_alumno        = id_alumno

      const addDescuentoGrupo = await descuentos.addDescuentoAlumno( opciones ).then(response => response)
    }

    
    res.send({ message: 'Descuento generado correctamente' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.getDescuentosAlumno = async(req, res) => {
  try {

  	const { id } = req.params

    // consultar el grupo actual del alumno
    const getGrupoActual      = await inscripciones.getGrupoActual( id ).then(response=> response)

    let id_grupo = getGrupoActual ? getGrupoActual.id_grupo : ''

    const getDescuentosAlumno = await descuentos.getDescuentosAlumno( id, id_grupo ).then(response=> response)

    const suma = getDescuentosAlumno.map(item => item.monto ).reduce((prev, curr) => prev + curr, 0)

    const respuesta = {
    	id_alumno: id,
    	id_grupo,
    	grupo:     getGrupoActual.grupo,
    	suma,
    	getDescuentosAlumno
    }    

    res.send(respuesta);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

// OBTENER TODOs LOS DESCUENTOS
exports.getDescuentoAll = async(req, res) => {
  try {
    // consultar el grupo actual del alumno
    const getDescuentoAll      = await descuentos.getDescuentoAll( ).then(response=> response)

    res.send(getDescuentoAll);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.obtenerAlumnos = async(req, res) => {
  try {
    let getAlumnos         = await inscripciones.getListadoAlumnos( ).then(response=> response)

    res.send(getAlumnos);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

// OBETENER TODOS LOS GRUPOS ACTIVOS ACTUALMENTE
exports.obtenerGruposDescuento = async(req, res) => {
  try {
    let gruposActivos         = await descuentos.obtenerGruposDescuento( ).then(response=> response)

    const idgrupos = gruposActivos.map( registro => { return registro.id_grupo  })

    const alumnosGrupo = await descuentos.alumnosPorGrupo( idgrupos ).then(response=> response)

    for( const i in gruposActivos ){
      const { id_grupo } = gruposActivos[i]

      gruposActivos[i]['alumnos']     = alumnosGrupo.filter( el => { return el.id_grupo == id_grupo })
      gruposActivos[i]['costo_grupo'] = alumnosGrupo.filter( el => { return el.id_grupo == id_grupo }).map(item => item.pagado).reduce((prev, curr) => prev + curr, 0);
    
    }

    const respuesta = gruposActivos.filter( el => { return el.alumnos.length > 0 })

    res.send(respuesta);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

// OBETENER TODOS LOS GRUPOS ACTIVOS ACTUALMENTE
exports.eliminarDescuento = async(req, res) => {
  try {

    const { id } = req.params

    let eliminarDescuento = await descuentos.eliminarDescuento( id ).then(response=> response)

    res.send({ message: 'EliminadoCorrectamente' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};