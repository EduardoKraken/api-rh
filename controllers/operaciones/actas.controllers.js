const actas = require("../../models/operaciones/actas.model.js");

/*********************************************************************************************************/
/*********************************************************************************************************/

exports.getActaAdministrativaList = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const actasList = await actas.getActaAdministrativaList( ).then( response => response )

    res.send( actasList )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};


exports.getActaAdministrativaUsuario = async(req, res) => {
  try{
  	const { id } = req.params
    // Desestrucutramos los datos
    const actasList = await actas.getActaAdministrativaUsuario( id ).then( response => response )

    res.send( actasList )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};


exports.addActaAdministrativa = async(req, res) => {
  try{

  	if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

    // Desestrucutramos los datos
    const actasList = await actas.addActaAdministrativa( req.body ).then( response => response )

    res.send({ message: 'Datos agregados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};


exports.updateActaAdministrativa = async(req, res) => {
  try{

  	if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

    // Desestrucutramos los datos
    const actasList = await actas.updateActaAdministrativa( req.body ).then( response => response )

    res.send({ message: 'Datos actualizados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};



/*****************************************/
/*        FALTAS ADMINISTRATICAS         */
/*****************************************/

exports.getFaltasAdminList = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const actasList = await actas.getFaltasAdminList( ).then( response => response )

    res.send( actasList )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.addFaltasAdmin = async(req, res) => {
  try{

  	if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(500).send({ message: "El Contenido no puede estar vacio" });
	  }

    // Desestrucutramos los datos
    const actasList = await actas.addFaltasAdmin( req.body ).then( response => response )

    res.send({ message: 'Datos agregados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.updateFaltasAdmin = async(req, res) => {
  try{

  	if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

    // Desestrucutramos los datos
    const actasList = await actas.updateFaltasAdmin( req.body ).then( response => response )

    res.send({ message: 'Datos actualizados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};


/*****************************************/
/*      SANCIONES ADMINISTRATICAS        */
/*****************************************/

exports.getSancionesAdminList = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const actasList = await actas.getSancionesAdminList( ).then( response => response )

    res.send( actasList )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.addSancionesAdmin = async(req, res) => {
  try{

  	if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

    // Desestrucutramos los datos
    const actasList = await actas.addSancionesAdmin( req.body ).then( response => response )

    res.send({ message: 'Datos agregados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};

exports.updateSancionesAdmin = async(req, res) => {
  try{

  	if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

    // Desestrucutramos los datos
    const actasList = await actas.updateSancionesAdmin( req.body ).then( response => response )

    res.send({ message: 'Datos actualizados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};