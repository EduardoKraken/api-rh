const encuestas = require("../../models/operaciones/encuestas.model.js");

exports.getEncuestaPresencial = async(req, res) => {
  try {
    // Primero sacamos a todos los grupos que están en ese ciclo y funcionando 
    const getEncuestaPresencialFast  = await encuestas.getEncuestaPresencialFast( ).then(response => response)
    // Sacar la cantidad de alumnos que debieron contestar la encuesta
    const getAlumnosPorEncuestarFast  = await encuestas.getAlumnosPorEncuestarFast( ).then(response => response)

    // Ahora a los alumnos de esos grupos
    const getEncuestaPresencialInbi  = await encuestas.getEncuestaPresencialInbi( ).then(response => response)
    // Sacar la cantidad de alumnos que debieron contestar la encuesta
    const getAlumnosPorEncuestarInbi  = await encuestas.getAlumnosPorEncuestarInbi( ).then(response => response)
    

    res.send({
      fast: getEncuestaPresencialFast, 
      alumnosFast: getAlumnosPorEncuestarFast.length, 
      inbi: getEncuestaPresencialInbi,
      alumnosInbi: getAlumnosPorEncuestarInbi.length
    });

  } catch (error) {
    res.status(500).send({message:error})
  }
};