const alumnos    = require("../../models/operaciones/alumnos.model");
const catalogos  = require("../../models/lms/catalogoslms.model.js");


// Obtener los alumnos inscritos
exports.addAlumnoErp = async(req, res) => {
  try {
  	if (!req.body || Object.keys(req.body).length === 0)
  		return res.status( 400 ).send({ message: 'No puedes enviar datos vacios' })

  	// Primeto validaremos si tiene un id_empleado
  	const { id_empleado, tutor, unidad_negocio, id_contacto } = req.body

  	// Si el empleado no esta vacío, validar que otro alumno no tenga ese empleado
  	if( id_empleado ){
  		
      // Obtener los alumnos del empleado
  		const getAlumnoEmpleado = await alumnos.getAlumnoEmpleado( id_empleado ).then( response => response )

  		if( getAlumnoEmpleado ) 
  			return res.status( 400 ).send({ message: `Error, el empleado ya cuenta con un alumno ${ getAlumnoEmpleado.matricula }` })
  	}

    // Agregar el alumno
    let addAlumno = await alumnos.addAlumno( req.body ).then(response=> response)

    // Desestructuramos el objeto 
    const { id } = addAlumno

    const cerrarProspecto = await alumnos.cerrarProspecto(  id, 1, id_contacto ).then( response => response )

    // Le agregamos el 0 al final del ID
    const matricula = id.toString().padStart(5,'0')
    const updateMatricula = await alumnos.updateMatricula( matricula, id ).then(response=> response)

    if( tutor ){
    	const addAlumnoTutor = await alumnos.addAlumnoTutor( tutor, id ).then(response=> response)
    }

    // consultar al alumno en generla
    const alumno = await alumnos.getAlumnoInscrito( id ).then( response => response )

    // Hacemos una peticion async para obtener los datos de ambas bases de datos
    const usuariosLMS = await catalogos.getUsuariosLMSFAST( unidad_negocio ).then(response => response)

    // Buscamos si ya esta el usuario
    const usuario = usuariosLMS.find(el => el.iderp == alumno.id_alumno)
    
    // Si no esta, se dede insertar
    if(!usuario){

      // Validamos que esten los datos correctos y agregamos el grupo
      // Para agregar al usuario se manda como parametro el usuario del erp
      const usuarioAddLMS    = await catalogos.usuariosAddFast( alumno, unidad_negocio ).then(response => response)

      // Para agregar el alumno mandamos como parametros los datos del usuario
      const addAlumnoLMS     = await catalogos.addAlumnoFast( usuarioAddLMS, unidad_negocio ).then(response => response)
    }

    // enviar los alumnos
    res.send( alumno );

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};



exports.addAlumnosHermanos = async(req, res) => {
  try {
    if (!req.body || Object.keys(req.body).length === 0)
      return res.status( 400 ).send({ message: 'No puedes enviar datos vacios' })

    // Primeto validaremos si tiene un id_empleado
    const { hermanos, tutor } = req.body

    let id_alumnos = []
    let id_tutor   = 0

    for( const i in hermanos ){
      // Agregar el alumno
      let addAlumno = await alumnos.addAlumno( hermanos[i] ).then(response=> response)

      // Desestructuramos el objeto 
      const { id } = addAlumno
      id_alumnos.push( id )

      // Le agregamos el 0 al final del ID
      const matricula = id.toString().padStart(5,'0')
      const updateMatricula = await alumnos.updateMatricula( matricula, id ).then(response=> response)

      if( tutor ){
        const addAlumnoTutor = await alumnos.addAlumnoTutor( tutor, id ).then(response=> response)
        id_tutor = addAlumnoTutor.id
      }
    }

    for( const i in id_alumnos ){
      // Agregar el alumno
      let addHermano = await alumnos.addHermano( id_tutor, id_alumnos[i]  ).then(response=> response)
    }

    const alumnosHermanos = await alumnos.getAlumnosInscrito( id_alumnos  ).then(response=> response)

    // enviar los alumnos
    res.send( alumnosHermanos );

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

exports.addAlumnoTutor = async(req, res) => {
  try {
  	if (!req.body || Object.keys(req.body).length === 0)
  		return res.status( 400 ).send({ message: 'No puedes enviar datos vacios' })
    // Consultar solo los alumnos activos
    const addAlumnoTutor = await alumnos.addAlumnoTutor( req.body ).then(response=> response)

    // enviar los alumnos
    res.send(addAlumnoTutor);

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};



exports.getVideosAlumnos = async(req, res) => {
  try {
    const videosFast = await alumnos.getVideosEscuela( 2 ).then( response => response )
    const videosInbi = await alumnos.getVideosEscuela( 1 ).then( response => response )

    const respuesta = videosFast.concat( videosInbi )
    // enviar los alumnos
    res.send(respuesta);

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};
