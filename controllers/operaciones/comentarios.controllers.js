const comentarios = require("../../models/operaciones/comentarios.model.js");
const catalogos   = require("../../models/lms/catalogoslms.model.js");


/*********************************************************************************************************/
/*********************************************************************************************************/
// RI por grupos

exports.getAlumnosCicloActual = async(req, res) => {
  try {
  	const { cicloFast }  = await comentarios.cicloActualFAST( ).then(response => response)
  	const { cicloInbi }  = await comentarios.cicloActualINBI( ).then(response => response)
    

    const alumnosFast    = await comentarios.getAlumnosCicloActualFast( cicloFast ).then(response=> response)
    const alumnosInbi    = await comentarios.getAlumnosCicloActualInbi( cicloInbi ).then(response=> response)

    const comentario     = await comentarios.getCantidadComentarios().then(response=> response)

    for(const i in alumnosFast){
      const { id_alumno } = alumnosFast[i]

      // Comentarios teacher
      const teacher = comentario.filter(el=>{
        if(el.id_alumno == id_alumno && el.idperfil == 10 && el.unidad_negocio == 2){return true}
      })

      const supervisora = comentario.filter(el=>{
        if(el.id_alumno == id_alumno && el.idperfil == 17 && el.unidad_negocio == 2){return true}
      })

      const ventas = comentario.filter(el=>{
        if(el.id_alumno == id_alumno && el.idperfil == 18 && el.unidad_negocio == 2){return true}
      })

      const recep = comentario.filter(el=>{
        if(el.id_alumno == id_alumno && el.idperfil == 19 && el.unidad_negocio == 2){return true}
      })

      const admin = comentario.filter(el=>{
        if(el.id_alumno == id_alumno && el.idperfil == 12 && el.unidad_negocio == 2
          || el.id_alumno == id_alumno && el.idperfil == 40 && el.unidad_negocio == 2
          || el.id_alumno == id_alumno && el.idperfil == 43 && el.unidad_negocio == 2){return true}
      })

      alumnosFast[i] = {
        ...alumnosFast[i],
        cantTeacher: teacher.length,
        cantRecep:   recep.length,
        cantSupervi: supervisora.length,
        cantVentas:  ventas.length,
        cantAdmin:   admin.length,
      }
    }


    for(const i in alumnosInbi){
      const { id_alumno } = alumnosInbi[i]

      // Comentarios teacher
      const teacher = comentario.filter(el=>{
        if(el.id_alumno == id_alumno && el.idperfil == 10 && el.unidad_negocio == 1){return true}
      })

      const supervisora = comentario.filter(el=>{
        if(el.id_alumno == id_alumno && el.idperfil == 17 && el.unidad_negocio == 1){return true}
      })

      const ventas = comentario.filter(el=>{
        if(el.id_alumno == id_alumno && el.idperfil == 18 && el.unidad_negocio == 1){return true}
      })

      const recep = comentario.filter(el=>{
        if(el.id_alumno == id_alumno && el.idperfil == 19 && el.unidad_negocio == 1){return true}
      })

      const admin = comentario.filter(el=>{
        if(el.id_alumno == id_alumno && el.idperfil == 12 && el.unidad_negocio == 1
          || el.id_alumno == id_alumno && el.idperfil == 40 && el.unidad_negocio == 1
          || el.id_alumno == id_alumno && el.idperfil == 43 && el.unidad_negocio == 1){return true}
      })

      alumnosInbi[i] = {
        ...alumnosInbi[i],
        cantTeacher: teacher.length,
        cantRecep:   recep.length,
        cantSupervi: supervisora.length,
        cantVentas:  ventas.length,
        cantAdmin:   admin.length,
      }
    }

    const respuestaFinal = alumnosFast.concat(alumnosInbi)

    res.send(respuestaFinal);
  } catch (error) {
    res.status(500).send({message:error})
  }
};

/* AGREGAR UN COMENTARIO DEL ALUMNO */
exports.addComentarioAlumno = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  comentarios.addComentarioAlumno(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al solicitar el cambio"
      })
    else res.status(200).send({ message: `Se agrego el comentario correctamente` });
  })
};

/* COMENTARIO DEL ALUMNO */


exports.getComentarioAlumno = async(req, res) => {
  try {
		const { id, escuela } = req.params    

    let comentario    = await comentarios.getComentarioAlumno( id, escuela ).then(response=> response)

    const usuarios    = await comentarios.getUsuariosActivos().then(response=>response)

    for(const i in comentario){
    	const usuario = usuarios.find(el=> el.id_usuario == comentario[i].id_usuarioerp)
    	if(usuario){
    		comentario[i] = {
    			...comentario[i],
    			usuario: usuario.nombre_completo
    		}
    	}
    }

    res.send(comentario);
  } catch (error) {
    res.status(500).send({message:error})
  }
};

/* ALUMNOS DEL TEACHER */

exports.getAlumnosTeacher = async(req, res) => {
  try {
  	const { cicloFast }  = await comentarios.cicloActualFAST( ).then(response => response)
  	const { cicloInbi }  = await comentarios.cicloActualINBI( ).then(response => response)

  	const { email } = req.body

    // Hacemos una peticion async para obtener los datos de ambas bases de datos
		const gruposTeachersFast = await comentarios.getAlumnosTeacherFast( email, cicloFast ).then(response => response)
		const gruposTeachersInbi = await comentarios.getAlumnosTeacherInbi( email, cicloInbi ).then(response => response)

		// Generamos la repsuesta
		const respuesta = {
			gruposTeachersFast,
			gruposTeachersInbi
		}

		// Envíamos la respuesta
		res.send(respuesta);

  } catch (error) {
    res.status(500).send({message:error})
  }
};