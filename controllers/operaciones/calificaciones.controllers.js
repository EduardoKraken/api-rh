const calificaciones = require("../../models/operaciones/calificaciones.model.js");

/*********************************************************************************************************/
/*********************************************************************************************************/

exports.getCalificacionesAlumno = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const { id_alumno, id_grupo, id_nivel, escuela, id_curso } = req.body

    const grupo = await calificaciones.existeGrupo( id_grupo, escuela ).then( response => response )


    // Formato en español 
  	const fechaEspaniol    = await calificaciones.fechaEspaniol( escuela ).then( response => response )
    
    // Consultar las asistencias del alumno
  	const asistencia = await calificaciones.getAsistenciaUsuario( id_alumno, id_grupo, escuela ).then( response=> response )

  	/*********************** A S I S T E N C I A S ******************************/
  	const ptsPorAsistencia = grupo.valor_asistencia / grupo.total_dias_laborales

  	/*
  		Inidicador      Valor
			1-. Asistencia  100
			2-. Falta       0
			3-. Retardo     50
			4-. Justificada 0
  	*/

  	// Puntajes
  	const pts_asistencia       = ptsPorAsistencia * asistencia.filter( el=> { return el.valor_asistencia == 1}).length
  	// El retardo vale .5 puntos
  	const pts_retardo          = ((ptsPorAsistencia * asistencia.filter( el=> { return el.valor_asistencia == 3}).length) / 2)
  	const pts_total_asistencia = (pts_asistencia + pts_retardo ).toFixed(1)

  	// Desglose, para poder mostrar los días en caso de que se necesite
  	const desglose_asistencias = {
  		pts_total_asistencia,
  		limite:       grupo.valor_asistencia,
  		asistencias:  asistencia.filter( el=> { return el.valor_asistencia == 1}),
  		faltas:       asistencia.filter( el=> { return el.valor_asistencia == 2}),
  		retardo:      asistencia.filter( el=> { return el.valor_asistencia == 3}),
  		justificadas: asistencia.filter( el=> { return el.valor_asistencia == 4}),
  		desgloce:     asistencia
  	} 

  	/****************************** E X A M E N E S *****************************/
  	const ptsPorExamen = grupo.valor_examenes / grupo.num_examenes

  	/*
			TIPO DE RECURSOS
			1-. Ejercicio
			2-. Examen normal
			3-. Examen recuperacion
			4-. Examen extraordinario
			0-. No es valido
  	*/
  	// Necesitamos sacar el id curso
  	const recursosContestados = await calificaciones.recursosContestados( id_alumno, id_grupo, id_nivel, id_curso, escuela ).then( response=> response )

  	// Sacar los examenes de medio termino y final
  	let examenesConstados = recursosContestados.filter(el=> { return el.tipo == 2 || el.tipo == 3 })
  	let   sumaCalifExamenes = 0
  	// Hacemos la sumatoria
  	for( const i in examenesConstados){ sumaCalifExamenes += ( examenesConstados[i].calificacion / 100) * ptsPorExamen }

  	// Cambiamos los nombres de las lecciones por el recurso
  	for( const i in examenesConstados){ examenesConstados[i].session = examenesConstados[i].recurso }

  	// Calculamos el total 
  	const pts_total_examenes = ( sumaCalifExamenes ).toFixed(0)

  	// Sacar si presento el extraordinario
    let extraordinario           = await calificaciones.recursoExtraordinarioContestado( id_alumno, id_grupo, escuela ).then( response => response )
    // Validamos si lo hizo o no el extraordinario
    if( extraordinario ){
    	extraordinario.session = extraordinario.recurso
    	examenesConstados.push(extraordinario)
    }

  	const examenes = {
  		pts_total_examenes,
  		limite:   grupo.valor_examenes,
  		desgloce: examenesConstados
  	}

  	/*************************** E J E R C I C I O S ****************************/
  	const ptsPorEjercicio = grupo.valor_ejercicios / grupo.num_ejercicios

  	const ejerciciosContestados = recursosContestados.filter(el=> { return el.tipo == 1 })
  	let   sumaCalifEjercicios = 0
  	// Hacemos la sumatoria
  	for( const i in ejerciciosContestados){ sumaCalifEjercicios += ( ejerciciosContestados[i].calificacion / 100 ) * ptsPorEjercicio }
  	// Calculamos el total 
  	const pts_total_ejercicios = ( sumaCalifEjercicios ).toFixed(1)

  	const calificacion_total = ( sumaCalifEjercicios + sumaCalifExamenes + ( pts_asistencia + pts_retardo )).toFixed(1)

  	const ejercicios = {
    	pts_total_ejercicios,
  		limite: grupo.valor_ejercicios,
  		desgloce: ejerciciosContestados
  	}

		// Respuesta 
    res.send({
    	grupo,
    	asistencias: desglose_asistencias,
    	examenes,
    	ejercicios,
    	calificacion_total,
    	recursosContestados
    })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al consultar las asistencias', error: error.message } )
  }
};

// Habilitar plataforma
exports.habilitarPlataforma = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const { idkardex, id_grupo, escuela } = req.body

    const grupo  = await calificaciones.habiltiarGrupo( id_grupo, escuela ).then( response => response )
    const cardex = await calificaciones.habiltiarKardex( idkardex, escuela ).then( response => response )

    res.send({ message: 'Habilitar plataforma' })
  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al consultar las asistencias', error: error.message } )
  }
};



exports.actualizarCalificacion = async(req, res) => {
  try{
    // Desestrucutramos los datos
    const { idkardex, id_grupo, escuela, calificacion_final_primera_oportunidad, calificacion_final_segunda_oportunidad } = req.body

    const cardex = await calificaciones.actualizarCalificacion( idkardex, calificacion_final_primera_oportunidad, calificacion_final_segunda_oportunidad, escuela ).then( response => response )

    res.send({ message: 'Habilitar plataforma' })
  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al consultar las asistencias', error: error.message } )
  }
};


exports.getEstatusAlumno = async(req, res) => {
  try{
    // Desestrucutramos los datos
    let { id_alumno, escuela } = req.body 
    let id_grupo, id_nivel, id_curso, iderp  = 0;

    // Consultar el último alumno
    const alumno = await calificaciones.ultimoAlumnoAsistio( id_alumno ).then( response => response )  

    if( !alumno ){
      return res.status( 500 ).send({message: 'No existe el alumno'})
    }

    iderp   = alumno ? alumno.id_alumno      : 0 
    escuela = alumno ? alumno.unidad_negocio : 0

    // Consultar el alumno en el lms
    const alumnosSistema = await calificaciones.getAlumnoSistema( iderp, escuela ).then( response => response )
   
    id_alumno = alumnosSistema ? alumnosSistema.id : 0
    
    // Consultar su último grupo
    const grupoEscuela = await calificaciones.getUltimoGrupo( id_alumno, escuela ).then( response => response )
   
    id_grupo = grupoEscuela ? grupoEscuela.id       : 0
    id_nivel = grupoEscuela ? grupoEscuela.id_nivel : 0
    id_curso = grupoEscuela ? grupoEscuela.id_curso : 0

//------------------------------------------------------------------------------------------------------------------------
    const maestros = await calificaciones.getMaestrosGrupo(id_grupo, escuela).then(response => response)


    const grupoActual = await calificaciones.getGrupoActual(iderp ).then( response => response )  
    
    idgrupo   = grupoActual ? grupoActual.id_grupo      : 0 
  

    
    const proximoGrupo = await calificaciones.getProximoGrupoAlumno( idgrupo ).then( response => response )

//------------------------------------------------------------------------------------------------------------------------
      

    // const grupo = await calificaciones.getProximoGrupoAlumno( id_grupo, escuela ).then( response => response )
    const grupo = await calificaciones.existeGrupo( id_grupo, escuela ).then( response => response )


    // Formato en español 
    const fechaEspaniol    = await calificaciones.fechaEspaniol( escuela ).then( response => response )
    
    // Consultar las asistencias del alumno
    const asistencia = await calificaciones.getAsistenciaUsuario( id_alumno, id_grupo, escuela ).then( response=> response )
  
    /*********************** A S I S T E N C I A S ******************************/
    const ptsPorAsistencia = grupo.valor_asistencia / grupo.total_dias_laborales
   
   
    /*
      Inidicador      Valor
      1-. Asistencia  100
      2-. Falta       0
      3-. Retardo     50
      4-. Justificada 0
    */

    // Puntajes
    const pts_asistencia       = ptsPorAsistencia * asistencia.filter( el=> { return el.valor_asistencia == 1}).length
    // El retardo vale .5 puntos
    const pts_retardo          = ((ptsPorAsistencia * asistencia.filter( el=> { return el.valor_asistencia == 3}).length) / 2)
    const pts_total_asistencia = ( pts_asistencia + pts_retardo ).toFixed(1)

    // Desglose, para poder mostrar los días en caso de que se necesite
    const desglose_asistencias = {
      pts_total_asistencia,
      limite:       grupo.valor_asistencia,
      asistencias:  asistencia.filter( el=> { return el.valor_asistencia == 1}),
      faltas:       asistencia.filter( el=> { return el.valor_asistencia == 2}),
      retardo:      asistencia.filter( el=> { return el.valor_asistencia == 3}),
      justificadas: asistencia.filter( el=> { return el.valor_asistencia == 4}),
      desgloce:     asistencia
    } 

    /****************************** E X A M E N E S *****************************/
    const ptsPorExamen = grupo.valor_examenes / grupo.num_examenes

    /*
      TIPO DE RECURSOS
      1-. Ejercicio
      2-. Examen normal
      3-. Examen recuperacion
      4-. Examen extraordinario
      0-. No es valido
    */
    // Necesitamos sacar el id curso
    const recursosContestados = await calificaciones.recursosContestados( id_alumno, id_grupo, id_nivel, id_curso, escuela ).then( response=> response )

    // Sacar los examenes de medio termino y final
    let examenesConstados = recursosContestados.filter(el=> { return el.tipo == 2 || el.tipo == 3 })
    let   sumaCalifExamenes = 0
    // Hacemos la sumatoria
    for( const i in examenesConstados){ sumaCalifExamenes += ( examenesConstados[i].calificacion / 100) * ptsPorExamen }

    // Cambiamos los nombres de las lecciones por el recurso
    for( const i in examenesConstados){ examenesConstados[i].session = examenesConstados[i].recurso }

    // Calculamos el total 
    const pts_total_examenes = ( sumaCalifExamenes ).toFixed(0)

    // Sacar si presento el extraordinario
    let extraordinario           = await calificaciones.recursoExtraordinarioContestado( id_alumno, id_grupo, escuela ).then( response => response )
    // Validamos si lo hizo o no el extraordinario
    if( extraordinario ){
      extraordinario.session = extraordinario.recurso
      examenesConstados.push(extraordinario)
    }

    const examenes = {
      pts_total_examenes,
      limite:   grupo.valor_examenes,
      desgloce: examenesConstados
    }

    /*************************** E J E R C I C I O S ****************************/
    const ptsPorEjercicio = grupo.valor_ejercicios / grupo.num_ejercicios

    const ejerciciosContestados = recursosContestados.filter(el=> { return el.tipo == 1 })
    let   sumaCalifEjercicios = 0
    // Hacemos la sumatoria
    for( const i in ejerciciosContestados){ sumaCalifEjercicios += ( ejerciciosContestados[i].calificacion / 100 ) * ptsPorEjercicio }
    // Calculamos el total 
    const pts_total_ejercicios = ( sumaCalifEjercicios ).toFixed(1)

    const calificacion_total = ( sumaCalifEjercicios + sumaCalifExamenes + ( pts_asistencia + pts_retardo )).toFixed(1)

    const ejercicios = {
      pts_total_ejercicios,
      limite: grupo.valor_ejercicios,
      desgloce: ejerciciosContestados
    }


    //////////////////////////////////////////////////////////////////*
    const avatar = await calificaciones.getAvatar ( id_alumno, escuela ).then( response=> response )


    // Respuesta 
    res.send({
      proximoGrupo,
      maestros,
      grupoEscuela,
      grupo,
      asistencias: desglose_asistencias,
      examenes,
      ejercicios,
      calificacion_total,
      recursosContestados,
      alumno: alumnosSistema,
      avatar: avatar ? avatar : null
    })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al consultar las asistencias', error: error.message } )
  }
};