var fs = require('fs');
const Auxiliares = require("../../models/tickets/auxiliares.models.js");

// Crear un Auxiliar
exports.addAuxiliar = (req, res) => {
    // Validacion de request
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio"
        });
    }

    // Guardar el Auxiliar en la BD
    Auxiliares.addAuxiliar(req.body, (err, data) => {
        // EVALUO QUE NO EXISTA UN ERROR
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la serie"
            })
        else res.send(data)
    })
};

exports.getAuxiliares = (req, res) => {
    Auxiliares.getAuxiliares((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los Auxiliares"
            });
        else res.send(data);
    });
};

exports.getAuxiliares_PorArea = (req, res) => {
    Auxiliares.getAuxiliares_PorArea(req.params.idauxi_area, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los Auxiliares"
            });
        else res.send(data);
    });
};


exports.putAuxiliares = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio!"
        });
    }
    Auxiliares.putAuxiliares(req.params.idauxi_area, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `No encontre la subserie con el id ${req.params.idauxi_area }.`
                });
            } else {
                res.status(500).send({
                    message: "Error al actualizar la subserie con el id" + req.params.idauxi_area
                });
            }
        } else res.send(data);
    });
}