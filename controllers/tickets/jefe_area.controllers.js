var fs = require('fs');
const Jefe_areas    = require("../../models/tickets/jefe_area.models.js");
const departamentos = require("../../models/departamentos.model.js");
const tickets       = require("../../models/tickets/ticket.models.js");

// Crear un Jefe_area
exports.addJefe_area = (req, res) => {
    // Validacion de request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio"
    });
  }

    // Guardar el Jefe_area en la BD
  Jefe_areas.addJefe_area(req.body, (err, data) => {
        // EVALUO QUE NO EXISTA UN ERROR
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la serie"
      })
    else res.send(data)
  })
};

exports.getJefe_areas_PorArea = (req, res) => {
  Jefe_areas.getJefe_areas_PorArea(req.params.idjefe_area,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Jefe_areas"
      });
    else res.send(data);
  });
};

exports.getJefe_areas = async (req, res) => {
  try{

    // Consultar todos los tickets
    let listDepartamentos = await departamentos.listDepartamentos( ).then( response => response )
    let listPuestos       = await departamentos.listPuestos( ).then( response => response )

    const getJefesArea    = await tickets.getJefesArea( ).then( response => response )

    // Obtner el puesto real de cada usuario
    let idUsuarios = getJefesArea.map((registro) => { return registro.id_usuario })

    const puestosUsuario = await tickets.getPuestosUsuarios( ).then( response => response )


    for( const i in getJefesArea ){

      const { id_usuario } = getJefesArea[i]

      let existePuesto = puestosUsuario.find( el => el.iderp == id_usuario )

      getJefesArea[i].idpuesto   = existePuesto ? existePuesto.idpuesto   : 0

    }

    for( const i in getJefesArea ){

      const { idpuesto } = getJefesArea[i]

      let existePuesto = listPuestos.find( el => el.idpuesto == idpuesto )

      getJefesArea[i]['departamento']   = existePuesto ? existePuesto.departamento   : ''
      getJefesArea[i]['iddepartamento'] = existePuesto ? existePuesto.iddepartamento : ''

    }

    // Falta consultar las areas, los jefes de area y consultar los auxiliares
    res.send({ areas: getJefesArea, puestos: listPuestos, puestosUsuario});
  }catch ( error ) {
    res.status(500).send({ message: error.message || "Se produjo algún error al recuperar los Tickets" });
  }
};

exports.putJefe_areas = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  Jefe_areas.putJefe_areas(req.params.idjefe_area, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre la subserie con el id ${req.params.idjefe_area }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar la subserie con el id" + req.params.idjefe_area
        });
      }
    } else res.send(data);
  });
}