var fs = require('fs');
const Usuarios = require("../../models/tickets/usuario.models.js");

// Crear un Usuario
exports.addRegistro = (req, res) => {
  // Validacion de request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio"
    });
  }

  // Guardar el Usuario en la BD
  Usuarios.addRegistro(req.body, (err, data) => {
    // EVALUO QUE NO EXISTA UN ERROR
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la serie"
      })
    else res.send(data)
  })
};

exports.getUsuariosERP = (req, res) => {
  Usuarios.getUsuariosERP((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Usuarios"
      });
    else res.send(data);
  });
};

exports.getRegistro = (req, res) => {
  Usuarios.getRegistro((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Usuarios"
      });
    else res.send(data);
  });
};


exports.updateRegistro = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  Usuarios.updateRegistro(req.params.id, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre la subserie con el id ${req.params.id }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar la subserie con el id" + req.params.id
        });
      }
    } else res.send(data);
  });
}

exports.getUsuarioId = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Usuarios.getUsuarioId(req.params.idusuarios,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los usuarios"
      });
    else res.send(data);
  });
};