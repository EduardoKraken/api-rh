var fs = require('fs');
const Reportes = require("../../models/tickets/reportes.models.js");

exports.getTotalTickets = (req, res) => {
    Reportes.getTotalTickets((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los Tickets"
            });
        else res.send(data);
    });
};

exports.getTotalTicketsINBI = (req, res) => {
    Reportes.getTotalTicketsINBI((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los tickets de INBI"
            });
        else res.send(data);
    });
};

exports.getTotalTicketsFASTENGLISH = (req, res) => {
    Reportes.getTotalTicketsFASTENGLISH((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los tickets de Fast English"
            });
        else res.send(data);
    });
};

exports.getEstatus1 = (req, res) => {
    Reportes.getEstatus1((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los datos"
            });
        else res.send(data);
    });
};
exports.getEstatus2 = (req, res) => {
    Reportes.getEstatus2((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los datos"
            });
        else res.send(data);
    });
};
exports.getEstatus3 = (req, res) => {
    Reportes.getEstatus3((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los datos"
            });
        else res.send(data);
    });
};
exports.getEstatus4 = (req, res) => {
    Reportes.getEstatus4((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los datos"
            });
        else res.send(data);
    });
};
exports.getEstatus5 = (req, res) => {
    Reportes.getEstatus5((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los datos"
            });
        else res.send(data);
    });
};
exports.getEstatus6 = (req, res) => {
    Reportes.getEstatus6((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los datos"
            });
        else res.send(data);
    });
};
exports.getEstatus7 = (req, res) => {
    Reportes.getEstatus7((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los datos"
            });
        else res.send(data);
    });
};

exports.getTicketsAreaTiempo = (req, res) => {
    Reportes.getTicketsAreaTiempo((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los datos"
            });
        else res.send(data);
    });
};

exports.getTicketsPorArea = (req, res) => {
    Reportes.getTicketsPorArea((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los datos"
            });
        else res.send(data);
    });
};


exports.getEstatus1_area = (req, res) => {
    Reportes.getEstatus1_area(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los Tickets"
            });
        else res.send(data);
    });
};

exports.getEstatus2_area = (req, res) => {
    Reportes.getEstatus2_area(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los Tickets"
            });
        else res.send(data);
    });
};

exports.getEstatus3_area = (req, res) => {
    Reportes.getEstatus3_area(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los Tickets"
            });
        else res.send(data);
    });
};

exports.getEstatus4_area = (req, res) => {
    Reportes.getEstatus4_area(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los Tickets"
            });
        else res.send(data);
    });
};

exports.getEstatus5_area = (req, res) => {
    Reportes.getEstatus5_area(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los Tickets"
            });
        else res.send(data);
    });
};

exports.getEstatus6_area = (req, res) => {
    Reportes.getEstatus6_area(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los Tickets"
            });
        else res.send(data);
    });
};

exports.getEstatus7_area = (req, res) => {
    Reportes.getEstatus7_area(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los Tickets"
            });
        else res.send(data);
    });
};

