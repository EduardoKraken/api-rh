var fs = require('fs');
const tickets           = require("../../models/tickets/ticket.models.js");
const historial_tickets = require("../../models/tickets/historial_ticket.models.js");
const { v4: uuidv4 }    = require('uuid')


// Crear un Ticket
exports.addTicket = async (req, res) => {
  try{

    const { id_usuario, motivo, id_suc, fotos } = req.body

    if( req.body.id_usuario == 0 ){
      return res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    if (!req.body) {
      return res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    // Consultar todos los tickets
    let allTickets  = await tickets.addTicket( req.body ).then( response => response )

    const payload = {
      idticket: allTickets.id,
      resupesta: "",
      motivo,
      respuestaauxi: "",
      id_suc
    }

    // Consultar todos los tickets
    let historial   = await historial_tickets.addHistorial_ticket( payload ).then( response => response )

    for( const i in fotos ){
      let fotoSubida   = await tickets.addFotoEvidenciaTicket( fotos[i],  historial.id ).then( response => response )
    }
    // Falta consultar las areas, los jefes de area y consultar los auxiliares
    res.send({ message: 'TIcket generado correctamente', ticket: allTickets, historial });

    // Falta consultar las areas, los jefes de area y consultar los auxiliares
  }catch ( error ) {
    res.status(500).send({ message: error.message || "Se produjo algún error al recuperar los Tickets" });
  }

};

exports.subirEvidenciaTicket = async(req, res) => {
  try {

    let nombreArchivos = []

    // desestrucutramos los arvhios cargados
    let { file  } = req.files

    if(!file.length){
      file = [file]
    }

    for( const i in file ){
      const nombreSplit = file[i].name.split('.')
      
      // Obtener la extensión del archivo 
      const extensioArchivo = nombreSplit[ nombreSplit.length - 1 ]
      
      // modificar el nombre del archivo con un uuid unico
      const nombreUuid = uuidv4() + '.' + extensioArchivo
      
      // extensiones validas
      let extensiones  = [ 'BMP', 'GIF', 'jpg', 'jpeg', 'png', 'webp' ]
      let ruta         = './../../evidencia-tickets/' + nombreUuid

      nombreArchivos.push( nombreUuid )

      // validar que la extensión del archivo recibido sea valido
      if( !extensiones.includes( extensioArchivo) )
        return res.status( 400 ).send({message: `Hola, la extensión: ${ extensioArchivo } no es valida, solo se aceptan: ${ extensiones }`})
      

      file[i].mv(`${ruta}`, async ( err ) => {
        if( err )
          return res.status( 500 ).send({ message: err ? err : 'Error' })
      })
    }

    
    res.send( nombreArchivos );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};



exports.getTickets = async (req, res) => {

  try{
    // Consultar todos los tickets
    let allTickets      = await tickets.getTickets( ).then( response => response )
    const departamentos = await tickets.getAreas( ).then( response => response )

    let idtickets = allTickets.map((registro) => { return registro.idticket })

    const evidenciaTicket = await tickets.getEvidenciaTicket( idtickets ).then( response => response )

    for( const i in allTickets ){
      const { id_area, idticket } = allTickets[i]

      const existeArea = departamentos.find( el => el.iddepartamento == id_area )

      allTickets[i]['area'] = existeArea ? existeArea.departamento : 'Sin departamento'
      allTickets[i]['evidencias'] = evidenciaTicket.filter( el => { return el.idticket == idticket })
    }

    // Falta consultar las areas, los jefes de area y consultar los auxiliares
    res.send(allTickets);
  }catch ( error ) {
    res.status(500).send({ message: error.message || "Se produjo algún error al recuperar los Tickets" });
  }
};


exports.getTicketsRH = async (req, res) => {

  try{
    // Consultar todos los tickets
    let allTickets      = await tickets.getTicketsRH( ).then( response => response )
    const departamentos = await tickets.getAreas( ).then( response => response )

    for( const i in allTickets ){
      const { id_area } = allTickets[i]

      const existeArea = departamentos.find( el => el.iddepartamento == id_area )

      allTickets[i]['area'] = existeArea ? existeArea.departamento : 'Sin departamento'
    }

    // Falta consultar las areas, los jefes de area y consultar los auxiliares
    res.send(allTickets);
  }catch ( error ) {
    res.status(500).send({ message: error.message || "Se produjo algún error al recuperar los Tickets" });
  }
};

exports.getTickets_area = async (req, res) => {
  try{
    // Consultar todos los tickets
    let allTickets      = await tickets.getTickets_area( req.params.id ).then( response => response )
    const departamentos = await tickets.getAreas( ).then( response => response )

    let idtickets = allTickets.map((registro) => { return registro.idticket })

    idtickets = idtickets.length ? idtickets : [0]

    const evidenciaTicket = await tickets.getEvidenciaTicket( idtickets ).then( response => response )

    for( const i in allTickets ){
      const { id_area, idticket } = allTickets[i]

      const existeArea = departamentos.find( el => el.iddepartamento == id_area )

      allTickets[i]['area'] = existeArea ? existeArea.departamento : 'Sin departamento'
      allTickets[i]['evidencias'] = evidenciaTicket.filter( el => { return el.idticket == idticket })
    }

    res.send(allTickets);
  }catch ( error ) {
    res.status(500).send({ message: error.message || "Se produjo algún error al recuperar los Tickets" });
  }
};

exports.getTickets_auxiliar_area = (req, res) => {
  tickets.getTickets_auxiliar_area(req.params.idauxi_area, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Tickets"
      });
    else res.send(data);
  });
};

exports.putTickets = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  tickets.putTickets(req.params.idticket, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre la subserie con el id ${req.params.idticket }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar la subserie con el id" + req.params.idticket
        });
      }
    } else res.send(data);
  });
}

exports.putTickets_folio = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  tickets.putTickets_folio(req.params.idticket, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre la subserie con el id ${req.params.idticket }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar la subserie con el id" + req.params.idticket
        });
      }
    } else res.send(data);
  });
}

exports.putTicketAsignacion = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  tickets.putTicketAsignacion(req.params.idticket, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre la subserie con el id ${req.params.idticket }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar la subserie con el id" + req.params.idticket
        });
      }
    } else res.send(data);
  });
}

exports.putTicketAsignacionAuxi = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  tickets.putTicketAsignacionAuxi(req.params.idticket, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre la subserie con el id ${req.params.idticket }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar la subserie con el id" + req.params.idticket
        });
      }
    } else res.send(data);
  });
}

exports.putTicketEstatus = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio!"
        });
    }
    tickets.putTicketEstatus(req.params.idticket, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `No encontre la subserie con el id ${req.params.idticket }.`
                });
            } else {
                res.status(500).send({
                    message: "Error al actualizar la subserie con el id" + req.params.idticket
                });
            }
        } else res.send(data);
    });
}

exports.getTicketsUsuarioLMS = (req, res) => {
    tickets.getTicketsUsuarioLMS(req.params.id_usuario, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los Tickets"
            });
        else res.send(data);
    });
};

exports.getTicketsUsuarioERP = (req, res) => {
    tickets.getTicketsUsuarioERP(req.params.id_usuario, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los Tickets"
            });
        else res.send(data);
    });
};

exports.getUsuariosALL = (req, res) => {
    tickets.getUsuariosALL(req.params.id_usuario, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los Tickets"
            });
        else res.send(data);
    });
};

exports.getCargoUsuario = (req, res) => {
    tickets.getCargoUsuario(req.params.id_usuario, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los Tickets"
            });
        else res.send(data);
    });
};

exports.getTicketsPorID = (req, res) => {
    tickets.getTicketsPorID(req.params.id_ticket, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los Tickets"
            });
        else res.send(data);
    });
};


exports.getNotas = (req, res) => {
  tickets.getNotas(req.params.idticket, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Tickets"
      });
    else res.send(data);
  });
};

exports.addNota = (req, res) => {
    // Validacion de request
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio"
        });
    }
    // Guardar el Ticket en la BD
    tickets.addNota(req.body, (err, data) => {
        // EVALUO QUE NO EXISTA UN ERROR
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la serie"
            })
        else res.send(data)
    })
};

exports.updateVisorRh = async (req, res) => {

  try{
    const { idticket, visorrh } = req.body
    // Consultar todos los tickets
    let updateVisorRh      = await tickets.updateVisorRh( visorrh, idticket ).then( response => response )

    // Falta consultar las areas, los jefes de area y consultar los auxiliares
    res.send({ message: 'Datos actualizados correctamente' });
  }catch ( error ) {
    res.status(500).send({ message: error.message || "Se produjo algún error al recuperar los Tickets" });
  }
};
