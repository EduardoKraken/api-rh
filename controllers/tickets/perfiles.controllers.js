var fs = require('fs');
const Perfiles = require("../../models/tickets/perfiles.models.js");

// Crear un cliente
exports.addPerfil = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Perfiles.addPerfil(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear la serie"
  		})
  	else res.send(data)
  })
};

exports.getPerfiles = (req,res) => {
    Perfiles.getPerfiles((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los clientes"
        });
      else res.send(data);
    });
};


exports.putPerfiles = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	Perfiles.putPerfiles(req.params.idperfil, req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre la subserie con el id ${req.params.idperfil }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar la subserie con el id" + req.params.idperfil 
				});
			}
		} 
		else res.send(data);
	});
}
