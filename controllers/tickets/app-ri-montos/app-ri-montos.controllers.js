const AppRIMontos = require("../../../models/tickets/app-ri-montos/app-ri-montos.model.js");

// Obtener todos los planteles activos
exports.getPlanteles = (req, res) => {
  AppRIMontos.getPlanteles((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

// Obtener todos los ciclos  activos
exports.getCiclos = (req, res) => {
  AppRIMontos.getCiclos((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

// Obtener todos los planteles activos
exports.getCantActual = (req, res) => {
  AppRIMontos.getCantActual(req.params.id,req.params.idfast, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

// Obtener todos los ciclos  activos
exports.getCantSiguiente = (req, res) => {
  AppRIMontos.getCantSiguiente(req.params.id,req.params.idfast, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};