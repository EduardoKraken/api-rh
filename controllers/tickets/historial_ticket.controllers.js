var fs = require('fs');
const Historial_tickets = require("../../models/tickets/historial_ticket.models.js");
const tickets           = require("../../models/tickets/ticket.models.js");


// Crear un Historial_ticket
exports.addHistorial_ticket = async (req, res) => {

  try{

    if (!req.body) {
      return res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    // Consultar todos los tickets
    let data      = await Historial_tickets.addHistorial_ticket( req.body ).then( response => response )

    // Falta consultar las areas, los jefes de area y consultar los auxiliares
    res.send({ message: 'TIcket generado correctamente', ticket: data });
    
  }catch ( error ) {
    res.status(500).send({ message: error.message || "Se produjo algún error al recuperar los Tickets" });
  }

};

exports.getHistorial_tickets = (req, res) => {
  Historial_tickets.getHistorial_tickets((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Historial_tickets"
      });
    else res.send(data);
  });
};

exports.getHistorial_tickets_usuario = (req, res) => {
  Historial_tickets.getHistorial_tickets_usuario(req.params.idhistorial_ticket, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Historial_tickets"
      });
    else res.send(data);
  });
};

exports.getHistorialTicketRespuesta = (req, res) => {
  Historial_tickets.getHistorialTicketRespuesta(req.params.idhistorial_ticket, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Historial_tickets"
      });
    else res.send(data);
  });
};

exports.putHistorialTicketRespuesta = (req, res) => {
  Historial_tickets.putHistorialTicketRespuesta(req.params.idhistorial_ticket, req.body.respuesta, req.params.idticket, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Historial_tickets"
      });
    else res.send(data);
  });
};


exports.putHistorial_tickets = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  Historial_tickets.putHistorial_tickets(req.params.idhistorial_ticket, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre la subserie con el id ${req.params.idhistorial_ticket }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar la subserie con el id" + req.params.idhistorial_ticket
        });
      }
    } else res.send(data);
  });
}

exports.getHistorialEstatus = async (req, res) => {

  try{
    // Consultar todos los tickets
    let allTickets  = await Historial_tickets.getHistorialEstatus( req.params.idticket ).then( response => response )

    let idtickets = allTickets.map((registro) => { return registro.idticket })

    idtickets = idtickets.length ? idtickets : [0]

    const evidenciaTicket = await tickets.getEvidenciaTicket( idtickets ).then( response => response )

    for( const i in allTickets ){
      const { idhistorial_ticket } = allTickets[i]
      allTickets[i]['evidencias'] = evidenciaTicket.filter( el => { return el.idhistorial_ticket == idhistorial_ticket })
    }

    // Falta consultar las areas, los jefes de area y consultar los auxiliares
    res.send(allTickets);

    
  }catch ( error ) {
    res.status(500).send({ message: error.message || "Se produjo algún error al recuperar los Tickets" });
  }
}

