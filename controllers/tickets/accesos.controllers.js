var fs = require('fs');
const Accesos = require("../../models/tickets/accesos.models.js");

// Crear un Acceso
exports.addAcceso = (req, res) => {
    // Validacion de request
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio"
        });
    }

    // Guardar el Acceso en la BD
    Accesos.addAcceso(req.body, (err, data) => {
        // EVALUO QUE NO EXISTA UN ERROR
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la serie"
            })
        else res.send(data)
    })
};

exports.getAccesos = (req, res) => {
    Accesos.getAccesos((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los Accesos"
            });
        else res.send(data);
    });
};


exports.putAccesos = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio!"
        });
    }
    Accesos.putAccesos(req.params.idacceso_ticket, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `No encontre la subserie con el id ${req.params.idacceso_ticket }.`
                });
            } else {
                res.status(500).send({
                    message: "Error al actualizar la subserie con el id" + req.params.idacceso_ticket
                });
            }
        } else res.send(data);
    });
}