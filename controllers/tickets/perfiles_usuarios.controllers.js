var fs = require('fs');
const Perfiles_usuarios = require("../../models/tickets/perfiles_usuarios.models.js");

// Crear un cliente
exports.addPerfil_usuario = (req, res) => {
    // Validacion de request
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio"
        });
    }

    // Guardar el CLiente en la BD
    Perfiles_usuarios.addPerfil_usuario(req.body, (err, data) => {
        // EVALUO QUE NO EXISTA UN ERROR
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la serie"
            })
        else res.send(data)
    })
};

exports.getPerfiles_usuarios = (req, res) => {
    Perfiles_usuarios.getPerfiles_usuarios((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los clientes"
            });
        else res.send(data);
    });
};


exports.putPerfiles_usuarios = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio!"
        });
    }
    Perfiles_usuarios.putPerfiles_usuarios(req.params.idperfilusuario, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `No encontre la subserie con el id ${req.params.idperfilusuario }.`
                });
            } else {
                res.status(500).send({
                    message: "Error al actualizar la subserie con el id" + req.params.idperfilusuario
                });
            }
        } else res.send(data);
    });
}