const Insignias = require("../../models/catalogos/insignias.model.js");

exports.getInsignias = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Insignias.getInsignias((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar las insignias"
            });
        else res.send(data);
    });
};

exports.addInsignia = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    Insignias.addInsignia(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la insignia"
            })
        else res.status(200).send({ message: `La insignia se ha creado correctamente` });
    })
};

exports.updateInsignia = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Insignias.updateInsignia(req.params.idInsignia, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro la insignia con id : ${req.params.idInsignia}` });
            } else {
                res.status(500).send({ message: "Error al actualizar la insignia con el id : " + req.params.idInsignia });
            }
        } else {
            res.status(200).send({ message: `La insignia se la actualizado correctamente.` })
        }
    })
}