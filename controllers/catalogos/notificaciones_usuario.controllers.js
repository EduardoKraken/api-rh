const NotificacionesUsuario = require("../../models/catalogos/notificaciones_usuario.model.js");

exports.getNotificacionesUsuario = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    NotificacionesUsuario.getNotificacionesUsuario((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar las notificaciones"
            });
        else res.send(data);
    });
};

exports.addNotificacionUsuario = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacio" });
    }

    NotificacionesUsuario.addNotificacionUsuario(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la notificacion"
            })
        else res.status(200).send({ message: `La notificacion se ha creado correctamente` });
    })
};

exports.updateNotificacionUsuario = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    NotificacionesUsuario.updateNotificacionUsuario(req.params.idNotificacionUsuario, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro la notificacion con id : ${req.params.idNotificacionUsuario}` });
            } else {
                res.status(500).send({ message: "Error al actualizar la notificacion con el id : " + req.params.idNotificacionUsuario });
            }
        } else {
            res.status(200).send({ message: `La notificacion se ha actualizado correctamente.` })
        }
    })
}