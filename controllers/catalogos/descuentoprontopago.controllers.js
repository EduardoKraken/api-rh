const descProntoPago = require("../../models/catalogos/descuentoprontopago.model");

// Obtener el listado de descProntoPago
exports.getDescuentoProntoPagoGrupo = async(req, res) => {
  try {
  	const { id } = req.params
  	// Consultar solo los descProntoPago activos
		const getDescuentoProntoPagoGrupo = await descProntoPago.getDescuentoProntoPagoGrupo( id ).then(response=> response)

		// enviar los descProntoPago
		res.send(getDescuentoProntoPagoGrupo);

  } catch (error) {
    res.status(500).send({message:error})
  }
};


// Obtener el listado de descProntoPago
exports.getGrupoTeachers = async(req, res) => {
  try {
  	const { id } = req.params
  	// Consultar solo los descProntoPago activos
		const descuentos = await descProntoPago.getGrupoTeachers( id ).then(response=> response)

		// enviar los descProntoPago
		res.send(descuentos);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addDescuentoGrupo = async (req, res) => {
	try {

	  //validamos que no venga vacio el body
	  if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

	  // Agregar el descProntoPago
	  const addDescuento = await descProntoPago.addDescuentoGrupo( req.body ).then(response=> response)
	  // Enviar el ciclo
	  res.send( { message:'Descuento agregado correctamente '});

	} catch (error) {
    res.status(500).send({message:error})
  }
};


exports.deleteDescuentoGrupo = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const deleteDescuento = await descProntoPago.deleteDescuentoGrupo( id, req.body ).then(response=> response)

	  res.send( {message: 'Descuento eliminado correctamente'} );

	} catch (error) {
    res.status(500).send({message:error})
  }
}

