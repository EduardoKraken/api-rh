const niveles = require("../../models/catalogos/niveles.model");

// Obtener el listado de niveles
exports.getNiveles = async(req, res) => {
  try {
  	// Consultar todos los niveles
		const getNiveles = await niveles.getNiveles( ).then(response=> response)
		// Enviar los niveles
		res.send(getNiveles);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

// Obtener el listado de niveles
exports.getNivelesActivos = async(req, res) => {
  try {
  	// Consultar solo los niveles activos
		const getNivelesActivos = await niveles.getNivelesActivos( ).then(response=> response)

		// enviar los niveles
		res.send(getNivelesActivos);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addNivel = async (req, res) => {
	try {

	  //validamos que no venga vacio el body
	  if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

	  // Agregar el niveles
	  const addNivel = await niveles.addNivel( req.body ).then(response=> response)
	  // Enviar el ciclo
	  res.send( addNivel );

	} catch (error) {
    res.status(500).send({message:error})
  }
};


exports.updateNivel = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const updateNivel = await niveles.updateNivel( id, req.body ).then(response=> response)

	  res.send({ updateNivel, message:'Ciclo actualizado correctamente' });

	} catch (error) {
    res.status(500).send({message:error})
  }
}

exports.deleteNivel = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const deleteNivel = await niveles.deleteNivel( id, req.body ).then(response=> response)

	  res.send( deleteNivel );

	} catch (error) {
    res.status(500).send({message:error})
  }
}

