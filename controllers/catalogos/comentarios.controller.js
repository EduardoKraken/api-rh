const Comentarios = require("../../models/catalogos/comentarios.model");

exports.getComentarios = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores
  //y data= los datos que estamos recibiendo
  Comentarios.getComentarios(req.params.prospectoId, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los clientes",
      });
    else res.send(data);
  });
};

exports.addComentario = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacio" });
  }

  Comentarios.addComentario(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el cliente",
      });
    else res.status(200).send({ message: data });
  });
};
