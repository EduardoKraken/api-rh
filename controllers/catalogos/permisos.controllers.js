const permisos = require("../../models/catalogos/permisos.model");

// ------------------------MENU---------------------------- //


exports.getMenu = async (req, res) => {
	try {

		// Consultar la lista de los menus desplegables
		const getMenu = await permisos.getMenu( ).then(response => response)

		// Consultar los submenos activos
		const getSubmenuActivos = await permisos.getSubmenuActivos( ).then(response => response)

		// Consultar loas actions Activos
		const getActionsActivos = await permisos.getActionsActivos( ).then(response => response)

		// Vamos a agregar los acctions al submenu
		for( const i in getSubmenuActivos ){
			const { idsubmenu } = getSubmenuActivos[i]

			getSubmenuActivos[i]['items'] = getActionsActivos.filter( el => { return el.idsubmenu == idsubmenu })

		}

		// Agregrar los actions a los menus
		for( const i in getMenu ){
			const { idmenu } = getMenu[i]

			getMenu[i]['items'] = []

			const submenus = getSubmenuActivos.filter( el => { return el.idmenu == idmenu })

			// Recorremos los subtemas
			for( const j in submenus ){
				getMenu[i].items.push( submenus[j] )
			}

			// Recorremos los actions para agregarlos al menu, los que estén directossssss
			const actions = getActionsActivos.filter( el => { return el.idmenu == idmenu })
			for( const j in actions ){
				getMenu[i].items.push( actions[j] )
			}

		}

		res.send(getMenu);

	} catch (error) {
		res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
	}
};

exports.getMenuActivos = async (req, res) => {
	try {

		const getMenuActivos = await permisos.getMenuActivos( ).then(response => response)

		res.send(getMenuActivos);

	} catch (error) {
		res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
	}
};


exports.addMenu = async (req, res) => {
	try {

		if (!req.body || Object.keys(req.body).length === 0) {
			return res.status(400).send({ message: "El Contenido no puede estar vacio" });
		}

		const addMenu = await permisos.addMenu(req.body).then(response => response)

		res.send(addMenu);

	} catch (error) {
		res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
	}
};

exports.deleteMenu = async (req, res) => {
	try {
		const { idmenu } = req.body

		if (!req.body || Object.keys(req.body).length === 0) {
			res.status(400).send({ message: "El contenido no puede estar vacío" });
		}

		const deleteMenu = await permisos.deleteMenu(idmenu).then(response => response)

		res.send({message:'Menú actualizado correctamente'});

	} catch (error) {
		res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
	}
}

exports.updateMenu = async (req, res) => {
	try {
		const { idmenu } = req.body

		if (!req.body || Object.keys(req.body).length === 0) {
			res.status(400).send({ message: "El contenido no puede estar vacío" });
		}

		const updateMenu = await permisos.updateMenu(idmenu).then(response => response)

		res.send({message:'Menú actualizado correctamente'});

	} catch (error) {
		res.status(500).send({ message: error ? error.message : 'error en el servidor'})
	}
}

// ------------------------SUBMENU---------------------------- //

exports.getSubmenu = async (req, res) => {
	try {

		const getSubmenu = await permisos.getSubmenu().then(response => response)

		res.send(getSubmenu);

	} catch (error) {
		res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
	}
};

exports.getSubmenuActivos = async (req, res) => {
	try {

		const getSubmenuActivos = await permisos.getSubmenuActivos( ).then(response => response)

		res.send(getSubmenuActivos);

	} catch (error) {
		res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
	}
};

exports.addSubmenu = async (req, res) => {
	try {

		if (!req.body || Object.keys(req.body).length === 0) {
			return res.status(400).send({ message: "El Contenido no puede estar vacio" });
		}

		const addSubmenu = await permisos.addSubmenu(req.body).then(response => response)

		res.send(addSubmenu);

	} catch (error) {
		res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
	}
};

exports.deleteSubmenu = async (req, res) => {
	try {
		const { idsubmenu } = req.body

		if (!req.body || Object.keys(req.body).length === 0) {
			res.status(400).send({ message: "El contenido no puede estar vacío" });
		}

		const deleteSubmenu = await permisos.deleteSubmenu(idsubmenu).then(response => response)

		res.send({message:'SubMenú actualizado correctamente'});

	} catch (error) {
		res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
	}
}

exports.updateSubmenu = async (req, res) => {
	try {
		const { idsubmenu } = req.body

		if (!req.body || Object.keys(req.body).length === 0) {
			res.status(400).send({ message: "El contenido no puede estar vacío" });
		}

		const updateSubmenu = await permisos.updateSubmenu(idsubmenu).then(response => response)

		res.send({message:'SubMenú actualizado correctamente'});

	} catch (error) {
		res.status(500).send({ message: error ? error.message : 'error en el servidor'})
	}
}


// ------------------------ACTIONS----------------------------- //

exports.getActions = async (req, res) => {
	try {

		const getActions = await permisos.getActions().then(response => response)

		res.send(getActions);

	} catch (error) {
		res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
	}
};

exports.getActionsActivos = async (req, res) => {
	try {

		const getActionsActivos = await permisos.getActionsActivos().then(response => response)

		res.send(getActionsActivos);

	} catch (error) {
		res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
	}
};


exports.addActions = async (req, res) => {
	try {

		if (!req.body || Object.keys(req.body).length === 0) {
			return res.status(400).send({ message: "El Contenido no puede estar vacio" });
		}

		const addActions = await permisos.addActions(req.body).then(response => response)

		res.send(addActions);

	} catch (error) {
		res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
	}
};

exports.deleteActions = async (req, res) => {
	try {
		const { idactions } = req.body

		if (!req.body || Object.keys(req.body).length === 0) {
			res.status(400).send({ message: "El contenido no puede estar vacío" });
		}

		const deleteActions = await permisos.deleteActions(idactions).then(response => response)

		res.send({message:'Actions actualizado correctamente'});

	} catch (error) {
		res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
	}
}

// ------------------------ACTIONS----------------------------- //
exports.addPermisos = async (req, res) => {
	try {

		if (!req.body || Object.keys(req.body).length === 0) {
			return res.status(400).send({ message: "El Contenido no puede estar vacio" });
		}

		const { idpuesto, idactions, seleccionado } = req.body

		// Validar si existe 
		const existePermisoPuesto = await permisos.existePermisoPuesto( idpuesto, idactions ).then(response => response)

		if( existePermisoPuesto ){
			// ACTUALIZAR
			const estatus = seleccionado ? 0 : 1
			const actualizarPermiso = await permisos.actualizarPermiso(idpuesto, idactions, estatus).then(response => response)
		}else{
			// AGREGAR
			const addPermisos       = await permisos.addPermisos(idpuesto, idactions).then(response => response)
		}

		res.send({message: 'Datos grabados correctamente'});

	} catch (error) {
		res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
	}
};



exports.getPermisosPuesto = async (req, res) => {
	try {

		const { id } = req.params

		const getPermisosPuesto = await permisos.getPermisosPuesto( id ).then(response => response)

		// CONSULTAR LOS ACTIONS DE LOS PERMISOS
		let idactions = getPermisosPuesto.map(registro=>{ return registro.idactions })

		idactions = idactions.length ? idactions : [0]

		// consultar los actions
		const getActionsId = await permisos.getActionsId( idactions ).then(response => response)

		// CONSULTAR LOS SUBMENUS DE LOS PERMISOS
		let idsubmenus = getActionsId.map(registro=>{ return registro.idsubmenu })

		idsubmenus = idsubmenus.length ? idsubmenus : [0]

		// consultar los actions
		const getSubmenusId = await permisos.getSubmenusId( idsubmenus ).then(response => response)

		// CONSULTAR LOS SUBMENUS DE LOS PERMISOS
		let idmenus  = getActionsId.map(registro=>{  return registro.idmenu })
		let idmenus2 = getSubmenusId.map(registro=>{ return registro.idmenu })

		idmenus = idmenus.length ? idmenus : [0]

		idmenus = idmenus.concat( idmenus2 )

		// CONSULTAR LOS MENUS
		const getMenusId = await permisos.getMenusId( idmenus ).then(response => response)

		// Vamos a agregar los acctions al submenu
		for( const i in getSubmenusId ){
			const { idsubmenu } = getSubmenusId[i]
			getSubmenusId[i]['items'] = getActionsId.filter( el => { return el.idsubmenu == idsubmenu })
		}

		// Agregrar los actions a los menus
		for( const i in getMenusId ){
			const { idmenu } = getMenusId[i]


			const submenus = getSubmenusId.filter( el => { return el.idmenu == idmenu })
			getMenusId[i]['items'] = submenus

			// Recorremos los actions para agregarlos al menu, los que estén directossssss
			const actions = getActionsId.filter( el => { return el.idmenu == idmenu })
			for( const j in actions ){
				getMenusId[i].items.push( actions[j] )
			}

		}

		res.send({getMenusId, idactions });

	} catch (error) {
		res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
	}
};

exports.updateActions = async (req, res) => {
	try {
		const { idactions } = req.body

		if (!req.body || Object.keys(req.body).length === 0) {
			res.status(400).send({ message: "El contenido no puede estar vacío" });
		}

		const updateActions = await permisos.updateActions(idactions).then(response => response)

		res.send({message:'Actions actualizado correctamente'});
		

	} catch (error) {
		res.status(500).send({ message: error ? error.message : 'error en el servidor'})
	}
}