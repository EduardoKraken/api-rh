const Materiales = require("../../models/catalogos/materiales.model.js");

exports.getMateriales = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores
  //y data= los datos que estamos recibiendo
  Materiales.getMateriales((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los materialess",
      });
    else res.send(data);
  });
};

exports.addMaterial = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacio" });
  }

  Materiales.addMaterial(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el material",
      });
    else
      res
        .status(200)
        .send({ message: `El material se ha creado correctamente` });
  });
};

exports.updateMaterial = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }

  Materiales.updateMaterial(req.params.idMaterial, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No se encontro el material con id : ${req.params.idMaterial}`,
        });
      } else {
        res.status(500).send({
          message:
            "Error al actualizar el material con el id : " +
            req.params.idMaterial,
        });
      }
    } else {
      res
        .status(200)
        .send({ message: `El material se ha actualizado correctamente.` });
    }
  });
};

exports.getHistoriaCliente = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores
  //y data= los datos que estamos recibiendo
  Materiales.getHistoriaCliente(req.params.id, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar las materiales",
      });
    else res.send(data);
  });
};
