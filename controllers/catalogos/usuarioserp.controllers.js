const { response } = require("express");
const { rest } = require("lodash");
const usuarioserp = require("../../models/catalogos/usuarioserp.model.js");


exports.getUsuariosERP = async (req, res,) => {
  try {
    const usuarios = await usuarioserp.getUsuariosERP( ).then(response => response) 
    const respuesta = usuarios;
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};

exports.getPerfilesERP = async (req, res,) => {
  try {
    const perfiles = await usuarioserp.getPerfilesERP( ).then(response => response) 
    const respuesta = perfiles;
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};

exports.getTrabajadoresERP = async (req, res,) => {
  try {
    const trabajadores = await usuarioserp.getTrabajadoresERP( ).then(response => response) 
    const respuesta = trabajadores;
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};

exports.getIdUsuarioERP = async (req, res,) => {
  try {
    const id_agregar = await usuarioserp.getIdUsuarioERP( ).then(response => response) 
    const respuesta = id_agregar;
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }

};

exports.updateUsuarioERP = async(req, res) => {
  try {
    const { usuario, password, nombre_usuario, apellido_usuario, id_plantel, email, telefono,  activo_sn, comentarios, id_trabajador, id_usuario} = req.body

    // Actualizar "finalizo"
    const respuesta = await usuarioserp.updateUsuario( usuario, password, nombre_usuario, apellido_usuario, id_plantel, email, telefono, activo_sn, comentarios, id_trabajador, id_usuario).then(response => response) 
    res.send({message: 'Actualización exitosa'});
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};

exports.updatePerfilUsuario = async(req, res) => {
  try {
    const { id_perfil, id_usuario } = req.body

    // Actualizar "finalizo"

    const existePerfil = await usuarioserp.getPerfilUsuario( id_usuario ).then(response => response) 

    if( existePerfil ){
      // Actualizar
      const respuesta = await usuarioserp.updatePerfilUsuario( id_perfil, id_usuario ).then(response => response) 
    }else{
      // AGregas
      const payload = { id_perfil, id_usuario }
      const respuesta = await usuarioserp.addPerfilUsuarioERP( payload ).then(response => response) 
    }




    res.send({message: 'Actualización exitosa'});
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};


exports.addUsuarioERP = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
      res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  usuarioserp.addUsuarioERP(req.body, (err, data) => {
      if (err)
          res.status(500).send({
              message: err.message || "Se produjo algún error al crear el usuario"
          })
      else res.status(200).send({ message: `El usuario se ha creado correctamente` });
  })
};


exports.addPerfilUsuarioERP = async (req, res) => {
  try{

    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    await usuarioserp.addPerfilUsuarioERP(req.body).then( response => response )

  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};


exports.deleteUsuarioERP = (req, res) => {
  usuarioserp.deleteUsuarioERP(req.params.id_usuario, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No se encontraron usuarios con id: ${req.params.id_usuario}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el nivel usuario con el id " + req.params.id_usuario
        });
      }
    } else res.send({ message: `El nivel usuario se elimino correctamente!` });
  });
};

//ANGEL RODRIGUEZ -- TODO