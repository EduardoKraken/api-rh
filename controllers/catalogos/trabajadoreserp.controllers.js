const { response } = require("express");
const { rest } = require("lodash");
const trabajadoreserp = require("../../models/catalogos/trabajadoreserp.model.js");


exports.getTrabajadoresERP = async (req, res,) => {
  try {
    const trabajadores = await trabajadoreserp.getTrabajadoresERP( ).then(response => response) 
    const respuesta = trabajadores;
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};

exports.getPuestosERP = async (req, res,) => {
  try {
    const puestos = await trabajadoreserp.getPuestosERP( ).then(response => response) 
    const respuesta = puestos;
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};

exports.getJornadaLaboralERP = async (req, res,) => {
  try {
    const jornada = await trabajadoreserp.getJornadaLaboralERP( ).then(response => response) 
    const respuesta = jornada;
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};

exports.getDepartamentoERP = async (req, res,) => {
  try {
    const departamento = await trabajadoreserp.getDepartamentoERP( ).then(response => response) 
    const respuesta = departamento;
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};

exports.getTipoTrabajadorERP = async (req, res,) => {
  try {
    const tipo = await trabajadoreserp.getTipoTrabajadorERP( ).then(response => response) 
    const respuesta = tipo;
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};

exports.getNivelERP = async (req, res,) => {
  try {
    const nivel = await trabajadoreserp.getNivelERP( ).then(response => response) 
    const respuesta = nivel;
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};

exports.getCursoERP = async (req, res,) => {
  try {
    const curso = await trabajadoreserp.getCursoERP( ).then(response => response) 
    const respuesta = curso;
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};

exports.getPerfilesERP = async (req, res,) => {
  try {
    const perfiles = await trabajadoreserp.getPerfilesERP( ).then(response => response) 
    const respuesta = perfiles;
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};

exports.getTrabajadorsERP = async (req, res,) => {
  try {
    const trabajadores = await trabajadoreserp.getTrabajadoresERP( ).then(response => response) 
    const respuesta = trabajadores;
    res.send(respuesta)
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};


exports.updateTrabajadorERP = async(req, res) => {
  try {
    const { nombres, apellido_paterno, apellido_materno, domicilio, horario_inicio, horario_fin, telefono1, telefono2, email, rfc, numero_poliza_sgm_mayores, numero_poliza_sgm_menores, numero_imss, fecha_ingreso, fecha_termino_labores,
      antiguedad, id_plantel, id_puesto, id_jornada_laboral, id_departamento, id_tipos_trabajadores, id_nivel, id_curso, fecha_nacimiento, motivo_salida, activo_sn, id} = req.body

    // Actualizar "finalizo"
    const respuesta = await trabajadoreserp.updateTrabajador( nombres, apellido_paterno, apellido_materno, domicilio, horario_inicio, horario_fin, telefono1, telefono2, email, rfc, numero_poliza_sgm_mayores, numero_poliza_sgm_menores, numero_imss, fecha_ingreso, fecha_termino_labores,
      antiguedad, id_plantel, id_puesto, id_jornada_laboral, id_departamento, id_tipos_trabajadores, id_nivel, id_curso, fecha_nacimiento, motivo_salida, activo_sn, id).then(response => response) 
    res.send({message: 'Actualización exitosa'});
  } catch (error) {
    res.status(500).send({message: error ? error.message : "Error en el servidor"})
  }
};


exports.addTrabajadorERP = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
      res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  trabajadoreserp.addTrabajadorERP(req.body, (err, data) => {
      if (err)
          res.status(500).send({
              message: err.message || "Se produjo algún error al crear el usuario"
          })
      else res.status(200).send({ message: `El usuario se ha creado correctamente` });
  })
};


exports.deleteUsuarioERP = (req, res) => {
    trabajadoreserp.deleteUsuarioERP(req.params.id_usuario, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No se encontraron usuarios con id: ${req.params.id_usuario}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el nivel usuario con el id " + req.params.id_usuario
        });
      }
    } else res.send({ message: `El nivel usuario se elimino correctamente!` });
  });
};

//ANGEL RODRIGUEZ -- TODO