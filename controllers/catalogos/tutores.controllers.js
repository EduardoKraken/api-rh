const Tutores = require("../../models/catalogos/tutores.model");

exports.getTutores = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores
  //y data= los datos que estamos recibiendo
  Tutores.getTutores((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los cursos",
      });
    else res.send(data);
  });
};

exports.addTutor = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Tutores.addCurso(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el tutor",
      });
    else
      res.status(200).send({ message: `El tutor se ha creado correctamente` });
  });
};

exports.updateTutor = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }

  Tutores.updateTutor(req.params.idTutor, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No se encontro el tutor con id : ${req.params.idTutor}`,
        });
      } else {
        res.status(500).send({
          message:
            "Error al actualizar el tutor con el id : " + req.params.idTutor,
        });
      }
    } else {
      res
        .status(200)
        .send({ message: `El tutor se ha actualizado correctamente.` });
    }
  });
};

exports.getTutor = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores
  //y data= los datos que estamos recibiendo
  Tutores.getTutor(req.params.id, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los temas",
      });
    else res.send(data);
  });
};

exports.deleteTutor = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores
  //y data= los datos que estamos recibiendo
  Tutores.deleteTutor(req.params.idTutor, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al eliminar al tutor",
      });
    else res.send(data);
  });
};
