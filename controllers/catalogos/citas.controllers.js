const Citas = require("../../models/catalogos/citas.model");

exports.getCitasEmpleado = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores
  //y data= los datos que estamos recibiendo
  Citas.getCitasEmpleado(req.params.idEmpleado, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los prospectos",
      });
    else res.send(data);
  });
};

exports.getCitasProspecto = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores
  //y data= los datos que estamos recibiendo
  Citas.getCitasProspecto(req.params.idProspecto, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los prospectos",
      });
    else res.send(data);
  });
};

exports.getCita = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores
  //y data= los datos que estamos recibiendo
  Citas.getCita(req.params.id, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar lo cita",
      });
    else res.send(data);
  });
};

exports.addCita = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Citas.addCita(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la cita",
      });
    else
      res
        .status(200)
        .send({ message: `La cita se ha creado correctamente`, data: data });
  });
};

exports.updateCita = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0 || isNaN(req.params.id)) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
    return;
  }

  Citas.updateCita(req.params.id, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No se encontro el prospecto con id : ${req.params.id}`,
        });
        return;
      } else {
        res.status(500).send({
          message:
            "Error al actualizar el prospecto con el id : " + req.params.id,
        });
        return;
      }
    } else {
      res
        .status(200)
        .send({ message: `El prospecto se ha actualizado correctamente.` });
    }
  });
};

exports.cancelCita = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores
  //y data= los datos que estamos recibiendo
  Citas.cancelCita(req.params.idCita, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al eliminar el prospecto",
      });
    else res.send(data);
  });
};
