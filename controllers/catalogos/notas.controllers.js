const Notas = require("../../models/catalogos/notas.model.js");

exports.getNotas = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Notas.getNotas((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar las notas"
            });
        else res.send(data);
    });
};

exports.addNota = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacio" });
    }

    Notas.addNota(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la nota"
            })
        else res.status(200).send({ message: `La nota se ha creado correctamente` });
    })
};

exports.updateNota = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Notas.updateNota(req.params.idNota, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro la nota con id : ${req.params.idNota}` });
            } else {
                res.status(500).send({ message: "Error al actualizar la nota con el id : " + req.params.idNota });
            }
        } else {
            res.status(200).send({ message: `La nota se ha actualizado correctamente.` })
        }
    })
}