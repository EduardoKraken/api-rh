const Prospectos = require("../../models/catalogos/prospectos.model");
const usuariosModel = require("../../models/usuarios.model");

exports.getProspectos = async (req, res) => {
  let idEmpleado = req.params.idEmpleado;
  const puesto = await usuariosModel
    .getPuestoUsuario(idEmpleado)
    .then((response) => response)
    .catch((err) => 323212);

  try {
    if (
      puesto[0].idpuesto == 25 ||
      puesto[0].idpuesto == 3 ||
      puesto[0].idpuesto == 7 ||
      puesto[0].idpuesto == 43
      || puesto[0].idpuesto == 12
    ) {
      Prospectos.getProspectos((err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message ||
              "Se produjo algún error al recuperar los prospectos",
          });
        else res.send(data);
      });
    } else {
      Prospectos.getProspectosId(idEmpleado, (err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message ||
              "Se produjo algún error al recuperar los prospectos",
          });
        else res.send(data);
      });
    }
  } catch (error) {
    res.status(500).send({
      message:
        error.message || "Se produjo algún error al recuperar los prospectos",
    });
  }
};

exports.getProspecto = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores
  //y data= los datos que estamos recibiendo
  Prospectos.getProspecto(req.params.id, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los temas",
      });
    else res.send(data);
  });
};

exports.addProspecto = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Prospectos.addProspectos(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el prospecto",
      });
    else
      res.status(200).send({
        message: `El prospecto se ha creado correctamente`,
        data: data,
      });
  });
};

exports.updateProspecto = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }

  Prospectos.updateProspecto(req.params.idProspecto, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No se encontro el prospecto con id : ${req.params.idCurso}`,
        });
      } else {
        res.status(500).send({
          message:
            "Error al actualizar el prospecto con el id : " +
            req.params.idProspecto,
        });
      }
    } else {
      res
        .status(200)
        .send({ message: `El prospecto se ha actualizado correctamente.` });
    }
  });
};

exports.deleteProspecto = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores
  //y data= los datos que estamos recibiendo
  Prospectos.deleteProspecto(req.params.idProspecto, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al eliminar el prospecto",
      });
    else res.send(data);
  });
};

// exports.asignarProspecto = async (req, res) => {};

exports.editarEstatusProspecto = async (req, res) => {
  let id_prospecto = req.params.idProspecto;
  let { estatus } = req.body;

  if (typeof id_prospecto == "undefined" || estatus == "undefined")
    res.status(400).send({
      error: true,
      message: "Debes enviar un prospecto y un prospecto",
    });

  await Prospectos.editarEstatusProspecto(estatus, id_prospecto)
    .then((response) => {
      res.status(200).send({
        error: false,
        message: "Se cambio el estatus correctamente",
      });
    })
    .catch((error) => {
      res.status(500).send({
        error: true,
        message:
          error.message ||
          "Se produjo algún error al tratar de reasignar el prospecto",
      });
    });
  return;
};

exports.getClientes = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores
  //y data= los datos que estamos recibiendo
  Prospectos.getClientes((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los clientes",
      });
    else res.send(data);
  });
};
