const horarios = require("../../models/catalogos/horarios.model");

// Obtener el listado de horarios
exports.getHorarios = async(req, res) => {
  try {
  	// Consultar todos los horarios
		const getHorarios = await horarios.getHorarios( ).then(response=> response)
		// Enviar los horarios
		res.send(getHorarios);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

// Obtener el listado de horarios
exports.getHorariosActivos = async(req, res) => {
  try {
  	// Consultar solo los horarios activos
		const getHorariosActivos = await horarios.getHorariosActivos( ).then(response=> response)

		// enviar los horarios
		res.send(getHorariosActivos);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addHorario = async (req, res) => {
	try {

	  //validamos que no venga vacio el body
	  if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

	  // Agregar el horarios
	  const addHorario = await horarios.addHorario( req.body ).then(response=> response)
	  // Enviar el ciclo
	  res.send( addHorario );

	} catch (error) {
    res.status(500).send({message:error})
  }
};


exports.updateHorario = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const updateHorario = await horarios.updateHorario( id, req.body ).then(response=> response)

	  res.send({ updateHorario, message:'Ciclo actualizado correctamente' });

	} catch (error) {
    res.status(500).send({message:error})
  }
}

exports.deleteHorario = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const deleteHorario = await horarios.deleteHorario( id, req.body ).then(response=> response)

	  res.send( deleteHorario );

	} catch (error) {
    res.status(500).send({message:error})
  }
}

