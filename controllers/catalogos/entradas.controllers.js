const entradas   = require("../../models/catalogos/entradas.model.js");
const usuarios   = require("../../models/usuarios.model.js");
const planteles = require("../../models/catalogos/planteles.model");

/*
  EP para actualizar los datos del usuario, además de actualizar los datos ( nombre ) de usuarios
*/
exports.getEntradasSucursal = async (req, res) => {
  try{
    // Actualizar los datos de datos_usuario
    let usuariosAbren = await entradas.getHorariosUsuario( ).then( response => response) 

    const idUsuarios = usuariosAbren.map((registro) => { return registro.iderp })


    const usuariosSistema = await entradas.getUsuariosERP( idUsuarios ).then( response => response )

    // Actualizar el nombre en usuarios
    let asistencias   = await entradas.getEntradasHoy( ).then( response => response) 

    for( const i in usuariosAbren ){
    	const { iderp, hora_inicio, diferencia } = usuariosAbren[i]

    	// La hora en la que debió entrar la recepcionista
    	const entrada  = moment(hora_inicio,'HH:mm')

    	// VERIFICAR LA ASISTENCIA DE LA RECEPCIONISTA
    	const existeAsistencia = asistencias.find( el => el.id_usuario == iderp )


    	// VALIDAR SI EL USUARIO EXISTE
    	const existeUsuario    = usuariosSistema.find( el => el.id_usuario == iderp )

    	// SACAR EL TIEMPO DE RETRADO DE LA RECEPCIONISTA
    	let retraso = diferencia > 0 ? diferencia : null 
    	if( existeAsistencia ){
    		const { hora_registro } = existeAsistencia

	    	// VER LA HORA EN LA QUE LLEGO LA RECEPCIONISTA
	     	const llegada  = moment(hora_registro,'HH:mm') 

    		if( llegada > entrada )
    			retraso = llegada.diff(entrada, 'minutes');
    		else
    			retraso = 'Sin retraso'
    	}

    	usuariosAbren[i]['asistencia'] = existeAsistencia ? 1 : 0
    	usuariosAbren[i]['registro']   = existeAsistencia ? existeAsistencia.hora_registro   : ''
    	usuariosAbren[i]['vendedora']  = existeUsuario    ? existeUsuario.nombre_completo    : ''
    	usuariosAbren[i]['plantel']    = existeUsuario    ? existeUsuario.plantel            : ''
    	usuariosAbren[i]['escuela']    = existeUsuario    ? existeUsuario.escuela            : ''
    	usuariosAbren[i]['retraso']    = retraso

    }

    res.send(usuariosAbren)
  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'ERROR EN EL SERVIDOR' } )
  }
}

exports.getHorariosApertura = async (req, res) => {
  try{

    // Actualizar los datos de datos_usuario
    let getUsuariosHorarios  = await entradas.getUsuariosHorarios( ).then( response => response) 

    let usuariosAbren = await entradas.getHorariosApertura( ).then( response => response) 

    for( const i in getUsuariosHorarios ){
    	const { id_usuario } = getUsuariosHorarios[i]

    	getUsuariosHorarios[i]['horarios']  = usuariosAbren.filter( el =>{ return el.iderp == id_usuario })
    }


    res.send(getUsuariosHorarios)

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'ERROR EN EL SERVIDOR' } )
  }
}


exports.getTrabajadoresUsuarios = async (req, res) => {
  try{

    // Actualizar los datos de datos_usuario
    let trabajadores     = await entradas.getTrabajadores( ).then( response => response) 

    let usuariosActivos  = await usuarios.getUsuariosActivos( ).then( response => response) 
    let planteles        = await entradas.getPlantelesApertura( ).then( response => response) 


    res.send({trabajadores, usuariosActivos, planteles })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'ERROR EN EL SERVIDOR' } )
  }
}


exports.crearRelacionUsuarios = async (req, res) => {
  try{

    const { id_usuario, id_trabajador } = req.body

    // Actualizar los datos de datos_usuario
    let trabajacrearRelacionUsuariosdores     = await entradas.crearRelacionUsuarios( id_usuario, id_trabajador ).then( response => response) 

    res.send({ message: 'Relación creada' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'ERROR EN EL SERVIDOR' } )
  }
}



exports.agregarHorarios = async (req, res) => {
  try{

    const { id_usuario, dias } = req.body

    for( const i in dias ){
      const { valor, hora } = dias[i]

      const existeHorario = await entradas.existeHorario( id_usuario, valor ).then( response => response )

      if( existeHorario ){
        console.log( 1 )
        let updateHorarioApertura = await entradas.updateHorarioApertura( id_usuario, valor, hora ).then( response => response) 

      }else{
        console.log( 2 )
        let agregarHorarios = await entradas.agregarHorarios( id_usuario, valor, hora ).then( response => response) 
      }
    }


    res.send({ message: 'Datos registrados correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'ERROR EN EL SERVIDOR' } )
  }
}



exports.eliminarHorarioApertura = async (req, res) => {
  try{

    const { id } = req.params

    let eliminarHorarioApertura = await entradas.eliminarHorarioApertura( id ).then( response => response) 


    res.send({ message: 'Horario de apertura eliminado' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'ERROR EN EL SERVIDOR' } )
  }
}

exports.eliminarUsuarioApertura = async (req, res) => {
  try{

    const { id } = req.params

    let eliminarUsuarioApertura = await entradas.eliminarUsuarioApertura( id ).then( response => response) 


    res.send({ message: 'Usuario Eliminado' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'ERROR EN EL SERVIDOR' } )
  }
}

exports.desactivarUsuario = async (req, res) => {
  try{

    const { id_usuario, estatus } = req.body

    let desactivarUsuario = await entradas.desactivarUsuario( id_usuario, estatus ).then( response => response) 

    res.send({ message: 'Horario de apertura eliminado' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'ERROR EN EL SERVIDOR' } )
  }
}


exports.asistenciasVisuales = async (req, res) => {
  try{

    // Consultar primero los planteles
    let plantelesActivos = await entradas.getPlantelesApertura( ).then( response => response) 

    // Consultar recepcionistas y encargadas
    let   personal   = await entradas.getPersonalApertura( ).then( response => response )
    const idPersonal = personal.map((registro) => { return registro.iderp })

    // Consultar los nombres
    let usuariosERP = await entradas.getUsuariosTrabajadores( idPersonal ).then( response => response )

    // consultar las enradas de hoy
    const entradasHoy = await entradas.getEntradasHoy( ).then( response => response )

    // Contador de checadas
    const contadorChecadas = await entradas.getContadorChecadas( ).then( response => response )


    // Agregar a cada personal su usuario y su id_trabajador
    for( const i in personal ){
      const { iderp } = personal[i]

      const existeUsuario    = usuariosERP.find( el => el.id_usuario == iderp )
      const existeAsistencia = entradasHoy.find( el => el.id_usuario == iderp )
      const existeContador   = contadorChecadas.filter( el => el.id_usuario == iderp )
    
      personal[i]['nombre_completo'] = existeUsuario    ? existeUsuario.nombre_completo    : 'Sin nombre'     
      personal[i]['nombre_usuario']  = existeUsuario    ? existeUsuario.nombre_usuario     : 'Sin nombre'     
      personal[i]['id_trabajador']   = existeUsuario    ? existeUsuario.id_trabajador      : 0     
      personal[i]['id_plantel']      = existeUsuario    ? existeUsuario.id_plantel         : 0  

      let par = 1

      if( existeContador.length ){
        if( (existeContador.length % 2) == 0  ){
          par = 0
        }
      }

      // Verificar entrada o salida
      if( existeAsistencia && existeContador.length )   {
        personal[i]['asistencia'] = par
      }else{
        personal[i]['asistencia'] = 0
      }
    }


    const excluir = [501,13,184] 
    // Agregar las recepsionistas y encargadas
    for( const i in plantelesActivos ){
      const { idplantel } = plantelesActivos[i]
      plantelesActivos[i]['encargadas']    = personal.filter( el => { return el.id_plantel == idplantel && el.idpuesto == 18 && !excluir.includes(el.iderp)  })
      plantelesActivos[i]['recepcionista'] = personal.filter( el => { return el.id_plantel == idplantel && el.idpuesto == 19 && !excluir.includes(el.iderp)  })
    }


    res.send(plantelesActivos)

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'ERROR EN EL SERVIDOR' } )
  }
}


exports.asistenciasVisualesTeachers = async (req, res) => {
  try{

    let clasesActivasFast = await entradas.clasesAcitvas( 2 ).then( response => response) 
    let clasesActivasInbi = await entradas.clasesAcitvas( 1 ).then( response => response) 

    // Ver si se genero codigos hoy y ver quienes ya lo agregaron a su clase
    const codigosGeneradosFast = await entradas.getCodigosGenerados( 2 ).then( response => response )
    const codigosGeneradosInbi = await entradas.getCodigosGenerados( 1 ).then( response => response )

    for( const i in clasesActivasFast ){
      const { id_teacher, id_grupo } = clasesActivasFast[i]

      const existeAsistencia = codigosGeneradosFast.find( el => el.id_usuario == id_teacher && el.id_grupo == id_grupo )
      clasesActivasFast[i]['asistencia'] = existeAsistencia ? 1 : null
    }


    for( const i in clasesActivasInbi ){
      const { id_teacher, id_grupo } = clasesActivasInbi[i]

      const existeAsistencia = codigosGeneradosInbi.find( el => el.id_usuario == id_teacher && el.id_grupo == id_grupo )
      clasesActivasInbi[i]['asistencia'] = existeAsistencia ? 1 : null
    }

    let asistencias       = await entradas.getEntradasHoyTeachers( ).then( response => response) 

    let respuesta = clasesActivasFast.concat( clasesActivasInbi )

    for( const i in respuesta ){
      const { id_teacher } = respuesta[i]
      const existeAsistencia = asistencias.find( el => el.id_usuario == id_teacher )

      if( !respuesta[i].asistencia ){
        respuesta[i].asistencia = existeAsistencia ? 1 : null
      }

    }

    res.send(respuesta)

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'ERROR EN EL SERVIDOR' } )
  }
}

exports.validarAsistenciaAlumno = async (req, res) => {
  try{

    const entradasAlumnos = await entradas.getEntradasAlumnos( ).then( response => response )

    for( const i in entradasAlumnos ){
      const { id_alumno, hora_registro } = entradasAlumnos[i]

      const claseInbi = await entradas.alumnoClaseHoy( 1, id_alumno ).then( response => response )
      const claseFast = await entradas.alumnoClaseHoy( 2, id_alumno ).then( response => response )

      let datos   = null
      let escuela = null

      if( claseInbi ){ datos = claseInbi; escuela = 1 }
      else if ( claseFast ){ datos = claseFast; escuela = 2 }

      if( datos ){
        const { id_teacher, id_grupo, id } = datos 
        // Validar si tiene ya una asistencia
        const existeAsistencia = await entradas.existeAsistencia( escuela, id, id_grupo ).then( response => response )
        // Si no, agregar asistencia
        if( !existeAsistencia ){
          // Agregar la asistencia
          const addAsistencia = await entradas.addAsistencia( escuela, id_grupo, id, id_teacher ).then( response => response )
          // Actualziar la asistencia del alumno en el ERP viejito
          const updateAsisteciaERP = await entradas.updateAsistenciaERP( entradasAlumnos[i].id, id_grupo ).then( response => response )
        }
      }
    }

    res.send( entradasAlumnos )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'ERROR EN EL SERVIDOR' } )
  }
}


exports.actualizarSucursalApertura = async (req, res) => {
  try{

    const { id_usuario, id_plantel_oficial } = req.body

    let actualizarSucursalApertura = await entradas.actualizarSucursalApertura( id_plantel_oficial, id_usuario ).then( response => response) 

    res.send({ message: 'Sucursal actualizada correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'ERROR EN EL SERVIDOR' } )
  }
}