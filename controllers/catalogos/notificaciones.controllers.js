const Notificaciones = require("../../models/catalogos/notificaciones.model.js");

exports.getNotificaciones = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Notificaciones.getNotificaciones((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las notificaciones"
      });
    else res.send(data);
  });
};

exports.addNotificacion = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacio" });
  }

  Notificaciones.addNotificacion(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        essage: err.message || "Se produjo algún error al crear la notificacion"
      })
    else res.status(200).send({ message: `La notificacion se ha creado correctamente` });
  })
};

exports.updateNotificacion = (req, res) => {

  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }

  Notificaciones.updateNotificacion(req.params.idNotificacion, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro la notificacion con id : ${req.params.idNotificacion}` });
      } else {
        res.status(500).send({ message: "Error al actualizar la notificacion con el id : " + req.params.idNotificacion });
      }
    } else {
      res.status(200).send({ message: `La notificacion se ha actualizado correctamente.` })
    }
  })
}

