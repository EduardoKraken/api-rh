const planteles = require("../../models/catalogos/planteles.model");

// Obtener el listado de grupos
exports.getGrupos = async(req, res) => {
  try {
  	// Consultar todos los grupos
		const getGrupos = await grupos.getGrupos( ).then(response=> response)
		// Enviar los grupos
		res.send(getGrupos);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

// Obtener el listado de grupos
exports.getPlantelesActivos = async(req, res) => {
  try {
  	// Consultar solo los grupos activos
		const getPlantelesActivos = await planteles.getPlantelesActivos( ).then(response=> response)

		// enviar los grupos
		res.send(getPlantelesActivos);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getPlantelesOficiales = async(req, res) => {
  try {
  	// Consultar solo los grupos activos
		const getPlantelesOficiales = await planteles.getPlantelesOficiales( ).then(response=> response)

		// enviar los grupos
		res.send(getPlantelesOficiales);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addGrupo = async (req, res) => {
	try {

	  //validamos que no venga vacio el body
	  if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

	  // Agregar el grupos
	  const addGrupo = await grupos.addGrupo( req.body ).then(response=> response)
	  // Enviar el ciclo
	  res.send( addGrupo );

	} catch (error) {
    res.status(500).send({message:error})
  }
};


exports.updateGrupo = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const updateGrupo = await grupos.updateGrupo( id, req.body ).then(response=> response)

	  res.send({ updateGrupo, message:'Ciclo actualizado correctamente' });

	} catch (error) {
    res.status(500).send({message:error})
  }
}

exports.deleteGrupo = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const deleteGrupo = await grupos.deleteGrupo( id, req.body ).then(response=> response)

	  res.send( deleteGrupo );

	} catch (error) {
    res.status(500).send({message:error})
  }
}

