const formaspago = require("../../models/catalogos/formaspago.model");

// Obtener el listado de formaspago
exports.getFormasPago = async(req, res) => {
  try {
  	// Consultar todos los formaspago
		const getFormasPago = await formaspago.getFormasPago( ).then(response=> response)
		// Enviar los formaspago
		res.send(getFormasPago);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

// Obtener el listado de formaspago
exports.getFormasPagoActivos = async(req, res) => {
  try {
  	// Consultar solo los formaspago activos
		const getFormasPagoActivos = await formaspago.getFormasPagoActivos( ).then(response=> response)

		// enviar los formaspago
		res.send(getFormasPagoActivos);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addFormasPago = async (req, res) => {
	try {

	  //validamos que no venga vacio el body
	  if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

	  // Agregar el formaspago
	  const addFormasPago = await formaspago.addFormasPago( req.body ).then(response=> response)
	  // Enviar el ciclo
	  res.send( addFormasPago );

	} catch (error) {
    res.status(500).send({message:error})
  }
};


exports.updateFormasPago = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const updateFormasPago = await formaspago.updateFormasPago( id, req.body ).then(response=> response)

	  res.send({ updateFormasPago, message:'Ciclo actualizado correctamente' });

	} catch (error) {
    res.status(500).send({message:error})
  }
}

exports.deleteFormasPago = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const deleteFormasPago = await formaspago.deleteFormasPago( id, req.body ).then(response=> response)

	  res.send( deleteFormasPago );

	} catch (error) {
    res.status(500).send({message:error})
  }
}

