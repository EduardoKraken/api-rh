const ciclos = require("../../models/catalogos/ciclos.model");

// Obtener el listado de horarios
exports.getCiclos = async(req, res) => {
  try {
  	// Consultar todos los ciclos
		const getCiclos = await ciclos.getCiclos( ).then(response=> response)
		// Enviar los ciclos
		res.send(getCiclos);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

// Obtener el listado de horarios
exports.getCiclosActivos = async(req, res) => {
  try {
  	// Consultar solo los ciclos activos
		const getCiclosActivos = await ciclos.getCiclosActivos( ).then(response=> response)

		// enviar los ciclos
		res.send(getCiclosActivos);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addCiclo = async (req, res) => {
	try {

	  //validamos que no venga vacio el body
	  if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

	  // Agregar el ciclo
	  const addCiclo = await ciclos.addCiclo( req.body ).then(response=> response)
	  // Enviar el ciclo
	  res.send( addCiclo );

	} catch (error) {
    res.status(500).send({message:error})
  }
};


exports.updateCiclo = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const updateCiclo = await ciclos.updateCiclo( id, req.body ).then(response=> response)

	  res.send({ updateCiclo, message:'Ciclo actualizado correctamente' });

	} catch (error) {
    res.status(500).send({message:error})
  }
}

exports.deleteCiclo = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const deleteCiclo = await ciclos.deleteCiclo( id, req.body ).then(response=> response)

	  res.send( deleteCiclo );

	} catch (error) {
    res.status(500).send({message:error})
  }
}

exports.relacionarCiclo = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const relacionarCiclo = await ciclos.relacionarCiclo( id, req.body ).then(response=> response)

	  res.send( relacionarCiclo );

	} catch (error) {
    res.status(500).send({message:error})
  }
}