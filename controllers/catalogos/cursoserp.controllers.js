const cursoserp = require("../../models/catalogos/cursoserp.model");

// Obtener el listado de cursoserp
exports.getCursoserp = async(req, res) => {
  try {
  	// Consultar todos los cursoserp
		const getCursoserp = await cursoserp.getCursoserp( ).then(response=> response)
		// Enviar los cursoserp
		res.send(getCursoserp);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

// Obtener el listado de cursoserp
exports.getCursoserpActivos = async(req, res) => {
  try {
  	// Consultar solo los cursoserp activos
		const getCursoserpActivos = await cursoserp.getCursoserpActivos( ).then(response=> response)

		// enviar los cursoserp
		res.send(getCursoserpActivos);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addCursoErp = async (req, res) => {
	try {

	  //validamos que no venga vacio el body
	  if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

	  // Agregar el cursoserp
	  const addCursoErp = await cursoserp.addCursoErp( req.body ).then(response=> response)
	  // Enviar el ciclo
	  res.send( addCursoErp );

	} catch (error) {
    res.status(500).send({message:error})
  }
};


exports.updateCursoErp = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const updateCursoErp = await cursoserp.updateCursoErp( id, req.body ).then(response=> response)

	  res.send({ updateCursoErp, message:'Ciclo actualizado correctamente' });

	} catch (error) {
    res.status(500).send({message:error})
  }
}

exports.deleteCursoErp = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const deleteCursoErp = await cursoserp.deleteCursoErp( id, req.body ).then(response=> response)

	  res.send( deleteCursoErp );

	} catch (error) {
    res.status(500).send({message:error})
  }
}

