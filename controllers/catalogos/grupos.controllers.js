const grupos = require("../../models/catalogos/grupos.model");

// Obtener el listado de grupos
exports.getGrupos = async(req, res) => {
  try {
  	// Consultar todos los grupos
		const getGrupos = await grupos.getGrupos( ).then(response=> response)
		// Enviar los grupos
		res.send(getGrupos);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

// Obtener el listado de grupos
exports.getGruposActivos = async(req, res) => {
  try {
  	// Consultar solo los grupos activos
		const getGruposActivos = await grupos.getGruposActivos( ).then(response=> response)

		// enviar los grupos
		res.send(getGruposActivos);

  } catch (error) {
    res.status(500).send({message:error})
  }
};


// Obtener el listado de grupos
exports.getGruposAlumnos = async(req, res) => {
  try {
  	const { id } = req.params
  	// Consultar solo los grupos activos
		const getGruposActivos = await grupos.getGruposAlumnos( id ).then(response=> response)

		// enviar los grupos
		res.send(getGruposActivos);

  } catch (error) {
    res.status(500).send({message:error})
  }
};


// Obtener el listado de grupos
exports.getGrupoTeachers = async(req, res) => {
  try {
  	const { id } = req.params
  	// Consultar solo los grupos activos
		const gruposTeacher = await grupos.getGrupoTeachers( id ).then(response=> response)

		// enviar los grupos
		res.send(gruposTeacher);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addGrupo = async (req, res) => {
	try {

		const { id_grupo_original } = req.body
	  //validamos que no venga vacio el body
	  if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

	  const grupoMax = await grupos.getIdMaxGrupo( ).then(response=> response)

	  // Agregar el grupos
	  const addGrupo = await grupos.addGrupo( req.body, grupoMax.id_grupo ).then(response=> response)

	  // Actualizar el grupo relacionado 
	  const updateGrupoIdConsecutivo = await grupos.updateGrupoIdConsecutivo( grupoMax.id_grupo, req.body.id_usuario, id_grupo_original).then(response=> response)

	  // Enviar el ciclo
	  res.send({ addGrupo, message:'Grupo creado correctamente', id: grupoMax.id_grupo });


	} catch (error) {
    res.status(500).send({message:error})
  }
};


exports.updateGrupo = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const updateGrupo = await grupos.updateGrupo( id, req.body ).then(response=> response)

	  res.send({ updateGrupo, message:'Grupo actualizado correctamente' });

	} catch (error) {
    res.status(500).send({message:error})
  }
}

exports.deleteGrupo = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const deleteGrupo = await grupos.deleteGrupo( id, req.body ).then(response=> response)
	  res.send({ deleteGrupo, message:'Grupo eliminado correctamente' });

	} catch (error) {
    res.status(500).send({message:error})
  }
}



exports.getGruposHabiles = async(req, res) => {
  try {

  	// Sacamos el id de la escuela que está solicitando los grupos
  	const { id } = req.params

  	// Consultar solo los grupos activos
		const getGruposHabiles = await grupos.getGruposHabiles( id ).then(response=> response)

		console.log( getGruposHabiles[0] )

		// Sacar los datos unicos de planteles, horarios, ciclos, niveles, y ordendos de manera alfabetica
		const planteles = getGruposHabiles.map(( planteles ) => { return planteles.plantel }).sort((a, b) => a - b);

		const ciclos    = getGruposHabiles.map(( ciclos ) => { return ciclos.ciclo }).sort((a, b) => a - b);
		
		const niveles   = getGruposHabiles.map(( niveles ) => { return niveles.nivel }).sort((a, b) => a - b);

		const horarios  = getGruposHabiles.map(( horarios ) => { return horarios.horario }).sort((a, b) => a - b);

		// enviar los grupos
		res.send({
			grupos    : getGruposHabiles,
			planteles : [...new Set(planteles)],
			ciclos    : [...new Set(ciclos)],
			niveles   : [...new Set(niveles)],
			horarios  : [...new Set(horarios)],
		});

  } catch (error) {
    res.status(500).send({message:error})
  }
};