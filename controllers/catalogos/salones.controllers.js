const salones = require("../../models/catalogos/salones.model");

// Obtener el listado de salones
exports.getSalones = async(req, res) => {
  try {
  	// Consultar todos los salones
		const getSalones = await salones.getSalones( ).then(response=> response)
		// Enviar los salones
		res.send(getSalones);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

// Obtener el listado de salones
exports.getSalonesActivos = async(req, res) => {
  try {
  	// Consultar solo los salones activos
		const getSalonesActivos = await salones.getSalonesActivos( ).then(response=> response)

		// enviar los salones
		res.send(getSalonesActivos);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addSalon = async (req, res) => {
	try {

	  //validamos que no venga vacio el body
	  if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

	  // Agregar el salones
	  const addSalon = await salones.addSalon( req.body ).then(response=> response)
	  // Enviar el ciclo
	  res.send( addSalon );

	} catch (error) {
    res.status(500).send({message:error})
  }
};


exports.updateSalon = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const updateSalon = await salones.updateSalon( id, req.body ).then(response=> response)

	  res.send({ updateSalon, message:'Ciclo actualizado correctamente' });

	} catch (error) {
    res.status(500).send({message:error})
  }
}

exports.deleteSalon = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const deleteSalon = await salones.deleteSalon( id, req.body ).then(response=> response)

	  res.send( deleteSalon );

	} catch (error) {
    res.status(500).send({message:error})
  }
}

