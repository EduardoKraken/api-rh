const Clientes = require("../../models/catalogos/clientes.model");

exports.getClientes = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores
  //y data= los datos que estamos recibiendo
  Clientes.getClientes((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los clientes",
      });
    else res.send(data);
  });
};

exports.getCliente = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores
  //y data= los datos que estamos recibiendo
  Clientes.getCliente(req.params.id, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los temas",
      });
    else res.send(data);
  });
};

exports.addCliente = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacio" });
  }

  Clientes.addCliente(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el cliente",
      });
    else res.status(200).send({ message: data });
  });
};

exports.updateCliente = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }

  Clientes.updateCliente(req.params.idCliente, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No se encontro el cliente con id : ${req.params.idCliente}`,
        });
      } else {
        res.status(500).send({
          message:
            "Error al actualizar el cliente con el id : " +
            req.params.idCliente,
        });
      }
    } else {
      res
        .status(200)
        .send({ message: `El cliente se ha actualizado correctamente.` });
    }
  });
};

exports.deleteCliente = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores
  //y data= los datos que estamos recibiendo
  Clientes.deleteCliente(req.params.idCliente, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al eliminar al cliente",
      });
    else res.send(data);
  });
};
