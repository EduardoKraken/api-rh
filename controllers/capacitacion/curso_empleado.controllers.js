const cursoEmpleado = require("../../models/capacitacion/curso_empleado.model.js");
const cursos        = require("../../models/capacitacion/cursos.model.js");
const entradas      = require("../../models/catalogos/entradas.model.js");


exports.getCursoEmpleado = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    cursoEmpleado.getCursoEmpleado((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los cursos empleados"
            });
        else res.send(data);
    });
};

exports.getCursoEmpleadoId = async (req, res) => {
  const { id } = req.params

  try{

    let getCursos = await cursoEmpleado.getCursoEmpleadoId( id ).then( response => response )
    const usuariosERP = await cursos.usuariosERP( ).then(response=> response)

    for( const i in getCursos){
      const { id_usucario_capacitor, id_usuario_registro, cantidad_evaluaciones, evaluaciones_contestadas } = getCursos[i]

      const existeUsuario  =  usuariosERP.find( el => el.id_usuario == id_usucario_capacitor )
      const existeEmpleado =  usuariosERP.find( el => el.id_usuario == id_usuario_registro )

      getCursos[i]['capacitador']  = existeUsuario  ? existeUsuario.nombre_completo  : ''
      getCursos[i]['empleado']     = existeEmpleado ? existeEmpleado.nombre_completo : ''
      getCursos[i]['avance']       = evaluaciones_contestadas > 0 ?  ((evaluaciones_contestadas / cantidad_evaluaciones) * 100).toFixed(0) : 0
    }

    res.send ( getCursos )
  }catch( error ){
    res.status(500).send({ message: error ? error.message : "Se produjo algún error al recuperar los cursos empleados" });
  }

};

exports.addCursoEmpleado = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    cursoEmpleado.addCursoEmpleado(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear el curso empleado"
            })
        else res.status(200).send(data);
    })
};

exports.updateCursoEmpleado = (req, res) => {

  if (!req.body || Object.keys(req.body).length === 0) {
      res.status(400).send({ message: "El contenido no puede estar vacío" });
  }

  cursoEmpleado.updateCursoEmpleado(req.params.idCursoEmpleado, req.body, (err, data) => {
    if (err) {
        if (err.kind === "not_found") {
            res.status(404).send({ message: `No se encontro el curso empleado con id : ${req.params.idCursoEmpleado}` });
        } else {
            res.status(500).send({ message: "Error al actualizar el curso empleado con el id : " + req.params.idCursoEmpleado });
        }
    } else {
        res.status(200).send({ message: `El curso empleado se ha actualizado correctamente.` })
    }
  })
}


exports.getCursoIdEmpleadoId = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  cursoEmpleado.getCursoIdEmpleadoId(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar el curso"
      });
    res.status(200).send(data)
  })
}


exports.inscribirTeacherCurso = async (req, res) => {

  try{

    const { iderp, idcurso } = req.body

    const inscribirTeacherCurso = await cursoEmpleado.inscribirTeacherCurso( iderp, idcurso ).then( response => response )

    res.send ( inscribirTeacherCurso )
  }catch( error ){
    res.status(500).send({ message: error ? error.message : "Se produjo algún error al recuperar los cursos empleados" });
  }

};


exports.getInscribirTeacherCurso = async (req, res) => {

  try{

    const getInscribirTeacherCurso = await cursoEmpleado.getInscribirTeacherCurso( ).then( response => response )

    let iderps = getInscribirTeacherCurso.map(( registro ) => { return registro.iderp })
    iderps = iderps.length ? iderps : [0]

    // consultar a los usuarios del erp
    const usuariosSistema = await entradas.getUsuariosERP( iderps ).then( response => response )


    for( const i in usuariosSistema ){
      const existeEmpleado = getInscribirTeacherCurso.find( el => el.iderp == usuariosSistema[i].id_usuario )
      usuariosSistema[i]['fecha_creacion'] = existeEmpleado ? existeEmpleado.fecha_creacion : '' 
    }


    res.send ( usuariosSistema )
  }catch( error ){
    res.status(500).send({ message: error ? error.message : "Se produjo algún error al recuperar los cursos empleados" });
  }

};