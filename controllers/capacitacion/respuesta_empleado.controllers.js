const Respuestas = require("../../models/capacitacion/respuesta_empleado.model.js");

exports.getRespuestas = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Respuestas.getRespuestas((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar las preguntas"
            });
        else res.send(data);
    });
};

exports.addRespuesta = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    Respuestas.addRespuesta(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la pregunta"
            })
        else res.status(200).send({ message: `La pregunta se ha creado correctamente` });
    })
};

exports.updateRespuesta = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Respuestas.updateRespuesta(req.params.idRespuesta, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro la respuesta con id : ${req.params.idRespuesta}` });
            } else {
                res.status(500).send({ message: "Error al actualizar la respuesta con el id : " + req.params.idRespuesta });
            }
        } else {
            res.status(200).send({ message: `La respuesta se ha actualizado correctamente.` })
        }
    })
}