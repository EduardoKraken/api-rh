const asignacion = require("../../models/capacitacion/asignacion_curso.model.js");

exports.getAsignacion = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  asignacion.getAsignacion((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los cursos"
      });
    else res.send(data);
  });
};

exports.addAsignacion = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  asignacion.addAsignacion(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el curso"
      })
    else res.status(200).send(data);
  })
};

exports.updateAsignacion = (req, res) => {

  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "La asignacion no puede estar vacío" });
  }

  asignacion.updateAsignacion(req.params.id, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro la asignacion con id : ${req.params.id}` });
      } else {
        res.status(500).send({ message: "Error al actualizar la asignacion con el id : " + req.params.id });
      }
    } else {
      res.status(200).send({ message: `La asignacion se ha actualizado correctamente.` })
    }
  })
}


exports.getAsignacionExiste = (req, res) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }
  const { tipo_asignacion } = req.body

  let consulta = ''
  if( tipo_asignacion == 4 ){
    consulta = `SELECT * FROM asignacion_cursos WHERE deleted=0 AND idempleado = ${ req.body.idempleado} AND idcurso = ${ req.body.idcurso};`
  }

  asignacion.getAsignacionExiste(consulta,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar el curso"
      });
    else res.send(data);
  })
}

// Obtener el listado de horarios
exports.getAsignacionExiste = async(req, res) => {
  try {
    // Declaramos las variables iniciales
    let message   = ''
    let consulta  = ''
    let estatus   = ''
    // obtenemos el tipo de asignación
    const { idempleado, idcurso } = req.body

    // Verificamos si existe un curo_empleado 
    const existeCursoEmpleado = await asignacion.getCursoEmpleado( idempleado, idcurso ).then(response=> response)
    
    // Validamos que no tenga niguna
    if(existeCursoEmpleado.length == 0){
      // Y como no tiene tenemos 
      const { idasignacion } = await asignacion.addAsignacion( req.body ).then(response=> response)
      // Insertamos el curso_empleado
      const addCursoEmpleado = await asignacion.addCursoEmpleado( req.body, idasignacion ).then(response=> response)

      message = 'Curso creado correctamente'
      estatus = 0

    }else{
      message = 'El usuario ya cuenta con un curso asignado'
      estatus = 1
    }

    // Envíamos la respuesta
    // Estatus 1 = ya existe, 0 = no existe, se agrego
    res.send({message, estatus});
  } catch (error) {
    res.status(500).send({message:error})
  }
};