const SubtemaEmpleado = require("../../models/capacitacion/subtema_empleado.model.js");

exports.getSubtemaEmpleados = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    SubtemaEmpleado.getSubtemaEmpleados((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los subtemas"
            });
        else res.send(data);
    });
};

exports.addSubtemaEmpleado = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    SubtemaEmpleado.addSubtemaEmpleado(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear el subtema"
            })
        else res.status(200).send({ message: `El subtema se ha creado correctamente` });
    })
};

exports.updateSubtemaEmpleado = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    SubtemaEmpleado.updateSubtemaEmpleado(req.params.idSubtemaEmpleado, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro el subtema con id : ${req.params.idSubtemaEmpleado}` });
            } else {
                res.status(500).send({ message: "Error al actualizar el subtema con el id : " + req.params.idSubtemaEmpleado });
            }
        } else {
            res.status(200).send({ message: `El subtema se ha actualizado correctamente.` })
        }
    })
}