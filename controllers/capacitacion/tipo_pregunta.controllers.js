const TipoPregunta = require("../../models/capacitacion/tipo_pregunta.model.js");

exports.getTipoPreguntas = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    TipoPregunta.getTipoPreguntas((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar las preguntas"
            });
        else res.send(data);
    });
};

exports.addTipoPregunta = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    TipoPregunta.addTipoPregunta(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la pregunta"
            })
        else res.status(200).send({ message: `La pregunta se ha creado correctamente` });
    })
};

exports.updateTipoPregunta = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    TipoPregunta.updateTipoPregunta(req.params.idTipoPregunta, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro la pregunta con id : ${req.params.idTipoPregunta}` });
            } else {
                res.status(500).send({ message: "Error al actualizar la pregunta con el id : " + req.params.idTipoPregunta });
            }
        } else {
            res.status(200).send({ message: `La pregunta se ha actualizado correctamente.` })
        }
    })
}