const evaluaciones = require("../../models/capacitacion/evaluaciones.model.js");
const catalogos      = require("../../models/lms/catalogoslms.model.js");



exports.getEvaluaciones = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
  evaluaciones.getEvaluaciones((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las evaluaciones"
      });
    else res.send(data);
  });
};

exports.addEvaluacion = (req, res) => {
    //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacio" });
  }

  evaluaciones.addEvaluacion(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la evaluacion"
      })
    else res.status(200).send(data);
  })
};

exports.updateEvaluacion = (req, res) => {

  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }

  evaluaciones.updateEvaluacion(req.params.idEvaluacion, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro la evaluacion con id : ${req.params.idEvaluacion}` });
      } else {
        res.status(500).send({ message: "Error al actualizar la evaluacion con el id : " + req.params.idEvaluacion });
      }
    } else {
      res.status(200).send({ message: `la evaluacion se ha actualizado correctamente.` })
    }
  })
}


exports.getEvaluacionesSubtema = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  evaluaciones.getEvaluacionesSubtema(req.params.id, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las evaluaciones"
      });
    else res.send(data);
  });
};

// Enviar el recibo de pago al alumno
exports.getPanelCapacitacionTeacher = async(req, res) => {
  try {

    const headersFast = [
      { text: 'Teacher'   , value: 'nombre_completo' },
      { text: 'Avance T'  , value: 'avance_teoria'      , class: 'header_avance white--text' },
      { text: 'Avance P'  , value: 'avance_practica'    , class: 'header_avance white--text' },
      { text: 'Avance G'  , value: 'avance_general'     , class: 'header_avance white--text' },
      { text: 'Nivel 1' , value: 'nivel_1_fast'  , class: 'header_fast white--text'  },
      { text: 'Nivel 2' , value: 'nivel_2_fast'  , class: 'header_fast white--text'  },
      { text: 'Nivel 3' , value: 'nivel_3_fast'  , class: 'header_fast white--text'  },
      { text: 'Nivel 4' , value: 'nivel_4_fast'  , class: 'header_fast white--text'  },
      { text: 'Nivel 5' , value: 'nivel_5_fast'  , class: 'header_fast white--text'  },
      { text: 'Nivel 6' , value: 'nivel_6_fast'  , class: 'header_fast white--text'  },
      { text: 'Nivel 7' , value: 'nivel_7_fast'  , class: 'header_fast white--text'  },
      { text: 'Nivel 8' , value: 'nivel_8_fast'  , class: 'header_fast white--text'  },
      { text: 'Nivel 9' , value: 'nivel_9_fast'  , class: 'header_fast white--text'  },
      { text: 'Nivel 10', value: 'nivel_10_fast' , class: 'header_fast white--text'  },
      { text: 'Nivel 11', value: 'nivel_11_fast' , class: 'header_fast white--text'  },
      { text: 'Nivel 12', value: 'nivel_12_fast' , class: 'header_fast white--text'  },
      { text: 'Nivel 13', value: 'nivel_13_fast' , class: 'header_fast white--text'  },
      { text: 'Nivel 14', value: 'nivel_14_fast' , class: 'header_fast white--text'  },
    ]

    const headersInbi = [
      { text: 'Teacher'   , value: 'nombre_completo' },
      { text: 'Avance T'  , value: 'avance_inbi_t'    , class: 'header_avance white--text'  },
      { text: 'Avance P'  , value: 'avance_inbi_p'    , class: 'header_avance white--text'  },
      { text: 'Avance G'  , value: 'avance_inbi_g'    , class: 'header_avance white--text'  },
      
      { text: 'Avance T'  , value: 'avance_teens_t'   , class: 'header_avance_teens black--text'  },
      { text: 'Avance P'  , value: 'avance_teens_p'   , class: 'header_avance_teens black--text'  },
      { text: 'Avance G'  , value: 'avance_teens_g'   , class: 'header_avance_teens black--text'  },

      { text: 'Nivel 1' , value: 'nivel_1_inbi'  , class: 'header_inbi white--text'  },
      { text: 'Nivel 1' , value: 'nivel_1_teens' , class: 'header_teens white--text' },
      { text: 'Nivel 2' , value: 'nivel_2_inbi'  , class: 'header_inbi white--text'  },
      { text: 'Nivel 2' , value: 'nivel_2_teens' , class: 'header_teens white--text' },
      { text: 'Nivel 3' , value: 'nivel_3_inbi'  , class: 'header_inbi white--text'  },
      { text: 'Nivel 3' , value: 'nivel_3_teens' , class: 'header_teens white--text' },
      { text: 'Nivel 4' , value: 'nivel_4_inbi'  , class: 'header_inbi white--text'  },
      { text: 'Nivel 4' , value: 'nivel_4_teens' , class: 'header_teens white--text' },
      { text: 'Nivel 5' , value: 'nivel_5_inbi'  , class: 'header_inbi white--text'  },
      { text: 'Nivel 5' , value: 'nivel_5_teens' , class: 'header_teens white--text' },
      { text: 'Nivel 6' , value: 'nivel_6_inbi'  , class: 'header_inbi white--text'  },
      { text: 'Nivel 6' , value: 'nivel_6_teens' , class: 'header_teens white--text' },
      { text: 'Nivel 7' , value: 'nivel_7_inbi'  , class: 'header_inbi white--text'  },
      { text: 'Nivel 7' , value: 'nivel_7_teens' , class: 'header_teens white--text' },
      { text: 'Nivel 8' , value: 'nivel_8_inbi'  , class: 'header_inbi white--text'  },
      { text: 'Nivel 8' , value: 'nivel_8_teens' , class: 'header_teens white--text' },
      { text: 'Nivel 9' , value: 'nivel_9_inbi'  , class: 'header_inbi white--text'  },
      { text: 'Nivel 9' , value: 'nivel_9_teens' , class: 'header_teens white--text' },
      { text: 'Nivel 10', value: 'nivel_10_inbi' , class: 'header_inbi white--text'  },
      { text: 'Nivel 10', value: 'nivel_10_teens', class: 'header_teens white--text' },
      { text: 'Nivel 11', value: 'nivel_11_inbi' , class: 'header_inbi white--text'  },
      { text: 'Nivel 11', value: 'nivel_11_teens', class: 'header_teens white--text' },
      { text: 'Nivel 12', value: 'nivel_12_inbi' , class: 'header_inbi white--text'  },
      { text: 'Nivel 12', value: 'nivel_12_teens', class: 'header_teens white--text' },
      { text: 'Nivel 13', value: 'nivel_13_inbi' , class: 'header_inbi white--text'  },
      { text: 'Nivel 13', value: 'nivel_13_teens', class: 'header_teens white--text' },
      { text: 'Nivel 14', value: 'nivel_14_inbi' , class: 'header_inbi white--text'  },
      { text: 'Nivel 14', value: 'nivel_14_teens', class: 'header_teens white--text' },
      { text: 'Nivel 15', value: 'nivel_15_teens', class: 'header_teens white--text' },
      { text: 'Nivel 16', value: 'nivel_16_teens', class: 'header_teens white--text' },
    ]

    const niveles_fast  = [ 1,2,3,4,5,6,7,8,9,10,11,12,13,14 ]
    const niveles_inbi  = [ 1,2,3,4,5,6,7,8,9,10,11,12,13,14 ]
    const niveles_teens = [ 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 ]

    // CONSULTAR LOS TEACHER ACTIVOS DEL ERP VIEJO
    let teachersERP = await catalogos.getTeachersLMSERP( ).then(response=> response)
    const idTeachers = teachersERP.map((registro) => { return registro.id_usuario })

    // CONSULTAR LOS id DE LOS TEACHER Y VER LOS GRUPOS EN LOS QUE HA DADO NIVEL EL TEACHER
    const maestrosGrupo = await evaluaciones.getMaestrosGrupo( idTeachers ).then( response => response )



    // // CONSULTAR LOS HORARIOS DISPONIBLES DE LOS TEACHERS
    // const horariosTeachers   = await evaluaciones.getHorariosTeacher( idTeachers ).then( response => response )

    // // CONSULTAR LAS SUCURSALES DONDE PUEDE DAR CLASE EL TEACHER
    // const sucursalesTeachers = await evaluaciones.getSucursalesTeacher( idTeachers ).then( response => response )

    for( const i in maestrosGrupo ){

      const { nivel } = maestrosGrupo[i] 

      maestrosGrupo[i]['nivel'] = nivel.replace(/[^0-9.]/g, '').trim()
    }

    const calificacionesTeacher = await evaluaciones.getCalificacionesTeacher( ).then( response => response )


    for( const i in calificacionesTeacher ){

      const { subtema } = calificacionesTeacher[i] 

      calificacionesTeacher[i]['nivel'] = subtema.replace(/[^0-9.]/g, '').trim()
    }

    for( const i in teachersERP ){

      const { id_usuario } = teachersERP[i]

      // AGREGARLES LAS SUCURSALES A LOS USUARIOS
      // teachersERP[i]['planteles'] = sucursalesTeachers.filter( el => { return el.iderp == id_usuario })
      // teachersERP[i]['horarios']  = horariosTeachers.filter( el => { return el.iderp == id_usuario })

      // VERIFICAR LOS NIVELES QUE HA DADO DE FAST
      for( const j in niveles_fast ){

        const existenGrupos = Array.from(new Set( maestrosGrupo.filter( el => { return el.id_maestro == id_usuario && el.cursos == 2 }).map((registro => { return parseInt( registro.nivel ) })) ))
        const existeTeoria  = Array.from(new Set( calificacionesTeacher.filter( el => { return el.idempleado == id_usuario && el.cursos == 2 }).map((registro => { return parseInt( registro.nivel ) })) ))

        if( existenGrupos.length && existeTeoria.length ){
          if( existenGrupos.includes( niveles_fast[j] ) && !existeTeoria.includes( niveles_fast[j] ) ) {
            teachersERP[i][`nivel_${niveles_fast[j]}_fast`] = 1
          }else if( !existenGrupos.includes( niveles_fast[j] ) && existeTeoria.includes( niveles_fast[j] ) ) {
            teachersERP[i][`nivel_${niveles_fast[j]}_fast`] = 2
          }else if( existenGrupos.includes( niveles_fast[j] ) && existeTeoria.includes( niveles_fast[j] ) ) {
            teachersERP[i][`nivel_${niveles_fast[j]}_fast`] = 3
          }else{
            teachersERP[i][`nivel_${niveles_fast[j]}_fast`] = 0
          }
        }else if( existenGrupos.length && !existeTeoria.length ){
          if( existenGrupos.includes( niveles_fast[j] ) ) {
            teachersERP[i][`nivel_${niveles_fast[j]}_fast`] = 1
          }else{
            teachersERP[i][`nivel_${niveles_fast[j]}_fast`] = 0
          }
        }else if( !existenGrupos.length && existeTeoria.length ){
          if( !existenGrupos.includes( niveles_fast[j] ) && existeTeoria.includes( niveles_fast[j] ) ) {
            teachersERP[i][`nivel_${niveles_fast[j]}_fast`] = 2
          }else {
            teachersERP[i][`nivel_${niveles_fast[j]}_fast`] = 0
          }
        }else {
          teachersERP[i][`nivel_${niveles_fast[j]}_fast`] = 0
        }

      }

      // VERIFICAR LOS NIVELES QUE HA DADO DE INBI
      for( const j in niveles_inbi ){

        const existenGrupos = Array.from(new Set( maestrosGrupo.filter( el => { return el.id_maestro == id_usuario && el.cursos == 1 }).map((registro => { return parseInt( registro.nivel ) })) ))
        const existeTeoria  = Array.from(new Set( calificacionesTeacher.filter( el => { return el.idempleado == id_usuario && el.cursos == 1 }).map((registro => { return parseInt( registro.nivel ) })) ))

        if( existenGrupos.length && existeTeoria.length ){
          if( existenGrupos.includes( niveles_inbi[j] ) && !existeTeoria.includes( niveles_inbi[j] ) ) {
            teachersERP[i][`nivel_${niveles_inbi[j]}_inbi`] = 1
          }else if( !existenGrupos.includes( niveles_inbi[j] ) && existeTeoria.includes( niveles_inbi[j] ) ) {
            teachersERP[i][`nivel_${niveles_inbi[j]}_inbi`] = 2
          }else if( existenGrupos.includes( niveles_inbi[j] ) && existeTeoria.includes( niveles_inbi[j] ) ) {
            teachersERP[i][`nivel_${niveles_inbi[j]}_inbi`] = 3
          }else{
            teachersERP[i][`nivel_${niveles_inbi[j]}_inbi`] = 0
          }
        }else if( existenGrupos.length && !existeTeoria.length ){
          if( existenGrupos.includes( niveles_inbi[j] ) ) {
            teachersERP[i][`nivel_${niveles_inbi[j]}_inbi`] = 1
          }else{
            teachersERP[i][`nivel_${niveles_inbi[j]}_inbi`] = 0
          }
        }else if( !existenGrupos.length && existeTeoria.length ){
          if( !existenGrupos.includes( niveles_inbi[j] ) && existeTeoria.includes( niveles_inbi[j] ) ) {
            teachersERP[i][`nivel_${niveles_inbi[j]}_inbi`] = 2
          }else {
            teachersERP[i][`nivel_${niveles_inbi[j]}_inbi`] = 0
          }
        }else {
          teachersERP[i][`nivel_${niveles_inbi[j]}_inbi`] = 0
        }
      }

      // VERIFICAR LOS NIVELES QUE HA DADO DE INBI
      for( const j in niveles_teens ){

        const existenGrupos = Array.from(new Set( maestrosGrupo.filter( el => { return el.id_maestro == id_usuario && el.cursos == 3 }).map((registro => { return parseInt( registro.nivel ) })) ))
        const existeTeoria  = Array.from(new Set( calificacionesTeacher.filter( el => { return el.idempleado == id_usuario && el.cursos == 3 }).map((registro => { return parseInt( registro.nivel ) })) ))

        if( existenGrupos.length && existeTeoria.length ){
          if( existenGrupos.includes( niveles_teens[j] ) && !existeTeoria.includes( niveles_teens[j] ) ) {
            teachersERP[i][`nivel_${niveles_teens[j]}_teens`] = 1
          }else if( !existenGrupos.includes( niveles_teens[j] ) && existeTeoria.includes( niveles_teens[j] ) ) {
            teachersERP[i][`nivel_${niveles_teens[j]}_teens`] = 2
          }else if( existenGrupos.includes( niveles_teens[j] ) && existeTeoria.includes( niveles_teens[j] ) ) {
            teachersERP[i][`nivel_${niveles_teens[j]}_teens`] = 3
          }else{
            teachersERP[i][`nivel_${niveles_teens[j]}_teens`] = 0
          }
        }else if( existenGrupos.length && !existeTeoria.length ){
          if( existenGrupos.includes( niveles_teens[j] ) ) {
            teachersERP[i][`nivel_${niveles_teens[j]}_teens`] = 1
          }else{
            teachersERP[i][`nivel_${niveles_teens[j]}_teens`] = 0
          }
        }else if( !existenGrupos.length && existeTeoria.length ){
          if( !existenGrupos.includes( niveles_teens[j] ) && existeTeoria.includes( niveles_teens[j] ) ) {
            teachersERP[i][`nivel_${niveles_teens[j]}_teens`] = 2
          }else {
            teachersERP[i][`nivel_${niveles_teens[j]}_teens`] = 0
          }
        }else {
          teachersERP[i][`nivel_${niveles_teens[j]}_teens`] = 0
        }


      }


    }

    for( const i in teachersERP ){

      const { id_usuario } = teachersERP[i]

      let sumatoria = 0
      let sumatoria_t = 0
      let sumatoria_p = 0

      // VERIFICAR LOS NIVELES QUE HA DADO DE FAST
      for( const j in niveles_fast ){
        if( [3].includes( teachersERP[i][`nivel_${niveles_fast[j]}_fast`] ) ){
          sumatoria += 2
          sumatoria_t += 1
          sumatoria_p += 1
        }else if( [1].includes( teachersERP[i][`nivel_${niveles_fast[j]}_fast`] ) ){
          sumatoria += 1
          sumatoria_p += 1
        }else if( [2].includes( teachersERP[i][`nivel_${niveles_fast[j]}_fast`] ) ){
          sumatoria += 1
          sumatoria_t += 1
        }
      }

      teachersERP[i]['avance_teoria']   = sumatoria_t > 0 ? ( (sumatoria_t / 14 ) * 100 ).toFixed(1) : 0
      teachersERP[i]['avance_practica'] = sumatoria_p > 0 ? ( (sumatoria_p / 14 ) * 100 ).toFixed(1) : 0
      teachersERP[i]['avance_general']  = sumatoria > 0   ? ( (sumatoria / 28 ) * 100 ).toFixed(1) : 0
    }


    for( const i in teachersERP ){

      const { id_usuario } = teachersERP[i]

      let sumatoria = 0
      let sumatoria_t = 0
      let sumatoria_p = 0

      // VERIFICAR LOS NIVELES QUE HA DADO DE FAST
      for( const j in niveles_inbi ){
        if( [3].includes( teachersERP[i][`nivel_${niveles_inbi[j]}_inbi`] ) ){
          sumatoria += 2
          sumatoria_t += 1
          sumatoria_p += 1
        }else if( [1].includes( teachersERP[i][`nivel_${niveles_inbi[j]}_inbi`] ) ){
          sumatoria += 1
          sumatoria_p += 1
        }else if( [2].includes( teachersERP[i][`nivel_${niveles_inbi[j]}_inbi`] ) ){
          sumatoria += 1
          sumatoria_t += 1
        }
      }
      teachersERP[i]['avance_inbi_t'] = sumatoria_t > 0 ? ( (sumatoria_t / 14 ) * 100 ).toFixed(1) : 0
      teachersERP[i]['avance_inbi_p'] = sumatoria_p > 0 ? ( (sumatoria_p / 14 ) * 100 ).toFixed(1) : 0
      teachersERP[i]['avance_inbi_g'] = sumatoria > 0   ? ( (sumatoria / 28 ) * 100 ).toFixed(1) : 0
    }

    for( const i in teachersERP ){

      const { id_usuario } = teachersERP[i]

      let sumatoria = 0
      let sumatoria_t = 0
      let sumatoria_p = 0

      // VERIFICAR LOS NIVELES QUE HA DADO DE FAST
      for( const j in niveles_teens ){
        if( [3].includes( teachersERP[i][`nivel_${niveles_teens[j]}_teens`] ) ){
          sumatoria += 2
          sumatoria_t += 1
          sumatoria_p += 1
        }else if( [1].includes( teachersERP[i][`nivel_${niveles_teens[j]}_teens`] ) ){
          sumatoria += 1
          sumatoria_p += 1
        }else if( [2].includes( teachersERP[i][`nivel_${niveles_teens[j]}_teens`] ) ){
          sumatoria += 1
          sumatoria_t += 1
        }
      }
      teachersERP[i]['avance_teens_t'] = sumatoria_t > 0 ? ( (sumatoria_t / 16 ) * 100 ).toFixed(1) : 0
      teachersERP[i]['avance_teens_p'] = sumatoria_p > 0 ? ( (sumatoria_p / 16 ) * 100 ).toFixed(1) : 0
      teachersERP[i]['avance_teens_g'] = sumatoria > 0   ? ( (sumatoria / 32 ) * 100 ).toFixed(1) : 0
    }

    res.send({ headersFast, headersInbi, teachersERP });

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor'})
  }
};

// Enviar el recibo de pago al alumno
exports.getPanelCapacitacionUsuario = async(req, res) => {
  try {
    const { matricula, adeudo, correo } = req.body

    const correoEnviado  = await reciboExci.enviarCorreo( 'Recibo de pago', correoHtml, correo, transporter, archivos ).then( response=> response ) 

    res.send({message: 'Correo enviado'});

  } catch (error) {
    res.status(500).send({message:error})
  }
};
