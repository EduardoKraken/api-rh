const cursos = require("../../models/capacitacion/cursos.model.js");

// Obtener el listado de horarios
exports.getCursos = async(req, res) => {
  try {
    // Consultar todos los ciclos
    let getCursos   = await cursos.getCursos( ).then(response=> response)
    const usuariosERP = await cursos.usuariosERP( ).then(response=> response)

    // Recorrer los cursos para poder poner el usuario y el capacitor
    for( const i in getCursos ){
      const { id_usucario_capacitor, id_usuario_registro } = getCursos[i]

      const existeUsuario  =  usuariosERP.find( el => el.id_usuario == id_usucario_capacitor )
      const existeEmpleado =  usuariosERP.find( el => el.id_usuario == id_usuario_registro )

      getCursos[i]['capacitador'] = existeUsuario  ? existeUsuario.nombre_completo  : ''
      getCursos[i]['empleado']    = existeEmpleado ? existeEmpleado.nombre_completo : ''
    }

    // Enviar los ciclos
    res.send(getCursos);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addCurso = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    cursos.addCurso(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear el curso"
            })
        else res.status(200).send({ message: `El curso se ha creado correctamente` });
    })
};

exports.updateCurso = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    cursos.updateCurso(req.params.idCurso, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro el curso con id : ${req.params.idCurso}` });
            } else {
                res.status(500).send({ message: "Error al actualizar el curso con el id : " + req.params.idCurso });
            }
        } else {
            res.status(200).send({ message: `El curso se ha actualizado correctamente.` })
        }
    })
}

exports.getCursoId = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    cursos.getCursoId(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los temas"
            });
        else res.send(data);
    });
};