const materiales     = require("../../models/capacitacion/materiales.model.js");
const cursos         = require("../../models/capacitacion/cursos.model.js");
const calificaciones = require("../../models/capacitacion/calificaciones.model.js");

const { v4: uuidv4 } = require('uuid')

exports.getMateriales = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  materiales.getMateriales((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los materialess"
      });
    else res.send(data);
  });
};

exports.addMaterial = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacio" });
  }

  materiales.addMaterial(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el material"
      })
    else res.status(200).send({ message: `El material se ha creado correctamente` });
  })
};

exports.updateMaterial = (req, res) => {

  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }

  materiales.updateMaterial(req.params.idMaterial, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro el material con id : ${req.params.idMaterial}` });
      } else {
        res.status(500).send({ message: "Error al actualizar el material con el id : " + req.params.idMaterial });
      }
    } else {
      res.status(200).send({ message: `El material se ha actualizado correctamente.` })
    }
  })
}

exports.deleteMaterial = (req, res) => {
  materiales.deleteMaterial(req.params.idMaterial, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro el material con id : ${req.params.idMaterial}` });
      } else {
        res.status(500).send({ message: "Error al actualizar el material con el id : " + req.params.idMaterial });
      }
    } else {
      res.status(200).send({ message: `El material se ha actualizado correctamente.` })
    }
  })
}


exports.getMaterialesSubtema = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  materiales.getMaterialesSubtema(req.params.id, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las materiales"
      });
    else res.send(data);
  });
};

exports.getMaterialId = async (req, res) => {
  const { id } = req.params

  let material = await materiales.getMaterialId( id ).then( response => response )

  if( !material ){
    return res.status( 400 ).send( {message: 'No existe el material '})
  }

  const { idcapacitador } = material

  const usuariosERP = await cursos.usuariosERP( ).then(response=> response)
  const existeUsuario  =  usuariosERP.find( el => el.id_usuario == idcapacitador )


  material['capacitador'] = existeUsuario  ? existeUsuario.nombre_completo  : ''

  res.send( material )
};

exports.agregarMaterial = async ( req, res) => {
  try{

    const { tipo } = req.params

    if( !req.files ){
      return res.status( 400 ).send({ message : 'El contenido no puede ir vacio' })
    }
    
    // desestrucutramos los arvhios cargados
    const { file } = req.files

    const nombreSplit = file.name.split('.')
    
    // Obtener la extensión del archivo 
    const extensioArchivo = nombreSplit[ nombreSplit.length - 1 ]
    
    let extensiones = []
    let ruta        = ''

    // modificar el nombre del archivo con un uuid unico
    const nombreUuid = uuidv4() + '.' + extensioArchivo
    
    // Videos
    if( tipo == 1 ){
      // extensiones validas
      extensiones = [ 'mp4', 'MOV', 'AVI', 'WMV' ]
      ruta        = './../../capacitacion-videos/' + nombreUuid
    }

    // Audios
    if( tipo == 2 ){
      // extensiones validas
      extensiones = [ 'mp3', 'wav', 'wma' ]
      ruta        = './../../capacitacion-audios/' + nombreUuid
    }

    // Imapgenes
    if( tipo == 3 ){
      // extensiones validas
      extensiones = [ 'BMP', 'GIF', 'jpg', 'jpeg', 'png', 'webp' ]
      ruta        = './../../capacitacion-imagenes/' + nombreUuid
    }

    // Archvios
    if( tipo == 4 ){
      // extensiones validas
      extensiones = [ 'pdf', 'ppt', 'pptx' ]
      ruta        = './../../capacitacion-archivos/' + nombreUuid
    }

    // Archvios
    if( tipo == 6 ){
      // extensiones validas
      extensiones = [ 'ppt', 'pptx' ]
      ruta        = './../../capacitacion-archivos/' + nombreUuid
    }

    // validar que la extensión del archivo recibido sea valido
    if( !extensiones.includes( extensioArchivo) )
      return res.status( 400 ).send({message: `Hola, la extensión: ${ extensioArchivo } no es valida, solo se aceptan: ${ extensiones }`})
    

    file.mv(`${ruta}`, err => {
      if( err )
        return res.status( 400 ).send({ message: err })
      
      return res.send({ message: 'Imagen cargada correctamente', nombre: nombreUuid, extension: extensioArchivo })
    })

  }catch( error ){
    res.status( 500 ).send({ message: 'Error al subir la imagen' })
  }
}

exports.getComentariosMaterial = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  materiales.getComentariosMaterial(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las materiales"
      });
    else res.send(data);
  });
};


exports.getComentariosMaterialAll = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  materiales.getComentariosMaterialAll(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las materiales"
      });
    else res.send(data);
  });
};


exports.addComentariosMaterial = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacio" });
  }

  materiales.addComentariosMaterial(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la nota"
      })
    else res.status(200).send({ message: `Nota agregada correctamente` });
  })
};


// Obtener todos los comentarios de los usuarios de todos los cursos
exports.getComentariosCursos = async (req, res) => {

  try{

    // Obtener todos los cursos por empleado
    let comentariosUsuarios = await materiales.getcomentariosUsuarios( ).then( response => response )

    // Consultamos esos usuarios
    const usuariosERP = await cursos.usuariosERP( ).then(response=> response)

    // Recorrer los cursos por empleado
    for( const i in comentariosUsuarios ){

      // Desestructurar todo
      const { iderp } = comentariosUsuarios[i]

      // Validamos si existe un usuario para agregarle un vendedora
      const existeUsuario = usuariosERP.find( el => el.id_usuario == iderp )

      // Agregar el nombre
      comentariosUsuarios[i]['nombre_completo'] = existeUsuario ? existeUsuario.nombre_completo : ''

    }


    // Retornamos solo los datos que necesitamos de los usuarios
    const arrayEmpleados = comentariosUsuarios.map(({ nombre_completo, iderp }) => ({ nombre_completo, iderp }));

    // Quitamos los duplicados
    let empleadosFinales = arrayEmpleados.filter((objeto, indice, arreglo) =>
      arreglo.findIndex(elemento => elemento.iderp === objeto.iderp) === indice
    );

    // Retornamos solo los datos que necesitamos de los cursos
    const arrayCursos   = comentariosUsuarios.map(({ curso, idcurso }) => ({ curso, idcurso }));

    // Quitamos los duplicados
    let cursosFinales = arrayCursos.filter((objeto, indice, arreglo) =>
      arreglo.findIndex(elemento => elemento.iderp === objeto.iderp) === indice
    );

    // Ordenamos los datos por nombre
    empleadosFinales.sort((a, b) => a.nombre_completo.localeCompare(b.nombre_completo));
    cursosFinales.sort((a, b) => a.curso.localeCompare(b.curso));

    res.send({ 
      comentariosUsuarios, 
      empleadosFinales, 
      cursosFinales, 
    })

  }catch( error ){
    res.status( 500 ).send({message: error ? error.message : 'Error en el servidor' })
  }

  
};