const Preguntas = require("../../models/capacitacion/preguntas.model.js");

exports.getPreguntas = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Preguntas.getPreguntas((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar las preguntas"
            });
        else res.send(data);
    });
};

exports.addPregunta = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    Preguntas.addPregunta(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la pregunta"
            })
        else res.status(200).send(data);
    })
};

exports.updatePregunta = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Preguntas.updatePregunta(req.params.idPregunta, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro la pregunta con id : ${req.params.idPregunta}` });
            } else {
                res.status(500).send({ message: "Error al actualizar la pregunta con el id : " + req.params.idPregunta });
            }
        } else {
            res.status(200).send({ message: `La pregunta se ha actualizado correctamente.` })
        }
    })
}
exports.getPreguntasEvaluacion = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Preguntas.getPreguntasEvaluacion(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar las preguntas"
            });
        else res.send(data);
    });
};

exports.getPreguntasOpcion = async (req, res) => {

  try{
    const { id } = req.params

    let getPreguntasOpcion =  await Preguntas.getPreguntasOpcion( id ).then( response => response )
    for( const i in getPreguntasOpcion){
      getPreguntasOpcion[i]['eleccion'] = ''
    }
    
    res.send( getPreguntasOpcion )

  }catch( error ){
    return res.status( 500 ).send({ message: error ? error.message : 'Error al recuperar las evaluaciones' })
  }
    
};