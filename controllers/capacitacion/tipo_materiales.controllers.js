const TipoMaterial = require("../../models/capacitacion/tipo_materiales.model.js");

exports.getTipoMateriales = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    TipoMaterial.getTipoMateriales((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los materiales"
            });
        else res.send(data);
    });
};

exports.addTipoMaterial = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacio" });
    }

    TipoMaterial.addTipoMaterial(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear el material"
            })
        else res.status(200).send({ message: `El material se ha creado correctamente` });
    })
};

exports.updateTipoMaterial = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    TipoMaterial.updateTipoMaterial(req.params.idTipoMaterial, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro el material con id : ${req.params.idTipoMaterial}` });
            } else {
                res.status(500).send({ message: "Error al actualizar el material con el id : " + req.params.idTipoMaterial });
            }
        } else {
            res.status(200).send({ message: `El material se ha actualizado correctamente.` })
        }
    })
}