const temas = require("../../models/capacitacion/temas.model.js");

exports.getTemas = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    temas.getTemas((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los temas"
            });
        else res.send(data);
    });
};

// exports.getTemasCurso = (req, res) => {
//   //mandamos a llamar al model y le enviamos dos parametros err=para errores 
//   //y data= los datos que estamos recibiendo
//   temas.getTemasCurso(req.params.id,(err, data) => {
//     if (err)
//       res.status(500).send({
//         message: err.message || "Se produjo algún error al recuperar los temas"
//       });
//     else res.send(data);
//   });
// };


exports.getTemasCurso = async(req, res) => {
  try {
    const { id } = req.params
    // Consultar los temas del curso 
    let getTemasCurso = await temas.getTemasCurso( id ).then(response=> response)
    // Consultar los subtemas de los temas
    let mapIdTemas  = getTemasCurso.map((registro) => registro.idtema);
    let getSubtemas = await temas.getSubtemasTema( mapIdTemas ).then(response=> response)

    // Consultar los materiales de los subtemas
    let mapIdSubtemas = getSubtemas.length ? getSubtemas.map((registro) => registro.idsubtema) : [0]
    let getMaterial   = await temas.getMaterialesSubtema( mapIdSubtemas ).then(response=> response)

    // Obtener las evaluaciones de esos subtemas
    let getEvaluaciones = await temas.getEvaluacionesSubtema( mapIdSubtemas ).then(response=> response)
    // Validar si hay evaluaciones
    if( getEvaluaciones.length ){
      // Obtener las preguntas de las evaluaciones
      let mapIdEval      = getEvaluaciones.map((registro) => registro.idevaluacion);
      // Validar si hay evaluaciones 
      let getPreguntas   = await temas.getPreguntasEvaluacion( mapIdEval ).then(response=> response)
      // Agregar el material al subtema
      for( const i in getEvaluaciones ){
        const { idevaluacion } = getEvaluaciones[i]
        getEvaluaciones[i]['preguntas']   = getPreguntas.filter( el => { return el.idevaluacion == idevaluacion })
      }
    }

    // Agregar el material al subtema
    for( const i in getSubtemas ){
      const { idsubtema } = getSubtemas[i]
      getSubtemas[i]['materiales']   = getMaterial.filter( el => { return el.idsubtema == idsubtema })
      getSubtemas[i]['evaluaciones'] = getEvaluaciones.filter( el => { return el.idsubtema == idsubtema })
    }

    // Agregar subtemas al tema
    for( const i in getTemasCurso){
      const { idtema } = getTemasCurso[i]
      // Agregar subtemas
      getTemasCurso[i]['subtemas'] = getSubtemas.filter( el=> { return el.idtema == idtema })
    }

    // Enviar los ciclos
    res.send(getTemasCurso);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.getTemasxCurso = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    temas.getTemasxCurso(req.params.id,(err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los temas"
            });
        else res.send(data);
    });
};



exports.addTema = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    temas.addTema(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear el tema"
            })
        else res.status(200).send({ message: `El tema se ha creado correctamente` });
    })
};

exports.updateTema = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    temas.updateTema(req.params.idTema, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro el tema con id : ${req.params.idTema}` });
            } else {
                res.status(500).send({ message: "Error al actualizar el tema con el id : " + req.params.idTema });
            }
        } else {
            res.status(200).send({ message: `El tema se ha actualizado correctamente.` })
        }
    })
}