const PreguntaMultiple = require("../../models/capacitacion/pregunta_multiple.model.js");

exports.getPreguntaMultiple = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    PreguntaMultiple.getPreguntaMultiple((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar las preguntas"
            });
        else res.send(data);
    });
};

exports.addPreguntaMultiple = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    PreguntaMultiple.addPreguntaMultiple(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la pregunta"
            })
        else res.status(200).send(data);
    })
};

exports.updatePreguntaMultiple = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    PreguntaMultiple.updatePreguntaMultiple(req.params.idPreguntaMultiple, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro la pregunta con id : ${req.params.idPreguntaMultiple}` });
            } else {
                res.status(500).send({ message: "Error al actualizar la pregunta con el id : " + req.params.idPreguntaMultiple });
            }
        } else {
            res.status(200).send({ message: `La pregunta se ha actualizado correctamente.` })
        }
    })
}

exports.getOpcionMultiplePregunta = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    PreguntaMultiple.getOpcionMultiplePregunta(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar las preguntas"
            });
        else res.send(data);
    });
};