const TipoEvaluacion = require("../../models/capacitacion/tipo_evaluacion.model.js");

exports.getTipoEvaluacion = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    TipoEvaluacion.getTipoEvaluacion((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar las evaluaciones"
            });
        else res.send(data);
    });
};

exports.addTipoEvaluacion = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacio" });
    }

    TipoEvaluacion.addTipoEvaluacion(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la evaluacion"
            })
        else res.status(200).send({ message: `la evaluacion se ha creado correctamente` });
    })
};

exports.updateTipoEvaluacion = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    TipoEvaluacion.updateTipoEvaluacion(req.params.idTipoEvaluacion, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro la evaluacion con id : ${req.params.idTipoEvaluacion}` });
            } else {
                res.status(500).send({ message: "Error al actualizar la evaluacion con el id : " + req.params.idTipoEvaluacion });
            }
        } else {
            res.status(200).send({ message: `la evaluacion se ha actualizado correctamente.` })
        }
    })
}