const capacitador = require("../../models/capacitacion/capacitador.model.js");
const cursos      = require("../../models/capacitacion/cursos.model.js");

// Agregar mensaje para el capacitador
exports.agregarMensajeCapacitador = async (req, res) => {

  // Validamos que mande datos
  if (!req.body || Object.keys(req.body).length === 0) {
    return res.status(400).send({ message: "El contenido no puede estar vacío" });
  }

  try{
    // Ahora si, ingresamos los datos
    let mensaje = await capacitador.agregarMensajeCapacitador( req.body ).then( response => response )

    res.send({ message: 'Mensaje enviado, podrás visualizarlo en tu bandeja, saludos y buen día'} )
  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error al enviar el mensaje'})
  }

};

// Obtener los mensajes del capacitador
exports.getMensajesCapacitador = async (req, res) => {
  try{
    const { id } = req.params 
    
    // Aquí recuperamos todos los mensajes
    let mensajes = await capacitador.getMensajesCapacitador( id ).then( response => response )

    if( !mensajes.length ){ return res.send(mensajes) }

    // Sacamos quienes mendaron mensajes
    let usuarios = await capacitador.getCapacitadorMensaje( id ).then( response => response )

    // Tenemos que buscar esos usuarios y sus nombres
    const usuariosERP = await cursos.usuariosERP( ).then(response=> response)

    for( const i in usuarios ){
      const { idusuarioerp, idcurso, curso } = usuarios[i]
      const existeUsuario = usuariosERP.find( el => el.id_usuario == idusuarioerp )
      usuarios[i]['empleado']  = existeUsuario ? existeUsuario.nombre_completo : ''
      usuarios[i]['mensajes']  = mensajes.filter( el => { return el.idusuarioerp == idusuarioerp && el.idcurso == idcurso }) 
      usuarios[i]['sinver']    = mensajes.filter( el => { return el.emisor == idusuarioerp && el.visto == 0  && el.idcurso == idcurso }).length 
    }

    res.send(usuarios)
  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error al enviar el mensaje'})
  }

};


// Obtener los mensajes del capacitador
exports.getMensajesUsuario = async (req, res) => {
  try{
    const { id } = req.params 
    
    // Aquí recuperamos todos los mensajes
    let mensajes = await capacitador.getMensajesUsuario( id ).then( response => response )

    if( !mensajes.length ){ return res.send(mensajes) }

    // Sacamos quienes mendaron mensajes
    let usuarios = await capacitador.getUsuariosMensaje( id ).then( response => response )

    // Tenemos que buscar esos usuarios y sus nombres
    const usuariosERP = await cursos.usuariosERP( ).then(response=> response)

    for( const i in usuarios ){
      const { idcapacitador, idcurso, curso } = usuarios[i]
      const existeUsuario = usuariosERP.find( el => el.id_usuario == idcapacitador )
      usuarios[i]['empleado']  = existeUsuario ? existeUsuario.nombre_completo : ''
      usuarios[i]['mensajes']  = mensajes.filter( el => { return el.idcapacitador == idcapacitador && el.idcurso == idcurso }) 
      usuarios[i]['sinver']    = mensajes.filter( el => { return el.emisor == idcapacitador && el.visto == 0  && el.idcurso == idcurso }).length 
    }

    res.send(usuarios)
  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error al enviar el mensaje'})
  }

};


exports.updateMensajeCapacitador = async (req, res) => {
  try{
    const { mensajes, usuario } = req.body

    const mensajesCapacitador = mensajes.filter( el => { return el.visto == 0 && el.emisor != usuario })

    for( const i in mensajesCapacitador ){
      const { idmensajes_capacitacion } = mensajesCapacitador[i]

      // Ahora si, ingresamos los datos
      let mensaje = await capacitador.updateMensajeCapacitador( idmensajes_capacitacion ).then( response => response )

    }

    res.send({ message: 'Mensaje enviado, podrás visualizarlo en tu bandeja, saludos y buen día'} )
  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error al enviar el mensaje'})
  }
};