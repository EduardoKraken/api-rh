const calificaciones = require("../../models/capacitacion/calificaciones.model.js");
const respuestas     = require("../../models/capacitacion/respuesta_empleado.model.js");
const preguntasfre   = require("../../models/preguntasfre/preguntasfre.model.js");
const reportes       = require("../../models/kpi/reportes.model.js");
const usuarios       = require("../../models/usuarios.model.js");
const cursos         = require("../../models/capacitacion/cursos.model.js");


exports.getCalificaciones = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  calificaciones.getCalificaciones((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

exports.addCalificacion = async (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  try{

    // Modificar algo 
    const modificarString = await calificaciones.modificarString( ).then( response => response )

    const { idempleado, usuario_registro, idevaluacion, preguntas } = req.body 

    // Primero hay que ver cuantos intentos lleva
    const intentos = await calificaciones.getIntentosCalificacion( idempleado, idevaluacion ).then( response => response )
    // Sacamos el numero de intentos
    const numero_intento = intentos ? ( parseInt(intentos.cant_intentos) + 1 ) : 1

    for( const i in preguntas ){
      const { opcion_correcta, eleccion } = preguntas[i]
      // Validamos que haya puesto una respuesta 
      if( eleccion == '' )
        return res.status( 400 ).send({ message: 'Por favor, responde la pregunta: ' + ( parseInt(i) + 1 ) })

      preguntas[i].respuesta =  eleccion == opcion_correcta ? 1 : 0
    }

    // Sacamos el calculo de las respuestas
    const calificacion_total = preguntas.map(item => item.valor).reduce((prev, curr) => prev + curr, 0);
    const calificacion       = preguntas.filter( el => { return el.respuesta == 1 }).map(item => item.valor).reduce((prev, curr) => prev + curr, 0);

    // Agregamos la calificacion
    const addCalificacion = await calificaciones.addCalificacion( calificacion, calificacion_total, numero_intento, idevaluacion, idempleado, usuario_registro ).then( response => response )

    const idcalificacion = addCalificacion ? addCalificacion.id : 0

    // Agregamos las respuestas ahora si
    for( const i in preguntas ){
      // Agregamos la calificacion
      const addRespuesta = await respuestas.addRespuesta( preguntas[i], idempleado, usuario_registro ).then( response => response )
    }

    res.send({message: 'Evaluación contestada, saludos y buen día'})

  }catch( error ){
    return res.status( 500 ).send({ message: error ? error.message : 'Error al agregar la evaluacion' })
  }
};

exports.updateCalificacion = (req, res) => {

  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El contenido no puede estar vacío" });
  }

  calificaciones.updateCalificacion(req.params.idCalificacion, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `No se encontro la calificacion con id : ${req.params.idCalificacion}` });
      } else {
        res.status(500).send({ message: "Error al actualizar la calificacion con el id : " + req.params.idCalificacion });
      }
    } else {
      res.status(200).send({ message: `La calificacion se ha actualizado correctamente.` })
    }
  })
}

exports.getIntentosCalificacion = async (req, res) => {

  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  try{

    const { idempleado, idevaluacion } = req.body 

    // Primero hay que ver cuantos intentos lleva
    const intentos = await calificaciones.getIntentosUsuario( idempleado, idevaluacion ).then( response => response )
    if( intentos && intentos.calificacion ){ return res.send( intentos )}
    else{ return res.status( 400 ).send({ message: 'No existe evaluación' }) }

  }catch( error ){
    return res.status( 500 ).send({ message: error ? error.message : 'Error al agregar la evaluacion' })
  }
}

//obtner todas las calificaciones
exports.getCalificacionesEmpleados = async (req, res) => {

  try{

    // Consultar los usuarios activos
    const empleadosActivos  = await usuarios.getUsuarios( ).then(response=>response)


    // Habilitar el group by
    const habilitarGroup    = await reportes.habilitarGroup().then(response => response)
    const habilitarGroupERP = await calificaciones.habilitarGroupERP().then(response => response)
    
    // Obtener todos los cursos por empleado
    let cursosEmpleado = await calificaciones.getCursosEmpledado( ).then( response => response )

    // Consultar las cantidad de evaluaciones por curso
    let evaluacionesCurso = await calificaciones.getCanEvaluacionesCurso( ).then( response => response )

    // Consultar las cantidad de evaluaciones por curso
    let calificacionesEmpleado = await calificaciones.getCalificacionesEmpleados( ).then( response => response )

    // Calificaciones de las evaluaciones de los usuarios
    let calificacionesEvaluaciones = await calificaciones.getCalificacionesEvaluacionesEmpleados( ).then( response => response )

    const usuariosNuevoERP = await calificaciones.usuariosNuevoERP( ).then( response => response )

    // Recorrer los cursos por empleado
    for( const i in empleadosActivos ){

      // Desestructurar todo
      const { id_usuario } = empleadosActivos[i]

      // SACAR CUANTOS CURSOS TIENE ASIGNADOS
      const cursos = cursosEmpleado.filter( el => { return el.idempleado == id_usuario })

      // Sacar los id de los cursos
      let idCUrsos = cursos.map(( registro ) => { return registro.idcurso })

      idCUrsos = idCUrsos ? idCUrsos : [0]

      const calificacionCursosEmpleado   = calificacionesEmpleado.filter( el => { return idCUrsos.includes(el.idcurso)  && el.idempleado == id_usuario })

      let cursosCompletos = 0

      for(const j in calificacionCursosEmpleado ){
        const { total_calificaciones, idcurso } = calificacionCursosEmpleado[j]

        const evaluaciones   = evaluacionesCurso.find( el => el.idcurso == idcurso )

        const { total_evaluaciones } = evaluaciones

        if( total_calificaciones >= total_evaluaciones ){ cursosCompletos += 1; calificacionCursosEmpleado[j]['estatus'] = 1 }
        else
          calificacionCursosEmpleado[j]['estatus'] = 0
      }

      // Sacar el puesto del empleado 
      const existeUsuario = usuariosNuevoERP.find( el => el.iderp == id_usuario )
      
      for( const k in cursos ){
        const { idcurso } = cursos[k]

        // Hay buscar sus evaluaciones de cada curso
        const evaluacionesFinales = calificacionesEvaluaciones.filter( el => { return el.idcurso == idcurso && el.idempleado == id_usuario })

        const existeCalificacion   =  calificacionCursosEmpleado.find( el => el.idcurso == idcurso )

        cursos[k]['calificacion']  = existeCalificacion ? existeCalificacion.calificacion : 0
        cursos[k]['estatus']       = existeCalificacion ? existeCalificacion.estatus : 0
        cursos[k]['evaluaciones']  = evaluacionesFinales
      }

      // Agregar el nombre
      empleadosActivos[i]['curso_realizados']  = cursosCompletos
      empleadosActivos[i]['cant_cursos']       = cursos.length
      empleadosActivos[i]['cursos']            = cursos
      empleadosActivos[i]['avance']            = cursos.length ? (( cursosCompletos / cursos.length  ) * 100 ).toFixed(2) : 0
      empleadosActivos[i]['puesto']            = existeUsuario ? existeUsuario.puesto : 'SIN PUESTO'
      empleadosActivos[i]['idpuesto']          = existeUsuario ? existeUsuario.idpuesto : 'SIN PUESTO'

    }

    const empleadosFinales = empleadosActivos.filter( el => { return ![501,13,184].includes(el.id_usuario) })

    const puestos = await calificaciones.getPuestos( ).then( response => response )
    // res.send({ cursos, empleados, calificaciones: empleadosActivos, puestos } )
    res.send({ calificaciones: empleadosFinales, empleados: empleadosFinales, puestos } )

  }catch( error ){
    res.status( 500 ).send({message: error ? error.message : 'Error en el servidor' })
  }

  
};