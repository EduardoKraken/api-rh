const Planteles_usuario = require("../models/planteles_usuario.model.js");

exports.all_planteles_usuario = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Planteles_usuario.all_planteles_usuario((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los planteles_usuarios"
            });
        else res.send(data);
    });
};

exports.getPlantelesUsuarioId = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Planteles_usuario.getPlantelesUsuarioId(req.params.idusuario,(err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los planteles_usuarios"
            });
        else res.send(data);
    });
};


exports.add_planteles_usuario = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }

    Planteles_usuario.add_planteles_usuario(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear El plantel_usuario"
            })
        else res.status(200).send({ message: `El plantel_usuario se ha creado correctamente` });
    })
};

exports.update_planteles_usuario = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Planteles_usuario.update_planteles_usuario(req.params.idplanteles_usuario, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro el plantel_usuario con id : ${req.params.idplanteles_usuario}` });
            } else {
                res.status(500).send({ message: "Error al actualizar el plantel_usuario con el id : " + req.params.idplanteles_usuario });
            }
        } else {
            res.status(200).send({ message: `El plantel_usuario se ha actualizado correctamente.` })
        }
    })
}


exports.delete_planteles_usuario = (req, res) => {
    Planteles_usuario.delete_planteles_usuario(req.params.idplanteles_usuario, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found plantel usuario with id ${req.params.idplanteles_usuario}.`
                });
            } else {
                res.status(500).send({
                    message: "No encontre plantel usuario con el id " + req.params.idplanteles_usuario
                });
            }
        } else res.send({ message: `El plantel usuario se elimino correctamente!` });
    });
};