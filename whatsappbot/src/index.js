const wa = require("@open-wa/wa-automate");
const whatsappModel = require("./models/whatsapp.model");
const axios = require("axios");
const { open, writeFile } = require("fs").promises;
const config = require('./config');
//realtime
const path = require("path");

//Whatsapp
let conversacionesActivas = new Map();

let BASE_IMAGE_PATH = path.resolve(__dirname, "..", "..", "imageneswhatsapp");
let RELATIVE_PATH = path.relative(__dirname, BASE_IMAGE_PATH);
const utilidades = require('./utlis/index');

(async () => {
  const PORT = process.env.PORT || 3025;
  try {
    //Instancia de whatsapp web
    var clienteWhatsapp = await wa.create().then((client) => client).catch(err => console.log(err));


    clienteWhatsapp.onMessage(async (message) => {
      // guardar el from para usarlo en el proceso
      let from = message.from;
      //si es un grupo no hacer nada
      if (message.chat.isGroup) {
        return;
      }

      // if (from != "8121058094@c.us") {
      //   return true;
      // }

      //Buscar este contacto en base de datos
      let whatsappModelResult = await whatsappModel
        .findContact(from)
        .then((res) => res)
        .catch((err) => console.log(err));

      //Existe?
      if (whatsappModelResult.length == 0) {
        //NO

        //guardar usuario en la base de datos
        let id_whatsapp = await whatsappModel
          .addUserWhatsApp(from)
          .then((res) => res)
          .catch((err) => {
            console.log("Hubo un error al guardar el usuario");
          });

        //crear un chat
        let chat_id = await whatsappModel
          .addChatWhatsApp(id_whatsapp)
          .then((response) => response)
          .catch((err) => console.log(err, 'here 1'));

        //guardar su mensaje
        await utilidades.guardarMensajeCliente(message, chat_id, clienteWhatsapp);

        //saludar
        await utilidades.guardarMensajeEmpleado("Hola, gracias por contactar a INBI School, ¿En qué podemos ayudarte?", from, clienteWhatsapp, chat_id)

        //Guardar la conversación antes de guardar el prospecto
        conversacionesActivas.set(`${from}`, { from, id_whatsapp, chat_id });
      } else {
        //si
        //cliente/prospecto

        if (conversacionesActivas.has(from)) {
          let chatActual = conversacionesActivas.get(from);

          /////////////////////GUARDAR EL EL MENSAJE
          utilidades.guardarMensajeCliente(message, chatActual.chat_id, clienteWhatsapp)
          /////////////////////////////////

          //crear el prospecto con la api
          await axios
            .post(`${config.API_URL}prospectos.add`, {
              nombre: message.sender.pushname,
              telefono: from.substring(0, 13),
              email: "",
              estatus: "Primer contacto",
              id_empleado: null,
              source: "Whatsapp",
            })
            .then(async (response)=> {
              //
              await utilidades.agregarProspectoAlChat(response.data.data.id,chatActual.id_whatsapp)
              //
            })
            .catch(function (error) {
              
            });

            await utilidades.guardarMensajeEmpleado( "Permiteme un momento, te conecto con un asesor", from, clienteWhatsapp,chatActual.chat_id)


          conversacionesActivas.delete(from);

        } else {
          //Redirigir
          await utilidades.guardarMensajeCliente(message, whatsappModelResult[0].idchat,clienteWhatsapp)
                
        }
      }
    });
  } catch (e) { }


  //servidor para recibir mensajes de la api y enviarlos por whatsapp
  const express = require("express");
  const app = express();
  const bodyParser = require("body-parser");

  var cors = require("cors");
  app.use(cors());
  var server = require("http").Server(app);
  // parse requests of content-type: application/json
  app.use(bodyParser.json());

  app.post("/", async (req, res) => {
    if (req.body.file) {
      let filePath = path.join(
        path.relative(__dirname, BASE_IMAGE_PATH),
        `${req.body.fileName}.${req.body.fileExtension}`
      );

      let mimetype = ''

      switch (req.body.fileExtension) {
        case "jpeg":
          mimetype = "imagen";
          break;
        case "jpg":
          mimetype = "imagen";
          break;
        case "png":
          mimetype = "imagen";
          break;
        case "pdf":
          mimetype = "pdf";
        break;
      }

      if(mimetype == 'imagen'){
        await clienteWhatsapp.sendImage(
          req.body.from,
          filePath,
          "imagen",
          req.body.content
        );
      }else{
        await clienteWhatsapp.sendFile(
          req.body.from,
          filePath,
          "pdf",
          req.body.content
        );
      }

      

      res.status(200).send({ estatus: "ok" });
    } else {
      await clienteWhatsapp.sendText(req.body.from, req.body.content);
      res.status(200).send({ estatus: "ok" });
    }
  });

  server.listen(PORT);
})();
