const mime = require("mime-types");
const hash = require("hash.js");
const moment = require("moment");
const config = require('../config');
const path = require("path");
const whatsappModel = require('../models/whatsapp.model');
const { open, writeFile } = require("fs").promises;
const axios = require("axios");
const wa = require("@open-wa/wa-automate");


let BASE_IMAGE_PATH = path.resolve(__dirname, "..", "..", "imageneswhatsapp");
let RELATIVE_PATH = path.relative(__dirname, BASE_IMAGE_PATH);

const guardarMensajeCliente = async (message, chat_id,clienteWhatsapp) => {
    if (message.mimetype) {
        //*SI ENVIA UN VIDEO ***********************
        if (
            message.mimetype != "image/jpeg" &&
            message.mimetype != "image/png"
        ) {
            await clienteWhatsapp.sendText(
                message.from,
                "Lo sentimos no podemos recibir PDF o videos por este medio, si gustas escríbenos a este correo"
            );

            await whatsappModel.addMensajeWhatsApp(
                chat_id,
                1,
                `**********Usuario trato de enviar un archivo**********`
            );

            //*SI ENVIA UN VIDEO ***********************
            return;
        }

        const fileName = hash
            .sha256()
            .update(moment.now().toString())
            .digest("hex");
        const extension = mime.extension(message.mimetype);
        const fullFileName = `${fileName}.${extension}`;
        const fullPath = path.join(
            path.relative(__dirname, BASE_IMAGE_PATH),
            fullFileName
        );
        const mediaData = await wa.decryptMedia(message);
        try {
            await writeFile(fullPath, mediaData);
        } catch (error) {
            console.log(error, 'here 3');
        }

        await whatsappModel.addMensajeWhatsAppImage(
            chat_id,
            1,
            message.caption,
            fileName,
            extension
        );
        await whatsappModel.newMessageNotSeen(chat_id);

        await axios
        .post(`${config.API_URL}wpp.income.message`, {
          inM: 1,
          mensaje: message.caption,
          whatsapp_user: message.from,
          mimetype: message.mimetype,
          url_media: fullFileName,
        })
        .then(function (response) {
          console.log(response.data);
        })
        .catch(function (error) {
          
        });


    } else {
        whatsappModel
            .addMensajeWhatsApp(chat_id, 1, message.content)
            .then((response) => response)
            .catch((err) => console.error(err));

        await whatsappModel.newMessageNotSeen(chat_id);

        await axios
        .post(`${config.API_URL}wpp.income.message`, {
          inM: 1,
          mensaje: message.content,
          whatsapp_user: message.from,
        })
        .then(function (response) {
          console.log(response.data);
        })
        .catch(function (error) {
          
        });

    }
}

const guardarMensajeEmpleado = async (content,whatsapp_id,clienteWhatsapp,id_chat) => {
    await whatsappModel
    .addMensajeWhatsApp(id_chat, 0, content)
    .then((response) => response)
    .catch((err) => console.error(err));

    await clienteWhatsapp.sendText(
      whatsapp_id,
      content
    );

}

const agregarProspectoAlChat = async (id_prospecto, id_whatsapp) => {
    whatsappModel
    .addProspectToChat(
        id_prospecto,
        id_whatsapp
    )
    .then((response) => response)
    .catch((err) => console.error(err));
}

module.exports = {
    guardarMensajeCliente,
    agregarProspectoAlChat,
    guardarMensajeEmpleado
}