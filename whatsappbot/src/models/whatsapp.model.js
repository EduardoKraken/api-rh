const sql = require("../db/db.js")
const moment = require('moment');
//const constructor
const Whatsapp = function (whatsapp) { };

Whatsapp.addContacto = (contacto, result) => {
  sql.query(
    `INSERT INTO whatsapp(whatsapp_user)VALUES(?)`,
    [contacto],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }

      console.log("Crear contacto: ", { id: res.insertId });
      result(null, { id: res.insertId });
    }
  );
};

Whatsapp.getAllMessage = (id_chat, result) => {
  sql.query(
    `SELECT * FROM mensajes where id_chat = ?;`,
    [id_chat],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }

      console.log("Mensajes del chat: ", { id: id_chat });
      result(null, { mensajes: res });
    }
  );
};

Whatsapp.getContacto = (id, result) => {
  sql.query(
    `SELECT * FROM whatsapp where whatsapp_user=?`,
    [id],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      //   console.log("whatspp: ", res);
      result(null, res[0]);
    }
  );
};

Whatsapp.findContact = (from) => {
  return new Promise((resolve, reject) => {
    sql.query(
      "SELECT w.idwhatsapp, w.whatsapp_user, w.id_prospecto, c.idchat FROM whatsapp as w inner join chat as c on c.id_whatsapp = w.idwhatsapp WHERE whatsapp_user=?;",
      [from],
      (err, res) => {
        if (err) {
          reject(err);
        }

        resolve(res);
      }
    );
  });
};

Whatsapp.addUserWhatsApp = (from) => {
  return new Promise((resolve, reject) => {
    sql.query(
      "INSERT INTO whatsapp(whatsapp_user) VALUES(?);",
      [from],
      (err, results) => {
        try {
          if (err) {
            console.log(err.message);
            reject(err);
            return;
          }
          console.log(results);
          resolve(results.insertId);
        } catch (error) {
          reject(error);
        }
      }
    );
  });
};

Whatsapp.addChatWhatsApp = (id_whatsapp) => {
  return new Promise((resolve, reject) => {
    sql.query(
      "INSERT INTO chat(id_whatsapp) value(?);",
      [id_whatsapp],
      (err, res) => {
        if (err) reject(err);

        resolve(res.insertId);
      }
    );
  });
};

Whatsapp.addMensajeWhatsApp = (id_chat, inM, mensaje) => {
  return new Promise((resolve, reject) => {
    sql.query(
      "INSERT INTO mensajes(id_chat,inM,mensaje) VALUE(?,?,?);",
      [id_chat, inM, mensaje],
      (err, results) => {
        if (err) {
          console.log(err);
          reject(err);
          return;
        }
        console.log(err);
        console.log(results);
        resolve(results.insertId);
        return;
      }
    );
  });
};

Whatsapp.addMensajeWhatsAppImage = (
  id_chat,
  inM,
  mensaje,
  fileName,
  fileExt
) => {
  let mimetype = "";

  switch (fileExt) {
    case "jpeg":
      mimetype = "image/jpeg";
      break;
    case "jpg":
      mimetype = "image/jpeg";
      break;
    case "png":
      mimetype = "image/png";
      break;
  }

  return new Promise((resolve, reject) => {
    sql.query(
      "INSERT INTO mensajes(id_chat,inM,mensaje,mimetype,url_media) VALUE(?,?,?,?,?);",
      [id_chat, inM, mensaje, mimetype, `${fileName}.${fileExt}`],
      (err, res) => {
        if (err) {
          reject(err);
          return;
        }

        resolve(res.insertId);
        return;
      }
    );
  });
};

Whatsapp.addProspectToChat = (id_prospecto, id_whatsapp) => {
  return new Promise((resolve, reject) => {
    sql.query(
      "UPDATE whatsapp SET id_prospecto=? WHERE idwhatsapp=?;",
      [id_prospecto, id_whatsapp],
      (err, result) => {
        if (err) {
          reject(err);
        }

        resolve(result);
      }
    );
  });
};

Whatsapp.getAllChatsEmpleado = (id_empleado) => {
  return new Promise((resolve, reject) => {
    sql.query(
      `
  SELECT * FROM chat
      INNER JOIN
  whatsapp ON whatsapp.idwhatsapp = chat.id_whatsapp
      LEFT JOIN
  prospectos on prospectos.idprospectos = whatsapp.id_prospecto
  WHERE prospectos.id_empleado = ?
  ORDER BY chat.fecha_actualizo DESC;`,
      [id_empleado],
      (err, res) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(res);
        return;
      }
    );
  });
};

Whatsapp.getAllChats = () => {
  return new Promise((resolve, reject) => {
    sql.query(
      `
  SELECT * FROM chat
      INNER JOIN
  whatsapp ON whatsapp.idwhatsapp = chat.id_whatsapp
      LEFT JOIN
  prospectos on prospectos.idprospectos = whatsapp.id_prospecto
  ORDER BY chat.fecha_actualizo DESC;`,
      (err, res) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(res);
        return;
      }
    );
  });
};

Whatsapp.newMessageNotSeen = (id_chat) => {
  let updated_at = moment().format("yyyy-MM-DD HH:mm:ss")
  return new Promise((resolve, reject) => {
    sql.query("UPDATE chat SET messageNotSeen=1, fecha_actualizo=? WHERE idchat=?", [updated_at, id_chat],
      (err, res) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(res);
        return;
      }
    );
  })
}

Whatsapp.seenNewMessage = (id_chat) => {
  let updated_at = moment().format("yyyy-MM-DD HH:mm:ss")
  return new Promise((resolve, reject) => {
    sql.query("UPDATE chat SET messageNotSeen=0, fecha_actualizo=? WHERE idchat=?", [updated_at, id_chat],
      (err, res) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(res);
        return;
      }
    );
  })
}

module.exports = Whatsapp;
