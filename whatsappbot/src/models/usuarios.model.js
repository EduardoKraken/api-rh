const sql = require("../db/db.js");

//const constructor
const Usuarios = function (usuarios) {
  this.idusuario = usuarios.idusuario;
  this.iderp = usuarios.iderp;
  this.idpuesto = usuarios.idpuesto;
};


Usuarios.getPuestoUsuario = (id_usuario) => {
  return new Promise((resolve, reject) => {
    sql.query(
      `SELECT usuarios.idpuesto FROM usuarios WHERE deleted = 0 AND idusuario=?`,
      [id_usuario],
      (err, res) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(res);
      }
    );
  });
};



module.exports = Usuarios;
