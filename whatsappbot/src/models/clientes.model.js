const { result } = require("lodash");
const sql = require("../db/db.js");

//const constructor
const Clientes = function (clientes) {};

Clientes.getClientes = (result) => {
  sql.query(
    `SELECT clientes.*,plantel.plantel FROM clientes INNER JOIN plantel on plantel.idplantel = clientes.id_planteles WHERE clientes.deleted = 0`,
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }
      ///
      var clientes = [];

      for (const i in res) {
        //declaro el objeto que utilizare para llenar el arreglo
        var datos = {
          idClientes: res[i].idclientes,
          nombre: res[i].nombre,
          apellido_paterno: res[i].apellido_paterno,
          apellido_materno: res[i].apellido_materno,
          telefono: res[i].telefono,
          email: res[i].email,
          matricula: res[i].matricula,
          matricula_sistema_anterior: res[i].matricula_sistema_anterior,
          fecha_nacimiento: res[i].fecha_nacimiento,
          plantel: res[i].plantel,
          fecha_creacion: res[i].fecha_creacion,
          fecha_actualizo: res[i].fecha_actualizo,
        };

        clientes.push(datos);
      }

      console.log("Clientes: ", clientes);
      result(null, clientes);
      ///
      // console.log("cursos: ", res);
      // result(null, res);
    }
  );
};

Clientes.getCliente = (id, result) => {
  sql.query(
    `SELECT clientes.*, plantel.plantel FROM clientes INNER JOIN plantel on plantel.idplantel = clientes.id_planteles WHERE clientes.deleted = 0 AND idclientes=?`,
    [id],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }
      console.log("Tutor: ", res);
      result(null, res);
    }
  );
};

Clientes.addCliente = (c, result) => {
  sql.beginTransaction(function (err) {
    if (err) {
      result(err, null);
    }
    sql.query(
      "SELECT idclientes as lastId FROM panel_capacitacion.clientes order by idclientes DESC limit 1;",
      function (error, queryResults) {
        if (error) {
          return sql.rollback(function () {
            result(error, null);
          });
        }
        const currentYear = new Date().getFullYear();
        const CURRENTID = (queryResults[0] ? queryResults[0].lastId : 0) + 1;
        let matricula = `${currentYear}-${CURRENTID}`;
        sql.query(
          "INSERT INTO clientes(nombre,apellido_paterno,apellido_materno,telefono,email,matricula,fecha_nacimiento, id_planteles) VALUES(?,?,?,?,?,?,?,?);",
          [
            c.nombre,
            c.apellido_paterno,
            c.apellido_materno,
            c.telefono,
            c.email,
            matricula,
            c.fecha_nacimiento,
            c.plantel,
          ],
          function (error, queryResults) {
            if (error) {
              return sql.rollback(function () {
                result(error, null);
              });
            }

            sql.commit(function (err) {
              if (err) {
                return sql.rollback(function () {
                  result(error, null);
                });
              }
              console.log(queryResults);
              result(null, {
                nombre: c.nombre,
                apellido_paterno: c.apellido_paterno,
                apellido_materno: c.apellido_materno,
                telefono: c.telefono,
                email: c.email,
                matricula,
                fecha_nacimiento: c.fecha_nacimiento,
                id_planteles: c.plantel,
              });
              console.log("cliente creado");
            });
          }
        );
      }
    );
  });
};

Clientes.updateCliente = (id, c, result) => {
  sql.query(
    `UPDATE clientes SET nombre=?,apellido_paterno=?,apellido_materno=?,telefono=?,email=?,fecha_actualizo=now(),fecha_nacimiento=?,id_planteles=? WHERE idclientes=?`,
    [
      c.nombre,
      c.apellido_paterno,
      c.apellido_materno,
      c.telefono,
      c.email,
      c.fecha_nacimiento,
      c.plantel,
      id,
    ],
    (err, res) => {
      if (err) {
        console.log(err);
        result(err, null);
        return;
      }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        console.log(err);
        return;
      }

      console.log("Tutor actualizado: ", { id: id, ...c });
      result(null, { id: id, ...c });
    }
  );
};

Clientes.deleteCliente = (id, result) => {
  sql.query(
    `UPDATE clientes SET deleted=1 WHERE idclientes=?`,
    [id],
    (err, res) => {
      if (err) {
        console.log("error");
        result(err, null);
        return;
      }
      if (res.affectedRows == 0) {
        result({ message: "No se encontro el cliente" }, null);
        return;
      }

      console.log("cliente eliminado: ", { id: id });
      result(null, { id: id });
    }
  );
};

Clientes.getClienteByMatricula = (matricula) => {
  return new Promise((resolve, reject) => {
    sql.query(
      "SELECT * FROM clientes where matricula=?",
      [matricula],
      (err, res) => {
        if (err) {
          reject(err);
        }
        resolve(res);
      }
    );
  });
};

module.exports = Clientes;
