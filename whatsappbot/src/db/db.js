const mysql = require("mysql");
const dbConfig = require("../config.js");

// Create a connection to the database
const connection = mysql.createConnection({
  host: dbConfig.DB_HOST,
  user: dbConfig.DB_USER,
  password: dbConfig.DB_PASSWORD,
  database: dbConfig.DB_NAME,
  charset: "utf8mb4",
});

// open the MySQL connection
connection.connect((error) => {
  if (error) throw error;
  console.log(
    "|********* CONEXIÓN A" + " " + dbConfig.DB_NAME + " " + "CORRECTO **********|"
  );
});

module.exports = connection;
