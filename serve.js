// IMPORTAR DEPENDENCIAS
const express    = require("express");
const bodyParser = require("body-parser");
var   cors       = require("cors");
const fileUpload = require("express-fileupload");
const fs         = require("fs");
// IMPORTAR EXPRESS
const app        = express();
const config     = require('./config/index');

//Para servidor con ssl
const server     = require('http').createServer(app);

var io = require("socket.io")(server, { path: "/ws/socket.io" });
app.io = io;

// Rutas estaticas
app.use("/imageneswhatsapp",               express.static("../imageneswhatsapp"));

// Capacitacion
app.use("/capacitacion.videos",            express.static("./../../capacitacion-videos"));
app.use("/capacitacion.imagenes",          express.static("./../../capacitacion-imagenes"));
app.use("/capacitacion.archivos",          express.static("./../../capacitacion-archivos"));
app.use("/capacitacion.audios",            express.static("./../../capacitacion-audios"));


// Otras
app.use("/pdfs",                           express.static("./../pdf"));
app.use('/fotos',                          express.static('./../../fotos'));
app.use('/contratos',                      express.static('./../../contratos'));
app.use('/imagenes-clases',                express.static('./../../imagenes-clases'));
app.use('/imagenes-tienda',                express.static('./../../imagenes-tienda'));
// 14 niveles
app.use('/imagenes-certificados',          express.static('./../../imagenes-certificados'));
app.use('/imagenes-recepcion',             express.static('./../../imagenes-recepcion'));
// Cambridge
app.use('/imagenes-recepcion-cambridge',   express.static('./../../imagenes-recepcion-cambridge'));
app.use('/imagenes-cambridge',             express.static('./../../imagenes-cambridge'));

//copias
app.use('/copias-certificados',            express.static('./../../copias-certificados'));

// Campañas
app.use('/imagenes-campanias',             express.static('./../../imagenes-campanias'));

// COMPROBANTES DE PAGO
app.use('/comprobante-pago',               express.static('./../../comprobante-pago'));

// JUSTIFICANTES
app.use('/justificantes',                  express.static('./../../justificante'));

// FACTURAS
app.use('/facturas',                       express.static('./../../facturas'));

// PAGOS EGRESOS
app.use('/fotos-egresos',                  express.static('./../../comprobantes-egresos'));

// FORMULARIOS 
app.use('/archivos',                       express.static('./archivos'));

// FORMULARIOS 
app.use('/examenes_medicos',               express.static('./examenes_medicos'));

// EVIDENCIA TICKETS
app.use('/evidencia-tickets',              express.static('./../../evidencia-tickets'));

// Recibos de pago
app.use('/recibo-pago',                    express.static('./../../recibos-pagos'));

// proceso-reclutamient
app.use('/proceso_recluta',                express.static('./../../proceso-reclutamiento'));

app.use('/archivos_empleados',             express.static('./../../archivos_empleados'));

app.use('/galeria',                        express.static('./../../galeria'));



// IMPORTAR PERMISOS
app.use(cors({ origin: '*' }));
// parse requests of content-type: application/json
app.use(bodyParser.json({ limit: '100000mb'}));
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.raw({ type: 'audio/mpeg', limit: '60mb' }));

app.use(fileUpload()); //subir archivos

// ----IMPORTAR RUTAS---------------------------------------->
require("./routes/departamentos.routes")(app)
require("./routes/puestos.routes")(app)
require("./routes/usuarios.routes")(app)
require("./routes/permiso_usuario.routes")(app)
require("./routes/permisos.routes")(app)
require("./routes/niveles_usuario.routes")(app)
require("./routes/zonas.routes")(app)
require("./routes/planteles.routes")(app)
require("./routes/unidad_negocio.routes")(app)
require("./routes/planteles_usuario.routes")(app)
require("./routes/disponibilidad.routes")(app)
require("./routes/roles.routes")(app)

//catalogos
require("./routes/catalogos/index.routes")(app)

// Archivos
require("./routes/archivos.routes")(app)

// Whatsapp
require("./routes/chatHandle.routes")(app)
require("./routes/tienda/indexTienda.routes")(app);

// KPI
require("./routes/kpi/index.routes")(app)

// Tickets
require("./routes/tickets/index.routes")(app)

// Operaciones
require("./routes/operaciones/index.routes")(app);

// Academico
require('./routes/academico/grupo_teachers.routes')(app); 
require('./routes/academico/certificacion.routes')(app); 
require('./routes/academico/evaluacion_teacher.routes')(app); 
require('./routes/academico/comentarios_maestros_calificaciones.routes')(app);  //ANGEL RODRIGUEZ 
require('./routes/academico/reglamento_escuela.routes')(app);  //ANGEL RODRIGUEZ 
require('./routes/academico/incidencias.routes')(app);  //ANGEL RODRIGUEZ 
require('./routes/academico/revisionexamenes.routes')(app);  //ANGEL RODRIGUEZ 
require('./routes/academico/recursos.routes')(app);  //ANGEL RODRIGUEZ 
require('./routes/academico/reporte_modelo_ventas.routes')(app);  //ANGEL RODRIGUEZ 

// lms
require('./routes/lms/prospectoslms.routes')(app); 
require('./routes/lms/catalogoslms.routes')(app); 

// CALIDAD
require("./routes/calidad/index.routes")(app)

// CRM PROSPECTOS
require('./routes/prospectos/index.routes')(app)        //ANGEL RODRIGUEZ -- LINEA ADENTRO

// REGISTRO DE ASISTENCIAS D
require('./routes/registro_asistencias/index.routes')(app)

// EXCI
require('./routes/exci/exci.routes')(app)

// PREGUNTAS FRECUENTES
require('./routes/preguntasfre/preguntasfre.routes')(app)

// Capacitacion
require("./routes/capacitacion/index.routes")(app)

require("./routes/whatsapp/whatsapp.routes")(app)

// EGRESOS
require("./routes/egresos/index.routes")(app)

require('./routes/crm-rh/index.routes')(app);

// SISTEMA WEB
require('./routes/sistemaweb/index.routes')(app);

// whatsapp.generaChat()


/// SERVER LISTEN
server.listen(config.PORT, () => {
    console.log(`Servidor escuchando en el puerto ${config.PORT}`);
});

