require('dotenv').config();
module.exports = {
    ENV: process.env.ENV,
    // PORT: process.env.PORT,
    PORT: 3004,
    DB_ERP_HOST: process.env.DB_ERP_HOST,
    DB_ERP_USERNAME: process.env.DB_ERP_USERNAME,
    DB_ERP_PASSWORD: process.env.DB_ERP_PASSWORD,
    DB_ERP_DBNAME:process.env.DB_ERP_DBNAME
}