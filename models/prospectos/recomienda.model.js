const { result } = require("lodash");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");
const sqlERP      = require("../db3.js");


//const constructor
const recomienda = function(recomienda) {};

recomienda.getRecomienda = ( escuela ) => {
	const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve, reject) => {
    db.query(`SELECT r.*, CONCAT(u.nombre, ' ', u.apellido_paterno, ' ',IFNULL(u.apellido_materno,'')) AS alumno,
			u.usuario AS matricula FROM recomienda r
			LEFT JOIN usuarios u ON u.id = r.id_alumno;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res );
    });
  })
};

recomienda.getRecomiendaInscritos = ( escuela ) => {
	const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve, reject) => {
    db.query(`SELECT r.*, CONCAT(u.nombre, ' ', u.apellido_paterno, ' ',IFNULL(u.apellido_materno,'')) AS alumno,
			u.usuario AS matricula FROM recomienda r
			LEFT JOIN usuarios u ON u.id = r.id_alumno
			WHERE r.inscrito > 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res );
    });
  })
};

recomienda.gruposMasivosRecomendado = ( idgrupos ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM grupos WHERE id_grupo IN (?);`,[ idgrupos ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res );
    });
  })
};

recomienda.addRecomienda = ( id_alumno, nombre_recomendado, escuela, folio ) => {
	const db = escuela == 2 ? sqlFAST : sqlINBI
	return new Promise((resolve, reject) => {
	  db.query(`INSERT INTO recomienda( id_alumno, nombre_recomendado, escuela, folio )VALUES( ?, ?, ?, ? )`,[ id_alumno, nombre_recomendado, escuela, folio ],
	  (err, res) => {
	    if (err) {
        return reject({ message: err.sqlMessage })
      }
	    resolve({ id: res.insertId, id_alumno, nombre_recomendado });
	  })
	})
}

recomienda.updateRecomienda = ( u, escuela ) => {
	const db = escuela == 2 ? sqlFAST : sqlINBI
	return new Promise((resolve, reject) => {
  	db.query(`UPDATE recomienda SET id_alumno=?, id_grupo=?, inscrito=?, aplicado=?, id_grupo_recomendado=?, id_alumno_recomendado=?   WHERE id_recomienda = ? `,
  		[ u.id_alumno, u.id_grupo, u.inscrito, u.aplicado, u.id_grupo_recomendado, u.id_alumno_recomendado, u.id_recomienda ],
	  (err, res) => {
	    if (err) { return reject({ message: err.sqlMessage }) }
	    if (res.affectedRows == 0) {
	      return reject({ message: 'No hay datos para actualizar' })
	    }
	    resolve({ u });
	  })
  })
}

module.exports = recomienda;