const { result } = require("lodash");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");

//const constructor
const fuente = function(depas) {};

fuente.getFuenteList = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM fuentes WHERE deleted = 0`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


fuente.addFuente = (u, result) => {
  sqlERPNUEVO.query(`INSERT INTO fuentes(fuente,estatus)VALUES(?,?)`,[ u.fuente, u.estatus ],
  (err, res) => {
    if (err) { 
    	result(err, null); 
    	return; 
    }
    result(null, { id: res.insertId, ...u });
  })
}

fuente.updateFuente = (id, u, result) => {
  sqlERPNUEVO.query(`UPDATE fuentes SET fuente=?, estatus=?, deleted=? WHERE idfuentes = ? `, [u.fuente, u.estatus, u.deleted, id],
  (err, res) => {
    if (err) { result(err,null); return; }
    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, { id: id, ...u });
  })
}

fuente.getFuenteActivos = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM fuentes WHERE deleted = 0 AND estatus = 1`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


/**************** DETALESS **************/

fuente.getFuenteDetalleList = (result) => {
  sqlERPNUEVO.query(`SELECT df.*, f.fuente FROM detalle_fuentes df
    LEFT JOIN fuentes f ON f.idfuentes = df.idfuentes WHERE df.deleted = 0`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


fuente.addFuenteDetalle = (u, result) => {
  sqlERPNUEVO.query(`INSERT INTO detalle_fuentes(detalle_fuente, idfuentes,estatus)VALUES(?,?,?)`,[ u.detalle_fuente,u.idfuentes, u.estatus ],
  (err, res) => {
    if (err) { 
    	result(err, null); 
    	return; 
    }
    result(null, { id: res.insertId, ...u });
  })
}

fuente.updateFuenteDetalle = (id, u, result) => {
  sqlERPNUEVO.query(`UPDATE detalle_fuentes SET detalle_fuente=?, idfuentes = ? ,estatus=?, deleted=? WHERE iddetalle_fuentes = ? `, 
  [u.detalle_fuente,u.idfuentes, u.estatus, u.deleted, id],
  (err, res) => {
    if (err) { result(err,null); return; }
    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, { id: id, ...u });
  })
}

fuente.getFuenteDetalleActivos = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM detalle_fuentes WHERE deleted = 0 AND estatus = 1`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};

module.exports = fuente;

