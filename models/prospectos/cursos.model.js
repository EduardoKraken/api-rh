const { result } = require("lodash");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");

//const constructor
const cursos = function(depas) {};

cursos.getCursosList = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM cursos_escuela WHERE deleted = 0`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


cursos.getCursosActivos = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM cursos_escuela WHERE deleted = 0 AND estatus = 1`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


cursos.addCursos = (u, result) => {
  sqlERPNUEVO.query(`INSERT INTO cursos_escuela(curso,escuela,estatus)VALUES(?,?,?)`,[u.curso,u.escuela,u.estatus],
  (err, res) => {
    if (err) { 
    	result(err, null); 
    	return; 
    }
    result(null, { id: res.insertId, ...u });
  })
}

cursos.updateCursos = (id, u, result) => {
  sqlERPNUEVO.query(`UPDATE cursos_escuela SET curso=?,escuela=?,estatus=?,deleted=? WHERE idcursos_escuela=?`, [ u.curso, u.escuela, u.estatus, u.deleted, id],
  (err, res) => {
    if (err) { result(err,null); return; }
    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, { id: id, ...u });
  })
}

/*************************** M O D A L I D A D E S ***************************/

cursos.getModalidades = (result) => {
  sqlERPNUEVO.query(`SELECT m.*, c.curso FROM modalidad_cursos m 
	LEFT JOIN cursos_escuela c ON c.idcursos_escuela = m.idcursos_escuela
	WHERE m.deleted = 0;`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


cursos.getModalidadesActivas = (result) => {
  sqlERPNUEVO.query(`SELECT m.*, c.curso FROM modalidad_cursos m 
	LEFT JOIN cursos_escuela c ON c.idcursos_escuela = m.idcursos_escuela
	WHERE m.deleted = 0 AND m.estatus = 1;`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


cursos.addModalidades = (u, result) => {
  sqlERPNUEVO.query(`INSERT INTO modalidad_cursos(modalidad,idcursos_escuela,estatus,escuela)VALUES(?,?,?,?)`,[u.modalidad,u.idcursos_escuela,u.estatus,u.escuela],
  (err, res) => {
    if (err) { 
    	result(err, null); 
    	return; 
    }
    result(null, { id: res.insertId, ...u });
  })
}

cursos.updateModalidades = (id, u, result) => {
  sqlERPNUEVO.query(`UPDATE modalidad_cursos SET modalidad=?,idcursos_escuela=?,estatus=?,deleted=?,escuela=? WHERE idmodalidad_cursos=?`, [ u.modalidad, u.idcursos_escuela, u.estatus, u.deleted,u.escuela, id],
  (err, res) => {
    if (err) { result(err,null); return; }
    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, { id: id, ...u });
  })
}

/*************************** F R E C U E N C I A S ***************************/


cursos.getFrecuencias = (result) => {
  sqlERPNUEVO.query(`SELECT f.*, m.modalidad, c.curso FROM frecuencias f
    LEFT JOIN modalidad_cursos m ON m.idmodalidad_cursos = f.idmodalidad_cursos
    LEFT JOIN cursos_escuela c ON c.idcursos_escuela = m.idcursos_escuela
    WHERE f.deleted = 0;`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


cursos.getFrecuenciasActivas = (result) => {
  sqlERPNUEVO.query(`SELECT f.*, m.modalidad, c.curso FROM frecuencias f
    LEFT JOIN modalidad_cursos m ON m.idmodalidad_cursos = f.idmodalidad_cursos
    LEFT JOIN cursos_escuela c ON c.idcursos_escuela = m.idcursos_escuela
    WHERE f.deleted = 0 AND f.estatus = 1;`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


cursos.addFrecuencias = (u, result) => {
  sqlERPNUEVO.query(`INSERT INTO frecuencias(idcursos_escuela,idmodalidad_cursos,escuela,estatus,lunes,martes,miercoles,jueves,viernes,sabado,domingo,frecuencia)VALUES(?,?,?,?,?,?,?,?,?,?,?,?)`,
  [u.idcursos_escuela,u.idmodalidad_cursos,u.escuela,u.estatus,u.lunes,u.martes,u.miercoles,u.jueves,u.viernes,u.sabado,u.domingo,u.frecuencia],
  (err, res) => {
    if (err) { 
      result(err, null); 
      return; 
    }
    result(null, { id: res.insertId, ...u });
  })
}

cursos.updateFrecuencias = (id, u, result) => {
  sqlERPNUEVO.query(`UPDATE frecuencias SET idcursos_escuela=?,idmodalidad_cursos=?,deleted=?,escuela=?,estatus=?,lunes=?,martes=?,miercoles=?,jueves=?,viernes=?,sabado=?,domingo=?,frecuencia=? WHERE idfrecuencias=?`,
  [ u.idcursos_escuela, u.idmodalidad_cursos,u.deleted,u.escuela,u.estatus,u.lunes,u.martes,u.miercoles,u.jueves,u.viernes,u.sabado,u.domingo,u.frecuencia, id],(err, res) => {
    if (err) { result(err,null); return; }
    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, { id: id, ...u });
  })
}

module.exports = cursos;