const { result } = require("lodash");
// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");

//const constructor
const familiares = function(depas) {};


familiares.addFamiliar = (z) => {
	return new Promise((resolve, reject) => {
  	sqlERPNUEVO.query(`INSERT INTO familiares(familiar,idprospectos,edad,correo,telefono,idusuarioerp)VALUES(?,?,?,?,?,?)`, [z.familiar, z.idprospectos, z.edad, z.correo, z.telefono,z.idusuarioerp],
    (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    })
  });
}

familiares.getFamiliares  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM familiares WHERE deleted = 0 AND idprospectos = ?`,[ id ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

familiares.deleteFamiliar  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE familiares SET deleted = 1 WHERE idfamiliares = ${id};`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

familiares.updateFamiliarFinalizoRechazo = ( idfamiliares, notaRechazo ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE familiares SET finalizo = 1, fecha_finalizo = NOW(), nota_rechazo = ? WHERE idfamiliares = ?`,
      [ notaRechazo, idfamiliares ],(err, res) => {
      if (err) { reject(err); return; }
      if (res.affectedRows == 0) {
        reject(err);
        return;
      }
      resolve({ idfamiliares });
    });
  });
};


familiares.getRegistroFamilair = ( id_alumno ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM familiares WHERE idalumno = ?`,[id_alumno],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

familiares.updateFamiliarFinalizo = ( idfamiliares, id_alumno, idgrupo, codigo_clase ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE familiares SET finalizo = 1, fecha_finalizo = NOW(), idalumno = ?, idgrupo = ?, codigo_clase = ? WHERE idfamiliares = ?`,
      [ id_alumno, idgrupo, codigo_clase,idfamiliares ],(err, res) => {
      if (err) { reject(err); return; }
      if (res.affectedRows == 0) {
        reject(err);
        return;
      }
      resolve({ idfamiliares, id_alumno, idgrupo });
    });
  });
};

familiares.addEvento = ( evento, idprospectos, idfamiliares, idtipo_evento, usuario_registro ) => {
	return new Promise((resolve, reject) => {
	  sqlERPNUEVO.query(`INSERT INTO eventos(evento,idprospectos,idtipo_evento, usuario_registro, idfamiliares)VALUES(?,?,?,?,?)`,
   		[evento,idprospectos,idtipo_evento,usuario_registro,idfamiliares],(err, res) => {
	    if (err) {
	      reject(err);
	      return;
	    }
	     resolve({ id: res.insertId });
	  });
	});
};

module.exports = familiares;

