const { result } = require("lodash");
const sqlERP      = require("../db3.js");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");


//const constructor
const dashboardVentas = function(e) {};

dashboardVentas.conInformacion  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT pl.* FROM puntos_llamada pl
			LEFT JOIN prospectos p ON p.idprospectos = pl.idprospectos
			WHERE p.usuario_asignado = ? AND DATE(pl.fecha_creacion) = CURRENT_DATE() ;`, [ id ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

dashboardVentas.tareasHoy  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM tareas_prospectos WHERE idusuarioerp = ? AND tipo_tarea = 1 AND dia = CURRENT_DATE();`, [ id ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

dashboardVentas.inscritosHoy  = ( id, inicio, fin ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT idprospectos, IF(DATE(fecha_finalizo) = CURRENT_DATE(),1,0) AS hoy FROM prospectos WHERE usuario_asignado = ? AND finalizo = 1 AND idgrupo > 0
      AND DATE(fecha_creacion) BETWEEN   '${inicio}'  AND '${fin}';`, [ id ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

dashboardVentas.eventosHoy  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM eventos WHERE usuario_registro = ? AND DATE(fecha_creacion) = CURRENT_DATE() AND idtipo_evento IN(11,12,9,10,5);`, [ id ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

dashboardVentas.seguimientos  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT COUNT(*) AS cant_eventos FROM eventos WHERE usuario_registro = ? AND DATE(fecha_creacion) = CURRENT_DATE() AND idtipo_evento IN(11,12,9,10,5,3,7,2,6) GROUP BY idprospectos;`, [ id ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

dashboardVentas.fechasActuales = () => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DATE(fecha_inicio_ciclo) AS inicio, DATE(fecha_fin_ciclo) AS final FROM ciclos WHERE CURRENT_DATE() BETWEEN DATE(fecha_inicio_ciclo) AND DATE(fecha_fin_ciclo)
      AND ciclo NOT LIKE '%EXCI%'
      AND ciclo NOT LIKE '%INDUC%'
      AND ciclo NOT LIKE '%INVER%'
      AND ciclo NOT LIKE '%VERA%'
      AND activo_sn = 1
      LIMIT 1;`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};


module.exports = dashboardVentas;
