const { result } = require("lodash");
// const sqlERP = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");

const sqlFAST = require("../dbFAST.js");
const sqlERPNUEVO = require("../db.js");

//const constructor
const cambiarProspecto = function () { };

//Traer alumnos
cambiarProspecto.getProspectosAllCambiar = () => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT idprospectos,folio, telefono, nombre_completo, usuario_asignado, DATE(fecha_creacion) AS fecha_creacion 
      FROM prospectos
      ORDER BY fecha_creacion DESC;`,(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};

cambiarProspecto.getNombreVendedora = () => {
    return new Promise((resolve, reject) => {
      sqlERP.query(`SELECT id_usuario, nombre_completo FROM usuarios;`,(err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage })
        }
        resolve(res)
      })
    })
  };

//Traer vendedoras
cambiarProspecto.getVendedora = () => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT id_usuario, nombre_completo FROM usuarios
                  ORDER BY nombre_completo;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};


 cambiarProspecto.updateProspectoVendedora = (usuario_asignado, idprospectos) => {
   return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE prospectos SET usuario_asignado = ? WHERE idprospectos = ?; `, [usuario_asignado, idprospectos], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
       }
       resolve(res);
});
});
};


module.exports = cambiarProspecto;

//ANGEL RODRIGUEZ -- TODO