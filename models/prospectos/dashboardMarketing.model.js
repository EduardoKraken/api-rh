const { result } = require("lodash");
// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");

const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");


//const constructor
const dashboardVentas = function(e) {};

dashboardVentas.getLEADS  = ( fecha_inicio, fecha_final, escuela ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT l.*, p.folio, p.finalizo, p.idgrupo, p.nota_rechazo, pl.plantel,pl.idplantel, p.usuario_asignado, p.idalumno, p.idetapa, 
      p.telefono, IF(LOCATE('81',p.telefono)=1,0,1) AS isForaneo FROM leds l
			LEFT JOIN prospectos p ON p.idleds = l.idleds 
			LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
			WHERE DATE(l.fecha_creacion ) BETWEEN ? AND ?  
      AND l.escuela = ?;`, [ fecha_inicio, fecha_final, escuela ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage}) }
      resolve(res);
    });
  });
};

dashboardVentas.getFuentesLeads  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM fuentes WHERE deleted = 0;`,(err, res) => {
      if (err) { return reject({message: err.sqlMessage}) }
      resolve(res);
    });
  });
};

dashboardVentas.getDetalleFuentesLeads  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM detalle_fuentes WHERE deleted = 0;`,(err, res) => {
      if (err) { return reject({message: err.sqlMessage}) }
      resolve(res);
    });
  });
};

dashboardVentas.getGruposalumnosInscritos  = ( idGruposInscritos ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT ga.id_grupo, c.id_curso, c.curso FROM gruposalumnos ga
			LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
			LEFT JOIN cursos c ON c.id_curso = g.id_curso
			WHERE ga.id_grupo IN ( ? );`,[ idGruposInscritos ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage}) }
      resolve(res);
    });
  });
};

dashboardVentas.getCursosEscuela  = ( escuela ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM cursos_escuela WHERE deleted = 0 AND estatus = 1 AND escuela = ?;`, [ escuela ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage}) }
      resolve(res);
    });
  });
};


dashboardVentas.getContactos  = ( fecha_inicio, fecha_final, escuela ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT idprospectos, sucursal_interes, usuario_creo, usuario_asignado, folio, escuela, finalizo, idalumno, idgrupo, idleds, idpuesto, escuela, telefono
FROM prospectos WHERE deleted = 0 AND escuela = ? AND DATE(fecha_creacion) BETWEEN ? AND ?;`, [ escuela, fecha_inicio, fecha_final ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage}) }
      resolve(res);
    });
  });
};


dashboardVentas.getAlumnosInscritos  = ( idalumnos, telefonos ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT a.* FROM alumnos a
      LEFT JOIN alumnotutor t ON t.id_alumno = a.id_alumno
      WHERE a.id_alumno IN ( ? ) OR a.telefono IN ( ? ) OR a.celular IN ( ? ) OR t.telefono IN ( ? ) OR t.celular IN ( ? );`,
      [ idalumnos, telefonos, telefonos, telefonos, telefonos ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage}) }
      resolve(res);
    });
  });
};

dashboardVentas.getGruposAlumnos  = ( idalumnos ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT ga.id_alumno, g.id_grupo, g.grupo, IF(LOCATE('INDUCC',g.grupo),1,0) AS induccion FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE ga.id_alumno IN ( ? );`,[ idalumnos ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage}) }
      resolve(res);
    });
  });
};


dashboardVentas.graficaContactos  = ( fecha_inicio, fecha_final ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT COUNT(escuela) AS conteo, DATE(fecha_creacion) AS fecha_creacion, IF( escuela = 2, 'FAST', 'INBI') AS escuela FROM prospectos 
      WHERE DATE(fecha_creacion) BETWEEN ? AND ?  AND escuela > 0
      GROUP BY escuela, DATE(fecha_creacion) ;`,[ fecha_inicio, fecha_final ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage}) }
      resolve(res);
    });
  });
};

dashboardVentas.getFechasUnicas  = ( fecha_inicio, fecha_final ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT DATE(fecha_creacion) AS fecha_creacion FROM prospectos WHERE DATE(fecha_creacion) BETWEEN ? AND ?  AND escuela > 0
      GROUP BY DATE(fecha_creacion);`,[ fecha_inicio, fecha_final ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage}) }
      resolve(res);
    });
  });
};

dashboardVentas.graficaContactosVendedora  = ( fecha_inicio, fecha_final ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT COUNT(escuela) AS conteo, DATE(fecha_creacion) AS fecha_creacion, usuario_asignado FROM prospectos 
      WHERE DATE(fecha_creacion) BETWEEN ? AND ?  AND escuela > 0
      GROUP BY escuela, DATE(fecha_creacion), usuario_asignado;`,[ fecha_inicio, fecha_final ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage}) }
      resolve(res);
    });
  });
};

dashboardVentas.getVendedorasUnicas  = ( fecha_inicio, fecha_final ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT usuario_asignado FROM prospectos WHERE DATE(fecha_creacion) BETWEEN ? AND ?  AND escuela > 0
      GROUP BY usuario_asignado;`,[ fecha_inicio, fecha_final ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage}) }
      resolve(res);
    });
  });
};

dashboardVentas.getFechasUnicasInscritos  = ( fecha_inicio, fecha_final ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DISTINCT DATE(ga.fecha_alta) AS fecha_creacion
      FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = a.id_alumno AND ga.id_grupo = aa.id_grupo
      WHERE ga.id_alumno IN (SELECT id_alumno FROM alumnos WHERE DATE(fecha_alta) BETWEEN ? AND ?)
      ORDER BY ga.fecha_alta;`,[ fecha_inicio, fecha_final ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage}) }
      resolve(res);
    });
  });
};


dashboardVentas.getFechasInscritos  = ( fecha_inicio, fecha_final ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT ga.id_alumno, IF(LOCATE('FAST', g.grupo)>0,2,1) AS unidad_negocio,a.id_usuario_ultimo_cambio,
      DATE(a.fecha_alta) AS fecha_creacion, a.matricula, CONCAT( a.nombre,' ', a.apellido_paterno, IFNULL(a.apellido_materno,'')) AS alumno
      FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = a.id_alumno AND ga.id_grupo = aa.id_grupo
      WHERE ga.id_alumno IN (SELECT id_alumno FROM alumnos WHERE DATE(fecha_alta) BETWEEN ? AND ?)
      GROUP BY id_alumno
      ORDER BY ga.fecha_alta;`,[ fecha_inicio, fecha_final ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage}) }
      resolve(res);
    });
  });
};

dashboardVentas.getFechasInscritosVendedora  = ( fecha_inicio, fecha_final ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT COUNT(a.id_usuario_ultimo_cambio) AS conteo, a.id_usuario_ultimo_cambio, DATE(a.fecha_alta) AS fecha_creacion
      FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = a.id_alumno AND ga.id_grupo = aa.id_grupo
      WHERE ga.id_alumno IN (SELECT id_alumno FROM alumnos WHERE DATE(fecha_alta) BETWEEN ? AND ?)
      GROUP BY a.id_usuario_ultimo_cambio
      ORDER BY ga.fecha_alta;`,[ fecha_inicio, fecha_final ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage}) }
      resolve(res);
    });
  });
};


dashboardVentas.getVendedorasAll = (  ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT u.iderp, (SELECT idplantel FROM planteles_usuario WHERE idusuario = u.idusuario LIMIT 1) AS idplantel,
      (SELECT idunidad_negocio FROM plantel WHERE idplantel = (SELECT idplantel FROM planteles_usuario WHERE idusuario = u.idusuario LIMIT 1)) AS escuela FROM usuarios u
      LEFT JOIN puesto p ON p.idpuesto = u.idpuesto
      WHERE p.idpuesto = 18 AND (SELECT idplantel FROM planteles_usuario WHERE idusuario = u.idusuario LIMIT 1) IS NOT NULL AND u.deleted = 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};


module.exports = dashboardVentas;
