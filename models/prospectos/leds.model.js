const { result } = require("lodash");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");

//const constructor
const leds = function(depas) {};

leds.getLeds = (escuela, result) => {
  sqlERPNUEVO.query(`SELECT l.*, IFNULL(f.fuente, 'SIN INFORMACIÓN') AS fuente, IFNULL(df.detalle_fuente, 'SIN INFORMACIÓN') AS detalle_fuente,
    IFNULL(mc.medio, 'SIN INFORMACIÓN') AS medio, IFNULL(c.campania, 'SIN INFORMACIÓN') AS campania, pl.plantel AS sucursal_interes,
    IFNULL(p.folio,'SIN FOLIO') AS folio, IFNULL(p.telefono,'') AS telefono, DATE(l.fecha_creacion) AS fecha, cu.curso FROM leds l
    LEFT JOIN fuentes f ON f.idfuentes = l.idfuentes
    LEFT JOIN detalle_fuentes df ON df.iddetalle_fuentes = l.iddetalle_fuentes
    LEFT JOIN medio_contacto mc ON mc.idmedio_contacto = l.idmedio_contacto
    LEFT JOIN campanias c ON c.idcampanias = l.idcampanias
    LEFT JOIN prospectos p ON p.idleds = l.idleds
    LEFT JOIN cursos_escuela cu ON cu.idcursos_escuela = l.idcursos_escuela
    LEFT JOIN plantel pl ON pl.idplantel = l.id_sucursal_interes
    WHERE l.deleted = 0 AND l.escuela = ? ORDER BY fecha_creacion DESC;`, [ escuela ], (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


leds.addLeds = (u, result) => {
  sqlERPNUEVO.query(`INSERT INTO leds(nombre_completo,idfuentes,iddetalle_fuentes,escuela, foraneo, idcursos_escuela,idmedio_contacto,idcampanias,id_sucursal_interes)VALUES(?,?,?,?,?,?,?,?,?)`,
  [ u.nombre_completo,u.idfuentes,u.iddetalle_fuentes,u.escuela, u.foraneo, u.idcursos_escuela, u.idmedio_contacto, u.idcampanias, u.id_sucursal_interes ],
  (err, res) => {
    if (err) { 
    	result(err, null); 
    	return; 
    }
    result(null, { id: res.insertId, ...u });
  })
}

leds.updateLead = (u, result) => {
  sqlERPNUEVO.query(`UPDATE leds SET nombre_completo=?,idfuentes=?,iddetalle_fuentes=?,foraneo=?,idmedio_contacto=?,idcampanias=? WHERE idleds = ? `,
  [u.nombre_completo,u.idfuentes,u.iddetalle_fuentes,u.foraneo,u.idmedio_contacto,u.idcampanias, u.idleds],
  (err, res) => {
    if (err) { result(err,null); return; }
    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, { u });
  })
}

leds.updateFuente = (id, u, result) => {
  sqlERPNUEVO.query(`UPDATE fuentes SET fuente=?, estatus=?, deleted=? WHERE idfuentes = ? `, [u.fuente, u.estatus, u.deleted, id],
  (err, res) => {
    if (err) { result(err,null); return; }
    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, { id: id, ...u });
  })
}

leds.getFuenteActivos = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM fuentes WHERE deleted = 0 AND estatus = 1`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


leds.buscarTelefono = (data, result) => {
  sqlERPNUEVO.query(`SELECT folio FROM prospectos WHERE escuela = ? AND telefono = ?;`, [ data.escuela, data.telefono ], (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res[0]);
  });
};

leds.deleteLead = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE leds SET deleted = 1 WHERE idleds = ?`, [ id ],(err, res) => {
      if (err) { reject({ message: err.sqlMessage}); return; }
      if (res.affectedRows == 0) {
        reject({ message: err.sqlMessage});
        return;
      }
      resolve({ id });
    });
  });
};


module.exports = leds;

