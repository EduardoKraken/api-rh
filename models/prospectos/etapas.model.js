const { result } = require("lodash");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");

//const constructor
const etapas = function(depas) {};

etapas.getEtapasList = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM etapas WHERE deleted = 0 ORDER BY orden ASC`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


etapas.getEtapasActivas = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM etapas WHERE deleted = 0 AND estatus = 1 ORDER BY orden`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


etapas.addEtapas = (u, result) => {
  sqlERPNUEVO.query(`INSERT INTO etapas(etapa,color,estatus,orden)VALUES(?,?,?,?)`,[u.etapa,u.color,u.estatus,u.orden],
  (err, res) => {
    if (err) { 
    	result(err, null); 
    	return; 
    }
    result(null, { id: res.insertId, ...u });
  })
}

etapas.updateEtapas = (id, u, result) => {
  sqlERPNUEVO.query(`UPDATE etapas SET etapa=?,color=?,estatus=?,deleted=?,orden=? WHERE idetapas=?`, [u.etapa, u.color, u.estatus, u.deleted, u.orden, id],
  (err, res) => {
    if (err) { result(err,null); return; }
    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, { id: id, ...u });
  })
}


module.exports = etapas;

