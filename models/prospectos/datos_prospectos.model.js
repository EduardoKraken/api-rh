const { result } = require("lodash");
const sqlERP      = require("../db3.js");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");

//const constructor
const prospectos = function(depas) {};

prospectos.existeProspecto  = ( escuela, telefono ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM prospectos WHERE telefono = ? AND escuela = ?;`, [ telefono, escuela ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};


prospectos.habilitarGroupBy = () => {
   return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));`,(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

prospectos.addProspecto = ( u ) => {
	return new Promise((resolve, reject) => {
	  sqlERPNUEVO.query(`INSERT INTO prospectos(nombre_completo,telefono,correo,sucursal_interes,usuario_creo,usuario_asignado,nota_inicial,folio,escuela,idpuesto,como_llego)
	  	VALUES(?,?,?,?,?,?,?,?,?,?,?)`,
   		[u.nombre_completo,u.telefono,u.correo,u.sucursal_interes,u.usuario_creo,u.usuario_asignado,u.nota_inicial,u.folio,u.escuela,u.idpuesto,u.como_llego],(err, res) => {
	    if (err) {
	      reject(err);
	      return;
	    }
	   resolve({ id: res.insertId, ...u });
	  });
	});
};

prospectos.addEvento = ( evento, idprospectos, idtipo_evento, usuario_registro ) => {
	return new Promise((resolve, reject) => {
	  sqlERPNUEVO.query(`INSERT INTO eventos(evento,idprospectos,idtipo_evento, usuario_registro)VALUES(?,?,?,?)`,
   		[evento,idprospectos,idtipo_evento,usuario_registro],(err, res) => {
	    if (err) {
	      reject(err);
	      return;
	    }
	     resolve({ id: res.insertId });
	  });
	});
};

prospectos.updateProspectoFolio = (id, folio ) => {
	return new Promise((resolve, reject) => {
	  sqlERPNUEVO.query(`UPDATE prospectos SET folio=? WHERE idprospectos=?`, [ folio, id],(err, res) => {
	  	if (err) { reject(err); return; }
      if (res.affectedRows == 0) {
        reject(err);
        return;
      }
	    resolve({ id, folio });
	  });
	});
};

prospectos.updateProspecto = (id, u ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE prospectos SET telefono = ?, correo = ?, nombre_completo = ?, induccion = ?, fecha_induccion = ? WHERE idprospectos = ?`,
      [ u.telefono, u.correo, u.nombre_completo, u.induccion, u.fecha_induccion, id],(err, res) => {
      if (err) { reject(err); return; }
      if (res.affectedRows == 0) {
        reject(err);
        return;
      }
      resolve({ id, ...u });
    });
  });
};

prospectos.updateEtapaProspecto = (id, idetapa ) => {
	return new Promise((resolve, reject) => {
	  sqlERPNUEVO.query(`UPDATE prospectos SET idetapa = ? WHERE idprospectos = ?`, [ idetapa, id],(err, res) => {
	  	if (err) { reject(err); return; }
      if (res.affectedRows == 0) {
        reject(err);
        return;
      }
	    resolve({ id, idetapa });
	  });
	});
};

prospectos.getProspectos  = ( ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.idprospectos,p.nombre_completo, REPLACE(p.telefono,' ','') AS telefono, p.correo, p.sucursal_interes, p.usuario_creo, p.usuario_asignado,p.induccion,p.fecha_induccion,
      p.nota_inicial, p.folio, p.escuela, p.idpuesto, p.como_llego, p.reasignado, p.deleted, p.fecha_creacion, p.finalizo, p.fecha_finalizo, p.idalumno, 
      p.idgrupo, p.nota_rechazo, pl.plantel, Date_format(TIMEDIFF(NOW(), p.fecha_creacion),'%H, h, %i m') AS tiempo,
      DATE(p.fecha_creacion) AS fecha,
      IF(p.finalizo = 0, DATEDIFF(NOW(), p.fecha_creacion), DATEDIFF(NOW(), p.fecha_finalizo)) AS tiempo_transcurrido,
      (SELECT DATEDIFF(NOW(), fecha_creacion) FROM eventos WHERE idprospectos = p.idprospectos ORDER BY fecha_creacion DESC LIMIT 1) AS ultimo_movimiento,
      IF(DATEDIFF(NOW(), p.fecha_creacion) > 0, DATEDIFF(NOW(), p.fecha_creacion), '') AS dias,
      IF(TIMEDIFF(NOW(), p.fecha_creacion) > '00:10:00', 1, 0) AS retardo FROM prospectos p 
      LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
      WHERE p.deleted = 0 AND p.finalizo = 0
      ORDER BY DATEDIFF(NOW(), p.fecha_creacion) DESC, TIMEDIFF(NOW(), p.fecha_creacion) DESC;`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.getProspecto  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.idprospectos,p.nombre_completo, REPLACE(p.telefono,' ','') AS telefono, p.correo, p.sucursal_interes, p.usuario_creo, p.usuario_asignado,p.induccion,p.fecha_induccion,
      p.nota_inicial, p.folio, p.escuela, p.idpuesto, p.como_llego, p.reasignado, p.deleted, p.fecha_creacion, p.finalizo, p.fecha_finalizo, p.idalumno, 
      p.idgrupo, p.nota_rechazo, pl.plantel, Date_format(TIMEDIFF(NOW(), p.fecha_creacion),'%H, h, %i m') AS tiempo,
      DATE(p.fecha_creacion) AS fecha,
			IF(DATEDIFF(NOW(), p.fecha_creacion) > 0, DATEDIFF(NOW(), p.fecha_creacion), '') AS dias,
			IF(TIMEDIFF(NOW(), p.fecha_creacion) > '00:10:00', 1, 0) AS retardo FROM prospectos p 
			LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
			WHERE p.deleted = 0 AND idprospectos = ${ id }
			ORDER BY DATEDIFF(NOW(), p.fecha_creacion) DESC, TIMEDIFF(NOW(), p.fecha_creacion) DESC;`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.getCursosActivos  = ( ) => {
	return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM cursos WHERE activo_sn = 1 ORDER BY curso;`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.getCiclosActivos  = ( ) => {
	return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM ciclos WHERE activo_sn = 1`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.getComentarios  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM comentarios_prospectos WHERE deleted = 0 AND idprospectos = ?`,[ id ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.addComentario = (z, result) => {
  sqlERPNUEVO.query(`INSERT INTO comentarios_prospectos(idprospectos,idusuarioerp,comentario)VALUES(?,?,?)`, [z.idprospectos, z.idusuarioerp, z.comentario],
    (err, res) => {
      if (err) { result(err, null); return; }
      result(null, { id: res.insertId, ...z });
    }
  )
}

prospectos.addTareaProspecto = ( z ) => {
	return new Promise((resolve, reject) => {
	  sqlERPNUEVO.query(`INSERT INTO tareas_prospectos(idtareas,idmotivos,dia,hora,idprospectos,idusuarioerp,estatus,medio,tipo_tarea)VALUES(?,?,${z.dia},${z.hora},?,?,?,?,?)`,
	  [z.idtareas,z.idmotivos,z.idprospectos,z.idusuarioerp,z.estatus,z.medio, z.tipo_tarea],
    (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve({ id: res.insertId, ...z });
    })
  })
}

prospectos.actualizarTareaProspecto  = ( t, id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE tareas_prospectos SET idmotivos = ?, dia = ${t.dia}, hora = ${t.hora}, idprospectos = ?, idusuarioerp = ?, estatus = ?, medio = ?, tipo_tarea  = ? WHERE idtareas_prospectos = ${id};`,
      [ t.idmotivos, t.idprospectos, t.idusuarioerp, t.estatus, t.medio, t.tipo_tarea ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.getTareasProspectos  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT tp.*, Date_format(hora,'%h:%i %p') AS tiempo, Date_format(dia,'%d-%M-%Y') AS fecha, t.tipo_tarea, IF(CURDATE() > dia,1, IF(CURDATE() = dia, IF(CURTIME() < hora ,0,1),0)) AS cumplida, m.motivo FROM tareas_prospectos tp
      LEFT JOIN tareas t ON t.idtareas = tp.idtareas
      LEFT JOIN motivos m ON m.idmotivos = tp.idmotivos
			WHERE tp.deleted = 0 AND tp.idprospectos = ? ORDER BY dia, hora;`,[ id ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.getTareasUsuario  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT tp.*, Date_format(hora,'%h:%i %p') AS tiempo, p.folio, p.induccion, p.fecha_induccion,
      Date_format(dia,'%d-%M-%Y') AS fecha, t.tipo_tarea, 
      IF(CURDATE() = dia,1,0) AS hoy,
      IF(CURDATE() > dia,1, IF(CURDATE() = dia, IF(CURTIME() < hora ,0,1),0)) AS cumplida,
      p.nombre_completo, m.motivo FROM tareas_prospectos tp
      LEFT JOIN tareas t ON t.idtareas = tp.idtareas
      LEFT JOIN prospectos p ON p.idprospectos = tp.idprospectos
      LEFT JOIN motivos m ON m.idmotivos = tp.idmotivos
      WHERE tp.deleted = 0 AND tp.idusuarioerp = ? AND tp.tipo_tarea = 1 ORDER BY dia, hora;`,[ id ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.existeTarea  = ( id, dia, hora ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT *, CONCAT(dia, ' ', hora) AS fecha FROM tareas_prospectos WHERE deleted = 0
			AND CONCAT(dia, ' ', hora) = CONCAT(?, ' ', ?,':00') AND idusuarioerp =  ?;`,[ dia, hora, id ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.getTarea  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT tp.*, t.tarea FROM tareas_prospectos tp
      LEFT JOIN tareas t ON t.idtareas = tp.idtareas WHERE tp.idtareas_prospectos = ?;`,[ id ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

prospectos.eliminarTarea  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE tareas_prospectos SET deleted = 1 WHERE idtareas_prospectos = ?;`,[ id ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

prospectos.getEventosProspectos  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM eventos WHERE idprospectos = ? AND deleted = 0 ORDER BY fecha_creacion DESC;`,[ id ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.teareaRealizada  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE tareas_prospectos SET deleted = 1, estatus = 1 WHERE idtareas_prospectos = ?;`,[ id ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

// Llamadas
prospectos.existeLlamada  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM estatus_llamada WHERE idusuarioerp = ? AND estatus = 1;`,[ id ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.updateEstatusLlamada  = ( id, estatus ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE estatus_llamada SET estatus = ${estatus} WHERE idusuarioerp = ${id} AND estatus = 1;`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.addLlamada = ( idusuarioerp, estatus, idprospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`INSERT INTO estatus_llamada(idusuarioerp,estatus,idprospectos)VALUES(?,?,?)`, [idusuarioerp, estatus, idprospectos],
    (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve({ id: res.insertId });
    })
  })
}

// Puntos de la llamada
prospectos.existePuntoLlamada  = ( idprospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.*, a.anuncio, m.modalidad, c.curso, f.frecuencia  FROM puntos_llamada p
      LEFT JOIN anuncios a ON a.idanuncios = p.idanuncios
      LEFT JOIN modalidad_cursos m ON m.idmodalidad_cursos = p.idmodalidad
      LEFT JOIN cursos_escuela c   ON c.idcursos_escuela = p.idcurso
      LEFT JOIN frecuencias f      ON f.idfrecuencias    = p.idfrecuencias WHERE p.idprospectos = ?;`,[ idprospectos ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.getEstatusllamadaList  = ( idprospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT e.*, p.folio, p.nombre_completo, TIME(e.fecha_creacion) AS inicio, TIME(e.fecha_actualizo) AS finalizo, e.idusuarioerp,
      SEC_TO_TIME((TIMESTAMPDIFF(SECOND, e.fecha_creacion, e.fecha_actualizo ))*60) AS duracion FROM estatus_llamada e
      LEFT JOIN prospectos p ON p.idprospectos = e.idprospectos WHERE e.idprospectos = ?;`,[ idprospectos ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.updatePuntosLlamada  = ( id, d ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE puntos_llamada SET pregunta1 = ?, pregunta2 = ?, pregunta3 = ?, pregunta4 = ?, idplantel = ?, edad = ?,
      idanuncios = ?, idcurso = ?, idciclo = ?, idmodalidad =? , idfrecuencias = ?, notas = ?  WHERE idpuntos_llamada = ${ id };`,
      [ d.pregunta1,d.pregunta2,d.pregunta3,d.pregunta4,d.idplantel,d.edad,d.idanuncios,d.idcurso,d.idciclo, d.idmodalidad, d.idfrecuencias, d.notas ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.addPuntosLlamada = ( d ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`INSERT INTO puntos_llamada(idprospectos,pregunta1,pregunta2,pregunta3,pregunta4,idplantel,edad,idanuncios,idcurso,idciclo,idmodalidad,idfrecuencias,notas)
    VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)`, 
    [ d.idprospectos,d.pregunta1,d.pregunta2,d.pregunta3,d.pregunta4,d.idplantel,d.edad,d.idanuncios,d.idcurso,d.idciclo,d.idmodalidad,d.idfrecuencias,d.notas ],
    (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve({ id: res.insertId, ...d });
    })
  })
};

// consultar cursos activos
prospectos.getCursosActivosId  = ( idcurso ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM cursos WHERE id_curso = ?;`,[ idcurso ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

// Consultar ciclos activos
prospectos.getCiclosActivosId  = ( idciclo ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM ciclos WHERE id_ciclo = ? `,[ idciclo ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

prospectos.getNotasLlamada  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM notas_llamada WHERE idprospectos = ? `,[ id ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.addNotaLlamada = ( d ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`INSERT INTO notas_llamada(idprospectos,idusuarioerp,nota,idestatus_llamada) VALUES(?,?,?,?)`, [ d.idprospectos,d.idusuarioerp,d.nota,d.idestatus_llamada ],
    (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve({ id: res.insertId, ...d });
    })
  })
};

/* PLANTELES POR VENDEDORA */

prospectos.plantelesVendedora  = ( escuela ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.idplantel, p.plantel, p.acronimo, pu.idusuario, u.iderp, p.escuela FROM planteles_usuario pu
      LEFT JOIN plantel p ON p.idplantel = pu.idplantel
      LEFT JOIN usuarios u ON u.idusuario = pu.idusuario 
      WHERE pu.deleted = 0 AND u.idpuesto = 18 AND p.escuela = ${ escuela }
      GROUP BY idusuario;`,(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.reasignarProspecto = ( u ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE prospectos SET usuario_asignado = ?, reasignado = ? WHERE idprospectos = ?`,
      [ u.usuario_asignado ,u.reasignado, u.idprospectos ],(err, res) => {
      if (err) { reject(err); return; }
      if (res.affectedRows == 0) {
        reject(err);
        return;
      }
      resolve({ ...u });
    });
  });
};

/* ERP VIEJO */

prospectos.getAlumnoMatricula = ( matricula ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM alumnos WHERE matricula = ?`,[matricula],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

prospectos.getRegistroProspecto = ( id_alumno ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM prospectos WHERE idalumno = ?`,[id_alumno],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

prospectos.updateProspectoFinalizo = ( idprospectos, id_alumno, idgrupo ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE prospectos SET finalizo = 1, fecha_finalizo = NOW(), idalumno = ?, idgrupo = ? WHERE idprospectos = ?`,
      [ id_alumno, idgrupo, idprospectos ],(err, res) => {
      if (err) { reject(err); return; }
      if (res.affectedRows == 0) {
        reject(err);
        return;
      }
      resolve({ idprospectos, id_alumno, idgrupo });
    });
  });
};

prospectos.getGrupos = ( escuela ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM grupos WHERE id_ciclo IN (
      SELECT id_ciclo FROM ciclos WHERE ciclo NOT LIKE '%INV%' AND ciclo NOT LIKE '%CAMBI%');`,(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.getGrupoAlumno = ( id_alumno, idgrupo ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM gruposalumnos WHERE id_alumno = ? AND id_grupo = ?;`,[id_alumno, idgrupo],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

prospectos.updateProspectoFinalizoRechazo = ( idprospectos, notaRechazo ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE prospectos SET finalizo = 1, fecha_finalizo = NOW(), nota_rechazo = ? WHERE idprospectos = ?`,
      [ notaRechazo, idprospectos ],(err, res) => {
      if (err) { reject(err); return; }
      if (res.affectedRows == 0) {
        reject(err);
        return;
      }
      resolve({ idprospectos });
    });
  });
};

/* REMARKETING */
prospectos.getRemarketing = ( escuela ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM prospectos WHERE deleted = 0 AND finalizo = 1 AND idalumno = 0 AND idgrupo = 0 AND escuela = ${ escuela };`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.addImagenCampania = ( imagen, extension ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`INSERT INTO imagen_campania(imagen,extension) VALUES(?,?)`, [ imagen, extension ],
    (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve({ id: res.insertId });
    })
  })
};

/* INSCRITOS */
prospectos.getProspectosInscritos = ( escuela, fechaini, fechafin ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT *, DATE(fecha_finalizo) AS fecha_filtro FROM prospectos WHERE deleted = 0 AND finalizo = 1 AND idalumno > 0 AND idgrupo > 0 AND escuela = ${ escuela }
      AND DATE(fecha_finalizo) BETWEEN ? AND ?;`,[ fechaini, fechafin ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.getEventosPorVendedora = ( fechaini, fechafin ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT COUNT(idprospectos) AS total, idprospectos,usuario_registro FROM eventos 
      WHERE idtipo_evento IN (3, 5, 7)
      AND DATE(fecha_creacion) BETWEEN ? AND ?
      GROUP BY usuario_registro, idprospectos;`,[ fechaini, fechafin ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.getGruposProspectos = ( grupos ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT id_ciclo, id_grupo FROM grupos WHERE activo_sn = 1 AND id_grupo IN ( ? );`,[ grupos ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};


prospectos.getVendedoras = ( escuela ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT u.iderp, (SELECT idplantel FROM planteles_usuario WHERE idusuario = u.idusuario LIMIT 1) AS idplantel,
      (SELECT idunidad_negocio FROM plantel WHERE idplantel = (SELECT idplantel FROM planteles_usuario WHERE idusuario = u.idusuario LIMIT 1)) AS escuela FROM usuarios u
      LEFT JOIN puesto p ON p.idpuesto = u.idpuesto
      WHERE p.idpuesto = 18 AND (SELECT idplantel FROM planteles_usuario WHERE idusuario = u.idusuario LIMIT 1) IS NOT NULL
      AND (SELECT idunidad_negocio FROM plantel WHERE idplantel = (SELECT idplantel FROM planteles_usuario WHERE idusuario = u.idusuario LIMIT 1)) = ${ escuela };`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

// Información que se le da a los prospectos
prospectos.getInfoEscuelaProspecto  = ( idprospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT  * FROM info_escuela_prospecto  WHERE idprospectos = ?;`,[ idprospectos ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

prospectos.updateInfoEscuelaProspecto  = ( idprospectos, iderp, informacion ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE info_escuela_prospecto SET informacion = ? WHERE idinfo_escuela_prospecto > 0 AND idprospectos = ${ idprospectos };`,
      [ informacion ],(err, res) => {
      if (err) { reject(err); return; }
      if (res.affectedRows == 0) {
        reject(err);
        return;
      }
      resolve({ idprospectos });
    });
  });
};

prospectos.addInfoEscuelaProspecto = ( idprospectos, iderp, informacion ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`INSERT INTO info_escuela_prospecto( idprospectos, iderp, informacion ) VALUES( ?, ?, ? )`, 
    [ idprospectos, iderp, informacion ],
    (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve({ id: res.insertId, idprospectos, iderp, informacion });
    })
  })
};

prospectos.getNuevosProspectos  = ( idusuarioerp ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT COUNT(p.idprospectos) AS nuevos FROM prospectos p
      WHERE (SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos ) IS NULL
      AND (SELECT COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) IS NULL
      AND p.usuario_asignado = ?
      AND DATE(p.fecha_creacion) = CURRENT_DATE()
      AND p.finalizo = 0;`,[ idusuarioerp ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

// Atrasados ( 1 )
prospectos.getProspectosVendedoraAtrasados  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.*, pl.plantel, DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido,pl.plantel,
      IFNULL((SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos),0) AS tareasProgramadas,
      (SELECT  COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) AS puntos_llamada,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) GROUP BY idprospectos),0) AS cantEventos,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) AND DATE(fecha_creacion) = CURRENT_DATE() GROUP BY idprospectos),0) AS cantEventosHoy,
      DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, 1 AS idetapa
      FROM prospectos p 
      LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
      WHERE p.deleted = 0 
      AND p.finalizo = 0
      AND (SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos ) IS NULL
      AND (SELECT COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) IS NULL
      AND CURRENT_DATE() > DATE(p.fecha_creacion) 
      AND induccion = 0;`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

// Nuevosss ( 2 )
prospectos.getProspectosVendedora  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.*, pl.plantel, DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido,pl.plantel,DATE(p.fecha_creacion) AS fecha,
      IFNULL((SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos),0) AS tareasProgramadas,
      (SELECT  COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) AS puntos_llamada,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) GROUP BY idprospectos),0) AS cantEventos,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) AND DATE(fecha_creacion) = CURRENT_DATE() GROUP BY idprospectos),0) AS cantEventosHoy,
      DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, 2 AS idetapa
      FROM prospectos p 
      LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
      WHERE p.deleted = 0 
      AND p.finalizo = 0
      AND (SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos ) IS NULL
      AND (SELECT COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) IS NULL
      AND DATE(p.fecha_creacion) = CURRENT_DATE()
      AND induccion = 0;`,(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

// Induccion ( 3 )
prospectos.getProspectosVendedoraInduccion  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.*, pl.plantel, DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido,pl.plantel,DATE(p.fecha_creacion) AS fecha,
      IFNULL((SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos),0) AS tareasProgramadas,
      (SELECT  COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) AS puntos_llamada,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) GROUP BY idprospectos),0) AS cantEventos,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) AND DATE(fecha_creacion) = CURRENT_DATE() GROUP BY idprospectos),0) AS cantEventosHoy,
      DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, 3 AS idetapa
      FROM prospectos p 
      LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
      WHERE p.deleted = 0 
      AND p.finalizo = 0
      AND induccion = 1;`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

// Con infooo ( 4 ) 
prospectos.getProspectosVendedoraConInfo  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.*, pl.plantel, DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido,pl.plantel,
      IFNULL((SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos),0) AS tareasProgramadas,
      (SELECT  COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) AS puntos_llamada,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) GROUP BY idprospectos),0) AS cantEventos,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) AND DATE(fecha_creacion) = CURRENT_DATE() GROUP BY idprospectos),0) AS cantEventosHoy,
      DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, 4 AS idetapa
      FROM prospectos p 
      LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
      WHERE p.deleted = 0 
      AND p.finalizo = 0
      AND (SELECT COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) IS NOT NULL  
      AND induccion = 0;`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

// Sin infooo ( 5 )
prospectos.getProspectosVendedoraSinInfo  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.*, pl.plantel, DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, pl.plantel,DATE(p.fecha_creacion) AS fecha,
      IFNULL((SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos),0) AS tareasProgramadas,
      (SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) GROUP BY idprospectos ) AS cantEventos,
      DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, 5 AS idetapa
      FROM prospectos p 
      LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
      WHERE p.deleted = 0 
      AND p.finalizo = 0
      AND (SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos ) IS NOT NULL
      AND (SELECT COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) IS NULL  
      AND induccion = 0;`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.getProspectosOrdenados  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.*, pl.plantel, DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido,
      IF(p.induccion = 1,IF(CURDATE() > DATE(fecha_induccion), 1,0),0) AS dia_induccion, 
      IF(p.induccion = 1, 3,
          IF((SELECT COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) IS NOT NULL,4,
          IF((SELECT  COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) IS NULL,
            IF((SELECT  COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos) IS NOT NULL, 5, 0),
                  0))) AS idetapa,
      IFNULL((SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos),0) AS tareasProgramadas,
      IFNULL((SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos AND tipo_tarea = 1 AND deleted = 0  AND dia = CURDATE() AND hora >= current_time() GROUP BY idprospectos),0) AS cita,
      (SELECT  COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) AS puntos_llamada,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) GROUP BY idprospectos),0) AS cantEventos,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) AND DATE(fecha_creacion) = CURRENT_DATE() GROUP BY idprospectos),0) AS cantEventosHoy,
      DATE(p.fecha_creacion) AS fecha
      FROM prospectos p
      LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes 
      WHERE p.deleted = 0 AND p.finalizo = 0 
      AND IF(p.induccion = 1, 3,
          IF((SELECT COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) IS NOT NULL,5,
          IF((SELECT  COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) IS NULL,
            IF((SELECT  COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos) IS NOT NULL, 4, 0),
                  0))) IN (3,4,5)
      ORDER BY cita DESC, cantEventos , tiempo_transcurrido , idetapa DESC;`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

// FINALIZADOS ( 7 )
prospectos.getProspectosFinalizados  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.*, pl.plantel, DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido,pl.plantel,
      0 AS tareasProgramadas,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) GROUP BY idprospectos),0) AS cantEventos,
      0 AS cantEventosHoy,
      DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, 7 AS idetapa
      FROM prospectos p 
      LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
      WHERE p.deleted = 0 
      AND p.idgrupo = 0
      AND p.finalizo = 1;`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.getProspectosInscritos  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.*, pl.plantel, DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido,pl.plantel,
      0 AS tareasProgramadas,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) GROUP BY idprospectos),0) AS cantEventos,
      0 AS cantEventosHoy,
      DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, 8 AS idetapa
      FROM prospectos p 
      LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
      WHERE p.deleted = 0 
      AND p.idgrupo > 0
      AND p.finalizo = 1;`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.getTareasProspectosAll  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT tp.*, Date_format(hora,'%h:%i %p') AS tiempo, Date_format(dia,'%d-%M-%Y') AS fecha, t.tipo_tarea, IF(CURDATE() > dia,1, IF(CURDATE() = dia, IF(CURTIME() < hora ,0,1),0)) AS cumplida, m.motivo FROM tareas_prospectos tp
      LEFT JOIN tareas t ON t.idtareas = tp.idtareas
      LEFT JOIN motivos m ON m.idmotivos = tp.idmotivos
      WHERE tp.deleted = 0 ORDER BY dia, hora;`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.getEventosRecluta  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM eventos_recluta WHERE idformularios = ? AND deleted = 0 ORDER BY fecha_creacion DESC;`,[ id ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};


module.exports = prospectos;

