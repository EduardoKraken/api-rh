const { result } = require("lodash");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");

//const constructor
const mediocontacto = function(depas) {};

mediocontacto.getMedioContactoList = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM medio_contacto WHERE deleted = 0`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


mediocontacto.addMedioContacto = (u, result) => {
  sqlERPNUEVO.query(`INSERT INTO medio_contacto(medio,estatus)VALUES(?,?)`,[ u.medio, u.estatus ],
  (err, res) => {
    if (err) { 
    	result(err, null); 
    	return; 
    }
    result(null, { id: res.insertId, ...u });
  })
}

mediocontacto.updateMedioContacto = (id, u, result) => {
  sqlERPNUEVO.query(`UPDATE medio_contacto SET medio=?, estatus=?, deleted=? WHERE idmedio_contacto = ? `, [u.medio, u.estatus, u.deleted, id],
  (err, res) => {
    if (err) { result(err,null); return; }
    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, { id: id, ...u });
  })
}


mediocontacto.getMedioActivas = (result) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM medio_contacto WHERE deleted = 0 AND estatus = 1;`, (err, res) => {
      if (err) {
        return reject({message: err.sqlMessage});
      }
       resolve(res);
    });
  })
};


module.exports = mediocontacto;

