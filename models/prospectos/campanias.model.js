const { result } = require("lodash");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");

//const constructor
const campanias = function(depas) {};

campanias.getCampaniasList = (result) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT c.*, mc.medio FROM campanias c
      LEFT JOIN medio_contacto mc ON mc.idmedio_contacto = c.idmedio_contacto 
      WHERE c.deleted = 0
      ORDER BY mc.medio;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res );
    });
  })
};

campanias.getCampaniasActivos = (result) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT c.*, mc.medio FROM campanias c
      LEFT JOIN medio_contacto mc ON mc.idmedio_contacto = c.idmedio_contacto 
      WHERE c.deleted = 0 AND c.estatus = 1
      ORDER BY mc.medio;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res );
    });
  });
};

campanias.addCampanias = (u, result) => {
  sqlERPNUEVO.query(`INSERT INTO campanias(campania,estatus,descrip,idmedio_contacto)VALUES(?,?,?,?)`,
  [ u.campania, u.estatus, u.descrip, u.idmedio_contacto ],
  (err, res) => {
    if (err) { 
    	result(err, null); 
    	return; 
    }
    result(null, { id: res.insertId, ...u });
  })
}

campanias.updateCampanias = (id, u, result) => {
  sqlERPNUEVO.query(`UPDATE campanias SET campania=?, estatus=?, deleted=?, descrip = ?, idmedio_contacto = ?  WHERE idcampanias = ? `,
  [u.campania, u.estatus, u.deleted, u.descrip, u.idmedio_contacto, id],
  (err, res) => {
    if (err) { result(err,null); return; }
    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, { id: id, ...u });
  })
}

module.exports = campanias;

