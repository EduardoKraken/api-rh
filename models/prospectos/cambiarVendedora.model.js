const { result }  = require("lodash");
// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");
const sqlFAST     = require("../dbFAST.js");
const sqlERPNUEVO = require("../db.js");

//const constructor
const cambiarVendedora = function () { };

//Traer alumnos
cambiarVendedora.getAlumnos = () => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT a.id_alumno, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', a.apellido_materno) nombre_alumno, u.nombre_completo AS nombre_vendedora, a.matricula
                  FROM alumnos a
                  LEFT JOIN usuarios u ON a.id_usuario_ultimo_cambio = u.id_usuario
                  ORDER BY nombre_alumno; `,(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};

//Traer vendedoras
cambiarVendedora.getVendedora = () => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT id_usuario, nombre_completo FROM usuarios
                  ORDER BY nombre_completo;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};


 cambiarVendedora.updateAlumnoVendedora = (id_usuario_ultimo_cambio, id_alumno) => {
   return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE alumnos SET id_usuario_ultimo_cambio = ? WHERE id_alumno = ?; `, [id_usuario_ultimo_cambio, id_alumno], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
       }
       resolve(res);
});
});
};


module.exports = cambiarVendedora;

//ANGEL RODRIGUEZ -- TODO