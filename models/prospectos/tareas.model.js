const { result } = require("lodash");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");

//const constructor
const tareas = function(depas) {};

tareas.getTareasList = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM tareas WHERE deleted = 0`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};

tareas.getTareasActivos = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM tareas WHERE deleted = 0 AND estatus = 1;`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};

tareas.getTareasActivosFormulario = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM tareas WHERE deleted = 0 AND estatus = 1;`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};

tareas.addTareas = (u, result) => {
  sqlERPNUEVO.query(`INSERT INTO tareas(tarea,estatus,tipo_tarea)VALUES(?,?,?)`,[ u.tarea, u.estatus,u.tipo_tarea ],
  (err, res) => {
    if (err) { 
    	result(err, null); 
    	return; 
    }
    result(null, { id: res.insertId, ...u });
  })
}

tareas.updateTareas = (id, u, result) => {
  sqlERPNUEVO.query(`UPDATE tareas SET tarea=?, estatus=?, deleted=?, tipo_tarea=? WHERE idtareas = ? `, [u.tarea, u.estatus, u.deleted, u.tipo_tarea, id],
  (err, res) => {
    if (err) { result(err,null); return; }
    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, { id: id, ...u });
  })
}

module.exports = tareas;

