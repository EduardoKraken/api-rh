const { result } = require("lodash");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");

//const constructor
const anuncios = function(depas) {};

anuncios.getAnunciosList = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM anuncios WHERE deleted = 0`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};

anuncios.getAnunciosActivos = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM anuncios WHERE deleted = 0 AND estatus = 1;`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};

anuncios.addAnuncios = (u, result) => {
  sqlERPNUEVO.query(`INSERT INTO anuncios(anuncio,estatus)VALUES(?,?)`,[ u.anuncio, u.estatus ],
  (err, res) => {
    if (err) { 
    	result(err, null); 
    	return; 
    }
    result(null, { id: res.insertId, ...u });
  })
}

anuncios.updateAnuncios = (id, u, result) => {
  sqlERPNUEVO.query(`UPDATE anuncios SET anuncio=?, estatus=?, deleted=? WHERE idanuncios = ? `, [u.anuncio, u.estatus, u.deleted, id],
  (err, res) => {
    if (err) { result(err,null); return; }
    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, { id: id, ...u });
  })
}

module.exports = anuncios;

