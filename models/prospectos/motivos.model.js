const { result } = require("lodash");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");

//const constructor
const motivo = function(depas) {};

motivo.getMotivoList = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM motivos WHERE deleted = 0`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


motivo.addMotivo = (u, result) => {
  sqlERPNUEVO.query(`INSERT INTO motivos(motivo,estatus)VALUES(?,?)`,[ u.motivo, u.estatus ],
  (err, res) => {
    if (err) { 
    	result(err, null); 
    	return; 
    }
    result(null, { id: res.insertId, ...u });
  })
}

motivo.updateMotivo = (id, u, result) => {
  sqlERPNUEVO.query(`UPDATE motivos SET motivo=?, estatus=?, deleted=? WHERE idmotivos = ? `, [u.motivo, u.estatus, u.deleted, id],
  (err, res) => {
    if (err) { result(err,null); return; }
    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, { id: id, ...u });
  })
}

motivo.getMotivoActivos = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM motivos WHERE deleted = 0 AND estatus = 1`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


motivo.getMotivoActivosFormulario = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM motivos WHERE deleted = 1 AND estatus = 1`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};

module.exports = motivo;

