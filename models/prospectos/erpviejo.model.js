const { result } = require("lodash");
// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");

const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");

//const constructor
const erpviejo = function(depas) {};

erpviejo.getCursosActivos = (result) => {
  sqlERP.query(`SELECT * FROM cursos WHERE activo_sn = 1;`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};

erpviejo.getCiclosActivos = (result) => {
  sqlERP.query(`SELECT id_ciclo, ciclo, IF(LOCATE ('FE', ciclo) > 0, 2,1) AS unidad_negocio FROM ciclos WHERE activo_sn = 1 AND ciclo NOT LIKE '%CAMBIOS%' AND ciclo NOT LIKE '%INDU%'
		AND DATEDIFF(fecha_inicio_ciclo,CURRENT_DATE) > -5;`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};

erpviejo.getVendedoras  = (result) => {
  sqlERP.query(`SELECT id_ciclo, ciclo, IF(LOCATE ('FE', ciclo) > 0, 2,1) AS unidad_negocio FROM ciclos WHERE activo_sn = 1 AND ciclo NOT LIKE '%CAMBIOS%' AND ciclo NOT LIKE '%INDU%'
		AND DATEDIFF(fecha_inicio_ciclo,CURRENT_DATE) > -5;`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};

// erpviejo.getUsuariosERP  = ( ) => {
// 	return new Promise((resolve, reject) => {
//     sqlERP.query(`SELECT * FROM usuarios WHERE activo_sn = 1;`, (err, res) => {
//       if (err) {
//         return reject({message: err.sqlMessage })
//       }
//       resolve(res);
//     });
//   });
// };

erpviejo.getUsuariosERP  = ( ) => {
	return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT u.*, p.plantel, CASE WHEN p.plantel LIKE '%FAST%' THEN 2 ELSE 1 END as escuela
    FROM usuarios u
    LEFT JOIN planteles p ON p.id_plantel = u.id_plantel
    WHERE u.activo_sn = 1;`, (err, res) => {
      if (err) {
        return reject({message: err.sqlMessage })
      }
      resolve(res);
    });
  });
};

erpviejo.getCiclosActivoParaProspectos = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT id_ciclo, ciclo, IF(LOCATE ('FE', ciclo) > 0, 2,1) AS unidad_negocio FROM ciclos WHERE activo_sn = 1 AND ciclo NOT LIKE '%CAMBIOS%' AND ciclo NOT LIKE '%INDU%' 
      AND ciclo NOT LIKE '%exci%' AND ciclo NOT LIKE '%campa%'
      AND DATEDIFF(fecha_fin_ciclo,CURRENT_DATE) > 7 AND DATEDIFF(fecha_fin_ciclo,CURRENT_DATE) < 50 LIMIT 2;`, (err, res) => {
      if (err) {
        return reject({message: err.sqlMessage })
      }
      console.log( res )
      resolve(res);
    });
  });
};


module.exports = erpviejo;
