const { result } = require("lodash");
// const sqlERP = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");
const sqlFAST     = require("../dbFAST.js");
const sqlERPNUEVO = require("../db.js");

//const constructor
const organigrama = function () { };

//Traer datos
organigrama.getOrganigrama = () => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT o.idorganigrama AS id, o.pid, o.iderp, o2.iderp AS idjefe, o.idorganigrama
    FROM organigrama o 
    LEFT JOIN organigrama o2 ON o2.idorganigrama = o.pid
    WHERE o.deleted = 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};

//Traer Cantidad de puestos
organigrama.getPuestos = () => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT *
    FROM puesto
    WHERE deleted = 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};


//Traer Cantidad de personas
organigrama.getPersonas = () => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT *
    FROM organigrama o
    LEFT JOIN usuarios u ON o.iderp = u.id_usuario
    WHERE o.iderp < 10000 and o.deleted = 0 and u.apellido_usuario NOT LIKE '%VACANTE%' and pid != 59; `, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};


//Traer Cantidad de Vacantes
organigrama.getVacantes = () => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT v.*, p.puesto, pl.plantel, TIMESTAMPDIFF(DAY, DATE(v.fecha_creacion), CURRENT_DATE) AS dias_transcurrido FROM vacantes v
      LEFT JOIN puesto p ON p.idpuesto = v.idpuesto
      LEFT JOIN plantel pl ON pl.idplantel = v.sucursal
      WHERE v.deleted = 0
      ORDER BY v.urgencia;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};


//Traer Cantidad Puestos Requerida
organigrama.getRequerida = () => {
  return new Promise((resolve, reject) => {
    sqlERP.query(` SELECT *
    FROM organigrama o
    LEFT JOIN usuarios u ON o.iderp = u.id_usuario
    WHERE o.iderp < 10000 and o.deleted = 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};



organigrama.getReclutadoras = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM usuarios WHERE idpuesto IN (SELECT idpuesto FROM puesto WHERE puesto LIKE '%Reclutador%' AND deleted = 0) AND deleted = 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};



//Traer Plantel
organigrama.getPlantel = (id_usuario) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT p.id_plantel, u.id_usuario, p.plantel, IF(LOCATE('FAST',p.plantel)>0,2,1) AS escuela
      FROM planteles p
      LEFT JOIN usuarios u ON p.id_plantel = u.id_plantel
      where u.id_usuario IN (?);`, [id_usuario], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};

//Añadir Nodos
organigrama.addOrganigrama = (pid, iderp) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`INSERT INTO organigrama(pid,iderp,deleted) values(?,?,0);`, [pid, iderp], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};

//Eliminar Nodo
organigrama.deleteOrganigrama = (id) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE organigrama SET deleted = 1 WHERE  idorganigrama = ? `, [id], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res);
    });
  });
};


organigrama.updateOrganigrama = (iderp, idorganigrama) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE organigrama SET iderp = ? WHERE  idorganigrama = ? `, [iderp, idorganigrama], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res);
    });
  });
};

organigrama.updateTablaVacantes = ( u ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE vacantes SET horario = ?, sucursal = ?, idpuesto = ?, fecha_entrega = ?, reclutadora = ?, urgencia = ?, deleted = ?, proceso = ?, capacitacion = ?
      WHERE  idvacantes = ? `, [ u.horario, u.sucursal, u.idpuesto, u.fecha_entrega, u.reclutadora, u.urgencia, u.deleted, u.proceso, u.capacitacion, u.idvacantes ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res);
    });
  });
};

organigrama.addVacante = ( u ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`INSERT INTO vacantes(horario, sucursal, idpuesto, fecha_entrega, reclutadora, urgencia )values(?,?,?,?,?,?);`,
      [u.horario, u.sucursal, u.idpuesto, u.fecha_entrega, u.reclutadora, u.urgencia], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};

organigrama.getUsuariosOrganigrama = (result) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT id_usuario, nombre_completo FROM usuarios WHERE activo_sn = 1 ORDER BY nombre_completo ASC;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res);
    })
  })
};

module.exports = organigrama;

//ANGEL RODRIGUEZ -- TODO