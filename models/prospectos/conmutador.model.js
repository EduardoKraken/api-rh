const { result } = require("lodash");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");
// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");


//const constructor
const conmutador = function(depas) {};

conmutador.getConmutador = (result) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT c.*, u.nombre_completo, p.plantel FROM crm_cuentas_sip c
      LEFT JOIN usuarios u ON u.id_usuario = c.id_usuario
      LEFT JOIN planteles p ON p.id_plantel = u.id_plantel
      WHERE c.eliminado = 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res );
    });
  })
};


conmutador.addConmutador = (u, result) => {
  sqlERP.query(`INSERT INTO crm_cuentas_sip(id_usuario,usuario_sip,ext)VALUES(?,?,?)`,[ u.id_usuario, u.usuario_sip, u.ext ],
  (err, res) => {
    if (err) { 
    	result(err, null); 
    	return; 
    }
    result(null, { id: res.insertId, ...u });
  })
}

conmutador.updateConmutador = (u, result) => {
  sqlERP.query(`UPDATE crm_cuentas_sip SET id_usuario=?, eliminado=?, usuario_sip=?, ext = ? WHERE id_sip = ? `, [u.id_usuario, u.eliminado, u.usuario_sip, u.ext, u.id_sip],
  (err, res) => {
    if (err) { result(err,null); return; }
    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, u);
  })
}

/*
  REGISTROS DEL CONMUTADOR
*/

conmutador.existeRegistro = ( uniqueid, start_time, answer_time, end_time ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM conmutador_excel WHERE uniqueid = ? AND start_time = ? AND answer_time = ? AND end_time = ?;`,[ uniqueid, start_time, answer_time, end_time ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res[0] );
    });
  })
};

conmutador.addRegistroConmutador = ( u ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`INSERT INTO conmutador_excel(account_code,caller_number,callee_number,context,calerid,start_time,answer_time,end_time,call_time,talk_time,disposition,userfield,dest_channel_extension,caller_name,answer_by,id_usuario,uniqueid,ext)
      VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
      [ u.account_code,u.caller_number,u.callee_number,u.context,u.calerid,u.start_time,u.answer_time,u.end_time,u.call_time,u.talk_time,u.disposition,u.userfield,u.dest_channel_extension,u.caller_name,u.answer_by,u.id_usuario,u.uniqueid,u.ext ],
      (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve({ id: res.insertId, ...u });
    })
  })
}

/*
  REPORTE DE LLAMADAS
*/

conmutador.getConmutadorFechas = ( fecha_inicio, fecha_fin ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT c.*,IF(LENGTH(callee_number) < 6,1,0) AS internas, u.nombre_completo FROM conmutador_excel c
      LEFT JOIN usuarios u ON u.id_usuario = c.id_usuario
      WHERE DATE(start_time) BETWEEN ? AND ? AND userfield != 'Internal';`,[ fecha_inicio, fecha_fin ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res );
    });
  })
};

conmutador.getLlamadasTelefonos = ( telefonos ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT IF(LOCATE(c.ext,c.callee_number)=1,SUBSTRING(c.callee_number,3, 20),c.callee_number) AS telefono, c.start_time, c.talk_time, c.disposition,
        CONCAT(c.start_time, ' duración: ',TIME_FORMAT(SEC_TO_TIME(c.talk_time), "%H:%i:%s")) AS mensaje, id_usuario FROM conmutador_excel c
        WHERE c.id_usuario > 0 AND c.disposition = 'ANSWERED'
        AND IF(LOCATE(c.ext,c.callee_number)=1,SUBSTRING(c.callee_number,3, 20),c.callee_number) IN (?)
        ORDER BY start_time ASC;`,[ telefonos ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res );
    });
  })
};

conmutador.getLlamadasTelefonosAll = ( telefonos ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT IF(LOCATE(c.ext,c.callee_number)=1,SUBSTRING(c.callee_number,3, 20),c.callee_number) AS telefono, c.start_time, c.talk_time, c.disposition,
        CONCAT(c.start_time, ' duración: ',TIME_FORMAT(SEC_TO_TIME(c.talk_time), "%H:%i:%s")) AS mensaje, id_usuario FROM conmutador_excel c
        WHERE c.id_usuario > 0
        AND IF(LOCATE(c.ext,c.callee_number)=1,SUBSTRING(c.callee_number,3, 20),c.callee_number) IN (?)
        ORDER BY start_time ASC;`,[ telefonos ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res );
    });
  })
};

module.exports = conmutador;