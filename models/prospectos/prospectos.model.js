const { result } = require("lodash");
// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");

//const constructor
const prospectos = function(depas) {};

prospectos.existeProspecto  = ( escuela, telefono ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM prospectos WHERE telefono = ? AND escuela = ?;`, [ telefono, escuela ],(err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve(res[0]);
    });
  });
};

prospectos.existeProspectoERP  = ( escuela, telefono ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT a.id_alumno, CONCAT(a.nombre, ' ',a.apellido_paterno, ' ',IFNULL(a.apellido_materno,'')) AS nombre, a.matricula, a.telefono, a.celular, g.grupo,p.plantel,
      (SELECT MAX(ga.id_grupo) FROM gruposalumnos ga WHERE ga.id_alumno = a.id_alumno) AS id_grupo
      FROM alumnos a
      LEFT JOIN grupos g ON g.id_grupo = (SELECT MAX(ga.id_grupo) FROM gruposalumnos ga WHERE ga.id_alumno = a.id_alumno)
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE a.telefono = ? 
      AND ? = (SELECT IF(LOCATE('FAST', g.grupo)=1,2,1) FROM grupos g WHERE g.id_grupo = (SELECT MAX(ga.id_grupo) FROM gruposalumnos ga WHERE ga.id_alumno = a.id_alumno))
      OR a.celular = ?
      AND ? = (SELECT IF(LOCATE('FAST', g.grupo)=1,2,1) FROM grupos g WHERE g.id_grupo = (SELECT MAX(ga.id_grupo) FROM gruposalumnos ga WHERE ga.id_alumno = a.id_alumno));`,
      [ telefono, escuela, telefono , escuela ],(err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve(res[0]);
    });
  });
};

prospectos.habilitarGroupBy = () => {
   return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));`,(err, res) => {
      if(err){
        return reject({message:err.sqlMessage})
      }
      resolve(res)
    });
  });
};

prospectos.addProspecto = ( u ) => {
	return new Promise((resolve, reject) => {
	  sqlERPNUEVO.query(`INSERT INTO prospectos(nombre_completo,telefono,correo,sucursal_interes,usuario_creo,usuario_asignado,nota_inicial,folio,escuela,idpuesto,como_llego,idleds)
	  	VALUES(?,?,?,?,?,?,?,?,?,?,?,?)`,
   		[u.nombre_completo,u.telefono,u.correo,u.sucursal_interes,u.usuario_creo,u.usuario_asignado,u.nota_inicial,u.folio,u.escuela,u.idpuesto,u.como_llego,u.idleds],(err, res) => {
	    if (err) {
	      return reject({message: err.sqlMessage});
	      return;
	    }
	   resolve({ id: res.insertId, ...u });
	  });
	});
};

prospectos.addEvento = ( evento, idprospectos, idtipo_evento, usuario_registro ) => {
	return new Promise((resolve, reject) => {
	  sqlERPNUEVO.query(`INSERT INTO eventos(evento,idprospectos,idtipo_evento, usuario_registro)VALUES(?,?,?,?)`,
   		[evento,idprospectos,idtipo_evento,usuario_registro],(err, res) => {
	    if (err) {
	      return reject({message: err.sqlMessage});
	      return;
	    }
	     resolve({ id: res.insertId });
	  });
	});
};

prospectos.updateProspectoFolio = (id, folio ) => {
	return new Promise((resolve, reject) => {
	  sqlERPNUEVO.query(`UPDATE prospectos SET folio=? WHERE idprospectos=?`, [ folio, id],(err, res) => {
	  	if (err) { return reject({message:err.sqlMessage}) }
      if (res.affectedRows == 0) {
        return reject({message:err.sqlMessage})
      }
	    resolve({ id, folio });
	  });
	});
};

prospectos.updateProspecto = (id, u ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE prospectos SET telefono = ?, correo = ?, nombre_completo = ?, induccion = ?, fecha_induccion = ? WHERE idprospectos = ?`,
      [ u.telefono, u.correo, u.nombre_completo, u.induccion, u.fecha_induccion, id],(err, res) => {
      if (err) { return reject({message:err.sqlMessage}) }
      if (res.affectedRows == 0) {
        return reject({message:err.sqlMessage})
      }
      resolve({ id, ...u });
    });
  });
};

prospectos.updateEtapaProspecto = (id, idetapa ) => {
	return new Promise((resolve, reject) => {
	  sqlERPNUEVO.query(`UPDATE prospectos SET idetapa = ? WHERE idprospectos = ?`, [ idetapa, id],(err, res) => {
	  	if (err) { return reject({message:err.sqlMessage}) }
      if (res.affectedRows == 0) {
        return reject({message:err.sqlMessage})
      }
	    resolve({ id, idetapa });
	  });
	});
};

prospectos.getProspectos  = ( ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.idprospectos,p.nombre_completo, REPLACE(p.telefono,' ','') AS telefono, p.correo, p.sucursal_interes, p.usuario_creo, p.usuario_asignado,p.induccion,p.fecha_induccion,
      p.nota_inicial, p.folio, p.escuela, p.idpuesto, p.como_llego, p.reasignado, p.deleted, p.fecha_creacion, p.finalizo, p.fecha_finalizo, p.idalumno, 
      p.idgrupo, p.nota_rechazo, pl.plantel, Date_format(TIMEDIFF(NOW(), p.fecha_creacion),'%H, h, %i m') AS tiempo,
      DATE(p.fecha_creacion) AS fecha, p.ultimoevento AS intentoLlamada,
      IF(p.finalizo = 0, DATEDIFF(NOW(), p.fecha_creacion), DATEDIFF(NOW(), p.fecha_finalizo)) AS tiempo_transcurrido,
      (SELECT DATEDIFF(NOW(), fecha_creacion) FROM eventos WHERE idprospectos = p.idprospectos ORDER BY fecha_creacion DESC LIMIT 1) AS ultimo_movimiento,
      IF(DATEDIFF(NOW(), p.fecha_creacion) > 0, DATEDIFF(NOW(), p.fecha_creacion), '') AS dias,
      IF(TIMEDIFF(NOW(), p.fecha_creacion) > '00:10:00', 1, 0) AS retardo FROM prospectos p 
      LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
      WHERE p.deleted = 0 AND p.finalizo = 0
      ORDER BY DATEDIFF(NOW(), p.fecha_creacion) DESC, TIMEDIFF(NOW(), p.fecha_creacion) DESC;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getProspecto  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.idprospectos,p.nombre_completo, REPLACE(p.telefono,' ','') AS telefono, p.correo, p.sucursal_interes, p.usuario_creo, p.usuario_asignado,p.induccion,p.fecha_induccion,
      p.nota_inicial, p.folio, p.escuela, p.idpuesto, p.como_llego, p.reasignado, p.deleted, p.fecha_creacion, p.finalizo, p.fecha_finalizo, p.idalumno, 
      p.idgrupo, p.nota_rechazo, pl.plantel, Date_format(TIMEDIFF(NOW(), p.fecha_creacion),'%H, h, %i m') AS tiempo,
      DATE(p.fecha_creacion) AS fecha,
			IF(DATEDIFF(NOW(), p.fecha_creacion) > 0, DATEDIFF(NOW(), p.fecha_creacion), '') AS dias,
			IF(TIMEDIFF(NOW(), p.fecha_creacion) > '00:10:00', 1, 0) AS retardo FROM prospectos p 
			LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
			WHERE p.deleted = 0 AND idprospectos = ${ id }
			ORDER BY DATEDIFF(NOW(), p.fecha_creacion) DESC, TIMEDIFF(NOW(), p.fecha_creacion) DESC;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getCursosActivos  = ( ) => {
	return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM cursos WHERE activo_sn = 1 ORDER BY curso;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getCiclosActivos  = ( ) => {
	return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM ciclos WHERE activo_sn = 1`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getComentarios  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM comentarios_prospectos WHERE deleted = 0 AND idprospectos = ?`,[ id ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.addComentario = (z, result) => {
  sqlERPNUEVO.query(`INSERT INTO comentarios_prospectos(idprospectos,idusuarioerp,comentario)VALUES(?,?,?)`, [z.idprospectos, z.idusuarioerp, z.comentario],
    (err, res) => {
      if (err) { result(err, null); return; }
      result(null, { id: res.insertId, ...z });
    }
  )
}

prospectos.addComentarioFormulario = (z, result) => {
  sqlERPNUEVO.query(`INSERT INTO comentarios_reclutadora(idformularios,idusuarioerp,comentario,etapa)VALUES(?,?,?,?)`, [z.idformularios, z.idusuarioerp, z.comentario, z.etapa],
    (err, res) => {
      if (err) { result(err, null); return; }
      result(null, { id: res.insertId, ...z });
    }
  )
}

prospectos.addTareaProspecto = ( z ) => {
	return new Promise((resolve, reject) => {
	  sqlERPNUEVO.query(`INSERT INTO tareas_prospectos(idtareas,idmotivos,dia,hora,idprospectos,idusuarioerp,estatus,medio,tipo_tarea,descrip_motivo)VALUES(?,?,${z.dia},${z.hora},?,?,?,?,?,?)`,
	  [z.idtareas,z.idmotivos,z.idprospectos,z.idusuarioerp,z.estatus,z.medio, z.tipo_tarea, z.descrip_motivo],
    (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve({ id: res.insertId, ...z });
    })
  })
}

prospectos.actualizarTareaProspecto  = ( t, id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE tareas_prospectos SET idmotivos = ?, dia = ${t.dia}, hora = ${t.hora}, idprospectos = ?, idusuarioerp = ?, estatus = ?, medio = ?, tipo_tarea  = ?, descrip_motivo = ?, idtareas = ? WHERE idtareas_prospectos = ${id};`,
      [ t.idmotivos, t.idprospectos, t.idusuarioerp, t.estatus, t.medio, t.tipo_tarea, t.descrip_motivo, t.idtareas ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getTareasProspectos  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT tp.*, Date_format(hora,'%h:%i %p') AS tiempo, Date_format(dia,'%d-%M-%Y') AS fecha, t.tipo_tarea AS tarea_tipo, IF(CURDATE() > dia,1, IF(CURDATE() = dia, IF(CURTIME() < hora ,0,1),0)) AS cumplida, m.motivo FROM tareas_prospectos tp
      LEFT JOIN tareas t ON t.idtareas = tp.idtareas
      LEFT JOIN motivos m ON m.idmotivos = tp.idmotivos
			WHERE tp.deleted = 0 AND tp.idprospectos = ? ORDER BY dia, hora;`,[ id ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getTareasUsuario  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT tp.*, Date_format(hora,'%h:%i %p') AS tiempo, p.*,
      Date_format(dia,'%d-%M-%Y') AS fecha, t.tipo_tarea, 
      IF(CURDATE() = dia,1,0) AS hoy,
      IF(CURDATE() > dia,1, IF(CURDATE() = dia, IF(CURTIME() < hora ,0,1),0)) AS cumplida,
      p.nombre_completo, m.motivo FROM tareas_prospectos tp
      LEFT JOIN tareas t ON t.idtareas = tp.idtareas
      LEFT JOIN prospectos p ON p.idprospectos = tp.idprospectos
      LEFT JOIN motivos m ON m.idmotivos = tp.idmotivos
      WHERE tp.deleted = 0 AND tp.idusuarioerp = ? AND tp.tipo_tarea = 1 ORDER BY dia, hora;`,[ id ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.existeTarea  = ( id, dia, hora ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT *, CONCAT(dia, ' ', hora) AS fecha FROM tareas_prospectos WHERE deleted = 0
			AND CONCAT(dia, ' ', hora) = CONCAT(?, ' ', ?,':00') AND idusuarioerp =  ?;`,[ dia, hora, id ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getTarea  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT tp.*, t.tarea FROM tareas_prospectos tp
      LEFT JOIN tareas t ON t.idtareas = tp.idtareas WHERE tp.idtareas_prospectos = ?;`,[ id ], (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve(res[0]);
    });
  });
};

prospectos.eliminarTarea  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE tareas_prospectos SET deleted = 1 WHERE idtareas_prospectos = ?;`,[ id ], (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve(res[0]);
    });
  });
};

prospectos.getEventosProspectos  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM eventos WHERE idprospectos = ? AND deleted = 0 ORDER BY fecha_creacion DESC;`,[ id ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.teareaRealizada  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE tareas_prospectos SET deleted = 1, estatus = 1 WHERE idtareas_prospectos = ?;`,[ id ], (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve(res[0]);
    });
  });
};

// Llamadas
prospectos.existeLlamada  = ( id ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM estatus_llamada WHERE idusuarioerp = ? AND estatus = 1;`,[ id ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.updateEstatusLlamada  = ( id, estatus ) => {
	return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE estatus_llamada SET estatus = ${estatus} WHERE idusuarioerp = ${id} AND estatus = 1;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.addLlamada = ( idusuarioerp, estatus, idprospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`INSERT INTO estatus_llamada(idusuarioerp,estatus,idprospectos)VALUES(?,?,?)`, [idusuarioerp, estatus, idprospectos],
    (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve({ id: res.insertId });
    })
  })
}

// Puntos de la llamada
prospectos.existePuntoLlamada  = ( idprospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.*, a.anuncio, m.modalidad, c.curso, f.frecuencia  FROM puntos_llamada p
      LEFT JOIN anuncios a ON a.idanuncios = p.idanuncios
      LEFT JOIN modalidad_cursos m ON m.idmodalidad_cursos = p.idmodalidad
      LEFT JOIN cursos_escuela c   ON c.idcursos_escuela = p.idcurso
      LEFT JOIN frecuencias f      ON f.idfrecuencias    = p.idfrecuencias WHERE p.idprospectos = ?;`,[ idprospectos ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getEstatusllamadaList  = ( idprospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT e.*, p.folio, p.nombre_completo, TIME(e.fecha_creacion) AS inicio, TIME(e.fecha_actualizo) AS finalizo, e.idusuarioerp,
      SEC_TO_TIME((TIMESTAMPDIFF(SECOND, e.fecha_creacion, e.fecha_actualizo ))*60) AS duracion FROM estatus_llamada e
      LEFT JOIN prospectos p ON p.idprospectos = e.idprospectos WHERE e.idprospectos = ?;`,[ idprospectos ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.updatePuntosLlamada  = ( id, d ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE puntos_llamada SET pregunta1 = ?, pregunta2 = ?, pregunta3 = ?, pregunta4 = ?, idplantel = ?, edad = ?,
      idanuncios = ?, idcurso = ?, idciclo = ?, idmodalidad =? , idfrecuencias = ?, notas = ?  WHERE idpuntos_llamada = ${ id };`,
      [ d.pregunta1,d.pregunta2,d.pregunta3,d.pregunta4,d.idplantel,d.edad,d.idanuncios,d.idcurso,d.idciclo, d.idmodalidad, d.idfrecuencias, d.notas ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.addPuntosLlamada = ( d ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`INSERT INTO puntos_llamada(idprospectos,pregunta1,pregunta2,pregunta3,pregunta4,idplantel,edad,idanuncios,idcurso,idciclo,idmodalidad,idfrecuencias,notas,medio_informacion)
    VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, 
    [ d.idprospectos,d.pregunta1,d.pregunta2,d.pregunta3,d.pregunta4,d.idplantel,d.edad,d.idanuncios,d.idcurso,d.idciclo,d.idmodalidad,d.idfrecuencias,d.notas,d.medio_informacion ],
    (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve({ id: res.insertId, ...d });
    })
  })
};

// consultar cursos activos
prospectos.getCursosActivosId  = ( idcurso ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM cursos WHERE id_curso = ?;`,[ idcurso ], (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve(res[0]);
    });
  });
};

// Consultar ciclos activos
prospectos.getCiclosActivosId  = ( idciclo ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM ciclos WHERE id_ciclo = ? `,[ idciclo ], (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve(res[0]);
    });
  });
};

prospectos.getNotasLlamada  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM notas_llamada WHERE idprospectos = ? `,[ id ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.addNotaLlamada = ( d ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`INSERT INTO notas_llamada(idprospectos,idusuarioerp,nota,idestatus_llamada) VALUES(?,?,?,?)`, [ d.idprospectos,d.idusuarioerp,d.nota,d.idestatus_llamada ],
    (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve({ id: res.insertId, ...d });
    })
  })
};

/* PLANTELES POR VENDEDORA */

prospectos.plantelesVendedora  = ( escuela ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.idplantel, p.plantel, p.acronimo, pu.idusuario, u.iderp, p.escuela FROM planteles_usuario pu
      LEFT JOIN plantel p ON p.idplantel = pu.idplantel
      LEFT JOIN usuarios u ON u.idusuario = pu.idusuario 
      WHERE pu.deleted = 0 AND u.idpuesto = 18 AND p.escuela = ${ escuela } AND u.deleted = 0
      GROUP BY idusuario;`,(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.reasignarProspecto = ( u ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE prospectos SET usuario_asignado = ?, reasignado = ? WHERE idprospectos = ?`,
      [ u.usuario_asignado ,u.reasignado, u.idprospectos ],(err, res) => {
      if (err) { return reject({message:err.sqlMessage}) }
      if (res.affectedRows == 0) {
        return reject({message:err.sqlMessage})
      }
      resolve({ ...u });
    });
  });
};

/* ERP VIEJO */

prospectos.getAlumnoMatricula = ( matricula ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM alumnos WHERE matricula = ?`,[matricula],(err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve(res[0]);
    });
  });
};

prospectos.getRegistroProspecto = ( id_alumno ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM prospectos WHERE idalumno = ?`,[id_alumno],(err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve(res[0]);
    });
  });
};

prospectos.updateProspectoFinalizo = ( idprospectos, id_alumno, idgrupo, codigo_clase ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE prospectos SET finalizo = 1, fecha_finalizo = NOW(), idalumno = ?, idgrupo = ?, codigo_clase = ? WHERE idprospectos = ?`,
      [ id_alumno, idgrupo, codigo_clase,idprospectos ],(err, res) => {
      if (err) { return reject({message:err.sqlMessage}) }
      if (res.affectedRows == 0) {
        return reject({message:err.sqlMessage})
      }
      resolve({ idprospectos, id_alumno, idgrupo });
    });
  });
};

prospectos.updateProspectoFinalizoFormulario = ( idformulario, id_alumno, idgrupo, nombres, apellido_paterno, apellido_materno ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE formulario SET finalizo = 1, fecha_finalizo = NOW(), 
      idalumno = ?, 
      idgrupo = ?, 
      nombres = ?, 
      apellido_paterno = ?, 
      apellido_materno = ? 
      WHERE idformulario = ?`,[ id_alumno, idgrupo, nombres, apellido_paterno, apellido_materno, idformulario ],(err, res) => {
      if (err) { return reject({message:err.sqlMessage}) }
      if (res.affectedRows == 0) {
        return reject({message: "datos vacios"})
      }
      resolve({ idformulario, id_alumno, idgrupo });
    });
  });
};

prospectos.getColaboradoresList = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT idformulario, nombres, apellido_paterno, apellido_materno, fecha_finalizo FROM formulario WHERE idalumno = 1 AND finalizo = 1;`,(err, res) => {
      if (err) { return reject({message:err.sqlMessage}) }
      resolve(res);
    });
  });
};


prospectos.getGrupos = ( escuela ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM grupos WHERE id_ciclo IN (
      SELECT id_ciclo FROM ciclos WHERE ciclo NOT LIKE '%INV%' AND ciclo NOT LIKE '%CAMBI%' AND activo_sn = 1)
      ORDER BY id_plantel, id_nivel;`,(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getGrupoAlumno = ( id_alumno, idgrupo ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM gruposalumnos WHERE id_alumno = ? AND id_grupo = ?;`,[id_alumno, idgrupo],(err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve(res[0]);
    });
  });
};

prospectos.updateProspectoFinalizoRechazo = ( idprospectos, notaRechazo ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE prospectos SET finalizo = 1, fecha_finalizo = NOW(), nota_rechazo = ? WHERE idprospectos = ?`,
      [ notaRechazo, idprospectos ],(err, res) => {
      if (err) { return reject({message:err.sqlMessage}) }
      if (res.affectedRows == 0) {
        return reject({message:err.sqlMessage})
      }
      resolve({ idprospectos });
    });
  });
};

prospectos.updateProspectoFinalizoRechazoRecluta = ( idformulario, notaRechazo ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE formulario SET finalizo = 1, fecha_finalizo = NOW(), nota_rechazo = ? WHERE idformulario = ?`,
      [ notaRechazo, idformulario ],(err, res) => {
      if (err) { return reject({message:err.sqlMessage}) }
      if (res.affectedRows == 0) {
        return reject({message:err.sqlMessage})
      }
      resolve({ idformulario });
    });
  });
};

//Angel Rodriguez
prospectos.updateProspectoCerrado = ( idprospectos) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE prospectos SET finalizo = 0 WHERE idprospectos = ?`,
      [ idprospectos ],(err, res) => {
      if (err) { return reject({message:err.sqlMessage}) }
      if (res.affectedRows == 0) {
        return reject({message:err.sqlMessage})
      }
      resolve({ idprospectos });
    });
  });
};

prospectos.activarProspectoCerradoRecluta = ( idformulario) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE formulario SET finalizo = 0 WHERE idformulario = ?`,
      [ idformulario ],(err, res) => {
      if (err) { return reject({message:err.sqlMessage}) }
      if (res.affectedRows == 0) {
        return reject({message:err.sqlMessage})
      }
      resolve({ idformulario });
    });
  });
};

/* REMARKETING */
prospectos.getRemarketing = ( escuela ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM prospectos WHERE deleted = 0 AND finalizo = 1 AND idalumno = 0 AND idgrupo = 0 AND escuela = ${ escuela };`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.addImagenCampania = ( imagen, extension ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`INSERT INTO imagen_campania(imagen,extension) VALUES(?,?)`, [ imagen, extension ],
    (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve({ id: res.insertId });
    })
  })
};

/* INSCRITOS */
prospectos.getProspectosInscritos = ( escuela, fechaini, fechafin ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT *, DATE(fecha_finalizo) AS fecha_filtro FROM prospectos WHERE deleted = 0 AND finalizo = 1 AND idalumno > 0 AND idgrupo > 0 AND escuela = ${ escuela }
      AND DATE(fecha_finalizo) BETWEEN ? AND ?;`,[ fechaini, fechafin ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getEventosPorVendedora = ( fechaini, fechafin ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT COUNT(idprospectos) AS total, idprospectos,usuario_registro FROM eventos 
      WHERE idtipo_evento IN (3, 5, 7)
      AND DATE(fecha_creacion) BETWEEN ? AND ?
      GROUP BY usuario_registro, idprospectos;`,[ fechaini, fechafin ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getGruposProspectos = ( grupos ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT id_ciclo, id_grupo FROM grupos WHERE activo_sn = 1 AND id_grupo IN ( ? );`,[ grupos ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};


prospectos.getVendedoras = ( escuela ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT u.iderp, (SELECT idplantel FROM planteles_usuario WHERE idusuario = u.idusuario LIMIT 1) AS idplantel,
      (SELECT idunidad_negocio FROM plantel WHERE idplantel = (SELECT idplantel FROM planteles_usuario WHERE idusuario = u.idusuario LIMIT 1)) AS escuela FROM usuarios u
      LEFT JOIN puesto p ON p.idpuesto = u.idpuesto
      WHERE p.idpuesto = 18 AND (SELECT idplantel FROM planteles_usuario WHERE idusuario = u.idusuario LIMIT 1) IS NOT NULL
      AND (SELECT idunidad_negocio FROM plantel WHERE idplantel = (SELECT idplantel FROM planteles_usuario WHERE idusuario = u.idusuario LIMIT 1)) = ${ escuela };`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

// Información que se le da a los prospectos
prospectos.getInfoEscuelaProspecto  = ( idprospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT  * FROM info_escuela_prospecto  WHERE idprospectos = ?;`,[ idprospectos ], (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve(res[0]);
    });
  });
};

prospectos.updateInfoEscuelaProspecto  = ( idprospectos, iderp, informacion ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE info_escuela_prospecto SET informacion = ? WHERE idinfo_escuela_prospecto > 0 AND idprospectos = ${ idprospectos };`,
      [ informacion ],(err, res) => {
      if (err) { return reject({message:err.sqlMessage}) }
      if (res.affectedRows == 0) {
        return reject({message:err.sqlMessage})
      }
      resolve({ idprospectos });
    });
  });
};

prospectos.addInfoEscuelaProspecto = ( idprospectos, iderp, informacion ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`INSERT INTO info_escuela_prospecto( idprospectos, iderp, informacion ) VALUES( ?, ?, ? )`, 
    [ idprospectos, iderp, informacion ],
    (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve({ id: res.insertId, idprospectos, iderp, informacion });
    })
  })
};

prospectos.getNuevosProspectos  = ( idusuarioerp ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT COUNT(p.idprospectos) AS nuevos FROM prospectos p
      WHERE p.idetapa IN (1,2)
      AND p.usuario_asignado = ?
      AND DATE(p.fecha_creacion) = CURRENT_DATE()
      AND p.finalizo = 0;`,[ idusuarioerp ], (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve(res[0]);
    });
  });
};

// Atrasados ( 1 )
// prospectos.getProspectosVendedoraAtrasados  = ( ) => {
//   return new Promise((resolve, reject) => {
//     sqlERPNUEVO.query(`SELECT p.*, pl.plantel, DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido,pl.plantel,
//       IFNULL((SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos),0) AS tareasProgramadas,
//       (SELECT  COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) AS puntos_llamada,
//       IFNULL((SELECT idtipo_evento FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (9,10,11,12) ORDER BY fecha_creacion DESC LIMIT 1),0) AS intentoLlamada,
//       IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) GROUP BY idprospectos),0) AS cantEventos,
//       IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) AND DATE(fecha_creacion) = CURRENT_DATE() GROUP BY idprospectos),0) AS cantEventosHoy,
//       DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, 1 AS idetapa
//       FROM prospectos p 
//       LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
//       WHERE p.deleted = 0 
//       AND p.finalizo = 0
//       AND (SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos ) IS NULL
//       AND (SELECT COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) IS NULL
//       AND CURRENT_DATE() > DATE(p.fecha_creacion) 
//       AND induccion = 0;`, (err, res) => {
//       if (err) {
//         reject(err);
//         return;
//       }
//       resolve(res);
//     });
//   });
// };

// Nuevosss ( 2 )
// prospectos.getProspectosVendedora  = ( ) => {
//   return new Promise((resolve, reject) => {
//     sqlERPNUEVO.query(`SELECT p.*, pl.plantel, DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido,pl.plantel,DATE(p.fecha_creacion) AS fecha,
//       IFNULL((SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos),0) AS tareasProgramadas,
//       (SELECT  COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) AS puntos_llamada,
//       IFNULL((SELECT idtipo_evento FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (9,10,11,12) ORDER BY fecha_creacion DESC LIMIT 1),0) AS intentoLlamada,
//       IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) GROUP BY idprospectos),0) AS cantEventos,
//       IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) AND DATE(fecha_creacion) = CURRENT_DATE() GROUP BY idprospectos),0) AS cantEventosHoy,
//       DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, 2 AS idetapa
//       FROM prospectos p 
//       LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
//       WHERE p.deleted = 0 
//       AND p.finalizo = 0
//       AND (SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos ) IS NULL
//       AND (SELECT COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) IS NULL
//       AND DATE(p.fecha_creacion) = CURRENT_DATE()
//       AND induccion = 0;`,(err, res) => {
//       if (err) {
//         reject(err);
//         return;
//       }
//       resolve(res);
//     });
//   });
// };

// Obtener los datos de un solo prospecto
prospectos.getProspecto  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.idprospectos, p.nombre_completo,p.telefono,p.correo,p.sucursal_interes,p.usuario_asignado,p.usuario_creo,p.nota_inicial,
      p.folio, p.escuela, p.idpuesto, p.reasignado, p.fecha_creacion, p.finalizo, p.idexamenubicacion,
      p.idalumno,p.idgrupo, p.induccion, p.fecha_induccion,DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido,DATE(p.fecha_creacion) AS fecha,
      IFNULL((SELECT idtipo_evento FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (9,10,11,12) ORDER BY fecha_creacion DESC LIMIT 1),0) AS intentoLlamada, 
      DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, 
      IF(p.idetapa > 2, p.idetapa, IF(DATE(p.fecha_creacion) = CURRENT_DATE(),2,1)) AS idetapa
      FROM prospectos p 
      WHERE p.deleted = 0 AND p.finalizo = 0 AND p.idprospectos = ${ id };`,(err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve(res[0]);
    });
  });
};

// Todos los prospectos
prospectos.getProspectosVendedora  = ( parametro ) => {      
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT  p.idprospectos, p.nombre_completo,p.telefono,p.correo,p.sucursal_interes,p.usuario_asignado,p.usuario_creo,p.nota_inicial,
    p.folio, p.escuela, p.idpuesto, p.reasignado, p.fecha_creacion, p.finalizo,p.ultimoevento AS intentoLlamada, p.clasificacion, p.justificacion, p.idexamenubicacion,
    p.idalumno,p.idgrupo, p.induccion, p.fecha_induccion,DATE(p.fecha_creacion) AS fecha,
    DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, pl.medio_informacion,
    IF(p.idetapa > 2, p.idetapa, IF(DATE(p.fecha_creacion) = CURRENT_DATE(),2,1)) AS idetapa
    FROM prospectos p
    LEFT JOIN puntos_llamada pl ON pl.idprospectos = p.idprospectos 
    WHERE p.deleted = 0 AND p.finalizo = 0 ${ parametro } ORDER BY idetapa;`,(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};


prospectos.getProspectosSeguimiento  = ( parametro ) => {      
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT  idprospectos, nombre_completo, telefono, usuario_asignado, folio, fecha_creacion,
    IF(idetapa > 2, idetapa, IF(DATE(fecha_creacion) = CURRENT_DATE(),2,1)) AS idetapa
    FROM prospectos
    WHERE deleted = 0 AND finalizo = 0 ${ parametro }
    AND usuario_asignado <> 86
    ORDER BY idetapa;`,(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getProspectosCalidad  = ( parametro, fechaini, fechafin ) => {      
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT  idprospectos, nombre_completo, telefono, usuario_asignado, folio, fecha_creacion, clasificacion, justificacion, justificacion2
    FROM prospectos
    WHERE deleted = 0 ${ parametro }
    AND usuario_asignado <> 86
    AND DATE(fecha_creacion) BETWEEN ? AND ? 
    ORDER BY idetapa;`,[ fechaini, fechafin ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};



//Prospectos cerrados                                 //Angel Rodriguez
prospectos.getProspectosCerrados = ( parametro ) => {
  return new Promise((resolve,reject) => {
    sqlERPNUEVO.query(`SELECT p.idprospectos, p.folio, p.nombre_completo, p.telefono, p.nota_rechazo 
    FROM prospectos p
    WHERE p.finalizo = 1 AND p.idgrupo = 0 ${ parametro };`,(err, res) => {
      if(err){
        return reject({message: err.sqlMessage })
      }
      resolve(res)
    });
  });
};

prospectos.getProspectosCerradosRecluta = ( parametro ) => {
  return new Promise((resolve,reject) => {
    sqlERPNUEVO.query(`SELECT p.idformulario, p.correo, p.nombre_completo, p.numero_wa, p.nota_rechazo 
    FROM formulario p
    WHERE p.finalizo = 1;`,(err, res) => {
      if(err){
        return reject({message: err.sqlMessage })
      }
      resolve(res)
    });
  });
};


// Tareas progrmadas
prospectos.tareasProspectos  = ( prospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM tareas_prospectos WHERE idprospectos IN ( ? );`,[ prospectos ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

// Tareas progrmadas
prospectos.tareasProspectosCount  = ( prospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT COUNT( * ) AS cantidad, idprospectos FROM tareas_prospectos WHERE idprospectos IN ( ? )
      GROUP BY idprospectos;`,[ prospectos ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

// Eventos Generales
prospectos.eventosProspectos  = ( prospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM eventos WHERE idprospectos IN ( ? ) AND idtipo_evento IN (3, 5, 11, 12);`,[ prospectos ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.eventosProspectosCount  = ( prospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT COUNT( * ) AS cantidad, idprospectos FROM eventos WHERE idprospectos IN ( ? ) AND idtipo_evento IN (3, 5, 11, 12)
      GROUP BY idprospectos;`,[ prospectos ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

// Eventos del día de hoy
prospectos.eventosProspectosHoy  = ( prospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM eventos WHERE idprospectos IN ( ? ) AND idtipo_evento IN (3, 5, 11, 12) AND DATE(fecha_creacion) = CURRENT_DATE();`,[ prospectos ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

// Eventos del día de hoy
prospectos.eventosProspectosHoyCount  = ( prospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT COUNT( * ) AS cantidad, idprospectos FROM eventos WHERE idprospectos IN ( ? ) AND idtipo_evento IN (3, 5, 11, 12) 
      AND DATE(fecha_creacion) = CURRENT_DATE()
      GROUP BY idprospectos;`,[ prospectos ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.intentoLlamadaProspectos  = ( prospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM eventos WHERE idprospectos IN ( ? ) AND idtipo_evento IN (3, 5, 11, 12) AND DATE(fecha_creacion) = CURRENT_DATE();`,[ prospectos ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.citasProspectos  = ( prospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM tareas_prospectos WHERE tipo_tarea = 1 AND deleted = 0  AND dia = CURDATE() AND hora >= current_time() AND idprospectos IN ( ? );`,[ prospectos ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};


// Induccion ( 3 )
prospectos.getProspectosVendedoraInduccion  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.*, pl.plantel, DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido,pl.plantel,DATE(p.fecha_creacion) AS fecha,
      IFNULL((SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos),0) AS tareasProgramadas,
      (SELECT  COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) AS puntos_llamada,
      IFNULL((SELECT idtipo_evento FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (9,10,11,12) ORDER BY fecha_creacion DESC LIMIT 1),0) AS intentoLlamada,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) GROUP BY idprospectos),0) AS cantEventos,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) AND DATE(fecha_creacion) = CURRENT_DATE() GROUP BY idprospectos),0) AS cantEventosHoy,
      DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, 3 AS idetapa
      FROM prospectos p 
      LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
      WHERE p.deleted = 0 
      AND p.finalizo = 0
      AND induccion = 1;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

// Con infooo ( 4 ) 
prospectos.getProspectosVendedoraConInfo  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.*, pl.plantel, DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido,pl.plantel,
      IFNULL((SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos),0) AS tareasProgramadas,
      (SELECT  COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) AS puntos_llamada,
      IFNULL((SELECT idtipo_evento FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (9,10,11,12) ORDER BY fecha_creacion DESC LIMIT 1),0) AS intentoLlamada,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) GROUP BY idprospectos),0) AS cantEventos,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) AND DATE(fecha_creacion) = CURRENT_DATE() GROUP BY idprospectos),0) AS cantEventosHoy,
      DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, 4 AS idetapa
      FROM prospectos p 
      LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
      WHERE p.deleted = 0 
      AND p.finalizo = 0
      AND (SELECT COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) IS NOT NULL  
      AND induccion = 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

// Sin infooo ( 5 )
prospectos.getProspectosVendedoraSinInfo  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.*, pl.plantel, DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, pl.plantel,DATE(p.fecha_creacion) AS fecha,
      IFNULL((SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos),0) AS tareasProgramadas,
      IFNULL((SELECT idtipo_evento FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (9,10,11,12) ORDER BY fecha_creacion DESC LIMIT 1),0) AS intentoLlamada,
      (SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) GROUP BY idprospectos ) AS cantEventos,
      DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, 5 AS idetapa
      FROM prospectos p 
      LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
      WHERE p.deleted = 0 
      AND p.finalizo = 0
      AND (SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos ) IS NOT NULL
      AND (SELECT COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) IS NULL  
      AND induccion = 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getProspectosOrdenados  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.*, pl.plantel, DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido,
      IF(p.induccion = 1,IF(CURDATE() > DATE(fecha_induccion), 1,0),0) AS dia_induccion, 
      IF(p.induccion = 1, 3,
          IF((SELECT COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) IS NOT NULL,4,
          IF((SELECT  COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) IS NULL,
            IF((SELECT  COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos) IS NOT NULL, 5, 0),
                  0))) AS idetapa,
      IFNULL((SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos),0) AS tareasProgramadas,
      IFNULL((SELECT idtipo_evento FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (9,10,11,12) ORDER BY fecha_creacion DESC LIMIT 1),0) AS intentoLlamada,
      IFNULL((SELECT COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos AND tipo_tarea = 1 AND deleted = 0  AND dia = CURDATE() AND hora >= current_time() GROUP BY idprospectos),0) AS cita,
      (SELECT  COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) AS puntos_llamada,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) GROUP BY idprospectos),0) AS cantEventos,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) AND DATE(fecha_creacion) = CURRENT_DATE() GROUP BY idprospectos),0) AS cantEventosHoy,
      DATE(p.fecha_creacion) AS fecha
      FROM prospectos p
      LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes 
      WHERE p.deleted = 0 AND p.finalizo = 0 
      AND IF(p.induccion = 1, 3,
          IF((SELECT COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) IS NOT NULL,5,
          IF((SELECT  COUNT(idprospectos) FROM puntos_llamada WHERE idprospectos = p.idprospectos AND deleted = 0 GROUP BY idprospectos) IS NULL,
            IF((SELECT  COUNT(idprospectos) FROM tareas_prospectos WHERE idprospectos = p.idprospectos GROUP BY idprospectos) IS NOT NULL, 4, 0),
                  0))) IN (3,4,5)
      ORDER BY cita DESC, cantEventos , tiempo_transcurrido , idetapa DESC;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

// FINALIZADOS ( 7 )
prospectos.getProspectosFinalizados  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.*, pl.plantel, DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido,pl.plantel,
      0 AS tareasProgramadas,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) GROUP BY idprospectos),0) AS cantEventos,
      IFNULL((SELECT idtipo_evento FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (9,10,11,12) ORDER BY fecha_creacion DESC LIMIT 1),0) AS intentoLlamada,
      0 AS cantEventosHoy,
      DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, 7 AS idetapa
      FROM prospectos p 
      LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
      WHERE p.deleted = 0 
      AND p.idgrupo = 0
      AND p.finalizo = 1;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getProspectosInscritos  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.*, pl.plantel, DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido,pl.plantel,
      0 AS tareasProgramadas,
      IFNULL((SELECT COUNT(idprospectos) FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (3, 5, 7) GROUP BY idprospectos),0) AS cantEventos,
      IFNULL((SELECT idtipo_evento FROM eventos WHERE idprospectos = p.idprospectos AND idtipo_evento IN (9,10,11,12) ORDER BY fecha_creacion DESC LIMIT 1),0) AS intentoLlamada,
      0 AS cantEventosHoy,
      DATEDIFF(NOW(), p.fecha_creacion) AS tiempo_transcurrido, 8 AS idetapa
      FROM prospectos p 
      LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes
      WHERE p.deleted = 0 
      AND p.idgrupo > 0
      AND p.finalizo = 1;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getTareasProspectosAll  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT tp.*, Date_format(hora,'%h:%i %p') AS tiempo, Date_format(dia,'%d-%M-%Y') AS fecha, t.tipo_tarea AS tarea_tipo, 
      IF(CURDATE() > dia,1, IF(CURDATE() = dia, IF(CURTIME() < hora ,0,1),0)) AS cumplida, m.motivo, IF(dia <= CURDATE(),1,0) AS tareaParaHoy 
      FROM tareas_prospectos tp
      LEFT JOIN tareas t ON t.idtareas = tp.idtareas
      LEFT JOIN motivos m ON m.idmotivos = tp.idmotivos
      WHERE tp.deleted = 0 ORDER BY dia, hora;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getTareasProspectosID  = ( ids ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT tp.*, Date_format(hora,'%h:%i %p') AS tiempo, Date_format(dia,'%d-%M-%Y') AS fecha, t.tipo_tarea AS tarea_tipo, 
      IF(CURDATE() > dia,1, IF(CURDATE() = dia, IF(CURTIME() < hora ,0,1),0)) AS cumplida, m.motivo, IF(dia <= CURDATE(),1,0) AS tareaParaHoy 
      FROM tareas_prospectos tp
      LEFT JOIN tareas t ON t.idtareas = tp.idtareas
      LEFT JOIN motivos m ON m.idmotivos = tp.idmotivos
      WHERE tp.deleted = 0 AND tp.idprospectos IN ( ? ) ORDER BY dia, hora;`,[ ids ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getIntentosLlamada  = ( ids ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.idprospectos, (SELECT e.idtipo_evento FROM eventos e WHERE e.idprospectos = p.idprospectos AND e.idtipo_evento IN (9,10,11,12) ORDER BY e.fecha_creacion DESC LIMIT 1) AS idtipo_evento
    FROM prospectos p WHERE p.idprospectos IN ( ? );`,[ ids ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};


prospectos.getLlamadaActiva  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.* FROM estatus_llamada e
      LEFT JOIN prospectos p ON p.idprospectos = e.idprospectos WHERE e.idusuarioerp = ? AND e.estatus = 1;`, [ id ],(err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve(res[0]);
    });
  });
};

prospectos.getTareasFinalizdasProspecto  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT tp.*, Date_format(hora,'%h:%i %p') AS tiempo, Date_format(dia,'%d-%M-%Y') AS fecha, t.tipo_tarea AS tarea_tipo, IF(CURDATE() > dia,1, IF(CURDATE() = dia, IF(CURTIME() < hora ,0,1),0)) AS cumplida, m.motivo FROM tareas_prospectos tp
      LEFT JOIN tareas t ON t.idtareas = tp.idtareas
      LEFT JOIN motivos m ON m.idmotivos = tp.idmotivos
      WHERE tp.deleted = 1 AND tp.estatus = 1 AND tp.idprospectos = ? ORDER BY dia, hora;`, [ id ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getContactosVendedora  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.nombre_completo, p.folio, p.usuario_creo, p.escuela, p.usuario_asignado, p.finalizo, p.fecha_creacion, DATE(p.fecha_creacion) AS fecha, pt.puesto FROM prospectos p
      LEFT JOIN puesto pt ON pt.idpuesto = p.idpuesto
      WHERE folio NOT LIKE '%EXCI%';`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.updateUltimoEvento = ( idprospectos, tipo_evento ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE prospectos SET ultimoevento = ? WHERE idprospectos = ?`, [ tipo_evento, idprospectos ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage}); }
      resolve({ tipo_evento, idprospectos });
    });
  });
};


prospectos.getProspectosxTelefonoOrIdAlumno  = ( celulares, telefonos, id_alumnos, telefonoTutor, celularTutor ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT f.fuente, df.detalle_fuente, p.* FROM prospectos p
      LEFT JOIN leds l ON l.idleds = p.idleds
      LEFT JOIN fuentes f ON f.idfuentes = l.idfuentes
      LEFT JOIN detalle_fuentes df ON df.iddetalle_fuentes = l.iddetalle_fuentes
      WHERE p.telefono IN ( ? ) AND p.folio NOT LIKE '%EXCI%' 
      OR p.telefono IN ( ? ) AND p.folio NOT LIKE '%EXCI%'
      OR p.telefono IN ( ? ) AND p.folio NOT LIKE '%EXCI%'
      OR p.telefono IN ( ? ) AND p.folio NOT LIKE '%EXCI%'
      OR p.idalumno IN ( ? ) AND p.folio NOT LIKE '%EXCI%';`,
      [ celulares, telefonos, telefonoTutor, celularTutor,  id_alumnos ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.updateProspectoClasificar = ( clasificacion, justificacion, idprospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE prospectos SET clasificacion = ?, justificacion = ? WHERE idprospectos = ?`, [ clasificacion, justificacion, idprospectos ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage}); }
      resolve({ clasificacion, justificacion, idprospectos });
    });
  });
};

prospectos.addRespuestaMkt = ( justificacion2, idprospectos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE prospectos SET justificacion2 = ? WHERE idprospectos = ?`, [ justificacion2, idprospectos ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage}); }
      resolve({ justificacion2, idprospectos });
    });
  });
};

prospectos.examenInduccionFast = ( uniques ) => {
  return new Promise((resolve, reject) => {
    sqlFAST.query(`SELECT e.id, e.telefono, e.nombre, n.nombre AS nivel, e.uniq FROM examen_interno.examen_ubicacion e
      LEFT JOIN examen_interno.niveles_evaluacion n ON n.id = e.nivel WHERE e.uniq IN (?);`,[ uniques ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

prospectos.examenInduccionInbi = ( uniques ) => {
  return new Promise((resolve, reject) => {
    sqlINBI.query(`SELECT e.id, e.telefono, e.nombre, n.nombre AS nivel, e.uniq FROM examen_interno.examen_ubicacion e
      LEFT JOIN examen_interno.niveles_evaluacion n ON n.id = e.nivel WHERE e.uniq IN (?);`,[ uniques ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};


prospectos.prospectosUsuario = ( id_usuario, fecha_inicio, fecha_final ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(
      `SELECT * FROM prospectos WHERE usuario_asignado = ?
      AND DATE(fecha_creacion) BETWEEN ? AND ?;`, [ id_usuario, fecha_inicio, fecha_final ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage}); }
      resolve(res);
    });
  });
};

prospectos.prospectosAlumnos = ( ids ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(
      `SELECT * FROM prospectos WHERE idalumno IN( ? );`, [ ids ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage}); }
      resolve(res);
    });
  });
};


// Todos los prospectos
prospectos.getProspectosReclutadora  = ( ) => {      
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT
      f.idformulario, f.nombre_completo, f.edad, f.correo, f.direccion, f.numero_wa, 
      f.puesto_interes, p.puesto, d.departamento,
      f.alguien_depende, f.estudiando, f.horario_estudio, f.horario_trabajo, f.trabajando, f.horario_estudio, 
      f.medio_vacante, f.dominio_ingles, f.cv, f.portafolio, f.aceptado, f.motivo_rechazo,
      f.finalizo,f.ultimoevento AS intentoLlamada, DATE(f.fecha_creacion) AS fecha, f.etapa,
      DATEDIFF(NOW(), f.fecha_creacion) AS tiempo_transcurrido,
      IF(f.idetapa > 2, f.idetapa, IF(DATE(f.fecha_creacion) = CURRENT_DATE(),2,1)) AS idetapa
      FROM formulario f
      LEFT JOIN puesto p ON p.idpuesto = f.puesto_interes
      LEFT JOIN departamento d ON d.iddepartamento = p.iddepartamento
      WHERE f.deleted = 0 AND f.finalizo = 0
      AND d.deleted = 0 ;`,(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};


prospectos.tareasReclutaCount  = ( formularios ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT COUNT( * ) AS cantidad, idformularios FROM tareas_reclutadora WHERE idformularios IN ( ? )
      GROUP BY idformularios;`,[ formularios ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};


prospectos.eventosReclutadoraCount  = ( formularios ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT COUNT( * ) AS cantidad, idformularios FROM eventos_recluta WHERE idformularios IN ( ? ) AND idtipo_evento IN (3, 5, 11, 12)
      GROUP BY idformularios;`,[ formularios ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};


prospectos.eventosReclutadoraHoyCount  = ( formularios ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT COUNT( * ) AS cantidad, idformularios FROM eventos_recluta WHERE idformularios IN ( ? ) AND idtipo_evento IN (3, 5, 11, 12) 
      AND DATE(fecha_creacion) = CURRENT_DATE()
      GROUP BY idformularios;`,[ formularios ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};


prospectos.citasReclutadora  = ( formularios ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM tareas_reclutadora WHERE tipo_tarea = 1 AND deleted = 0  AND dia = CURDATE() AND hora >= current_time() AND idformularios IN ( ? );`,[ formularios ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getTareasReclutadoraID  = ( ids ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT tp.*, Date_format(hora,'%h:%i %p') AS tiempo, Date_format(dia,'%d-%M-%Y') AS fecha, t.tipo_tarea AS tarea_tipo, 
      IF(CURDATE() > dia,1, IF(CURDATE() = dia, IF(CURTIME() < hora ,0,1),0)) AS cumplida, m.motivo, IF(dia <= CURDATE(),1,0) AS tareaParaHoy 
      FROM tareas_reclutadora tp
      LEFT JOIN tareas t ON t.idtareas = tp.idtareas
      LEFT JOIN motivos m ON m.idmotivos = tp.idmotivos
      WHERE tp.deleted = 0 AND tp.idformularios IN ( ? ) ORDER BY dia, hora;`,[ ids ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.getComentariosFormulario  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM comentarios_reclutadora WHERE deleted = 0 AND idformularios = ?`,[ id ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};


prospectos.getTareasFormulario  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT tp.*, Date_format(hora,'%h:%i %p') AS tiempo, Date_format(dia,'%d-%M-%Y') AS fecha, t.tipo_tarea AS tarea_tipo, 
      IF(CURDATE() > dia,1, IF(CURDATE() = dia, IF(CURTIME() < hora ,0,1),0)) AS cumplida, m.motivo FROM tareas_reclutadora tp
      LEFT JOIN tareas t ON t.idtareas = tp.idtareas
      LEFT JOIN motivos m ON m.idmotivos = tp.idmotivos
      WHERE tp.deleted = 0 AND tp.idformularios = ? ORDER BY dia, hora;`,[ id ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.addTareaReclutadora = ( z ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`INSERT INTO tareas_reclutadora(idtareas,idmotivos,dia,hora,idformularios,idusuarioerp,estatus,medio,tipo_tarea,descrip_motivo)VALUES(?,?,${z.dia},${z.hora},?,?,?,?,?,?)`,
    [z.idtareas,z.idmotivos,z.idformularios,z.idusuarioerp,z.estatus,z.medio, z.tipo_tarea, z.descrip_motivo],
    (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve({ id: res.insertId, ...z });
    })
  })
}

prospectos.existeTareaReclutadora  = ( id, dia, hora ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT *, CONCAT(dia, ' ', hora) AS fecha FROM tareas_reclutadora WHERE deleted = 0
      AND CONCAT(dia, ' ', hora) = CONCAT(?, ' ', ?,':00') AND idusuarioerp =  ?;`,[ dia, hora, id ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.addEventoRecluta = ( evento, idformularios, idtipo_evento, usuario_registro ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`INSERT INTO eventos_recluta(evento,idformularios,idtipo_evento, usuario_registro)VALUES(?,?,?,?)`,
      [evento,idformularios,idtipo_evento,usuario_registro],(err, res) => {
      if (err) {
        return reject({message: err.sqlMessage});
        return;
      }
       resolve({ id: res.insertId });
    });
  });
};


prospectos.getTareasUsuarioRecluta  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT tp.*, Date_format(hora,'%h:%i %p') AS tiempo, p.*,
      Date_format(dia,'%d-%M-%Y') AS fecha, t.tipo_tarea, 
      IF(CURDATE() = dia,1,0) AS hoy,
      IF(CURDATE() > dia,1, IF(CURDATE() = dia, IF(CURTIME() < hora ,0,1),0)) AS cumplida,
      p.nombre_completo, m.motivo FROM tareas_reclutadora tp
      LEFT JOIN tareas t ON t.idtareas = tp.idtareas
      LEFT JOIN formulario p ON p.idformulario = tp.idformularios
      LEFT JOIN motivos m ON m.idmotivos = tp.idmotivos
      WHERE tp.deleted = 0 AND tp.idusuarioerp = ? AND tp.tipo_tarea = 1 ORDER BY dia, hora;`,[ id ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.updateEtapaFormulario = (id, idetapa ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE formulario SET idetapa = ? WHERE idformulario = ?`, [ idetapa, id],(err, res) => {
      if (err) { return reject({message:err.sqlMessage}) }
      if (res.affectedRows == 0) {
        return reject({message:err.sqlMessage})
      }
      resolve({ id, idetapa });
    });
  });
};

prospectos.updateEtapaActualFormulario = (id, etapa ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE formulario SET etapa = ? WHERE idformulario = ?`, [ etapa, id],(err, res) => {
      if (err) { return reject({message:err.sqlMessage}) }
      if (res.affectedRows == 0) {
        return reject({message:err.sqlMessage})
      }
      resolve({ id, etapa });
    });
  });
};


prospectos.getEventosRecluta  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM eventos_recluta WHERE idformularios = ? AND deleted = 0 ORDER BY fecha_creacion DESC;`,[ id ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

prospectos.getTareaRecluta  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT tp.*, t.tarea FROM tareas_reclutadora tp
      LEFT JOIN tareas t ON t.idtareas = tp.idtareas WHERE tp.idtareas_reclutadora = ?;`,[ id ], (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve(res[0]);
    });
  });
};

prospectos.eliminarTareaRecluta  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE tareas_reclutadora SET deleted = 1 WHERE idtareas_reclutadora = ?;`,[ id ], (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve(res[0]);
    });
  });
};

prospectos.teareaRealizadaRecluta  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE tareas_reclutadora SET deleted = 1, estatus = 1 WHERE idtareas_reclutadora = ?;`,[ id ], (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve(res[0]);
    });
  });
};

// Llamadas
prospectos.existeLlamadaRecluta  = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM estatus_llamada_recluta WHERE idusuarioerp = ? AND estatus = 1;`,[ id ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.updateEstatusLlamadaRecluta  = ( id, estatus ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE estatus_llamada_recluta SET estatus = ${estatus} WHERE idusuarioerp = ${id} AND estatus = 1;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage});
      }
      resolve(res);
    });
  });
};

prospectos.addLlamadaRecluta = ( idusuarioerp, estatus, idformularios ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`INSERT INTO estatus_llamada_recluta(idusuarioerp,estatus,idformularios)VALUES(?,?,?)`, [idusuarioerp, estatus, idformularios],
    (err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve({ id: res.insertId });
    })
  })
}

prospectos.updateUltimoEventoRecluta = ( idformularios, tipo_evento ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE formulario SET ultimoevento = ? WHERE idformulario = ?`, [ tipo_evento, idformularios ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage}); }
      resolve({ tipo_evento, idformularios });
    });
  });
};



module.exports = prospectos;

