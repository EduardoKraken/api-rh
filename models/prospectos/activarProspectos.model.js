const { result }  = require("lodash");
const sqlERP      = require("../db3.js");
const sqlFAST     = require("../dbFAST.js");
const sqlERPNUEVO = require("../db.js");

//const constructor
const activarProspectos = function() {};

activarProspectos.getProspectos = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT idprospectos, folio, nombre_completo, telefono, nota_rechazo 
    FROM prospectos WHERE finalizo = 1 and idgrupo = 0;`,(err, res) => {
      if(err){
        return reject({message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};

activarProspectos.updateProspecto = ( idprospectos ) => {
return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE prospectos SET finalizo = 0 WHERE idprospectos = ?`,[idprospectos],(err, res) => {
    if (err) {
      return reject({message:err.sqlMessage})
    }

    resolve( res );
    });
});
};

module.exports = activarProspectos;

//ANGEL RODRIGUEZ -- TODO