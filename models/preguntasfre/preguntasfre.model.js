const { result }  = require("lodash");
const sqlERPNUEVO = require("../db.js");
const sqlERP      = require("../db3.js");


//const constructor
const preguntasfre = function(clases) {};

// el usuario agrega una pregunta
preguntasfre.addPregunta = ( pregunta, quien_pregunto, idpuesto ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO preguntas_usuarios(pregunta,quien_pregunto,idpuesto)VALUES(?,?,?)`, [ pregunta, quien_pregunto, idpuesto ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id: res.insertId, pregunta, quien_pregunto, idpuesto })
    })
  });
};

// Responder la pregunta que hizo el usuario
preguntasfre.responderPreguntaUsuario = ( idpreguntas_usuarios, respuesta, quien_respondio ) => {
	return new Promise(( resolve, reject )=>{
	  sqlERPNUEVO.query(`UPDATE preguntas_usuarios SET respuesta = ?, quien_respondio = ?  WHERE idpreguntas_usuarios = ?`,[ respuesta, quien_respondio, idpreguntas_usuarios ],
	    (err, res) => {
      if (err) {
        return reject( err.sqlMessage );
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve({ idpreguntas_usuarios, respuesta, quien_respondio });
	  });
	});
};

// Agregar una pregunta frecuente
preguntasfre.addPreguntaFrecuente = ( pregunta, respuesta, idpuesto, iderp, idpreguntas_usuarios ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO preguntas_frecuentes(pregunta, respuesta, idpuesto, iderp, idpreguntas_usuarios)VALUES(?,?,?,?,?)`, 
      [ pregunta, respuesta, idpuesto, iderp, idpreguntas_usuarios ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id: res.insertId, pregunta, respuesta, idpuesto, iderp, idpreguntas_usuarios })
    })
  });
};

// Ver mis preguntas realizadas
preguntasfre.misPreguntas = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM preguntas_usuarios WHERE quien_pregunto = ? AND deleted = 0;`,[ id ], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  })
}

// Todas las preguntas frecuentes
preguntasfre.getPreguntasFrecuentes = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT pr.*, p.puesto, pu.quien_pregunto, pu.quien_respondio FROM preguntas_frecuentes pr
        LEFT JOIN preguntas_usuarios pu ON pu.idpreguntas_usuarios = pr.idpreguntas_usuarios
        LEFT JOIN puesto p ON p.idpuesto = pr.idpuesto WHERE pr.deleted = 0;`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  })
}

// Actualizar una pregunta frecuente
preguntasfre.updatePreguntaFrecuente = ( idpreguntas_frecuentes, pregunta, respuesta, estatus ) => {
	return new Promise(( resolve, reject )=>{
	  sqlERPNUEVO.query(`UPDATE preguntas_frecuentes SET pregunta = ?, respuesta = ?, estatus = ?  WHERE idpreguntas_frecuentes = ?`,[ pregunta, respuesta, estatus, idpreguntas_frecuentes ],
	    (err, res) => {
      if (err) {
        return reject( err.sqlMessage );
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve({ idpreguntas_frecuentes, pregunta, respuesta });
	  });
	});
};


// Eliminar una pregunta frecuente
preguntasfre.eliminarFrecuente = ( idpreguntas_frecuentes ) => {
	return new Promise(( resolve, reject )=>{
	  sqlERPNUEVO.query(`UPDATE preguntas_frecuentes SET deleted = 1  WHERE idpreguntas_frecuentes = ?`,[ idpreguntas_frecuentes ],
	    (err, res) => {
      if (err) {
        return reject( err.sqlMessage );
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve({ idpreguntas_frecuentes });
	  });
	});
};

// Todas las preguntas de los usuarios
preguntasfre.getPreguntasUsuario = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT p.*, pf.idpreguntas_frecuentes FROM preguntas_usuarios p
        LEFT JOIN preguntas_frecuentes pf ON pf.idpreguntas_usuarios = p.idpreguntas_usuarios
        WHERE p.deleted = 0;`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  })
}


preguntasfre.getUsuariosERPID = ( ids ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM usuarios WHERE id_usuario IN ( ? );`,[ ids ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  })
}


module.exports = preguntasfre;