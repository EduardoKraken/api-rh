const mysql = require('mysql');
const dbConfig = require("../config/dbSistemaWeb.config.js");

// CREATE A CONNECTION TO THE DATABASE
const connection = mysql.createConnection({
    host: dbConfig.HOST,
    user: dbConfig.USER,
    password: dbConfig.PASSWORD,
    database: dbConfig.DB,
    dateStrings: true
});

// OPEN THE MYSQL CONNECTION
connection.connect(error => {
    if (error) throw error;
    console.log("|********* CONEXIÓN A" + " " + dbConfig.DB + " " + "CORRECTAMENTE ********************|");
});

module.exports = connection;