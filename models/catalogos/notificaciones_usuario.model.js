const { result } = require("lodash");
const sql = require("../db.js");


//const constructor
const NotificacionesUsuario = function(notificaciones) {};

NotificacionesUsuario.getNotificacionesUsuario = result => {
    sql.query(`SELECT * FROM notificacion_usuario WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

NotificacionesUsuario.addNotificacionUsuario = (c, result) => {
    sql.query(`INSERT INTO notificacion_usuario(estatus,idnotificacion,idusuario,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?,?,?)`, [c.estatus, c.idnotificacion, c.idusuario, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo],
        (err, res) => {
            if (err) { result(err, null); return; }
            result(null, { id: res.insertId, ...c });
        }
    )
}

NotificacionesUsuario.updateNotificacionUsuario = (id, c, result) => {
    sql.query(` UPDATE notificacion_usuario SET estatus=?,idnotificacion=?,idusuario=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idnotificacion_usuario=?`, [c.estatus, c.idnotificacion, c.idusuario, c.usuario_registro, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            result(null, { id: id, ...c });
        })
}



module.exports = NotificacionesUsuario;