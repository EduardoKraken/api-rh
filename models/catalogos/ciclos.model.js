const { result }  = require("lodash");
// Original
// const sqlERP      = require("../db3.js");

// Prueba
const sqlERP      = require("../dbErpPrueba.js");

//const constructor
const ciclos = function(ciclos) {};

// Consultar todos los ciclos
ciclos.getCiclos = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT id_ciclo, ciclo, DATE(fecha_inicio_ciclo) AS fecha_inicio_ciclo,  DATE(fecha_fin_ciclo) AS fecha_fin_ciclo,
			comentarios, fecha_alta, activo_sn, DATE(fecha_baja) AS fecha_baja, id_usuario_ultimo_cambio, DATE(fecha_ultimo_cambio) AS fecha_ultimo_cambio,
			ciclo_abierto_sn, id_ciclo_relacionado, IF(id_ciclo IN (SELECT id_ciclo FROM grupos),1,0) AS en_grupo FROM ciclos;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

// consultar solo los ciclos activos
ciclos.getCiclosActivos = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT id_ciclo, ciclo, DATE(fecha_inicio_ciclo) AS fecha_inicio_ciclo,  DATE(fecha_fin_ciclo) AS fecha_fin_ciclo,
			comentarios, fecha_alta, activo_sn, DATE(fecha_baja) AS fecha_baja, id_usuario_ultimo_cambio, DATE(fecha_ultimo_cambio) AS fecha_ultimo_cambio,
			ciclo_abierto_sn, id_ciclo_relacionado, IF(id_ciclo IN (SELECT id_ciclo FROM grupos),1,0) AS en_grupo FROM ciclos WHERE activo_sn = 1 AND ciclo_abierto_sn = 1;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

// Agregar un ciclo+
ciclos.addCiclo = ( pu ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO ciclos(ciclo,fecha_inicio_ciclo,fecha_fin_ciclo,comentarios,id_usuario_ultimo_cambio)VALUES(?,?,?,?,?)`, 
    [pu.ciclo, pu.fecha_inicio_ciclo, pu.fecha_fin_ciclo, pu.comentarios, pu.id_usuario],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id: res.insertId, ...pu })
    })
  })
}

// Actualizar el ciclo en general
ciclos.updateCiclo = ( id, a ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE ciclos SET id_usuario_ultimo_cambio = ?, ciclo_abierto_sn = ?, activo_sn = ?  WHERE id_ciclo = ?`, [ a.id_usuario, a.ciclo_abierto_sn, a.activo_sn, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, a })
    })
  })
}

// Eliminar el ciclo
ciclos.deleteCiclo = ( id, u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE ciclos SET activo_sn = 0, fecha_baja = NOW(), id_usuario_ultimo_cambio = ?, ciclo_abierto_sn = 0 WHERE id_ciclo = ?`, [ u.id_usuario, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, u })
    })
  })
}

// Realacionar el ciclo
ciclos.relacionarCiclo = ( id, a ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE ciclos SET id_ciclo_relacionado = ?, id_usuario_ultimo_cambio = ?  WHERE id_ciclo = ?`, [ a.id_ciclo_relacionado, a.id_usuario, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, a })
    })
  })
}


module.exports = ciclos;