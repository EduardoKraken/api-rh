const { result }  = require("lodash");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");
const sqlERPNUEVO = require("../db.js");
const sqlERP      = require("../dbErpPrueba.js");

//const constructor
const usuarioserp = function() {};

usuarioserp.getUsuariosERP = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT u.id_usuario, u.usuario, u.nombre_completo, u.nombre_usuario, u.apellido_usuario, u.id_plantel, p.plantel, pe.id_perfil, 
      pe.perfil, f.id,f.nombre_completo AS maestro, u.comentarios,u.password,u.activo_sn, u.ajusta_pagos_sn, u.fecha_alta, u.fecha_baja, u.telefono, u.email, pa.descripcion AS puesto
      FROM usuarios u
      LEFT JOIN planteles p ON u.id_plantel = p.id_plantel
      LEFT JOIN fyn_cat_trabajadores f ON u.id_trabajador = f.id
      LEFT JOIN perfilesusuario pu ON u.id_usuario = pu.id_usuario
      LEFT JOIN perfiles pe ON pu.id_perfil = pe.id_perfil
      LEFT JOIN fyn_cat_puestos pa ON pa.id = f.id_puesto;`,(err, res) => {
        if(err){
          return reject({message: err.sqlMessage })  
        }
        resolve(res)
      })
  })
};

usuarioserp.getPerfilesERP= ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT id_perfil, perfil FROM perfiles 
      WHERE activo_sn = 1;`,(err, res) => {
        if(err){
          return reject({message: err.sqlMessage })  
        }
        resolve(res)
      })
  })
};

usuarioserp.getTrabajadoresERP= ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT id, nombre_completo FROM fyn_cat_trabajadores;`,(err, res) => {
      if(err){
        return reject({message: err.sqlMessage })  
      }
      resolve(res)
    })
  })
};

usuarioserp.getIdUsuarioERP= ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT (MAX(id_usuario) + 1) AS id_agregar FROM usuarios;`,(err, res) => {
      if(err){
        return reject({message: err.sqlMessage })  
      }
      resolve(res)
    })
  })
};

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

usuarioserp.updateUsuario = ( usuario, password, nombre_usuario, apellido_usuario, id_plantel, email, telefono,  activo_sn, comentarios, id_trabajador, id_usuario) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE usuarios
      SET usuario = ?, password = ?, nombre_usuario = ?, apellido_usuario = ?, nombre_completo = CONCAT(nombre_usuario, ' ', apellido_usuario), 
      id_plantel = ?, email = ?, telefono = ?, activo_sn = ?, comentarios = ?, id_trabajador = ?, id_usuario_ultimo_cambio = 1
      WHERE id_usuario = ?;`,[ usuario, password, nombre_usuario, apellido_usuario, id_plantel, email, telefono, activo_sn, comentarios, id_trabajador, id_usuario],(err, res) => {
        if (err) {
          return reject({message:err.sqlMessage})
        }
        if (res.affectedRows == 0) {
          return reject({ message: "not_found" });      
        }  
        resolve( res );
      });
  });
};

usuarioserp.updatePerfilUsuario = ( id_perfil, id_usuario ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE perfilesusuario SET id_perfil = ? WHERE id_usuario = ?;`,[ id_perfil, id_usuario ],(err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      if (res.affectedRows == 0) {
        return reject({ message: "not_found" });      
      }  
      resolve( res );
    });
  });
};

usuarioserp.getPerfilUsuario = ( id_usuario ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM perfilesusuario WHERE id_usuario = ?;`,[ id_usuario ],(err, res) => {
      if (err) {
        return reject({message:err.sqlMessage})
      }
      resolve( res[0] );
    });
  });
};

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

usuarioserp.addUsuarioERP = (u, result) => {
  sqlERP.query(`INSERT INTO usuarios (usuario, password, nombre_usuario, apellido_usuario, nombre_completo, 
    id_plantel, email, telefono, activo_sn, comentarios, id_trabajador, id_usuario_ultimo_cambio) 
    VALUES (?, ?, ?, ?,CONCAT(nombre_usuario, ' ', apellido_usuario),?, ?, ?, ?, ?, ?,1);`,
    [u.usuario, u.password, u.nombre_usuario, u.apellido_usuario, u.id_plantel, u.email, u.telefono, u.activo_sn, u.comentarios, u.id],
    (err, res) => {
      if (err) { result(err, null); return; }
      result(null, { id: res.insertId, ...u });
    });
};


usuarioserp.addPerfilUsuarioERP = (u, result) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO perfilesusuario(id_usuario, id_perfil, id_usuario_ultimo_cambio)
      VALUES (?, ?, 1);`,
      [u.id_usuario, u.id_perfil],
      (err, res) => {
        if (err) {
          return reject({message:err.sqlMessage})
        }
        resolve({ id: res.insertId, ...u });
      });
  })
};

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

usuarioserp.deleteUsuarioERP = (id_usuario, result) => {
  sql.query("DELETE FROM usuarios  WHERE id_usuario = ?;", id, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, res);
  });
};

module.exports = usuarioserp;

//ANGEL RODRIGUEZ -- TODO