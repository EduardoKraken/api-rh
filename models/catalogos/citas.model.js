const { result, functionsIn } = require("lodash");
const sql = require("../db.js");
const moment = require("moment");
//const constructor
const Citas = function (citas) {};

Citas.getCitasEmpleado = (id_empleado, result) => {
  sql.query(
    `SELECT citas.* ,p.nombre as name ,p.telefono as telefono_habitual FROM citas INNER JOIN prospectos as p on p.idprospectos = citas.id_prospecto WHERE citas.id_empleado = ? AND citas.cancel = 0`,
    [id_empleado],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      ///
      var citas = [];

      for (const i in res) {
        let color = res[i].cancel == 0 ? "blue" : "red";

        //declaro el objeto que utilizare para llenar el arreglo
        var datos = {
          tipo_cita: res[i].tipo_cita,
          name: res[i].name,
          start: moment(res[i].fecha_inicio).format("YYYY-MM-DD HH:mm:ss"),
          end: moment(res[i].fecha_fin).format("YYYY-MM-DD HH:mm:ss"),
          color: color,
          timed: false,
          ubicacion: res[i].ubicacion,
          observaciones: res[i].observaciones,
          telefono_contacto: res[i].tel_contacto_esp,
          id_empleado: id_empleado,
          id_prospecto: res[i].id_prospecto,
          fecha_creacion: res[i].fecha_creacion,
          fecha_actualizo: res[i].fecha_actualizo,
          telefono_habitual: res[i].telefono_habitual,
          idcita: res[i].idcitas,
        };

        citas.push(datos);
      }

      result(null, citas);
    }
  );
};

Citas.getCitasProspecto = (id_empleado, result) => {
  sql.query(
    `SELECT citas.* ,p.nombre as name ,p.telefono as telefono_habitual FROM citas INNER JOIN prospectos as p on p.idprospectos = citas.id_prospecto WHERE citas.id_prospecto = ? AND citas.cancel=0 ORDER BY fecha_inicio ASC`,
    [id_empleado],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      ///
      var citas = [];

      for (const i in res) {
        //declaro el objeto que utilizare para llenar el arreglo
        var datos = {
          tipo_cita: res[i].tipo_cita,
          name: res[i].name,
          start: moment(res[i].fecha_inicio).format("YYYY-MM-DD HH:mm:ss"),
          end: moment(res[i].fecha_fin).format("YYYY-MM-DD HH:mm:ss"),
          color: "blue",
          timed: false,
          id_cita: res[i].idcitas,
          ubicacion: res[i].ubicacion,
          observaciones: res[i].observaciones,
          telefono_contacto: res[i].tel_contacto_esp,
          id_empleado: id_empleado,
          id_prospecto: res[i].id_prospecto,
          fecha_creacion: res[i].fecha_creacion,
          fecha_actualizo: res[i].fecha_actualizo,
          telefono_habitual: res[i].telefono_habitual,
        };

        citas.push(datos);
      }
      result(null, citas);
    }
  );
};

Citas.getCita = (id, result) => {
  sql.query(
    `SELECT citas.*, p.nombre as name ,p.telefono as telefono_habitual FROM citas INNER JOIN prospectos as p on p.idprospectos = citas.id_prospecto WHERE citas.deleted = 0 AND citas.idcitas=?`,
    [id],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }
      citas = [];
      for (const i in res) {
        //declaro el objeto que utilizare para llenar el arreglo
        var datos = {
          tipo_cita: res[i].tipo_cita,
          name: res[i].name,
          start: moment(res[i].fecha_inicio).format("YYYY-MM-DD HH:mm:ss"),
          end: moment(res[i].fecha_fin).format("YYYY-MM-DD HH:mm:ss"),
          color: "blue",
          timed: false,
          ubicacion: res[i].ubicacion,
          observaciones: res[i].observaciones,
          telefono_contacto: res[i].tel_contacto_esp,
          id_empleado: id,
          id_prospecto: res[i].id_prospecto,
          fecha_creacion: res[i].fecha_creacion,
          fecha_actualizo: res[i].fecha_actualizo,
          telefono_habitual: res[i].telefono_habitual,
        };

        citas.push(datos);
      }
      result(null, citas);
    }
  );
};

Citas.addCita = (c, result) => {
  sql.query(
    `INSERT INTO citas(tipo_cita,fecha_inicio,fecha_fin,ubicacion,observaciones,tel_contacto_esp,id_empleado,id_prospecto)VALUES(?,?,?,?,?,?,?,?)`,
    [
      c.tipo_cita,
      c.fecha_inicio,
      c.fecha_fin,
      c.ubicacion,
      c.observaciones,
      c.telefono_contacto,
      c.id_empleado,
      c.id_prospecto,
    ],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      result(null, { id: res.insertId, ...c });
    }
  );
};

Citas.updateCita = (id, cita, result) => {
  sql.query(
    `UPDATE citas SET tipo_cita=?, fecha_inicio=?, fecha_fin=?, ubicacion=?, observaciones=?, tel_contacto_esp=?,fecha_actualizo=now() WHERE idcitas=?`,
    [
      cita.tipo_cita,
      cita.fecha_inicio,
      cita.fecha_fin,
      cita.ubicacion,
      cita.observaciones,
      cita.telefono_contacto,
      id,
    ],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...cita });
    }
  );
};

Citas.cancelCita = (id, result) => {
  sql.query(`UPDATE citas SET cancel=1 WHERE idcitas=?`, [id], (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
    if (res.affectedRows == 0) {
      result({ message: "No se encontro la cita" }, null);
      return;
    }

    result(null, { id: id });
  });
};

module.exports = Citas;
