const { result }  = require("lodash");
// Original
const sqlERP      = require("../db3.js");

// Prueba
// const sqlERP      = require("../dbErpPrueba.js");

//const constructor
const cursoserp = function(cursoserp) {};

// Consultar todos los cursoserp
cursoserp.getCursoserp = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM cursos;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

// consultar solo los cursoserp activos
cursoserp.getCursoserpActivos = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM cursos WHERE activo_sn = 1 ;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

// Agregar un salon+
cursoserp.addCursoErp = ( pu ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO cursos(curso,precio_total,numero_niveles,precio_nivel,comentarios,id_usuario_ultimo_cambio)VALUES(?,?,?,?,?,?)`, 
    [pu.curso, pu.precio_total, pu.numero_niveles, pu.precio_nivel, pu.comentarios, pu.id_usuario],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id: res.insertId, ...pu })
    })
  })
}

// Actualizar el salon en general
cursoserp.updateCursoErp = ( id, a ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE cursos SET curso = ?, precio_total = ?, numero_niveles = ?, precio_nivel = ?, comentarios = ?, id_usuario_ultimo_cambio = ?, activo_sn = ? WHERE id_curso = ?`, 
    	[ a.curso, a.precio_total, a.numero_niveles, a.precio_nivel, a.comentarios, a.id_usuario, a.activo_sn, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, a })
    })
  })
}

// Eliminar el salon
cursoserp.deleteCursoErp = ( id, u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE cursos SET activo_sn = 0, fecha_baja = NOW(), id_usuario_ultimo_cambio = ? WHERE id_curso = ?`, [ u.id_usuario, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, u })
    })
  })
}


module.exports = cursoserp;