const { result } = require("lodash");
const sql = require("../db.js");


//const constructor
const Notificaciones = function(notificaciones) {};

Notificaciones.getNotificaciones = result => {
    sql.query(`SELECT * FROM notificaciones WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

Notificaciones.addNotificacion = (c, result) => {
    sql.query(`INSERT INTO notificaciones(titulo,notificacion,tipo_notificacion,general,departamento,curso,empleado,capacitador,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?,?,?,?,?,?,?,?)`, [c.titulo, c.notificacion, c.tipo_notificacion, c.general, c.departamento, c.curso, c.empleado, c.capacitador, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo],
        (err, res) => {
            if (err) { result(err, null); return; }
            result(null, { id: res.insertId, ...c });
        }
    )
}

Notificaciones.updateNotificacion = (id, c, result) => {
    sql.query(` UPDATE notificaciones SET titulo=?,notificacion=?,tipo_notificacion=?,general=?,departamento=?,curso=?,empleado=?,capacitador=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idnotificacion=?`, [c.titulo, c.notificacion, c.tipo_notificacion, c.general, c.departamento, c.curso, c.empleado, c.capacitador, c.usuario_registro, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            result(null, { id: id, ...c });
        })
}



module.exports = Notificaciones;