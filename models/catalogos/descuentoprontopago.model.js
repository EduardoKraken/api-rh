const { result }  = require("lodash");
// Original
const sqlERP      = require("../db3.js");

// Prueba
// const sqlERP      = require("../dbErpPrueba.js");

//const constructor
const descProntoPago = function(descProntoPago) {};

// consultar solo los grupos activos
descProntoPago.getDescuentoProntoPagoGrupo = ( id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT d.*, t.tipo_descuento FROM descuentosprontopago d
			LEFT JOIN tiposdescuento t ON t.id_tipo_descuento = d.id_tipo_descuento WHERE d.id_grupo = ? AND d.activo_sn = 1;`,[id_grupo],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}


// Agregar un salon+
descProntoPago.addDescuentoGrupo = ( pu ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO descuentosprontopago(id_grupo,descuento_pronto_pago,id_tipo_descuento,valor_descuento,fecha_inicio_descuento,fecha_fin_descuento,id_usuario_ultimo_cambio,fecha_limite_descuento,comentarios)
    VALUES(?,?,?,?,?,?,?,?,?)`, 
    [pu.id_grupo,pu.descuento_pronto_pago,pu.id_tipo_descuento,pu.valor_descuento,pu.fecha_inicio_descuento,pu.fecha_fin_descuento,pu.id_usuario_ultimo_cambio,pu.fecha_limite_descuento,pu.comentarios],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id: res.insertId, ...pu })
    })
  })
}


// Eliminar el salon
descProntoPago.deleteDescuentoGrupo = ( id, u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE descuentosprontopago SET activo_sn = 0, fecha_baja = NOW(), id_usuario_ultimo_cambio = ? WHERE id_descuento_pronto_pago = ?`, [ u.id_usuario, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, u })
    })
  })
}


module.exports = descProntoPago;