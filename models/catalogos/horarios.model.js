const { result }  = require("lodash");
// Original
// const sqlERP      = require("../db3.js");

// Prueba
const sqlERP      = require("../dbErpPrueba.js");

//const constructor
const horarios = function(horarios) {};

// Consultar todos los horarios
horarios.getHorarios = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM horarios;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

// consultar solo los horarios activos
horarios.getHorariosActivos = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM horarios WHERE activo_sn = 1 ;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

// Agregar un ciclo+
horarios.addHorario = ( pu ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO horarios(horario,hora_inicio,hora_fin,comentarios,id_usuario_ultimo_cambio)VALUES(?,?,?,?,?)`, 
    [pu.horario, pu.hora_inicio, pu.hora_fin, pu.comentarios, pu.id_usuario],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id: res.insertId, ...pu })
    })
  })
}

// Actualizar el ciclo en general
horarios.updateHorario = ( id, a ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE horarios SET id_usuario_ultimo_cambio = ?, activo_sn = ?, comentarios = ?  WHERE id_horario = ?`, [ a.id_usuario, a.activo_sn, a.comentarios, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, a })
    })
  })
}

// Eliminar el ciclo
horarios.deleteHorario = ( id, u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE horarios SET activo_sn = 0, fecha_baja = NOW(), id_usuario_ultimo_cambio = ? WHERE id_horario = ?`, [ u.id_usuario, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, u })
    })
  })
}


module.exports = horarios;