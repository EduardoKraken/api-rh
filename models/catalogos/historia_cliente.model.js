const { result } = require("lodash");
const sql = require("../db.js");

//const constructor
const historialCliente = function (historialCliente) {};

Materiales.getMateriales = (result) => {
  sql.query(`SELECT * FROM materiales WHERE deleted = 0`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Materiales.addMaterial = (c, result) => {
  sql.query(
    `INSERT INTO materiales(idtipo_material,url,idsubtema,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?,?,?)`,
    [
      c.idtipo_material,
      c.url,
      c.idsubtema,
      c.usuario_registro,
      c.fecha_creacion,
      c.fecha_actualizo,
    ],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      result(null, { id: res.insertId, ...c });
    }
  );
};

Materiales.updateMaterial = (id, c, result) => {
  sql.query(
    ` UPDATE materiales SET idtipo_material=?,url=?,idsubtema=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idmaterial=?`,
    [
      c.idtipo_material,
      c.url,
      c.idsubtema,
      c.usuario_registro,
      c.fecha_actualizo,
      c.deleted,
      id,
    ],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...c });
    }
  );
};

Materiales.getHistoriaCliente = (id, result) => {
  sql.query(
    `SELECT * from historial_clientes WHERE id_clientes=?`,
    [id],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }
      result(null, res);
    }
  );
};

module.exports = historialCliente;
