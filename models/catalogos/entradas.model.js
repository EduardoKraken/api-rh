const { result }  = require("lodash");
// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");

const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");

//const constructor
const entradas = (entradas) => {};

entradas.getEntradasHoy = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT ut.id_trabajador, u.id_usuario, u.nombre_completo, u.id_plantel, e.hora_registro
    	FROM usuario_trabajador ut
			LEFT JOIN usuarios u ON u.id_usuario = ut.id_usuario
			LEFT JOIN fyn_proc_entradas_salidas e ON e.id_trabajador = ut.id_trabajador
			WHERE DATE(e.fecha_registro) = CURRENT_DATE
			GROUP BY ut.id_trabajador;`, (err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};

entradas.getUsuariosERP = ( idUsuarios ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT u.id_usuario, u.nombre_completo, p.plantel, IF(LOCATE('FAST',p.plantel)>0,2,1) AS escuela, email FROM usuarios u
			LEFT JOIN planteles p ON p.id_plantel = u.id_plantel 
			WHERE id_usuario IN ( ? );`,[ idUsuarios ],(err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};



entradas.getHorariosUsuario = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT *, TIMESTAMPDIFF(MINUTE,hora_inicio,CURRENT_TIME) as diferencia 
			FROM horarios_apertura
			WHERE dia = (WEEKDAY(CURRENT_DATE) + 1)
			ORDER BY hora_inicio;`,(err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};


entradas.getHorariosApertura = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT  *, (ELT(dia + 1,'domingo' ,'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado','Domingo')) AS diaNombre FROM horarios_apertura WHERE deleted = 0;`,(err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};

entradas.getUsuariosHorarios = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT t.*, u.nombre_usuario, u.apellido_usuario, u.nombre_completo, u.id_plantel, p.plantel, u.id_plantel_oficial,  p.plantel AS plantel_oficial FROM usuario_trabajador t
      LEFT JOIN usuarios u ON u.id_usuario = t.id_usuario
      LEFT JOIN planteles p ON p.id_plantel = u.id_plantel
      LEFT JOIN planteles p2 ON p2.id_plantel = u.id_plantel_oficial
      WHERE u.activo_sn = 1 AND t.deleted = 0
      ORDER BY u.nombre_completo;`, (err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};


entradas.getTrabajadores = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM fyn_cat_trabajadores WHERE activo_sn = 1  ORDER BY nombre_completo;`, (err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};


entradas.crearRelacionUsuarios = ( id_usuario, id_trabajador ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO usuario_trabajador ( id_usuario, id_trabajador ) VALUES ( ?, ? );`, [ id_usuario, id_trabajador ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve({ id: res.insertId })
    })
  });
};

entradas.existeHorario = ( id_usuario, valor ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM horarios_apertura WHERE iderp = ? AND dia = ? AND deleted = 0;`,[ id_usuario, valor ],(err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res[0] );
    });
  })
};


entradas.agregarHorarios = ( id_usuario, valor, hora ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`INSERT INTO horarios_apertura ( iderp, dia, hora_inicio ) VALUES ( ?, ?, ? );`, [ id_usuario, valor, hora ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve({ id: res.insertId })
    })
  });
};


entradas.eliminarHorarioApertura = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE horarios_apertura SET deleted = 1  WHERE idhorarios_apertura = ?;`,[ id ],
      (err, res) => {
      if (err) {
        return reject({message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve({ id });
    });
  });
};


entradas.updateHorarioApertura = ( id_usuario, valor, hora ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE horarios_apertura SET  hora_inicio = ?  WHERE idhorarios_apertura > 0 AND iderp = ? AND dia = ?`,[ hora, id_usuario, valor ],
      (err, res) => {
      if (err) {
        return reject({message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({message: 'Actualización no completada, no se encontro el registro' });
      }

      resolve({  valor, hora, id_usuario  });
    });
  });
};

entradas.eliminarUsuarioApertura = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE usuario_trabajador SET deleted = 1  WHERE idusuario_trabajador > 0 AND id_usuario = ?`,[ id ],
      (err, res) => {
      if (err) {
        return reject({message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve({ id });
    });
  });
};

entradas.desactivarUsuario = ( id, estatus ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE usuario_trabajador SET activo = ?  WHERE idusuario_trabajador > 0 AND id_usuario = ?`,[ estatus, id ],
      (err, res) => {
      if (err) {
        return reject({message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve({ id });
    });
  });
};


entradas.actualizarSucursalApertura = ( id_plantel_oficial, id_usuario ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE usuarios SET id_plantel_oficial = ?  WHERE id_usuario = ?`,[ id_plantel_oficial, id_usuario ],
      (err, res) => {
      if (err) {
        return reject({message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve({ id_usuario });
    });
  });
};

entradas.getPlantelesApertura = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM plantel WHERE idplantel > 0;`,(err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};

entradas.getPersonalApertura = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM usuarios WHERE idpuesto IN (18,19) AND deleted = 0;`,(err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};

entradas.getUsuariosTrabajadores = ( id_usuarios ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT u.id_usuario, u.nombre_completo, u.id_plantel_oficial AS id_plantel, ut.id_trabajador, u.nombre_usuario, IF(LOCATE('FAST',p.plantel)>0,2,1) AS escuela
        FROM usuarios u 
        LEFT JOIN usuario_trabajador ut ON ut.id_usuario = u.id_usuario 
        LEFT JOIN planteles p ON p.id_plantel = u.id_plantel_oficial
        WHERE u.id_usuario IN (?) AND u.id_plantel_oficial IS NOT NULL AND ut.deleted = 0 AND ut.activo = 1;`,[ id_usuarios ],(err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};

entradas.getContadorChecadas = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT  u.id_usuario, HOUR(e.hora_registro) AS hora, hora_registro
        FROM usuario_trabajador ut
        LEFT JOIN usuarios u ON u.id_usuario = ut.id_usuario
        LEFT JOIN fyn_proc_entradas_salidas e ON e.id_trabajador = ut.id_trabajador
        WHERE DATE(e.fecha_registro) = CURRENT_DATE
        GROUP BY HOUR(e.hora_registro), ut.id_trabajador;`,(err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};

entradas.clasesAcitvas = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise(( resolve, reject )=>{
    db.query(`SELECT g.id AS id_grupo,c.iderp AS id_ciclo, g.nombre AS grupo, c.nombre AS curso, h.hora_inicio, g.id_unidad_negocio,
      IF((ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")),CONCAT(u2.nombre, " ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno, ""))) AS teacher,
      IF((ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,u.iderp,u2.iderp) AS id_teacher,
      TIMESTAMPDIFF(MINUTE, TIME(h.hora_inicio), CURRENT_TIME) AS tiempo_diferencia
      FROM grupos g
      LEFT JOIN cursos cu         ON cu.id = g.id_curso
      LEFT JOIN frecuencia f      ON f.id = cu.id_frecuencia
      LEFT JOIN horarios h        ON h.id = g.id_horario
      LEFT JOIN ciclos c          ON c.id = g.id_ciclo
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
      LEFT JOIN usuarios u        ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2       ON u2.id = gt.id_teacher_2
      WHERE (ELT(DAYOFWEEK(CURRENT_DATE),domingo,lunes,martes,miercoles,jueves,viernes,sabado)) = 1
      AND g.editado = 1 
      AND g.optimizado = 0 
      AND g.deleted = 0 
      AND g.nombre NOT LIKE '%certif%' 
      AND g.nombre NOT LIKE '%indu%'
      AND g.nombre NOT LIKE '%exci%' 
      AND c.id = ( SELECT id FROM ciclos WHERE CURRENT_DATE BETWEEN fecha_inicio AND fecha_fin AND deleted = 0 AND nombre LIKE '%CICLO%')
      AND g.optimizado = 0
      ORDER BY h.hora_inicio;`,(err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};


entradas.getCodigosGenerados = ( escuela ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM codigos_asistencia WHERE unidad_negocio = ? AND DATE(fecha_creacion) = CURRENT_DATE AND estatus = 1;`,[ escuela ],(err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};

entradas.clasesPorIniciar = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise(( resolve, reject )=>{
    db.query(`SELECT g.id AS id_grupo,c.iderp AS id_ciclo, g.nombre AS grupo, c.nombre AS curso, h.hora_inicio, g.id_unidad_negocio,
      IF((ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")),CONCAT(u2.nombre, " ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno, ""))) AS teacher,
      IF((ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,u.iderp,u2.iderp) AS iderp,
      TIMESTAMPDIFF(MINUTE, TIME(h.hora_inicio), CURRENT_TIME) AS tiempo_diferencia
      FROM grupos g
      LEFT JOIN cursos cu         ON cu.id = g.id_curso
      LEFT JOIN frecuencia f      ON f.id = cu.id_frecuencia
      LEFT JOIN horarios h        ON h.id = g.id_horario
      LEFT JOIN ciclos c          ON c.id = g.id_ciclo
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
      LEFT JOIN usuarios u        ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2       ON u2.id = gt.id_teacher_2
      WHERE (ELT(DAYOFWEEK(CURRENT_DATE),domingo,lunes,martes,miercoles,jueves,viernes,sabado)) = 1
      AND g.editado = 1 
      AND g.optimizado = 0 
      AND g.deleted = 0 
      AND g.nombre NOT LIKE '%certif%' 
      AND g.nombre NOT LIKE '%indu%'
      AND g.nombre NOT LIKE '%exci%' 
      AND c.id = ( SELECT id FROM ciclos WHERE CURRENT_DATE BETWEEN fecha_inicio AND fecha_fin AND deleted = 0 AND nombre LIKE '%CICLO%')
      AND g.optimizado = 0
      AND TIMESTAMPDIFF(MINUTE, TIME(h.hora_inicio), CURRENT_TIME) BETWEEN -15 AND 20
      ORDER BY h.hora_inicio;`,(err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};

entradas.getEntradasHoyTeachers = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT u.id_usuario, u.nombre_completo, u.id_trabajador, e.hora_registro, e.notificacion FROM usuarios u
      LEFT JOIN perfilesusuario p ON p.id_usuario = u.id_usuario
      LEFT JOIN fyn_proc_entradas_salidas e ON e.id_trabajador = u.id_trabajador
      WHERE u.activo_sn = 1 AND DATE(e.fecha_registro) = CURRENT_DATE;`,(err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};

entradas.getEntradasAlumnos = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT e.id, e.id_alumno, e.hora_registro, a.nombre, a.apellido_paterno, a.apellido_materno FROM fyn_proc_entradas_salidas e
      LEFT JOIN alumnos a ON a.id_alumno = e.id_alumno
      WHERE DATE(e.fecha_registro) = CURRENT_DATE
      AND e.id_alumno
      AND e.id_grupo IS NULL;`,(err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};

entradas.alumnoClaseHoy = ( escuela, id_alumno ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise(( resolve, reject )=>{
    db.query(`SELECT u.iderp, u.id, ga.id_grupo, g.nombre, u2.id AS id_teacher FROM grupo_alumnos ga 
        LEFT JOIN grupos g ON g.id = ga.id_grupo
        LEFT JOIN usuarios u ON u.id = ga.id_alumno
        LEFT JOIN cursos cu ON cu.id = g.id_curso
        LEFT JOIN frecuencia f ON f.id = cu.id_frecuencia
        LEFT JOIN horarios h ON h.id = g.id_horario
        LEFT JOIN ciclos c ON c.id = g.id_ciclo
        LEFT JOIN grupo_teachers gt ON gt.id_grupo = ga.id_grupo
        LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher
        WHERE u.iderp = ?
        AND CURRENT_DATE BETWEEN c.fecha_inicio AND c.fecha_fin
        AND (ELT(DAYOFWEEK(CURRENT_DATE),domingo,lunes,martes,miercoles,jueves,viernes,sabado)) = 1;`,[id_alumno],(err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res[0] );
    });
  })
};


entradas.existeAsistencia = ( escuela, id, id_grupo ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise(( resolve, reject )=>{
    db.query(`SELECT * FROM asistencia_grupo WHERE id_usuario = ? AND id_grupo = ? AND DATE(fecha_asistencia) = CURRENT_DATE;`,[ id, id_grupo ],(err, res) => {
      if (err) { return reject({ message : err.sqlMessage }) }
      // Retornamos solo un dato
      return resolve( res[0] );
    });
  })
};


entradas.addAsistencia = ( escuela, id_grupo, id_alumno, id_teacher ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`INSERT INTO asistencia_grupo( id_grupo, id_usuario, id_teacher, numero_de_semana, fecha_asistencia, valor_asistencia )
      VALUES(?,?,?,WEEKOFYEAR(NOW()),CURRENT_DATE,1)`,[ id_grupo, id_alumno, id_teacher ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId })
    })
  })
}

entradas.updateAsistenciaERP = ( id, id_grupo ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE fyn_proc_entradas_salidas SET id_grupo = ?  WHERE id =  ?`,[ id_grupo, id ],
      (err, res) => {
      if (err) {
        return reject({message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve({ id });
    });
  });
};


module.exports = entradas;