const { result }  = require("lodash");
// Original
// const sqlERP      = require("../db3.js");

// Prueba
const sqlERP      = require("../dbErpPrueba.js");

//const constructor
const salones = function(salones) {};

// Consultar todos los salones
salones.getSalones = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM salones;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

// consultar solo los salones activos
salones.getSalonesActivos = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM salones WHERE activo_sn = 1 ;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

// Agregar un salon+
salones.addSalon = ( pu ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO salones(salon,comentarios,id_usuario_ultimo_cambio,capacidad)VALUES(?,?,?,?)`, 
    [pu.salon, pu.comentarios, pu.id_usuario, pu.capacidad],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id: res.insertId, ...pu })
    })
  })
}

// Actualizar el salon en general
salones.updateSalon = ( id, a ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE salones SET id_usuario_ultimo_cambio = ?, activo_sn = ?, comentarios = ?, capacidad = ?  WHERE id_salon = ?`,
      [ a.id_usuario, a.activo_sn, a.comentarios, a.capacidad, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, a })
    })
  })
}

// Eliminar el salon
salones.deleteSalon = ( id, u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE salones SET activo_sn = 0, fecha_baja = NOW(), id_usuario_ultimo_cambio = ? WHERE id_salon = ?`, [ u.id_usuario, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, u })
    })
  })
}


module.exports = salones;