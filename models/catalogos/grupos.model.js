const { result }  = require("lodash");
// Original
// const sqlERP      = require("../db3.js");

// Prueba
const sqlERP      = require("../dbErpPrueba.js");

//const constructor
const grupos = function(grupos) {};

// Consultar todos los grupos
grupos.getGrupos = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT g.id_grupo, g.grupo, g.fecha_alta, g.precio, g.id_plantel, g.id_curso, g.id_ciclo, g.id_horario, g.id_salon,
      g.id_nivel, g.comentarios, g.activo_sn, g.fecha_baja, g.fecha_ultimo_cambio, g.id_grupo_consecutivo,
      s.capacidad AS cupo, n.nivel, c.curso, p.plantel, ci.ciclo, h.horario, s.salon, IF(LOCATE('FAST',p.plantel),2,1) AS escuela FROM grupos g
			LEFT JOIN niveles n ON n.id_nivel = g.id_nivel
			LEFT JOIN cursos c ON c.id_curso = g.id_curso
			LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
			LEFT JOIN ciclos ci ON ci.id_ciclo = g.id_ciclo
			LEFT JOIN horarios h ON h.id_horario = g.id_horario
			LEFT JOIN salones s ON s.id_salon = g.id_salon;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

// consultar solo los grupos activos
grupos.getGruposActivos = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM grupos WHERE activo_sn = 1 ;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

// consultar solo los grupos activos
grupos.getGruposAlumnos = ( id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN alumnos al ON al.id_alumno = a.id_alumno WHERE a.id_grupo = ?;`,[id_grupo],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

grupos.getGrupoTeachers = ( id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT u.nombre_completo, u.id_trabajador, m.numero_teacher FROM maestrosgrupo m
      LEFT JOIN usuarios u ON u.id_trabajador = m.id_maestro
      WHERE m.numero_teacher IN (1,2) AND m.activo_sn = 1 AND id_grupo = ?;`,[id_grupo],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

grupos.getIdMaxGrupo = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT (max(id_grupo) + 1) AS id_grupo FROM grupos;`, (err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res[0])
    })
  })
}


// Agregar un salon+
grupos.addGrupo = ( pu ,id_grupo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO grupos(id_grupo, grupo,id_plantel,id_curso,id_ciclo,id_horario,id_salon,id_nivel,cupo,precio,disponibles,comentarios,id_usuario_ultimo_cambio)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)`, 
    [id_grupo, pu.grupo,pu.id_plantel,pu.id_curso,pu.id_ciclo,pu.id_horario,pu.id_salon,pu.id_nivel,pu.cupo,pu.precio,pu.disponibles,pu.comentarios, pu.id_usuario],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id: res.insertId, ...pu })
    })
  })
}

// Actualizar el salon en general
grupos.updateGrupo = ( id, a ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE grupos SET id_plantel = ?, id_curso = ?, id_ciclo = ?, id_horario = ?, id_salon = ?, id_nivel = ?, comentarios = ?, id_usuario_ultimo_cambio = ?, activo_sn = ? WHERE id_grupo = ?`, 
    	[ a.id_plantel, a.id_curso, a.id_ciclo, a.id_horario, a.id_salon, a.id_nivel, a.comentarios, a.id_usuario, a.activo_sn, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, a })
    })
  })
}

// Eliminar el salon
grupos.deleteGrupo = ( id, u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE grupos SET activo_sn = 0, fecha_baja = NOW(), id_usuario_ultimo_cambio = ? WHERE id_grupo = ?`, [ u.id_usuario, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, u })
    })
  })
}


// Actualizar el salon en general
grupos.updateGrupoIdConsecutivo = ( id_grupo_consecutivo, id_usuario, id_grupo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE grupos SET id_grupo_consecutivo = ?, id_usuario_ultimo_cambio = ? WHERE id_grupo = ?`, 
      [ id_grupo_consecutivo, id_usuario, id_grupo ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id_grupo_consecutivo, id_usuario, id_grupo })
    })
  })
}


// consultar solo los grupos activos
grupos.getGruposHabiles = ( escuela ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT g.id_grupo, g.grupo, p.plantel, n.nivel, h.horario, s.capacidad, c.fecha_fin_ciclo, c.ciclo, c.id_ciclo,
      (s.capacidad - IFNULL(cu.inscritos,0)) AS disponibles, IFNULL(cu.inscritos,0) AS inscritos
      FROM grupos g 
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN horarios h ON h.id_horario = g.id_horario
      LEFT JOIN niveles n ON n.id_nivel = g.id_nivel
      LEFT JOIN salones s ON s.id_salon = g.id_salon
      LEFT JOIN (SELECT COUNT(*) AS inscritos, id_grupo FROM gruposalumnos GROUP BY id_grupo) cu ON cu.id_grupo = g.id_grupo
      WHERE DATE( c.fecha_fin_ciclo ) >= CURRENT_DATE AND IF(LOCATE('FAST', p.plantel) > 0, 2, 1 ) = ?
      ORDER BY p.plantel, c.fecha_inicio_ciclo, n.id_nivel, h.hora_inicio;`,[ escuela ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

module.exports = grupos;