const { result } = require("lodash");
const sql = require("../db.js");
const sqlErp = require("../db3.js");
//const constructor
const Prospectos = function (prospectos) {};

Prospectos.getProspectos = (result) => {
  sql.query(
    `SELECT prospectos.*, usuarios.iderp FROM prospectos left join usuarios on usuarios.idusuario = prospectos.id_empleado WHERE prospectos.deleted = 0 AND prospectos.estatus <> 'Inscrito' ORDER BY prospectos.fecha_creacion DESC;`,
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      ///
      var prospectos = [];

      for (const i in res) {
        //declaro el objeto que utilizare para llenar el arreglo
        var datos = {
          idProspecto: res[i].idprospectos,
          nombre: res[i].nombre,
          telefono: res[i].telefono,
          email: res[i].email,
          source: res[i].source,
          fecha_creacion: res[i].fecha_creacion,
          fecha_actualizo: res[i].fecha_actualizo,
          fecha_actualizo: res[i].fecha_actualizo,
          status: res[i].estatus,
          deleted: res[i].deleted,
          iderp: res[i].iderp,
        };

        prospectos.push(datos);
      }

      sqlErp.query(
        `SELECT * FROM usuarios WHERE activo_sn = 1 ORDER BY nombre_completo`,
        (err, res) => {
          if (err) {
            result(null, err);
            return;
          }

          for (const i in prospectos) {
            for (const j in res) {
              if (prospectos[i].iderp == res[j].id_usuario) {
                prospectos[i].nombreEmpleado = res[j].nombre_completo;
              }
            }
          }

          result(null, prospectos);
          return;
        }
      );

    }
  );
};

Prospectos.getProspectosId = (id, result) => {
  sql.query(
    `SELECT prospectos.*, usuarios.iderp FROM prospectos left join usuarios on usuarios.idusuario = prospectos.id_empleado WHERE prospectos.deleted = 0 AND prospectos.id_empleado = ? AND prospectos.estatus <> 'Inscrito' ORDER BY prospectos.fecha_creacion DESC;`,
    [id],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      ///
      var prospectos = [];

      for (const i in res) {
        //declaro el objeto que utilizare para llenar el arreglo
        var datos = {
          idProspecto: res[i].idprospectos,
          nombre: res[i].nombre,
          telefono: res[i].telefono,
          email: res[i].email,
          source: res[i].source,
          fecha_creacion: res[i].fecha_creacion,
          fecha_actualizo: res[i].fecha_actualizo,
          fecha_actualizo: res[i].fecha_actualizo,
          status: res[i].estatus,
          deleted: res[i].deleted,
          iderp: res[i].iderp,
        };

        prospectos.push(datos);
      }

      sqlErp.query(
        `SELECT * FROM usuarios WHERE activo_sn = 1 ORDER BY nombre_completo`,
        (err, res) => {
          if (err) {
            result(null, err);
            return;
          }

          for (const i in prospectos) {
            for (const j in res) {
              if (prospectos[i].iderp == res[j].id_usuario) {
                prospectos[i].nombreEmpleado = res[j].nombre_completo;
              }
            }
          }

          result(null, prospectos);
          return;
        }
      );

    }
  );
};

Prospectos.getClientes = (result) => {
  sql.query(
    `SELECT prospectos.*, usuarios.iderp FROM prospectos left join usuarios on usuarios.idusuario = prospectos.id_empleado WHERE prospectos.deleted = 0 AND prospectos.estatus = 'Inscrito' ORDER BY prospectos.fecha_creacion DESC `,
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      ///
      var prospectos = [];

      for (const i in res) {
        //declaro el objeto que utilizare para llenar el arreglo
        var datos = {
          idProspecto: res[i].idprospectos,
          nombre: res[i].nombre,
          telefono: res[i].telefono,
          email: res[i].email,
          source: res[i].source,
          fecha_creacion: res[i].fecha_creacion,
          fecha_actualizo: res[i].fecha_actualizo,
          fecha_actualizo: res[i].fecha_actualizo,
          status: res[i].estatus,
          deleted: res[i].deleted,
          iderp: res[i].iderp,
        };

        prospectos.push(datos);
      }

      sqlErp.query(
        `SELECT * FROM usuarios WHERE activo_sn = 1 ORDER BY nombre_completo`,
        (err, res) => {
          if (err) {
            result(null, err);
            return;
          }

          for (const i in prospectos) {
            for (const j in res) {
              if (prospectos[i].iderp == res[j].id_usuario) {
                prospectos[i].nombreEmpleado = res[j].nombre_completo;
              }
            }
          }

          result(null, prospectos);
          return;
        }
      );

    }
  );
};

Prospectos.getProspecto = (id, result) => {
  sql.query(
    `SELECT * FROM prospectos WHERE deleted = 0 AND idprospectos=?`,
    [id],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }
      result(null, res);
    }
  );
};

Prospectos.addProspectos = (p, result) => {
  sql.query(
    `INSERT INTO prospectos(nombre,telefono,email,estatus,id_empleado,source)VALUES(?,?,?,?,?,?)`,
    [p.nombre, p.telefono, p.email, p.estatus, p.id_empleado, p.source],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      result(null, { id: res.insertId, ...p });
    }
  );
};

Prospectos.updateProspecto = (id, prospecto, result) => {
  sql.query(
    `UPDATE prospectos SET nombre=?,telefono=?, email=?, id_empleado=?, estatus=?, source=? WHERE idprospectos=?`,
    [
      prospecto.nombre,
      prospecto.telefono,
      prospecto.email,
      prospecto.id_empleado,
      prospecto.estatus,
      prospecto.source,
      id,
    ],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...prospecto });
    }
  );
};

Prospectos.deleteProspecto = (id, result) => {
  sql.query(
    `UPDATE prospectos SET deleted=1 WHERE idprospectos=?`,
    [id],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      if (res.affectedRows == 0) {
        result({ message: "No se encontro el prospectos" }, null);
        return;
      }

      result(null, { id: id });
    }
  );
};

Prospectos.asignarProspecto = (id_prospecto, id_empleado) => {
  return new Promise((resolve, reject) => {
    sql.query(
      "UPDATE prospectos SET id_empleado=? WHERE idprospectos=?",
      [id_empleado, id_prospecto],
      (err, res) => {
        if (err) {
          reject(err);
          return;
        } else if (res.affectedRows == 0) {
          reject({ message: "No existe ese prospecto" });
        }

        resolve(res.affectedRows);
      }
    );
  });
};

Prospectos.editarEstatusProspecto = (estatus, id_prospecto) => {
  return new Promise((resolve, reject) => {
    sql.query(
      "UPDATE prospectos SET estatus=? WHERE idprospectos=?",
      [estatus, id_prospecto],
      (err, res) => {
        if (err) {
          reject(err);
          return;
        } else if (res.affectedRows == 0) {
          reject({ message: "No existe ese prospecto" });
        }

        resolve(res.affectedRows);
      }
    );
  });
};

module.exports = Prospectos;
