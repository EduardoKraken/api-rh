const { result }  = require("lodash");
// Original
// const sqlERP      = require("../db3.js");

// Prueba
const sqlERP      = require("../dbErpPrueba.js");
const sqlERPNUEVO = require("../db.js");


//const constructor
const planteles = function(planteles) {};

// Consultar todos los planteles
planteles.getplanteles = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT *, n.nivel, c.curso, p.plantel, ci.ciclo, h.horario, s.salon FROM planteles g
			LEFT JOIN niveles n ON n.id_nivel = g.id_nivel
			LEFT JOIN cursos c ON c.id_curso = g.id_curso
			LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
			LEFT JOIN ciclos ci ON ci.id_ciclo = g.id_ciclo
			LEFT JOIN horarios h ON h.id_horario = g.id_horario
			LEFT JOIN salones s ON s.id_salon = g.id_salon;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

// consultar solo los planteles activos
planteles.getPlantelesActivos = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM planteles WHERE activo_sn = 1 ;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

planteles.getPlantelesOficiales = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM plantel;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

// Agregar un salon+
planteles.addGrupo = ( pu ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO planteles(forma_pago,comentarios,id_usuario_ultimo_cambio)VALUES(?,?,?)`, 
    [pu.forma_pago, pu.comentarios, pu.id_usuario],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id: res.insertId, ...pu })
    })
  })
}

// Actualizar el salon en general
planteles.updateGrupo = ( id, a ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE planteles SET forma_pago = ?, comentarios = ?, id_usuario_ultimo_cambio = ?, activo_sn = ? WHERE id_forma_pago = ?`, 
    	[ a.forma_pago, a.comentarios, a.id_usuario, a.activo_sn, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, a })
    })
  })
}

// Eliminar el salon
planteles.deleteGrupo = ( id, u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE planteles SET activo_sn = 0, fecha_baja = NOW(), id_usuario_ultimo_cambio = ? WHERE id_forma_pago = ?`, [ u.id_usuario, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, u })
    })
  })
}


module.exports = planteles;