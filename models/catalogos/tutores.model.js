const { result } = require("lodash");
const sql = require("../db.js");

//const constructor
const Tutores = function (tutores) {};

Tutores.getTutores = (result) => {
  sql.query(`SELECT * FROM tutores WHERE deleted = 0`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    ///
    var tutores = [];

    for (const i in res) {
      //declaro el objeto que utilizare para llenar el arreglo
      var datos = {
        idTutor: res[i].idtutores,
        nombre: res[i].nombre,
        apellido_paterno: res[i].apellido_paterno,
        apellido_materno: res[i].apellido_materno,
        telefono: res[i].telefono,
        fecha_creacion: res[i].fecha_creacion,
        fecha_actualizo: res[i].fecha_actualizo,
        deleted: res[i].deleted,
      };

      tutores.push(datos);
    }

    result(null, tutores);
  });
};

Tutores.addTutor = (t, result) => {
  sql.query(
    `INSERT INTO tutores(nombre,apellido_paterno,apellido_materno,telefono,email)VALUES(?,?,?,?,?)`,
    [t.nombre, t.apellido_paterno, t.apellido_materno, t.telefono, t.email],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      result(null, { id: res.insertId, ...t });
    }
  );
};

Tutores.updateTutor = (id, t, result) => {
  sql.query(
    `UPDATE tutores SET nombre=?,apellido_materno=?,apellido_paterno=?,telefono=?,email=?, fecha_actualizo=? WHERE idtutores=?`,
    [
      t.nombre,
      t.apellido_paterno,
      t.apellido_materno,
      t.telefono,
      t.email,
      t.fecha_actualizo,
      id,
    ],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...t });
    }
  );
};

Tutores.getTutor = (id, result) => {
  sql.query(
    `SELECT * FROM tutores WHERE deleted = 0 AND idtutores=?`,
    [id],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }
      result(null, res);
    }
  );
};

Tutores.deleteTutor = (id, result) => {
  sql.query(
    `UPDATE tutores SET deleted=1 WHERE idtutores=?`,
    [id],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      if (res.affectedRows == 0) {
        result({ message: "No se encontro el tutor" }, null);
        return;
      }

      result(null, { id: id });
    }
  );
};

module.exports = Tutores;
