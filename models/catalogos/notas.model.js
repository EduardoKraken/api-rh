const { result } = require("lodash");
const sqlERPNUEVO = require("../db.js");

//const constructor
const Notas = function(notas) {};

Notas.getNotas = result => {
    sql.query(`SELECT * FROM notas WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

Notas.addNota = (c, result) => {
    sql.query(`INSERT INTO notas(nota,idempleado,idsubtema,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?,?,?)`, [c.nota, c.idempleado, c.idsubtema, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo],
        (err, res) => {
            if (err) { result(err, null); return; }
            result(null, { id: res.insertId, ...c });
        }
    )
}

Notas.updateNota = (id, c, result) => {
    sql.query(` UPDATE notas SET nota=?,idempleado=?,idsubtema=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idnota=?`, [c.nota, c.idempleado, c.idsubtema, c.usuario_registro, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            result(null, { id: id, ...c });
        })
}



module.exports = Notas;