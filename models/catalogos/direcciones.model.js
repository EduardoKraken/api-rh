const { result } = require("lodash");
const sql = require("../db.js");

//const constructor
const Direcciones = function (direcciones) {};

Direcciones.getDireccion = (id, result) => {
  sql.query(
    `SELECT * FROM direcciones WHERE deleted = 0 AND iddirecciones=?`,
    [id],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }
      result(null, res);
    }
  );
};

Direcciones.addDireccion = (direccion, result) => {
  sql.query(
    `INSERT INTO direcciones(calle,numero_ext,numero_int,colonia,cp,municipio,estado,pais)VALUES(?,?,?,?,?,?,?,?)`,
    [
      direccion.calle,
      direccion.numero_ext,
      direccion.numero_int,
      direccion.colonia,
      direccion.cp,
      direccion.municipio,
      direccion.estado,
      direccion.pais,
    ],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      result(null, { id: res.insertId });
    }
  );
};

Direcciones.updateDireccion = (id, direccion, result) => {
  sql.query(
    `UPDATE direcciones SET calle=?, numero_ext=?,numero_int=?,colonia=?,cp=?,municipio=?,estado=?,pais=? WHERE iddirecciones=?`,
    [
      direccion.calle,
      direccion.numero_ext,
      direccion.numero_int,
      direccion.colonia,
      direccion.cp,
      direccion.municipio,
      direccion.estado,
      direccion.pais,
      id,
    ],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...t });
    }
  );
};

Direcciones.deleteDireccion = (id, result) => {
  sql.query(
    `UPDATE direcciones SET deleted=1 WHERE iddirecciones=?`,
    [id],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      if (res.affectedRows == 0) {
        result({ message: "No se encontro la direccion" }, null);
        return;
      }

      result(null, { id: id });
    }
  );
};

module.exports = Tutores;
