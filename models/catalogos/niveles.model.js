const { result }  = require("lodash");
// Original
const sqlERP      = require("../db3.js");

// Prueba
// const sqlERP      = require("../dbErpPrueba.js");

//const constructor
const niveles = function(niveles) {};

// Consultar todos los niveles
niveles.getNiveles = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM niveles;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

// consultar solo los niveles activos
niveles.getNivelesActivos = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM niveles WHERE activo_sn = 1 ;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

// Agregar un nivel+
niveles.addNivel = ( pu ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO niveles(nivel,comentarios,id_usuario_ultimo_cambio)VALUES(?,?,?)`, 
    [pu.nivel, pu.comentarios, pu.id_usuario],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id: res.insertId, ...pu })
    })
  })
}

// Actualizar el nivel en general
niveles.updateNivel = ( id, a ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE niveles SET id_usuario_ultimo_cambio = ?, activo_sn = ?, comentarios = ?  WHERE id_nivel = ?`, [ a.id_usuario, a.activo_sn, a.comentarios, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, a })
    })
  })
}

// Eliminar el nivel
niveles.deleteNivel = ( id, u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE niveles SET activo_sn = 0, fecha_baja = NOW(), id_usuario_ultimo_cambio = ? WHERE id_nivel = ?`, [ u.id_usuario, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, u })
    })
  })
}


module.exports = niveles;