const { result }  = require("lodash");
// Original
const sqlERPNUEVO = require("../db.js");


const permisos = function(permisos) {};


//------------------------------Menu-------------------------------------//

// Consultar MENU
permisos.getMenu = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM menu WHERE deleted = 0;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

permisos.getMenuActivos = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM menu WHERE deleted = 0 AND activo = 1;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

// Agregar MENU
permisos.addMenu = ( mn ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`INSERT INTO menu(action,title,sub)VALUES(?,?,?)`, 
    [mn.action, mn.title, mn.sub],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...mn })
    })
  })
}


// Eliminar MENU
permisos.deleteMenu = ( idmenu ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`UPDATE menu SET deleted = 1 WHERE idmenu = ?`, [ idmenu ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

// Actualizar MENU
permisos.updateMenu = ( um ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE menu SET action=?, title=?, sub=? WHERE idmenu=?`, [ um.action, um.title, um.sub, um.idmenu ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res);
    });
  });
}


//------------------------------SubMenu-------------------------------------//

// Consultar SUBMENU
permisos.getSubmenu = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT s.*, m.title AS menu FROM submenu s
      LEFT JOIN menu m  ON m.idmenu = s.idmenu
      WHERE s.deleted = 0 ORDER BY s.title;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

permisos.getSubmenuActivos = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT s.*, m.title AS menu FROM submenu s
      LEFT JOIN menu m  ON m.idmenu = s.idmenu
      WHERE s.deleted = 0 AND s.activo = 1 ORDER BY s.title;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

// Agregar SUBMENU
permisos.addSubmenu = ( sb ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`INSERT INTO submenu(soysub,active,action,title,idmenu)VALUES(?,?,?,?,?)`, 
    [sb.soysub, sb.active, sb.action, sb.title, sb.idmenu],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...sb })
    })
  })
}


// Eliminar SUBMENU
permisos.deleteSubmenu = ( idsubmenu ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`UPDATE submenu SET deleted = 1 WHERE idsubmenu = ?`, [ idsubmenu ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

// Actualizar SUBMENU
permisos.updateSubmenu = ( us ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE submenu SET soysub=?, active=?, action=?, title=?, idmenu=? WHERE idsubmenu=?`, 
    [ us.soysub, us.active, us.action, us.title, us.idmenu, us.idsubmenu ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res);
    });
  });
}


//------------------------------Actions-------------------------------------//

// Consultar ACTIONS
permisos.getActions = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT a.*, m.title AS menu, s.title AS submenu FROM actions a
    LEFT JOIN menu m ON m.idmenu = a.idmenu
    LEFT JOIN submenu s ON s.idsubmenu = a.idsubmenu
    WHERE a.deleted = 0
    ORDER BY a.title;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}


permisos.getActionsActivos = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT a.*, m.title AS menu, s.title AS submenu FROM actions a
    LEFT JOIN menu m ON m.idmenu = a.idmenu
    LEFT JOIN submenu s ON s.idsubmenu = a.idsubmenu
    WHERE a.deleted = 0 AND a.activo = 1 
    ORDER BY a.title;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}


// Agregar ACTIONS
permisos.addActions = ( ac ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`INSERT INTO actions(title,path,icon,active,color,idsubmenu,idmenu)VALUES(?,?,?,?,?,?,?)`, 
    [ac.title, ac.path, ac.icon, ac.active, ac.color, ac.idsubmenu, ac.idmenu],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...ac })
    })
  })
}


// Eliminar ACTIONS
permisos.deleteActions = ( idactions ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`UPDATE actions SET deleted = 1 WHERE idactions = ?`, [ idactions ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

// Actualizar ACTIONS
permisos.updateActions = ( ua ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE actions SET title=?, path=?, icon=?, active=?, color=?, idsubmenu=?, idmenu=? WHERE idactions=?`, 
    [ ua.title, ua.path, ua.icon, ua.active, ua.color, ua.idsubmenu, ua.idmenu, ua.idactions ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res);
    });
  });
}


//------------------------------Actions-------------------------------------


permisos.existePermisoPuesto = ( idpuesto, idactions ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM permisos_puestos WHERE idpuesto = ? AND idactions = ?;`,[ idpuesto, idactions ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

permisos.addPermisos = ( idpuesto, idactions ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`INSERT INTO permisos_puestos(idpuesto, idactions)VALUES(?,?)`, 
    [idpuesto, idactions],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, idpuesto, idactions })
    })
  })
}


// Eliminar ACTIONS
permisos.actualizarPermiso = ( idpuesto, idactions, seleccionado ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`UPDATE permisos_puestos SET deleted = ? WHERE idpermisos_puestos > 0 AND idpuesto = ? AND idactions = ?`, [ seleccionado, idpuesto, idactions ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ idpuesto, idactions, seleccionado })
    })
  })
}

permisos.getPermisosPuesto = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM permisos_puestos WHERE idpuesto = ? AND deleted = 0;`,[ id ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

permisos.getActionsId = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT a.*, m.title AS menu, s.title AS submenu FROM actions a
      LEFT JOIN menu m ON m.idmenu = a.idmenu
      LEFT JOIN submenu s ON s.idsubmenu = a.idsubmenu
      WHERE a.deleted = 0 AND a.idactions IN (?) AND a.activo = 1
      ORDER BY a.title;`,[ id ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

permisos.getSubmenusId = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM submenu WHERE deleted = 0 AND activo  = 1 AND idsubmenu IN ( ? ) ORDER BY title;`,[ id ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

permisos.getMenusId = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM menu WHERE deleted = 0 AND activo = 1 AND idmenu IN (?);`,[ id ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

module.exports = permisos;