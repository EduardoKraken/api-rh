const { result }  = require("lodash");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");
const sqlERPNUEVO = require("../db.js");
const sqlERP      = require("../dbErpPrueba.js");

//const constructor
const trabajadoreserp = function() {};

trabajadoreserp.getTrabajadoresERP = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT t.id as id_trabajador, t.nombres, t.apellido_paterno, t.apellido_materno, t.nombre_completo, 
    t.id_puesto, p.descripcion AS puesto, t.id_departamento, d.descripcion AS departamento, t.domicilio, 
    t.horario_inicio, t.horario_fin, t.telefono1, t.telefono2, t.email, t.rfc, t.numero_poliza_sgm_mayores, t.numero_poliza_sgm_menores,
    t.numero_imss, t.fecha_ingreso, t.fecha_termino_labores, t.antiguedad, t.id_tipos_trabajadores AS id_tipo_trabajador, tt.descripcion AS tipo_trabajador,
    t.id_plantel, pl.plantel, t.id_jornada_laboral ,j.tipo_jornada, t.activo_sn, t.fecha_alta ,t.fecha_baja , t.motivo_salida,
    t.id_nivel, n.nivel, t.id_curso, c.curso, t.fecha_nacimiento, t.motivo_salida
    FROM fyn_cat_trabajadores t
    LEFT JOIN fyn_cat_puestos p ON t.id_puesto = p.id
    LEFT JOIN fyn_cat_departamentos d ON t.id_departamento = d.id
    LEFT JOIN planteles pl ON t.id_plantel = pl.id_plantel
    LEFT JOIN fyn_cat_jornadas_laborales j ON t.id_jornada_laboral = j.id
    LEFT JOIN niveles n ON t.id_nivel = n.id_nivel
    LEFT JOIN cursos c ON t.id_curso = c.id_curso
    LEFT JOIN fyn_cat_tipos_trabajadores tt ON t.id_tipos_trabajadores = tt.id;`,(err, res) => {
      if(err){
        return reject({message: err.sqlMessage })  
      }
      resolve(res)
    })
  })
};

trabajadoreserp.getPuestosERP= ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT id AS id_puesto, descripcion AS puesto FROM fyn_cat_puestos;`,(err, res) => {
      if(err){
        return reject({message: err.sqlMessage })  
      }
      resolve(res)
    })
  })
};

trabajadoreserp.getJornadaLaboralERP= ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT id AS id_jornada_laboral, tipo_jornada FROM fyn_cat_jornadas_laborales;`,(err, res) => {
      if(err){
        return reject({message: err.sqlMessage })  
      }
      resolve(res)
    })
  })
};

trabajadoreserp.getDepartamentoERP= ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT id AS id_departamento, descripcion as departamento FROM fyn_cat_departamentos;`,(err, res) => {
      if(err){
        return reject({message: err.sqlMessage })  
      }
      resolve(res)
    })
  })
};

trabajadoreserp.getTipoTrabajadorERP= ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT id AS id_tipo_trabajador, descripcion AS tipo_trabajador FROM fyn_cat_tipos_trabajadores;`,(err, res) => {
      if(err){
        return reject({message: err.sqlMessage })  
      }
      resolve(res)
    })
  })
};

trabajadoreserp.getNivelERP= ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT id_nivel, nivel FROM niveles;`,(err, res) => {
      if(err){
        return reject({message: err.sqlMessage })  
      }
      resolve(res)
    })
  })
};

trabajadoreserp.getCursoERP= ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT id_curso, curso FROM cursos;`,(err, res) => {
      if(err){
        return reject({message: err.sqlMessage })  
      }
      resolve(res)
    })
  })
};

trabajadoreserp.getPerfilesERP= ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT id_perfil, perfil FROM perfiles 
    WHERE activo_sn = 1;`,(err, res) => {
      if(err){
        return reject({message: err.sqlMessage })  
      }
      resolve(res)
    })
  })
};

trabajadoreserp.getTrabajadorsERP= ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT id, nombre_completo FROM fyn_cat_trabajadores;`,(err, res) => {
      if(err){
        return reject({message: err.sqlMessage })  
      }
      resolve(res)
    })
  })
};


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  trabajadoreserp.updateTrabajador = ( nombres, apellido_paterno, apellido_materno, domicilio, horario_inicio, horario_fin, telefono1, telefono2, email, rfc, numero_poliza_sgm_mayores, numero_poliza_sgm_menores, numero_imss, fecha_ingreso, fecha_termino_labores,
    antiguedad, id_plantel, id_puesto, id_jornada_laboral, id_departamento, id_tipos_trabajadores, id_nivel, id_curso, fecha_nacimiento, motivo_salida, activo_sn, id) => {
    return new Promise(( resolve, reject )=>{
      sqlERP.query(`UPDATE fyn_cat_trabajadores
      SET  nombres = ?, apellido_paterno = ?, apellido_materno = ?, nombre_completo = CONCAT(?, ' ', ? ,' ', ?), 
      domicilio = ?, horario_inicio = ?, horario_fin = ?, telefono1 = ?, telefono2 = ?, email = ?, rfc = ?, numero_poliza_sgm_mayores = ?,
      numero_poliza_sgm_menores = ?, numero_imss = ?, fecha_ingreso = ?, fecha_termino_labores = ?, antiguedad = ?, id_plantel = ?, id_puesto = ?,
      id_jornada_laboral = ?, id_departamento = ?, id_tipos_trabajadores = ?, id_nivel = ?, id_curso = ?, fecha_nacimiento = ?, motivo_salida = ?, 
      activo_sn = ?
      WHERE id = ?;`,[ nombres, apellido_paterno, apellido_materno, nombres, apellido_paterno, apellido_materno, domicilio, horario_inicio, horario_fin, telefono1, telefono2, email, rfc, numero_poliza_sgm_mayores, numero_poliza_sgm_menores, numero_imss, fecha_ingreso, fecha_termino_labores,
        antiguedad, id_plantel, id_puesto, id_jornada_laboral, id_departamento, id_tipos_trabajadores, id_nivel, id_curso, fecha_nacimiento, motivo_salida, activo_sn, id],(err, res) => {
        if (err) {
          return reject({message:err.sqlMessage})
        }
        if (res.affectedRows == 0) {
          return reject({ message: "not_found" });      
        }  
        resolve( res );
        });
    });
    };

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

trabajadoreserp.addTrabajadorERP = (u, result) => {
  sqlERP.query(`INSERT INTO fyn_cat_trabajadores ( nombres, apellido_paterno, apellido_materno, nombre_completo ,domicilio, horario_inicio, horario_fin, telefono1, telefono2, email, rfc, 
    numero_poliza_sgm_mayores, numero_poliza_sgm_menores, numero_imss, fecha_ingreso, fecha_termino_labores, antiguedad, id_plantel, id_puesto, 
    id_jornada_laboral, id_departamento, id_tipos_trabajadores, id_nivel, id_curso, fecha_nacimiento, motivo_salida, activo_sn, salario_bruto, salario_neto, id_usuario_ultimo_cambio ) 
    VALUES (?, ?, ?,CONCAT(nombres, ' ', apellido_paterno, ' ', apellido_materno),?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0.00, 0.00, 1);`,
   [u.nombres, u.apellido_paterno, u.apellido_materno, u.domicilio, u.horario_inicio, u.horario_fin, u.telefono1, u.telefono2, u.email, u.rfc,
    u.numero_poliza_sgm_mayores, u.numero_poliza_sgm_menores, u.numero_imss, u.fecha_ingreso, u.fecha_termino_labores, u.antiguedad, u.id_plantel, u.id_puesto,
    u.id_jornada_laboral, u.id_departamento, u.id_tipos_trabajadores, u.id_nivel, u.id_curso, u.fecha_nacimiento, u.motivo_salida, u.activo_sn],
  (err, res) => {
    if (err) { result(err, null); return; }
    result(null, { id: res.insertId, ...u });
  });
};


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

trabajadoreserp.deleteUsuarioERP = (id_usuario, result) => {
  sql.query("DELETE FROM usuarios  WHERE id_usuario = ?;", id, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, res);
  });
};

module.exports = trabajadoreserp;

//ANGEL RODRIGUEZ -- TODO