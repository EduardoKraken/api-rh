const { result } = require("lodash");
const sql = require("../db.js");


//const constructor
const Insignias = function(insignias) {};

Insignias.getInsignias = result => {
    sql.query(`SELECT * FROM insignias WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

Insignias.addInsignia = (c, result) => {
    sql.query(`INSERT INTO insignias(insignia,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?)`, [c.insignia, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo],
        (err, res) => {
            if (err) { result(err, null); return; }
            result(null, { id: res.insertId, ...c });
        }
    )
}

Insignias.updateInsignia = (id, c, result) => {
    sql.query(` UPDATE insignias SET insignia=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idinsignia=?`, [c.insignia, c.usuario_registro, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            result(null, { id: id, ...c });
        })
}



module.exports = Insignias;