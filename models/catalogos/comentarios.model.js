const { result } = require("lodash");
const sql = require("../db.js");
const sqlErp = require("../db3.js");
const moment = require("moment");
//const constructor
const Comentarios = function (comentarios) {};

Comentarios.getComentarios = (idCliente, result) => {
  sql.query(
    `SELECT comentarios.*,u.iderp FROM comentarios INNER JOIN usuarios as u ON u.idusuario = comentarios.id_empleado WHERE id_prospecto = ? ORDER BY comentarios.fecha_creacion DESC;`,
    [idCliente],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      ///
      var comentarios = [];
      for (const i in res) {
        //declaro el objeto que utilizare para llenar el arreglo
        var datos = {
          idComentario: res[i].idcomentarios,
          iderp: res[i].iderp,
          idProspecto: res[i].id_prospecto,
          idEmpleado: res[i].id_empleado,
          comentario: res[i].comentario,
          pinned: res[i].pinned,
          fecha_creacion: moment(res[i].fecha_creacion).format(
            "YYYY-MM-DD HH:mm:ss"
          ),
          fecha_actualizo: moment(res[i].fecha_actualizo).format(
            "YYYY-MM-DD HH:mm:ss"
          ),
        };
        comentarios.push(datos);
      }

      if (!comentarios) {
        result(null, comentarios);
        return;
      }
      sqlErp.query(
        `SELECT * FROM usuarios WHERE activo_sn = 1 ORDER BY nombre_completo`,
        (err, res) => {
          if (err) {
            result(null, err);
            return;
          }

          for (const i in comentarios) {
            for (const j in res) {
              if (comentarios[i].iderp == res[j].id_usuario) {
                comentarios[i].nombreEmpleado = res[j].nombre_completo;
              }
            }
          }

          result(null, comentarios);
          return;
        }
      );
    }
  );
};

Comentarios.addComentario = (comentario, result) => {
  sql.query(
    `INSERT INTO comentarios(id_prospecto,id_empleado,comentario,pinned)VALUES(?,?,?,?)`,
    [
      comentario.id_prospecto,
      comentario.id_empleado,
      comentario.comentario,
      comentario.pinned,
    ],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      result(null, { id: res.insertId, ...comentario });
    }
  );
};

module.exports = Comentarios;
