const { result }  = require("lodash");
// Original
const sqlERP      = require("../db3.js");

// Prueba
// const sqlERP      = require("../dbErpPrueba.js");

//const constructor
const formaspago = function(formaspago) {};

// Consultar todos los formaspago
formaspago.getFormasPago = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM formaspago;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

// consultar solo los formaspago activos
formaspago.getFormasPagoActivos = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM formaspago WHERE activo_sn = 1 ;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

// Agregar un salon+
formaspago.addFormasPago = ( pu ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO formaspago(forma_pago,comentarios,id_usuario_ultimo_cambio)VALUES(?,?,?)`, 
    [pu.forma_pago, pu.comentarios, pu.id_usuario],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id: res.insertId, ...pu })
    })
  })
}

// Actualizar el salon en general
formaspago.updateFormasPago = ( id, a ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE formaspago SET forma_pago = ?, comentarios = ?, id_usuario_ultimo_cambio = ?, activo_sn = ? WHERE id_forma_pago = ?`, 
    	[ a.forma_pago, a.comentarios, a.id_usuario, a.activo_sn, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, a })
    })
  })
}

// Eliminar el salon
formaspago.deleteFormasPago = ( id, u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE formaspago SET activo_sn = 0, fecha_baja = NOW(), id_usuario_ultimo_cambio = ? WHERE id_forma_pago = ?`, [ u.id_usuario, id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id, u })
    })
  })
}


module.exports = formaspago;