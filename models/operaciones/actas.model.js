const { result } = require("lodash");
const sqlERP  = require("../db3.js");
const sqlINBI = require("../dbINBI.js");
const sqlFAST = require("../dbFAST.js");


//const constructor
const actas = function(depas) {

};

actas.getActaAdministrativaList = (  ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT a.*, u.nombre_completo AS asigno, u2.nombre_completo AS asignado, f.falta, s.sancion FROM actas_admin a 
			LEFT JOIN usuarios u ON u.id_usuario = a.id_asigno
			LEFT JOIN usuarios u2 ON u2.id_usuario = a.id_usuario
			LEFT JOIN faltas_admin f ON f.idfaltas = a.id_faltas
			LEFT JOIN sancion_admin s ON s.idsancion = a.id_sancion
			WHERE a.deleted = 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      // Retornamos solo un dato
      return resolve( res);
    });
  })
};

actas.getActaAdministrativaUsuario = ( id_usuario ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT a.*, u.nombre_completo AS asigno, u2.nombre_completo AS asignado, f.falta, s.sancion FROM actas_admin a 
			LEFT JOIN usuarios u ON u.id_usuario = a.id_asigno
			LEFT JOIN usuarios u2 ON u2.id_usuario = a.id_usuario
			LEFT JOIN faltas_admin f ON f.idfaltas = a.id_faltas
			LEFT JOIN sancion_admin s ON s.idsancion = a.id_sancion
			WHERE a.deleted = 0 AND a.id_usuario = ?;`, [ id_usuario ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      // Retornamos solo un dato
      return resolve( res);
    });
  })
};

actas.addActaAdministrativa = ( u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO actas_admin( id_asigno, id_usuario, motivo, id_sancion, id_faltas )VALUES(?,?,?,?,?)`, 
    	[ u.id_asigno, u.id_usuario, u.motivo, u.id_sancion, u.id_faltas ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve({ id: res.insertId, ...u })
    })
  })
}

actas.updateActaAdministrativa = ( u ) => {
	return new Promise((resolve,reject)=>{
	  sqlERP.query(`UPDATE actas_admin SET id_asigno = ?, id_usuario = ?, motivo = ?, id_sancion = ?, id_faltas = ?, deleted = ?, estatus = ? WHERE idactas_admin = ?`, 
	    [ u.id_asigno, u.id_usuario, u.motivo, u.id_sancion, u.id_faltas, u.deleted, u.estatus, u.idactas_admin ],
	    (err, res) => {
      if (err) {  return reject({ message: err.sqlMessage }); }
      if (res.affectedRows == 0) {
        return reject({ message: 'No se encontraron datos para actualizar' });
      }
      return resolve({ ... u });
	  })
	})
}


/*****************************************/
/*        FALTAS ADMINISTRATICAS         */
/*****************************************/

actas.getFaltasAdminList = (  ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM faltas_admin WHERE deleted = 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      // Retornamos solo un dato
      return resolve( res);
    });
  })
};

actas.addFaltasAdmin = ( u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO faltas_admin( falta )VALUES( ? )`, 
    	[ u.falta ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve({ id: res.insertId, ...u })
    })
  })
}

actas.updateFaltasAdmin = ( u ) => {
	return new Promise((resolve,reject)=>{
	  sqlERP.query(`UPDATE faltas_admin SET falta = ?, deleted = ? WHERE idfaltas = ?`, 
	    [ u.falta, u.deleted, u.idfaltas ],
	    (err, res) => {
      if (err) {  return reject({ message: err.sqlMessage }); }
      if (res.affectedRows == 0) {
        return reject({ message: 'No se encontraron datos para actualizar' });
      }
      return resolve({ ... u });
	  })
	})
}


/*****************************************/
/*      SANCIONES ADMINISTRATICAS        */
/*****************************************/

actas.getSancionesAdminList = (  ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM sancion_admin WHERE deleted = 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      // Retornamos solo un dato
      return resolve( res);
    });
  })
};

actas.addSancionesAdmin = ( u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO sancion_admin( sancion )VALUES( ? )`, 
    	[ u.sancion ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve({ id: res.insertId, ...u })
    })
  })
}

actas.updateSancionesAdmin = ( u ) => {
	return new Promise((resolve,reject)=>{
	  sqlERP.query(`UPDATE sancion_admin SET sancion = ?, deleted = ? WHERE idsancion = ?`, 
	    [ u.sancion, u.deleted, u.idsancion ],
	    (err, res) => {
      if (err) {  return reject({ message: err.sqlMessage }); }
      if (res.affectedRows == 0) {
        return reject({ message: 'No se encontraron datos para actualizar' });
      }
      return resolve({ ... u });
	  })
	})
}

module.exports = actas;

