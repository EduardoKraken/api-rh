const { result } = require("lodash");
// const sqlERP  = require("../db3.js");

const sqlERP  = require("../dbErpPrueba.js");
const sqlINBI = require("../dbINBI.js");
const sqlFAST = require("../dbFAST.js");

//const constructor
const descuentos = function(depas) {};

// General
/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/

descuentos.addDescuentoAlumno = ( a ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO descuentos_especiales( id_alumno, id_grupo_actual, monto, fecha_limite, tipo_descuento, id_curso )VALUES( ?, ?, ?, ?, ?, ? )`,
    	[ a.id_alumno, a.id_grupo_actual, a.monto, a.fecha_limite, a.tipo_descuento, a.id_curso ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve( { id: res.insertId, ...a })
    })
  });
};

descuentos.getDescuentosAlumno = ( id_alumno, id_grupo_actual ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM descuentos_especiales
			WHERE id_alumno = ?
			AND IF(CURRENT_DATE > fecha_limite, 1, 0) = 0
			AND deleted = 0
			AND aplicado = 0
			AND tipo_descuento = 2
			OR id_grupo_actual = ?
			AND id_alumno = ?
			AND deleted = 0
			AND aplicado = 0
			AND tipo_descuento = 1;`,[ id_alumno, id_grupo_actual, id_alumno ], (err,  res) => {
			if (err) { return reject({ message: err.sqlMessage }) }
      resolve( res )
    });
  });
};

descuentos.updateDescuetoEspecial = ( id_grupo, iddescuentos_especiales ) => {
	return new Promise((resolve,reject)=>{
	  sqlERP.query(`UPDATE descuentos_especiales SET id_grupo_aplicado = ?, aplicado = 1 WHERE iddescuentos_especiales = ?`, [ id_grupo, iddescuentos_especiales ],
	    (err, res) => {
	      if (err) { return reject({ message: err.sqlMessage }) }
	      if (res.affectedRows == 0) {
	        return reject({ message: 'No existe el descuentos' })
	      }
	      resolve( res )
	  })
	})
}

// OBTENER TODOS LOS DESCUENTOS
descuentos.getDescuentoAll = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT d.*, CONCAT(a.nombre, ' ', a.apellido_paterno, IFNULL(CONCAT(' ', a.apellido_materno), '')) AS alumno, 
    	a.matricula, g.grupo
			FROM descuentos_especiales d
			LEFT JOIN alumnos a ON a.id_alumno = d.id_alumno
			LEFT JOIN grupos g ON g.id_grupo = d.id_grupo_actual
			WHERE d.deleted = 0;`, (err,  res) => {
			if (err) { return reject({ message: err.sqlMessage }) }
      resolve( res )
    });
  });
};

// OBTENER TODOS LOS GRUPOS ACTIVOS ACTUALES
descuentos.obtenerGruposDescuento = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT g.id_grupo, g.grupo FROM grupos g
			LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
			LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
			WHERE g.activo_sn = 1
			AND CURRENT_DATE BETWEEN c.fecha_inicio_ciclo AND c.fecha_fin_ciclo
			ORDER BY p.plantel, g.id_nivel;`, (err,  res) => {
			if (err) { return reject({ message: err.sqlMessage }) }
      resolve( res )
    });
  });
}; 

// OBTENER LOS ALUMNOS INSCRITOS EN ESE GRUPO
descuentos.alumnosPorGrupo = ( idgrupos ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT *, CONCAT(al.nombre, ' ', al.apellido_paterno, IFNULL(CONCAT(' ', al.apellido_materno), '')) AS alumno FROM alumnos_grupos_especiales_carga a
			LEFT JOIN alumnos al ON al.id_alumno = a.id_alumno
			WHERE a.id_grupo IN ( ? )
			AND adeudo = 0 AND pagado > 0;`,[ idgrupos ],(err,  res) => {
			if (err) { return reject({ message: err.sqlMessage }) }
      resolve( res )
    });
  });
}; 

descuentos.eliminarDescuento = ( id ) => {
	return new Promise((resolve,reject)=>{
	  sqlERP.query(`UPDATE descuentos_especiales SET deleted = 1 WHERE iddescuentos_especiales = ?`, [ id ],
	    (err, res) => {
	      if (err) { return reject({ message: err.sqlMessage }) }
	      if (res.affectedRows == 0) {
	        return reject({ message: 'No existe el descuentos' })
	      }
	      resolve( res )
	  })
	})
}

module.exports = descuentos;

