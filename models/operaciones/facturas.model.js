const { result } = require("lodash");
// const sqlERP  = require("../db3.js");
const sqlERP  = require("../dbErpPrueba.js");

//const constructor
const facturas = function(depas) {
};

facturas.getDatosAlumnos = (  ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT a.id_alumno, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ',IFNULL(a.apellido_materno,'')) AS alumno, a.unidad_negocio,
      a.telefono, a.celular, a.matricula, a.id_contacto, a.id_ultimo_ciclo_pago,
      CONCAT(t.nombre, ' ', t.apellido_paterno, ' ',IFNULL(t.apellido_materno,'')) AS tutor, t.telefono AS telTutor, t.celular AS celTutor,
      CONCAT(
        '[',TRIM(a.matricula), '] ',
        TRIM(REPLACE(a.nombre,'  ','')), ' ', 
        TRIM(REPLACE(a.apellido_paterno,'','')), ' ',
        IFNULL(TRIM(REPLACE(a.apellido_materno,'  ','')),''), ' ', 
        a.matricula, ' ',
        IFNULL(TRIM(REPLACE(a.telefono,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(a.celular,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(t.telefono,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(t.celular,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(t.nombre,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(t.apellido_paterno,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(t.apellido_materno,' ','')),''), ' ') AS datos_completos
      FROM alumnos a
      LEFT JOIN alumnotutor t ON t.id_alumno = a.id_alumno;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

facturas.getMunicipios = (  ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM municipios;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

facturas.getDatosFiscales = (  ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT d.*, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ',IFNULL(a.apellido_materno,'')) AS alumno, m.nombre AS municipio, r.regimen FROM datosfiscales d

			LEFT JOIN alumnos a ON a.id_alumno = d.id_alumno
			LEFT JOIN municipios m ON m.idmunicipios = d.idmunicipios
			LEFT JOIN regimen_fiscal r ON r.idregimen_fiscal = d.regimen_fiscal;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

facturas.getRegimenFiscal = (  ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM regimen_fiscal;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};


facturas.addDatosFiscales = ( u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO datosfiscales( id_alumno, rfc, regimen_fiscal, direccion, idmunicipios, cp, telefono, correo, razon_social )VALUES(?,?,?,?,?,?,?,?,?)`, 
    	[ u.id_alumno, u.rfc, u.regimen_fiscal, u.direccion, u.idmunicipios, u.cp, u.telefono, u.correo, u.razon_social ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve({ id: res.insertId, ...u })
    })
  })
}

facturas.updateDatosFiscales = ( u ) => {
	return new Promise((resolve,reject)=>{
	  sqlERP.query(`UPDATE datosfiscales SET id_alumno = ?, rfc = ?, regimen_fiscal = ?, direccion = ?, idmunicipios = ?, cp = ?, telefono = ?, correo = ?, deleted = ?, razon_social = ? WHERE iddatosfiscales = ?`, 
	    [ u.id_alumno, u.rfc, u.regimen_fiscal, u.direccion, u.idmunicipios, u.cp, u.telefono, u.correo, u.deleted, u.razon_social, u.iddatosfiscales ],
	    (err, res) => {
      if (err) {  return reject({ message: err.sqlMessage }); }
      if (res.affectedRows == 0) {
        return reject({ message: 'No se encontraron datos para actualizar' });
      }
      return resolve({ ... u });
	  })
	})
}

/***************************/
/***************************/
/***************************/

facturas.getFacturasSolicitadas = (  ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT s.*, TIMESTAMPDIFF(DAY, DATE(s.fecha_creacion), CURRENT_DATE) AS tiempo_transcurrido, u.nombre_completo FROM solicitar_factura s
      LEFT JOIN usuarios u ON u.id_usuario = s.id_usuario WHERE deleted = 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

facturas.getFacturasSolicitadasUsuario = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT *, TIMESTAMPDIFF(DAY, DATE(fecha_creacion), CURRENT_DATE) AS tiempo_transcurrido FROM solicitar_factura WHERE id_usuario = ? AND deleted = 0;`,[ id ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

facturas.getUsoCFDI = (  ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM uso_cfdi;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

facturas.getSucursalesFactura = (  ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM planteles;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

facturas.getCiclosFactura = (  ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM ciclos WHERE activo_sn = 1 AND ciclo_abierto_sn = 1;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};


facturas.solicitarFactura = ( u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO solicitar_factura( razon_social, id_usuario, rfc, regimen_fiscal, direccion, municipio, cp, telefono, correo, uso_cfdi, alumno, subtotal, total, cuenta_escuela, plantel, ciclo, metodo_pago, observaciones )VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, 
    	[ u.razon_social, u.id_usuario, u.rfc, u.regimen_fiscal, u.direccion, u.municipio, u.cp, u.telefono, u.correo, u.uso_cfdi, u.alumno, u.subtotal, u.total, u.cuenta_escuela, u.plantel, u.ciclo, u.metodo_pago, u.observaciones ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve({ id: res.insertId, ...u })
    })
  })
}


facturas.subirXMLFactura = ( id, archivo ) => {
	return new Promise((resolve,reject)=>{
	  sqlERP.query(`UPDATE solicitar_factura SET xml = ? WHERE idsolicitar_factura = ?`, [ archivo, id ],
	    (err, res) => {
      if (err) {  return reject({ message: err.sqlMessage }); }
      if (res.affectedRows == 0) {
        return reject({ message: 'No se encontraron datos para actualizar' });
      }
      return resolve({ id, archivo });
	  })
	})
}

facturas.subirPDFFactura = ( id, archivo ) => {
	return new Promise((resolve,reject)=>{
	  sqlERP.query(`UPDATE solicitar_factura SET pdf = ? WHERE idsolicitar_factura = ?`, [ archivo, id ],
	    (err, res) => {
      if (err) {  return reject({ message: err.sqlMessage }); }
      if (res.affectedRows == 0) {
        return reject({ message: 'No se encontraron datos para actualizar' });
      }
      return resolve({ id, archivo });
	  })
	})
}

facturas.eliminarFactura = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE solicitar_factura SET deleted = 1 WHERE idsolicitar_factura = ?`, [ id ],
      (err, res) => {
      if (err) {  return reject({ message: err.sqlMessage }); }
      if (res.affectedRows == 0) {
        return reject({ message: 'No se encontraron datos para actualizar' });
      }
      return resolve({ id });
    })
  })
}


module.exports = facturas;

