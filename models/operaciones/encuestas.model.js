const { result } = require("lodash");
const sqlERP  = require("../db3.js");
const sqlINBI = require("../dbINBI.js");
const sqlFAST = require("../dbFAST.js");


//const constructor
const encuestas = function(depas) {

};


encuestas.getEncuestaPresencialFast = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT CONCAT(u.nombre, ' ', u.apellido_paterno, ' ', IFNULL(u.apellido_paterno,'')) AS alumno,
        radica_nuevoleon, presencial,
        p.nombre AS plantel,
        g.nombre AS grupo,
        IF(e.frecuencia = 1, 'Lunes a viernes',IF(frecuencia = 2, 'Sábados', 'Domingos') ) AS frecuencia,
        e.inicio, e.final, e.dispositivo FROM encuesta_presencial e
        LEFT JOIN usuarios u ON u.id = e.id_alumno
        LEFT JOIN planteles p ON p.id = e.plantel
        LEFT JOIN grupos g ON g.id = e.id_grupo;`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

encuestas.getAlumnosPorEncuestarFast = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM grupo_alumnos ga 
      LEFT JOIN grupos g ON g.id = ga.id_grupo 
      WHERE g.id_ciclo IN (
      SELECT id FROM ciclos WHERE CURRENT_DATE BETWEEN fecha_inicio AND fecha_fin AND nombre LIKE '%CICLO%')
      AND g.deleted = 0 AND g.nombre NOT LIKE '%CERTI%' AND g.online = 1
      AND g.editado = 1 AND g.nombre NOT LIKE '%INDU%';`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};


encuestas.getEncuestaPresencialInbi = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT CONCAT(u.nombre, ' ', u.apellido_paterno, ' ', IFNULL(u.apellido_paterno,'')) AS alumno,
        radica_nuevoleon, presencial,
        p.nombre AS plantel,
        g.nombre AS grupo,
        IF(e.frecuencia = 1, 'Lunes a viernes',IF(frecuencia = 2, 'Sábados', 'Domingos') ) AS frecuencia,
        e.inicio, e.final, e.dispositivo FROM encuesta_presencial e
        LEFT JOIN usuarios u ON u.id = e.id_alumno
        LEFT JOIN planteles p ON p.id = e.plantel
        LEFT JOIN grupos g ON g.id = e.id_grupo;`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};


encuestas.getAlumnosPorEncuestarInbi = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM grupo_alumnos ga 
      LEFT JOIN grupos g ON g.id = ga.id_grupo 
      WHERE g.id_ciclo IN (
      SELECT id FROM ciclos WHERE CURRENT_DATE BETWEEN fecha_inicio AND fecha_fin AND nombre LIKE '%CICLO%')
      AND g.deleted = 0 AND g.nombre NOT LIKE '%CERTI%' AND g.online = 1
      AND g.editado = 1 AND g.nombre NOT LIKE '%INDU%';`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};


module.exports = encuestas;