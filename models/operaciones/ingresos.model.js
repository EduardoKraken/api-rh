const { result } = require("lodash");
// const sqlERP  = require("../db3.js");

const sqlERP      = require("../dbErpPrueba.js");

//const constructor
const ingresos = function(depas) {
};

ingresos.getIngresosAlumnoGrupo = ( id_ciclo ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT i.*, sf.monto AS saldo_favor, ga.activo_sn AS baja,
      c.ciclo, p.plantel, p.id_plantel, u.nombre_completo, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, f.forma_pago, g.grupo 
      FROM ingresos i 
      LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN usuarios u ON u.id_usuario = i.id_usuario_ultimo_cambio
      LEFT JOIN alumnos a ON a.id_alumno = i.id_alumno
      LEFT JOIN formaspago f ON f.id_forma_pago = i.id_forma_pago
      LEFT JOIN saldofavoralumnos sf ON sf.id_alumno = i.id_alumno AND sf.id_grupo = i.id_grupo AND sf.tipo = 1
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = i.id_alumno AND ga.id_grupo = i.id_grupo
			WHERE c.id_ciclo = ? AND ga.activo_sn = 1
			AND i.activo_sn = 1;`,[ id_ciclo ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

ingresos.eliminarIngreso = ( id_ingreso ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`DELETE FROM ingresos WHERE id_ingreso = ?;`,[ id_ingreso ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve({ message: 'El ingreso se eliminó correctamente'});
    });
  })
};

ingresos.reporteAjustePagos = ( u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO ingresos_reporte(id_ingreso, id_alumno, id_grupo, id_tipo_inscripcion, id_forma_pago, monto_pagado, id_tipo_ingreso, comentarios, fecha_pago, 
      id_usuario_ultimo_cambio, fecha_ultimo_cambio,adeudo_grupo, activo_sn, monto_saldo_favor, url_foto, id_plantel_ingreso, texto_completo, tipo_reporte,id_solicita,
      nuevo_tipo_pago,nueva_fecha_pago,nuevo_monto_pago,nuevo_descuento,baja_alumno)
      VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, 
      [u.id_ingreso, u.id_alumno, u.id_grupo, u.id_tipo_inscripcion, u.id_forma_pago, u.monto_pagado, u.id_tipo_ingreso, u.comentarios, u.fecha_pago, u.id_usuario_ultimo_cambio, 
        u.fecha_ultimo_cambio,u.adeudo_grupo, u.activo_sn, u.monto_saldo_favor, u.url_foto, u.id_plantel_ingreso, u.texto_completo, u.tipo_reporte, u.id_solicita,
        u.nuevo_tipo_pago,u.nueva_fecha_pago, u.nuevo_monto_pago,u.nuevo_descuento,u.baja_alumno],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

ingresos.getReporteAjustePagosUsuario = ( id_usuario ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT i.*, sf.monto AS saldo_favor,
      c.ciclo, p.plantel, p.id_plantel, u.nombre_completo, u2.nombre_completo AS solicito, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, f.forma_pago, g.grupo FROM ingresos_reporte i 
      LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN usuarios u ON u.id_usuario = i.id_usuario_ultimo_cambio
      LEFT JOIN usuarios u2 ON u2.id_usuario = i.id_solicita
      LEFT JOIN alumnos a ON a.id_alumno = i.id_alumno
      LEFT JOIN formaspago f ON f.id_forma_pago = i.id_forma_pago
      LEFT JOIN saldofavoralumnos sf ON sf.id_alumno = i.id_alumno AND sf.id_grupo = i.id_grupo AND sf.tipo = 1
      WHERE i.deleted = 0;`,[ id_usuario ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

ingresos.updateReporteAjuste = ( estatus, id_autoriza, idingresos_reporte ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE ingresos_reporte SET estatus = ?, id_autoriza = ? WHERE idingresos_reporte = ?;`,[ estatus, id_autoriza, idingresos_reporte ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve({ message: 'El ingreso se eliminó correctamente'});
    });
  })
};

ingresos.eliminarReporte = ( idingresos_reporte ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE ingresos_reporte SET deleted = 1 WHERE idingresos_reporte = ?;`,[ idingresos_reporte ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve({ message: 'El ingreso se eliminó correctamente'});
    });
  })
};

ingresos.getPagosDuplicados = ( u ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT i.*, sf.monto AS saldo_favor,
      c.ciclo, p.plantel, p.id_plantel, u.nombre_completo, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, f.forma_pago, g.grupo FROM ingresos i 
      LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN usuarios u ON u.id_usuario = i.id_usuario_ultimo_cambio
      LEFT JOIN alumnos a ON a.id_alumno = i.id_alumno
      LEFT JOIN formaspago f ON f.id_forma_pago = i.id_forma_pago
      LEFT JOIN saldofavoralumnos sf ON sf.id_alumno = i.id_alumno AND sf.id_grupo = i.id_grupo AND sf.tipo = 1
      WHERE i.id_alumno = ? AND i.id_grupo = ? AND i.monto_pagado = ? AND i.id_ingreso <> ?;`,
      [ u.id_alumno, u.id_grupo, u.monto_pagado, u.id_ingreso ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res );
    });
  })
};

ingresos.updateMetodoIngreso = ( id_ingreso, id_forma_pago ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE ingresos SET id_forma_pago = ? WHERE id_ingreso = ?;`,[ id_forma_pago, id_ingreso ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve({ message: 'El ingreso se actualió correctamente'});
    });
  })
};

/*   ERROR EN MONTO PAGADO */

ingresos.updateGrupoAlumno = ( id_grupo, id_alumno, monto_pagado_total, monto_saldo_favor, pago_completado_sn ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE gruposalumnos SET monto_pagado_total = ?, monto_saldo_favor = ?, pago_completado_sn = ?
      WHERE id_gruposalumnos > 0 AND id_grupo = ? AND id_alumno = ?;`,[ monto_pagado_total, monto_saldo_favor, pago_completado_sn, id_grupo, id_alumno ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve({ message: 'El ingreso se actualió correctamente'});
    });
  })
};

ingresos.updateGrupoAlumnoDescuento = ( id_grupo, id_alumno, monto_saldo_favor, pago_completado_sn, monto_descuento_grupo ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE gruposalumnos SET monto_descuento_grupo = ?, monto_saldo_favor = ?, pago_completado_sn = ?
      WHERE id_gruposalumnos > 0 AND id_grupo = ? AND id_alumno = ?;`,[ monto_descuento_grupo, monto_saldo_favor, pago_completado_sn, id_grupo, id_alumno ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve({ message: 'El ingreso se actualió correctamente'});
    });
  })
};

ingresos.updateMontoPago = ( monto_pagado, id_ingreso ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE ingresos SET monto_pagado = ? WHERE id_ingreso = ?;`,[ monto_pagado, id_ingreso ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve({ message: 'El ingreso se actualió correctamente'});
    });
  })
};

ingresos.updateAlumnosCarga = ( pagado, adeudo, id_grupo, id_alumno ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE alumnos_grupos_especiales_carga SET pagado = ?, adeudo = ? 
      WHERE id > 0  AND id_grupo = ? AND id_alumno = ?;`,[ pagado, adeudo, id_grupo, id_alumno ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve({ message: 'El ingreso se actualió correctamente'});
    });
  })
};

ingresos.updateAlumnosCargaDescuento = ( descuento, adeudo, precio_con_descuento, id_grupo, id_alumno ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE alumnos_grupos_especiales_carga SET descuento = ?, adeudo = ?, precio_grupo_con_descuento_x_alumno = ? 
      WHERE id > 0  AND id_grupo = ? AND id_alumno = ?;`,[ descuento, adeudo, precio_con_descuento, id_grupo, id_alumno ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve({ message: 'El ingreso se actualió correctamente'});
    });
  })
};

/*   FIN ERROR MONTO PAGADO */


/*******   ERROR BAJA ALUMNOS *******/

ingresos.bajaGrupoAlumnos = ( id_grupo, id_alumno ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE gruposalumnos SET activo_sn = 0, fecha_baja = NOW()
      WHERE id_gruposalumnos > 0 AND id_grupo = ? AND id_alumno = ?;`,[ id_grupo, id_alumno ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve({ message: 'El ingreso se actualió correctamente'});
    });
  })
};

ingresos.bajaCargaEspecial = ( id_grupo, id_alumno ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`DELETE FROM alumnos_grupos_especiales_carga 
      WHERE id > 0  AND id_grupo = ? AND id_alumno = ?;`,[ id_grupo, id_alumno ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve({ message: 'El ingreso se actualió correctamente'});
    });
  })
};

ingresos.deleteSaldoFavor = ( id_grupo, id_alumno ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`DELETE FROM saldofavoralumnos 
      WHERE idsaldofavoralumnos > 0  AND id_grupo = ? AND id_alumno = ?;`,[ id_grupo, id_alumno ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve({ message: 'El ingreso se actualió correctamente'});
    });
  })
};

ingresos.addEgresoDevolucion = ( pago_anterior, id_autoriza, id_grupo, id_alumno, id_plantel ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO egresos(id_tipo_egreso, monto_egreso, comentarios, id_usuario_egreso, id_usuario_ultimo_cambio, id_alumno, id_grupo, id_plantel)
      VALUES(?,?,?,?,?,?,?,?)`, 
      [ 6 , pago_anterior, 'Devolución de dinero', id_autoriza, id_autoriza, id_alumno, id_grupo, id_plantel],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, id_autoriza, id_autoriza, id_alumno, id_grupo, id_plantel })
    })
  })
}

/*   FIN BAJA ALUMNO */
module.exports = ingresos;

