const { result }   = require("lodash");
// const sqlERP       = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");
const sqlINBI      = require("../dbINBI.js");
const sqlFAST      = require("../dbFAST.js");
const sqlERPNUEVO  = require("../db.js");


//const constructor
const comentarios = function(depas) {

};

/* CICLOS ACTUALES */
comentarios.cicloActualFAST = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT id AS cicloFast, iderp AS cicloFasterp FROM ciclos WHERE deleted = 0 AND CURRENT_DATE BETWEEN fecha_inicio AND fecha_fin LIMIT 1;`,(err, cicloActual) => {
      if(err){
        reject(err);
        return;
      }
      resolve(cicloActual[0])
    });
  });
};


comentarios.cicloActualINBI = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT id AS cicloInbi, iderp AS cicloInbierp FROM ciclos WHERE deleted = 0 AND CURRENT_DATE BETWEEN fecha_inicio AND fecha_fin LIMIT 1;`,(err, cicloActual) => {
      if(err){
        reject(err);
        return;
      }
      resolve(cicloActual[0])
    });
  });
};

comentarios.getCantidadComentarios = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT id_alumno, idperfil, unidad_negocio FROM comentario_alumno`,(err, cicloActual) => {
      if(err){
        reject(err);
        return;
      }
      resolve(cicloActual)
    });
  });
};



/****************************/
/* ALUMNOS DEL CICLO ACTUAL */
/****************************/

comentarios.getAlumnosCicloActualFast = ( ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT u.id AS id_alumno, UPPER(LTRIM(CONCAT(u.nombre, ' ', u.apellido_paterno, ' ', IFNULL(u.apellido_materno,'')))) AS alumno, g.id AS id_grupo, g.nombre AS grupo, u.usuario, c.id AS id_ciclo,
			gt.id_teacher, CONCAT(u1.nombre, ' ', u1.apellido_paterno, ' ', IFNULL(u1.apellido_materno,'')) AS teacher1, 
			gt.id_teacher_2,CONCAT(u2.nombre, ' ', u2.apellido_paterno, ' ', IFNULL(u2.apellido_materno,'')) AS teacher2, g.id_unidad_negocio, g.id_plantel FROM grupo_alumnos ga 
			LEFT JOIN grupos g ON g.id = ga.id_grupo
			LEFT JOIN usuarios u ON u.id = 
			ga.id_alumno
			LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
			LEFT JOIN ciclos c ON c.id = g.id_ciclo
			LEFT JOIN usuarios u1 ON u1.id = gt.id_teacher
			LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
			WHERE g.id_ciclo = ${ciclo} AND g.nombre NOT LIKE '%indu%' AND g.nombre NOT LIKE '%certi%' ORDER BY alumno;`,(err, alumnos) => {
      if(err){
        reject(err);
        return;
      }
      resolve(alumnos)
    });
  });
};


comentarios.getAlumnosCicloActualInbi = ( ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT u.id AS id_alumno, UPPER(LTRIM(CONCAT(u.nombre, ' ', u.apellido_paterno, ' ', IFNULL(u.apellido_materno,'')))) AS alumno, g.id AS id_grupo, g.nombre AS grupo, u.usuario, c.id AS id_ciclo,
			gt.id_teacher, CONCAT(u1.nombre, ' ', u1.apellido_paterno, ' ', IFNULL(u1.apellido_materno,'')) AS teacher1, 
			gt.id_teacher_2,CONCAT(u2.nombre, ' ', u2.apellido_paterno, ' ', IFNULL(u2.apellido_materno,'')) AS teacher2, g.id_unidad_negocio, g.id_plantel FROM grupo_alumnos ga 
			LEFT JOIN grupos g ON g.id = ga.id_grupo
			LEFT JOIN usuarios u ON u.id = 
			ga.id_alumno
			LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
			LEFT JOIN ciclos c ON c.id = g.id_ciclo
			LEFT JOIN usuarios u1 ON u1.id = gt.id_teacher
			LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
			WHERE g.id_ciclo = ${ciclo} AND g.nombre NOT LIKE '%indu%' AND g.nombre NOT LIKE '%certi%' ORDER BY alumno;`,(err, alumnos) => {
      if(err){
        reject(err);
        return;
      }
      resolve(alumnos)
    });
  });
};


/* AGREGAR UN COMENTARIO AL ALUMNO */

comentarios.addComentarioAlumno = (c, result) => {
  sqlERPNUEVO.query(`INSERT INTO comentario_alumno(id_alumno, id_usuarioerp, id_grupo, id_ciclo, comentario, idperfil, unidad_negocio, grupo, perfil) VALUES(?,?,?,?,?,?,?,?,?)`, 
	[ c.id_alumno, c.id_usuarioerp, c.id_grupo, c.id_ciclo, c.comentario, c.idperfil, c.unidad_negocio, c.grupo, c.perfil],
  (err, res) => {
    if (err) { result(err, null); return; }
    result(null, { id: res.insertId, ...c });
  })
}

comentarios.getComentarioAlumno = ( id_alumno, unidad_negocio ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM comentario_alumno WHERE id_alumno = ${ id_alumno } AND unidad_negocio = ${ unidad_negocio }`,(err, comentarios) => {
      if(err){
        reject(err);
        return;
      }
      resolve(comentarios)
    });
  });
};

/* GRUPOS DEL TEACHER */
comentarios.getAlumnosTeacherFast = ( email, ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT u.id AS id_alumno, CONCAT(u.nombre, ' ', u.apellido_paterno, ' ', IFNULL(u.apellido_materno,'')) AS alumno, g.id AS id_grupo, g.nombre AS grupo,
      g.id_unidad_negocio FROM grupo_alumnos ga 
      LEFT JOIN grupos g ON g.id = ga.id_grupo
      LEFT JOIN usuarios u ON u.id = 
      ga.id_alumno
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
      LEFT JOIN usuarios u1 ON u1.id = gt.id_teacher
      LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
      WHERE g.id_ciclo = ${ ciclo } AND u1.email = ?
      OR g.id_ciclo = ${ ciclo } AND u2.email = ?;`,[ email, email ],(err, alumnos) => {
      if(err){
        reject(err);
        return;
      }
      resolve(alumnos)
    });
  });
};


comentarios.getAlumnosTeacherInbi = ( email, ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT u.id AS id_alumno, CONCAT(u.nombre, ' ', u.apellido_paterno, ' ', IFNULL(u.apellido_materno,'')) AS alumno, g.id AS id_grupo, g.nombre AS grupo,
      g.id_unidad_negocio FROM grupo_alumnos ga 
      LEFT JOIN grupos g ON g.id = ga.id_grupo
      LEFT JOIN usuarios u ON u.id = 
      ga.id_alumno
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
      LEFT JOIN usuarios u1 ON u1.id = gt.id_teacher
      LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
      WHERE g.id_ciclo = ${ ciclo } AND u1.email = ?
      OR g.id_ciclo = ${ ciclo } AND u2.email = ?;`,[ email, email ],(err, alumnos) => {
      if(err){
        reject(err);
        return;
      }
      resolve(alumnos)
    });
  });
};

/* USUARIOS ACTIVOS */

comentarios.getUsuariosActivos = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM usuarios ORDER BY nombre_completo;`,(err, comentarios) => {
      if(err){
        reject(err);
        return;
      }
      resolve(comentarios)
    });
  });
};


module.exports = comentarios;

