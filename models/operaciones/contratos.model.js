const { result } = require("lodash");
const sqlERP  = require("../db3.js");
const sqlINBI = require("../dbINBI.js");
const sqlFAST = require("../dbFAST.js");


//const constructor
const contratos = function(depas) {

};

// General
/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/

contratos.busquedaContratoTeacher = ( id_usuario ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM folios_contratos WHERE id_usuario = ? `,[ id_usuario ], (err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};

contratos.addContratoTeacher = ( folio, id_usuario ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO folios_contratos( folio, estatus, deleted, id_usuario )VALUES(?,?,?,?)`,[ folio, 0, 0, id_usuario ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve( { id: res.insertId })
    })
  });
};

contratos.getContratos = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT f.*, UPPER(u.nombre_completo) AS nombre_completo FROM folios_contratos f
			LEFT JOIN usuarios u ON u.id_usuario = f.id_usuario
			WHERE f.deleted = 0;`,(err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};


contratos.existeContrato = ( folio ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM folios_contratos WHERE folio = ? AND deleted = 0;`,[ folio ],(err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};


contratos.existeContrato = ( folio ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM folios_contratos WHERE folio = ? AND deleted = 0;`,[ folio ],(err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};


contratos.actualizaDatosContrato = ( nombre_completo, direccion, folio ) => {
	return new Promise((resolve,reject)=>{
	  sqlERP.query(`UPDATE folios_contratos SET nombre_completo = ?, direccion = ?, estatus = 0 WHERE idfolios_contratos > 0 AND folio = ?`, [nombre_completo, direccion, folio],
	    (err, res) => {
	      if (err) { reject(err);return; }
	      if (res.affectedRows == 0) {
	        reject(res);
	        return;
	      }
	      resolve( res )
	  })
	})
}

contratos.getContrato = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT *, EXTRACT(DAY FROM NOW()) AS dia, MONTHNAME(NOW()) AS mes, SUBSTRING(EXTRACT(YEAR FROM NOW()),3,2) AS anio
      FROM folios_contratos WHERE idfolios_contratos = ? AND deleted = 0;`,[ id ],(err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response[0] )
    });
  });
};

contratos.updateEstatusContrato = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE folios_contratos SET estatus = 1 WHERE idfolios_contratos = ?`, [id],
      (err, res) => {
        if (err) { reject(err);return; }
        if (res.affectedRows == 0) {
          reject(res);
          return;
        }
        resolve( res )
    })
  })
}


module.exports = contratos;

