const { result } = require("lodash");
const sqlERPNUEVO = require("../db.js");

// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");
const sqlERP2      = require("../db3.js");


const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");

//const constructor
const whatsapp = function(depas) {

};

whatsapp.agregarMensajeWhatsApp = ( message_create, fullFileName ) => {

  var u = {
    fromMe                      : message_create._data.id.fromMe,
    remote                      : message_create._data.id.remote,
    mimetype                    : message_create._data.mimetype ? message_create._data.mimetype : '',
    filename                    : message_create._data.filename ? message_create._data.filename : '',
    filehash                    : message_create._data.filehash ? message_create._data.filehash : '',
    body                        : message_create._data.body,
    type                        : message_create._data.type,
    timestamp                   : message_create._data.t,
    notifyName                  : message_create._data.notifyName,
    lastPlaybackProgress        : message_create._data.lastPlaybackProgress,
    isMdHistoryMsg              : message_create._data.isMdHistoryMsg,
    stickerSentTs               : message_create._data.stickerSentTs,
    isAvatar                    : message_create._data.isAvatar,
    requiresDirectConnection    : message_create._data.requiresDirectConnection,
    pttForwardedFeaturesEnabled : message_create._data.pttForwardedFeaturesEnabled,
    isStatusV3                  : message_create._data.isStatusV3,
    mediaKey                    : message_create.mediaKey,
    hasMedia                    : message_create.hasMedia,
    from                        : message_create.from,
    to                          : message_create.to,
    author                      : message_create.author,
    deviceType                  : message_create.deviceType,
    hasQuotedMsg                : message_create.hasQuotedMsg,
    duration                    : message_create.duration,
    location                    : message_create.location,
    isGif                       : message_create.isGif,
    isEphemeral                 : message_create.isEphemeral,
    fullFileName
  }

  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query('INSERT INTO mensajes_whatsapp (`fromMe`, `remote`, `body`, `type`, `timestamp`, `notifyName`, `isMdHistoryMsg`, `stickerSentTs`, `isAvatar`, `requiresDirectConnection`, `pttForwardedFeaturesEnabled`, `isEphemeral`, `isStatusV3`, `mediaKey`, `hasMedia`, `froms`, `tos`, `author`, `deviceType`, `hasQuotedMsg`, `duration`, `location`, `isGif`, `mimetype`, `filename`, `filehash`, `fullFileName` )VALUES( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )', 
    	[ u.fromMe,u.remote,u.body,u.type,u.timestamp,u.notifyName,u.isMdHistoryMsg,u.stickerSentTs,u.isAvatar,u.requiresDirectConnection,u.pttForwardedFeaturesEnabled,u.isEphemeral,u.isStatusV3,u.mediaKey,u.hasMedia,u.from,u.to,u.author,u.deviceType,u.hasQuotedMsg,u.duration,u.location,u.isGif, u.mimetype, u.filename, u.filehash, u.fullFileName ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve({ id: res.insertId, ...u })
    })
  })
}

whatsapp.usuariosWhatsApp = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT u.id_usuario, CONCAT(UPPER(u.nombre_completo),' / ', p.plantel) AS nombre_completo, u.whatsappservidor, u.url_servidor, p.plantel,
      IF(LOCATE('FAST', p.plantel)>0, 2, 1) AS escuela FROM usuarios u
      LEFT JOIN planteles p ON p.id_plantel = u.id_plantel_oficial
      WHERE u.whatsappservidor ORDER BY nombre_completo;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

whatsapp.puestosUsuarios = ( idUsuarios ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT u.*, p.puesto FROM usuarios u
      LEFT JOIN puesto p ON p.idpuesto = u.idpuesto
      WHERE iderp IN(?);`,[ idUsuarios ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

whatsapp.estatusWhatsApp = ( whatsapps ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM sesiones_whatsapp
      WHERE whatsapp IN (?)
      AND deleted = 0;`,[ whatsapps ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}


whatsapp.chatsWhatsApp = ( whatsappservidor ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT froms, tos, IF(LOCATE('@g.us',froms)>0,'grupo','chat') AS tipoChat, notifyName, MAX(fecha_creacion) AS fecha_creacion 
      FROM mensajes_whatsapp
      WHERE fromMe = 0
      AND tos = ? AND tos NOT LIKE '%@g.us%' AND froms NOT LIKE '%@g.us%'
      GROUP BY froms, tos
      ORDER BY MAX(fecha_creacion) DESC;`,[ whatsappservidor ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}


whatsapp.chatsWhatsAppSinRespuesta = ( froms, whatsappservidor ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT froms AS tos, tos AS froms, IF(LOCATE('@g.us',froms)>0,'grupo','chat') AS tipoChat, notifyName, MAX(fecha_creacion) AS fecha_creacion 
      FROM mensajes_whatsapp
      WHERE fromMe = 1
      AND froms = ?
      AND tos NOT IN ( ? )
      AND tos NOT LIKE '%@g.us%'
      GROUP BY froms, tos
      ORDER BY MAX(fecha_creacion) DESC;`,[ whatsappservidor, froms ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}



whatsapp.ultimoMensajeChats = ( froms, whatsappservidor ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT m.froms, m.tos, IF(LOCATE('@g.us',m.froms)>0,'grupo','chat') AS tipoChat, m.notifyName, remote,
      MAX(fecha_creacion) max_time
      FROM mensajes_whatsapp m
      WHERE m.froms IN (?) AND m.tos = ?
      AND m.remote NOT LIKE '%@g.us%' 
      OR m.tos IN (?) AND m.froms = ?
      AND m.remote NOT LIKE '%@g.us%' 
      GROUP BY m.remote
      ORDER BY MAX(fecha_creacion) DESC;`,[ froms, whatsappservidor, froms, whatsappservidor ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

whatsapp.getMensajesWhatsApp = ( tos, froms, whatsappservidor ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT *, IF(LOCATE('@g.us',froms)>0,'grupo','chat') AS tipoChat 
      FROM mensajes_whatsapp 
      WHERE type NOT IN ('e2e_notification' , 'vcard', 'call_log')
      AND tos = ? AND froms = ?
      OR tos = ? AND froms = ?;`,[ whatsappservidor, froms, froms, whatsappservidor ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

whatsapp.getUsuariosWhaEscuela = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT CONCAT(nombre, ' ', apellido_paterno, ' ', IFNULL(apellido_materno,'')) AS alumno, whatsapp 
      FROM usuarios WHERE whatsapp;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

whatsapp.datosWhatsVendedora = (  id_usuario ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM usuarios WHERE id_usuario = ?;`,[ id_usuario ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

whatsapp.datosReciboPago = (  id_pago ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT a.id_alumno, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno,
      a.matricula, p.plantel, c.ciclo, cu.curso, ( i.monto_pagado + IFNULL(sal.saldoFavor,0)) AS monto_pagado, al.descuento, IF(LOCATE('FAST',p.plantel)>0,2,1) AS escuela,
      al.adeudo, u.nombre_completo, g.id_grupo, i.fecha_pago
      FROM ingresos i 
      LEFT JOIN alumnos a ON a.id_alumno = i.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN cursos cu ON cu.id_curso = g.id_curso
      LEFT JOIN alumnos_grupos_especiales_carga al ON al.id_alumno = i.id_alumno AND al.id_grupo = i.id_grupo
      LEFT JOIN usuarios u ON u.id_usuario = i.id_usuario_ultimo_cambio
      LEFT JOIN (SELECT IFNULL( SUM(IF(tipo = 1, monto, monto * -1)),0) AS saldoFavor, id_alumno, id_grupo 
      FROM saldofavoralumnos ss1 GROUP BY id_alumno) sal ON sal.id_alumno = i.id_alumno
      WHERE i.id_ingreso = ?;`,[ id_pago ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

whatsapp.datosWhatsAlumno = (  id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM alumnos WHERE id_alumno = ?;`,[ id_alumno ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

whatsapp.existeAlertaDesconeta = (  froms ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM sesiones_whatsapp WHERE whatsapp = ? AND deleted = 0;`,[ froms ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

whatsapp.addAlertaDesconecta = (  froms ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query('INSERT INTO sesiones_whatsapp ( whatsapp ) VALUES ( ? )', [ froms ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve({ id: res.insertId, froms })
    })
  })
}

whatsapp.updateAlertaWha = ( from ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE sesiones_whatsapp SET deleted = 1, notificacion = 1  WHERE idsesiones_whatsapp > 0 AND whatsapp = ?;`, [ from ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage }) }
      resolve({ from });
    });
  });
};

whatsapp.updateWhaLMS = ( escuela, id_alumno, telefonoLMS ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve, reject) => {
    db.query(`UPDATE usuarios SET whatsapp = ? WHERE id = ?;`, [ telefonoLMS, id_alumno ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage }) }
      resolve({ escuela, id_alumno, telefonoLMS });
    });
  });
};

whatsapp.updateWhaERP = ( iderp, nuevoTelefono ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE alumnos SET telefono = ?, celular = ?  WHERE id_alumno = ?;`, [ nuevoTelefono, nuevoTelefono, iderp ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage }) }
      resolve({ iderp, nuevoTelefono });
    });
  });
};


whatsapp.getFolioProspecto = (  folio ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM prospectos WHERE folio LIKE '%${folio}%' OR telefono LIKE '%${folio}%';`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve(res[0])
    })
  })
}


whatsapp.getMensajesWhaProspecto = (  telefono ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT *, IF(LOCATE('${telefono}',froms)>0,1,0) AS mio FROM mensajes_whatsapp WHERE froms LIKE '%${telefono}%' OR tos LIKE '%${telefono}%';`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve(res)
    })
  })
}

whatsapp.getMensajesIdWhat = (  idWha, telefonos ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT *, IF(LOCATE('${idWha}',froms)>0,1,0) AS mio FROM mensajes_whatsapp 
      WHERE froms LIKE '%${idWha}%' ${ idWha.match( '@c.us' ) ? ` AND remote NOT LIKE "%@g.us%" `  : ` ` }
      AND tos IN ( ? ) AND remote NOT LIKE "%@g.us%"
      OR tos LIKE '%${idWha}%' ${ idWha.match( '@c.us' ) ? ` AND remote NOT LIKE "%@g.us%" ` : ` ` } 
      AND froms IN ( ? ) AND remote NOT LIKE "%@g.us%"
      ORDER BY idmensajes_whatsapp DESC
      LIMIT 100`,[ telefonos, telefonos ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve(res)
    })
  })
}


// Asignación de vendedora
whatsapp.existeRegistro = (  id_whatsapp ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM acceso_whats WHERE id_whatsapp = ?;`,[ id_whatsapp ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

whatsapp.addRegistro = (  id_vendedora, id_whatsapp, estatus ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query('INSERT INTO acceso_whats ( id_vendedora, id_whatsapp, estatus ) VALUES ( ?, ?, ? )', [ id_vendedora, id_whatsapp, estatus ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve({ id: res.insertId, id_vendedora, id_whatsapp, estatus })
    })
  })
}

whatsapp.updateRegistro = ( id_vendedora, id_whatsapp, estatus, idacceso_whats ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE acceso_whats SET id_vendedora = ?, id_whatsapp = ?, estatus = ? WHERE idacceso_whats = ?;`, [ id_vendedora, id_whatsapp, estatus, idacceso_whats ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage }) }
      resolve({ id_vendedora, id_whatsapp, estatus, idacceso_whats });
    });
  });
};


whatsapp.getAsignacion = ( id_whatsapp ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM acceso_whats WHERE id_whatsapp = ? AND deleted = 0; `,[ id_whatsapp ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}


whatsapp.getWhatsAppAsignados = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM acceso_whats WHERE id_vendedora = ? AND deleted = 0; `,[ id ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

module.exports = whatsapp;

