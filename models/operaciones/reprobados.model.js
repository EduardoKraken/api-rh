const { result } = require("lodash");
// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");
const sqlINBI = require("../dbINBI.js");
const sqlFAST = require("../dbFAST.js");
const sqlFastExRe = require("../dbFastExRe.js");

//const constructor
const reprobados = function(depas) {

};

// General
/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/

reprobados.getAlumnosFAST = ( cicloFast ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id AS id_grupo, ga.id_alumno, ga.id_curso, g.nombre AS grupo, g.id_unidad_negocio, g.id_plantel, p.nombre AS plantel, g.deleted AS cerrado,
    CONCAT(u2.nombre, ' ', u2.apellido_paterno, ' ', IFNULL(u2.apellido_materno,'')) AS teacher,
    CONCAT(u3.nombre, ' ', u3.apellido_paterno, ' ', IFNULL(u3.apellido_materno,'')) AS teacher2,
		CONCAT(u.nombre,' ', u.apellido_paterno, ' ', IFNULL(u.apellido_materno,'')) AS alumno, u.usuario  FROM grupo_alumnos ga
		LEFT JOIN grupos g ON g.id = ga.id_grupo
		LEFT JOIN cursos c ON c.id = g.id_curso
		LEFT JOIN usuarios u ON u.id = ga.id_alumno
		LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
		LEFT JOIN planteles p ON p.id = g.id_plantel
    LEFT JOIN grupo_teachers gt ON g.id = gt.id_grupo
    LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher
		LEFT JOIN usuarios u3 ON u3.id = gt.id_teacher_2
			WHERE ci.iderp = ${ cicloFast } AND g.editado = 1
			AND g.nombre NOT LIKE '%CERTI%'
			AND g.nombre NOT LIKE '%INDU%'
			AND g.nombre NOT LIKE '%EXCI%';`, (err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
}

reprobados.getCalifAsistencias = ( cicloFast ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT a.id_usuario, (c.valor_asistencia - (TRUNCATE((c.valor_asistencia /c.total_dias_laborales),1) * COUNT(a.valor_asistencia))) AS inasistencias, c.valor_asistencia  FROM asistencia_grupo a 
			LEFT JOIN grupos g ON g.id = a.id_grupo
			LEFT JOIN cursos c ON c.id = g.id_curso
			LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
			WHERE ci.iderp = ${ cicloFast } AND g.editado = 1
			AND g.nombre NOT LIKE '%CERTI%'
			AND g.nombre NOT LIKE '%INDU%'
			AND g.nombre NOT LIKE '%EXCI%'
			AND a.valor_asistencia IN(0,2)
			GROUP BY a.id_usuario;`, (err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};

reprobados.getCalifEjerciciosFast = ( cicloFast ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT  c.id_alumno, (cu.num_ejercicios - COUNT(c.id_alumno)) AS cantEjercicios,
			TRUNCATE((((COUNT(c.id_alumno) * TRUNCATE((cu.valor_ejercicios /cu.num_ejercicios),1)) * AVG(calificacion)) / 100),2) AS puntos,
			(((cu.num_ejercicios - COUNT(c.id_alumno)) * TRUNCATE((cu.valor_ejercicios /cu.num_ejercicios),1)) + TRUNCATE((((COUNT(c.id_alumno) * TRUNCATE((cu.valor_ejercicios /cu.num_ejercicios),1)) * AVG(calificacion)) / 100),2)) AS posibles FROM control_grupos_calificaciones_evaluaciones c
			LEFT JOIN grupos g ON g.id = c.id_grupo
			LEFT JOIN cursos cu ON cu.id = g.id_curso
			LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
			LEFT JOIN recursos r ON r.id_evaluacion = c.id_evaluacion
			WHERE ci.iderp = ${ cicloFast } AND g.editado = 1
			AND g.nombre NOT LIKE '%CERTI%'
			AND g.nombre NOT LIKE '%INDU%' 
			AND g.nombre NOT LIKE '%EXCI%'
			AND r.tipo_recurso = 1 AND r.id_tipo_evaluacion = 1 AND r.id_categoria_evaluacion = 1 AND r.deleted = 0
			GROUP BY c.id_alumno;`, (err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};

reprobados.getCalifExamenesFast = ( cicloFast ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT  c.id_alumno, (cu.num_examenes - COUNT(c.id_alumno)) AS cantExamenes,
			TRUNCATE((((COUNT(c.id_alumno) * TRUNCATE((cu.valor_examenes /cu.num_examenes),1)) * AVG(calificacion)) / 100),2) AS puntos,
			(((cu.num_examenes - COUNT(c.id_alumno)) * TRUNCATE((cu.valor_examenes /cu.num_examenes),1)) + TRUNCATE((((COUNT(c.id_alumno) * TRUNCATE((cu.valor_examenes /cu.num_examenes),1)) * AVG(calificacion)) / 100),2)) AS posibles FROM control_grupos_calificaciones_evaluaciones c
			LEFT JOIN grupos g ON g.id = c.id_grupo
			LEFT JOIN cursos cu ON cu.id = g.id_curso
			LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
			LEFT JOIN recursos r ON r.id_evaluacion = c.id_evaluacion
			WHERE ci.iderp = ${ cicloFast } AND g.editado = 1
			AND g.nombre NOT LIKE '%CERTI%'
			AND g.nombre NOT LIKE '%INDU%' 
			AND g.nombre NOT LIKE '%EXCI%'
			AND r.tipo_recurso = 1 AND r.id_tipo_evaluacion = 2 AND r.deleted = 0
			GROUP BY c.id_alumno;`, (err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};

reprobados.getCalifExtraordinarioFast = ( cicloFast ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT  c.id_alumno, calificacion FROM control_grupos_calificaciones_evaluaciones c
			LEFT JOIN grupos g ON g.id = c.id_grupo
			LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
			LEFT JOIN recursos r ON r.id_evaluacion = c.id_evaluacion
			WHERE ci.iderp = ${ cicloFast } AND g.editado = 1
			AND g.nombre NOT LIKE '%CERTI%'
			AND g.nombre NOT LIKE '%INDU%' 
			AND g.nombre NOT LIKE '%EXCI%'
			AND r.tipo_recurso = 1 AND r.id_tipo_evaluacion = 3 AND r.deleted = 0
			GROUP BY c.id_alumno;`, (err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};


reprobados.getAlumnosInbi = ( cicloInbi ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id AS id_grupo, ga.id_alumno, ga.id_curso, g.nombre AS grupo, g.id_unidad_negocio, g.id_plantel, p.nombre AS plantel, g.deleted AS cerrado,
	    CONCAT(u2.nombre, ' ', u2.apellido_paterno, ' ', IFNULL(u2.apellido_materno,'')) AS teacher,
	    CONCAT(u3.nombre, ' ', u3.apellido_paterno, ' ', IFNULL(u3.apellido_materno,'')) AS teacher2,
			CONCAT(u.nombre,' ', u.apellido_paterno, ' ', IFNULL(u.apellido_materno,'')) AS alumno  FROM grupo_alumnos ga
			LEFT JOIN grupos g ON g.id = ga.id_grupo
			LEFT JOIN cursos c ON c.id = g.id_curso
			LEFT JOIN usuarios u ON u.id = ga.id_alumno
			LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
			LEFT JOIN planteles p ON p.id = g.id_plantel
	    LEFT JOIN grupo_teachers gt ON g.id = gt.id_grupo
	    LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher
			LEFT JOIN usuarios u3 ON u3.id = gt.id_teacher_2
			WHERE ci.iderp = ${ cicloInbi } AND g.editado = 1
			AND g.nombre NOT LIKE '%CERTI%'
			AND g.nombre NOT LIKE '%INDU%'
			AND g.nombre NOT LIKE '%EXCI%';`, (err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
}

reprobados.getCalifAsistenciasInbi = ( cicloInbi ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT a.id_usuario, (c.valor_asistencia - (TRUNCATE((c.valor_asistencia /c.total_dias_laborales),1) * COUNT(a.valor_asistencia))) AS inasistencias, c.valor_asistencia  FROM asistencia_grupo a 
			LEFT JOIN grupos g ON g.id = a.id_grupo
			LEFT JOIN cursos c ON c.id = g.id_curso
			LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
			WHERE ci.iderp = ${ cicloInbi } AND g.editado = 1
			AND g.nombre NOT LIKE '%CERTI%'
			AND g.nombre NOT LIKE '%INDU%'
			AND g.nombre NOT LIKE '%EXCI%'
			AND a.valor_asistencia IN(0,2)
			GROUP BY a.id_usuario;`, (err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};

reprobados.getCalifEjerciciosInbi = ( cicloInbi ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT  c.id_alumno, (cu.num_ejercicios - COUNT(c.id_alumno)) AS cantEjercicios,
			TRUNCATE((((COUNT(c.id_alumno) * TRUNCATE((cu.valor_ejercicios /cu.num_ejercicios),1)) * AVG(calificacion)) / 100),2) AS puntos,
			(((cu.num_ejercicios - COUNT(c.id_alumno)) * TRUNCATE((cu.valor_ejercicios /cu.num_ejercicios),1)) + TRUNCATE((((COUNT(c.id_alumno) * TRUNCATE((cu.valor_ejercicios /cu.num_ejercicios),1)) * AVG(calificacion)) / 100),2)) AS posibles FROM control_grupos_calificaciones_evaluaciones c
			LEFT JOIN grupos g ON g.id = c.id_grupo
			LEFT JOIN cursos cu ON cu.id = g.id_curso
			LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
			LEFT JOIN recursos r ON r.id_evaluacion = c.id_evaluacion
			WHERE ci.iderp = ${ cicloInbi } AND g.editado = 1
			AND g.nombre NOT LIKE '%CERTI%'
			AND g.nombre NOT LIKE '%INDU%' 
			AND g.nombre NOT LIKE '%EXCI%'
			AND r.tipo_recurso = 1 AND r.id_tipo_evaluacion = 1 AND r.id_categoria_evaluacion = 1 AND r.deleted = 0
			GROUP BY c.id_alumno;`, (err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};

reprobados.getCalifExamenesInbi = ( cicloInbi ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT  c.id_alumno, (cu.num_examenes - COUNT(c.id_alumno)) AS cantExamenes,
			TRUNCATE((((COUNT(c.id_alumno) * TRUNCATE((cu.valor_examenes /cu.num_examenes),1)) * AVG(calificacion)) / 100),2) AS puntos,
			(((cu.num_examenes - COUNT(c.id_alumno)) * TRUNCATE((cu.valor_examenes /cu.num_examenes),1)) + TRUNCATE((((COUNT(c.id_alumno) * TRUNCATE((cu.valor_examenes /cu.num_examenes),1)) * AVG(calificacion)) / 100),2)) AS posibles FROM control_grupos_calificaciones_evaluaciones c
			LEFT JOIN grupos g ON g.id = c.id_grupo
			LEFT JOIN cursos cu ON cu.id = g.id_curso
			LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
			LEFT JOIN recursos r ON r.id_evaluacion = c.id_evaluacion
			WHERE ci.iderp = ${ cicloInbi } AND g.editado = 1
			AND g.nombre NOT LIKE '%CERTI%'
			AND g.nombre NOT LIKE '%INDU%' 
			AND g.nombre NOT LIKE '%EXCI%'
			AND r.tipo_recurso = 1 AND r.id_tipo_evaluacion = 2 AND r.deleted = 0
			GROUP BY c.id_alumno;`, (err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};

reprobados.getCalifExtraordinarioInbi = ( cicloInbi ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT  c.id_alumno, calificacion FROM control_grupos_calificaciones_evaluaciones c
			LEFT JOIN grupos g ON g.id = c.id_grupo
			LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
			LEFT JOIN recursos r ON r.id_evaluacion = c.id_evaluacion
			WHERE ci.iderp = ${ cicloInbi } AND g.editado = 1
			AND g.nombre NOT LIKE '%CERTI%'
			AND g.nombre NOT LIKE '%INDU%' 
			AND g.nombre NOT LIKE '%EXCI%'
			AND r.tipo_recurso = 1 AND r.id_tipo_evaluacion = 3 AND r.deleted = 0
			GROUP BY c.id_alumno;`, (err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};

reprobados.getNotasALumnoFast = ( id_grupo, id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM notas WHERE id_alumno = ${ id_alumno } AND id_grupo = ${ id_grupo } `, (err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};

reprobados.getNotasALumnoInbi = ( id_grupo, id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM notas WHERE id_alumno = ${ id_alumno } AND id_grupo = ${ id_grupo } `, (err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};

reprobados.addNotaFast = ( id_usuario, id_grupo, id_alumno, nota, tipo_usuario ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`INSERT INTO notas ( id_usuario, id_grupo, nota, id_alumno, tipo_usuario) VALUES (?,?,?,?,?)`, 
    	[ id_usuario, id_grupo, nota, id_alumno, tipo_usuario ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve({ id_ciclo: res.insertId, id_usuario, id_grupo, nota, id_alumno})
    })
  });
};


reprobados.addNotaInbi = (id_usuario, id_grupo, id_alumno, nota, tipo_usuario) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`INSERT INTO notas ( id_usuario, id_grupo, nota, id_alumno, tipo_usuario) VALUES (?,?,?,?,?)`,
      [ id_usuario, id_grupo, nota, id_alumno, tipo_usuario ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve({ id_ciclo: res.insertId, id_usuario, id_grupo, nota, id_alumno})
    })
  });
};

reprobados.getGruposFastTeacher = ( email, fast ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT gt.id_grupo, g.id_unidad_negocio FROM grupo_teachers gt
			LEFT JOIN grupos g ON g.id = gt.id_grupo
			LEFT JOIN ciclos c ON c.id = g.id_ciclo
			LEFT JOIN usuarios u ON u.id = gt.id_teacher
			LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
			WHERE c.iderp = ${ fast }
			AND u.email = ?
			OR c.iderp = ${ fast }
			AND u2.email = ?; `,[ email, email ],(err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};

reprobados.getGruposInbiTeacher = ( email, inbi ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT gt.id_grupo, g.id_unidad_negocio FROM grupo_teachers gt
			LEFT JOIN grupos g ON g.id = gt.id_grupo
			LEFT JOIN ciclos c ON c.id = g.id_ciclo
			LEFT JOIN usuarios u ON u.id = gt.id_teacher
			LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
			WHERE c.iderp = ${ inbi }
			AND u.email = ?
			OR c.iderp = ${ inbi }
			AND u2.email = ?; `,[ email, email ],(err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};

reprobados.getUsuariosERP = ( ) => {
	return new Promise((resolve,reject)=>{
	  sqlERP.query(`SELECT id_usuario, nombre_completo FROM usuarios WHERE activo_sn = 1 ORDER BY nombre_completo ASC `, (err, res) => {
	    if(err){
        reject(err);
        return;
      }
      resolve( res )
	  })
	});
};


reprobados.getAlumnosFast2 = ( idciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT u.id, CONCAT(u.nombre, ' ',u.apellido_paterno, ' ',IFNULL(u.apellido_materno,'')) AS nombre,
				u.iderp,u.usuario,g.nombre AS grupo FROM grupo_alumnos ga
				LEFT JOIN grupos g ON g.id = ga.id_grupo
				LEFT JOIN ciclos c ON c.id = g.id_ciclo
				LEFT JOIN usuarios u ON u.id = ga.id_alumno
				WHERE c.iderp = ?
				AND g.deleted = 0 AND g.nombre NOT LIKE '%CERTI%' AND g.nombre NOT LIKE '%INDU%';`,[ idciclo ],(err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};

reprobados.getAlumnosInbi2 = ( idciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT u.id, CONCAT(u.nombre, ' ',u.apellido_paterno, ' ',IFNULL(u.apellido_materno,'')) AS nombre,
				u.iderp,u.usuario,g.nombre AS grupo FROM grupo_alumnos ga
				LEFT JOIN grupos g ON g.id = ga.id_grupo
				LEFT JOIN ciclos c ON c.id = g.id_ciclo
				LEFT JOIN usuarios u ON u.id = ga.id_alumno
				WHERE c.iderp = ?
				AND g.deleted = 0 AND g.nombre NOT LIKE '%CERTI%' AND g.nombre NOT LIKE '%INDU%';`,[ idciclo ],(err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};


reprobados.getAlumnosFechaPagoFast = ( ids, idciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT a.id, a.id_usuario, a.fecha_pago FROM asistencia_grupo a
			LEFT JOIN grupos g ON g.id = a.id_grupo
			LEFT JOIN ciclos c ON c.id = g.id_ciclo
			WHERE c.iderp = ? AND id_usuario IN (?)
			AND a.fecha_pago IS NOT NULL
			AND g.nombre NOT LIKE '%CERTI%' AND g.nombre NOT LIKE '%INDU%';`,[ idciclo, ids ],(err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};

reprobados.getAlumnosFechaPagoInbi = ( ids, idciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT a.id, a.id_usuario, a.fecha_pago FROM asistencia_grupo a
			LEFT JOIN grupos g ON g.id = a.id_grupo
			LEFT JOIN ciclos c ON c.id = g.id_ciclo
			WHERE c.iderp = ? 
			AND a.fecha_pago IS NOT NULL
			AND g.deleted = 0 AND g.nombre NOT LIKE '%CERTI%' AND g.nombre NOT LIKE '%INDU%';`,[ idciclo, ids ],(err,  response) => {
      if(err){
        reject(err);
        return;
      }
      resolve( response )
    });
  });
};


module.exports = reprobados;

