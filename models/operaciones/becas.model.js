const { result }  = require("lodash");
// Original
// const sqlERP      = require("../db3.js");

// Prueba
const sqlERP      = require("../dbErpPrueba.js");
const sqlERPNUEVO = require("../db.js");

//const constructor
const becas = function(becas) {};

// VERIFICAR SI EL ALUMNO TIENE UNA BECA PARA EL GRUPO
becas.getBecaGrupoAlumno = ( id_alumno, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT fb.*, b.beca AS becaTotal FROM fyn_proc_becas_prospectos_alumnos fb
			LEFT JOIN fyn_proc_becas b ON b.id = fb.id_beca
			WHERE fb.id_alumno = ? AND fb.id_grupo = ?;`,[ id_alumno, id_grupo ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      const respuesta = res.length > 0 ? res[0] : null
      resolve(respuesta)
    })
  })
}

// Agregar al alumno en el grupo
becas.solicitarBeca = ( id_alumno, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO fyn_proc_becas_prospectos_alumnos(id_alumno,id_grupo,id_beca,aprobadoSN,comentarios,tipo_registro,id_usuario_ultimo_cambio)VALUES(?,?,4,1,'BECA EMPLEADO',1,334)`, 
    [id_alumno, id_grupo],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve({ id: res.insertId, id_alumno, id_grupo })
    })
  })
}

// VER SI TIENE BECAS RESTANTES EL ALUMNO
becas.becasRestantes = ( id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT fb.*, b.beca AS becaTotal FROM fyn_proc_becas_prospectos_alumnos fb
      LEFT JOIN fyn_proc_becas b ON b.id = fb.id_beca
      WHERE fb.id_alumno = ? AND fb.tipo_registro = 4
      AND cantidad - (SELECT COUNT(*) FROM gruposalumnos g WHERE g.id_alumno = ? AND g.idbecas = fb.id) > 0;`, [ id_alumno, id_alumno ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve(res[0])
    })
  })
}


// Listar todas las becas solicitadas
becas.getBecasAll = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT b.id, b.id_alumno, b.id_grupo, b.id_beca, b.aprobadoSN, b.comentarios, b.tipo_registro, b.fecha_alta,
      CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, g.grupo, p.beca,
      b.cantidad, IFNULL(IF((b.cantidad - bg.cant )<0,'',( b.cantidad - bg.cant )),'') AS restan
      FROM fyn_proc_becas_prospectos_alumnos b
      LEFT JOIN alumnos a ON a.id_alumno = b.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = b.id_grupo
      LEFT JOIN fyn_proc_becas p ON p.id = b.id_beca
      LEFT JOIN (SELECT id_alumno, COUNT(*) AS cant FROM gruposalumnos WHERE idbecas > 0 GROUP BY id_alumno) bg ON bg.id_alumno = b.id_alumno
      ORDER BY fecha_alta DESC;`,(err, res) => {

      if(err){ return reject({ message: err.sqlMessage }); }

      resolve(res)
    })
  })
}


module.exports = becas;