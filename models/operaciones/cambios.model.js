const { result } = require("lodash");
// const sqlERP     = require("../db3.js");
const sqlINBI    = require("../dbINBI.js");
const sqlFAST    = require("../dbFAST.js");
const sqlERP     = require("../dbErpPrueba.js");


//const constructor
const cambios = function(depas) {

};

// General
cambios.getTipoCambios = (result) => {
  sqlERP.query(`SELECT * FROM tipocambio WHERE activo = 1;`,(err, res) => {
    if(err){
      result(err,null);
      return;
    }
    result(null, res)
  });
};

// Recepcionista
cambios.cambiosGruposxCiclo = (tipociclo,result) => {
  sqlERP.query(`SELECT * FROM grupos WHERE id_ciclo IN(SELECT id_ciclo FROM ciclos
		WHERE ciclo NOT LIKE '%inve%' AND ciclo NOT LIKE '%cambios%' AND ciclo NOT LIKE '%campa%' AND ciclo NOT LIKE '%capa%' AND ciclo NOT LIKE '%entrena%'
		AND ${ tipociclo } AND activo_sn = 1);`,(err, res) => {
    if(err){
      result(err,null);
      return;
    }
    result(null, res)
  });
};

cambios.cambiosNuevoGruposxAlumno = (tipociclo,id_ciclo,result) => {
  sqlERP.query(`SELECT * FROM grupos WHERE activo_sn = 1 AND  id_ciclo IN(SELECT id_ciclo FROM ciclos
    WHERE ciclo NOT LIKE '%inve%' AND ciclo NOT LIKE '%cambios%' AND ciclo NOT LIKE '%campa%' AND ciclo NOT LIKE '%capa%' AND ciclo NOT LIKE '%entrena%'
    AND ${ tipociclo } AND activo_sn = 1 
    AND CURRENT_DATE BETWEEN fecha_inicio_ciclo AND fecha_fin_ciclo)
    OR activo_sn = 1 AND  id_ciclo IN(SELECT id_ciclo FROM ciclos
    WHERE ciclo NOT LIKE '%inve%' AND ciclo NOT LIKE '%cambios%' AND ciclo NOT LIKE '%campa%' AND ciclo NOT LIKE '%capa%' AND ciclo NOT LIKE '%entrena%'
    AND ${ tipociclo } AND activo_sn = 1
    AND fecha_inicio_ciclo >= CURRENT_DATE )
    ORDER BY id_plantel, id_nivel, id_ciclo;`,[id_ciclo],(err, res) => {
    if(err){
      result(err,null);
      return;
    }
    result(null, res)
  });
};

cambios.getAlumnoxGrupo = (tipociclo,result) => {
  sqlERP.query(`SELECT g.id_grupo, a.id_alumno,matricula, UPPER(CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,""))) AS alumno,
    g.id_grupo, g.grupo, ga.monto_pagado_total, g.id_nivel, n.nivel, p.id_plantel, p.plantel, g.id_ciclo FROM gruposalumnos ga
    LEFT JOIN grupos  g ON g.id_grupo = ga.id_grupo
    LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
    LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
    LEFT JOIN niveles n ON n.id_nivel = g.id_nivel
    WHERE g.id_ciclo IN(SELECT id_ciclo FROM ciclos
    WHERE activo_sn = 1 AND ciclo NOT LIKE '%inve%' AND ${ tipociclo } AND ciclo NOT LIKE '%cambios%'  AND ciclo NOT LIKE '%campa%' AND ciclo NOT LIKE '%capa%' AND ciclo NOT LIKE '%entrena%') 
    GROUP BY alumno
    ORDER BY alumno ;`,(err, res) => {
    if(err){
      result(err,null);
      return;
    }
    result(null, res)
  });
};

cambios.addCambios = (z, result) => {
  sqlERP.query(`INSERT INTO solicitud_cambio(id_alumno, idtipo_cambio, solicito_cambio, id_plantel, id_grupo_actual, id_grupo_nuevo, estatus, diferencia, motivo ) VALUES(?,?,?,?,?,?,?,?, ?)`, 
  	[z.id_alumno, z.idtipo_cambio, z.solicito_cambio, z.id_plantel, z.id_grupo_actual, z.id_grupo_nuevo, z.estatus, z.diferencia, z.motivo],
    (err, res) => {
      if (err) { result(err, null); return; }
      result(null, { id: res.insertId, ...z });
    }
  )
}

cambios.getCambiosUsuario = (idusuario,result) => {
  sqlERP.query(`SELECT a.id_alumno,matricula, UPPER(CONCAT(a.nombre, " ", a.apellido_paterno, IFNULL(a.apellido_materno,""))) AS alumno,
		g.id_grupo, g.grupo, g.id_nivel, n.nivel, p.id_plantel, p.plantel, sc.*, gn.grupo AS nuevo_grupo,t.tipocambio, sc.motivo,
    (ELT(sc.estatus, 'Solicitado', 'Por aceptar', 'Rechazado', 'Aceptado')) AS estatus_nombre FROM solicitud_cambio sc
		LEFT JOIN grupos g ON g.id_grupo = sc.id_grupo_actual
    LEFT JOIN grupos gn ON gn.id_grupo = sc.id_grupo_nuevo
		LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
		LEFT JOIN alumnos a ON a.id_alumno = sc.id_alumno
		LEFT JOIN niveles n ON n.id_nivel = g.id_nivel
		LEFT JOIN tipocambio t ON t.idtipocambio = sc.idtipo_cambio 
    WHERE sc.solicito_cambio = ?;`,[idusuario],(err, res) => {
    if(err){
      result(err, null);
      return;
    }
    result(null, res)
  });
};

cambios.getCambiosAll = (result) => {
  sqlERP.query(`SELECT a.id_alumno,matricula, UPPER(CONCAT(a.nombre, " ", a.apellido_paterno, IFNULL(a.apellido_materno,""))) AS alumno,
    g.id_grupo, g.grupo, g.id_nivel, n.nivel, p.id_plantel, p.plantel, sc.*, gn.grupo AS nuevo_grupo,t.tipocambio, sc.motivo,
        u.nombre_completo AS responsable,
    (ELT(sc.estatus, 'Solicitado', 'Por aceptar', 'Rechazado', 'Aceptado')) AS estatus_nombre FROM solicitud_cambio sc
    LEFT JOIN grupos g ON g.id_grupo = sc.id_grupo_actual
    LEFT JOIN grupos gn ON gn.id_grupo = sc.id_grupo_nuevo
    LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
    LEFT JOIN alumnos a ON a.id_alumno = sc.id_alumno
        LEFT JOIN usuarios u ON u.id_usuario = sc.solicito_cambio
    LEFT JOIN tipocambio t ON t.idtipocambio = sc.idtipo_cambio 
    LEFT JOIN niveles n ON n.id_nivel = g.id_nivel;`,(err, res) => {
    if(err){
      result(err, null);
      return;
    }
    result(null, res)
  });
};

cambios.updateCambios = (id, z, result) => {
  console.log( z )
  sqlERP.query(`UPDATE solicitud_cambio SET estatus = ?, fecha_cambio = ?, whatsapp = ?, lms = ?, zoom = ? , realizo_cambio = ?, fecha_aceptacion = ?  WHERE idsolicitud_cambio = ?`, 
    [z.estatus, z.fecha_cambio, z.whatsapp, z.lms, z.zoom, z.realizo_cambio, z.fecha_aceptacion, z.idsolicitud_cambio],
    (err, res) => {
      if (err) {  result(null, err); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id, ...z });
  })
}

cambios.cambiosMensaje = (z, result) => {
  sqlERP.query(`INSERT INTO mensaje_cambios(idsolicitud_cambio, mensaje, respuesta, envia) VALUES(?,?,?,?)`, 
  	[z.idsolicitud_cambio, z.mensaje, z.respuesta, z.envia],
    (err, res) => {
      if (err) { result(err, null); return; }
      result(null, { id: res.insertId, ...z });
    }
  )
}

cambios.cambiosGetMensaje = (id,result) => {
  sqlERP.query(`SELECT * FROM mensaje_cambios WHERE idsolicitud_cambio = ?;`,[id],(err, res) => {
    if(err){
      result(err, null);
      return;
    }
    result(null, res)
  });
};

cambios.getGruposUsuario = (idusuario ,result) => {
  sqlERP.query(`SELECT g.id_grupo, 
    g.id_grupo, g.grupo, ga.monto_pagado_total, g.id_nivel, n.nivel, p.id_plantel, p.plantel, g.id_ciclo, ac.precio_grupo, ac.descuento, ac.precio_grupo_con_descuento_x_alumno, ac.pagado 
    FROM gruposalumnos ga
    LEFT JOIN grupos  g ON g.id_grupo = ga.id_grupo
    LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
    LEFT JOIN niveles n ON n.id_nivel = g.id_nivel
    LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
    LEFT JOIN alumnos_grupos_especiales_carga ac ON ac.id_alumno = ga.id_alumno AND ac.id_grupo = g.id_grupo
    WHERE g.id_ciclo IN(SELECT id_ciclo FROM ciclos
      WHERE activo_sn = 1 
      AND ciclo NOT LIKE '%inve%' AND ciclo NOT LIKE '%cambios%' AND ciclo NOT LIKE '%campa%' AND ciclo NOT LIKE '%capa%' AND ciclo NOT LIKE '%entrena%'
    )
    AND CURRENT_DATE BETWEEN fecha_inicio_ciclo AND fecha_fin_ciclo
    AND ga.id_alumno = ?
    OR g.id_ciclo IN(SELECT id_ciclo FROM ciclos
      WHERE activo_sn = 1 
      AND ciclo NOT LIKE '%inve%' AND ciclo NOT LIKE '%cambios%' AND ciclo NOT LIKE '%campa%' AND ciclo NOT LIKE '%capa%' AND ciclo NOT LIKE '%entrena%'
    )
    AND ga.id_alumno = ?
    ORDER BY p.plantel, g.id_nivel;`,[ idusuario, idusuario ],(err, res) => {
    if(err){
      result(err,null);
      return;
    }
    result(null, res)
  });
};

/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/

cambios.reporteTeacherAsistenciaFast1 = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id, g.nombre,g.id_curso,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher, a.fecha_asistencia,
      (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) AS dia
      FROM asistencia_grupo a
      LEFT JOIN grupos g ON g.id = a.id_grupo 
      LEFT JOIN grupo_teachers gt1 ON gt1.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt1.id_teacher
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      WHERE g.id_ciclo = ? AND g.optimizado = 0 AND g.deleted = 0 AND a.valor_asistencia = 0 AND  DATE(a.fecha_asistencia) <= CURDATE() 
      AND (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) IN ('Lunes','Martes','Miercoles','Sabado', 'Domingo')
      GROUP BY a.fecha_asistencia, a.id_grupo ORDER BY alumno;`,[actual], (err, cicloActual) => {
      if(err){
        return reject({ message: err.sqlMessage});
      }
      resolve(cicloActual)
    });
  });
};

/************************************/
/************************************/

cambios.getAlumnoCargaEspecialGrupo = ( id_alumno, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT pagado, adeudo, descuento, precio_grupo_con_descuento_x_alumno, total_pagado, fecha_registro
      FROM alumnos_grupos_especiales_carga
      WHERE id_alumno = ? AND id_grupo = ?;`,[id_alumno, id_grupo], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage + 'getAlumnoCargaEspecialGrupo' });
      }
      resolve(res[0])
    });
  });
};


cambios.eliminarAlumnoCargaEspecial = ( id_grupo, id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`DELETE FROM alumnos_grupos_especiales_carga WHERE id_grupo = ? AND id_alumno = ?;`,[ id_grupo, id_alumno ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage + ' -> eliminarAlumnoCargaEspecial' });
      }
      resolve(res)
    });
  });
};

cambios.eliminarGrupoAlumnoActual = ( id_grupo, id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`DELETE FROM gruposalumnos WHERE id_alumno = ? AND id_grupo = ?;`,[ id_alumno, id_grupo ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage + ' -> eliminarGrupoAlumnoActual' });
      }
      resolve(res)
    });
  });
};


cambios.updateIngresos = ( id_alumno, id_grupo, id_grupo_nuevo, iderp ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE ingresos SET id_alumno = ?, id_grupo = ? , comentarios = concat('Alta de ingreso por cambio a grupo nuevo ', ?, ' -> del grupo anterior ',?), id_usuario_ultimo_cambio = ?
      WHERE id_alumno = ? AND   id_grupo = ?;`,[ id_alumno, id_grupo_nuevo, id_grupo_nuevo, id_grupo, iderp, id_alumno, id_grupo ], (err, res) => {
      if (err) {  return reject({ message: err.sqlMessage + ' -> updateIngresos' }); }
      if (res.affectedRows == 0) {
        return reject({ message: 'No existe el datos para actualizar' });
      }
      resolve(res)
    });
  });
};


cambios.insertarEgreso = ( id_grupo_nuevo, id_grupo, iderp, pago_grupo_actual, id_alumno, id_plantel ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO egresos( id_tipo_egreso, fecha_egreso, comentarios, id_usuario_egreso, id_usuario_ultimo_cambio, monto_egreso, id_alumno, id_grupo, id_plantel)
      VALUES(4,NOW(),concat('Egreso por cambio al grupo nuevo ', ?, ' -> del grupo anterior ', ?), ?, ?, ?, ?, ?, ? );`,
      [ id_grupo_nuevo, id_grupo, iderp, iderp, pago_grupo_actual, id_alumno, id_grupo, id_plantel ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage + ' -> insertarEgreso' });
      }
      resolve(res)
    });
  });
};

cambios.insertarGrupoAlumno = ( id_grupo_nuevo, id_alumno, precio_grupo, precio_descuento, iderp ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO gruposalumnos (id_grupo, id_alumno, precio_curso, precio_grupo_sin_descuento,id_usuario_ultimo_cambio, motivo_baja)
      values(?, ?, ?, ?, ?, ?);`,
      [ id_grupo_nuevo, id_alumno, precio_grupo, precio_descuento, iderp, null ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage + ' -> insertarGrupoAlumno' });
      }
      resolve(res)
    });
  });
};

cambios.insertarGruposEspeciales = ( id_alumno, id_grupo_nuevo, id_ciclo, pagado, adeudo, descuento, precio_grupo, precio_grupo_con_descuento_x_alumno, total_pagado ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO alumnos_grupos_especiales_carga (id_alumno, id_grupo, id_ciclo, pagado, adeudo, descuento, precio_grupo, precio_grupo_con_descuento_x_alumno, total_pagado, fecha_registro)
      values(?, ?, ?, ?, ?, ?, ?, ?, ?, NOW());`,
      [ id_alumno, id_grupo_nuevo, id_ciclo, pagado, adeudo, descuento, precio_grupo, precio_grupo_con_descuento_x_alumno, total_pagado ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage + ' -> insertarGrupoAlumno' });
      }
      resolve(res)
    });
  });
};

cambios.cambiosNuevoGruposxAlumnoPrueba = (tipociclo,id_ciclo,result) => {
  sqlERP.query(`SELECT * FROM grupos WHERE activo_sn = 1 AND  id_ciclo IN(SELECT id_ciclo FROM ciclos
    WHERE ciclo NOT LIKE '%inve%' AND ciclo NOT LIKE '%cambios%' AND ciclo NOT LIKE '%campa%' AND ciclo NOT LIKE '%capa%' AND ciclo NOT LIKE '%entrena%'
    AND activo_sn = 1 
    AND CURRENT_DATE BETWEEN fecha_inicio_ciclo AND fecha_fin_ciclo)
    OR activo_sn = 1 AND  id_ciclo IN(SELECT id_ciclo FROM ciclos
    WHERE ciclo NOT LIKE '%inve%' AND ciclo NOT LIKE '%cambios%' AND ciclo NOT LIKE '%campa%' AND ciclo NOT LIKE '%capa%' AND ciclo NOT LIKE '%entrena%'
    AND activo_sn = 1 )
    ORDER BY id_plantel, id_nivel, id_ciclo;`,[id_ciclo],(err, res) => {
    if(err){
      result(err,null);
      return;
    }
    result(null, res)
  });
};

cambios.getAlumnoxGrupoPrueba = (tipociclo,result) => {
  sqlERP.query(`SELECT g.id_grupo, a.id_alumno,matricula, UPPER(CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,""))) AS alumno,
    g.id_grupo, g.grupo, ga.monto_pagado_total, g.id_nivel, n.nivel, p.id_plantel, p.plantel, g.id_ciclo FROM gruposalumnos ga
    LEFT JOIN grupos  g ON g.id_grupo = ga.id_grupo
    LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
    LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
    LEFT JOIN niveles n ON n.id_nivel = g.id_nivel
    WHERE g.id_ciclo IN(SELECT id_ciclo FROM ciclos
    WHERE activo_sn = 1 AND ciclo NOT LIKE '%inve%' AND ciclo NOT LIKE '%cambios%'  AND ciclo NOT LIKE '%campa%' AND ciclo NOT LIKE '%capa%' AND ciclo NOT LIKE '%entrena%') 
    GROUP BY alumno
    ORDER BY alumno ;`,(err, res) => {
    if(err){
      result(err,null);
      return;
    }
    result(null, res)
  });
};

cambios.getGruposUsuarioPrueba = (idusuario ,result) => {
  sqlERP.query(`SELECT g.id_grupo, 
    g.id_grupo, g.grupo, ga.monto_pagado_total, g.id_nivel, n.nivel, p.id_plantel, p.plantel, g.id_ciclo, ac.precio_grupo, ac.descuento, ac.precio_grupo_con_descuento_x_alumno, ac.pagado 
    FROM gruposalumnos ga
    LEFT JOIN grupos  g ON g.id_grupo = ga.id_grupo
    LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
    LEFT JOIN niveles n ON n.id_nivel = g.id_nivel
    LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
    LEFT JOIN alumnos_grupos_especiales_carga ac ON ac.id_alumno = ga.id_alumno AND ac.id_grupo = g.id_grupo
    WHERE g.id_ciclo IN(SELECT id_ciclo FROM ciclos
      WHERE activo_sn = 1 
      AND ciclo NOT LIKE '%inve%' AND ciclo NOT LIKE '%cambios%' AND ciclo NOT LIKE '%campa%' AND ciclo NOT LIKE '%capa%' AND ciclo NOT LIKE '%entrena%'
    )
    AND CURRENT_DATE BETWEEN fecha_inicio_ciclo AND fecha_fin_ciclo
    AND ga.id_alumno = ?
    OR g.id_ciclo IN(SELECT id_ciclo FROM ciclos
      WHERE activo_sn = 1 
      AND ciclo NOT LIKE '%inve%' AND ciclo NOT LIKE '%cambios%' AND ciclo NOT LIKE '%campa%' AND ciclo NOT LIKE '%capa%' AND ciclo NOT LIKE '%entrena%'
    )
    AND ga.id_alumno = ?
    ORDER BY p.plantel, g.id_nivel`,[ idusuario, idusuario ],(err, res) => {
    if(err){
      result(err,null);
      return;
    }
    result(null, res)
  });
};
module.exports = cambios;

