const { result } = require("lodash");
// const sqlERP  = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");
//const constructor
const permisos = function(depas) {

};

permisos.getPermisosUsuario = ( id_usuario ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT s.*, u.nombre_completo AS solicitante, IFNULL(u2.nombre_completo,'PENDIENTE') AS jefe, IFNULL(u3.nombre_completo,'PENDIENTE') AS rh 
			FROM solicitud_permisos s
			LEFT JOIN usuarios u ON u.id_usuario = s.id_usuario
			LEFT JOIN usuarios u2 ON u2.id_usuario = s.id_jefe_directo
			LEFT JOIN usuarios u3 ON u3.id_usuario = s.id_rh
			WHERE s.id_usuario = ?
			ORDER BY fecha_creacion DESC;`, [ id_usuario ], (err, res) => {
      if (err) {
        return reject({message : err.sqlMessage });
      }
      // Retornamos solo un dato
      return resolve( res);
    });
  })
};

permisos.getPermisosArea = ( ids ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT s.*, u.nombre_completo AS solicitante, IFNULL(u2.nombre_completo,'PENDIENTE') AS jefe, IFNULL(u3.nombre_completo,'PENDIENTE') AS rh 
			FROM solicitud_permisos s
			LEFT JOIN usuarios u ON u.id_usuario = s.id_usuario
			LEFT JOIN usuarios u2 ON u2.id_usuario = s.id_jefe_directo
			LEFT JOIN usuarios u3 ON u3.id_usuario = s.id_rh
			WHERE s.id_usuario IN( ? )
			ORDER BY fecha_creacion DESC;`, [ ids ], (err, res) => {
      if (err) {
        return reject({message : err.sqlMessage });
      }
      // Retornamos solo un dato
      return resolve( res);
    });
  })
};


permisos.getPermisosRh = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT s.*, u.nombre_completo AS solicitante, IFNULL(u2.nombre_completo,'PENDIENTE') AS jefe, IFNULL(u3.nombre_completo,'PENDIENTE') AS rh 
			FROM solicitud_permisos s
			LEFT JOIN usuarios u ON u.id_usuario = s.id_usuario
			LEFT JOIN usuarios u2 ON u2.id_usuario = s.id_jefe_directo
			LEFT JOIN usuarios u3 ON u3.id_usuario = s.id_rh
			ORDER BY fecha_creacion DESC;`, (err, res) => {
      if (err) {
        return reject({message : err.sqlMessage });
      }
      // Retornamos solo un dato
      return resolve( res);
    });
  })
};

permisos.addPermisos = ( u ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO solicitud_permisos(id_usuario,idareas_ticket,id_jefe_directo,id_rh,autorizo_jefe,autorizo_rh,fecha_inicio,fecha_final,motivo,archivo_adjunto,motivo_rechazo_jefe,motivo_rechazo_rh)
    	VALUES(?,?,?,?,?,?,?,?,?,?,?,?)`,
    	[ u.id_usuario,u.idareas_ticket,u.id_jefe_directo,u.id_rh,u.autorizo_jefe,u.autorizo_rh,u.fecha_inicio,u.fecha_final,u.motivo,u.archivo_adjunto,u.motivo_rechazo_jefe,u.motivo_rechazo_rh ],(err, res) => {
      if(err){
        return reject({message : err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  });
};


permisos.updatePermisoRH = ( u ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE solicitud_permisos SET id_rh = ?, autorizo_rh = ?, motivo_rechazo_rh = ? WHERE idsolicitud_permisos = ?`,
      [ u.id_rh, u.autorizo_rh, u.motivo_rechazo_rh, u.idsolicitud_permisos ],(err, res) => {
      if (err) { return reject({message : err.sqlMessage }); }
      if (res.affectedRows == 0) {
        return reject({message : err.sqlMessage });
      }
      resolve({ u });
    });
  });
};

permisos.updatePermisoJefe = ( u ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE solicitud_permisos SET id_jefe_directo = ?, autorizo_jefe = ?, motivo_rechazo_jefe = ? WHERE idsolicitud_permisos = ?`,
      [ u.id_jefe_directo, u.autorizo_jefe, u.motivo_rechazo_jefe, u.idsolicitud_permisos ],(err, res) => {
      if (err) { return reject({message : err.sqlMessage }); }
      if (res.affectedRows == 0) {
        return reject({message : err.sqlMessage });
      }
      resolve({ u });
    });
  });
};

module.exports = permisos;

