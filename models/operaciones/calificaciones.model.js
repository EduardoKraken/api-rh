const { result } = require("lodash");
const sqlERP  = require("../db3.js");
const sqlINBI = require("../dbINBI.js");
const sqlFAST = require("../dbFAST.js");


//const constructor
const calificaciones = function(depas) {

};

// Mostrar fecha en español
calificaciones.fechaEspaniol = (  escuela ) => {
  return new Promise(( resolve, reject )=>{
  	const db = escuela == 2 ? sqlFAST : sqlINBI
    db.query(`SET lc_time_names = 'es_ES';`, (err, res) => {
      if (err) {
        return reject( err );
      }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};

calificaciones.getAsistenciaUsuario = ( id_alumno, id_grupo, escuela ) => {
  return new Promise(( resolve, reject )=>{
  	const db = escuela == 2 ? sqlFAST : sqlINBI
  	
    db.query(`SELECT *, DATE_FORMAT(fecha_asistencia, '%W %d %M %Y') fecha_formato,
      IF(valor_asistencia = 1, 'Con participación',
      IF(valor_asistencia = 2, 'Sin participación',
      IF(valor_asistencia = 3, 'Retardo',
      IF(valor_asistencia = 4, 'Justificada','Pendiente')))) AS valor_text,
      IF(valor_asistencia = 1, 'success',
      IF(valor_asistencia = 2, 'error',
      IF(valor_asistencia = 3, 'warning',
      IF(valor_asistencia = 4, 'orange','Pendiente')))) AS color
      FROM asistencia_grupo WHERE id_usuario = ? AND id_grupo = ? AND deleted = 0;`, [ id_alumno, id_grupo ], (err, res) => {
      if (err) {
        return reject({ message : err.sqlMessage });
      }
      // Retornamos solo un dato
      return resolve( res);
    });
  })
};


calificaciones.recursosContestados = ( id_alumno, id_grupo, id_nivel, id_curso, escuela ) => {
  return new Promise(( resolve, reject )=>{
  	const db = escuela == 2 ? sqlFAST : sqlINBI
	    db.query(`SELECT DISTINCT c.*, r.nombre AS recurso, r.tipo_recurso, r.id_tipo_evaluacion, r.id_categoria_evaluacion,
			IF(r.tipo_recurso = 1 AND r.id_tipo_evaluacion = 1 AND r.id_categoria_evaluacion = 1, 1,
			IF(r.tipo_recurso = 1 AND r.id_tipo_evaluacion = 2 AND r.id_categoria_evaluacion = 1, 2 ,
			IF(r.tipo_recurso = 1 AND r.id_tipo_evaluacion = 2 AND r.id_categoria_evaluacion = 2, 3 ,
			IF(r.tipo_recurso = 1 AND r.id_tipo_evaluacion = 2 AND r.id_categoria_evaluacion = 2, 4 , 0)))) AS tipo,
			gg.fecha, a.valor_asistencia
			FROM control_grupos_calificaciones_evaluaciones c
			LEFT JOIN recursos r ON r.id_evaluacion = c.id_evaluacion AND r.id_curso = ${ id_curso }
      LEFT JOIN ( SELECT * FROM grupo_fechas WHERE idgrupo = ${ id_grupo } AND idnivel = ${ id_nivel } ) gg ON gg.sesion = c.session
			LEFT JOIN asistencia_grupo a ON a.id_usuario = c.id_alumno AND  a.id_grupo = c.id_grupo AND DATE(a.fecha_asistencia) = gg.fecha
			WHERE c.id_alumno = ? AND c.id_grupo = ? AND c.deleted = 0 ORDER BY c.session;`, [ id_alumno, id_grupo ], (err, res) => {
      if (err) {
        return reject({ message : err.sqlMessage });
      }
      // Retornamos solo un dato
      return resolve( res);
  	});
  })
};

calificaciones.recursoExtraordinarioContestado = ( id_alumno, id_grupo, escuela ) => {
  return new Promise(( resolve, reject )=>{
  	const db = escuela == 2 ? sqlFAST : sqlINBI
		db.query(`SELECT DISTINCT c.*, r.nombre AS recurso, r.tipo_recurso, r.id_tipo_evaluacion, r.id_categoria_evaluacion
      FROM control_grupos_calificaciones_evaluaciones c
      LEFT JOIN recursos r ON r.id_evaluacion = c.id_evaluacion
      WHERE c.id_alumno = ${id_alumno} AND c.id_grupo = ${id_grupo} AND c.deleted = 0 
      AND r.id_tipo_evaluacion = 3;`, (err, res) => {
      if (err) {
        return reject({ message : err.sqlMessage });
      }
      // Retornamos solo un dato
      return resolve( res[0]);
    });
  	
  })
};

calificaciones.existeGrupo = ( id_grupo, escuela ) => {
  return new Promise(( resolve, reject )=>{
  	if( escuela == 2 ){
	    sqlFAST.query(`SELECT g.id, g.id_curso, g.id_nivel, g.nombre AS grupo, g.iderp AS idgrupoerp, c.nombre AS curso, p.nombre AS plantel,
				c.total_dias_laborales, c.id_frecuencia, c.num_examenes, c.num_ejercicios, c.valor_asistencia, g.id_ciclo,
				c.valor_ejercicios, c.valor_examenes, c.valor_total, c.calificacion_aprobar, gt.id_teacher, gt.id_teacher_2,
				CONCAT(u.nombre, ' ', u.apellido_paterno, ' ', IFNULL(u.apellido_materno, '')) AS teacher1,
				CONCAT(u2.nombre, ' ', u2.apellido_paterno, ' ', IFNULL(u2.apellido_materno, '')) AS teacher2,
				h.hora_inicio, h.hora_fin, c.tipo FROM grupos g
				LEFT JOIN cursos c ON c.id = g.id_curso
				LEFT JOIN planteles p ON p.id = g.id_plantel
				LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
				LEFT JOIN usuarios u ON u.id = gt.id_teacher
				LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
				LEFT JOIN horarios h ON h.id = g.id_horario
				WHERE g.id = ?;`, [ id_grupo ], (err, res) => {
	      if (err) {
	        return reject({ message : err.sqlMessage });
	      }
	      // Retornamos solo un dato
	      return resolve( res[0] );
	    });
  	}else{
  		sqlINBI.query(`SELECT g.id, g.id_curso, g.id_nivel, g.nombre AS grupo, g.iderp AS idgrupoerp, c.nombre AS curso, p.nombre AS plantel,
				c.total_dias_laborales, c.id_frecuencia, c.num_examenes, c.num_ejercicios, c.valor_asistencia, g.id_ciclo,
				c.valor_ejercicios, c.valor_examenes, c.valor_total, c.calificacion_aprobar, gt.id_teacher, gt.id_teacher_2,
				CONCAT(u.nombre, ' ', u.apellido_paterno, ' ', IFNULL(u.apellido_materno, '')) AS teacher1,
				CONCAT(u2.nombre, ' ', u2.apellido_paterno, ' ', IFNULL(u2.apellido_materno, '')) AS teacher2,
				h.hora_inicio, h.hora_fin, c.tipo FROM grupos g
				LEFT JOIN cursos c ON c.id = g.id_curso
				LEFT JOIN planteles p ON p.id = g.id_plantel
				LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
				LEFT JOIN usuarios u ON u.id = gt.id_teacher
				LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
				LEFT JOIN horarios h ON h.id = g.id_horario
				WHERE g.id = ? ;`, [ id_grupo ], (err, res) => {
	      if (err) {
	        return reject({ message : err.sqlMessage });
	      }
	      // Retornamos solo un dato
	      return resolve( res[0] );
	    });
  	}
  })
};

calificaciones.habiltiarGrupo = ( id_grupo, escuela ) => {
  return new Promise(( resolve, reject )=>{
  	if( escuela == 2 ){
	    sqlFAST.query(`UPDATE grupos SET deleted = 0 WHERE id = ?;`, [ id_grupo ], (err, res) => {
	      if (err) {
	        return reject({ message : err.sqlMessage });
	      }
	      // Retornamos solo un dato
	      return resolve( res);
	    });
  	}else{
  		sqlINBI.query(`UPDATE grupos SET deleted = 0 WHERE id = ?;`, [ id_grupo ], (err, res) => {
	      if (err) {
	        return reject({ message : err.sqlMessage });
	      }
	      // Retornamos solo un dato
	      return resolve( res);
	    });
  	}
  })
};


calificaciones.habiltiarKardex = ( idkardex, escuela ) => {
  return new Promise(( resolve, reject )=>{
  	if( escuela == 2 ){
	    sqlFAST.query(`UPDATE cardex_curso SET deleted = 1 WHERE id = ?;`, [ idkardex ], (err, res) => {
	      if (err) {
	        return reject({ message : err.sqlMessage });
	      }
	      // Retornamos solo un dato
	      return resolve( res);
	    });
  	}else{
  		sqlINBI.query(`UPDATE cardex_curso SET deleted = 1 WHERE id = ?;`, [ idkardex ], (err, res) => {
	      if (err) {
	        return reject({ message : err.sqlMessage });
	      }
	      // Retornamos solo un dato
	      return resolve( res);
	    });
  	}
  })
};


calificaciones.actualizarCalificacion = ( idkardex, primera, segunda, escuela ) => {
  return new Promise(( resolve, reject )=>{
  	if( escuela == 2 ){
	    sqlFAST.query(`UPDATE cardex_curso SET deleted = 0, calificacion_final_primera_oportunidad = ?, calificacion_final_segunda_oportunidad = ? WHERE id = ?;`,
	    	[ primera, segunda, idkardex ], (err, res) => {
	      if (err) {
	        return reject({ message : err.sqlMessage });
	      }
	      // Retornamos solo un dato
	      return resolve( res);
	    });
  	}else{
  		sqlINBI.query(`UPDATE cardex_curso SET deleted = 0, calificacion_final_primera_oportunidad = ?, calificacion_final_segunda_oportunidad = ? WHERE id = ?;`,
  			[ primera, segunda, idkardex ], (err, res) => {
	      if (err) {
	        return reject({ message : err.sqlMessage });
	      }
	      // Retornamos solo un dato
	      return resolve( res);
	    });
  	}
  })
};

//---------------------------------------------------------------------------------------------------------------//

calificaciones.ultimoAlumnoAsistio = ( id_alumno ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT a.id_alumno, a.unidad_negocio  FROM fyn_proc_entradas_salidas e
        LEFT JOIN alumnos a ON a.id_alumno = e.id_alumno
        WHERE DATE(e.fecha_registro) = CURRENT_DATE
        AND a.id_alumno = ?;`, [ id_alumno ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      // Retornamos solo un dato
      return resolve( res[0] );
    });
  })
};


calificaciones.getAlumnoSistema = ( iderp, escuela ) => {
  const db = escuela == 1 ? sqlINBI : sqlFAST
  return new Promise(( resolve, reject )=>{
    db.query(`SELECT * FROM usuarios WHERE iderp = ?;`, [ iderp ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      // Retornamos solo un dato
      return resolve( res[0] );
    });
  })
};

calificaciones.getUltimoGrupo = ( id_alumno, escuela ) => {
  const db = escuela == 1 ? sqlINBI : sqlFAST
  return new Promise(( resolve, reject )=>{
    db.query(`SELECT g.* FROM grupo_alumnos ga
        LEFT JOIN grupos g ON g.id = ga.id_grupo
        WHERE ga.id_alumno = ?
        ORDER BY ga.id_grupo DESC 
        LIMIT 1;`, [ id_alumno ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      // Retornamos solo un dato
      return resolve( res[0] );
    });
  })
};


calificaciones.getMaestrosGrupo = ( id_grupo, escuela ) => {
	const db = escuela == 1 ? sqlINBI : sqlFAST
	return new Promise(( resolve, reject )=>{
	  db.query(`SELECT 
				CONCAT(u.nombre, ' ', u.apellido_paterno, ' ', IFNULL(u.apellido_materno, '')) AS teacher1,
				CONCAT(u2.nombre, ' ',u2.apellido_paterno, ' ', IFNULL(u2.apellido_materno, '')) AS teacher2
				FROM grupo_teachers gt
				LEFT JOIN usuarios u ON u.id = gt.id_teacher
				LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
				WHERE gt.id_grupo = ?;`, [ id_grupo ], (err, res) => {
		if (err) {
		  return reject({ message: err.sqlMessage });
		}
		// Retornamos solo un dato
		return resolve( res[0] );
	  });
	})
  };

  //---------------------------------------------------------------------------------------------------------------//

 
  calificaciones.getGrupoActual = (iderp ) => {
	return new Promise(( resolve, reject )=>{
	  sqlERP.query(`SELECT id_grupo
					FROM gruposalumnos
					WHERE id_alumno = ? 
					ORDER BY id_grupo DESC 
					LIMIT 1;`, [ iderp ], (err, res) => {
		if (err) {
		  return reject({ message: err.sqlMessage });
		}
		// Retornamos solo un dato
		return resolve( res[0] );
	  });
	})
  };


calificaciones.getProximoGrupoAlumno = (id_grupo ) => {
	return new Promise(( resolve, reject )=>{
	  sqlERP.query(`SELECT grupo AS Siguiente_grupo FROM 
	  				grupos WHERE id_grupo = ?;`, [id_grupo], (err, res) => {
		if (err) {
		  return reject({ message: err.sqlMessage });
		}
		// Retornamos solo un dato
		return resolve( res[0] );
	  });
	})
  };
  


calificaciones.getAvatar = ( id_alumno, escuela ) => {
	const db = escuela == 1 ? sqlINBI : sqlFAST
  return new Promise(( resolve, reject )=>{
    db.query(`SELECT * FROM avatar WHERE id_alumno = ?;`, [ id_alumno ], (err, res) => {
      if (err) {
        console.log( err )
        return reject({ message: err.sqlMessage });
      }
      // Retornamos solo un dato
      return resolve( res[0] );
    });
  })
};


module.exports = calificaciones;

