const { result }  = require("lodash");
// Original
// const sqlERP      = require("../db3.js");

// Prueba
const sqlERP      = require("../dbErpPrueba.js");
const sqlERPNUEVO = require("../db.js");

const sqlFAST = require("../dbFAST.js");
const sqlINBI = require("../dbINBI.js");

//const constructor
const alumnos = function(alumnos) {};


// Agregar un salon+
alumnos.addAlumno = ( a ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO alumnos(nombre, apellido_paterno, apellido_materno, telefono, celular, id_tipo_alumno, id_usuario_ultimo_cambio,edad,id_contacto, unidad_negocio, id_prospecto,id_empleado,iderp,matricula_erp)
    	VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, 
    	[a.nombre, a.apellido_paterno, a.apellido_materno, a.telefono, a.celular, a.id_tipo_alumno, a.id_usuario_ultimo_cambio, a.edad,a.id_contacto, a.unidad_negocio,a.id_prospecto,a.id_empleado,a.iderp,a.matricula_erp],(err, res) => {
      if(err)
        return reject({ message: err.sqlMessage });
      
      resolve({ id: res.insertId, ...a })
    })
  })
}

// Actualizar la matricula del alumno
alumnos.updateMatricula = ( matricula, id ) => {
	return new Promise((resolve,reject)=>{
	  sqlERP.query(` UPDATE alumnos SET matricula=CONCAT(YEAR(CURRENT_DATE),'-',?) WHERE id_alumno = ?`, [matricula, id],
	  (err, res) => {
      if(err)
        return reject({ message: err.sqlMessage });

      if (res.affectedRows == 0) {
        return reject({ message: 'Dato no encontrado' });
      }
      resolve({ matricula, id });
	  });
	});
};

// Agregar el tutor del alumno
alumnos.addAlumnoTutor = ( a, id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO alumnotutor(id_alumno, nombre, apellido_paterno, apellido_materno, telefono, celular, tipo_parentesco, id_usuario_ultimo_cambio)
    	VALUES(?,?,?,?,?,?,?,?)`, 
    	[id, a.nombre, a.apellido_paterno, a.apellido_materno, a.telefono, a.celular, a.tipo_parentesco, a.id_usuario_ultimo_cambio],(err, res) => {
      if(err)
        return reject({ message: err.sqlMessage });
      
      resolve({ id: res.insertId, ...a })
    })
  })
}

// Agregar hermanos a la tabla
alumnos.addHermano = ( id_tutor, id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO hermanos(id_tutor, id_alumno, usuario_registro ) VALUES(?,?,?)`, [id_tutor, id_alumno, 334 ],(err, res) => {
      if(err)
        return reject({ message: err.sqlMessage });
      
      resolve({ id: res.insertId,id_tutor, id_alumno})
    })
  })
}

// Validar si el empleado ya tiene su alumno
alumnos.getAlumnoEmpleado = ( id_empleado ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM alumnos WHERE id_empleado = ?;`,[ id_empleado ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}


// Consultar todos los cursoserp
alumnos.getAlumnoInscrito = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT a.*, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, u.activo_sn AS empleado_activo
      FROM alumnos a
      LEFT JOIN usuarios u ON u.id_usuario = a.id_empleado 
      WHERE a.id_alumno = ?;`,[ id ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

alumnos.getAlumnosInscrito = ( ids ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT a.*, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, u.activo_sn AS empleado_activo
      FROM alumnos a
      LEFT JOIN usuarios u ON u.id_usuario = a.id_empleado 
      WHERE a.id_alumno IN ( ? );`,[ ids ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

// Consultar las cantidades de beca disponibles
alumnos.getBecas = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM fyn_proc_becas;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

alumnos.getVideosEscuela = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT v.*, CONCAT(a.nombre, ' ', a.apellido_paterno, IFNULL(CONCAT(' ',a.apellido_materno),'')) AS alumno, a.usuario, ${escuela} AS escuela 
      FROM videos_alumno v
      LEFT JOIN usuarios a ON a.id = v.id_alumno
      ORDER BY v.fecha_creacion DESC;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

alumnos.cerrarProspecto = ( idalumno, idgrupo, idprospectos ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(` UPDATE prospectos SET idalumno = ?, idgrupo = ?, finalizo = 1, fecha_finalizo = CURRENT_DATE WHERE idprospectos = ?`, [ idalumno, idgrupo, idprospectos ],
    (err, res) => {
      if(err)
        return reject({ message: err.sqlMessage });

      if (res.affectedRows == 0) {
        return reject({ message: 'Dato no encontrado' });
      }
      resolve({ idalumno, idgrupo, idprospectos });
    });
  });
};


module.exports = alumnos;