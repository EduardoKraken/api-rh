const { result }  = require("lodash");
// Original
const sqlERPViejo = require("../db3.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");

// Prueba
const sqlERP      = require("../dbErpPrueba.js");
const sqlERPNUEVO = require("../db.js");

//const constructor
const inscripciones = function(inscripciones) {};

// Consultar todos los cursoserp
inscripciones.getinscripciones = (  ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT a.id_alumno,a.activo_sn, t2.fecha_pago AS fecha_alta ,CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, a.matricula, a.matricula_erp, g.grupo, b.id_beca,
			g.precio, ag.adeudo, ag.pagado, ag.precio_grupo_con_descuento_x_alumno,((t2.monto_pagado) + ifnull(t2.monto_saldo_favor, 0)) AS total_pagado, i.iva, g.id_plantel, g.id_ciclo,a.celular,
      IF(LOCATE('FAST',p.plantel)>0,2,1) AS unidad_negocio, a.fecha_alta AS fecha_inscripcion,
			IF(b.id_beca = 4,g.precio,IF(b.id_beca = 3,(g.precio*.75),IF(b.id_beca = 2,(g.precio*.50),IF(b.id_beca = 1,(g.precio*.25),IF(ag.descuento,ag.descuento,0))))) AS valor_descuento,
			IF(b.id_beca = 4,0,IF(b.id_beca = 3,(g.precio*.35),IF(b.id_beca = 2,(g.precio*.50),IF(b.id_beca = 1,(g.precio*.85),IF(ag.descuento,(g.precio-ag.descuento),g.precio))))) AS precio_con_descuento,
			(SELECT MAX(id_grupo) FROM gruposalumnos WHERE id_alumno = a.id_alumno AND activo_sn = 1) AS id_grupo_actual, IF(ag.adeudo > 0,'adeudo_alumno','') AS clase_row  FROM alumnos a
			LEFT JOIN grupos g ON g.id_grupo = (SELECT MAX(id_grupo) FROM gruposalumnos WHERE id_alumno = a.id_alumno AND activo_sn = 1)
			LEFT JOIN alumnos_grupos_especiales_carga ag ON ag.id_alumno = a.id_alumno AND ag.id_grupo = (SELECT MAX(id_grupo) FROM gruposalumnos WHERE id_alumno = a.id_alumno AND activo_sn = 1)
			LEFT JOIN (	SELECT	id_alumno, id_grupo, sum(monto_pagado) as monto_pagado, 
						sum(monto_saldo_favor) as monto_saldo_favor, fecha_pago
				FROM 	ingresos
				GROUP BY id_alumno, id_grupo
			) t2 ON a.id_alumno = t2.id_alumno AND (SELECT MAX(id_grupo) FROM gruposalumnos WHERE id_alumno = a.id_alumno AND activo_sn = 1) = t2.id_grupo
			LEFT JOIN fyn_proc_becas_prospectos_alumnos b ON b.id_alumno = a.id_alumno AND b.id_grupo = (SELECT MAX(id_grupo) FROM gruposalumnos WHERE id_alumno = a.id_alumno AND activo_sn = 1)
			LEFT JOIN fyn_proc_alumnos_grupos_iva i ON i.id_alumno = a.id_alumno AND i.id_grupo = (SELECT MAX(id_grupo) FROM gruposalumnos WHERE id_alumno = a.id_alumno AND activo_sn = 1)
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo;`,[ ciclo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

inscripciones.getinscripcionesCiclo = (  ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT a.id_alumno,a.activo_sn, t2.fecha_pago AS fecha_alta,CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, a.matricula, a.matricula_erp, g.grupo, b.id_beca,
      g.precio, ag.adeudo, ag.pagado, ag.precio_grupo_con_descuento_x_alumno,((t2.monto_pagado) + ifnull(t2.monto_saldo_favor, 0)) AS total_pagado, i.iva, g.id_plantel, g.id_ciclo,a.celular,
      ( ag.precio_grupo_con_descuento_x_alumno - ( t2.monto_pagado / ( 1 + IFNULL ( i.iva, 0 ) )  )) AS diferencia,
      g.id_grupo AS id_grupo, ga.activo_sn AS baja, p.plantel, a.fecha_alta AS fecha_inscripcion,
      IF(LOCATE('FAST',p.plantel)>0,2,1) AS unidad_negocio,
      IF(b.id_beca = 4,g.precio,IF(b.id_beca = 3,(g.precio*.75),IF(b.id_beca = 2,(g.precio*.50),IF(b.id_beca = 1,(g.precio*.25),IF(ag.descuento,ag.descuento,0))))) AS valor_descuento,
      IF(b.id_beca = 4,0,IF(b.id_beca = 3,(g.precio*.35),IF(b.id_beca = 2,(g.precio*.50),IF(b.id_beca = 1,(g.precio*.85),IF(ag.descuento,(g.precio-ag.descuento),g.precio))))) AS precio_con_descuento,
      (SELECT MAX(id_grupo) FROM gruposalumnos WHERE id_alumno = a.id_alumno AND activo_sn = 1) AS id_grupo_actual, IFNULL(sal.saldoFavor,0) AS saldofavor, IF(ag.adeudo > 0,'adeudo_alumno','') AS clase_row,
      IF((SELECT COUNT(*) FROM gruposalumnos ga2
        LEFT JOIN grupos g1 ON g1.id_grupo = ga2.id_grupo
        LEFT JOIN ciclos c2 ON c2.id_ciclo = g1.id_ciclo
        WHERE c2.fecha_inicio_ciclo < c.fecha_inicio_ciclo
        AND c2.ciclo LIKE '%ciclo%'
        AND id_alumno IN (a.id_alumno))>0,0,1) AS alumno_ni
      FROM alumnos_grupos_especiales_carga ag
      LEFT JOIN grupos g ON g.id_grupo = ag.id_grupo
      LEFT JOIN alumnos a ON a.id_alumno = ag.id_alumno AND ag.id_grupo = g.id_grupo
      LEFT JOIN gruposalumnos ga ON ga.id_grupo = g.id_grupo AND ga.id_alumno = a.id_alumno
      LEFT JOIN ( SELECT  id_alumno, id_grupo, sum(monto_pagado) as monto_pagado, 
            sum(monto_saldo_favor) as monto_saldo_favor, fecha_pago
        FROM  ingresos
        GROUP BY id_alumno, id_grupo
      ) t2 ON a.id_alumno = t2.id_alumno AND t2.id_grupo = ag.id_grupo
      LEFT JOIN (SELECT IFNULL( SUM(IF(tipo = 1, monto, monto * -1)),0) AS saldoFavor, id_alumno, id_grupo FROM saldofavoralumnos ss1
      GROUP BY id_alumno) sal ON sal.id_alumno = a.id_alumno
      LEFT JOIN fyn_proc_becas_prospectos_alumnos b ON b.id_alumno = a.id_alumno AND b.id_grupo = ag.id_grupo
      LEFT JOIN fyn_proc_alumnos_grupos_iva i ON i.id_alumno = ag.id_alumno AND i.id_grupo = ag.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      WHERE g.id_ciclo = ?;`,[ ciclo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

inscripciones.ciclosParaRI = ( id_ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM ciclos 
      WHERE fecha_inicio_ciclo = (SELECT DATE_ADD(fecha_fin_ciclo, interval 1 DAY) AS nueva_fecha FROM ciclos WHERE id_ciclo = ?)
      AND ciclo LIKE '%CICLO%'
      AND id_ciclo_relacionado;`,[ id_ciclo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

inscripciones.alumnosParaRI = ( id_alumnos, id_ciclos ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT a.*, m.id_maestro FROM alumnos_grupos_especiales_carga  a
      LEFT JOIN maestrosgrupo m ON m.id_grupo = a.id_grupo
      WHERE a.id_alumno IN ( ? ) AND a.id_ciclo IN ( ? ) AND a.adeudo <= 0;`,[ id_alumnos, id_ciclos ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}


// Consultar todos los cursoserp
inscripciones.getAlumnoInscrito = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT *, CONCAT(nombre, ' ', apellido_paterno, ' ', IFNULL(apellido_materno,'')) AS alumno FROM alumnos WHERE id_alumno = ?;`,[ id ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

// Consultar todos los cursoserp
inscripciones.getGruposAlumnoInscrito = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT i.id_ingreso, c.curso, g.grupo, g.id_grupo, c.id_curso, c.precio_nivel, ga.fecha_alta, i.monto_pagado AS pagado
      FROM ingresos i
      LEFT JOIN gruposalumnos ga ON ga.id_grupo = i.id_grupo AND ga.id_alumno = i.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN cursos c ON c.id_curso = g.id_curso
      WHERE i.id_alumno = ?;`,[ id ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

// Consultar las cantidades de beca disponibles
inscripciones.getBecas = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM fyn_proc_becas;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

// Consutlar las becas del alumno
inscripciones.getBecasAlumno = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT b.id, b.id_alumno, b.id_grupo, b.id_beca, bs.beca, b.fecha_alta, b.aprobadoSN, g.grupo FROM fyn_proc_becas_prospectos_alumnos b
			LEFT JOIN grupos g ON g.id_grupo = b.id_grupo
			LEFT JOIN fyn_proc_becas bs ON bs.id = b.id_beca
			WHERE b.id_alumno = ?;`,[ id ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

// Consultar todos los cursoserp
inscripciones.getGrupoAlumnos = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT a.*,CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, g.grupo,
			ga.precio_curso, ga.precio_grupo_sin_descuento, ga.precio_grupo_con_descuento_x_alumno, ga.monto_pagado_total, ga.monto_adeudo_grupo,
			(SELECT MAX(id_grupo) FROM gruposalumnos WHERE id_alumno = a.id_alumno AND activo_sn = 1) AS id_grupo  FROM alumnos a
			LEFT JOIN grupos g ON g.id_grupo = (SELECT MAX(id_grupo) FROM gruposalumnos WHERE id_alumno = a.id_alumno AND activo_sn = 1)
			LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno AND ga.id_grupo = (SELECT MAX(id_grupo) FROM gruposalumnos WHERE id_alumno = a.id_alumno AND activo_sn = 1);`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

// Consultar todos los alumnos
inscripciones.getListadoAlumnos = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT a.id_alumno, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ',IFNULL(a.apellido_materno,'')) AS alumno, a.unidad_negocio,
      a.telefono, a.celular, a.matricula, a.id_contacto, a.id_ultimo_ciclo_pago, a.email, a.matricula_erp,
      CONCAT(t.nombre, ' ', t.apellido_paterno, ' ',IFNULL(t.apellido_materno,'')) AS tutor, t.telefono AS telTutor, t.celular AS celTutor,
      CONCAT(
        '[',TRIM(a.matricula), '] ',
        TRIM(REPLACE(a.nombre,'  ','')), ' ', 
        TRIM(REPLACE(a.apellido_paterno,'','')), ' ',
        IFNULL(TRIM(REPLACE(a.apellido_materno,'  ','')),''), ' ', 
        a.matricula, ' ',
        IFNULL(TRIM(REPLACE(a.telefono,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(a.celular,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(t.telefono,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(t.celular,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(t.nombre,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(t.apellido_paterno,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(t.apellido_materno,' ','')),''), ' ') AS datos_completos
      FROM alumnos a
      LEFT JOIN alumnotutor t ON t.id_alumno = a.id_alumno;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

inscripciones.getAlumnosId = ( id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT a.id_alumno, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ',IFNULL(a.apellido_materno,'')) AS alumno, a.unidad_negocio,
      a.telefono, a.celular, a.matricula, a.id_contacto, a.id_ultimo_ciclo_pago, a.email,
      CONCAT(t.nombre, ' ', t.apellido_paterno, ' ',IFNULL(t.apellido_materno,'')) AS tutor, t.telefono AS telTutor, t.celular AS celTutor,
      CONCAT(
        '[',TRIM(a.matricula), '] ',
        TRIM(REPLACE(a.nombre,'  ','')), ' ', 
        TRIM(REPLACE(a.apellido_paterno,'','')), ' ',
        IFNULL(TRIM(REPLACE(a.apellido_materno,'  ','')),''), ' ', 
        a.matricula, ' ',
        IFNULL(TRIM(REPLACE(a.telefono,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(a.celular,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(t.telefono,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(t.celular,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(t.nombre,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(t.apellido_paterno,' ','')),''), ' ',
        IFNULL(TRIM(REPLACE(t.apellido_materno,' ','')),''), ' ') AS datos_completos
      FROM alumnos a
      LEFT JOIN alumnotutor t ON t.id_alumno = a.id_alumno
      WHERE a.id_alumno = ?;`,[ id_alumno ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

// Consultar todos los alumnos
inscripciones.getDatosProspectos = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT nombre_completo AS alumno, telefono, idprospectos, usuario_asignado, CONCAT(nombre_completo, ' ', telefono, ' ', folio) AS datos_completos, 
      escuela AS unidad_negocio, folio AS matricula 
      FROM prospectos;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

// Consultar el ciclo actual dependiendo de la escuela para saber si se puede inscribir o no 
inscripciones.getCicloActualInscripciones = ( escuela ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT *, TIMESTAMPDIFF(DAY, fecha_inicio_ciclo, CURRENT_DATE) AS dias_transcurridos  FROM ciclos 
      WHERE CURRENT_DATE BETWEEN fecha_inicio_ciclo AND fecha_fin_ciclo
      AND ciclo NOT LIKE '%CAMBIOS%'
      AND ciclo NOT LIKE '%INDUCCION%'
      AND ciclo NOT LIKE '%EXCI%'
      AND ciclo NOT LIKE '%INVERS%'
      AND ciclo NOT LIKE '%CAMPA%'
      AND IF(LOCATE('FE', ciclo) > 0,2,1) = ?
      AND activo_sn = 1;`,[ escuela ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

inscripciones.getGrupoActual = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT g.*, a.adeudo, a.pagado, s.capacidad, (SELECT COUNT(*) FROM gruposalumnos WHERE id_grupo = g.id_grupo) AS inscritos, c.fecha_fin_ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN salones s ON s.id_salon = g.id_salon
      LEFT JOIN alumnos_grupos_especiales_carga a ON a.id_alumno = ? AND a.id_grupo = (
        SELECT gaa.id_grupo 
        FROM gruposalumnos gaa 
        LEFT JOIN grupos gg ON gg.id_grupo = gaa.id_grupo 
        WHERE ? = gaa.id_alumno AND gg.grupo NOT LIKE '%INDUC%' AND gg.grupo NOT LIKE '%CAMBI%' AND gg.grupo NOT LIKE '%EXCI%' AND gg.grupo NOT LIKE '%CAMBI%'
        ORDER BY gg.id_ciclo DESC
        LIMIT 1 
      )
      WHERE ga.id_alumno = ?
      AND ga.id_grupo = (
        SELECT gaa.id_grupo
        FROM gruposalumnos gaa 
        LEFT JOIN grupos gg ON gg.id_grupo = gaa.id_grupo 
        WHERE ? = gaa.id_alumno AND gg.grupo NOT LIKE '%INDUC%' AND gg.grupo NOT LIKE '%CAMBI%' AND gg.grupo NOT LIKE '%EXCI%' AND gg.grupo NOT LIKE '%CAMBI%'
        ORDER BY gg.id_ciclo DESC
        LIMIT 1 
      )
      AND ga.activo_sn = 1;`,[ id, id, id, id ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}


inscripciones.getUltimoGrupoPagado = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT a.id_alumno, a.pagado, a.adeudo, a.descuento, a.precio_grupo, a.precio_grupo_con_descuento_x_alumno, a.total_pagado, g.grupo,
      (a.pagado - a.precio_grupo_con_descuento_x_alumno) AS diferencia FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN salones s ON s.id_salon = g.id_salon
      LEFT JOIN alumnos_grupos_especiales_carga a ON a.id_alumno = ? AND a.id_grupo = (
        SELECT gaa.id_grupo 
        FROM gruposalumnos gaa 
        LEFT JOIN grupos gg ON gg.id_grupo = gaa.id_grupo 
        WHERE ? = gaa.id_alumno 
        ORDER BY gg.id_ciclo DESC
        LIMIT 1 
      )
      WHERE ga.id_alumno = ?
      AND ga.id_grupo = (
        SELECT gaa.id_grupo
        FROM gruposalumnos gaa 
        LEFT JOIN grupos gg ON gg.id_grupo = gaa.id_grupo 
        WHERE ? = gaa.id_alumno 
        ORDER BY gg.id_ciclo DESC
        LIMIT 1 
      )
      AND ga.activo_sn = 1;`,[ id, id, id, id ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

inscripciones.getIngresosAlumnos = ( id_alumno, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT SUM(monto_pagado) AS monto_pagado FROM ingresos WHERE id_alumno = ? AND id_grupo = ?;`,[ id_alumno, id_grupo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

inscripciones.alumnosGrupoCarga = ( id_alumno, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM alumnos_grupos_especiales_carga WHERE id_alumno = ? AND id_grupo = ? ;`,[ id_alumno, id_grupo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

inscripciones.pagoActual = ( id_alumno, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT g.*, a.adeudo, a.pagado, s.capacidad, (SELECT COUNT(*) FROM gruposalumnos WHERE id_grupo = g.id_grupo) AS inscritos, c.fecha_fin_ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN salones s ON s.id_salon = g.id_salon
      LEFT JOIN alumnos_grupos_especiales_carga a ON a.id_alumno = ? AND a.id_grupo = ?
      WHERE ga.id_alumno = ?
      AND ga.id_grupo = ?
      AND ga.activo_sn = 1;`,[ id_alumno, id_grupo, id_alumno, id_grupo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

inscripciones.getGrupoSiguiente = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT g.*, g.id_ciclo AS id_ciclo_grupo_siguiente, s.capacidad, (SELECT COUNT(*) FROM gruposalumnos WHERE id_grupo = g.id_grupo) AS inscritos
      FROM grupos g
      LEFT JOIN salones s ON s.id_salon = g.id_salon
      WHERE g.id_grupo = ? AND g.activo_sn = 1;`,[ id ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

inscripciones.getAlumnoEmpleado = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT a.*, u.activo_sn AS empleado_activo FROM alumnos a
      LEFT JOIN usuarios u ON u.id_usuario = a.id_empleado WHERE a.id_alumno = ?`,[ id ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

inscripciones.getSemanaPago = ( id_ciclo, fechapago ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM descuentos_ciclo
      WHERE ? BETWEEN fecha_inicio AND fecha_final
      AND activo = 1
      AND id_ciclo = ?;`,[ fechapago, id_ciclo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

// Valdiar el descuento a aplicar
inscripciones.getDescuentosPorAplicar = ( id_curso ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM descuentos_cursos 
      WHERE id_curso = ?
      AND activo = 1;`,[ id_curso ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

// Ver el último curso aplicado para el alumno
inscripciones.getUltimoCursoAlumno = ( id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM alumnos_id_curso 
      WHERE id_alumno = ?
      AND deleted = 0
      ORDER BY fecha_creacion DESC LIMIT 1;`,[ id_alumno ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

// Agregar al alumno en el grupo
inscripciones.addGrupoAlumno = ( a ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO gruposalumnos(id_alumno, id_grupo, id_usuario_ultimo_cambio, precio_curso,precio_grupo_sin_descuento,monto_pagado_total,monto_adeudo_grupo,monto_saldo_favor,pago_completado_sn,
      id_descuento_pronto_pago,id_tipo_descuento,monto_descuento_grupo,idbecas)
      VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?);`,
      [ a.id_alumno,a.id_grupo,a.id_usuario_ultimo_cambio,a.precio_curso,a.precio_grupo_sin_descuento,a.monto_pagado_total,a.monto_adeudo_grupo,a.monto_saldo_favor,a.pago_completado_sn
      ,a.id_descuento_pronto_pago,a.id_tipo_descuento,a.monto_descuento_grupo,a.idbecas ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...a })
    })
  })
}

// Ver el último curso aplicado para el alumno
inscripciones.addUltimoCursoAplicado = ( id_alumno, id_curso ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO alumnos_id_curso(id_alumno, id_curso)VALUES(?,?);`, [ id_alumno, id_curso ],
      (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, id_alumno, id_curso })
    })
  })
}

// Registrar un ingreso
inscripciones.addIngreso = ( a ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO ingresos(id_alumno, id_grupo, id_tipo_inscripcion, id_forma_pago,monto_pagado, id_tipo_ingreso, comentarios, fecha_pago, id_usuario_ultimo_cambio
      , base_path, url_foto, id_plantel_ingreso,texto_completo)values(?, ?, ?, ?, ?, ?, ?, now(), ?, ?, ?, ?, ?);`,
      [ a.id_alumno,a.id_grupo,a.id_tipo_inscripcion,a.id_forma_pago,a.monto_pagado_total,a.id_tipo_ingreso,a.comentarios,a.id_usuario_ultimo_cambio,a.base_path
      ,a.url_foto,a.id_plantel_ingreso, a.texto_completo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...a })
    })
  })
}

// Registar alumnos_grupo_carga_especial
inscripciones.addalumnoCargaEspecial = ( a ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO alumnos_grupos_especiales_carga(id_alumno,id_grupo,id_ciclo,pagado,adeudo,descuento,precio_grupo,precio_grupo_con_descuento_x_alumno,total_pagado) 
      VALUES(?,?,?,?,?,?,?,?,?);`,
      [ a.id_alumno,a.id_grupo,a.id_ciclo,a.monto_pagado_total,a.adeudo,a.descuento,a.precio_grupo_sin_descuento,a.precio_grupo_con_descuento,a.total_pagado_alumno ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...a })
    })
  })
}

// OBTENER LOS GRUPOS DISPONIBLEs
inscripciones.getGruposDisponibles = ( escuela ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT g.id_grupo, g.grupo, g.id_plantel, p.plantel, g.id_curso, c.curso,
      g.id_nivel, n.nivel, g.id_ciclo, ci.ciclo, h.hora_inicio, h.hora_fin, h.horario,
      s.capacidad AS cupo, (SELECT COUNT(*) FROM gruposalumnos WHERE id_grupo = g.id_grupo) AS inscritos,
      g.disponibles, g.id_grupo_consecutivo FROM grupos g
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN cursos c ON c.id_curso = g.id_curso
      LEFT JOIN niveles n ON n.id_nivel = g.id_nivel
      LEFT JOIN ciclos ci ON ci.id_ciclo = g.id_ciclo
      LEFT JOIN horarios h ON h.id_horario = g.id_horario
      LEFT JOIN salones s ON s.id_salon = g.id_salon
      WHERE g.id_ciclo IN (SELECT id_ciclo FROM ciclos 
        WHERE  IF(LOCATE('FE', ciclo) > 0,2,1) = ${escuela} AND activo_sn = 1
        OR ciclo LIKE '%EXCI%' AND activo_sn = 1 
      )
      AND ci.fecha_inicio_ciclo >= DATE_SUB(CURRENT_DATE, Interval 30 Day)
      AND g.activo_sn = 1
      
      OR 
      g.id_ciclo IN (SELECT id_ciclo FROM ciclos 
        WHERE  IF(LOCATE('FE', ciclo) > 0,2,1) = ${escuela} AND activo_sn = 1
        OR ciclo LIKE '%EXCI%' AND activo_sn = 1 
      )
      AND ci.ciclo LIKE '%INVER%'
      AND g.activo_sn = 1

      ORDER BY ci.fecha_inicio_ciclo, g.id_nivel, h.hora_inicio;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve( res )
    })
  })
}

// SABER SI ES NI CON LA CANTIDAD DE GRUPOS ASIGNADOS AL ALUMNO
inscripciones.getGruposAlumnoNi = ( id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo  = ga.id_grupo
      WHERE  ga.id_alumno = ? AND g.grupo NOT LIKE '%EXCI%' AND g.grupo NOT LIKE '%INDUCCI%';`,[ id_alumno ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve( res )
    })
  })
}

inscripciones.existeGrupoAlumno = ( id_alumno, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM gruposalumnos WHERE id_alumno = ? AND id_grupo = ?;`,[ id_alumno, id_grupo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve( res[0] )
    })
  })
}

inscripciones.updateGrupoAlumno = ( u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE gruposalumnos SET monto_pagado_total = ( monto_pagado_total + ? ), monto_adeudo_grupo = ?, monto_saldo_favor = ?, pago_completado_sn = ?, monto_descuento_grupo = ?  
      WHERE id_gruposalumnos > 0 AND id_alumno = ? AND id_grupo = ?;`, 
      [ u.monto_pagado_total, u.monto_adeudo_grupo, u.monto_saldo_favor, u.pago_completado_sn, u.monto_descuento_grupo, u.id_alumno, u.id_grupo ],
      (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

inscripciones.existeCargaAlumno = ( id_alumno, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM alumnos_grupos_especiales_carga WHERE id_alumno = ? AND id_grupo = ?;`,[ id_alumno, id_grupo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve( res[0] )
    })
  })
}

inscripciones.updateCargaespecial = ( u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE alumnos_grupos_especiales_carga SET pagado = ( pagado + ? ), adeudo = ?, descuento = ?,
      precio_grupo_con_descuento_x_alumno = ?, total_pagado = ? 
      WHERE id > 0 AND id_alumno = ? AND id_grupo = ?;`, 
      [ u.monto_pagado_total, u.adeudo, u.descuento, u.precio_grupo_con_descuento, u.precio_grupo_sin_descuento, u.id_alumno, u.id_grupo ],
    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

inscripciones.getAlumnoEmpleadoActivo = ( id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM alumnos a
      LEFT JOIN usuarios u ON u.id_usuario = a.id_empleado
      WHERE u.activo_sn = 1 AND a.id_alumno = ?;`,[ id_alumno ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve( res[0] )
    })
  })
}

inscripciones.updateUltimoCursoAlumno = ( id_curso, id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE alumnos_id_curso SET id_curso = ? WHERE idalumnos_id_curso = ?;`, [ id_curso, id ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

inscripciones.getAlumnosHermanos = ( id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT h2.* FROM hermanos h 
      LEFT JOIN (SELECT * FROM hermanos) h2 ON h2.id_tutor = h.id_tutor
      WHERE h.id_alumno = ?;`,[ id_alumno ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve( res )
    })
  })
}

inscripciones.getHermanos = ( id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT h2.*, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, a.matricula FROM hermanos h 
        LEFT JOIN (SELECT * FROM hermanos) h2 ON h2.id_tutor = h.id_tutor
        LEFT JOIN alumnos a ON a.id_alumno = h2.id_alumno
        WHERE h.id_alumno = ?;`,[ id_alumno ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve( res )
    })
  })
}

/*
  VERIFICAR EL SALDO A FAVOR
*/
inscripciones.getSaldoPositivo = ( id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT IFNULL(SUM(monto),0) AS acumulado FROM saldofavoralumnos 
      WHERE id_alumno = ?
      AND deleted = 0
      AND tipo = 1;`,[ id_alumno ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve( res[0] )
    })
  })
}

inscripciones.getSaldoFavorGrupo = ( id_alumno, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT IFNULL(SUM(monto),0) AS acumulado FROM saldofavoralumnos 
      WHERE id_alumno = ?
      AND deleted = 0
      AND tipo = 2
      AND id_grupo = ?;`,[ id_alumno, id_grupo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve( res[0] )
    })
  })
}

inscripciones.getSaldoNegativo = ( id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT IFNULL(SUM(monto),0) AS resta FROM saldofavoralumnos 
      WHERE id_alumno = ?
      AND deleted = 0
      AND tipo = 2;`,[ id_alumno ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve( res[0] )
    })
  })
}

inscripciones.addSaldoFavor = ( id_alumno, id_grupo, monto, concepto, tipo, id_grupo_utilizo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO saldofavoralumnos(id_alumno,id_grupo,monto,concepto,id_usuario_ultimo_cambio,tipo,id_grupo_utilizo) 
      VALUES(?,?,?,?,334,?,?);`,
      [ id_alumno, id_grupo, monto, concepto, tipo, id_grupo_utilizo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ idsaldofavoralumnos: res.insertId, id_alumno, id_grupo, monto, concepto, tipo })
    })
  })
}

inscripciones.updateCorreoAlumno = ( u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE alumnos SET email = ? WHERE id_alumno = ?;`, 
      [ u.email, u.id_alumno ],
      (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

inscripciones.existeFactura = ( id_alumno, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM fyn_proc_alumnos_grupos_iva 
      WHERE id_alumno = ? AND id_grupo = ? AND activo_sn = 1;`,[ id_alumno, id_grupo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve( res[0] )
    })
  })
}

inscripciones.solicitarFactura = ( id_alumno, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO fyn_proc_alumnos_grupos_iva(id_alumno,id_grupo,id_usuario_ultimo_cambio) 
      VALUES(?,?,334);`,
      [ id_alumno, id_grupo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ idsaldofavoralumnos: res.insertId, id_alumno, id_grupo })
    })
  })
}

// OBTENER EL DESCUENTO DE INDCCUIÓn
inscripciones.getDescuentoInduccion = ( id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT ga.* FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE ga.id_alumno = ? 
      AND g.grupo LIKE '%INDUCCION%';`,[id_alumno],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

// OBTENER EL DESCUENTO DE INDCCUIÓn
inscripciones.getDescuentoRecomendados = ( id_alumno, escuela, id_grupo ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT r.*, ? AS escuela FROM recomienda r
      LEFT JOIN usuarios u ON u.id = r.id_alumno
      WHERE r.deleted = 0
      AND r.id_grupo_recomendado > 0
      AND r.aplicado = ?
      AND r.inscrito = 1
      AND u.iderp = ?;`,[escuela, id_grupo, id_alumno],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

// OBTENER EL DESCUENTO DE INDCCUIÓn
inscripciones.pagoGrupoCargaEspecial = ( id_alumno, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM alumnos_grupos_especiales_carga WHERE id_alumno = ? AND id_grupo = ?;`,[ id_alumno, id_grupo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

inscripciones.updateRecomienda = ( id_grupo, id_recomienda, escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`UPDATE recomienda SET id_grupo = ?, aplicado = 1 WHERE id_recomienda = ?;`, [ id_grupo, id_recomienda ],
      (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

// VER LA DIFERENCIA DE FECHAS DE CICLO
inscripciones.diferenciaCiclos = ( fecha_fin, fecha_inicio ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT DATEDIFF(?, ?) AS diferencia;`,[fecha_fin, fecha_inicio],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

inscripciones.getMatriculaViejita = ( matricula_erp ) => {
  return new Promise((resolve,reject)=>{
    sqlERPViejo.query(`SELECT * FROM alumnos WHERE matricula = ?;`,[ matricula_erp ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}


inscripciones.getPagosERP = ( id_plantel, id_ciclo, erp ) => {
  const db = erp == 1 ? sqlERP : sqlERPViejo
  return new Promise((resolve,reject)=>{
    db.query(`SELECT a.matricula, a.matricula_erp, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ',IFNULL(a.apellido_materno,'')) AS alumno,
      i.monto_pagado, (IFNULL(sp.saldo_positivo,0) - IFNULL(sn.saldo_negativo,0)) AS saldofavor, g.grupo, p.plantel, u.nombre_completo  FROM ingresos i
      LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN usuarios u ON u.id_usuario = i.id_usuario_ultimo_cambio
      LEFT JOIN alumnos a ON a.id_alumno = i.id_alumno
      LEFT JOIN (SELECT SUM(monto) AS saldo_positivo, id_alumno FROM saldofavoralumnos WHERE tipo = 1 GROUP BY id_alumno ) sp ON sp.id_alumno = i.id_alumno
      LEFT JOIN (SELECT SUM(monto) AS saldo_negativo, id_alumno FROM saldofavoralumnos WHERE tipo = 2 GROUP BY id_alumno ) sn ON sn.id_alumno = i.id_alumno
      WHERE g.id_ciclo = ? AND g.id_plantel IN (?)
      ORDER BY CONCAT(a.nombre, ' ', a.apellido_paterno, ' ',IFNULL(a.apellido_materno,''));`,[ id_ciclo, id_plantel ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

inscripciones.getPagosERPCiclo = ( id_ciclo, erp, escuela) => {
  const db = erp == 1 ? sqlERP : sqlERPViejo
  return new Promise((resolve,reject)=>{
    db.query(`SELECT a.matricula, a.matricula_erp, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ',IFNULL(a.apellido_materno,'')) AS alumno,
      i.monto_pagado, (IFNULL(sp.saldo_positivo,0) - IFNULL(sn.saldo_negativo,0)) AS saldofavor, g.grupo, p.plantel, u.nombre_completo  FROM ingresos i
      LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN usuarios u ON u.id_usuario = i.id_usuario_ultimo_cambio
      LEFT JOIN alumnos a ON a.id_alumno = i.id_alumno
      LEFT JOIN (SELECT SUM(monto) AS saldo_positivo, id_alumno FROM saldofavoralumnos WHERE tipo = 1 GROUP BY id_alumno ) sp ON sp.id_alumno = i.id_alumno
      LEFT JOIN (SELECT SUM(monto) AS saldo_negativo, id_alumno FROM saldofavoralumnos WHERE tipo = 2 GROUP BY id_alumno ) sn ON sn.id_alumno = i.id_alumno
      WHERE g.id_ciclo = ?
      ORDER BY CONCAT(a.nombre, ' ', a.apellido_paterno, ' ',IFNULL(a.apellido_materno,''));`,[ id_ciclo, escuela],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}


inscripciones.getPlantelesINBI = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERPViejo.query(`SELECT plantel FROM planteles WHERE plantel NOT LIKE '%FAST%' AND plantel NOT LIKE '%CEAA Asesorias%' AND plantel NOT LIKE '%Entrenamiento%' AND plantel NOT LIKE '%Corporativo%' AND plantel <> "INBI";`,[ ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

inscripciones.getPlantelesFAST = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERPViejo.query(`SELECT plantel FROM planteles WHERE plantel LIKE '%FAST%' AND plantel NOT LIKE '%CEAA Asesorias%' AND plantel NOT LIKE '%Entrenamiento%' AND plantel NOT LIKE '%Corporativo%' AND plantel <> "INBI" AND plantel <> "FAST ENGLISH";`,[ ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}


inscripciones.getUltimoGrupos = ( mapMatriculas, erp ) => {
  const db = erp == 1 ? sqlERP : sqlERPViejo
  return new Promise((resolve,reject)=>{
    db.query(`SELECT a.matricula, (SELECT id_ciclo FROM grupos WHERE id_grupo = MAX(g.id_grupo)) AS id_ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      WHERE a.matricula IN ( ? )
      GROUP BY ga.id_alumno;`,[ mapMatriculas ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

inscripciones.getUltimoGruposCiclo = ( mapMatriculas, erp, idCiclo ) => {
  const db = erp == 1 ? sqlERP : sqlERPViejo
  return new Promise((resolve,reject)=>{
    db.query(`SELECT a.matricula, (SELECT g2.id_ciclo FROM gruposalumnos ga2 
      LEFT JOIN grupos g2 ON g2.id_grupo = ga2.id_grupo
      LEFT JOIN ciclos c2 ON c2.id_ciclo = g2.id_ciclo
      WHERE id_alumno = a.id_alumno
      ORDER BY c2.fecha_inicio_ciclo DESC 
      LIMIT 1) AS id_ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      WHERE a.matricula IN ( ? )
      GROUP BY a.matricula;`,[ mapMatriculas, idCiclo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

inscripciones.getUltimoSaldoFavor = ( id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM saldofavoralumnos WHERE id_alumno = ?
      AND tipo = 1
      ORDER BY fecha_creacion DESC
      LIMIT 1;`,[ id_alumno ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}


inscripciones.updateSaldoFavor = ( id_grupo, idsaldofavoralumnos ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE saldofavoralumnos SET id_grupo_utilizo = ? WHERE idsaldofavoralumnos = ?;`, [ id_grupo, idsaldofavoralumnos ],
      (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

inscripciones.getGrupoAlumno = ( id_alumno, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM gruposalumnos WHERE id_alumno = ? AND id_grupo = ?;`,[ id_alumno, id_grupo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}


inscripciones.getIngresosAlumno = ( id_alumno, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM ingresos WHERE id_alumno = ? AND id_grupo = ?;`,[ id_alumno, id_grupo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}


inscripciones.GetCargaEspecialAlumno = ( id_alumno, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM alumnos_grupos_especiales_carga WHERE id_alumno = ? AND id_grupo = ?;`,[ id_alumno, id_grupo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}


inscripciones.updateGrupoAlumnoMonto = ( id, monto ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE gruposalumnos SET monto_pagado_total = ( monto_pagado_total - ? ) WHERE id_gruposalumnos = ?;`, [ monto, id ],
      (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

inscripciones.updateIngresoAlumno = ( id, monto ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE ingresos SET monto_pagado = ( monto_pagado - ? ) WHERE id_ingreso = ?;`, [ monto, id ],
      (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

inscripciones.updateCargaEspecialMonto = ( id, monto ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE alumnos_grupos_especiales_carga SET pagado = ( pagado - ? ) WHERE id = ?;`, [ monto, id ],
      (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

inscripciones.getAlumnoLMSInduccion = ( id_alumno, escuela, tipo ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI

  return new Promise((resolve,reject)=>{
    db.query(`SELECT * FROM induccion WHERE iderp = ? AND deleted = 0 AND tipo = ?;`,[ id_alumno, tipo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}


inscripciones.addAlumnoInduccion = ( id_alumno, id_grupo, pago, tipo, escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`INSERT INTO induccion(iderp,id_grupo,pagado,tipo) VALUES(?,?,?,?);`,[ id_alumno, id_grupo, pago, tipo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, id_alumno, id_grupo, pago, tipo, escuela })
    })
  })
}

// Angel Rodriguez
inscripciones.getTutorAlumno = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT *, CONCAT(nombre, ' ', apellido_paterno, ' ', apellido_materno) AS tutor
    FROM alumnotutor
    WHERE id_alumno = ?;`,[ id ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}


inscripciones.getPreciosAlumno = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT a.idalumnos_id_curso, a.id_alumno, a.id_curso, b.curso, b.precio_nivel 
    FROM alumnos_id_curso a
    LEFT JOIN cursos b ON a.id_curso = b.id_curso
    WHERE a.id_alumno = ? AND a.deleted = 0
    ORDER BY a.fecha_creacion DESC LIMIT 1;`,[ id ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

inscripciones.getAllPrecios = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT iddescuentos_cursos, id_curso, precio_lista FROM descuentos_cursos WHERE deleted = 0 ORDER BY precio_lista;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}


inscripciones.updatePrecioAlumno = (id_curso, id_alumno) => {
  return new Promise((resolve, reject) => {
   sqlERP.query(`UPDATE alumnos_id_curso SET id_curso = ? WHERE id_alumno = ? AND idalumnos_id_curso > 0;`, [id_curso, id_alumno], (err, res) => {
     if (err) {
       return reject({ message: err.sqlMessage })
      }
      resolve(res);
    })
  })
}

inscripciones.insertPrecioAlumno = ( id_alumno, id_curso ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO alumnos_id_curso(id_alumno, id_curso) VALUES (?, ?);`, [ id_alumno, id_curso ],
      (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, id_alumno, id_curso })
    })
  })
}


module.exports = inscripciones;