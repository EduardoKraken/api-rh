const { result }  = require("lodash");

// Prueba
const sqlERP      = require("../dbErpPrueba.js");
const sqlERPNUEVO = require("../db.js");
const sqlFAST     = require("../dbFAST.js");
const sqlINBI     = require("../dbINBI.js");
const sqlERPVIEJO = require("../db3.js");



/*************************************************************************/
/******************************* F A S T *********************************/
/*************************************************************************/

// constructor
const riesgo = function (riesgo) {
    this.idweb = riesgo.idweb;
    this.nomcli = riesgo.nomcli;
    this.calle = riesgo.calle;
};


riesgo.getRiesgoAlumnoRegistro = ( escuela ) => {
  return new Promise(( resolve, reject ) => {
    const bd = escuela == 1 ? sqlINBI : sqlFAST

    bd.query(`SELECT CONCAT(u.nombre," ",u.apellido_paterno," ",IFNULL(u.apellido_materno,"")) as nombre, g.nombre AS grupo, u.id, g.id as id_grupo FROM usuarios u 
      LEFT JOIN datos_alumno d ON d.id_usuario = u.id
      LEFT JOIN grupo_alumnos ga ON ga.id_alumno =  u.id
      LEFT JOIN grupos g ON g.id = ga.id_grupo
      WHERE u.id_tipo_usuario = 1 AND g.nombre IS NOT NULL AND d.id_usuario IS NULL;`,(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};

riesgo.getAlumnosRiesgoPago = (iderp, result)=>{
  return new Promise(( resolve, reject ) => {
    sqlERP.query(`SELECT id_alumno, pagado AS total_pagado FROM alumnos_grupos_especiales_carga WHERE id_ciclo = ?;`,[iderp],(err,res)=>{
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};

riesgo.getCantRecibos = ( idAlumnosRecibor )=>{
  return new Promise(( resolve, reject ) => {
    sqlERP.query(`SELECT COUNT(*) AS cant_recibo, id_alumno, id_grupo 
      FROM recibos_enviados 
      WHERE id_alumno IN (?);`,[idAlumnosRecibor],(err,res)=>{
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};

riesgo.getAlumnos = ( ciclo, escuela ) => {
  return new Promise(( resolve, reject ) => {
    const bd = escuela == 1 ? sqlINBI : sqlFAST

    bd.query(`SELECT g.nombre as grupo, CONCAT(u.nombre," ",u.apellido_paterno," ", IFNULL(u.apellido_materno,"")) as nombre, u.id, g.id as id_grupo, g.id_curso, g.id_nivel, g.codigo_acceso, g.id_plantel,u.iderp,
      (SELECT fecha_inicio FROM ciclos WHERE iderp = ${ ciclo }) as inicio, NOW() as hoy, u.usuario AS matricula,
      DATEDIFF(NOW(),(SELECT fecha_inicio FROM ciclos WHERE iderp = ${ ciclo })) as diferencia
      FROM grupo_alumnos ga
      LEFT JOIN grupos g ON g.id = ga.id_grupo
      LEFT JOIN usuarios u ON u.id = ga.id_alumno
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      WHERE c.iderp = ${ ciclo } AND g.nombre NOT LIKE '%INDUC%';`,(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};


riesgo.getAsistencias = ( ciclo, escuela ) => {
  return new Promise(( resolve, reject ) => {
    const bd = escuela == 1 ? sqlINBI : sqlFAST

    bd.query(`SELECT a.valor_asistencia, a.id_usuario, a.fecha_asistencia
      FROM asistencia_grupo a
      LEFT JOIN grupos g ON a.id_grupo = g.id 
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      WHERE c.iderp = ${ ciclo } 
      ORDER BY a.id_usuario;`,(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};

riesgo.getEjercicios = ( ciclo, escuela ) => {
  return new Promise(( resolve, reject ) => {
    const bd = escuela == 1 ? sqlINBI : sqlFAST

    bd.query(`SELECT ce.id_alumno, ce.id_grupo, COUNT(*) as contestado FROM control_grupos_calificaciones_evaluaciones ce 
        LEFT JOIN grupo_alumnos ga ON ce.id_grupo = ga.id_grupo
        LEFT JOIN grupos g ON g.id = ga.id_grupo 
        LEFT JOIN evaluaciones e ON e.id = ce.id_evaluacion
        LEFT JOIN ciclos c ON c.id = g.id_ciclo
        WHERE c.iderp = ${ ciclo } AND e.tipo_evaluacion = 1 AND e.idCategoriaEvaluacion = 1  AND ce.id_grupo = g.id AND ce.id_alumno = ga.id_alumno
        GROUP BY ce.id_alumno, ce.id_grupo;`,(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};

riesgo.getExamenes = ( ciclo, escuela ) => {
  return new Promise(( resolve, reject ) => {
    const bd = escuela == 1 ? sqlINBI : sqlFAST

    bd.query(`SELECT cc.calificacion, cc.id_alumno as id FROM control_grupos_calificaciones_evaluaciones cc 
      LEFT JOIN grupos g ON g.id = cc.id_grupo 
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      WHERE c.iderp = ${ ciclo }
      AND  cc.id_evaluacion IN (SELECT id FROM evaluaciones e 
        WHERE e.tipo_evaluacion = 2 
        AND e.idCategoriaEvaluacion = 1 OR e.tipo_evaluacion = 2 
        AND e.idCategoriaEvaluacion = 2
      );`,(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};

riesgo.getRecursoFaltaEjercicios = (c,result)=>{
  return new Promise(( resolve, reject ) => {
    const bd = c.escuela == 1 ? sqlINBI : sqlFAST

    bd.query(`SELECT r.nombre, r.id_leccion, (SELECT calificacion FROM control_grupos_calificaciones_evaluaciones WHERE  id_grupo = ? AND id_alumno = ? AND id_evaluacion = r.id_evaluacion) as calificacion
      FROM recursos r WHERE r.id_nivel = ? AND r.id_curso = ? AND r.id_tipo_evaluacion = 1 AND r.id_categoria_evaluacion = 1 AND r.deleted = 0
      AND (SELECT calificacion FROM control_grupos_calificaciones_evaluaciones WHERE  id_grupo = ? AND id_alumno = ? AND id_evaluacion = r.id_evaluacion) IS NULL
      AND r.id_leccion IN (SELECT sesion FROM grupo_fechas g WHERE g.fecha <= CURRENT_DATE AND idgrupo = ?);`,
      [ c.id_grupo, c.id_alumno, c.nivel, c.curso, c.id_grupo, c.id_alumno, c.id_grupo ],(err,res)=>{
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};

riesgo.getRecursoFaltaExamenes = (c,result)=>{
  return new Promise(( resolve, reject ) => {
    const bd = c.escuela == 1 ? sqlINBI : sqlFAST

    bd.query(`SELECT r.nombre, r.id_leccion, (SELECT calificacion FROM control_grupos_calificaciones_evaluaciones WHERE  id_grupo = ? AND id_alumno = ? AND id_evaluacion = r.id_evaluacion) as calificacion
      FROM recursos r WHERE r.id_nivel = ? AND r.id_curso = ? AND r.id_tipo_evaluacion = 2 AND r.id_categoria_evaluacion = 2 AND r.deleted = 0
      AND (SELECT calificacion FROM control_grupos_calificaciones_evaluaciones WHERE  id_grupo = ? AND id_alumno = ? AND id_evaluacion = r.id_evaluacion) IS NULL;`,
      [c.id_grupo,c.id_alumno,c.nivel, c.curso,c.id_grupo,c.id_alumno],(err,res)=>{
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};


riesgo.getAlumnosRiesgo = ( ciclo, escuela ) => {
  const bd = escuela == 1 ? sqlINBI : sqlFAST

  // Alumnos inscritos
  bd.query(`SELECT g.nombre as grupo, CONCAT(u.nombre," ",u.apellido_paterno," ", IFNULL(u.apellido_materno,"")) as nombre, u.id, g.id as id_grupo, g.id_curso, g.id_nivel, g.codigo_acceso, g.id_plantel,u.iderp,
            (SELECT fecha_inicio FROM ciclos WHERE id = ?) as inicio, NOW() as hoy, u.usuario AS matricula,
            DATEDIFF(NOW(),(SELECT fecha_inicio FROM ciclos WHERE id = ?)) as diferencia
            FROM grupo_alumnos ga
						LEFT JOIN grupos g ON g.id = ga.id_grupo
						LEFT JOIN usuarios u ON u.id = ga.id_alumno
						WHERE g.id_ciclo = ? AND g.nombre NOT LIKE '%INDUC%';`, [c,c,c], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }

    // No hubo error, si trajo a los alumnos
    var paylod = []
    // Asistencias de los alumnos
    bd.query(`SELECT a.valor_asistencia, a.id_usuario, a.fecha_asistencia
     FROM asistencia_grupo a
      LEFT JOIN grupos g ON a.id_grupo = g.id 
      WHERE g.id_ciclo = ?
      ORDER BY a.id_usuario;`,[ciclo],(err, asistencias) => {

      // Ejercicios
      bd.query(`SELECT ce.id_alumno, ce.id_grupo, COUNT(*) as contestado FROM control_grupos_calificaciones_evaluaciones ce 
        LEFT JOIN grupo_alumnos ga ON ce.id_grupo = ga.id_grupo
        LEFT JOIN grupos g ON g.id = ga.id_grupo 
        LEFT JOIN evaluaciones e ON e.id = ce.id_evaluacion
        WHERE g.id_ciclo = ? AND e.tipo_evaluacion = 1 AND e.idCategoriaEvaluacion = 1  AND ce.id_grupo = g.id AND ce.id_alumno = ga.id_alumno
        GROUP BY ce.id_alumno, ce.id_grupo;`,[ciclo],(err, ejercicios) => {


        // Examenes
        bd.query(`SELECT cc.calificacion, cc.id_alumno as id FROM control_grupos_calificaciones_evaluaciones cc 
          LEFT JOIN grupos g ON g.id = cc.id_grupo WHERE g.id_ciclo = ? AND 
          cc.id_evaluacion IN (SELECT id FROM evaluaciones e WHERE e.tipo_evaluacion = 2 AND e.idCategoriaEvaluacion = 1 OR e.tipo_evaluacion = 2 AND e.idCategoriaEvaluacion = 2);`,[ciclo],(err, examenes) => {

          var inicio = new Date(res[0].inicio); //Fecha inicial
          var fin = new Date(res[0].hoy); //Fecha final
          var timeDiff = Math.abs(fin.getTime() - inicio.getTime());
          var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); //Días entre las dos fechas
          var cuentaDom = 0; //Número de  Domingos
          var cuentaSab = 0; //Número de Sábados 
          var cuentaLMV = 0; //Número de lunes a viernes
          var cuentaLV = 0; //Número de lunes a viernes
          var cuentaMJ = 0; //Número de martes y jueves
          var array = new Array(diffDays);

          // Cantidad de días que han transcurrido entre 
          for (var i=0; i < diffDays; i++){
            //0 => Domingo
            if (inicio.getDay() == 0) {
                cuentaDom++;
            }
            //0 => Domingo - 6 => Sábado
            if (inicio.getDay() == 6) {
                cuentaSab++;
            }
            if (inicio.getDay() == 1 || inicio.getDay() == 3 || inicio.getDay() == 5) {
                cuentaLMV++;
            }
            if (inicio.getDay() == 2 || inicio.getDay() == 4) {
                cuentaMJ++;
            }

            if (inicio.getDay() == 1 || inicio.getDay() == 2 || inicio.getDay() == 3 || inicio.getDay() == 4 || inicio.getDay() == 5) {
                cuentaLV++;
            }
            inicio.setDate(inicio.getDate() + 1);
          }

  		    if (err) {
  		      result(null, err);
  		      return;
  		    }

          for(const i in res){
    		    var riesgo = false
    		    var countFalta = 0
    		    var countAsistencia = 0
            var asistencias_totales = 0
            var faltas_totales = 0

            var arrayFalta = []
    		    // No hubo error, si trajo a los alumnos
    		    // Hacemos un ciclo para ver si tiene 2 asistencias seguidas y mandarlo al panel de riesgo
    		    for(const j in asistencias){
              // Comparamos que sea el mismo usuario
              if(asistencias[j].id_usuario == res[i].id){

      		    	if(asistencias[j].valor_asistencia == 2){
                  arrayFalta.push(asistencias[j].fecha_asistencia)
      		    		countFalta += 1
                  faltas_totales += 1
      		    	}else{
                  if(asistencias[j].valor_asistencia > 0 && asistencias[j].valor_asistencia < 4){
                    asistencias_totales += 1
      		    		  countFalta = 0
                  }
      		    	}

              }

    		    }

            // Ciclo para ejercicios
            var estatus_ejercicios = 'NULO'
            var diferencia = 0
            // For para ejercicios
            for(const k in ejercicios){
              // Comparamos que sea el mismo usuario
              if(ejercicios[k].id_alumno == res[i].id){
                // Sacar el curso Lunes a viernes
                switch(res[i].id_curso){
                  case 2: // Lunes a viernes
                    diferencia = cuentaLV - ejercicios[k].contestado
                    if(res[0].diferencia >=  3){
                      if(res[0].diferencia >=  15){
                        if(diferencia > 3){estatus_ejercicios='ALTO'}else{estatus_ejercicios ='NULO'}
                      }else{
                        if(diferencia > 2){estatus_ejercicios='ALTO'}else{estatus_ejercicios ='NULO'}
                      }
                    }else{
                      estatus_ejercicios='NULO'
                    }
                      
                  break;

                  case 4: //Lunes miercoles viernes
                    diferencia = cuentaLMV - ejercicios[k].contestado
                    if(diferencia > 1){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                  break;

                  case 6: //Sabatino
                    if(res[0].diferencia >= 6){
                      diferencia = cuentaSab - ejercicios[k].contestado
                      if(diferencia >= 1){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;

                  case 7: //Dominical
                    if(res[0].diferencia >= 7){
                      diferencia = cuentaDom - ejercicios[k].contestado
                      if(diferencia >= 1){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;

                  case 8: //Martes y jueves
                    if(res[0].diferencia >= 2){
                      diferencia = cuentaMJ - ejercicios[k].contestado
                      if(diferencia > 1){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;
                }

              }
            }

            const found = ejercicios.find( id => id.id_alumno == res[i].id  );
            if(!found){
              estatus_ejercicios='ALTO'
            }

            var estatus = 'NULO'
            switch(res[i].id_curso){
              case 2: // Lunes a viernes
                if(res[0].diferencia >= 2){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              case 4: //Lunes miercoles viernes
                if(res[0].diferencia >= 2){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              case 6: //Sabatino
                if(res[0].diferencia >= 6){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              case 7: //Dominical
                if(res[0].diferencia >= 7){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              case 8: //Martes y jueves
                if(res[0].diferencia >= 4){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;
            }// fin del switch

            // Ciclo para examenes
            var count = 0
            var estatus_examenes  = 'NULO'
            
            if(res[0].diferencia >= 15){
              var existeExamen = examenes.find(exa => exa.id == res[i].id && exa.calificacion > 0)
              if(existeExamen){
                estatus_examenes = 'NULO'
              }else{
                estatus_examenes = 'ALTO'
              }
            }


    		    // validamos el estatus para dos faltas seguidas
    		    // Lunes a viernes
            paylod.push({
              matricula:              res[i].matricula,
              id_alumno:              res[i].id,
              grupo:                  res[i].grupo,
              nombre:                 res[i].nombre,
              curso:                  res[i].id_curso,
              nivel:                  res[i].id_nivel,
              estatus:                estatus,
              estatus_ejercicios:     estatus_ejercicios,
              diferencia:             diferencia,
              estatus_examenes:       estatus_examenes,
              estatus_calificaciones: 'NULO',
              estatus_registro:       'NULO',
              id_grupo:               res[i].id_grupo,
              id_plantel:             res[i].id_plantel,
              faltas:                 arrayFalta,
              iderp:                  res[i].iderp,
              monto:                  'NI',
            })

          }
          result(null, paylod);
  		  });
      })
    })
  });
};

riesgo.getAlumnosPuntos = ( ciclo, escuela ) => {
  const bd = escuela == 1 ? sqlINBI : sqlFAST
  return new Promise((resolve, reject) => {
    bd.query(`SELECT CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")) AS nombre, u.id AS id_alumno, g.nombre AS 'grupo', g.id_curso, g.id AS id_grupo, g.id_nivel,
    cu.valor_asistencia, cu.valor_ejercicios, cu.valor_examenes, cu.total_dias_laborales, cu.num_examenes, cu.num_ejercicios, u.usuario AS matricula, g.id_plantel, cc.resultado AS estado_comentario
    FROM
      grupo_alumnos ga
      LEFT JOIN grupos g ON g.id = ga.id_grupo
      LEFT JOIN usuarios u ON u.id = ga.id_alumno
      LEFT JOIN cursos cu ON cu.id = g.id_curso
      LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
      LEFT JOIN (
        SELECT
          id_alumno,
          id_grupo,
          CASE
            WHEN calificacion_final_primera_oportunidad >= 70 OR calificacion_final_segunda_oportunidad >= 70 THEN 1
			      WHEN calificacion_final_segunda_oportunidad = "" THEN 3
            WHEN calificacion_final_primera_oportunidad < 70 AND calificacion_final_segunda_oportunidad < 70 
                 AND COALESCE((SELECT 1 FROM maestro_mensaje WHERE id_alumno = cardex_curso.id_alumno AND estado = 0), 0) = 1 THEN 2
            ELSE 0
          END AS resultado
        FROM
          cardex_curso
      ) cc ON cc.id_alumno = u.id AND cc.id_grupo = g.id
    WHERE ci.iderp = ${ciclo} AND g.nombre NOT LIKE '%indu%' AND u.id IS NOT NULL;`,(err, res) => {
      if (err) {
        reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  })
};

riesgo.getRiesgoAlumnosPuntos = (idCiclo, escuela ) => {
  const bd = escuela == 1 ? sqlINBI : sqlFAST
  return new Promise((resolve, reject) => {
    bd.query(`SELECT DISTINCT CONCAT(u.nombre," ",u.apellido_paterno," ",IFNULL(u.apellido_materno,"")) as nombre, ccc.resultado AS estado_comentario, c.id_alumno, u.usuario AS matricula, cu.valor_asistencia, da.telefono, da.celular,g.id_curso,g.id_plantel,
    cu.valor_ejercicios, cu.valor_examenes, c.id_grupo, g.nombre as 'grupo',c.calificacion, cu.total_dias_laborales, e.id as idevaluacion, u.usuario AS matricula, g.id_nivel,
    (SELECT fecha_inicio FROM ciclos WHERE id = ?) as inicio, NOW() as hoy,
            DATEDIFF(NOW(),(SELECT fecha_inicio FROM ciclos WHERE id = ?)) as diferencia,
    cu.num_examenes, cu.num_ejercicios, e.tipo_evaluacion, e.idCategoriaEvaluacion,
    cc.calificacion_final_primera_oportunidad, cc.calificacion_final_segunda_oportunidad, ga.id_grupo AS id_grupo_oficial
    FROM control_grupos_calificaciones_evaluaciones c
    LEFT JOIN grupos g ON g.id = c.id_grupo
    LEFT JOIN cursos cu ON cu.id = g.id_curso
    LEFT JOIN evaluaciones e ON e.id = id_evaluacion
    LEFT JOIN datos_alumno da ON da.id_usuario = c.id_alumno
    LEFT JOIN usuarios u ON u.id = c.id_alumno
    LEFT JOIN cardex_curso cc ON cc.id_alumno = u.id AND cc.id_grupo = g.id
    LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
    LEFT JOIN grupo_alumnos ga ON ga.id_alumno = c.id_alumno AND ga.id_grupo = c.id_grupo
    LEFT JOIN (
    SELECT
      id_alumno,
      id_grupo,
      CASE
        WHEN calificacion_final_primera_oportunidad >= 70 OR calificacion_final_segunda_oportunidad >= 70 THEN 1
			  WHEN calificacion_final_segunda_oportunidad = "" THEN 3
        WHEN calificacion_final_primera_oportunidad < 70 AND calificacion_final_segunda_oportunidad < 70 
             AND COALESCE((SELECT 1 FROM maestro_mensaje WHERE id_alumno = cardex_curso.id_alumno AND estado = 0), 0) = 1 THEN 2
        ELSE 0
      END AS resultado
    FROM
      cardex_curso
  ) ccc ON ccc.id_alumno = u.id AND ccc.id_grupo = g.id
    WHERE ci.iderp = ?  AND g.nombre NOT LIKE '%indu%' AND c.deleted = 0 AND u.id IS NOT NULL AND ga.id_grupo IS NOT NULL  ORDER BY nombre;`,[idCiclo,idCiclo,idCiclo],(err, res) => {
      if (err) {
        reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  })
};

riesgo.asistenciasPuntos = ( idCiclo, escuela ) => {
  const bd = escuela == 1 ? sqlINBI : sqlFAST
  return new Promise((resolve, reject) => {
    bd.query(`SELECT a.*, u.usuario AS matricula FROM asistencia_grupo a 
      LEFT JOIN grupos g ON g.id = a.id_grupo
      LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
      LEFT JOIN usuarios u ON u.id = a.id_usuario
      WHERE ci.iderp = ? AND a.deleted = 0;`,[idCiclo],(err, res) => {
      if (err) {
        reject({ message: err.sqlMessage });
      }
      console.log( res[0] )
      resolve(res);
    });
  })
};



riesgo.getCiclosFast = (result)=>{
  sqlFAST.query(`SELECT * FROM ciclos WHERE deleted = 0 AND estatus = 1;`,(err,res)=>{
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  })
};

riesgo.getAlumnosRiesgoPagoFast = (iderp, result)=>{
  sql3.query(`SELECT id_alumno, pagado AS total_pagado FROM alumnos_grupos_especiales_carga WHERE id_ciclo = ?;`,[iderp],(err,res)=>{
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  })
};

// Grupos por teacher
riesgo.getGruposTeacherFast = (data,result)=>{
  sqlFAST.query(`SELECT g.id  FROM grupo_teachers gt
    LEFT JOIN grupos g ON g.id = gt.id_grupo
    LEFT JOIN usuarios u ON u.id = gt.id_teacher OR u.id = gt.id_teacher_2
    WHERE g.id_ciclo = ? AND u.email  = ? ;`,
    [data.ciclo,data.email],(err,res)=>{
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  })
};


// Grupos por teacher
riesgo.getRecursoFaltaFast = (data,result)=>{
  sqlFAST.query(`SELECT r.nombre, r.id_leccion, (SELECT calificacion FROM control_grupos_calificaciones_evaluaciones WHERE  id_ciclo = ? AND id_alumno = ? AND id_evaluacion = r.id_evaluacion) as calificacion
FROM recursos r WHERE r.id_nivel = ? AND r.id_curso = ? AND r.id_tipo_evaluacion = 1 AND r.id_categoria_evaluacion = 1 AND r.deleted = 0;`,
    [data.ciclo,data.id_alumno,data.nivel, data.curso],(err,res)=>{
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  })
};

/*************************************************************************/
/******************************** I N B I ********************************/
/*************************************************************************/

riesgo.getAlumnosRiesgoInbi = (c, result) => {
  var ciclo = c
  sqlERP.query(`SELECT g.nombre as grupo, CONCAT(u.nombre," ",u.apellido_paterno," ", IFNULL(u.apellido_materno,"")) as nombre, u.id, g.id as id_grupo, g.id_curso, g.id_nivel, g.codigo_acceso, g.id_plantel,u.iderp,
            (SELECT fecha_inicio FROM ciclos WHERE id = ?) as inicio, NOW() as hoy, u.usuario AS matricula,
            DATEDIFF(NOW(),(SELECT fecha_inicio FROM ciclos WHERE id = ?)) as diferencia
            FROM grupo_alumnos ga
            LEFT JOIN grupos g ON g.id = ga.id_grupo
            LEFT JOIN usuarios u ON u.id = ga.id_alumno
            WHERE g.id_ciclo = ? AND g.nombre NOT LIKE '%INDUC%';`, [c,c,c], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }

    // No hubo error, si trajo a los alumnos
    var paylod = []
    // for(const i in res){
    // Consultamos las asistencias de los alumnos
    // sqlERP.query(`SELECT valor_asistencia FROM asistencia_grupo WHERE id_grupo = ? AND id_usuario = ?;`, [res[i].id_grupo, res[i].id], (err, asistencias) => {
    sqlERP.query(`SELECT a.valor_asistencia, a.id_usuario, a.fecha_asistencia
     FROM asistencia_grupo a
      LEFT JOIN grupos g ON a.id_grupo = g.id 
      WHERE g.id_ciclo = ?
      ORDER BY a.id_usuario;`,[ciclo],(err, asistencias) => {

      sqlERP.query(`SELECT ce.id_alumno, ce.id_grupo, COUNT(*) as contestado FROM control_grupos_calificaciones_evaluaciones ce 
        LEFT JOIN grupo_alumnos ga ON ce.id_grupo = ga.id_grupo
        LEFT JOIN grupos g ON g.id = ga.id_grupo 
        LEFT JOIN evaluaciones e ON e.id = ce.id_evaluacion
        WHERE g.id_ciclo = ? AND e.tipo_evaluacion = 1 AND e.idCategoriaEvaluacion = 1  AND ce.id_grupo = g.id AND ce.id_alumno = ga.id_alumno
        GROUP BY ce.id_alumno, ce.id_grupo;`,[ciclo],(err, ejercicios) => {

        sqlERP.query(`SELECT u.id,
          IFNULL((SELECT calificacion FROM control_grupos_calificaciones_evaluaciones cc WHERE 
          cc.id_alumno = u.id AND cc.id_grupo = g.id AND
          cc.id_evaluacion IN(SELECT id FROM evaluaciones e WHERE e.tipo_evaluacion = 2 AND e.idCategoriaEvaluacion = 1 OR e.tipo_evaluacion = 2 AND e.idCategoriaEvaluacion = 2) LIMIT 1),0) As calificacion
          FROM grupo_alumnos ga
          LEFT JOIN usuarios u ON u.id = ga.id_alumno
          LEFT JOIN grupos g ON g.id = ga.id_grupo
          WHERE g.id_ciclo = ?;`,[ciclo],(err, examenes) => {

          var inicio = new Date(res[0].inicio); //Fecha inicial
          var fin = new Date(res[0].hoy); //Fecha final
          var timeDiff = Math.abs(fin.getTime() - inicio.getTime());
          var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); //Días entre las dos fechas
          var cuentaLV = 0; //Número de lunes a viernes
          var cuentaSab = 0; //Número de Sábados 
          var cuentaLMV = 0; //Número de lunes a viernes
          var cuentaLJ = 0; //Número de martes y jueves
          var array = new Array(diffDays);

          // 2 Lunes a Viernes
          // 4 Lunes miercoles viernes
          // 9 Lunes a jueves
          // 3 Sabatino semi-intensivo a
          // 7 Sabatino
          // 8 Sabatino semi-intensivo b

          for (var i=0; i < diffDays; i++){
            //6 => Sábado
            if (inicio.getDay() == 6) {
                cuentaSab++;
            }
            // Lunes miercoles y viernes
            if (inicio.getDay() == 1 || inicio.getDay() == 3 || inicio.getDay() == 5) {
                cuentaLMV++;
            }
            // Lunes a juves
            if (inicio.getDay() == 1 || inicio.getDay() == 2 || inicio.getDay() == 3 || inicio.getDay() == 4) {
                cuentaLJ++;
            }
            // Lunes a viernes
            if (inicio.getDay() == 1 || inicio.getDay() == 2 || inicio.getDay() == 3 || inicio.getDay() == 4 || inicio.getDay() == 5) {
                cuentaLV++;
            }
            inicio.setDate(inicio.getDate() + 1);
          }

          if (err) {
            result(null, err);
            return;
          }

          for(const i in res){
            var riesgo = false
            var countFalta = 0
            var countAsistencia = 0
            var asistencias_totales = 0
            var faltas_totales = 0

            var arrayFalta = []
            // No hubo error, si trajo a los alumnos
            // Hacemos un ciclo para ver si tiene 2 asistencias seguidas y mandarlo al panel de riesgo
            for(const j in asistencias){
              // Comparamos que sea el mismo usuario
              if(asistencias[j].id_usuario == res[i].id){

                if(asistencias[j].valor_asistencia == 2){
                  arrayFalta.push(asistencias[j].fecha_asistencia)
                  countFalta += 1
                  faltas_totales += 1
                }else{
                  if(asistencias[j].valor_asistencia > 0 && asistencias[j].valor_asistencia < 4){
                    asistencias_totales += 1
                    countFalta = 0
                  }
                }

              }

            }


          // 2 Lunes a Viernes
          // 4 Lunes miercoles viernes
          // 9 Lunes a jueves
          // 3 Sabatino semi-intensivo a
          // 8 Sabatino semi-intensivo b
          // 7 Sabatino

            // Ciclo para ejercicios
            var estatus_ejercicios = 'NULO'
            var diferencia = 0
            // For para ejercicios
            for(const k in ejercicios){
              // Comparamos que sea el mismo usuario
              if(ejercicios[k].id_alumno == res[i].id){
                // Sacar el curso Lunes a viernes
                switch(res[i].id_curso){
                  case 2: // Lunes a viernes
                    if(res[0].diferencia >= 2){
                      diferencia = cuentaLV - ejercicios[k].contestado
                      if(res[0].diferencia >= 15){
                        if(diferencia > 3){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                      }else{
                        if(diferencia > 2){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                      }
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;

                  case 4: //Lunes miercoles viernes
                    if(res[0].diferencia >= 2){
                      diferencia = cuentaSab - ejercicios[k].contestado
                      if(diferencia >= 2){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;

                  case 9: //Lunes a jueves
                    if(res[0].diferencia >= 2){
                      diferencia = cuentaSab - ejercicios[k].contestado
                      if(diferencia >= 2){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;

                  // Todos los cursos del sabatino
                  case 3: //Sabatino
                    if(res[0].diferencia >= 6){
                      diferencia = cuentaSab - ejercicios[k].contestado
                      if(diferencia >= 1){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;

                  case 7: //Sabatino
                    if(res[0].diferencia >= 6){
                      diferencia = cuentaSab - ejercicios[k].contestado
                      if(diferencia >= 1){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;

                  case 8: //Martes y jueves
                    if(res[0].diferencia >= 2){
                      diferencia = cuentaSab - ejercicios[k].contestado
                      if(diferencia >= 1){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;
                }

              }
            }


            const found = ejercicios.find( id => id.id_alumno == res[i].id  );
            if(!found){
              estatus_ejercicios='ALTO'
            }

            // Ciclo para examenes
            var estatus_examenes  = 'NULO'


            if(res[0].diferencia >= 15){

              var existeExamen = examenes.find(exa => exa.id == res[i].id && exa.calificacion > 0)
              if(existeExamen){
                estatus_examenes = 'NULO'
              }else{
                estatus_examenes = 'ALTO'
              }
            }

            // Calular asistencias
            var estatus = 'NULO'
            switch(res[i].id_curso){
              case 2: // Lunes a viernes
                if(res[0].diferencia >= 3){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              case 4: //Lunes miercoles viernes
                if(res[0].diferencia >= 4){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              case 9: //Lunes a jueves
                if(res[0].diferencia >= 3){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              // Todos los cursos del sabatino
              case 3: //Sabatino
                if(res[0].diferencia >= 6){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              case 7: //Sabatino
                if(res[0].diferencia >= 6){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              case 8: //Martes y jueves
                if(res[0].diferencia >= 2){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;
            }

            

            paylod.push({
              matricula:               res[i].matricula,
              id_alumno:               res[i].id,
              grupo:                   res[i].grupo,
              nombre:                  res[i].nombre,
              curso:                   res[i].id_curso,
              nivel:                   res[i].id_nivel,
              estatus:                 estatus,
              estatus_ejercicios:      estatus_ejercicios,
              diferencia:              diferencia,
              estatus_examenes:        estatus_examenes,
              estatus_calificaciones:  'NULO',
              estatus_registro:        'NULO',
              id_grupo:                res[i].id_grupo,
              id_plantel:              res[i].id_plantel,
              faltas:                  arrayFalta,
              iderp:                   res[i].iderp,
              monto:                   'NI',
            })

          }
          result(null, paylod);
        });
      })
    })
    
  });
};

riesgo.getCiclosInbi = (result)=>{
  sqlINBI.query(`SELECT * FROM ciclos WHERE deleted = 0 AND estatus = 1;`,(err,res)=>{
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  })
};

// Grupos por teacher
riesgo.getGruposTeacherInbi = (data,result)=>{
  sqlERP.query(`SELECT g.id  FROM grupo_teachers gt
    LEFT JOIN grupos g ON g.id = gt.id_grupo
    LEFT JOIN usuarios u ON u.id = gt.id_teacher OR u.id = gt.id_teacher_2
    WHERE g.id_ciclo = ? AND u.email  = ? ;`,
    [data.ciclo,data.email],(err,res)=>{
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  })
};

// Grupos por teacher
riesgo.getRecursoFaltaInbi = (data,result)=>{
  sqlERP.query(`SELECT r.nombre, r.id_leccion, (SELECT calificacion FROM control_grupos_calificaciones_evaluaciones WHERE  id_ciclo = ? AND id_alumno = ? AND id_evaluacion = r.id_evaluacion) as calificacion
FROM recursos r WHERE r.id_nivel = ? AND r.id_curso = ? AND r.id_tipo_evaluacion = 1 AND r.id_categoria_evaluacion = 1 AND r.deleted = 0;`,
    [data.ciclo,data.id_alumno,data.nivel, data.curso],(err,res)=>{
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  })
};



riesgo.getCalificacionesAlumnos = (data,result) => {
  sqlFAST.query(`SELECT CONCAT(u.nombre," ",u.apellido_paterno," ",u.apellido_materno) as nombre, c.id_alumno, cu.valor_asistencia, da.telefono, da.celular,
      cu.valor_ejercicios, cu.valor_examenes, c.id_grupo, g.nombre as 'grupo',c.calificacion, cu.total_dias_laborales,
      cu.num_examenes, cu.num_ejercicios, e.tipo_evaluacion, e.idCategoriaEvaluacion,
      (SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 1) as asistencia,
      (SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 2) as falta,
      (SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 3) as retardo,
      (SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 4) as justicada
       FROM control_grupos_calificaciones_evaluaciones c
      LEFT JOIN grupos g ON g.id = c.id_grupo
      LEFT JOIN cursos cu ON cu.id = g.id_curso
      LEFT JOIN evaluaciones e ON e.id = id_evaluacion
      LEFT JOIN usuarios u ON u.id = c.id_alumno
      LEFT JOIN datos_alumno da ON da.id_usuario = u.id
            WHERE  g.id_plantel = ? AND g.id_ciclo = ? AND c.deleted = 0 ORDER BY nombre;`,[data.id_plantel, data.ciclo],(err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  })
};

riesgo.getCalificacionesAlumnosInbi = (data,result) => {
  sqlERP.query(`SELECT CONCAT(u.nombre," ",u.apellido_paterno," ",u.apellido_materno) as nombre, c.id_alumno, cu.valor_asistencia, da.telefono, da.celular,
    cu.valor_ejercicios, cu.valor_examenes, c.id_grupo, g.nombre as 'grupo',c.calificacion, cu.total_dias_laborales,
    cu.num_examenes, cu.num_ejercicios, e.tipo_evaluacion, e.idCategoriaEvaluacion,
    (SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 1) as asistencia,
    (SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 2) as falta,
    (SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 3) as retardo,
    (SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 4) as justicada
     FROM control_grupos_calificaciones_evaluaciones c
    LEFT JOIN grupos g ON g.id = c.id_grupo
    LEFT JOIN cursos cu ON cu.id = g.id_curso
    LEFT JOIN evaluaciones e ON e.id = id_evaluacion
    LEFT JOIN usuarios u ON u.id = c.id_alumno
    LEFT JOIN datos_alumno da ON da.id_usuario = u.id
            WHERE  g.id_plantel = ? AND g.id_ciclo = ? AND c.deleted = 0 ORDER BY nombre;`,[data.id_plantel, data.ciclo],(err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  })
};


riesgo.getAlumnosFAST = ( ciclo ) => {
   return new Promise((resolve, reject) => {
    sqlFAST.query(`SELECT CONCAT(u.nombre," ",u.apellido_paterno," ",IFNULL(u.apellido_materno,"")) as nombre, u.id AS id_alumno, g.nombre as 'grupo', g.id_curso, g.id AS id_grupo,
    cu.valor_asistencia, cu.valor_ejercicios, cu.valor_examenes, cu.total_dias_laborales, cu.num_examenes, cu.num_ejercicios FROM grupo_alumnos ga 
    LEFT JOIN grupos g ON g.id = ga.id_grupo 
    LEFT JOIN usuarios u ON u.id = ga.id_alumno
    LEFT JOIN cursos cu ON cu.id = g.id_curso
    WHERE g.id_ciclo = ${ciclo} AND g.nombre NOT LIKE '%certi%' AND g.nombre NOT LIKE '%indu%';`,(err, res) => {
      if (err) {
        reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  })
};


riesgo.getRiesgoAlumnosFast = (idCiclo) => {
   return new Promise((resolve, reject) => {
    sqlFAST.query(`SELECT DISTINCT CONCAT(u.nombre," ",u.apellido_paterno," ",IFNULL(u.apellido_materno,"")) as nombre, c.id_alumno, cu.valor_asistencia, da.telefono, da.celular,g.id_curso,g.id_plantel,
    cu.valor_ejercicios, cu.valor_examenes, c.id_grupo, g.nombre as 'grupo',c.calificacion, cu.total_dias_laborales, e.id as idevaluacion, u.usuario AS matricula,
    (SELECT fecha_inicio FROM ciclos WHERE id = ?) as inicio, NOW() as hoy,
            DATEDIFF(NOW(),(SELECT fecha_inicio FROM ciclos WHERE id = ?)) as diferencia,
    cu.num_examenes, cu.num_ejercicios, e.tipo_evaluacion, e.idCategoriaEvaluacion,
    cc.calificacion_final_primera_oportunidad, cc.calificacion_final_segunda_oportunidad
    FROM control_grupos_calificaciones_evaluaciones c
    LEFT JOIN grupos g ON g.id = c.id_grupo
    LEFT JOIN cursos cu ON cu.id = g.id_curso
    LEFT JOIN evaluaciones e ON e.id = id_evaluacion
    LEFT JOIN datos_alumno da ON da.id_usuario = c.id_alumno
    LEFT JOIN usuarios u ON u.id = c.id_alumno
    LEFT JOIN cardex_curso cc ON cc.id_alumno = u.id AND cc.id_grupo = g.id
    WHERE g.id_ciclo = ? AND g.nombre NOT LIKE '%certi%' AND g.nombre NOT LIKE '%indu%' AND c.deleted = 0 ORDER BY nombre;`,[idCiclo,idCiclo,idCiclo],(err, res) => {
      if (err) {
        reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  })
};

riesgo.asistenciasFast = (idCiclo) => {
   return new Promise((resolve, reject) => {
    sqlFAST.query(`SELECT a.* FROM asistencia_grupo a 
      LEFT JOIN grupos g ON g.id = a.id_grupo
      WHERE g.id_ciclo = ? ;`,[idCiclo],(err, res) => {
      if (err) {
        reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  })
};


riesgo.getRiesgoAlumnosInbi = (idCiclo) => {
   return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DISTINCT CONCAT(u.nombre," ",u.apellido_paterno," ",IFNULL(u.apellido_materno,"")) as nombre, c.id_alumno, cu.valor_asistencia, da.telefono, da.celular,g.id_curso,g.id_plantel,
    cu.valor_ejercicios, cu.valor_examenes, c.id_grupo, g.nombre as 'grupo',c.calificacion, cu.total_dias_laborales, e.id as idevaluacion, u.usuario AS matricula,
    (SELECT fecha_inicio FROM ciclos WHERE id = ?) as inicio, NOW() as hoy,
            DATEDIFF(NOW(),(SELECT fecha_inicio FROM ciclos WHERE id = ?)) as diferencia,
    cu.num_examenes, cu.num_ejercicios, e.tipo_evaluacion, e.idCategoriaEvaluacion,
    cc.calificacion_final_primera_oportunidad, cc.calificacion_final_segunda_oportunidad
    FROM control_grupos_calificaciones_evaluaciones c
    LEFT JOIN grupos g ON g.id = c.id_grupo
    LEFT JOIN cursos cu ON cu.id = g.id_curso
    LEFT JOIN evaluaciones e ON e.id = id_evaluacion
    LEFT JOIN datos_alumno da ON da.id_usuario = c.id_alumno
    LEFT JOIN usuarios u ON u.id = c.id_alumno
    LEFT JOIN cardex_curso cc ON cc.id_alumno = u.id AND cc.id_grupo = g.id
    WHERE g.id_ciclo = ? AND g.nombre NOT LIKE '%certi%' AND g.nombre NOT LIKE '%indu%' AND c.deleted = 0 ORDER BY nombre;`,[idCiclo,idCiclo,idCiclo],(err, res) => {
      if (err) {
        reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  })
};


riesgo.getAlumnosINBI = ( ciclo ) => {
   return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT CONCAT(u.nombre," ",u.apellido_paterno," ",IFNULL(u.apellido_materno,"")) as nombre, u.id AS id_alumno, g.nombre as 'grupo', g.id_curso, g.id AS id_grupo,
    cu.valor_asistencia, cu.valor_ejercicios, cu.valor_examenes, cu.total_dias_laborales, cu.num_examenes, cu.num_ejercicios FROM grupo_alumnos ga 
    LEFT JOIN grupos g ON g.id = ga.id_grupo 
    LEFT JOIN usuarios u ON u.id = ga.id_alumno
    LEFT JOIN cursos cu ON cu.id = g.id_curso
    WHERE g.id_ciclo = ${ciclo} AND g.nombre NOT LIKE '%certi%' AND g.nombre NOT LIKE '%induc%'`,(err, res) => {
      if (err) {
        reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  })
};


riesgo.asistenciasInbi = (idCiclo) => {
   return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT a.* FROM asistencia_grupo a 
      LEFT JOIN grupos g ON g.id = a.id_grupo
      WHERE g.id_ciclo = ? ;`,[idCiclo],(err, res) => {
      if (err) {
        reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  })
};

riesgo.getRiesgoAlumnoInbiRegistro = (result) => {
  sqlERP.query(`SELECT CONCAT(u.nombre," ",u.apellido_paterno," ",IFNULL(u.apellido_materno,"")) as nombre, g.nombre AS grupo, u.id, g.id as id_grupo FROM usuarios u 
    LEFT JOIN datos_alumno d ON d.id_usuario = u.id
    LEFT JOIN grupo_alumnos ga ON ga.id_alumno =  u.id
    LEFT JOIN grupos g ON g.id = ga.id_grupo
    WHERE u.id_tipo_usuario = 1 AND g.nombre IS NOT NULL AND d.id_usuario IS NULL AND g.id_ciclo = 16;`,(err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  })
};

riesgo.getAsistenciaAlumnoGrupo = (data,result) => {
  sqlFAST.query(`SELECT id_usuario,valor_asistencia, COUNT(*) as total FROM asistencia_grupo WHERE id_usuario = ? AND id_grupo = ? 
    GROUP BY valor_asistencia;`,[data.id_alumno, data.id_grupo],(err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  })
};


riesgo.existeCalificacionKardex = ( escuela, id_alumno, id_grupo ) => {
  return new Promise(( resolve, reject ) => {
    const bd = escuela == 1 ? sqlINBI : sqlFAST
    bd.query(`SELECT * FROM cardex_curso WHERE id_alumno IN(?) AND id_grupo IN(?);;`,[ id_alumno, id_grupo ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};

riesgo.updateCalifKardex = ( escuela, total, extra, id ) => {
  return new Promise(( resolve, reject ) => {
    const bd = escuela == 1 ? sqlINBI : sqlFAST
    bd.query(`UPDATE cardex_curso SET calificacion_final_primera_oportunidad = ?, calificacion_final_segunda_oportunidad = ? WHERE id = ?`,[ total, extra, id ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};

riesgo.addCalifKardex = ( escuela, id_curso, id_grupo, id_nivel, id_alumno, total, extra ) => {
  return new Promise(( resolve, reject ) => {
    const bd = escuela == 1 ? sqlINBI : sqlFAST

    bd.query(`INSERT INTO cardex_curso(id_alumno,id_curso,id_grupo,calificacion_final_primera_oportunidad,calificacion_final_segunda_oportunidad,nivel)VALUES(?,?,?,?,?,?)`,
      [  id_alumno, id_curso, id_grupo, total, extra, id_nivel ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};


/**********************************/

riesgo.getAlumnosPorCiclo = ( ciclo, escuela ) => {
  return new Promise(( resolve, reject ) => {
    const bd = escuela == 1 ? sqlINBI : sqlFAST
    bd.query(`SELECT u.iderp, u.usuario AS matricula, CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL( u.apellido_materno, "")) AS nombre, g.id AS id_grupo, u.id AS id_alumno,
      IFNULL(DATE_FORMAT(u.ultimo_movimiento, "%d - %M - %Y"), 'SIN DATO') AS ultimo_movimiento, g.id_curso, ${escuela} AS escuela, cu.nombre AS curso, g.id_plantel,
      IF(u.ultimo_movimiento,IF(TIMESTAMPDIFF(DAY, CURRENT_DATE, DATE(u.ultimo_movimiento)) < -3, 1, 0),1) AS seguimiento_acceso_plataforma, p.nombre AS plantel, g.nombre AS grupo,
      g.deleted, u.whatsapp FROM grupo_alumnos ga
      LEFT JOIN grupos g ON g.id = ga.id_grupo
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN usuarios u ON u.id = ga.id_alumno
      LEFT JOIN cursos cu ON cu.id = g.id_curso
      LEFT JOIN planteles p ON p.id = g.id_plantel
      WHERE c.iderp = ${ ciclo } AND u.iderp > 0 AND g.nombre NOT LIKE '%CERTI%';`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};

riesgo.alumnosERPVIEJO = ( cicloFast, cicloInbi ) => {
  return new Promise(( resolve, reject ) => {
    sqlERP.query(`SELECT u.matricula, CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL( u.apellido_materno, "")) AS nombre, u.telefono AS whatsapp,
      g.id_grupo, u.id_alumno, g.id_plantel, p.plantel, a.adeudo, g.grupo, u.telefono
      FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN alumnos u ON u.id_alumno = ga.id_alumno
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos_grupos_especiales_carga a ON a.id_alumno = ga.id_alumno AND a.id_grupo = ga.id_grupo
      WHERE g.id_ciclo IN(?,?);`,[cicloFast, cicloInbi],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};


riesgo.getFechasCiclo = ( ciclo, escuela ) => {
  return new Promise(( resolve, reject ) => {
    const bd = escuela == 1 ? sqlINBI : sqlFAST
    bd.query(`SELECT
      ELT( DAYOFWEEK(CURRENT_DATE), 
      DATE_SUB(CURRENT_DATE, Interval 6 DAY), 
        DATE_SUB(CURRENT_DATE, Interval 0 DAY) , 
        DATE_SUB(CURRENT_DATE, Interval 1 DAY), 
        DATE_SUB(CURRENT_DATE, Interval 2 DAY), 
        DATE_SUB(CURRENT_DATE, Interval 3 DAY), 
        DATE_SUB(CURRENT_DATE, Interval 4 DAY), 
        DATE_SUB(CURRENT_DATE, Interval 5 DAY) ) as fecha_inicial,
        CURRENT_DATE AS fecha_final,
        ELT( DAYOFWEEK(CURRENT_DATE), 
        DATE_ADD(CURRENT_DATE, Interval 0 DAY), 
        DATE_ADD(CURRENT_DATE, Interval 6 DAY) , 
        DATE_ADD(CURRENT_DATE, Interval 5 DAY), 
        DATE_ADD(CURRENT_DATE, Interval 4 DAY), 
        DATE_ADD(CURRENT_DATE, Interval 3 DAY), 
        DATE_ADD(CURRENT_DATE, Interval 2 DAY), 
        DATE_ADD(CURRENT_DATE, Interval 1 DAY) ) as fecha_final_semana;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res[0] )
    })
  })
};


riesgo.getParticipacionCiclo = ( idCiclo, escuela ) => {
  return new Promise(( resolve, reject ) => {
    const bd = escuela == 1 ? sqlINBI : sqlFAST
    bd.query(`SELECT a.id_usuario AS id_alumno, a.id_grupo, a.fecha_asistencia, a.valor_asistencia, g.id_curso, cu.nombre AS curso FROM asistencia_grupo a
      LEFT JOIN grupos g ON g.id = a.id_grupo
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN cursos cu ON cu.id = g.id_curso
      WHERE c.iderp = ?
      ORDER BY fecha_asistencia;`,[ idCiclo ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};

riesgo.getEjerciciosCant = ( idCiclo, escuela ) => {
  return new Promise(( resolve, reject ) => {
    const bd = escuela == 1 ? sqlINBI : sqlFAST
    bd.query(`SELECT COUNT(*) AS cant_actividades, id_alumno, id_grupo,
      (SELECT COUNT(*) FROM grupo_fechas f WHERE f.idgrupo = cal.id_grupo AND f.fecha <= CURRENT_DATE GROUP BY f.idgrupo) AS cantidad_actual
      FROM control_grupos_calificaciones_evaluaciones cal
      LEFT JOIN grupos g ON g.id = cal.id_grupo
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN cursos cu ON cu.id = g.id_curso
      WHERE c.iderp = ?
      GROUP BY id_alumno, id_grupo;`,[ idCiclo ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};


riesgo.mensajesWhaAlumnos = ( whats ) => {
  return new Promise(( resolve, reject ) => {
    sqlERPNUEVO.query(`SELECT tos, MAX(fecha_creacion) AS max_fecha, TIMESTAMPDIFF(DAY, DATE(MAX(fecha_creacion)), CURRENT_DATE) AS diferencia_dias FROM mensajes_whatsapp 
      WHERE tos NOT LIKE '%@g.us%'
      GROUP BY tos;`, [ whats ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};

module.exports = riesgo;