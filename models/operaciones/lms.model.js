const { result } = require("lodash");
const sqlERP  = require("../db3.js");
const sqlINBI = require("../dbINBI.js");
const sqlFAST = require("../dbFAST.js");


//const constructor
const lms = function(depas) {

};


lms.getAlumnosLMSFAST = ( ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.nombre AS grupo, CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS alumno FROM grupo_alumnos ga
		LEFT JOIN grupos g ON g.id = ga.id_grupo
		LEFT JOIN ciclos c ON c.id = g.id_ciclo
		LEFT JOIN usuarios u ON u.id = ga.id_alumno
		WHERE c.iderp = ?`,[ ciclo ], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};


lms.getAlumnosLMSINBI = ( ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.nombre AS grupo, CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS alumno FROM grupo_alumnos ga
		LEFT JOIN grupos g ON g.id = ga.id_grupo
		LEFT JOIN ciclos c ON c.id = g.id_ciclo
		LEFT JOIN usuarios u ON u.id = ga.id_alumno
		WHERE c.iderp = ?`,[ ciclo ], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};


module.exports = lms;

