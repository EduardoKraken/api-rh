const { result } = require("lodash");
// const sqlERP  = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");


//const constructor
const corte = function(depas) {
};

corte.getCorteSucursal = ( fecha_corte, id_plantel ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT DISTINCT i.id_ingreso, i.id_alumno, i.id_grupo, i.aut_clave_rastreo, i.folio_operacion, i.cuenta, i.aceptado, i.id_forma_pago, i.monto_pagado + (IFNULL(sal.saldoFavor,0)) AS monto_pagado, i.fecha_pago, i.fecha_ultimo_cambio, i.id_usuario_ultimo_cambio, 
      c.ciclo, p.plantel, p.id_plantel, u.nombre_completo, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, i.url_foto, f.forma_pago, sal.saldoFavor, c.id_ciclo FROM ingresos i 
      LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN usuarios u ON u.id_usuario = i.id_usuario_ultimo_cambio
      LEFT JOIN alumnos a ON a.id_alumno = i.id_alumno
      LEFT JOIN formaspago f ON f.id_forma_pago = i.id_forma_pago
      LEFT JOIN (SELECT IFNULL( SUM(IF(tipo = 1, monto, monto * -1)),0) AS saldoFavor, id_alumno, id_grupo FROM saldofavoralumnos ss1
      GROUP BY id_alumno) sal ON sal.id_alumno = i.id_alumno
			WHERE i.fecha_pago  BETWEEN CONCAT(DATE_SUB( ? , INTERVAL 1 DAY), ' 18:29:59') AND CONCAT( ?, ' 18:30:00')  
			${ id_plantel ? 'AND i.id_plantel_ingreso = ? ' : '' } 
      AND c.ciclo NOT LIKE '%CAPAC%'
			AND i.activo_sn = 1
      ORDER BY a.nombre, p.plantel, c.ciclo, f.forma_pago;`,[ fecha_corte, fecha_corte, id_plantel ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};


corte.getMovimientosCorteFecha = ( fecha_inicio, fecha_final ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT DISTINCT i.id_ingreso, i.id_alumno, i.id_grupo, i.aut_clave_rastreo, i.folio_operacion, i.cuenta, i.aceptado, i.id_forma_pago, i.monto_pagado + (IFNULL(sal.saldoFavor,0)) AS monto_pagado, i.fecha_pago, i.fecha_ultimo_cambio, i.id_usuario_ultimo_cambio, 
      c.ciclo, p.plantel, p.id_plantel, u.nombre_completo, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, i.url_foto, f.forma_pago, sal.saldoFavor, c.id_ciclo FROM ingresos i 
      LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN usuarios u ON u.id_usuario = i.id_usuario_ultimo_cambio
      LEFT JOIN alumnos a ON a.id_alumno = i.id_alumno
      LEFT JOIN formaspago f ON f.id_forma_pago = i.id_forma_pago
      LEFT JOIN (SELECT IFNULL( SUM(IF(tipo = 1, monto, monto * -1)),0) AS saldoFavor, id_alumno, id_grupo FROM saldofavoralumnos ss1
      GROUP BY id_alumno) sal ON sal.id_alumno = i.id_alumno
      WHERE DATE(i.fecha_pago)  BETWEEN ? AND ?
      AND c.ciclo NOT LIKE '%CAPAC%'
      AND i.activo_sn = 1
      ORDER BY a.nombre, p.plantel, c.ciclo, f.forma_pago;`,[ fecha_inicio, fecha_final ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

corte.getMovimientosCorteCiclo = ( id_ciclo ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT DISTINCT i.id_ingreso, i.id_alumno, i.id_grupo, i.aut_clave_rastreo, i.folio_operacion, i.cuenta, i.aceptado, i.id_forma_pago, i.monto_pagado + (IFNULL(sal.saldoFavor,0)) AS monto_pagado, i.fecha_pago, i.fecha_ultimo_cambio, i.id_usuario_ultimo_cambio, 
      c.ciclo, p.plantel, p.id_plantel, u.nombre_completo, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, i.url_foto, f.forma_pago, sal.saldoFavor, c.id_ciclo FROM ingresos i 
      LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN usuarios u ON u.id_usuario = i.id_usuario_ultimo_cambio
      LEFT JOIN alumnos a ON a.id_alumno = i.id_alumno
      LEFT JOIN formaspago f ON f.id_forma_pago = i.id_forma_pago
      LEFT JOIN (SELECT IFNULL( SUM(IF(tipo = 1, monto, monto * -1)),0) AS saldoFavor, id_alumno, id_grupo FROM saldofavoralumnos ss1
      GROUP BY id_alumno) sal ON sal.id_alumno = i.id_alumno
      WHERE c.id_ciclo = ?
      AND i.activo_sn = 1
      ORDER BY a.nombre, p.plantel, c.ciclo, f.forma_pago;`,[ id_ciclo ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

corte.getMovimientosCorteCicloFecha = ( fecha_inicio, fecha_final, id_ciclo ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT DISTINCT i.id_ingreso, i.id_alumno, i.id_grupo, i.aut_clave_rastreo, i.folio_operacion, i.cuenta, i.aceptado, i.id_forma_pago, i.monto_pagado + (IFNULL(sal.saldoFavor,0)) AS monto_pagado, i.fecha_pago, i.fecha_ultimo_cambio, i.id_usuario_ultimo_cambio, 
      c.ciclo, p.plantel, p.id_plantel, u.nombre_completo, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, i.url_foto, f.forma_pago, sal.saldoFavor, c.id_ciclo FROM ingresos i 
      LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN usuarios u ON u.id_usuario = i.id_usuario_ultimo_cambio
      LEFT JOIN alumnos a ON a.id_alumno = i.id_alumno
      LEFT JOIN formaspago f ON f.id_forma_pago = i.id_forma_pago
      LEFT JOIN (SELECT IFNULL( SUM(IF(tipo = 1, monto, monto * -1)),0) AS saldoFavor, id_alumno, id_grupo FROM saldofavoralumnos ss1
      GROUP BY id_alumno) sal ON sal.id_alumno = i.id_alumno
      WHERE DATE(i.fecha_pago)  BETWEEN ? AND ?
      AND c.id_ciclo = ?
      AND i.activo_sn = 1
      ORDER BY a.nombre, p.plantel, c.ciclo, f.forma_pago;`,[ fecha_inicio, fecha_final, id_ciclo ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

corte.getDiferenciasCorte = ( fecha_corte, id_plantel ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT i.id_ingreso, i.id_alumno, i.id_grupo, i.id_forma_pago, i.monto_pagado, i.fecha_pago, i.id_usuario_ultimo_cambio, 
			c.ciclo, p.plantel, p.id_plantel, u.nombre_completo, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, f.forma_pago FROM ingresos i 
			LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
			LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN planteles p ON p.id_plantel = i.id_plantel_ingreso
			LEFT JOIN usuarios u ON u.id_usuario = i.id_usuario_ultimo_cambio
			LEFT JOIN alumnos a ON a.id_alumno = i.id_alumno
			LEFT JOIN formaspago f ON f.id_forma_pago = i.id_forma_pago
			WHERE i.fecha_pago  BETWEEN CONCAT(DATE_SUB( ? , INTERVAL 1 DAY), ' 18:29:59') AND CONCAT( ?, ' 18:30:00')
			AND g.id_plantel = ?
			AND i.activo_sn = 1
      AND c.ciclo NOT LIKE '%CAPAC%'
      AND i.id_plantel_ingreso <> ?;`,[ fecha_corte, fecha_corte, id_plantel, id_plantel ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

corte.getCifrasActuales = ( fecha_corte, id_plantel ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT sum(i.monto_pagado ) as suma, c.id_ciclo, IFNULL(c.ciclo,"Sin ciclo") AS ciclo, p.plantel, p.id_plantel FROM ingresos i
      LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN (SELECT IFNULL( SUM(monto),0) AS saldoFavor, id_alumno, id_grupo FROM saldofavoralumnos ss1
      WHERE tipo = 1 GROUP BY id_alumno, id_grupo) sal ON sal.id_alumno = i.id_alumno AND sal.id_grupo = i.id_grupo
      WHERE c.activo_sn = 1
      AND i.fecha_pago <= CONCAT( ?, ' 18:30:00')
      ${ id_plantel ? 'AND p.id_plantel = ? ' : '' } 
      AND c.ciclo NOT LIKE '%CAPAC%'
      GROUP BY g.id_ciclo, p.plantel;`,[ fecha_corte, id_plantel ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

corte.getCifrasAyer = ( fecha_corte, id_plantel ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT sum(i.monto_pagado ) as suma, c.id_ciclo, IFNULL(c.ciclo,"Sin ciclo") AS ciclo, p.plantel, p.id_plantel FROM ingresos i
      LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN (SELECT IFNULL( SUM(monto),0) AS saldoFavor, id_alumno, id_grupo FROM saldofavoralumnos ss1
      WHERE tipo = 1 AND fecha_creacion <= CONCAT(DATE_SUB(?, INTERVAL 1 DAY), ' 18:29:59') GROUP BY id_alumno, id_grupo) sal ON sal.id_alumno = i.id_alumno AND sal.id_grupo = i.id_grupo
      WHERE c.activo_sn = 1
      AND i.fecha_pago <= CONCAT(DATE_SUB(?, INTERVAL 1 DAY), ' 18:29:59')
      ${ id_plantel ? 'AND p.id_plantel = ? ' : ' ' }
      AND c.ciclo NOT LIKE '%CAPAC%'
      GROUP BY g.id_ciclo, p.plantel;`,[ fecha_corte, fecha_corte, id_plantel ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

corte.getCambiosCorte = ( fecha_corte ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM egresos WHERE fecha_egreso 
      BETWEEN 
        CONCAT(DATE_SUB( ? , INTERVAL 1 DAY), ' 18:29:59') 
      AND 
        CONCAT( ?, ' 18:30:00');`,[ fecha_corte, fecha_corte ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

corte.getCorteSucursalCambios = ( id_alumnos, id_plantel, idsNuevoGrupo ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT DISTINCT i.id_ingreso, i.id_alumno, i.id_grupo, i.aut_clave_rastreo, i.folio_operacion, i.cuenta, i.aceptado, i.id_forma_pago, i.monto_pagado + (IFNULL(sal.saldoFavor,0)) AS monto_pagado, i.fecha_pago, i.fecha_ultimo_cambio, i.id_usuario_ultimo_cambio, 
      c.ciclo, p.plantel, p.id_plantel, u.nombre_completo, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, i.url_foto, f.forma_pago FROM ingresos i 
      LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN usuarios u ON u.id_usuario = i.id_usuario_ultimo_cambio
      LEFT JOIN alumnos a ON a.id_alumno = i.id_alumno
      LEFT JOIN formaspago f ON f.id_forma_pago = i.id_forma_pago
      LEFT JOIN (SELECT IFNULL( SUM(IF(tipo = 1, monto, monto * -1)),0) AS saldoFavor, id_alumno, id_grupo FROM saldofavoralumnos ss1
      GROUP BY id_alumno) sal ON sal.id_alumno = i.id_alumno
      WHERE i.id_alumno IN ( ? )
      ${ id_plantel ? `AND i.id_plantel_ingreso =  ${ id_plantel }` : '' } 
      AND c.ciclo NOT LIKE '%CAPAC%'
      AND i.id_grupo IN (?)
      AND i.activo_sn = 1
      ORDER BY a.nombre, p.plantel, c.ciclo, f.forma_pago;`,[ id_alumnos, idsNuevoGrupo ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};


corte.getSaldosFavorCiclo = ( fecha_corte ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT a.id_alumno, (SELECT c.id_ciclo FROM gruposalumnos ga 
                                      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
                                      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
                                      WHERE id_alumno = a.id_alumno
                                      ORDER BY c.fecha_inicio_ciclo DESC LIMIT 1) AS id_ciclo,
                                      (SELECT g.id_plantel FROM gruposalumnos ga 
                                      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
                                      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
                                      WHERE id_alumno = a.id_alumno
                                      ORDER BY c.fecha_inicio_ciclo DESC LIMIT 1) AS id_plantel,
      IFNULL(sal.saldoFavor,0) AS saldoFavor
      FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN (SELECT IFNULL( SUM(IF(tipo = 1, monto, monto * -1)),0) AS saldoFavor, id_alumno, id_grupo FROM saldofavoralumnos ss1
      GROUP BY id_alumno) AS sal ON sal.id_alumno = a.id_alumno
      GROUP BY ga.id_alumno;`,[ fecha_corte ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

corte.getSaldosFavorCicloAyer = ( fecha_corte ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT a.id_alumno, (SELECT c.id_ciclo FROM gruposalumnos ga 
                                      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
                                      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
                                      WHERE id_alumno = a.id_alumno
                                      ORDER BY c.fecha_inicio_ciclo DESC LIMIT 1) AS id_ciclo,
                                      (SELECT g.id_plantel FROM gruposalumnos ga 
                                      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
                                      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
                                      WHERE id_alumno = a.id_alumno
                                      ORDER BY c.fecha_inicio_ciclo DESC LIMIT 1) AS id_plantel,
      IFNULL(sal.saldoFavor,0) AS saldoFavor
      FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN (SELECT IFNULL( SUM(IF(tipo = 1, monto, monto * -1)),0) AS saldoFavor, id_alumno, id_grupo FROM saldofavoralumnos ss1
      GROUP BY id_alumno) AS sal ON sal.id_alumno = a.id_alumno
      GROUP BY ga.id_alumno;`,[ fecha_corte ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

corte.saldosFavorUtilizados = ( fecha_corte ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT IFNULL( SUM(IF(tipo = 1, monto, monto * -1)),0) AS saldoFavor, id_alumno, ss1.id_grupo, g.id_plantel  FROM saldofavoralumnos ss1
        LEFT JOIN grupos g ON g.id_grupo = ss1.id_grupo
        WHERE tipo = 2
        GROUP BY id_alumno, id_grupo;`,[ fecha_corte ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};


corte.getGruposCambio = ( idsNuevoGrupo, idsViejoGrupo ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT g.*, p.plantel, c.ciclo 
      FROM grupos g 
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      WHERE g.id_grupo IN (?) OR g.id_grupo IN (?);`,[ idsNuevoGrupo, idsViejoGrupo ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};


corte.getReporteIngresosCiclo = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT p.plantel, CONCAT(a.nombre, ' ',a.apellido_paterno, ' ',IFNULL(a.apellido_materno,'')) AS alumno, g.grupo, i.monto_pagado, f.forma_pago, i.fecha_pago FROM ingresos i
      LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = i.id_alumno
      LEFT JOIN formaspago f ON f.id_forma_pago = i.id_forma_pago
      WHERE g.id_ciclo = ?
      ORDER BY p.plantel, CONCAT(a.nombre, ' ',a.apellido_paterno, ' ',IFNULL(a.apellido_materno,''));`,[ id ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res);
    });
  })
};

corte.updateClaveRastreo = ( id_ingreso, aut_clave_rastreo, folio_operacion, cuenta ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE ingresos SET aut_clave_rastreo = ?, folio_operacion = ?, cuenta = ? WHERE id_ingreso = ?;`,[ aut_clave_rastreo, folio_operacion, cuenta, id_ingreso ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res );
    });
  })
};

corte.updateFolioOperacion = ( id_ingreso, folio_operacion ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE ingresos SET folio_operacion = ? WHERE id_ingreso = ?;`,[ folio_operacion, id_ingreso ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res );
    });
  })
};

corte.updateCuentaIngreso = ( id_ingreso, cuenta ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE ingresos SET cuenta = ? WHERE id_ingreso = ?;`,[ cuenta, id_ingreso ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res );
    });
  })
};

corte.updateAceptado = ( id_ingreso, aceptado ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE ingresos SET aceptado = ? WHERE id_ingreso = ?;`,[ aceptado, id_ingreso ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      return resolve( res );
    });
  })
};

module.exports = corte;

