const { result } = require("lodash");
const sql = require("./db.js");


//const constructor
const Zonas = function(zonas) {
    this.idzona = zonas.idzona;
    this.zona = zonas.zona;
    this.usuario_registro = zonas.usuario_registro;
    this.fecha_creacion = zonas.fecha_creacion;
    this.fecha_actualizo = zonas.fecha_actualizo;
    this.deleted = zonas.deleted;
};

Zonas.all_zonas = result => {
    sql.query(`SELECT * FROM zona WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

Zonas.add_zonas = (z, result) => {
    sql.query(`INSERT INTO zona(zona,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?)`, [z.zona, z.usuario_registro, z.fecha_creacion, z.fecha_actualizo],
        (err, res) => {
            if (err) { result(err, null); return; }

            result(null, { id: res.insertId, ...z });
        }
    )
}

Zonas.update_zona = (id, z, result) => {
    sql.query(` UPDATE zona SET zona=?,usuario_registro=?,fecha_creacion=?,fecha_actualizo=?,deleted=? WHERE idzona=?`, [z.zona, z.usuario_registro, z.fecha_creacion, z.fecha_actualizo, z.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            result(null, { id: id, ...z });
        }
    )
}



module.exports = Zonas;