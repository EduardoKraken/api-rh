const sql = require("./db.js");

//const constructor
const Puestos = function(puestos) {
  this.idpuesto = puestos.idpuesto;
  this.puesto = puestos.puesto;
  this.iddepartamento = departamentos.iddepartamento;
  this.usuario_registro = puestos.usuario_registro;
  this.fecha_creacion = puestos.fecha_creacion;
  this.fecha_actualizo = puestos.fecha_actualizo;
  this.deleted = puestos.deleted;
};

Puestos.all_puestos = result => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT p.*, d.departamento FROM puesto p
      LEFT JOIN organigrama_niveles o ON o.idorganigrama_niveles = p.idorganigrama_niveles
      LEFT JOIN departamento d ON d.iddepartamento = p.iddepartamento
      WHERE p.deleted = 0 ORDER BY p.puesto;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  })
};

Puestos.add_puestos = (p, result) => {
  sql.query(`INSERT INTO puesto(puesto,iddepartamento,usuario_registro,fecha_creacion,fecha_actualizo,idorganigrama_niveles,dependiente,depende_de)VALUES(?,?,?,?,?,?,?,?)`, [p.puesto, p.iddepartamento, p.usuario_registro, p.fecha_creacion, p.fecha_actualizo, p.idorganigrama_niveles, p.dependiente, p.depende_de],
    (err, res) => {
      if (err) { result(err, null); return; }
      result(null, { id: res.insertId, ...p });
    });
};

Puestos.update_puesto = (id, p, result) => {
  sql.query(` UPDATE puesto SET puesto=?,iddepartamento=?,usuario_registro=?,fecha_actualizo=?, deleted  =?,idorganigrama_niveles=?,dependiente=?,depende_de=?
    WHERE idpuesto = ?`, [p.puesto, p.iddepartamento, p.usuario_registro, p.fecha_actualizo, p.deleted, p.idorganigrama_niveles, p.dependiente, p.depende_de, id],
    (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id, ...p });
    }
    );
};


Puestos.getPuestosDepto = (id, result) => {
  sql.query(`SELECT * FROM puesto WHERE deleted = 0 AND iddepartamento = ?`, [id], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};


module.exports = Puestos;