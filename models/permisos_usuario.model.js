const { result } = require("lodash");
const sql = require("./db.js");


//const constructor
const Permisos = function(depas) {};

Permisos.getPermisoUsuario = result => {
  sql.query(`SELECT * FROM permisos_usuario`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Permisos.addPermisoUsuario = (z, result) => {
  sql.query(`INSERT INTO permisos_usuario(idusuario,idpermiso,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?,?)`, [z.idusuario,z.idpermiso, z.usuario_registro, z.fecha_creacion, z.fecha_actualizo,z.deleted],
    (err, res) => {
      if (err) { result(err, null); return; }
      result(null, { id: res.insertId, ...z });
    }
  )
}

Permisos.updatePermisoUsuario = (id, z, result) => {
  sql.query(` UPDATE permisos_usuario SET idusuario=?,idpermiso=?,usuario_registro=?,fecha_creacion=?,fecha_actualizo=?,deleted=? WHERE idpermisos_usuario=?`, [z.idusuario,z.idpermiso, z.usuario_registro, z.fecha_creacion, z.fecha_actualizo, z.deleted, id],
    (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id, ...z });
  })
}



module.exports = Permisos;