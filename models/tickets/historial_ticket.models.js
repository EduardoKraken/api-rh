// const sql = require("../db3.js");
const sql      = require("../dbErpPrueba.js");


// constructor
const Historial_tickets = function (historial_tickets) {
  this.idweb = historial_tickets.idweb;
  this.nomcli = historial_tickets.nomcli;
  this.calle = historial_tickets.calle;
};

Historial_tickets.addHistorial_ticket = (c, result) => {
  return new Promise((resolve, reject) => {
    sql.query(`INSERT INTO historial_ticket(idticket, respuesta, motivo,respuestaauxi,id_suc) VALUES(?,?,?,?,?)`, 
    [c.idticket, c.respuesta, c.motivo,c.respuestaauxi,c.id_suc], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...c });
    });
  });
};

Historial_tickets.getHistorial_tickets = (result) => {
  sql.query(`SELECT * FROM historial_ticket`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Historial_tickets.getHistorial_tickets_usuario = (id, result) => {
  sql.query(`SELECT * FROM historial_ticket where id_usuario = ?`, [id], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Historial_tickets.getHistorialEstatus = (id, result) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT h.*, t.idjefe_area, t.idauxi_area, u.nombre_completo AS jefe, u2.nombre_completo AS auxiliar FROM historial_ticket h
      LEFT JOIN ticket t ON t.idticket = h.idticket
      LEFT JOIN usuarios u ON u.id_usuario = t.idjefe_area
      LEFT JOIN usuarios u2 ON u2.id_usuario = t.idauxi_area
      WHERE h.idticket = ?;`, [id], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    })
  })
};


Historial_tickets.getHistorialTicketRespuesta = (id, result) => {
  sql.query(`SELECT ht.idhistorial_ticket, ht.motivo, ht.respuestaauxi, ht.idticket, ht.respuesta FROM historial_ticket ht, ticket t WHERE ht.idticket = t.idticket AND ht.idticket = ?`, [id], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Historial_tickets.putHistorialTicketRespuesta = (id, respuesta, idticket, result) => {
  sql.query(`UPDATE historial_ticket SET respuesta = ? WHERE idhistorial_ticket = ?`, [respuesta, id], (err, res) => {
    sql.query(`UPDATE ticket SET estatus = 5 WHERE idticket = ?`, [idticket], (err, res) => {
      if (err) {
        result(null, err);
        return;
      }
      result(null, res);
    })
  })


};




Historial_tickets.putHistorial_tickets = (id, sub, result) => {
  sql.query(` UPDATE historial_ticket SET  respuestaauxi=?, respuesta=?, estatus=? WHERE idhistorial_ticket = ?`,
    [sub.respuestaauxi,sub.respuesta,sub.estatus,id],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({
          kind: "not_found"
        }, null);
        return;
      }


      result(null, {
        id: id,
        ...sub
      });
    }
    );
};

module.exports = Historial_tickets;