// const sql         = require("../db3.js");
const sql         = require("../dbErpPrueba.js");
const sqlERPNUEVO = require("../db.js");

// constructor
const tickets = function (usuarios) {
    this.idweb = usuarios.idweb;
    this.nomcli = usuarios.nomcli;
    this.calle = usuarios.calle;
};

tickets.addTicket = ( c ) => {
  return new Promise((resolve, reject) => {
    sql.query(`INSERT INTO ticket(id_usuario,id_unidad_negocio, motivo,estatus, tipousuario,id_suc) VALUES(?,?,?,?,?,?)`,[c.id_usuario, c.id_unidad_negocio, c.motivo, c.estatus, c.tipousuario,c.id_suc], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...c });
    });
  });
};

tickets.addFotoEvidenciaTicket = ( foto, id ) => {
  return new Promise((resolve, reject) => {
    sql.query(`INSERT INTO evidencia_historial_tickets( foto, idhistorial_ticket) VALUES( ?, ? )`,[ foto, id ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, foto, id });
    });
  });
};


tickets.getTickets = ( ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT t.*, IFNULL(p.plantel,"sin plantel") plantel, j.nombre_completo AS nombre_jefe, au.nombre_completo AS nombre_auxiliar,
      IF(t.tipousuario = 0, u.nombre_completo,(SELECT CONCAT(al.nombre," ",al.apellido_paterno," ", al.apellido_materno) FROM alumnos al WHERE al.id_alumno = t.id_usuario)) AS nombre_completo FROM ticket t 
      LEFT JOIN planteles p ON t.id_suc = p.id_plantel
      LEFT JOIN usuarios j ON j.id_usuario = t.idjefe_area
      LEFT JOIN usuarios au ON au.id_usuario = t.idauxi_area
      LEFT JOIN usuarios u ON t.id_usuario = u.id_usuario
      WHERE DATE_SUB(CURRENT_DATE,INTERVAL 6 MONTH) <= DATE(fecha_apertura);`, 
       (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve( res );
    })
  })
};

tickets.getEvidenciaTicket = ( id ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT e.*, t.idticket FROM evidencia_historial_tickets e
      LEFT JOIN historial_ticket h ON h.idhistorial_ticket = e.idhistorial_ticket
      LEFT JOIN ticket t ON t.idticket = h.idticket
      WHERE t.idticket IN (?);`,[ id ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve( res );
    })
  })
};


tickets.getTicketsRH = ( ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT t.*, IFNULL(p.plantel,"sin plantel") plantel, j.nombre_completo AS nombre_jefe, au.nombre_completo AS nombre_auxiliar,
      IF(t.tipousuario = 0, u.nombre_completo,(SELECT CONCAT(al.nombre," ",al.apellido_paterno," ", al.apellido_materno) FROM alumnos al WHERE al.id_alumno = t.id_usuario)) AS nombre_completo FROM ticket t 
      LEFT JOIN planteles p ON t.id_suc = p.id_plantel
      LEFT JOIN usuarios j ON j.id_usuario = t.idjefe_area
      LEFT JOIN usuarios au ON au.id_usuario = t.idauxi_area
      LEFT JOIN usuarios u ON t.id_usuario = u.id_usuario
      WHERE t.visorrh = 1;`, 
       (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve( res );
    })
  })
};


tickets.getAreas = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM departamento;`, 
       (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve( res );
    })
  })
};

tickets.getJefesArea = ( ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT j.*, u.nombre_completo FROM jefe_area j
     LEFT JOIN usuarios u ON u.id_usuario = j.id_usuario WHERE j.activo = 1;`, 
       (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve( res );
    })
  })
};

tickets.getPuestosUsuarios = ( ids ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM usuarios;`, [ ids ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve( res );
    })
  })
};

tickets.getAuxiArea = ( ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM auxi_area WHERE activo = 1;`, 
       (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve( res );
    })
  })
};


tickets.getTickets_area = (id, result) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT t.*, IFNULL(p.plantel,"sin plantel") plantel, j.nombre_completo AS nombre_jefe, au.nombre_completo AS nombre_auxiliar,
      IF(t.tipousuario = 0, u.nombre_completo,(SELECT CONCAT(al.nombre," ",al.apellido_paterno," ", al.apellido_materno) FROM alumnos al WHERE al.id_alumno = t.id_usuario)) AS nombre_completo FROM ticket t 
      LEFT JOIN planteles p ON t.id_suc = p.id_plantel
      LEFT JOIN usuarios j ON j.id_usuario = t.idjefe_area
      LEFT JOIN usuarios au ON au.id_usuario = t.idauxi_area
      LEFT JOIN usuarios u ON t.id_usuario = u.id_usuario
      WHERE t.idjefe_area = ? AND t.count_rechazos < 5;`, [id], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve( res );
    })
  })
};

tickets.getTickets_auxiliar_area = (id, result) => {
    sql.query(`
      SELECT t.*, IFNULL(p.plantel,"sin plantel") plantel, j.nombre_completo AS nombre_jefe, au.nombre_completo AS nombre_auxiliar,
    IF(t.tipousuario = 0, u.nombre_completo,(SELECT CONCAT(al.nombre," ",al.apellido_paterno," ", al.apellido_materno) FROM alumnos al WHERE al.id_alumno = t.id_usuario)) AS nombre_completo FROM ticket t 
    LEFT JOIN planteles p ON t.id_suc = p.id_plantel
    LEFT JOIN usuarios j ON j.id_usuario = t.idjefe_area
    LEFT JOIN usuarios au ON au.id_usuario = t.idauxi_area
    LEFT JOIN usuarios u ON t.id_usuario = u.id_usuario
    WHERE t.idauxi_area = ? AND t.count_rechazos < 3;`, [id], (err, res) => {
      if (err) {
        result(null, err);
        return;
      }
      result(null, res);
    })
};

tickets.putTickets = (id, sub, result) => {
    sql.query(` UPDATE ticket SET folio=?, id_unidad_negocio=?, idjefe_area=?, idauxi_area=?, fecha_apertura=?, fecha_cierre=?, id_area=?, estatus=?, motivo=?, count_rechazos=? WHERE idticket = ?`,
        [sub.folio, sub.id_unidad_negocio, sub.idjefe_area, sub.idauxi_area, sub.fecha_apertura, sub.fecha_cierre, sub.id_area, sub.estatus, sub.motivo, sub.count_rechazos, id],
        (err, res) => {
            if (err) {
                
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                result({
                    kind: "not_found"
                }, null);
                return;
            }

            result(null, {
                id: id,
                ...sub
            });
        }
    );
};

tickets.putTickets_folio = (id, sub, result) => {
    sql.query(` UPDATE ticket SET folio=? WHERE idticket = ?`,
        [sub.folio, id],
        (err, res) => {
            if (err) {
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                result({
                    kind: "not_found"
                }, null);
                return;
            }

            result(null, {
                id: id,
                ...sub
            });
        }
    );
};

tickets.putTicketAsignacion = (id, sub, result) => {
    var date = new Date();
    sql.query(` UPDATE ticket SET id_area=?, idjefe_area =?, estatus= ?, fecha_asignado=? WHERE idticket = ?`,
        [sub.id_area, sub.idjefe_area, sub.estatus, date,id],
        (err, res) => {
            if (err) {
                
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                result({
                    kind: "not_found"
                }, null);
                return;
            }

            result(null, {
                id: id,
                ...sub
            });
        }
    );
};

tickets.putTicketAsignacionAuxi = (id, sub, result) => {
    sql.query(` UPDATE ticket SET idauxi_area =?, estatus= ? WHERE idticket = ?`,
        [sub.idauxi_area, sub.estatus, id],
        (err, res) => {
            if (err) {
                
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                result({
                    kind: "not_found"
                }, null);
                return;
            }

            result(null, {
                id: id,
                ...sub
            });
        }
    );
};

tickets.putTicketEstatus = (id, sub, result) => {
  if(sub.estatus == 7){
    sql.query(` UPDATE ticket SET estatus= ?, count_rechazos = count_rechazos + 1 WHERE idticket = ?`,
      [sub.estatus, id],
      (err, res) => {
        if (err) {
          
          result(null, err);
          return;
        }

        if (res.affectedRows == 0) {
          result({
            kind: "not_found"
          }, null);
          return;
        }

        result(null, {
          id: id,
          ...sub
        });
      }
    );
  }else if(sub.estatus == 6){
    var date = new Date();
    sql.query(` UPDATE ticket SET estatus= ?, fecha_cierre=? WHERE idticket = ?`,
        [sub.estatus, date, id],
        (err, res) => {
            if (err) {
                
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                result({
                    kind: "not_found"
                }, null);
                return;
            }

            result(null, {
                id: id,
                ...sub
            });
        }
    );
  }else{
    sql.query(` UPDATE ticket SET estatus= ? WHERE idticket = ?`,
        [sub.estatus, id],
        (err, res) => {
            if (err) {
                
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                result({
                    kind: "not_found"
                }, null);
                return;
            }

            result(null, {
                id: id,
                ...sub
            });
        }
    );
  }
};

tickets.getTicketsUsuarioLMS = (id, result) => {
    sql.query(`SELECT t.idticket, t.estatus, t.fecha_apertura, t.motivo FROM ticket t WHERE id_usuario = ? AND tipousuario = 1;`, [id], (err, res) => {
        if (err) {
            
            result(null, err);
            return;
        }
        result(null, res);
    })
};

tickets.getTicketsUsuarioERP = (id, result) => {
    sql.query(`SELECT t.idticket, t.estatus, t.fecha_apertura, t.motivo, DATE_FORMAT(t.fecha_apertura, '%W %d %M %Y') fecha_formato, 
      DATE(t.fecha_apertura) AS fecha_corta, u.id_usuario, u.nombre_completo 
      FROM ticket t 
      LEFT JOIN usuarios u ON u.id_usuario = t.idjefe_area
      WHERE t.id_usuario = ? AND t.tipousuario = 0 ORDER BY t.fecha_apertura DESC;`, [id], (err, res) => {
        if (err) {
            
            result(null, err);
            return;
        }
        result(null, res);
    })
};

tickets.getUsuariosALL = (id, result) => {

    sql.query(`SELECT id_usuario, nombre_completo, usuario, password, id_plantel, email, telefono, id_trabajador, tipo_usuario FROM usuarios WHERE id_usuario = ?`, [id], (err, res) => {
        if (err) {
            
            result(null, err);
            return;
        }
        result(null, res);
    })
};

tickets.getTicketsPorID = (id, result) => {
    sql.query(`SELECT * FROM ticket WHERE idticket = ?`, [id], (err, res) => {
        if (err) {
            
            result(null, err);
            return;
        }
        result(null, res);
    })
};

tickets.getCargoUsuario = (id, result) => {

  sql.query(`SELECT j.*, a.* FROM jefe_area j
    LEFT JOIN areas_ticket a ON a.idareas_ticket = j.idareas_ticket
    WHERE j.id_usuario = ? AND j.activo = 1`, [id], (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }

    if (res == "") {
      sql.query(`SELECT au.*, a.* FROM auxi_area au
    LEFT JOIN areas_ticket a ON a.idareas_ticket = au.idareas_ticket
    WHERE au.id_usuario = ?`, [id], (err, res) => {
        if (err) {
          
          result(null, res);
          return;
        }

        if (res == "") {
          
          result(null, {
              cargo: "Sin cargo, no permitir el acceso"
          });

        } else {
          
          result(null, {
            cargo: "Auxiliar",
            permiso: res[0]
          });
        }
      })
    } else {
      result(null, {
        cargo: "Jefe",
        permiso: res[0]
      });
    }
  })
};

tickets.getNotas = (id, result) => {
  sql.query(`SELECT nt.*, u.nombre_completo FROM notas_ticket nt
    LEFT JOIN usuarios u ON u.id_usuario = nt.id_usuario WHERE nt.idticket = ?;`, [id], (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
    result(null,res);
  })
};

tickets.addNota = (c, result) => {
    var date = new Date();
    sql.query(`INSERT INTO notas_ticket(idticket,id_usuario,nota) VALUES(?,?,?)`,
     [c.idticket, c.id_usuario, c.nota], (err, res) => {
        if (err) {
            result(err, null);
            return;
        }
        result(null, {
            id: res.insertId,
            ...c
        });
    });
};

tickets.updateVisorRh = ( visorrh, idticket ) => {
  return new Promise((resolve,reject) => {
    sql.query(` UPDATE ticket SET visorrh=? WHERE idticket = ?`,[ visorrh, idticket ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }

      if (res.affectedRows == 0) {
        return reject({ message: 'No se encontró el ticket a actualizar' })
      }

      resolve({ visorrh, idticket });
    });
  })
};


module.exports = tickets;