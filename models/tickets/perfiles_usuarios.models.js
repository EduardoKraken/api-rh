// const sql = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");


// constructor
const Perfiles_usuarios = function(perfiles_usuarios) {
  this.idweb     = perfiles_usuarios.idweb;
  this.nomcli    = perfiles_usuarios.nomcli;
  this.calle     = perfiles_usuarios.calle;
};

Perfiles_usuarios.addPerfil_usuario = (c, result) => {
	sqlERP.query(`INSERT INTO perfilesusuario(id_usuario, id_perfil, fecha_alta, activo_sn, fecha_baja, id_usuario_ultimo_cambio, fecha_ultimo_cambio) VALUES(?,?,?,?,?,?,?,?)`,[c.id_usuario, c.id_perfil, c.fecha_alta, c.activo_sn, c.fecha_baja, c.id_usuario_ultimo_cambio, c.fecha_ultimo_cambio],(err, res) => {	
    if (err) {
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...c });
	});
};

Perfiles_usuarios.getPerfiles_usuarios = (result)=>{
	sqlERP.query(`SELECT * FROM perfilesusuario`,(err,res)=>{
		if (err) {
      result(null, err);
      return;
    }

    result(null, res);
	})
};

Perfiles_usuarios.putPerfiles_usuarios = (id, sub, result) => {
  sqlERP.query(` UPDATE perfilesusuario SET id_usuario=?, id_perfil=?, fecha_alta=?, activo_sn=?, fecha_baja=?, id_usuario_ultimo_cambio=?, fecha_ultimo_cambio=? WHERE id_perfilusuario = ?`,
   [sub.id_usuario, sub.id_perfil, sub.fecha_alta, sub.activo_sn, sub.fecha_baja, sub.id_usuario_ultimo_cambio, sub.fecha_ultimo_cambio,id],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...sub });
    }
  );
};

module.exports = Perfiles_usuarios;