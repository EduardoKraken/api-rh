const { result } = require("lodash");
const sql = require("../../db3.js");

//const constructor
const AppRIMontos = function(calificaciones) {};

// Obtener todos los planteles
AppRIMontos.getPlanteles = result => {
  sql.query(`SELECT * FROM planteles  WHERE activo_sn = 1`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

// Obtener todos los ciclos
AppRIMontos.getCiclos = result => {
  sql.query(`SELECT * FROM ciclos  WHERE activo_sn = 1`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};



AppRIMontos.getCantActual = (id,idfast,result) => {
  sql.query(`SELECT  a.*,p.plantel FROM alumnos_grupos_especiales_carga a 
LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
WHERE a.id_ciclo = ?  AND a.adeudo = 0 OR a.id_ciclo = ?  AND a.adeudo = 0;`,[id,idfast], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

// Obtener todos los ciclos
AppRIMontos.getCantSiguiente = (id,idfast,result) => {
  sql.query(`SELECT  a.*,p.plantel FROM alumnos_grupos_especiales_carga a 
LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
WHERE a.id_ciclo = ?  AND a.adeudo = 0 OR a.id_ciclo = ?  AND a.adeudo = 0;`,[id,idfast], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};



module.exports = AppRIMontos;