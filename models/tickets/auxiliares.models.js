const sql = require("../dbErpPrueba.js");
// constructor
const Auxiliares = function (Auxiliares) {
    this.idweb = Auxiliares.idweb;
    this.nomcli = Auxiliares.nomcli;
    this.calle = Auxiliares.calle;
};

Auxiliares.addAuxiliar = (c, result) => {
    sql.query(`INSERT INTO auxi_area(idareas_ticket, id_usuario) VALUES(?,?)`, [c.idareas_ticket, c.id_usuario], (err, res) => {
        if (err) {
            result(err, null);
            return;
        }
        result(null, {
            id: res.insertId,
            ...c
        });
    });
};

Auxiliares.getAuxiliares = (result) => {
    sql.query(`SELECT aa.idauxi_area, aa.idareas_ticket, a_t.area, u.nombre_completo, u.id_usuario, aa.activo FROM auxi_area aa, areas_ticket a_t, usuarios u 
        WHERE aa.idareas_ticket=a_t.idareas_ticket AND aa.id_usuario=u.id_usuario ORDER BY aa.activo DESC;`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    })
};

Auxiliares.getAuxiliares_PorArea = (id, result) => {
    sql.query(`SELECT aa.idauxi_area, aa.idareas_ticket,aa.id_usuario, a.area, u.nombre_completo FROM auxi_area aa
    LEFT JOIN areas_ticket a ON a.idareas_ticket = aa.idareas_ticket
    LEFT JOIN usuarios u ON u.id_usuario = aa.id_usuario WHERE aa.idareas_ticket = ? AND aa.activo = 1;`,[id], (err, res) => {

        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    })
};

Auxiliares.putAuxiliares = (id, sub, result) => {
    sql.query(` UPDATE auxi_area SET idareas_ticket=?, id_usuario=? WHERE idauxi_area = ?`,
        [sub.idareas_ticket, sub.id_usuario, id],
        (err, res) => {
            if (err) {
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                result({
                    kind: "not_found"
                }, null);
                return;
            }

            result(null, {
                id: id,
                ...sub
            });
        }
    );
};

module.exports = Auxiliares;