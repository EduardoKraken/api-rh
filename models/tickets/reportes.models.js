const sql = require("../db3.js");

// constructor
const Reportes = function (reportes) {
    this.idweb = reportes.idweb;
    this.nomcli = reportes.nomcli;
    this.calle = reportes.calle;
};

Reportes.getTotalTickets = (result) => {
    sql.query(`SELECT COUNT(*) AS total_tickets, fecha_apertura FROM ticket GROUP BY fecha_apertura`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

Reportes.getTotalTicketsINBI = (result) => {
    sql.query(`SELECT COUNT(*) AS total_tickets_INBI FROM ticket WHERE id_unidad_negocio = 1`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

Reportes.getTotalTicketsFASTENGLISH = (result) => {
    sql.query(`SELECT COUNT(*) AS total_tickets_FastEnglish FROM ticket WHERE id_unidad_negocio = 2`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

Reportes.getEstatus1 = (result) => {
    sql.query(`SELECT COUNT(*) AS cantidad_tickets, id_area FROM ticket WHERE estatus = 1`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

Reportes.getEstatus2 = (result) => {
    sql.query(`SELECT COUNT(*) AS cantidad_tickets, id_area FROM ticket WHERE estatus = 2`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

Reportes.getEstatus3 = (result) => {
    sql.query(`SELECT COUNT(*) AS cantidad_tickets, id_area FROM ticket WHERE estatus = 3`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

Reportes.getEstatus4 = (result) => {
    sql.query(`SELECT COUNT(*) AS cantidad_tickets, id_area FROM ticket WHERE estatus = 4`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

Reportes.getEstatus5 = (result) => {
    sql.query(`SELECT COUNT(*) AS cantidad_tickets, id_area FROM ticket WHERE estatus = 5`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

Reportes.getEstatus6 = (result) => {
    sql.query(`SELECT COUNT(*) AS cantidad_tickets, id_area FROM ticket WHERE estatus = 6`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

Reportes.getEstatus7 = (result) => {
    sql.query(`SELECT COUNT(*) AS cantidad_tickets, id_area FROM ticket WHERE estatus = 7`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};


Reportes.getTicketsAreaTiempo = (result) => {
    sql.query(`SELECT idticket, fecha_asignado, fecha_cierre, id_area FROM ticket WHERE estatus < 7 AND estatus > 1 AND fecha_cierre != ""`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

Reportes.getTicketsPorArea = (result) => {
    sql.query(`SELECT art.area, COUNT(*) Total FROM ticket t, areas_ticket art WHERE id_area != 0 AND estatus > 1 AND t.id_area = art.idareas_ticket GROUP BY t.id_area HAVING COUNT(*) > 1`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

Reportes.getEstatus1_area = (id, result) => {
    sql.query(`SELECT COUNT(*) AS cantidad_tickets, id_area FROM ticket WHERE estatus = 1 AND id_area = ?`,[id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

Reportes.getEstatus2_area = (id, result) => {
    sql.query(`SELECT COUNT(*) AS cantidad_tickets, id_area FROM ticket WHERE estatus = 2 AND id_area = ?`,[id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

Reportes.getEstatus3_area = (id, result) => {
    sql.query(`SELECT COUNT(*) AS cantidad_tickets, id_area FROM ticket WHERE estatus = 3 AND id_area = ?`,[id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

Reportes.getEstatus4_area = (id, result) => {
    sql.query(`SELECT COUNT(*) AS cantidad_tickets, id_area FROM ticket WHERE estatus = 4 AND id_area = ?`,[id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

Reportes.getEstatus5_area = (id, result) => {
    sql.query(`SELECT COUNT(*) AS cantidad_tickets, id_area FROM ticket WHERE estatus = 5 AND id_area = ?`,[id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

Reportes.getEstatus6_area = (id, result) => {
    sql.query(`SELECT COUNT(*) AS cantidad_tickets, id_area FROM ticket WHERE estatus = 6 AND id_area = ?`,[id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

Reportes.getEstatus7_area = (id, result) => {
    sql.query(`SELECT COUNT(*) AS cantidad_tickets, id_area FROM ticket WHERE estatus = 7 AND id_area = ?`,[id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        
        result(null, res);
    })
};

module.exports = Reportes;