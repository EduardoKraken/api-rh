const sql = require("../db3.js");

// constructor
const Accesos = function (accesos) {
    this.idweb = accesos.idweb;
    this.nomcli = accesos.nomcli;
    this.calle = accesos.calle;
};

Accesos.addAcceso = (c, result) => {
    sql.query(`INSERT INTO acceso_ticket(acceso) VALUES(?)`, [c.acceso], (err, res) => {
        if (err) {
            result(err, null);
            return;
        }
       
        result(null, {
            id: res.insertId,
            ...c
        });
    });
};

Accesos.getAccesos = (result) => {
    sql.query(`SELECT * FROM acceso_ticket`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    })
};

Accesos.putAccesos = (id, sub, result) => {
    sql.query(` UPDATE acceso_ticket SET acceso=? WHERE idacceso_ticket = ?`,
        [sub.acceso, id],
        (err, res) => {
            if (err) {
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                result({
                    kind: "not_found"
                }, null);
                return;
            }

           
            result(null, {
                id: id,
                ...sub
            });
        }
    );
};

module.exports = Accesos;