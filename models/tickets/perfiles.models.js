const sql = require("../db3.js");

// constructor
const Perfiles = function(clientes) {
  this.idweb     = clientes.idweb;
  this.nomcli    = clientes.nomcli;
  this.calle     = clientes.calle;
};

Perfiles.addPerfil = (c, result) => {
	sql.query(`INSERT INTO perfiles(perfil, fecha_alta, activo_sn, fecha_baja, id_usuario_ultimo_cambio, fecha_ultima_modificacion, comentarios, llamadas_sn)VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)`,[c.perfil, c.fecha_alta, c.activo_sn, c.fecha_baja, c.id_usuario_ultimo_cambio, c.fecha_ultima_modificacion, c.comentarios, c.llamadas_sn],(err, res) => {	
    if (err) {
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...c });
	});
};

Perfiles.getPerfiles = (result)=>{
	sql.query(`SELECT * FROM perfiles`,(err,res)=>{
		if (err) {
      result(null, err);
      return;
    }
    result(null, res);
	})
};

Perfiles.putPerfiles = (id, sub, result) => {
  sql.query(` UPDATE perfiles SET perfil=?, fecha_alta=?, activo_sn=?, fecha_baja=?, id_usuario_ultimo_cambio=?, fecha_ultima_modificacion=?, comentarios=?, llamadas_sn=? WHERE id_perfil = ?`,
  [sub.perfil, sub.fecha_alta, sub.activo_sn, sub.fecha_baja, sub.id_usuario_ultimo_cambio, sub.fecha_ultima_modificacion, sub.comentarios, sub.llamadas_sn,id],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...sub });
    }
  );
};

module.exports = Perfiles;