const sql      = require("../dbErpPrueba.js");


// constructor
const Areas = function(clientes) {
  this.idweb     = clientes.idweb;
  this.nomcli    = clientes.nomcli;
  this.calle     = clientes.calle;
};

Areas.addArea = (c, result) => {
	sql.query(`INSERT INTO areas_ticket(area)VALUES(?)`,[c.area],(err, res) => {	
    if (err) {
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...c });
	});
};

Areas.getAreas = (result)=>{
	sql.query(`SELECT * FROM areas_ticket`,(err,res)=>{
		if (err) {
      result(null, err);
      return;
    }
    result(null, res);
	})
};

Areas.putAreas = (id, sub, result) => {
  sql.query(` UPDATE areas_ticket SET area=? WHERE idareas_ticket = ?`,
   [sub.area,id],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...sub });
    }
  );
};

Areas.getSucursales = (result)=>{
  sql.query(`SELECT * FROM planteles ORDER BY plantel`,(err,res)=>{
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  })
};
module.exports = Areas;