const sql = require("../dbErpPrueba.js");

// constructor
const Jefe_areas = function (Jefe_areas) {
    this.idweb = Jefe_areas.idweb;
    this.nomcli = Jefe_areas.nomcli;
    this.calle = Jefe_areas.calle;
};

Jefe_areas.addJefe_area = (c, result) => {
    sql.query(`INSERT INTO jefe_area(idareas_ticket, id_usuario, idpuesto ) VALUES(?,?,?)`, [c.idareas_ticket,c.id_usuario, c.idpuesto], (err, res) => {
        if (err) {
            result(err, null);
            return;
        }
        result(null, {
            id: res.insertId,
            ...c
        });
    });
};

Jefe_areas.getJefe_areas = (result) => {
  sql.query(`SELECT j.idjefe_area, j.idareas_ticket,j.id_usuario, a.area, u.nombre_completo, j.activo FROM jefe_area j
    LEFT JOIN areas_ticket a ON a.idareas_ticket = j.idareas_ticket
    LEFT JOIN usuarios u ON u.id_usuario = j.id_usuario 
    WHERE u.activo_sn = 1 AND j.activo = 1
    ORDER BY j.activo DESC;`, (err, res) => {

        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    })
};

Jefe_areas.getJefe_areas_PorArea = (id, result) => {
    sql.query(`SELECT j.idjefe_area, j.idareas_ticket,j.id_usuario, a.area, u.nombre_completo FROM jefe_area j
    LEFT JOIN areas_ticket a ON a.idareas_ticket = j.idareas_ticket
    LEFT JOIN usuarios u ON u.id_usuario = j.id_usuario WHERE j.idareas_ticket = ? AND j.activo = 1;`,[id], (err, res) => {

        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    })
};

Jefe_areas.putJefe_areas = (id, sub, result) => {
    sql.query(` UPDATE jefe_area SET idareas_ticket=?, id_usuario=?, idpuesto = ? WHERE idjefe_area = ?`,
        [sub.idareas_ticket, sub.id_usuario, sub.idpuesto, id],
        (err, res) => {
            if (err) {
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                result({
                    kind: "not_found"
                }, null);
                return;
            }

            result(null, {
                id: id,
                ...sub
            });
        }
    );
};

module.exports = Jefe_areas;