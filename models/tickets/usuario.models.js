// const sql = require("./../db3.js");
const sqlERP      = require("../dbErpPrueba.js");
const sql2 = require("./../dbERP2.js");

// constructor
const Usuarios = function (usuarios) {
    this.idweb = usuarios.idweb;
    this.nomcli = usuarios.nomcli;
    this.calle = usuarios.calle;
};

Usuarios.addRegistro = (c, result) => {
  sql2.query(`INSERT INTO alumnos(nombre,sucursal,telefono,unidad_negocio) VALUES(?,?,?,?)`, [c.nombre, c.sucursal, c.telefono, c.unidad_negocio], (err, res) => {
    if (err) {
      
      result(err, null);
      return;
    }
    result(null, {
      id: res.insertId,
      ...c
    });
  });
};

Usuarios.getUsuariosERP = (result) => {
  sqlERP.query(`SELECT id_usuario, nombre_completo FROM usuarios WHERE activo_sn = 1 ORDER BY nombre_completo ASC `, (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }
    
    result(null, res);
  })
};

Usuarios.getRegistro = (result) => {
  sql2.query(`SELECT * FROM alumnos`, (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }
    
    result(null, res);
  })
};

Usuarios.updateRegistro = (idalumnos, sub, result) => {
  sql2.query(` UPDATE alumnos SET fecha=? WHERE idalumnos = ?`,
    [sub.fecha, idalumnos],
    (err, res) => {
      if (err) {
        
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({
          kind: "not_found"
        }, null);
        return;
      }

      result(null, {
        id: idalumnos,
        ...sub
      });
    }
  );
};


Usuarios.getUsuarioId = (id,result) => {
  sqlERP.query(`SELECT u.nombre_completo, pu.id_perfil FROM usuarios u
LEFT JOIN perfilesusuario pu ON pu.id_usuario = u.id_usuario WHERE pu.id_perfil = 24 AND u.id_usuario = ? OR pu.id_perfil = 1 AND u.id_usuario = ?;`,[id,id], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    if(res.length > 0){
      result(null, res[0]);
    }else{
      result(null, {mensaje: 'No puedes acceder a este sistema'});
    }
  });
};

module.exports = Usuarios;