const sqlERPNUEVO     = require("../db.js");


// CONSTRUCTOR
const investigacion_personal = (investigacion_personal) => {};


// AGREGAR INVESTIGACION
investigacion_personal.addInvestigacion = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO investigacion_personal( investigar ) 
      VALUES( ? )`,
      [ c.investigar ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR INVESTIGACION
investigacion_personal.updateInvestigacion = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE investigacion_personal SET 
      investigar = ?, deleted = ? 
      WHERE idinvestigacion_personal = ?`,
      [ c.investigar, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// OBTENER INVESTIGACION ID
investigacion_personal.getInvestigacion = ( id_investigacion ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM investigacion_personal
      WHERE idinvestigacion_personal = ? 
      AND deleted = 0;`, 
      [ id_investigacion ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER INVESTIGACIONES
investigacion_personal.getInvestigaciones = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM investigacion_personal
    WHERE deleted = 0;`,
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER INVESTIGACION (NOMBRE)
investigacion_personal.getInvestigacionNombre = ( investigar ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM investigacion_personal
      WHERE investigar = ? 
      AND deleted = 0;`, 
      [ investigar ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// ----- INVESTIGACION COMENTARIOS -----



// AGREGAR INVESTIGACION_COMENTARIOS
investigacion_personal.addComentario = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO investigacion_personal_comentarios( idreclutadora, idformulario, idinvestigacion_personal, comentarios ) 
      VALUES( ?, ?, ?, ? )`,
      [ c.idreclutadora, c.idformulario, c.idinvestigacion_personal, c.comentarios ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR INVESTIGACION_COMENTARIOS
investigacion_personal.updateComentario = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE investigacion_personal_comentarios SET 
      idreclutadora = ?, idformulario = ?, idinvestigacion_personal = ?, comentarios = ?, deleted = ?
      WHERE idinvestigacion_personal_comentarios = ?`, 
      [ c.idreclutadora, c.idformulario, c.idinvestigacion_personal, c.comentarios, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// OBTENER COMENTARIO ID
investigacion_personal.getComentario = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM investigacion_personal_comentarios
      WHERE idinvestigacion_personal_comentarios = ?
      AND deleted = 0;`, [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER INVESTIGACION_COMENTARIOS POR PROSPECTO
investigacion_personal.getProspecto = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
      invp.idinvestigacion_personal_comentarios, invp.idreclutadora,
      f.nombre_completo,
      ip.investigar, invp.comentarios
    FROM investigacion_personal_comentarios invp
    INNER JOIN formulario f ON f.idformulario = invp.idformulario
    INNER JOIN investigacion_personal ip ON ip.idinvestigacion_personal = invp.idinvestigacion_personal
    WHERE invp.idformulario = 1
    AND invp.deleted = 0
    AND f.deleted = 0;`,
      [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};

module.exports = investigacion_personal;