const sqlERPNUEVO     = require("../db.js");


// CONSTRUCTOR
const pruebas_tecnicas = (pruebas_tecnicas) => {};


// AGREGAR PRUEBA_TECNICA
pruebas_tecnicas.addPrueba = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO prueba_tecnica( idpuesto, prueba_tecnica )
      VALUES( ?, ? )`,
      [ c.idpuesto, c.prueba_tecnica ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR PRUEBA_TECNICA
pruebas_tecnicas.updatePrueba = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE prueba_tecnica SET 
      idpuesto = ?, prueba_tecnica = ?, deleted = ? 
      WHERE idprueba_tecnica = ?`,
      [ c.idpuesto, c.prueba_tecnica, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// OBTENER PRUEBA_TECNICA ID_PRUEBA_TECNICA
pruebas_tecnicas.getPrueba = ( id_prueba ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM prueba_tecnica
      WHERE idprueba_tecnica = ? 
      AND deleted = 0;`, 
      [ id_prueba ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER PRUEBAS_TECNICAS
pruebas_tecnicas.getPruebas = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM prueba_tecnica
    WHERE deleted = 0;`,
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER PRUEBAS_TECNICAS POR ID_PUESTO 
pruebas_tecnicas.getPruebaPuesto = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
        p.puesto, pt.idprueba_tecnica, pt.prueba_tecnica
      FROM prueba_tecnica pt
      INNER JOIN puesto p ON p.idpuesto = pt.idpuesto
      WHERE pt.idpuesto = ?
      AND pt.deleted = 0;`,
      [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};



// ----- AGENDA -----


// AGREGAR AGENDA
pruebas_tecnicas.addAgenda = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO prueba_tecnica_agenda( idreclutadora, idformulario, idprueba_tecnica, horario_agendado )
      VALUES( ?, ?, ?, ? )`,
      [ c.idreclutadora, c.idformulario, c.idprueba_tecnica, c.horario_agendado ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR AGENDA
pruebas_tecnicas.updateAgenda = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE prueba_tecnica_agenda SET 
      idreclutadora = ?, idformulario = ?, idprueba_tecnica = ?, horario_agendado = ?, deleted = ? 
      WHERE idprueba_tecnica_agenda = ?`,
      [ c.idreclutadora, c.idformulario, c.idprueba_tecnica, c.horario_agendado, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// OBTENER AGENDA DE RECLUTADORA
pruebas_tecnicas.getAgendaReclutadora = ( id_reclutadora ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM prueba_tecnica_agenda
    WHERE idreclutadora = ?
    AND deleted = 0;`,
    [ id_reclutadora ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER AGENDA
pruebas_tecnicas.getAgenda = ( id_agenda ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM prueba_tecnica_agenda
    WHERE idprueba_tecnica_agenda = ?
    AND deleted = 0;`,
    [ id_agenda ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};



// ----- NOTAS -----


// AGREGAR NOTA
pruebas_tecnicas.addNota = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO prueba_tecnica_notas( idprueba_tecnica_agenda, notas ) 
      VALUES( ?, ? )`,
      [ c.idprueba_tecnica_agenda, c.notas ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR NOTA
pruebas_tecnicas.updateNota = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE prueba_tecnica_notas SET 
      idprueba_tecnica_agenda = ?, notas = ?, deleted = ?
      WHERE idprueba_tecnica_notas = ?`, 
      [ c.idprueba_tecnica_agenda, c.notas, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// OBTENER NOTA ID
pruebas_tecnicas.getNota = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM prueba_tecnica_notas
      WHERE idprueba_tecnica_notas = ?
      AND deleted = 0;`, [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER NOTAS POR AGENDA
pruebas_tecnicas.getNotasAgenda = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
        ptn.idprueba_tecnica_notas, ptn.idprueba_tecnica_agenda, ptn.notas,
        pta.idreclutadora, pta.idformulario, pta.idprueba_tecnica,
        pta.horario_agendado, pta.evidencia
      FROM prueba_tecnica_notas ptn
      INNER JOIN prueba_tecnica_agenda pta ON pta.idprueba_tecnica_agenda = ptn.idprueba_tecnica_agenda
      WHERE ptn.idprueba_tecnica_agenda = ?
      AND ptn.deleted = 0
      AND pta.deleted = 0;`,
      [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


module.exports = pruebas_tecnicas;