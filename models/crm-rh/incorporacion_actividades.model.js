const sqlERPNUEVO     = require("../db.js");


// CONSTRUCTOR
const incorporacion_actividades = (incorporacion_actividades) => {};


// AGREGAR ACTIVIDAD_INCORPORACION
incorporacion_actividades.addActividades = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO incorporacion_actividades( idpuesto, actividad ) 
      VALUES( ?, ? )`,
      [ c.idpuesto, c.actividad ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR ACTIVIDAD_INCORPORACION
incorporacion_actividades.updateActividades = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE incorporacion_actividades SET 
      idpuesto = ?, actividad = ?, deleted = ? 
      WHERE idincorporacion_actividades = ?`, 
      [ c.idpuesto, c.actividad, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ ...c });
    });
  });
};


// OBTENER ACTIVIDAD_INCORPORACION ID
incorporacion_actividades.getActividad = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM incorporacion_actividades
      WHERE idincorporacion_actividades = ? 
      AND deleted = 0;`, 
      [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER ACTIVIDAD_INCORPORACION
incorporacion_actividades.getActividades = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM incorporacion_actividades
    WHERE deleted = 0;`,
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER ACTIVIDAD_INCORPORACION POR PUESTO
incorporacion_actividades.getActividadPuesto = ( id, actividad ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM incorporacion_actividades
      WHERE idpuesto = ?
      AND actividad = ?
      AND deleted = 0;`, 
      [ id, actividad ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER ACTIVIDADES_INCORPORACIONES POR FORMULARIO (PROSPECTO)
incorporacion_actividades.getActividadesPuesto = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM incorporacion_actividades
    WHERE idpuesto = ?
    AND deleted = 0;`, 
      [ id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


module.exports = incorporacion_actividades;