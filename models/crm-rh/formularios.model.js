const sqlERPNUEVO = require("../db.js");
const sqlERP = require("../dbErpPrueba.js");


// CONSTRUCTOR
const formularios = (formularios) => {};


// AGREGAR FORMULARIO
formularios.addFormularios = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO formulario( nombre_completo, edad, correo, direccion, numero_wa, puesto_interes, alguien_depende, 
      estudiando, horario_estudio, trabajando, horario_trabajo, medio_vacante, dominio_ingles, cv, portafolio )
      VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )`,
      [ c.nombre_completo, c.edad, c.correo, c.direccion, c.numero_wa, c.puesto_interes, c.alguien_depende, c.estudiando, c.horario_estudio, 
        c.trabajando, c.horario_trabajo, c.medio_vacante, c.dominio_ingles, c.cv, c.portafolio ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR FORMULARIO
formularios.updateFormularios = ( c, id ) => {
  console.log( c )
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE formulario SET
      nombre_completo = ?, edad = ?, correo = ?, direccion = ?, numero_wa = ?, puesto_interes = ?, alguien_depende = ?, estudiando = ?, horario_estudio = ?, 
      trabajando = ?, horario_trabajo = ?, medio_vacante = ?, dominio_ingles = ?, cv = ?, portafolio = ?, aceptado = ?, motivo_rechazo = ?, etapa = ?, deleted = ?
      WHERE idformulario = ?`,
      [ c.nombre_completo, c.edad, c.correo, c.direccion, c.numero_wa, c.puesto_interes, c.alguien_depende, c.estudiando, c.horario_estudio, 
        c.trabajando, c.horario_trabajo, c.medio_vacante, c.dominio_ingles, c.cv, c.portafolio, c.aceptado, c.motivo_rechazo, c.etapa, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ ...c });
    });
  });
};


// OBTENER FORMULARIO ID_FORMULARIO
formularios.getFormulario = ( id_formulario ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
      f.idformulario, f.nombre_completo, f.edad, f.correo, f.direccion, f.numero_wa, 
      p.puesto, d.departamento,
      f.alguien_depende, f.estudiando, f.horario_estudio, f.trabajando, f.horario_estudio, f.etapa, f.deleted,
      f.medio_vacante, f.dominio_ingles, f.cv, f.portafolio, f.aceptado, f.motivo_rechazo, f.puesto_interes
    FROM formulario f
    LEFT JOIN puesto p ON p.idpuesto = f.puesto_interes
    LEFT JOIN departamento d ON d.iddepartamento = p.iddepartamento
    WHERE idformulario = ?
    AND f.deleted = 0
    AND d.deleted = 0;`, [ id_formulario ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};

// OBTENER FORMULARIO ID_FORMULARIO
formularios.existeFormulario = ( correo, numero_wa ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM formulario WHERE correo = ? OR numero_wa = ?;`, [ correo, numero_wa ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};

// OBTENER FORMULARIOS
formularios.getFormularios = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
      f.idformulario, f.nombre_completo, f.edad, f.correo, f.direccion, f.numero_wa, 
      f.puesto_interes, p.puesto, d.departamento,
      f.alguien_depende, f.estudiando, f.horario_estudio, f.horario_trabajo, f.trabajando, f.horario_estudio, 
      f.medio_vacante, f.dominio_ingles, f.cv, f.portafolio, f.aceptado, f.motivo_rechazo
    FROM formulario f
    LEFT JOIN puesto p ON p.idpuesto = f.puesto_interes
    LEFT JOIN departamento d ON d.iddepartamento = p.iddepartamento
    WHERE f.deleted = 0
    AND d.deleted = 0;`,
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// -----VISTAS EXTRA-----



// OBTENER FORMULARIO ID_FORMULARIO
formularios.getPuesto = ( id_puesto ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM puesto
    WHERE idpuesto = ?;`, [ id_puesto ], 
    (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};



// OBTENER PUESTO
formularios.getPuestos = ( id_puesto ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
      f.idformulario, f.nombre_completo, f.edad, f.correo, f.direccion, f.numero_wa, 
      p.puesto, d.departamento,
      f.alguien_depende, f.estudiando, f.horario_estudio, f.trabajando, f.horario_estudio, 
      f.medio_vacante, f.dominio_ingles, f.cv, f.portafolio, f.aceptado, f.motivo_rechazo
    FROM formulario f
    LEFT JOIN puesto p ON p.idpuesto = f.puesto_interes
    LEFT JOIN departamento d ON d.iddepartamento = p.iddepartamento
    WHERE f.puesto_interes = ?
    AND f.deleted = 0
    AND d.deleted = 0;`, [ id_puesto ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};

// OBTENER FORMULARIO POR RANGO DE FECHAS
formularios.getFechas = ( hora_inicio, hora_final ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
      f.idformulario, f.nombre_completo, f.edad, f.correo, f.direccion, f.numero_wa, 
      p.puesto, d.departamento,
      f.alguien_depende, f.estudiando, f.horario_estudio, f.trabajando, f.horario_estudio, 
      f.medio_vacante, f.dominio_ingles, f.cv, f.portafolio, f.aceptado, f.motivo_rechazo
    FROM formulario f
    LEFT JOIN puesto p ON p.idpuesto = f.puesto_interes
    LEFT JOIN departamento d ON d.iddepartamento = p.iddepartamento
    WHERE f.fecha_creacion BETWEEN ? AND ?
    AND f.deleted = 0
    AND d.deleted = 0;`, 
    [ hora_inicio, hora_final ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};

formularios.getPuestosFormularios = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM puesto WHERE deleted = 0 AND reclutamiento = 1 ORDER BY puesto;`, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER FORMULARIO POR ENCARGADA
formularios.getEncargada = ( id_encargada ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
      u.idusuario, u.iderp,
      f.idformulario, f.nombre_completo, f.edad, f.correo, f.direccion, f.numero_wa, 
      p.puesto, d.departamento,
      f.alguien_depende, f.estudiando, f.horario_estudio, f.trabajando, f.horario_estudio, 
      f.medio_vacante, f.dominio_ingles, f.cv, f.portafolio, f.aceptado, f.motivo_rechazo
    FROM formulario f
    LEFT JOIN puesto p ON p.idpuesto = f.puesto_interes
    LEFT JOIN departamento d ON d.iddepartamento = p.iddepartamento
    LEFT JOIN encargada_departamento ed ON d.iddepartamento = ed.iddepartamento
    LEFT JOIN usuarios u ON u.idusuario = ed.idusuarios
    WHERE ed.idencargada_departamento = ?
    AND ed.deleted = 0
    AND u.deleted = 0
    AND f.deleted = 0
    AND d.deleted = 0;`, [ id_encargada ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER ENCARGADA ID_ERP
formularios.getEncargadaNombre = ( iderp ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT
      u.nombre_completo
    FROM usuarios u
    WHERE id_usuario = ?;`, [ iderp ], 
    (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER FORMULARIOS ENCARGADAS
formularios.getEncargadas = () => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
      u.idusuario, u.iderp,
      f.idformulario, f.nombre_completo, f.edad, f.correo, f.direccion, f.numero_wa, 
      p.puesto, d.departamento,
      f.alguien_depende, f.estudiando, f.horario_estudio, f.trabajando, f.horario_estudio, 
      f.medio_vacante, f.dominio_ingles, f.cv, f.portafolio, f.aceptado, f.motivo_rechazo
    FROM formulario f
    LEFT JOIN puesto p ON p.idpuesto = f.puesto_interes
    LEFT JOIN departamento d ON d.iddepartamento = p.iddepartamento
    LEFT JOIN encargada_departamento ed ON d.iddepartamento = ed.iddepartamento
    LEFT JOIN usuarios u ON u.idusuario = ed.idusuarios
    WHERE ed.deleted = 0
    AND u.deleted = 0
    AND f.deleted = 0
    AND d.deleted = 0;`, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER NOMBRES DE ENCARGADAS ID_ERP
formularios.getEncargadasNombres = ( idsEncargadas ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT
      u.id_usuario, u.nombre_completo
    FROM usuarios u
    WHERE id_usuario IN (?);`, 
    [ idsEncargadas ], 
    (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER FORMULARIOS POR ETAPA
formularios.getEtapas = ( idetapas ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
        f.etapas, f.idformulario, f.nombre_completo, f.edad, f.correo, f.direccion, f.numero_wa, 
        p.puesto, d.departamento,
        f.alguien_depende, f.estudiando, f.horario_estudio, f.trabajando, f.horario_estudio, 
        f.medio_vacante, f.dominio_ingles, f.cv, f.portafolio, f.aceptado, f.motivo_rechazo
      FROM formulario f
      LEFT JOIN puesto p ON p.idpuesto = f.puesto_interes
      LEFT JOIN departamento d ON d.iddepartamento = p.iddepartamento
      WHERE f.etapas = ?
      AND f.deleted = 0
      AND d.deleted = 0;`, 
      [ idetapas ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};

formularios.getAllMedioVacante = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM medio_contacto WHERE deleted = 0 ORDER BY medio;`, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


module.exports = formularios;