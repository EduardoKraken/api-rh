const sqlERPNUEVO     = require("../db.js");


// CONSTRUCTOR
const speeches = (speeches) => {};


// AGREGAR SPEECH
speeches.addSpeech = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO speech( idpuesto, speech ) 
      VALUES( ?, ? )`,
      [ c.idpuesto, c.speech ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR SPEECH
speeches.updateSpeech = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE speech SET 
      idpuesto = ?, speech = ?, deleted = ? 
      WHERE idspeech = ?`, 
      [ c.idpuesto, c.speech, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// OBTENER SPEECH ID_SPEECH
speeches.getSpeech = ( id_speech ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM speech
      WHERE idspeech = ? 
      AND deleted = 0;`, 
      [ id_speech ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER SPEECHES
speeches.getSpeeches = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM speech
    WHERE deleted = 0;`,
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER SPEECH (NOMBRE)
speeches.getSpeechNombre = ( speech ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM speech
      WHERE speech = ? 
      AND deleted = 0;`, [ speech ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// ----- VISTAS EXTRA -----


// OBTENER SPEECHES POR ID_PUESTO 
speeches.getSpeechPuesto = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
        p.puesto, s.idspeech, s.speech
      FROM speech s
      INNER JOIN puesto p ON p.idpuesto = s.idpuesto
      WHERE s.idpuesto = ?
      AND s.deleted = 0;`,
      [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// ----- SPEECH COMENTARIOS -----


// AGREGAR SPEECH_COMENTARIOS
speeches.addComentario = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO speech_comentarios( idreclutadora, idformulario, idspeech, comentarios ) 
      VALUES( ?, ?, ?, ? )`,
      [ c.idreclutadora, c.idformulario, c.idspeech, c.comentarios ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR SPEECH_COMENTARIOS
speeches.updateComentario = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE speech_comentarios SET 
      idreclutadora = ?, idformulario = ?, idspeech = ?, comentarios = ?, deleted = ?
      WHERE idspeech_comentarios = ?`, 
      [ c.idreclutadora, c.idformulario, c.idspeech, c.comentarios, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// OBTENER COMENTARIO
speeches.getComentario = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM speech_comentarios
      WHERE idspeech_comentarios = ?
      AND deleted = 0;`, [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER SPEECH
speeches.getProspecto = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
        sc.idspeech_comentarios, sc.idreclutadora,
        f.nombre_completo,
        s.speech, sc.comentarios
      FROM speech_comentarios sc
      INNER JOIN formulario f ON f.idformulario = sc.idformulario
      INNER JOIN speech s ON s.idspeech = sc.idspeech
      WHERE sc.idformulario = ?
      AND sc.deleted = 0
      AND f.deleted = 0;`,
      [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};

module.exports = speeches;