const sqlERPNUEVO     = require("../db.js");


// CONSTRUCTOR
const inducciones = (inducciones) => {};


// AGREGAR INDUCCION
inducciones.addInduccion = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO inducciones( idpuesto, actividad ) 
      VALUES( ?, ? )`,
      [ c.idpuesto, c.actividad ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR INDUCCION
inducciones.updateInduccion = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE inducciones SET 
      idpuesto = ?, actividad = ?, deleted = ? 
      WHERE idinducciones = ?`,
      [ c.idpuesto, c.actividad, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// OBTENER INDUCCION ID
inducciones.getInduccion = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM inducciones
      WHERE idinducciones = ?
      AND deleted = 0;`, 
      [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER INDUCCIONES
inducciones.getInducciones = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM inducciones
    WHERE deleted = 0;`,
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER ACTIVIDAD POR ID_PUESTO
inducciones.getActividadPuesto = ( idpuesto, actividad ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM inducciones
      WHERE idpuesto = ?
      AND actividad = ?
      AND deleted = 0;`,
      [ idpuesto, actividad ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER INDUCCIONES PUESTO
inducciones.getInduccionesPuesto = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM inducciones
      WHERE idpuesto = ? 
      AND deleted = 0;`,
      [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// ----- INDUCCIONES COMENTARIOS -----


// AGREGAR INDUCCIONES_COMENTARIOS
inducciones.addComentario = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO inducciones_comentarios( idinducciones, idformulario, comentarios ) 
      VALUES( ?, ?, ? )`,
      [ c.idinducciones, c.idformulario, c.comentarios ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR INDUCCIONES_COMENTARIOS
inducciones.updateComentario = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE inducciones_comentarios SET 
      idinducciones = ?, idformulario = ?, , comentarios = ?, evidencia = ?, asistio = ?, deleted = ?
      WHERE idinducciones_comentarios = ?`, 
      [ c.idinducciones, c.idformulario, c.comentarios, c.evidencia, c.asistio, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// OBTENER INDUCCIONES_COMENTARIOS
inducciones.getComentario = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM inducciones_comentarios
      WHERE idinducciones_comentarios = ?
      AND deleted = 0;`, [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER INDUCCIONES_COMENTARIOS POR PROSPECTO
inducciones.getComentariosProspecto = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
        ci.idinducciones_comentarios, ci.idformulario, f.nombre_completo, ci.idinducciones, ci.comentarios,
        ci.evidencia, ci.asistio
      FROM inducciones_comentarios ci
      INNER JOIN formulario f ON f.idformulario = ci.idformulario
      WHERE ci.idformulario = ?
      AND ci.deleted = 0
      AND f.deleted = 0;`,
      [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};

module.exports = inducciones;