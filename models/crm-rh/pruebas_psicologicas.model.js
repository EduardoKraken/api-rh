const sqlERPNUEVO     = require("../db.js");


// CONSTRUCTOR
const pruebas_psicologicas = (pruebas_psicologicas) => {};


// AGREGAR PRUEBA_PSICOLOGICA
pruebas_psicologicas.addPrueba = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO prueba_psicologica( idpuesto, prueba_psicologica )
      VALUES( ?, ? )`,
      [ c.idpuesto, c.prueba_psicologica ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR PRUEBA_PSICOLOGICA
pruebas_psicologicas.updatePrueba = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE prueba_psicologica SET 
      idpuesto = ?, prueba_psicologica = ?, deleted = ? 
      WHERE idprueba_psicologica = ?`,
      [ c.idpuesto, c.prueba_psicologica, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// OBTENER PRUEBA_PSICOLOGICA ID_PRUEBA_PSICOLOGICA
pruebas_psicologicas.getPrueba = ( id_prueba ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM prueba_psicologica
      WHERE idprueba_psicologica = ? 
      AND deleted = 0;`, 
      [ id_prueba ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER PRUEBAS_PSICOLOGICAS
pruebas_psicologicas.getPruebas = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM prueba_psicologica
    WHERE deleted = 0;`,
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER PRUEBAS_PSICOLOGICAS POR ID_PUESTO 
pruebas_psicologicas.getPruebaPuesto = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
        p.puesto, ps.idprueba_psicologica, ps.prueba_psicologica
      FROM prueba_psicologica ps
      INNER JOIN puesto p ON p.idpuesto = ps.idpuesto
      WHERE ps.idpuesto = ?
      AND ps.deleted = 0;`,
      [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// ----- AGENDA -----


// AGREGAR AGENDA
pruebas_psicologicas.addAgenda = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO prueba_psicologica_agenda( idreclutadora, idformulario, idprueba_psicologica, horario_agendado )
      VALUES( ?, ?, ?, ? )`,
      [ c.idreclutadora, c.idformulario, c.idprueba_psicologica, c.horario_agendado ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR AGENDA
pruebas_psicologicas.updateAgenda = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE prueba_psicologica_agenda SET 
      idreclutadora = ?, idformulario = ?, idprueba_psicologica = ?, horario_agendado = ?, deleted = ? 
      WHERE idprueba_psicologica_agenda = ?`,
      [ c.idreclutadora, c.idformulario, c.idprueba_psicologica, c.horario_agendado, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// OBTENER AGENDA DE RECLUTADORA
pruebas_psicologicas.getAgendaReclutadora = ( id_reclutadora ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM prueba_psicologica_agenda
    WHERE idreclutadora = ?
    AND deleted = 0;`,
    [ id_reclutadora ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER AGENDA
pruebas_psicologicas.getAgenda = ( id_agenda ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM prueba_psicologica_agenda
    WHERE idprueba_psicologica_agenda = ?
    AND deleted = 0;`,
    [ id_agenda ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};



// ----- NOTAS -----


// AGREGAR NOTA
pruebas_psicologicas.addNota = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO prueba_psicologica_notas( idprueba_psicologica_agenda, notas ) 
      VALUES( ?, ? )`,
      [ c.idprueba_psicologica_agenda, c.notas ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR NOTA
pruebas_psicologicas.updateNota = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE prueba_psicologica_notas SET 
      idprueba_psicologica_agenda = ?, notas = ?, deleted = ?
      WHERE idprueba_psicologica_notas = ?`, 
      [ c.idprueba_psicologica_agenda, c.notas, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// OBTENER NOTA ID
pruebas_psicologicas.getNota = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM prueba_psicologica_notas
      WHERE idprueba_psicologica_notas = ?
      AND deleted = 0;`, [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER NOTAS POR AGENDA
pruebas_psicologicas.getNotasAgenda = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
        psn.idprueba_psicologica_notas, psn.idprueba_psicologica_agenda, psn.notas,
        psa.idreclutadora, psa.idformulario, psa.idprueba_psicologica,
        psa.horario_agendado, psa.evidencia
      FROM prueba_psicologica_notas psn
      INNER JOIN prueba_psicologica_agenda psa ON psa.idprueba_psicologica_agenda = psn.idprueba_psicologica_agenda
      WHERE psn.idprueba_psicologica_agenda = ?
      AND psn.deleted = 0
      AND psa.deleted = 0;`,
      [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


module.exports = pruebas_psicologicas;