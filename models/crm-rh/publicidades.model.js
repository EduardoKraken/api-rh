const sqlERPNUEVO = require("../db.js");


// CONSTRUCTOR
const publicidades = (publicidades) => {};


// AGREGAR PUBLICIDAD
publicidades.addPublicidad = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO publicidad_rh( publicidad ) 
      VALUES( ? )`,
      [ c.publicidad ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR PUBLICIDAD
publicidades.updatePublicidad = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE publicidad_rh SET 
      publicidad = ?, deleted = ? 
      WHERE idpublicidad_rh = ?`, 
      [ c.publicidad, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// OBTENER PUBLICIDAD ID_PUBLICIDAD
publicidades.getPublicidad = ( id_publicidad ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM publicidad_rh
      WHERE idpublicidad_rh = ? AND deleted = 0;`, [ id_publicidad ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER PUBLICIDADES

publicidades.getPublicidades = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM publicidad_rh
    WHERE deleted = 0;`,
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER PUBLICIDAD NOMBRE
publicidades.getPublicidadNombre = ( nombre ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM publicidad_rh
      WHERE publicidad = ? AND deleted = 0;`, [ nombre ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};

module.exports = publicidades;