const sqlERPNUEVO     = require("../db.js");


// CONSTRUCTOR
const examenes_medicos = (examenes_medicos) => {};


// AGREGAR EXAMEN
examenes_medicos.addExamen = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO examenes_medicos( idformulario, examen, comentario )
      VALUES( ?, ?, ? )`,
      [ c.idformulario, c.examen, c.comentario ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR EXAMEN
examenes_medicos.updateExamen = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE examenes_medicos SET
      idformulario = ?, comentario = ?, examen = ?, deleted = ?
      WHERE idexamenes_medicos = ?`,
      [ c.idformulario, c.comentario, c.examen, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ ...c });
    });
  });
};


// OBTENER EXAMEN ID_EXAMEN
examenes_medicos.getExamen = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM examenes_medicos
    WHERE idexamenes_medicos = ?
    AND deleted = 0;`, [ id ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};



// OBTENER EXAMEN ID_FROMULARIO (PROSPECTO)
examenes_medicos.getExamenProspecto = ( idprospecto ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
    f.idformulario, f.nombre_completo,
      em.idexamenes_medicos, em.examen
    FROM examenes_medicos em
    INNER JOIN formulario f ON f.idformulario = em.idformulario
    WHERE em.idformulario = ?
    AND f.deleted = 0
    AND em.deleted = 0;`, [ idprospecto ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Agregado por Angel Rdz

examenes_medicos.getExamenes = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
    f.idformulario, f.nombre_completo,
      em.idexamenes_medicos, em.examen, em.comentario
    FROM examenes_medicos em
    INNER JOIN formulario f ON f.idformulario = em.idformulario
    WHERE f.deleted = 0
    AND em.deleted = 0;`, [ id ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


module.exports = examenes_medicos;