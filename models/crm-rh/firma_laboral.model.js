const sqlERPNUEVO     = require("../db.js");


// CONSTRUCTOR
const firma_laboral = (firma_laboral) => {};


// AGREGAR FIRMA_LABORAL
firma_laboral.addFirma = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO firma_laboral( idformulario, propuesta_economica, horario_agendado )
      VALUES( ?, ?, ? )`,
      [ c.idformulario, c.propuesta_economica, c.horario_agendado ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR FIRMA_LABORAL
firma_laboral.updateFirma = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE firma_laboral SET
      idformulario = ?, propuesta_economica = ?, propuesta_firmada = ?, horario_agendado = ?, deleted = ?
      WHERE idfirma_laboral = ?`,
      [ c.idformulario, c.propuesta_economica, c.propuesta_firmada, c.horario_agendado, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ ...c });
    });
  });
};


// OBTENER FIRMA_LABORAL ID_FIRMA_LABORAL
firma_laboral.getFirma = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM firma_laboral
    WHERE idfirma_laboral = ?
    AND deleted = 0;`, [ id ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};



// OBTENER FIRMAS_LABORALES ID
firma_laboral.getFirmas = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM firma_laboral
    WHERE deleted = 0;`,
    (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER FIRMA_LABORAL DE PROSPECTO
firma_laboral.getPropuestaProspecto = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM firma_laboral
    WHERE idformulario = ?
    AND deleted = 0;`, [ id ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER AGENDA POR FECHA PROGRAMADA
firma_laboral.getAgenda = ( fecha_inicio, fecha_final ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
      fl.idfirma_laboral, f.nombre_completo,
      fl.propuesta_economica, fl.propuesta_firmada, fl.horario_agendado
    FROM firma_laboral fl
    INNER JOIN formulario f ON f.idformulario = fl.idformulario
    WHERE fl.horario_agendado BETWEEN ? AND ?
    AND fl.deleted = 0
    AND f.deleted = 0;`, 
    [ fecha_inicio, fecha_final ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


module.exports = firma_laboral;