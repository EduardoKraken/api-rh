const sqlERPNUEVO     = require("../db.js");


// CONSTRUCTOR
const llamadas_tecnicas = (llamadas_tecnicas) => {};


// AGREGAR LLAMADA_TECNICA
llamadas_tecnicas.addLlamada = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO llamadas_tecnicas( idformulario, liga, horario_agendado )
      VALUES( ?, ?, ? )`,
      [ c.idformulario, c.liga, c.horario_agendado ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR LLAMADA_TECNICA
llamadas_tecnicas.updateLlamada = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE llamadas_tecnicas SET
      idformulario = ?, liga = ?, horario_agendado = ?, deleted = ?
      WHERE idllamadas_tecnicas = ?`,
      [ c.idformulario, c.liga, c.horario_agendado, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ ...c });
    });
  });
};


// OBTENER LLAMADA_TECNICA ID_LLAMADA_TECNICA
llamadas_tecnicas.getLlamada = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM llamadas_tecnicas
    WHERE idllamadas_tecnicas = ?
    AND deleted = 0;`, [ id ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER LLAMADA_TECNICA DE PROSPECTO
llamadas_tecnicas.getLlamadaProspecto = ( idformulario ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM llamadas_tecnicas
    WHERE idformulario = ?
    AND deleted = 0;`, [ idformulario ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER LLAMADA_TECNICA POR RANGO DE FECHAS
llamadas_tecnicas.getFecha = ( fecha_inicio, fecha_final ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
      lt.idllamadas_tecnicas, f.nombre_completo,
      lt.horario_agendado
    FROM llamadas_tecnicas lt
    INNER JOIN formulario f ON f.idformulario = lt.idformulario
    WHERE lt.horario_agendado BETWEEN ? AND ?
    AND lt.deleted = 0
    AND f.deleted = 0;`, 
    [ fecha_inicio, fecha_final ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// ----- PARTICIPANTES -----


// AGREGAR PARTICIPANTES
llamadas_tecnicas.addParticipantes = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO llamadas_tecnicas_participantes( idllamadas_tecnicas, idempleado )
      VALUES( ?, ? )`,
      [ c.idllamadas_tecnicas, c.idempleado ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR PARTICIPANTES
llamadas_tecnicas.updateParticipante = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE llamadas_tecnicas_participantes SET
      idllamadas_tecnicas = ?, idempleado = ?, deleted = ?
      WHERE idllamadas_tecnicas_participantes = ?`,
      [ c.idllamadas_tecnicas, c.idempleado, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ ...c });
    });
  });
};


// OBTENER PARTICIPANTES ID_LLAMADA_TECNICA
llamadas_tecnicas.getParticipanteLLamada = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM llamadas_tecnicas_participantes
    WHERE idllamadas_tecnicas = ?
    AND deleted = 0;`, [ id ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};



// OBTENER PARTICIPANTES ID_LLAMADA_TECNICA
llamadas_tecnicas.getParticipantesAgenda = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT 
      ltp.idempleado,
      lt.idllamadas_tecnicas, lt.liga, lt.horario_agendado
    FROM llamadas_tecnicas_participantes ltp
    INNER JOIN llamadas_tecnicas lt ON lt.idllamadas_tecnicas = ltp.idllamadas_tecnicas
    WHERE ltp.idempleado = ?
    AND lt.deleted = 0
    AND ltp.deleted = 0;`, [ id ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};



module.exports = llamadas_tecnicas;