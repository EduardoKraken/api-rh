const sqlERPNUEVO = require("../db.js");


// CONSTRUCTOR
const preguntas_frecuentes = (preguntas_frecuentes) => {};


// AGREGAR PREGUNTA
preguntas_frecuentes.addPregunta = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO preguntas_frecuentes( pregunta ) 
      VALUES( ? )`,
      [ c.pregunta ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR PREGUNTA
preguntas_frecuentes.updatePregunta = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE preguntas_frecuentes SET 
      pregunta = ?, deleted = ? 
      WHERE idpreguntas_frecuentes = ?`, 
      [ c.pregunta, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// OBTENER PREGUNTA ID_PREGUNTA
preguntas_frecuentes.getPregunta = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM preguntas_frecuentes
      WHERE idpreguntas_frecuentes = ? 
      AND deleted = 0;`, 
      [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER PREGUNTAS
preguntas_frecuentes.getPreguntas = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM preguntas_frecuentes
    WHERE deleted = 0;`,
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER PREGUNTA_NOMBRE
preguntas_frecuentes.getpreguntaNombre = ( pregunta ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM preguntas_frecuentes
      WHERE pregunta = ? 
      AND deleted = 0;`, 
      [ pregunta ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};

module.exports = preguntas_frecuentes;