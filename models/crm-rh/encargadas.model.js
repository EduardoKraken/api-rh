const sqlERPNUEVO = require("../db.js");
const sqlERP      = require("../dbErpPrueba.js");


// CONSTRUCTOR
const encargadas = (encargadas) => {};


// AGREGAR ENCARGADA
encargadas.addEncargada = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO encargada_departamento( idpuesto, idusuarios ) 
      VALUES( ?, ? )`,
      [ c.idpuesto, c.idusuarios ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR ENCARGADA
encargadas.updateEncargada = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE encargada_departamento SET deleted = 1 WHERE idencargada_departamento = ?`, [ id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id });
    });
  });
};


// OBTENER ENCARGADA ID_ENCARGADA
encargadas.getEncargada = ( id_encargada ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM encargada_departamento
      WHERE idusuarios = ? AND deleted = 0;`, [ id_encargada ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER ENCARGADAS
encargadas.getEncargadas = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT ed.idencargada_departamento, ed.idpuesto, d.puesto, ed.idusuarios 
    FROM encargada_departamento ed
  	LEFT JOIN puesto d ON d.idpuesto = ed.idpuesto
    WHERE ed.deleted = 0;`,
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Agregado por Angel Rdz

// OBTENER USUARIOS
encargadas.getUsuarios = (  ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT id_usuario, nombre_completo FROM usuarios WHERE activo_sn = 1;`, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};

// OBTENER DEPARTAMENTOS
encargadas.getDepartamentos = (  ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT iddepartamento, departamento FROM departamento WHERE deleted = 0;`, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};



module.exports = encargadas;