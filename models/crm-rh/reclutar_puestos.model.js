const sqlERPNUEVO = require("../db.js");


// CONSTRUCTOR
const reclutar_puestos = (reclutar_puestos) => {};


// AGREGAR RECLUTAR_PUESTOS
reclutar_puestos.addReclutadora = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO reclutar_puestos( idreclutadora, idpuesto ) 
      VALUES( ?, ? )`,
      [ c.idreclutadora, c.idpuesto ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR RECLUTAR_PUESTOS
reclutar_puestos.updateReclutadora = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE reclutar_puestos SET 
      iddepartamento = ?, idusuarios = ?, deleted = ? 
      WHERE idreclutar_puestos = ?`, 
      [ c.iddepartamento, c.idusuarios, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ ...c });
    });
  });
};


// OBTENER RECLUTAR_PUESTOS ID_RECLUTAR_PUESTOS
reclutar_puestos.getReclutadora = ( id_reclutadora ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM reclutar_puestos
      WHERE idreclutar_puestos = ? 
      AND deleted = 0;`, 
      [ id_reclutadora ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER RECLUTAR_PUESTOS
reclutar_puestos.getReclutadoras = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM reclutar_puestos WHERE deleted = 0;`,
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// ----- ACCIONES EXTRA -----


// OBTENER PUESTO
reclutar_puestos.getReclutadoraPuesto = ( id_puesto ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
      f.idformulario, f.nombre_completo, f.edad, f.correo, f.direccion, f.numero_wa, 
      p.puesto,
      f.alguien_depende, f.estudiando, f.horario_estudio, f.trabajando, f.horario_estudio,
      f.medio_vacante, f.dominio_ingles, f.cv, f.portafolio, f.aceptado, f.motivo_rechazo
    FROM formulario f
    LEFT JOIN reclutar_puestos rp ON rp.idpuesto = f.puesto_interes
    LEFT JOIN puesto p ON p.idpuesto = rp.idpuesto
    WHERE f.puesto_interes = ?
    AND rp.deleted = 0
    AND f.deleted = 0;`, 
    [ id_puesto ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER FORMULARIO MEDIANTE ID
reclutar_puestos.getPropectoFormulario = ( id_formulario ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM formulario
    WHERE idformulario = ?
    AND deleted = 0;`,
    [ id_formulario ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};



// ACTUALIZAR RECLUTAR_PUESTOS
reclutar_puestos.updateProspectoFormulario = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE formularios SET
      aceptado = ?, motivo_rechazo = ?
      WHERE idformularios = ?`, 
      [ c.aceptado, c.motivo_rechazo, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// ----- AGENDA -----


// AGREGAR RECLUTAR_PUESTOS
reclutar_puestos.addAgenda = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO reclutadora_agenda( idreclutadora, idformulario, horario )
      VALUES( ?, ?, ? )`,
      [ c.idreclutadora, c.idformulario, c.horario ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR AGENDA
reclutar_puestos.updateAgenda = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE reclutadora_agenda SET 
      idreclutadora = ?, idformulario = ?, horario = ?, duracion = ?, deleted = ?
      WHERE idreclutadora_agenda = ?`,
      [ c.idreclutadora, c.idformulario, c.horario, c.duracion, deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// OBTENER AGENDA DE RECLUTADORA
reclutar_puestos.getDisponibilidad = ( id_reclutadora, horario ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM reclutadora_agenda
    WHERE idreclutadora = ?
    AND horario = ?
    AND deleted = 0;`,
    [ id_reclutadora, horario ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER AGENDA
reclutar_puestos.getAgenda = ( id_agenda ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM reclutadora_agenda
    WHERE idreclutadora_agenda = ?
    AND deleted = 0;`,
    [ id_agenda ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// ACTUALIZAR DURACION_AGENDA
reclutar_puestos.updateAgendaHorario = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE reclutadora_agenda SET 
      duracion = ?
      WHERE idreclutadora_agenda = ?`, 
      [ c.duracion, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


module.exports = reclutar_puestos;