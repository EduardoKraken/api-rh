const sqlERPNUEVO     = require("../db.js");


// CONSTRUCTOR
const capacitaciones = (capacitaciones) => {};


// AGREGAR CAPACITACION
capacitaciones.addCapacitacion = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO capacitaciones( idpuesto, actividad ) 
      VALUES( ?, ? )`,
      [ c.idpuesto, c.actividad ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR CAPACITACION
capacitaciones.updateCapacitacion = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE capacitaciones SET 
      idpuesto = ?, actividad = ?, deleted = ? 
      WHERE idcapacitaciones = ?`,
      [ c.idpuesto, c.actividad, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// OBTENER CAPACITACION ID
capacitaciones.getCapacitacion = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM capacitaciones
      WHERE idcapacitaciones = ?
      AND deleted = 0;`, 
      [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER CAPACITACIONES
capacitaciones.getCapacitaciones = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM capacitaciones
    WHERE deleted = 0;`,
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER ACTIVIDAD POR ID_PUESTO
capacitaciones.getActividadPuesto = ( idpuesto, actividad ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM capacitaciones
      WHERE idpuesto = ?
      AND actividad = ?
      AND deleted = 0;`,
      [ idpuesto, actividad ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER CAPACITACIONES PUESTO
capacitaciones.getCapacitacionesPuesto = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM capacitaciones
      WHERE idpuesto = ? 
      AND deleted = 0;`,
      [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// ----- CAPACITACIONES COMENTARIOS -----


// AGREGAR CAPACITACIONES_COMENTARIOS
capacitaciones.addComentario = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO capacitaciones_comentarios( idcapacitaciones, idformulario, comentarios ) 
      VALUES( ?, ?, ? )`,
      [ c.idcapacitaciones, c.idformulario, c.comentarios ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR CAPACITACIONES_COMENTARIOS
capacitaciones.updateComentario = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE capacitaciones_comentarios SET 
      idcapacitaciones = ?, idformulario = ?, , comentarios = ?, evidencia = ?, aprobado = ?, deleted = ?
      WHERE idcapacitaciones_comentarios = ?`, 
      [ c.idcapacitaciones, c.idformulario, c.comentarios, c.evidencia, c.aprobado, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    });
  });
};


// OBTENER CAPACITACIONES_COMENTARIOS_ID
capacitaciones.getComentario = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM capacitaciones_comentarios
      WHERE idcapacitaciones_comentarios = ?
      AND deleted = 0;`, [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER CAPACITACIONES_COMENTARIOS POR PROSPECTO
capacitaciones.getComentariosProspecto = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
        cc.idcapacitaciones_comentarios, cc.idformulario, cc.idcapacitaciones, f.nombre_completo, cc.comentarios,
        cc.evidencia, cc.aprobado
      FROM capacitaciones_comentarios cc
      INNER JOIN formulario f ON f.idformulario = cc.idformulario
      WHERE cc.idformulario = ?
      AND cc.deleted = 0
      AND f.deleted = 0;`,
      [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};

module.exports = capacitaciones;