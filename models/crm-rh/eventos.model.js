const sqlERPNUEVO     = require("../db.js");


// CONSTRUCTOR
const eventos = (eventos) => {};


// AGREGAR EVENTOS
eventos.addEvento = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO eventos( tipo_evento, idformulario, idreclutadora, etapa, detalle ) 
      VALUES( ?, ?, ?, ?, ? )`,
      [ c.tipo_evento, c.idformulario, c.idreclutadora, c.etapa, c.detalle ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR EVENTOS
eventos.updateEvento = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE eventos SET 
      tipo_evento = ?, idformulario = ?, idreclutadora = ?, etapa = ?, detalle = ?, deleted = ? 
      WHERE ideventos = ?`, 
      [ c.tipo_evento, c.idformulario, c.idreclutadora, c.etapa, c.detalle, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ ...c });
    });
  });
};


// OBTENER EVENTO ID
eventos.getEvento = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM eventos
      WHERE ideventos = ? 
      AND deleted = 0;`, 
      [ id ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER EVENTOS
eventos.getEventos = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM eventos
    WHERE deleted = 0;`,
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER ACTIVIDADES_INCORPORACIONES POR FORMULARIO (PROSPECTO)
eventos.getEventosFormulario = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM eventos
    WHERE idformulario = ?
    AND deleted = 0;`,
      [ id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


module.exports = eventos;