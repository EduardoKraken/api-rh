const { result }  = require("lodash");
// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");
const sqlERPNUEVO = require("../db.js");


//const constructor
const clases = function(clases) {};

clases.getGruposPlantelFast = ( id_plantel, cicloFast, dia, planteles, fecha ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id AS id_grupo,c.iderp AS id_ciclo, g.nombre AS grupo, c.nombre AS curso, h.hora_inicio, g.id_unidad_negocio,
    	IF((ELT(WEEKDAY(?) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")),CONCAT(u2.nombre, " ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno, ""))) AS teacher,
      IF((ELT(WEEKDAY(?) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,gt.id_teacher,gt.id_teacher_2) AS id_teacher,
      (ELT(WEEKDAY(?) + 1, 1, 1, 1, 2, 2, 1, 2)) AS num_teacher,
      (ELT(WEEKDAY(?) + 1, 1, 2, 3, 4, 5, 6, 7)) AS dia
      FROM grupos g
      LEFT JOIN cursos cu         ON cu.id = g.id_curso
      LEFT JOIN frecuencia f      ON f.id = cu.id_frecuencia
      LEFT JOIN horarios h        ON h.id = g.id_horario
      LEFT JOIN ciclos c          ON c.id = g.id_ciclo
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
      LEFT JOIN usuarios u        ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2       ON u2.id = gt.id_teacher_2
      WHERE c.iderp = ${ cicloFast } AND ${ dia } = 1
      AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0 AND g.nombre NOT LIKE '%certif%' AND g.nombre NOT LIKE '%indu%'
      AND g.nombre NOT LIKE '%exci%' AND g.id_plantel in( ? )
      ORDER BY h.hora_inicio;`,[fecha,fecha,fecha,fecha,planteles],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


clases.getGruposPlantelInbi = ( id_plantel, cicloInbi, dia, planteles, fecha ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id AS id_grupo,c.iderp AS id_ciclo, g.nombre AS grupo, c.nombre AS curso, h.hora_inicio, g.id_unidad_negocio,
    	IF((ELT(WEEKDAY(?) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")),CONCAT(u2.nombre, " ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno, ""))) AS teacher,
      IF((ELT(WEEKDAY(?) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,gt.id_teacher,gt.id_teacher_2) AS id_teacher,
      (ELT(WEEKDAY(?) + 1, 1, 1, 1, 2, 2, 1, 2)) AS num_teacher,
      (ELT(WEEKDAY(?) + 1, 1, 2, 3, 4, 5, 6, 7)) AS dia
      FROM grupos g
      LEFT JOIN cursos cu         ON cu.id = g.id_curso
      LEFT JOIN frecuencia f      ON f.id = cu.id_frecuencia
      LEFT JOIN horarios h        ON h.id = g.id_horario
      LEFT JOIN ciclos c          ON c.id = g.id_ciclo
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
      LEFT JOIN usuarios u        ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2       ON u2.id = gt.id_teacher_2
      WHERE c.iderp = ${ cicloInbi } AND ${ dia } = 1
      AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0 AND g.nombre NOT LIKE '%certif%' AND g.nombre NOT LIKE '%indu%'
      AND g.nombre NOT LIKE '%exci%' AND g.id_plantel in ( ? )
      ORDER BY h.hora_inicio;`,[fecha,fecha,fecha,fecha,planteles],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

clases.addEvaluacionClase = (pu, result) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`INSERT INTO calidad_eval_clase(id_usuario_erp,id_teacher,id_grupo,estatus,unidad_negocio,id_ciclo,dia,deleted,url)VALUES(?,?,?,?,?,?,?,?,?)`, 
    [pu.id_usuario_erp, pu.id_teacher, pu.id_grupo, pu.estatus, pu.unidad_negocio, pu.id_ciclo, pu.dia, pu.deleted, pu.url],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve({ id: res.insertId, ...pu })
    })
  })
}


clases.addRespuestas = (pu, id) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`INSERT INTO calidad_res_clase(idcalidad_eval_clase, pregunta, estatus, valor, nota)VALUES(?,?,?,?,?)`, 
    [ id, pu.pregunta, pu.check, pu.valor, pu.nota ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve({ id: res.insertId, ...pu })
    })
  })
}


clases.updateClaseEvalArchivo = ( id, archivo ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`UPDATE calidad_eval_clase SET url = ? WHERE idcalidad_eval_clase = ?`, [ archivo, id ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve({ id, archivo })
    })
  })
}

clases.getEvaluacion = ( id_grupo, unidad_negocio ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM calidad_eval_clase WHERE id_grupo = ${ id_grupo } AND unidad_negocio = ${ unidad_negocio } AND DATE(fecha_creacion) = CURDATE();`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


clases.getClasesEvaluadas = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM calidad_eval_clase WHERE deleted = 0`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

clases.getClasesEvaluadasHoy = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM calidad_eval_clase WHERE deleted = 0 AND DATE(fecha_creacion) = CURDATE();`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

clases.getClasesEvaluadasUsuario = ( id_usuario_erp, cicloFast, cicloInbi ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM calidad_eval_clase WHERE deleted = 0 AND id_usuario_erp = ${ id_usuario_erp } AND id_ciclo IN (${ cicloFast },${ cicloInbi });`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

clases.getRespuestasEvaluacion = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM calidad_res_clase WHERE  idcalidad_eval_clase = ${ id };`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

clases.getCalificacionesClaseEvaluada = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT idcalidad_eval_clase, SUM(valor) AS calificacion FROM calidad_res_clase
    WHERE estatus = 1
    GROUP BY idcalidad_eval_clase;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

clases.getClasesEvaluadasAll = ( cicloFast, cicloInbi ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM calidad_eval_clase WHERE deleted = 0 AND id_ciclo IN (${ cicloFast },${ cicloInbi });`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

clases.getIdFastTeacher = ( email) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT id AS id_fast FROM usuarios WHERE email = ?;`,[ email ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res[0])
    })
  })
}

clases.getIdInbiTeacher = ( email) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT id AS id_inbi FROM usuarios WHERE email = ?;`,[ email ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res[0])
    })
  })
}

clases.getClasesEvaluadasTeacher = ( id_fast, id_inbi, cicloFast, cicloInbi ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM calidad_eval_clase 
      WHERE deleted = 0 AND id_teacher = ${ id_fast } AND unidad_negocio = 2 AND id_ciclo IN (${ cicloFast },${ cicloInbi })
      OR    deleted = 0 AND id_teacher = ${ id_inbi } AND unidad_negocio = 1 AND id_ciclo IN (${ cicloFast },${ cicloInbi });`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

clases.evaluacionClaseUpdate = (idcalidad_eval_clase, estatus ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`UPDATE calidad_eval_clase SET estatus = ? WHERE idcalidad_eval_clase = ?`,[ estatus, idcalidad_eval_clase ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  });
};


clases.updateRespuesta = (p) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`UPDATE calidad_res_clase SET aceptado = ?, respuesta  = ? WHERE idcalidad_res_clase = ? `,[ p.aceptado, p.respuesta, p.idcalidad_res_clase ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  });
};

clases.updateRespuesta = (p) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`UPDATE calidad_res_clase SET aceptado = ?, respuesta  = ? WHERE idcalidad_res_clase = ? `,[ p.aceptado, p.respuesta, p.idcalidad_res_clase ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  });
};


clases.getUsuariosActivos = () => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM usuarios WHERE activo_sn = 1 ORDER BY nombre_completo`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    });
  })
};

clases.gruposRol = ( id_ciclo, escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT g.iderp, g.nombre AS grupo, g.id_unidad_negocio, g.id_nivel, g.optimizado, g.online, cu.nombre AS curso,
      p.nombre AS plantel, h.hora_inicio, h.hora_fin, u.iderp AS id_teacher, u2.iderp AS id_teacher_2, g.deleted, g.online AS modalidad,
      IF(LOCATE('DOMIN', g.nombre) > 0,1,IF(LOCATE('SABAT', g.nombre) > 0,2,3)) AS curso
      FROM grupos g
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN planteles p ON p.id = g.id_plantel
      LEFT JOIN cursos cu ON cu.id = g.id_curso
      LEFT JOIN horarios h ON h.id = g.id_horario
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
      WHERE c.iderp =  ?
      AND g.nombre NOT LIKE '%CERTI%'
      AND g.nombre NOT LIKE '%INDUC%'
      AND g.nombre NOT LIKE '%EXCI%';`,[ id_ciclo ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    });
  })
};

clases.getTeachersErp = () => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT u.id_usuario, u.nombre_completo, t.id AS id_trabajador, p.id_perfil FROM fyn_cat_trabajadores t 
      LEFT JOIN usuarios u ON u.id_trabajador = t.id
      LEFT JOIN perfilesusuario pu ON pu.id_usuario = u.id_usuario
      LEFT JOIN perfiles p ON p.id_perfil = pu.id_perfil WHERE p.id_perfil = 9 ORDER BY nombre_completo;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    });
  })
};


clases.insertarRol = (pu, id) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO rol_clases(idgrupo, lu1, ma1, mi1, ju1, vi1, lu2, ma2, mi2, ju2, vi2, lu3, ma3, mi3, ju3, vi3, lu4, ma4, mi4, ju4, vi4)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, 
    [ pu.iderp, pu.lu1, pu.ma1, pu.mi1, pu.ju1, pu.vi1, pu.lu2, pu.ma2, pu.mi2, pu.ju2, pu.vi2, pu.lu3, pu.ma3, pu.mi3, pu.ju3, pu.vi3, pu.lu4, pu.ma4, pu.mi4, pu.ju4, pu.vi4 ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve({ id: res.insertId, ...pu })
    })
  })
}

clases.existeRolClase = ( id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM rol_clases WHERE idgrupo = ?;`,[id_grupo],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res[0])
    });
  })
};

clases.obtenerRolRegistrado = ( idgrupos ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT r.*, u1.nombre_completo AS lu1_n, u2.nombre_completo AS lu2_n, u3.nombre_completo AS lu3_n, u4.nombre_completo AS lu4_n, u5.nombre_completo AS ma1_n, 
      u6.nombre_completo AS ma2_n, u7.nombre_completo AS ma3_n, u8.nombre_completo AS ma4_n, u9.nombre_completo AS mi1_n, u10.nombre_completo AS mi2_n, u11.nombre_completo AS mi3_n, 
      u12.nombre_completo AS mi4_n, u13.nombre_completo AS ju1_n, u14.nombre_completo AS ju2_n, u15.nombre_completo AS ju3_n, u16.nombre_completo AS ju4_n, 
      u17.nombre_completo AS vi1_n, u18.nombre_completo AS vi2_n, u19.nombre_completo AS vi3_n, u20.nombre_completo AS vi4_n FROM rol_clases r
      LEFT JOIN usuarios u1 ON u1.id_usuario = r.lu1
      LEFT JOIN usuarios u2 ON u2.id_usuario = r.lu2
      LEFT JOIN usuarios u3 ON u3.id_usuario = r.lu3
      LEFT JOIN usuarios u4 ON u4.id_usuario = r.lu4
      LEFT JOIN usuarios u5 ON u5.id_usuario = r.ma1
      LEFT JOIN usuarios u6 ON u6.id_usuario = r.ma2
      LEFT JOIN usuarios u7 ON u7.id_usuario = r.ma3
      LEFT JOIN usuarios u8 ON u8.id_usuario = r.ma4
      LEFT JOIN usuarios u9 ON u9.id_usuario = r.mi1
      LEFT JOIN usuarios u10 ON u10.id_usuario = r.mi2
      LEFT JOIN usuarios u11 ON u11.id_usuario = r.mi3
      LEFT JOIN usuarios u12 ON u12.id_usuario = r.mi4
      LEFT JOIN usuarios u13 ON u13.id_usuario = r.ju1
      LEFT JOIN usuarios u14 ON u14.id_usuario = r.ju2
      LEFT JOIN usuarios u15 ON u15.id_usuario = r.ju3
      LEFT JOIN usuarios u16 ON u16.id_usuario = r.ju4
      LEFT JOIN usuarios u17 ON u17.id_usuario = r.vi1
      LEFT JOIN usuarios u18 ON u18.id_usuario = r.vi2
      LEFT JOIN usuarios u19 ON u19.id_usuario = r.vi3
      LEFT JOIN usuarios u20 ON u20.id_usuario = r.vi4
      WHERE idgrupo IN ( ? );`,[idgrupos],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    });
  })
};


clases.obtenerRolReemplazo = ( idgrupos ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT r.*, u1.nombre_completo AS lu1_n, u2.nombre_completo AS lu2_n, u3.nombre_completo AS lu3_n, u4.nombre_completo AS lu4_n, u5.nombre_completo AS ma1_n, 
      u6.nombre_completo AS ma2_n, u7.nombre_completo AS ma3_n, u8.nombre_completo AS ma4_n, u9.nombre_completo AS mi1_n, u10.nombre_completo AS mi2_n, u11.nombre_completo AS mi3_n, 
      u12.nombre_completo AS mi4_n, u13.nombre_completo AS ju1_n, u14.nombre_completo AS ju2_n, u15.nombre_completo AS ju3_n, u16.nombre_completo AS ju4_n, 
      u17.nombre_completo AS vi1_n, u18.nombre_completo AS vi2_n, u19.nombre_completo AS vi3_n, u20.nombre_completo AS vi4_n FROM rol_clases_reemplazos r
      LEFT JOIN usuarios u1 ON u1.id_usuario = r.lu1
      LEFT JOIN usuarios u2 ON u2.id_usuario = r.lu2
      LEFT JOIN usuarios u3 ON u3.id_usuario = r.lu3
      LEFT JOIN usuarios u4 ON u4.id_usuario = r.lu4
      LEFT JOIN usuarios u5 ON u5.id_usuario = r.ma1
      LEFT JOIN usuarios u6 ON u6.id_usuario = r.ma2
      LEFT JOIN usuarios u7 ON u7.id_usuario = r.ma3
      LEFT JOIN usuarios u8 ON u8.id_usuario = r.ma4
      LEFT JOIN usuarios u9 ON u9.id_usuario = r.mi1
      LEFT JOIN usuarios u10 ON u10.id_usuario = r.mi2
      LEFT JOIN usuarios u11 ON u11.id_usuario = r.mi3
      LEFT JOIN usuarios u12 ON u12.id_usuario = r.mi4
      LEFT JOIN usuarios u13 ON u13.id_usuario = r.ju1
      LEFT JOIN usuarios u14 ON u14.id_usuario = r.ju2
      LEFT JOIN usuarios u15 ON u15.id_usuario = r.ju3
      LEFT JOIN usuarios u16 ON u16.id_usuario = r.ju4
      LEFT JOIN usuarios u17 ON u17.id_usuario = r.vi1
      LEFT JOIN usuarios u18 ON u18.id_usuario = r.vi2
      LEFT JOIN usuarios u19 ON u19.id_usuario = r.vi3
      LEFT JOIN usuarios u20 ON u20.id_usuario = r.vi4
      WHERE idgrupo IN ( ? );`,[idgrupos],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    });
  })
};


clases.obtenerRolReemplazoGrupo = ( idgrupo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM rol_clases_reemplazos WHERE idgrupo = ? ;`,[idgrupo],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res[0])
    });
  })
};

clases.insertarRolReemplazo = ( pu ) => {
  console.log( pu )
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO rol_clases_reemplazos(idgrupo, lu1, ma1, mi1, ju1, vi1, lu2, ma2, mi2, ju2, vi2, lu3, ma3, mi3, ju3, vi3, lu4, ma4, mi4, ju4, vi4)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, 
    [ pu.idgrupo, pu.lu1, pu.ma1, pu.mi1, pu.ju1, pu.vi1, pu.lu2, pu.ma2, pu.mi2, pu.ju2, pu.vi2, pu.lu3, pu.ma3, pu.mi3, pu.ju3, pu.vi3, pu.lu4, pu.ma4, pu.mi4, pu.ju4, pu.vi4 ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve({ id: res.insertId, ...pu })
    })
  })
}

clases.updateRolReemplazo = ( idgrupo, bloque, id_teacher ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE rol_clases_reemplazos SET ${ bloque } = ${ id_teacher } WHERE idrol_clases_reemplazos > 0 AND idgrupo = ${ idgrupo } `,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  });
};

module.exports = clases;