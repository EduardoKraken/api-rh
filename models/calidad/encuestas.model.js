const { result }  = require("lodash");
const sqlERP      = require("../db3.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");
const sqlERPNUEVO = require("../db.js");

//const constructor
const preguntas = function(preguntas) {};

preguntas.cantidadAlumnosEncuestados = ( id_ciclo, escuela ) => {
	const db = escuela == 2 ? sqlFAST : sqlINBI
	return new Promise((resolve,reject)=>{
		db.query(`SELECT COUNT(*) AS totalEncuestas
			FROM encuesta_satis  e
			LEFT JOIN ciclos c ON c.id = e.id_ciclo
			WHERE e.deleted = 0 AND c.iderp = ?
			GROUP BY e.id_alumno;`,[ id_ciclo ],(err, res) => {
			if(err){
				reject({ message: err.sqlMessage });
				return;
			}
			resolve(res)
		})
	})
}

// CANTIDAD DE ENCUESTAS APLICADAS POR NIVEL
preguntas.cantidadAlumnosEncuestadosNivel = ( id_ciclo, escuela ) => {
	const db = escuela == 2 ? sqlFAST : sqlINBI
	return new Promise((resolve,reject)=>{
		db.query(`SELECT COUNT(*) AS totalEncuestas, g.id_nivel, e.idpre_enc_satis, e.idopc_enc_satis
			FROM encuesta_satis  e
			LEFT JOIN ciclos c ON c.id = e.id_ciclo
			LEFT JOIN grupos g ON g.id = e.id_grupo
			WHERE e.deleted = 0 AND c.iderp = ?
			GROUP BY g.id_nivel, e.idpre_enc_satis, e.idopc_enc_satis;`,[ id_ciclo ],(err, res) => {
			if(err){
				reject({ message: err.sqlMessage });
				return;
			}
			resolve(res)
		})
	})
}

preguntas.cantidadAlumnosPorEncuestar = ( id_ciclo, escuela ) => {
	const db = escuela == 2 ? sqlFAST : sqlINBI
	return new Promise((resolve,reject)=>{
		db.query(`SELECT COUNT(*) AS cantAlumnos FROM grupo_alumnos ga
			LEFT JOIN grupos g ON g.id = ga.id_grupo
			LEFT JOIN ciclos c ON c.id = g.id_ciclo
			WHERE c.iderp = ?;`,[ id_ciclo ],(err, res) => {
			if(err){
				reject({ message: err.sqlMessage });
				return;
			}
			resolve(res[0])
		})
	})
}

/* CANTDAD DE ALUMNO POR ENCUESTAS POR NIVEL */
preguntas.cantidadAlumnosPorEncuestarNivel = ( id_ciclo, escuela ) => {
	const db = escuela == 2 ? sqlFAST : sqlINBI
	return new Promise((resolve,reject)=>{
		db.query(`SELECT COUNT(*) AS cantAlumnos, g.id_nivel FROM grupo_alumnos ga
			LEFT JOIN grupos g ON g.id = ga.id_grupo
			LEFT JOIN ciclos c ON c.id = g.id_ciclo
			WHERE c.iderp = ?
			GROUP BY g.id_nivel;`,[ id_ciclo ],(err, res) => {
			if(err){
				reject({ message: err.sqlMessage });
				return;
			}
			resolve(res)
		})
	})
}

// OBTENER LAS PREGUNTAS
preguntas.getPreguntasSatisfaccion = ( escuela ) => {
	const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT * FROM pre_enc_satis WHERE deleted = 0;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

// OBTENER LAS OPCIONES DE LAS PREGUNTAS
preguntas.getOpcionesSatisfaccion = ( escuela ) => {
	const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT * FROM opc_enc_satis WHERE deleted = 0;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}


// OBTENER LAS RESPUESTAS DE LAS PREGUNTAS
preguntas.getRespuestasEncuesta = ( id_ciclo, escuela ) => {
	const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT e.*, g.id_nivel FROM encuesta_satis  e
			LEFT JOIN ciclos c ON c.id = e.id_ciclo
			LEFT JOIN grupos g ON g.id = e.id_grupo
			WHERE e.deleted = 0 AND c.iderp = ?;`,[ id_ciclo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}


preguntas.obtenerTeachersNombre = ( ids, escuela ) => {
	const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT id, CONCAT(nombre, ' ', apellido_paterno, ' ', IFNULL(apellido_materno,'')) AS nombre 
      FROM usuarios
      WHERE id IN ( ? );`,[ ids ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}


/* OBTENER LAS PREGUNTAS ABIERTAS */
preguntas.obtenerPreguntasAbiertas = ( id_ciclo, escuela ) => {
	const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT p.pregunta, e.id_teacher,e.num_teacher, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno,
			p.tipo_pregunta, p.seccion, g.nombre AS grupo, g.id_nivel, CONCAT(t.nombre, ' ', t.apellido_paterno, ' ', IFNULL(t.apellido_materno,'')) AS teacher,
			e.respuesta_abierta
			FROM encuesta_satis e
			LEFT JOIN usuarios a ON a.id = e.id_alumno
			LEFT JOIN pre_enc_satis p ON p.idpre_enc_satis = e.idpre_enc_satis
			LEFT JOIN grupos g ON g.id = e.id_grupo
			LEFT JOIN usuarios t ON t.id = e.id_teacher
			LEFT JOIN ciclos c ON c.id = e.id_ciclo
			WHERE p.tipo_pregunta = 2
			AND c.iderp = ?
			AND e.respuesta_abierta <> '';`,[ id_ciclo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}


module.exports = preguntas;
