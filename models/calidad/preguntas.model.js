const { result }  = require("lodash");
const sqlERP      = require("../db3.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");
const sqlERPNUEVO = require("../db.js");

//const constructor
const preguntas = function(preguntas) {};

preguntas.getPreguntasAll = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM calidad_preguntas WHERE deleted = 0`,(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  })
}


preguntas.addPreguntas = (p, result) => {
  sqlERPNUEVO.query(`INSERT INTO calidad_preguntas(pregunta,valor)VALUES(?,?)`, [ p.pregunta, p.valor],
  (err, res) => {
    if (err) { result(err, null); return; }
    result(null, { id: res.insertId, ...p });
  });
};

preguntas.updatePreguntas = (u, result) => {
  sqlERPNUEVO.query(` UPDATE calidad_preguntas SET pregunta=?, valor=?, deleted = ? WHERE idcalidad_preguntas = ?`, [u.pregunta, u.valor, u.deleted, u.idcalidad_preguntas],
  (err, res) => {
    if (err) { result(null, err); return; }
    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, u);
  })
}

module.exports = preguntas;
