const { result } = require("lodash");
const sql = require("./db.js");

//const constructor
const DisponibilidadUsuario = function(permisos) {
  this.idusuario = permisos.idusuario;
  this.iderp = permisos.iderp;
  this.idpuesto = permisos.idpuesto;
};

DisponibilidadUsuario.getDisponibilidadUsuario = result => {
  sql.query(`SELECT * FROM disponibilidad_usuario`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};


DisponibilidadUsuario.getDisponibilidadUsuarioId = (id,result) => {
  sql.query(`SELECT * FROM disponibilidad_usuario WHERE idusuario = ?`,[id], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};


DisponibilidadUsuario.addDisponibilidadUsuario = (u, result) => {
  sql.query(`INSERT INTO disponibilidad_usuario(idusuario,inicio,fin,usuario_registro,fecha_creacion,fecha_actualizo,deleted,dia)VALUES(?,?,?,?,?,?,?,?)`,
   [u.idusuario,u.inicio,u.fin,u.usuario_registro,u.fecha_creacion,u.fecha_actualizo,u.deleted,u.dia],
    (err, res) => {
      if (err) { result(err, null); return; }
      result(null, { id: res.insertId, ...u });
    }
  )
}

DisponibilidadUsuario.updateDisponibilidadUsuario = (id, u, result) => {
  sql.query(` UPDATE disponibilidad_usuario SET inicio=?,fin=?,fecha_actualizo=?,deleted=?,dia=? WHERE iddisponibilidad_usuario=?`, 
    [u.inicio,u.fin,u.fecha_actualizo,u.deleted,u.dia, id],
    (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id, ...u });
    }
  )
}

DisponibilidadUsuario.deleteDisponibilidadUsuario = (id, result) => {
  sql.query("DELETE FROM disponibilidad_usuario WHERE iddisponibilidad_usuario = ?", id, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    result(null, res);
  });
};



module.exports = DisponibilidadUsuario;