const { result } = require("lodash");
// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");
const sqlINBI = require("../dbINBI.js");
const sqlFAST = require("../dbFAST.js");


//const constructor
const Reportes = function(depas) {};

Reportes.habilitarGroup = () => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));`,(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

Reportes.reporteTeacherAsistenciaFast1 = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id, g.nombre,g.id_curso,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher, a.fecha_asistencia,
      (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) AS dia
      FROM asistencia_grupo a
      LEFT JOIN grupos g ON g.id = a.id_grupo 
      LEFT JOIN grupo_teachers gt1 ON gt1.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt1.id_teacher
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      WHERE g.id_ciclo = ? AND g.optimizado = 0 AND g.deleted = 0 AND a.valor_asistencia = 0 AND  DATE(a.fecha_asistencia) <= CURDATE() 
      AND (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) IN ('Lunes','Martes','Miercoles','Sabado', 'Domingo')
      GROUP BY a.fecha_asistencia, a.id_grupo;`,[actual], (err, cicloActual) => {
      if(err){
        reject(err);
        return;
      }
      resolve(cicloActual)
    });
  });
};

Reportes.reporteTeacherAsistenciaFast2 = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id, g.nombre,g.id_curso,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher, a.fecha_asistencia,
      (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) AS dia
      FROM asistencia_grupo a
      LEFT JOIN grupos g ON g.id = a.id_grupo 
      LEFT JOIN grupo_teachers gt1 ON gt1.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt1.id_teacher
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      WHERE g.id_ciclo = ? AND g.optimizado = 0 AND g.deleted = 0 AND a.valor_asistencia = 0 AND  DATE(a.fecha_asistencia) <= CURDATE() 
      AND (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) IN ('Lunes','Martes','Miercoles','Sabado', 'Domingo')
      GROUP BY a.fecha_asistencia, a.id_grupo;`,[actual], (err, cicloActual) => {
      if(err){
        reject(err);
        return;
      }
      resolve(cicloActual)
    });
  });
};

/****************************************************************************************************/

Reportes.habilitarGroupInbi = () => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));`,(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

Reportes.reporteTeacherAsistenciaInbi1 = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id, g.nombre,g.id_curso,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher, a.fecha_asistencia,
      (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) AS dia
      FROM asistencia_grupo a
      LEFT JOIN grupos g ON g.id = a.id_grupo 
      LEFT JOIN grupo_teachers gt1 ON gt1.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt1.id_teacher
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      WHERE g.id_ciclo = ? AND g.optimizado = 0 AND g.deleted = 0 AND a.valor_asistencia = 0 AND  DATE(a.fecha_asistencia) <= CURDATE() 
      AND (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) IN ('Lunes','Martes','Miercoles','Sabado', 'Domingo')
      GROUP BY a.fecha_asistencia, a.id_grupo;`,[actual], (err, cicloActual) => {
      if(err){
        reject(err);
        return;
      }
      resolve(cicloActual)
    });
  });
};

Reportes.reporteTeacherAsistenciaInbi2 = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id, g.nombre,g.id_curso,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher, a.fecha_asistencia,
      (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) AS dia
      FROM asistencia_grupo a
      LEFT JOIN grupos g ON g.id = a.id_grupo 
      LEFT JOIN grupo_teachers gt1 ON gt1.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt1.id_teacher
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      WHERE g.id_ciclo = ? AND g.optimizado = 0 AND g.deleted = 0 AND a.valor_asistencia = 0 AND  DATE(a.fecha_asistencia) <= CURDATE() 
      AND (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) IN ('Lunes','Martes','Miercoles','Sabado', 'Domingo')
      GROUP BY a.fecha_asistencia, a.id_grupo;`,[actual], (err, cicloActual) => {
      if(err){
        reject(err);
        return;
      }
      resolve(cicloActual)
    });
  });
};

/****************************************************************************************************/

Reportes.alumosCicloSiguiente = (inbi,fast,sigInbi,sigFast) => {
   return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT  DISTINCT a.id_alumno, g.grupo, g.id_grupo FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.id_ciclo IN (?,?) 
      AND a.adeudo = 0 
      AND g.activo_sn = 1
      AND g.grupo NOT LIKE '%IND%' AND g.grupo NOT LIKE '%CERT%'
      AND a.id_alumno IN (SELECT  DISTINCT a.id_alumno FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.id_ciclo IN (?,?) 
      AND a.adeudo = 0 
      AND g.activo_sn = 1
      AND g.grupo NOT LIKE '%IND%' AND g.grupo NOT LIKE '%CERT%'
      GROUP BY g.id_grupo)
      GROUP BY g.id_grupo;`,[inbi,fast,sigInbi,sigFast], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};




module.exports = Reportes;

