const { result } = require("lodash");
const sqlERPNUEVO = require("../db.js");
// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");

//const constructor
const reporteVentas = function(depas) {};

reporteVentas.getContactosVendedora = ( fecha_inicio, fecha_final ) => {
  return new Promise((resolve,reject)=>{
		sqlERPNUEVO.query(`SELECT * FROM prospectos WHERE DATE(fecha_creacion) BETWEEN DATE(?) AND DATE(?);`, [ fecha_inicio, fecha_final ],(err, res) => {
		  if (err) {
        return reject( err.sqlMessage );
      }
      // Retornamos solo un dato
      return resolve( res );
		});
  });
};

reporteVentas.getContactosInscritosVendedora = ( fecha_inicio, fecha_final ) => {
  return new Promise((resolve,reject)=>{
		sqlERPNUEVO.query(`SELECT * FROM prospectos WHERE DATE(fecha_creacion) BETWEEN DATE(?) AND DATE(?)
			AND finalizo = 1 AND idgrupo > 0 AND DATE(fecha_finalizo ) BETWEEN DATE(?) AND DATE(?);`, [ fecha_inicio, fecha_final, fecha_inicio, fecha_final ],(err, res) => {
		  if (err) {
        return reject( err.sqlMessage );
      }
      // Retornamos solo un dato
      return resolve( res );
		});
  });
};

reporteVentas.getInscritosRango = ( fecha_inicio, fecha_final ) => {
  return new Promise((resolve,reject)=>{
		sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo, plantel,ciclo,
			ga.id_usuario_ultimo_cambio FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta BETWEEN CONCAT(DATE(?)," 00:00:00") AND CONCAT(DATE(?)," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(DATE(?)," 00:00:00")) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY alumno;`, [ fecha_inicio, fecha_final, fecha_inicio ],(err, res) => {
		  if (err) {
        return reject( err.sqlMessage );
      }
      // Retornamos solo un dato
      return resolve( res );
		});
  });
};



module.exports = reporteVentas;