const { result } = require("lodash");
// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");


//const constructor
const ri_montos = function(e) {};

ri_montos.getIngresosCicloInicio = ( id_inicio_inbi, id_inicio_fast ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT sum(i.monto_pagado) as total, p.id_plantel, p.plantel, IF(LOCATE('FAST',p.plantel) = 0, 1,2) AS escuela FROM ingresos i
			LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
			LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
			WHERE g.id_ciclo IN (?,?)
			GROUP BY  p.id_plantel
			ORDER BY total ASC;`,[ id_inicio_inbi, id_inicio_fast ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};


ri_montos.getIngresosCicloRi = ( id_fin_inbi, id_fin_fast, id_inicio_inbi, id_inicio_fast ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`select sum(i.monto_pagado) as total, p.id_plantel, p.plantel, IF(LOCATE('FAST',p.plantel) = 0, 1,2) AS escuela from ingresos i 
			left join grupos g on i.id_grupo = g.id_grupo
			left join planteles p on g.id_plantel = p.id_plantel
			where g.id_ciclo IN (?,?)
			and ifnull(null, 0) 	in(0, p.id_plantel) 
			and i.id_alumno in(select id_alumno from ingresos i 
			left join grupos g on i.id_grupo = g.id_grupo
			left join ciclos c on g.id_ciclo = c.id_ciclo
			left join planteles p on g.id_plantel = p.id_plantel
			where g.id_ciclo IN (?,?))
			GROUP BY p.id_plantel;`,[ id_fin_inbi, id_fin_fast, id_inicio_inbi, id_inicio_fast ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

ri_montos.getAlumnosCicloActual = ( id_inicio_inbi, id_inicio_fast ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DISTINCT a.*,p.id_plantel, IF(LOCATE('FAST',p.plantel) = 0, 1,2) AS escuela FROM alumnos_grupos_especiales_carga a 
			LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
			LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
			WHERE a.id_ciclo IN (?,?) AND a.adeudo = 0  AND g.grupo NOT LIKE '%INVER%' AND g.grupo NOT LIKE '%CERTI%' 
			AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
			GROUP BY a.id_alumno;`,[ id_inicio_inbi, id_inicio_fast ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

ri_montos.getAlumnosCicloSiguiente = ( id_inicio_inbi, id_inicio_fast, id_fin_inbi, id_fin_fast ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT  DISTINCT a.*,p.id_plantel, IF(LOCATE('FAST',p.plantel) = 0, 1,2) AS escuela FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE a.id_ciclo IN (?,?) AND a.adeudo = 0 
      AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
      AND a.id_alumno IN(SELECT  DISTINCT a.id_alumno FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE a.id_ciclo IN (?,?) AND a.adeudo = 0  GROUP BY a.id_alumno)
      GROUP BY a.id_alumno;`,[ id_inicio_inbi, id_inicio_fast, id_fin_inbi, id_fin_fast ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

ri_montos.getPlanteles = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT p.id_plantel, p.plantel, IF(LOCATE('FAST',p.plantel) = 0, 1,2) AS escuela FROM planteles p WHERE id_plantel IN (1,2,3,4,5,6,7,8,9,10,11,12,14,16,17);`,[ ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

ri_montos.getLeadsAyer = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM leds WHERE DATE(fecha_creacion) = DATE_SUB(DATE(CURRENT_DATE()), interval 1 DAY);`,[ ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};


ri_montos.getContactosAyer = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM prospectos WHERE DATE(fecha_creacion) = DATE_SUB(DATE(CURRENT_DATE()), interval 1 DAY);`,[ ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};


module.exports = ri_montos;


