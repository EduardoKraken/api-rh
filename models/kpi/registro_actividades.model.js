const { result }  = require("lodash");
// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");
const sqlERPNUEVO = require("../db.js");


//const constructor
const registro_actividades = function(depas) {};

registro_actividades.getRegistrosActividades = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT a.*, p.puesto, WEEK(DATE(a.fecha_inicio)) AS semana, YEAR(DATE(a.fecha_inicio)) AS anio FROM actividades_personal a
			LEFT JOIN puesto p ON p.idpuesto = a.idpuesto WHERE a.deleted = 0;`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

registro_actividades.getMesAnio = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT WEEK(NOW()) AS semana, YEAR(NOW()) AS anio;`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res[0])
    });
  });
};

registro_actividades.existenActividades = ( idusuarioerp, fecha_inicio ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM actividades_personal
      WHERE idusuarioerp = ? 
      AND WEEK(fecha_inicio) = WEEK( ? )
      AND YEAR(fecha_inicio) = YEAR( ? );`,[ idusuarioerp, fecha_inicio, fecha_inicio ], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res[0])
    });
  });
};

// Registrar numero de actividad
registro_actividades.addRegistroActividades = ( a ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`INSERT INTO actividades_personal(numero_actividades,usuario_registro,idpuesto,idusuarioerp,realizadas,fecha_inicio)
    VALUES(?,?,?,?,?,?)`,[a.numero_actividades, a.usuario_registro, a.idpuesto,a.idusuarioerp, 0 ,a.fecha_inicio],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve({ id: res.insertId })
    })
  })
}

// Registrar los detalles de la actividad
registro_actividades.addRegistroDetalleActividad = ( a ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`INSERT INTO detalle_actividades_personal(idactividades_personal,numero_actividades,usuario_registro,suma_resta)
    VALUES(?,?,?,?)`,[a.idactividades_personal, a.numero_actividades, a.usuario_registro, a.suma_resta],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve({ id: res.insertId })
    })
  })
}

// Actualizar las actividades
registro_actividades.updateAddActividades =  ( numero_actividades, id ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`UPDATE actividades_personal SET numero_actividades = (numero_actividades + ?) WHERE idactividades_personal = ?;`,
      [ numero_actividades, id ],(err, res) => {
      if (err) { reject(err); return; }
      if (res.affectedRows == 0) {
        reject({ kind: "not_found" });
        return;
      }
      resolve({ numero_actividades, id});
    })
  })
}

// Actividades del usuario por semana actuallll
registro_actividades.getRegistroActividadesUsuario = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT a.*, p.puesto, WEEK(DATE(a.fecha_inicio)) AS semana, YEAR(DATE(a.fecha_inicio)) AS anio FROM actividades_personal a
      LEFT JOIN puesto p ON p.idpuesto = a.idpuesto 
      WHERE a.deleted = 0 
      AND a.idusuarioerp = ${ id }
      AND WEEK(fecha_inicio) = WEEK(NOW())
      AND YEAR(fecha_inicio) = YEAR(NOW());`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res[0])
    });
  });
};

registro_actividades.updateMisActividades =  ( realizadas, id ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`UPDATE actividades_personal SET realizadas = (realizadas + ?) WHERE idactividades_personal = ?;`,
      [ realizadas, id ],(err, res) => {
      if (err) { reject(err); return; }
      if (res.affectedRows == 0) {
        reject({ kind: "not_found" });
        return;
      }
      resolve({ realizadas, id});
    })
  })
}

/* REGISTRO DE VACANTES PARA RH, DISPONIBLES */
registro_actividades.addVacantesDisponibles = ( p, porcentaje ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`INSERT INTO vacantes_disponibles(vacantes,vacantes_disponibles,porcentaje,usuario_creo)
    VALUES(?,?,?,?)`,[p.vacantes, p.vacantes_disponibles, porcentaje, p.usuario_creo],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve({ id: res.insertId, ...p, porcentaje })
    })
  })
}

registro_actividades.getVacantesDisponibles = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM vacantes_disponibles WHERE deleted = 0 ORDER BY fecha_creacion DESC LIMIT 1 ;`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res[0])
    });
  });
};


module.exports = registro_actividades;