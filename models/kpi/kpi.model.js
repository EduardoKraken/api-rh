const { result } = require("lodash");
// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");
const sqlINBI = require("../dbINBI.js");
const sqlFAST = require("../dbFAST.js");

//const constructor
const kpi = function(depas) {};

/* KPI GENERAL */
kpi.getAlumnosInscritos = ( cicloActualInbi, cicloActualFast  ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DISTINCT a.id_alumno, a.id_grupo, a.adeudo, SUM(i.monto_pagado) AS pagado, p.plantel, p.id_plantel, IF(LOCATE('FAST', p.plantel) > 0,2,1) AS escuela, m.id_maestro, m2.id_maestro AS id_maestro_2,
      CONCAT(al.nombre, ' ', al.apellido_paterno, ' ', IFNULL(al.apellido_materno,'')) AS nombre, g.grupo,
      IF(LOCATE('CERTI', g.grupo) > 0,1,0) AS certificacion FROM ingresos i
      LEFT JOIN alumnos al ON al.id_alumno = i.id_alumno 
      LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos_grupos_especiales_carga a ON a.id_alumno = i.id_alumno AND i.id_grupo = a.id_grupo
      LEFT JOIN maestrosgrupo m ON m.id_grupo = g.id_grupo AND m.activo_sn = 1 AND m.numero_teacher = 1
      LEFT JOIN maestrosgrupo m2 ON m2.id_grupo = g.id_grupo AND m2.activo_sn = 1 AND m2.numero_teacher = 2
      WHERE g.id_ciclo IN (?,?)
      GROUP BY a.id_alumno, g.id_grupo;`, [ cicloActualInbi, cicloActualFast  ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res);
    });
  });
};


kpi.getAlumnosRezagados = ( fecha_inicio_ciclo, idAlumnosSiguientes, idAlumnosActuales ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT COUNT(id_alumno) AS cant_alumnos, id_alumno FROM alumnos_grupos_especiales_carga 
      WHERE id_ciclo IN (
        SELECT id_ciclo 
        FROM ciclos WHERE fecha_inicio_ciclo < ?
        AND ciclo NOT LIKE '%INDU%'
        AND ciclo NOT LIKE '%CAMBIOS%'
      )
      AND id_alumno IN ( ? )
      AND id_alumno NOT IN ( ? )
      GROUP BY id_alumno;`, [ fecha_inicio_ciclo, idAlumnosSiguientes, idAlumnosActuales ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res);
    });
  });
};

kpi.alumnosActualNoPre = ( cicloActualInbi, cicloActualFast, idAlumnosResagados ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DISTINCT i.id_alumno,  SUM(i.monto_pagado) AS pagado, a.adeudo, g.grupo, al.matricula, g.id_plantel, g.id_grupo FROM ingresos i
        LEFT JOIN alumnos_grupos_especiales_carga a ON a.id_alumno = i.id_alumno AND i.id_grupo = a.id_grupo
        LEFT JOIN grupos g ON g.id_grupo = i.id_grupo AND g.id_ciclo = a.id_ciclo
        LEFT JOIN alumnos al ON al.id_alumno = a.id_alumno
        WHERE g.id_ciclo IN ( ? , ? )
        GROUP BY i.id_alumno AND a.id_alumno NOT IN ( ? )`,
        [ cicloActualInbi, cicloActualFast, idAlumnosResagados ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res);
    });
  });
};


kpi.getAlumnosInscritosCicloSiguiente = ( cicloActualInbi, cicloActualFast, ciclosSiguienteInbi, cicloSiguienteFast ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT  DISTINCT a.id_alumno, a.id_grupo, a.adeudo, SUM(i.monto_pagado) AS pagado,p.id_plantel, IF(LOCATE('FAST', p.plantel) > 0,2,1) AS escuela, m.id_maestro, m2.id_maestro AS id_maestro_2,IFNULL(b.id_beca,'') AS id_beca,
      CONCAT(al.nombre, ' ', al.apellido_paterno, ' ', IFNULL(al.apellido_materno,'')) AS nombre, g.grupo, p.plantel,
      IF(LOCATE('TEENS', g.grupo) > 0,0,IF(g.id_nivel = 40 OR g.id_nivel = 42,1,0)) AS catorce,
      a3.pagado AS nuevoPago,
      (SELECT DISTINCT DATE(t1.fecha_alta) FROM gruposalumnos t1 
      LEFT JOIN grupos t2 ON t2.id_grupo = t1.id_grupo
      WHERE t1.id_alumno = a.id_alumno AND t2.id_ciclo IN (?,?)
      AND DATE(t1.fecha_alta) = IF((WEEKDAY(DATE_SUB(CURDATE(), INTERVAL 1 DAY)) + 1) = 7, DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 1 DAY), DATE_SUB(CURDATE(), INTERVAL 1 DAY))) AS fecha_alta_nuevo_grupo 
      FROM ingresos i
      LEFT JOIN alumnos_grupos_especiales_carga a ON a.id_alumno = i.id_alumno AND a.id_grupo = i.id_grupo
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN maestrosgrupo m ON m.id_grupo = g.id_grupo AND m.activo_sn = 1 AND m.numero_teacher = 1
      LEFT JOIN maestrosgrupo m2 ON m2.id_grupo = g.id_grupo AND m2.activo_sn = 1 AND m2.numero_teacher = 2
      LEFT JOIN fyn_proc_becas_prospectos_alumnos b ON b.id_grupo = a.id_grupo AND b.id_alumno = a.id_alumno
      LEFT JOIN alumnos al ON al.id_alumno = a.id_alumno 
      LEFT JOIN alumnos_grupos_especiales_carga a3 ON a3.id_alumno = a.id_alumno AND a3.id_ciclo IN (?,?) 
      WHERE a.id_ciclo IN (?,?) AND a.adeudo = 0 
      AND g.id_grupo IN (SELECT DISTINCT id_grupo FROM maestrosgrupo)
      AND a.id_alumno IN(SELECT  DISTINCT a.id_alumno FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE a.id_ciclo IN (?,?) AND a.adeudo = 0 AND a.pagado > 0  GROUP BY a.id_alumno)
      GROUP BY a.id_alumno
      ORDER BY b.id_beca;`, [ ciclosSiguienteInbi, cicloSiguienteFast, ciclosSiguienteInbi, cicloSiguienteFast, cicloActualInbi, cicloActualFast, ciclosSiguienteInbi, cicloSiguienteFast ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res);
    });
  });
};

kpi.getAlumnosInscritosCicloSiguienteRI = ( cicloActualInbi, cicloActualFast, ciclosSiguienteInbi, cicloSiguienteFast ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT  DISTINCT a.id_alumno, a.id_grupo, a.adeudo, SUM(i.monto_pagado) AS pagado,p.id_plantel, IF(LOCATE('FAST', p.plantel) > 0,2,1) AS escuela, m.id_maestro, m2.id_maestro AS id_maestro_2,IFNULL(b.id_beca,'') AS id_beca,
      CONCAT(al.nombre, ' ', al.apellido_paterno, ' ', IFNULL(al.apellido_materno,'')) AS nombre, g.grupo, p.plantel,
      IF(LOCATE('TEENS', g.grupo) > 0,0,IF(g.id_nivel = 40 OR g.id_nivel = 42,1,0)) AS catorce,
      a3.pagado AS nuevoPago,
      (SELECT DISTINCT DATE(t1.fecha_alta) FROM gruposalumnos t1 
      LEFT JOIN grupos t2 ON t2.id_grupo = t1.id_grupo
      WHERE t1.id_alumno = a.id_alumno AND t2.id_ciclo IN (?,?)
      AND DATE(t1.fecha_alta) = IF((WEEKDAY(DATE_SUB(CURDATE(), INTERVAL 1 DAY)) + 1) = 7, DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 1 DAY), DATE_SUB(CURDATE(), INTERVAL 1 DAY))) AS fecha_alta_nuevo_grupo 
      FROM ingresos i
      LEFT JOIN alumnos_grupos_especiales_carga a ON a.id_alumno = i.id_alumno AND a.id_grupo = i.id_grupo
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN maestrosgrupo m ON m.id_grupo = g.id_grupo AND m.activo_sn = 1 AND m.numero_teacher = 1
      LEFT JOIN maestrosgrupo m2 ON m2.id_grupo = g.id_grupo AND m2.activo_sn = 1 AND m2.numero_teacher = 2
      LEFT JOIN fyn_proc_becas_prospectos_alumnos b ON b.id_grupo = a.id_grupo AND b.id_alumno = a.id_alumno
      LEFT JOIN alumnos al ON al.id_alumno = a.id_alumno 
      LEFT JOIN alumnos_grupos_especiales_carga a3 ON a3.id_alumno = a.id_alumno AND a3.id_ciclo IN (?,?) 
      WHERE a.id_ciclo IN (?,?)
      AND a.id_alumno IN(SELECT  DISTINCT a.id_alumno FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE a.id_ciclo IN (?,?) AND a.adeudo = 0 AND a.pagado > 0  GROUP BY a.id_alumno)
      GROUP BY a.id_alumno
      ORDER BY b.id_beca;`, [ ciclosSiguienteInbi, cicloSiguienteFast, ciclosSiguienteInbi, cicloSiguienteFast, cicloActualInbi, cicloActualFast, ciclosSiguienteInbi, cicloSiguienteFast ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res);
    });
  });
};


kpi.getAlumnosFuturaBeca = ( cicloActualInbi, cicloActualFast, ciclosSiguienteInbi, cicloSiguienteFast ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT  DISTINCT a.id_alumno, a.id_grupo, a.pagado, a.adeudo,p.id_plantel,IF(LOCATE('FAST', p.plantel) > 0,2,1) AS escuela, m.id_maestro, m2.id_maestro AS id_maestro_2,
      CONCAT(al.nombre, ' ', al.apellido_paterno, ' ', IFNULL(al.apellido_materno,'')) AS nombre
      FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos al ON al.id_alumno = a.id_alumno 
      LEFT JOIN maestrosgrupo m   ON m.id_grupo  = g.id_grupo AND m.activo_sn  = 1 AND m.numero_teacher = 1
      LEFT JOIN maestrosgrupo m2  ON m2.id_grupo = g.id_grupo AND m2.activo_sn = 1 AND m2.numero_teacher = 2
      LEFT JOIN fyn_proc_becas_prospectos_alumnos fb ON fb.id_alumno = a.id_alumno AND fb.id_grupo = a.id_grupo
      WHERE a.id_ciclo IN (?,?) AND a.adeudo = 0 AND a.pagado > 0
      AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
      AND a.id_alumno IN(SELECT  DISTINCT a.id_alumno FROM alumnos_grupos_especiales_carga a 
        LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
        LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        WHERE a.id_ciclo IN (?,?) AND a.adeudo = 0  GROUP BY a.id_alumno)
        AND (SELECT id_beca FROM fyn_proc_becas_prospectos_alumnos b WHERE b.id_grupo IN (SELECT id_grupo FROM grupos WHERE id_ciclo IN (?,?))  AND b.id_alumno = a.id_alumno LIMIT 1)  = 4
        AND fb.id_beca IS NULL
      GROUP BY a.id_alumno;`, [ cicloActualInbi, cicloActualFast, ciclosSiguienteInbi, cicloSiguienteFast, ciclosSiguienteInbi, cicloSiguienteFast ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res);
    });
  });
};

kpi.getAlumnosBecaAutomatica = ( ciclosSiguienteInbi, cicloSiguienteFast ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT ga.id_alumno, ga.id_grupo, aa.pagado, aa.adeudo,p.id_plantel,IF(LOCATE('FAST', p.plantel) > 0,2,1) AS escuela, m.id_maestro, m2.id_maestro AS id_maestro_2,
      CONCAT(al.nombre, ' ', al.apellido_paterno, ' ', IFNULL(al.apellido_materno,'')) AS nombre FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN fyn_proc_becas_prospectos_alumnos b ON b.id = ga.idbecas
      LEFT JOIN alumnos al ON al.id_alumno = ga.id_alumno 
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN maestrosgrupo m   ON m.id_grupo  = g.id_grupo AND m.activo_sn  = 1 AND m.numero_teacher = 1
      LEFT JOIN maestrosgrupo m2  ON m2.id_grupo = g.id_grupo AND m2.activo_sn = 1 AND m2.numero_teacher = 2
      WHERE g.id_ciclo IN (?,?)
      AND b.id_beca = 4
      AND b.tipo_registro = 4;`, [ ciclosSiguienteInbi, cicloSiguienteFast ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res);
    });
  });
};


kpi.kpiInscripcionesGeneral = ( id_ciclo ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel, ciclo, aa.pagado, g.grupo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1;`,[id_ciclo,id_ciclo], (err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res);
    });
  });
};


/****************************************/
/*         KPI GENERAL TEACHER          */
/****************************************/
kpi.getTeachersAll = ( cicloActualInbi, cicloActualFast ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DISTINCT t.id AS id_maestro, t.nombre_completo FROM maestrosgrupo m
      LEFT JOIN grupos g ON g.id_grupo = m.id_grupo
      LEFT JOIN fyn_cat_trabajadores t ON t.id = m.id_maestro
      WHERE g.id_ciclo IN (?,?);`, [ cicloActualInbi, cicloActualFast ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res);
    });
  });
};


/****************************************/
/****************************************/

kpi.kpiInscripcionesAlumnosCicloRango = (ciclo, fechaini, fechafin) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 00:00:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1;`,[ciclo,fechaini,fechafin,ciclo], (err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res);
    });
  });
};


/***/

kpi.kpiInscripcionesAlumnosCicloDiaFAST = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1;`,[data.ciclo,data.fechaini,data.fechaini,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

kpi.kpiInscripcionesAlumnosCicloDiaINBI = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1;`,[data.ciclo,data.fechaini,data.fechaini,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

/***/

kpi.kpiInscripcionesAlumnosRangoFAST = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY alumno;`,[data.fechaini,data.fechafin,data.fechaini], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

kpi.kpiInscripcionesAlumnosRangoINBI = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY alumno;`,[data.fechaini,data.fechafin,data.fechaini], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

/***/

kpi.kpiInscripcionesAlumnosDiaFAST = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY alumno;`,[data.fechaini,data.fechaini,data.fechaini], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

kpi.kpiInscripcionesAlumnosDiaINBI = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY alumno;`,[data.fechaini,data.fechaini,data.fechaini], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

/************************************************************************************/
/************************************************************************************/
/************************************************************************************/

kpi.kpiInscripcionesTotalFAST = (actual,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, g.id_plantel, p.plantel, count(DISTINCT ga.id_alumno) AS total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY plantel;`,[actual,actual], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

kpi.kpiInscripcionesTotalINBI = (actual,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, g.id_plantel, p.plantel, count(DISTINCT ga.id_alumno) AS total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY plantel;`,[actual,actual], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

kpi.kpiInscripcionesCicloDiaFAST = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, g.id_plantel, p.plantel, count(DISTINCT ga.id_alumno) AS total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY plantel;`,[data.ciclo,data.fechaini,data.fechaini,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

kpi.kpiInscripcionesCicloDiaINBI = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, g.id_plantel, p.plantel, count(DISTINCT ga.id_alumno) AS total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY plantel;`,[data.ciclo,data.fechaini,data.fechaini,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

kpi.kpiInscripcionesCicloRangoFAST = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, g.id_plantel, p.plantel, count(DISTINCT ga.id_alumno) AS total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 00:00:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY plantel;`,[data.ciclo,data.fechaini,data.fechafin,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

kpi.kpiInscripcionesCicloRangoINBI = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, g.id_plantel, p.plantel, count(DISTINCT ga.id_alumno) AS total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 00:00:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY plantel;`,[data.ciclo,data.fechaini,data.fechafin,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

// Obtener todos los planteles
// Obtener todos los planteles
kpi.getPlanteles = result => {
  sqlERP.query(`SELECT * FROM planteles  WHERE activo_sn = 1`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

// Obtener todos los ciclos
kpi.getCiclos = () => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM ciclos WHERE ciclo_abierto_sn = 1
      AND ciclo NOT LIKE '%FE%'
      AND ciclo NOT LIKE '%CEAA%'
      AND ciclo NOT LIKE '%INVER%'
      AND ciclo NOT LIKE '%EXCI%'
      AND id_ciclo_relacionado IS NOT NULL
      ORDER BY fecha_inicio_ciclo 
      LIMIT 8;`, (err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res);
    });
  })
};


kpi.getCiclosExci = result => {
  sqlERP.query(`SELECT * FROM ciclos WHERE ciclo_abierto_sn = 1
    AND ciclo LIKE '%EXCI%'
    ORDER BY fecha_inicio_ciclo 
    LIMIT 6;`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};


kpi.getCiclosInduccion = result => {
  sqlERP.query(`SELECT * FROM ciclos WHERE activo_sn = 1 AND ciclo LIKE '%INDUC%' AND ciclo NOT LIKE '%FE%';`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};


kpi.getCantActual = (id, idfast, result) => {
  sqlERP.query(`SELECT DISTINCT a.*,p.plantel FROM alumnos_grupos_especiales_carga a 
    LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
    LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
    WHERE a.id_ciclo = ?  AND a.adeudo = 0  AND g.grupo NOT LIKE '%INVER%' AND g.grupo NOT LIKE '%CERTI%' 
    AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
    OR a.id_ciclo = ? AND a.adeudo = 0  AND g.grupo NOT LIKE '%INVER%' AND g.grupo NOT LIKE '%CERTI%' 
    AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
    GROUP BY a.id_alumno;`,[id, idfast], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

// Obtener todos los ciclos
kpi.getCantSiguiente = (id,idfast,result) => {
  sqlERP.query(`SELECT  DISTINCT a.*,p.plantel FROM alumnos_grupos_especiales_carga a 
  LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
  LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
  LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
  WHERE a.id_ciclo = ?  AND a.adeudo = 0  OR 
  a.id_ciclo = ?  AND a.adeudo = 0  GROUP BY a.id_alumno;`,[id,idfast], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

kpi.getCantSiguienteAvance = (id,idfast,result) => {
  sqlERP.query(`SELECT  DISTINCT a.*,p.plantel FROM alumnos_grupos_especiales_carga a 
  LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
  LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
  LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
  WHERE a.id_ciclo = ?  AND a.adeudo = 0  AND ga.fecha_alta BETWEEN CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 01:00:00") AND CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 23:59:59") OR 
  a.id_ciclo = ?  AND a.adeudo = 0 AND ga.fecha_alta BETWEEN CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 01:00:00") AND CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 23:59:59") GROUP BY a.id_alumno;`,[id,idfast], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

// TEEEEEEEEEEEEEEEEENNNNNNNNNNNNNNNSSSSSSSSSSSSS
kpi.getCantActualTeens = (id,idfast,result) => {
  sqlERP.query(`SELECT DISTINCT a.*,p.plantel FROM alumnos_grupos_especiales_carga a 
    LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
    LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
    WHERE a.id_ciclo = ?  AND a.adeudo = 0  AND g.grupo NOT LIKE '%INVER%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo LIKE '%TEENS%'
    AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
    OR a.id_ciclo = ? AND a.adeudo = 0  AND g.grupo NOT LIKE '%INVER%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo LIKE '%TEENS%'
    AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
    GROUP BY a.id_alumno;`,[id,idfast], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

// Obtener todos los ciclos
kpi.getCantSiguienteTeens = (id,idfast,result) => {
  sqlERP.query(`SELECT  DISTINCT a.*,p.plantel FROM alumnos_grupos_especiales_carga a 
    LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
    LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
    LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
    WHERE a.id_ciclo = ?  AND a.adeudo = 0 AND g.grupo LIKE '%TEENS%' OR 
    a.id_ciclo = ?  AND a.adeudo = 0 AND g.grupo LIKE '%TEENS%' GROUP BY a.id_alumno;`,[id,idfast], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

/*****************************************************************************************************/
kpi.kpiInscripcionesVendedoraCicloGeneral = ( cicloActual ) => {
  return new Promise(( resolve, reject ) => {
    sqlERP.query(`SELECT DISTINCT u.nombre_completo, adeudo, pagado, ga.id_alumno, a.matricula, CONCAT(a.nombre, ' ', a.apellido_paterno, IFNULL(a.apellido_materno,'')) AS alumno
        FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        LEFT JOIN alumnos al ON al.id_alumno = ga.id_alumno
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo < ?) ;`,[cicloActual,cicloActual], (err, cicloActual) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(cicloActual);
    });
  })
};

kpi.kpiInscripcionesVendedoraCicloGeneralGroup = ( cicloActual ) => {
  return new Promise(( resolve, reject ) => {
    sqlERP.query(`SELECT DISTINCT u.nombre_completo, adeudo, pagado, ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo < ?) GROUP BY u.nombre_completo ;`,[cicloActual,cicloActual], (err, cicloActual) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(cicloActual);
    });
  })
};

kpi.kpiInscripcionesVendedoraCicloRangoGeneral = (ciclo, fechaini, fechafin) => {
  return new Promise(( resolve, reject ) => {
    sqlERP.query(`SELECT DISTINCT u.nombre_completo, aa.adeudo, aa.pagado, ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo = ? AND DATE(ga.fecha_alta) BETWEEN ? AND ?
        AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo < ?) ;`,[ciclo,fechaini,fechafin,ciclo], (err, cicloActual) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(cicloActual);
    });
  })
};

kpi.kpiInscripcionesVendedoraCicloRangoGeneralGroup = (ciclo, fechaini, fechafin) => {
  return new Promise(( resolve, reject ) => {
    sqlERP.query(`SELECT DISTINCT u.nombre_completo, aa.adeudo, aa.pagado FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo = ? AND DATE(ga.fecha_alta) BETWEEN ? AND ?
        AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo < ?) GROUP BY u.nombre_completo ;`,[ciclo,fechaini,fechafin,ciclo], (err, cicloActual) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(cicloActual);
    });
  })
};

kpi.kpiInscripcionesVendedoraRangoFAST = (fechaini, fechafin) => {
  return new Promise(( resolve, reject ) => {
    sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo, plantel, aa.adeudo, aa.pagado, ciclo FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
        LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
        AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) GROUP BY alumno;`,[fechaini,fechafin,fechaini], (err, cicloActual) => {
      if (err) {
        return reject({message: err.sqlMessage });
      }
      resolve(cicloActual);
    });
  })
};

kpi.kpiInscripcionesVendedoraRangoFASTGroup = (fechaini, fechafin) => {
  return new Promise(( resolve, reject ) => {
    sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo, plantel, aa.adeudo, aa.pagado, ciclo FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
        LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
        AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) GROUP BY u.nombre_completo;`,[fechaini,fechafin,fechaini], (err, cicloActual) => {
      if (err) {
        return reject({message: err.sqlMessage });
      }
      resolve(cicloActual);
    });
  })
};

kpi.kpiInscripcionesVendedoraRangoINBI = (fechaini, fechafin) => {
  return new Promise(( resolve, reject ) => {
    sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo, plantel, aa.adeudo, aa.pagado, ciclo FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
        LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
        AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) GROUP BY alumno;`,[fechaini,fechafin,fechaini], (err, cicloActual) => {
      if (err) {
        return reject({message: err.sqlMessage });
      }
      resolve(cicloActual);
    });
  })
};


kpi.kpiInscripcionesVendedoraRangoINBIGroup = (fechaini, fechafin) => {
  return new Promise(( resolve, reject ) => {
    sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo, plantel, aa.adeudo, aa.pagado, ciclo FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
        LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
        AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) GROUP BY u.nombre_completo;`,[fechaini,fechafin,fechaini], (err, cicloActual) => {
      if (err) {
        return reject({message: err.sqlMessage });
      }
      resolve(cicloActual);
    });
  })
};

kpi.kpiInscripcionesVendedoraDiaFAST = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY alumno;`,[data.fechaini,data.fechaini,data.fechaini], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

kpi.kpiInscripcionesVendedoraDiaINBI = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY alumno;`,[data.fechaini,data.fechaini,data.fechaini], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};



kpi.TotalInscritosPorCiclo = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT ga.id_alumno, aa.adeudo, aa.pagado,
      (SELECT ga2.id_grupo FROM gruposalumnos ga2
      LEFT JOIN grupos g2 ON g2.id_grupo = ga2.id_grupo
      WHERE g2.grupo LIKE '%INDUCC%'
      AND ga2.id_alumno = ga.id_alumno LIMIT 1) AS id_grupo
      FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = g.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ? );
      `,[ciclo, ciclo],(err,result)=>{
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(result)
    });
  })
}

kpi.TotalInscritosPorCicloTEENS = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`
      SELECT  count(DISTINCT ga.id_alumno) as total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo LIKE '%TEENS%'
      AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo LIKE '%TEENS%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1;
      `,
      [ciclo, ciclo],(err,result)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(result[0].total)
      });
  })
}


kpi.acumuladoPorSucursal = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`
        SELECT DISTINCT p.id_plantel, plantel, count(DISTINCT ga.id_alumno) as acumulado FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = g.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 0
        group by plantel  ORDER BY acumulado;`,
      [ciclo, ciclo],(err,results)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(results)
      });
  })
}

kpi.acumuladoPorSucursalAll = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT DISTINCT ga.id_alumno, p.id_plantel, aa.pagado FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = g.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo < ?);`,
      [ciclo, ciclo],(err,results)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(results)
      });
  })
}

kpi.acumuladoPorSucursalTeens = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`
        SELECT DISTINCT plantel, count(DISTINCT ga.id_alumno) as acumulado FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'  AND g.grupo LIKE '%TEENS%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'  AND g.grupo LIKE '%TEENS%'
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 AND ga.fecha_alta <  CONCAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), " 23:59:00")
        group by plantel  ORDER BY acumulado;
      `,
      [ciclo, ciclo],(err,results)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(results)
      });
  })
}

/*********************************************************************************************/
/*********************************************************************************************/

kpi.acumuladoPorSucursalAyer = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`
        SELECT DISTINCT plantel, count(DISTINCT ga.id_alumno) as acumulado FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%'
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 
        AND ga.fecha_alta BETWEEN CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 00:00:00") AND  CONCAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), " 23:59:00")
        group by plantel ORDER BY acumulado;
      `,
      [ciclo, ciclo],(err,results)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(results)
      });
  })
}


kpi.acumuladoPorSucursalAyerAll = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`
        SELECT DISTINCT plantel, count(DISTINCT ga.id_alumno) as acumulado FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%'
        AND g.id_ciclo < ?)
        AND ga.fecha_alta BETWEEN CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 00:00:00") AND  CONCAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), " 23:59:00")
        group by plantel ORDER BY acumulado;
      `,
      [ciclo, ciclo],(err,results)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(results)
      });
  })
}

kpi.acumuladoPorSucursalAyerTeens = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`
        SELECT DISTINCT plantel, count(DISTINCT ga.id_alumno) as acumulado FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%' AND g.grupo LIKE '%TEENS%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%' AND g.grupo LIKE '%TEENS%'
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 
        AND ga.fecha_alta BETWEEN CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 00:00:00") AND  CONCAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), " 23:59:00")
        group by plantel ORDER BY acumulado;
      `,
      [ciclo, ciclo],(err,results)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(results)
      });
  })
}

kpi.semanaPorSucursal = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`
        SELECT  plantel, count(DISTINCT ga.id_alumno) as cantidad FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 AND ga.fecha_alta BETWEEN CONCAT(DATE_SUB(CURDATE(),INTERVAL weekday(CURDATE()) DAY), " 00:00:00") AND  CONCAT(ADDDATE(CURDATE(), INTERVAL (6 - weekday(CURDATE())) DAY), " 23:59:00")
        group by plantel ORDER BY cantidad;
      `,
      [ciclo, ciclo],(err,results)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(results)
      });
  })
}


kpi.ayerPorSucursal = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`
        SELECT DISTINCT plantel, count(DISTINCT ga.id_alumno) as cantidad FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 
        AND ga.fecha_alta BETWEEN CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 00:00:00") AND  CONCAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), " 23:59:00")
        group by plantel ORDER BY cantidad;
      `,
      [ciclo, ciclo],(err,results)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(results)
      });
  })
}

/*****************************************************************************************************************/
kpi.acumuladoPorVendedoraAyer = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT DISTINCT count(DISTINCT ga.id_alumno) AS total, u.nombre_completo FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%' 
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%' 
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 
        AND ga.fecha_alta BETWEEN CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 00:00:00") AND  CONCAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), " 23:59:00")
        GROUP BY u.id_usuario ORDER BY total;`,[actual,actual], (err, results) => {
      if(err){ return reject({ message: err.sqlMessage }) }

      resolve(results)
    });
  });
};

kpi.acumuladoPorVendedoraAyerTeens = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT DISTINCT count(DISTINCT ga.id_alumno) AS total, u.nombre_completo FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%' AND g.grupo LIKE '%TEENS%' 
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%' AND g.grupo LIKE '%TEENS%' 
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 
        AND ga.fecha_alta BETWEEN CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 00:00:00") AND  CONCAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), " 23:59:00")
        GROUP BY u.id_usuario ORDER BY total;`,[actual,actual], (err, results) => {
      if(err){ return reject({ message: err.sqlMessage }) }

      resolve(results)
    });
  });
};

kpi.kpiInscripcionesVendedoraCiclo = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT count(ga.id_alumno) AS total, u.nombre_completo FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY u.id_usuario ORDER BY total;`,[actual,actual], (err, results) => {
      if(err){ return reject({ message: err.sqlMessage }) }

      resolve(results)
    });
  });
};


kpi.kpiInscripcionesVendedoraCicloTEENS = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT DISTINCT count(DISTINCT ga.id_alumno) AS total, u.nombre_completo FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
        LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo LIKE '%TEENS%' 
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo LIKE '%TEENS%' 
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY u.id_usuario ORDER BY total;`,[actual,actual], (err, results) => {
      if(err){ return reject({ message: err.sqlMessage }) }

      resolve(results)
    });
  });
};

/*****************************************************************************************************/
// RI por grupos

kpi.getGruposActual = (inbi,fast) => {
   return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT  DISTINCT a.id_alumno, g.grupo, g.id_grupo, COUNT(a.id_alumno) AS alumnos_ciclo_actual, g.id_plantel FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.id_ciclo IN (?,?) AND a.id_ciclo IN(?,?)
      AND a.adeudo = 0 
      AND g.activo_sn = 1
      AND g.grupo NOT LIKE '%IND%' AND g.grupo NOT LIKE '%CERT%'
      GROUP BY g.id_grupo;`,[inbi,fast,inbi,fast], (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};

kpi.alumosCicloActual = (inbi,fast) => {
   return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT  DISTINCT a.id_alumno, g.grupo, g.id_grupo FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.id_ciclo IN (?,?) 
      AND a.adeudo = 0 
      AND g.grupo NOT LIKE '%IND%' AND g.grupo NOT LIKE '%CERT%'
      GROUP BY a.id_alumno;`,[inbi,fast], (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};

kpi.alumosCicloSiguiente = (inbi,fast,sigInbi,sigFast) => {
   return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT  DISTINCT a.id_alumno, g.grupo, g.id_grupo, COUNT(a.id_alumno) AS alumnos_siguiente_ciclo FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.id_ciclo IN(?,?) AND a.id_ciclo IN(?,?)
      AND a.adeudo = 0 
      AND g.activo_sn = 1
      AND g.grupo NOT LIKE '%IND%' AND g.grupo NOT LIKE '%CERT%'
      AND a.id_alumno IN (SELECT  DISTINCT a.id_alumno FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.id_ciclo IN(?,?) AND a.id_ciclo IN(?,?)
      AND a.adeudo = 0 
      AND g.activo_sn = 1
      AND g.grupo NOT LIKE '%IND%'  GROUP BY g.id_grupo)
      GROUP BY g.id_grupo;`,[inbi,fast,inbi,fast,sigInbi,sigFast,sigInbi,sigFast], (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};

/********************************************************/
/********************************************************/

kpi.getHorarioClaseFast = ( ciclo ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT DISTINCT hora_inicio FROM grupos g
      LEFT JOIN horarios h ON h.id = g.id_horario 
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      WHERE c.iderp = ${ ciclo } ORDER BY hora_inicio;`, (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};


kpi.getHorarioClaseInbi = ( ciclo ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT DISTINCT hora_inicio FROM grupos g
      LEFT JOIN horarios h ON h.id = g.id_horario 
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      WHERE c.iderp = ${ ciclo } ORDER BY hora_inicio;`, (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};


kpi.getNumberDiaBD = ( ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT (ELT(WEEKDAY(CURDATE()) + 1, 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado', 'domingo')) AS dia;`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res[0])
    });
  });
};


kpi.getNumberDiaEspecificoBD = (fecha) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT (ELT(WEEKDAY(?) + 1, 1, 2, 3,4,5,6,7)) AS diaEspecifico;`,[fecha],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res[0])
    });
  });
};

kpi.getNumberDiaEspecificoBD2 = (fecha) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT (ELT(WEEKDAY(?) + 1, 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado', 'domingo')) AS diaEspecifico;`,[fecha],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res[0])
    });
  });
};

kpi.getNumberDiaBDEspecifico = ( fecha ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT (ELT(WEEKDAY(DATE(?)) + 1, 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado', 'domingo')) AS diaClase;`,[fecha],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res[0])
    });
  });
};

kpi.getClasesFast = ( cicloFast, dia, hora_inicio ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id AS id_grupo,c.id AS id_ciclo, g.nombre AS grupo, c.nombre AS curso, h.hora_inicio, g.id_unidad_negocio, IF(g.id_unidad_negocio = 1, "INBI","FAST") AS escuela,
      IF((ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 2)) = 1,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")),CONCAT(u2.nombre, " ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno, ""))) AS teacher,
      IF((ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 2)) = 1,gt.id_teacher,gt.id_teacher_2) AS id_teacher,
      (ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 2)) AS num_teacher,
      (ELT(WEEKDAY(CURDATE()) + 1, 1, 2, 3, 4, 5, 6, 7)) AS dia
      FROM grupos g
      LEFT JOIN cursos cu         ON cu.id = g.id_curso
      LEFT JOIN frecuencia f      ON f.id = cu.id_frecuencia
      LEFT JOIN horarios h        ON h.id = g.id_horario
      LEFT JOIN ciclos c          ON c.id = g.id_ciclo
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
      LEFT JOIN usuarios u        ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2       ON u2.id = gt.id_teacher_2
      WHERE c.iderp = ? AND ${ dia } = 1 AND hora_inicio = ? AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0;`,[ cicloFast, hora_inicio ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};

kpi.getClasesInbi = ( cicloInbi, dia, hora_inicio ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id AS id_grupo,c.id AS id_ciclo, g.nombre AS grupo, c.nombre AS curso, h.hora_inicio, g.id_unidad_negocio, IF(g.id_unidad_negocio = 1, "INBI","FAST") AS escuela,
      IF((ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 2)) = 1,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")),CONCAT(u2.nombre, " ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno, ""))) AS teacher,
      IF((ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 2)) = 1,gt.id_teacher,gt.id_teacher_2) AS id_teacher,
      (ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 2)) AS num_teacher,
      (ELT(WEEKDAY(CURDATE()) + 1, 1, 2, 3, 4, 5, 6, 7)) AS dia
      FROM grupos g
      LEFT JOIN cursos cu         ON cu.id = g.id_curso
      LEFT JOIN frecuencia f      ON f.id = cu.id_frecuencia
      LEFT JOIN horarios h        ON h.id = g.id_horario
      LEFT JOIN ciclos c          ON c.id = g.id_ciclo
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
      LEFT JOIN usuarios u        ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2       ON u2.id = gt.id_teacher_2
      WHERE c.iderp = ? AND ${ dia } = 1 AND hora_inicio = ? AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0;`,[ cicloInbi, hora_inicio ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};



kpi.getClasesGrabadasFast = ( ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM indicador_clases
      WHERE dia = (ELT(WEEKDAY(CURDATE()) + 1, 1, 2, 3, 4, 5, 6, 7)) AND WEEK(DATE(fecha_creacion)) = WEEK(CURDATE());`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};

kpi.getClasesGrabadasInbi = ( ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM indicador_clases
      WHERE dia = (ELT(WEEKDAY(CURDATE()) + 1, 1, 2, 3, 4, 5, 6, 7)) AND WEEK(DATE(fecha_creacion)) = WEEK(CURDATE());`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};


kpi.updateClaseFast = (e) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`UPDATE indicador_clases SET inicio_tiempo = ?, maestro_asignado = ?, finaliza_tiempo = ?, contenido_correcto = ?, ninguna = ? WHERE idindicador_clases = ?`,
     [e.inicio_tiempo, e.maestro_asignado, e.finaliza_tiempo, e.contenido_correcto, e.ninguna, e.idindicador_clases],
      (err, res) => {
        if (err) { reject(err); return; }
        if (res.affectedRows == 0) {
          reject(err)
          return;
        }
        resolve(res);
    })
  })
}

kpi.addClaseFast = (c) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`INSERT INTO indicador_clases(id_grupo, hora_inicio, id_unidad_negocio, id_teacher, num_teacher, dia, id_ciclo, inicio_tiempo, maestro_asignado, finaliza_tiempo, contenido_correcto, ninguna ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)`, 
      [c.id_grupo, c.hora_inicio, c.id_unidad_negocio, c.id_teacher, c.num_teacher, c.dia, c.id_ciclo, c.inicio_tiempo, c.maestro_asignado, c.finaliza_tiempo, c.contenido_correcto, c.ninguna ],
      (err, res) => {
        if (err) { reject(err); return; }
        resolve({ id: res.insertId, ...c});
      }
    )
  })
}


kpi.updateClaseInbi = (e) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`UPDATE indicador_clases SET inicio_tiempo = ?, maestro_asignado = ?, finaliza_tiempo = ?, contenido_correcto = ?, ninguna = ? WHERE idindicador_clases = ?`,
     [e.inicio_tiempo, e.maestro_asignado, e.finaliza_tiempo, e.contenido_correcto, e.ninguna, e.idindicador_clases],
      (err, res) => {
        if (err) { reject(err); return; }
        if (res.affectedRows == 0) {
          reject(err)
          return;
        }
        resolve(res);
    })
  })
}

kpi.addClaseInbi = (c) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`INSERT INTO indicador_clases(id_grupo, hora_inicio, id_unidad_negocio, id_teacher, num_teacher, dia, id_ciclo, inicio_tiempo, maestro_asignado, finaliza_tiempo, contenido_correcto,ninguna ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)`, 
      [c.id_grupo, c.hora_inicio, c.id_unidad_negocio, c.id_teacher, c.num_teacher, c.dia, c.id_ciclo, c.inicio_tiempo, c.maestro_asignado, c.finaliza_tiempo, c.contenido_correcto, c.ninguna ],
      (err, res) => {
        if (err) { reject(err); return; }
        resolve({ id: res.insertId, ...c});
      }
    )
  })
}


/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/

kpi.getReporteClasesBuenasFast = ( fecha, fecha2, cicloFast ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT i.* FROM indicador_clases i 
      LEFT JOIN ciclos c ON c.id = i.id_ciclo
      WHERE c.iderp = ? AND i.inicio_tiempo = 1 AND i.maestro_asignado = 1 AND i.contenido_correcto = 1 AND i.finaliza_tiempo = 1
      AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59");`,[ cicloFast, fecha, fecha2 ],(err, res) => {
      if(err){
        reject(err);
        return
      }
      resolve(res)
    });
  });
};

kpi.getReporteClasesBuenasInbi = ( fecha, fecha2, cicloInbi ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT i.* FROM indicador_clases i 
      LEFT JOIN ciclos c ON c.id = i.id_ciclo
      WHERE c.iderp = ? AND i.inicio_tiempo = 1 AND i.maestro_asignado = 1 AND i.contenido_correcto = 1 AND i.finaliza_tiempo = 1
      AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59");`,[ cicloInbi, fecha, fecha2 ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};


kpi.getReporteClasesTotalesFast = ( cicloFast, dia, group, select ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT ${select} FROM grupos g
      LEFT JOIN cursos cu         ON cu.id = g.id_curso
      LEFT JOIN frecuencia f      ON f.id = cu.id_frecuencia
      LEFT JOIN horarios h        ON h.id = g.id_horario
      LEFT JOIN ciclos c          ON c.id = g.id_ciclo
      WHERE c.iderp = ?
      AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0 AND g.nombre NOT LIKE '%certi%' AND g.nombre NOT LIKE '%indu%' ${ dia }
      ${ group };`,[ cicloFast ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};

kpi.getReporteClasesTotalesInbi = ( cicloInbi, dia, group, select ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT ${select} FROM grupos g
      LEFT JOIN cursos cu         ON cu.id = g.id_curso
      LEFT JOIN frecuencia f      ON f.id = cu.id_frecuencia
      LEFT JOIN horarios h        ON h.id = g.id_horario
      LEFT JOIN ciclos c          ON c.id = g.id_ciclo
      WHERE c.iderp = ?
      AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0 AND g.nombre NOT LIKE '%certi%' AND g.nombre NOT LIKE '%indu%' ${ dia }
      ${ group };`,[ cicloInbi ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};


kpi.getReporteClasesMalasFast = ( fecha, fecha2, cicloFast ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT i.* FROM indicador_clases i 
      LEFT JOIN ciclos c ON c.id = i.id_ciclo
      WHERE c.iderp = ${ cicloFast } AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.ninguna = 1
      OR c.iderp = ${ cicloFast } AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.inicio_tiempo = 0
      OR c.iderp = ${ cicloFast } AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.maestro_asignado = 0
      OR c.iderp = ${ cicloFast } AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.contenido_correcto = 0
      OR c.iderp = ${ cicloFast } AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.finaliza_tiempo = 0;`,
      [ fecha, fecha2, fecha, fecha2, fecha, fecha2, fecha, fecha2, fecha, fecha2 ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};

kpi.getReporteclasesMalasInbi = ( fecha, fecha2, cicloInbi ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT i.* FROM indicador_clases i 
      LEFT JOIN ciclos c ON c.id = i.id_ciclo
      WHERE c.iderp = ${ cicloInbi } AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.ninguna = 1
      OR c.iderp = ${ cicloInbi } AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.inicio_tiempo = 0
      OR c.iderp = ${ cicloInbi } AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.maestro_asignado = 0
      OR c.iderp = ${ cicloInbi } AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.contenido_correcto = 0
      OR c.iderp = ${ cicloInbi } AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.finaliza_tiempo = 0;`,
      [ fecha, fecha2, fecha, fecha2, fecha, fecha2, fecha, fecha2, fecha, fecha2 ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};


/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/

kpi.getTeacherFAST = ( cicloFast, num_teacher ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT DISTINCT IF(${ num_teacher } = 1,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")),CONCAT(u2.nombre, " ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno, ""))) AS teacher,
      IF(${ num_teacher } = 1,gt.id_teacher,gt.id_teacher_2) AS id_teacher,
      IF(${ num_teacher } = 1,u.email,u2.email) AS email FROM grupos g
      LEFT JOIN grupo_teachers gt ON gt.id_grupo 
      LEFT JOIN usuarios u        ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2       ON u2.id = gt.id_teacher_2
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      WHERE c.iderp = ${ cicloFast } AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0 order by teacher;`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};

kpi.getTeacherINBI = ( cicloInbi, num_teacher ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT DISTINCT IF(${ num_teacher } = 1,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")),CONCAT(u2.nombre, " ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno, ""))) AS teacher,
      IF(${ num_teacher } = 1,gt.id_teacher,gt.id_teacher_2) AS id_teacher,
      IF(${ num_teacher } = 1,u.email,u2.email) AS email FROM grupos g
      LEFT JOIN grupo_teachers gt ON gt.id_grupo 
      LEFT JOIN usuarios u        ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2       ON u2.id = gt.id_teacher_2
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      WHERE c.iderp = ${ cicloInbi } AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0 order by teacher;`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};


/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/

kpi.getReporteClasesBuenasFastTeacher = ( fecha, fecha2, cicloFast, email ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT i.* FROM indicador_clases i 
      LEFT JOIN ciclos c ON c.id = i.id_ciclo
      LEFT JOIN usuarios u ON u.id = i.id_teacher
      WHERE c.iderp = ? AND i.inicio_tiempo = 1 AND i.maestro_asignado = 1 AND i.contenido_correcto = 1 AND i.finaliza_tiempo = 1
      AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND u.email = ?;`,[ cicloFast, fecha, fecha2, email ],(err, res) => {
      if(err){
        reject(err);
        return
      }
      resolve(res)
    });
  });
};

kpi.getReporteClasesBuenasInbiTeacher = ( fecha, fecha2, cicloInbi, email ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT i.* FROM indicador_clases i 
      LEFT JOIN ciclos c ON c.id = i.id_ciclo
      LEFT JOIN usuarios u ON u.id = i.id_teacher
      WHERE c.iderp = ? AND i.inicio_tiempo = 1 AND i.maestro_asignado = 1 AND i.contenido_correcto = 1 AND i.finaliza_tiempo = 1
      AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND u.email = ?;`,[ cicloInbi, fecha, fecha2, email ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};


kpi.getReporteClasesTotalesFastTeacher = ( diaSemana, cicloFast, email, number_teacer, number_teacer2  ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id AS id_grupo,c.id AS id_ciclo, g.nombre AS grupo, c.nombre AS curso, h.hora_inicio, g.id_unidad_negocio FROM grupos g
      LEFT JOIN cursos cu         ON cu.id = g.id_curso
      LEFT JOIN frecuencia f      ON f.id = cu.id_frecuencia
      LEFT JOIN horarios h        ON h.id = g.id_horario
      LEFT JOIN ciclos c          ON c.id = g.id_ciclo
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
      WHERE c.iderp = ${ cicloFast } AND ${ diaSemana } = 1
      AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0 AND u.email = ? AND ${ number_teacer } = 1
      OR c.iderp = ${ cicloFast } AND ${ diaSemana } = 1
      AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0 AND u2.email = ? AND ${ number_teacer2 } = 2;`,[ email, email ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};

kpi.getReporteClasesTotalesInbiTeacher = ( diaSemana, cicloInbi, email, number_teacer, number_teacer2  ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id AS id_grupo,c.id AS id_ciclo, g.nombre AS grupo, c.nombre AS curso, h.hora_inicio, g.id_unidad_negocio FROM grupos g
      LEFT JOIN cursos cu         ON cu.id = g.id_curso
      LEFT JOIN frecuencia f      ON f.id = cu.id_frecuencia
      LEFT JOIN horarios h        ON h.id = g.id_horario
      LEFT JOIN ciclos c          ON c.id = g.id_ciclo
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
      WHERE c.iderp = ${ cicloInbi } AND ${ diaSemana } = 1
      AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0 AND u.email = ? AND ${ number_teacer } = 1
      OR c.iderp = ${ cicloInbi } AND ${ diaSemana } = 1
      AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0 AND u2.email = ? AND ${ number_teacer2 } = 2;`,[ email, email ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};


kpi.getReporteClasesMalasFastTeacher = ( fecha, fecha2, cicloFast, email ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT i.* FROM indicador_clases i 
      LEFT JOIN ciclos c ON c.id = i.id_ciclo
      LEFT JOIN usuarios u ON u.id = i.id_teacher
      WHERE c.iderp = ${ cicloFast }  AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.ninguna = 1 AND u.email = ?
      OR c.iderp = ${ cicloFast }  AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.inicio_tiempo = 0 AND u.email = ?
      OR c.iderp = ${ cicloFast }  AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.maestro_asignado = 0 AND u.email = ?
      OR c.iderp = ${ cicloFast }  AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.contenido_correcto = 0 AND u.email = ?
      OR c.iderp = ${ cicloFast }  AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.finaliza_tiempo = 0 AND u.email = ?;`,
      [ fecha, fecha2, email, fecha, fecha2, email, fecha, fecha2, email, fecha, fecha2, email, fecha, fecha2, email ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};

kpi.getReporteclasesMalasInbiTeacher = ( fecha, fecha2, cicloInbi, email ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT i.* FROM indicador_clases i 
      LEFT JOIN ciclos c ON c.id = i.id_ciclo
      LEFT JOIN usuarios u ON u.id = i.id_teacher
      WHERE c.iderp = ${ cicloInbi } AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.ninguna = 1 AND u.email = ?
      OR c.iderp = ${ cicloInbi } AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.inicio_tiempo = 0 AND u.email = ?
      OR c.iderp = ${ cicloInbi } AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.maestro_asignado = 0 AND u.email = ?
      OR c.iderp = ${ cicloInbi } AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.contenido_correcto = 0 AND u.email = ?
      OR c.iderp = ${ cicloInbi } AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.finaliza_tiempo = 0 AND u.email = ?;`,
      [ fecha, fecha2, email, fecha, fecha2, email, fecha, fecha2, email, fecha, fecha2, email, fecha, fecha2, email ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};

/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/

kpi.getTeachersFast = ( cicloFast ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT *, CONCAT(nombre, ' ', apellido_paterno, ' ', IFNULL(apellido_materno,'')) AS nombre_completo FROM usuarios WHERE id IN (SELECT gt.id_teacher FROM grupo_teachers gt
      LEFT JOIN grupos g ON g.id = gt.id_grupo
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      WHERE g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0
      AND g.nombre NOT LIKE '%exci%' AND g.nombre NOT LIKE '%indu%' AND g.nombre NOT LIKE '%certi%'
      AND c.iderp = ?)
      OR id IN (SELECT gt.id_teacher_2 FROM grupo_teachers gt
      LEFT JOIN grupos g ON g.id = gt.id_grupo
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      WHERE g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0
      AND g.nombre NOT LIKE '%exci%' AND g.nombre NOT LIKE '%indu%' AND g.nombre NOT LIKE '%certi%'
      AND c.iderp = ?);`,[ cicloFast, cicloFast ],(err, res) => {
      if(err){
        reject(err);
        return
      }
      resolve(res)
    });
  });
};


kpi.getTotalClasesFAST = ( dia, cicloFast, fecha ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id AS id_grupo,c.iderp AS id_ciclo, g.nombre AS grupo, c.nombre AS curso, h.hora_inicio, g.id_unidad_negocio,
      IF((ELT(WEEKDAY(DATE(?)) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")),CONCAT(u2.nombre, " ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno, ""))) AS teacher,
      IF((ELT(WEEKDAY(DATE(?)) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,u.email, u2.email) AS email,
      (ELT(WEEKDAY(DATE(?)) + 1, 1, 1, 1, 2, 2, 1, 1)) AS num_teacher,
      (ELT(WEEKDAY(DATE(?)) + 1, 1, 2, 3, 4, 5, 6, 7)) AS dia
      FROM grupos g
      LEFT JOIN cursos cu         ON cu.id = g.id_curso
      LEFT JOIN frecuencia f      ON f.id = cu.id_frecuencia
      LEFT JOIN horarios h        ON h.id = g.id_horario
      LEFT JOIN ciclos c          ON c.id = g.id_ciclo
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
      LEFT JOIN usuarios u        ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2       ON u2.id = gt.id_teacher_2
      WHERE c.iderp = ${ cicloFast }   ${ dia } 
      AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0 AND g.nombre NOT LIKE '%certif%' AND g.nombre NOT LIKE '%indu%'
      AND g.nombre NOT LIKE '%exci%'`,[fecha, fecha, fecha, fecha],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};


kpi.getTotalClasesBuenasFAST = ( fecha, fecha2, cicloFast ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT i.*, u.email FROM indicador_clases i 
      LEFT JOIN ciclos c ON c.id = i.id_ciclo
      LEFT JOIN usuarios u ON u.id = i.id_teacher
      WHERE c.iderp = ? AND i.inicio_tiempo = 1 AND i.maestro_asignado = 1 AND i.contenido_correcto = 1 AND i.finaliza_tiempo = 1
      AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59");`,[ cicloFast, fecha, fecha2 ],(err, res) => {
      if(err){
        reject(err);
        return
      }
      resolve(res)
    });
  });
};

kpi.getTotalClasesMalasFAST = ( fecha, fecha2, cicloFast ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT i.*, u.email FROM indicador_clases i 
      LEFT JOIN ciclos c ON c.id = i.id_ciclo
      LEFT JOIN usuarios u ON u.id = i.id_teacher
      WHERE c.iderp = ${ cicloFast }  AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.ninguna = 1
      OR c.iderp = ${ cicloFast }  AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.inicio_tiempo = 0
      OR c.iderp = ${ cicloFast }  AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.maestro_asignado = 0
      OR c.iderp = ${ cicloFast }  AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.contenido_correcto = 0
      OR c.iderp = ${ cicloFast }  AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.finaliza_tiempo = 0;`,
      [ fecha, fecha2, fecha, fecha2, fecha, fecha2, fecha, fecha2, fecha, fecha2 ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};

kpi.getTeachersInbi = ( cicloInbi ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT *, CONCAT(nombre, ' ', apellido_paterno, ' ', IFNULL(apellido_materno,'')) AS nombre_completo FROM usuarios WHERE id IN (SELECT gt.id_teacher FROM grupo_teachers gt
      LEFT JOIN grupos g ON g.id = gt.id_grupo
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      WHERE g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0
      AND g.nombre NOT LIKE '%exci%' AND g.nombre NOT LIKE '%indu%' AND g.nombre NOT LIKE '%certi%'
      AND c.iderp = ?)
      OR id IN (SELECT gt.id_teacher_2 FROM grupo_teachers gt
      LEFT JOIN grupos g ON g.id = gt.id_grupo
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      WHERE g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0
      AND g.nombre NOT LIKE '%exci%' AND g.nombre NOT LIKE '%indu%' AND g.nombre NOT LIKE '%certi%'
      AND c.iderp = ?);`,[ cicloInbi, cicloInbi ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};


kpi.getTotalClasesINBI = ( dia, cicloInbi, fecha ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id AS id_grupo,c.iderp AS id_ciclo, g.nombre AS grupo, c.nombre AS curso, h.hora_inicio, g.id_unidad_negocio,
      IF((ELT(WEEKDAY(DATE(?)) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")),CONCAT(u2.nombre, " ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno, ""))) AS teacher,
      IF((ELT(WEEKDAY(DATE(?)) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,u.email, u2.email) AS email,
      (ELT(WEEKDAY(DATE(?)) + 1, 1, 1, 1, 2, 2, 1, 1)) AS num_teacher,
      (ELT(WEEKDAY(DATE(?)) + 1, 1, 2, 3, 4, 5, 6, 7)) AS dia
      FROM grupos g
      LEFT JOIN cursos cu         ON cu.id = g.id_curso
      LEFT JOIN frecuencia f      ON f.id = cu.id_frecuencia
      LEFT JOIN horarios h        ON h.id = g.id_horario
      LEFT JOIN ciclos c          ON c.id = g.id_ciclo
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
      LEFT JOIN usuarios u        ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2       ON u2.id = gt.id_teacher_2
      WHERE c.iderp = ${ cicloInbi }   ${ dia } 
      AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0 AND g.nombre NOT LIKE '%certif%' AND g.nombre NOT LIKE '%indu%'
      AND g.nombre NOT LIKE '%exci%'`,[fecha, fecha, fecha,fecha],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};

kpi.getTotalClasesBuenasINBI = ( fecha, fecha2, cicloInbi ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT i.*, u.email FROM indicador_clases i 
      LEFT JOIN ciclos c ON c.id = i.id_ciclo
      LEFT JOIN usuarios u ON u.id = i.id_teacher
      WHERE c.iderp = ? AND i.inicio_tiempo = 1 AND i.maestro_asignado = 1 AND i.contenido_correcto = 1 AND i.finaliza_tiempo = 1
      AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59");`,[ cicloInbi, fecha, fecha2 ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};


kpi.getTotalClasesMalasINBI = ( fecha, fecha2, cicloInbi ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT i.*, u.email FROM indicador_clases i 
      LEFT JOIN ciclos c ON c.id = i.id_ciclo
      LEFT JOIN usuarios u ON u.id = i.id_teacher
      WHERE c.iderp = ${ cicloInbi }  AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.ninguna = 1
      OR c.iderp = ${ cicloInbi }  AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.inicio_tiempo = 0
      OR c.iderp = ${ cicloInbi }  AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.maestro_asignado = 0
      OR c.iderp = ${ cicloInbi }  AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.contenido_correcto = 0
      OR c.iderp = ${ cicloInbi }  AND i.fecha_creacion BETWEEN  CONCAT(?," 00:00:00")  AND CONCAT(?," 23:59:59") AND i.finaliza_tiempo = 0;`,
      [ fecha, fecha2, fecha, fecha2, fecha, fecha2, fecha, fecha2, fecha, fecha2 ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    });
  });
};

kpi.getFechasCiclo = ( ciclo ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM ciclos WHERE iderp = ${ ciclo }`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res[0])
    });
  });
};



kpi.getCiclosInduccionRango = ( escuela, fecha_inicio_ciclo, fecha_fin_ciclo ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT *, IF(LOCATE('FE',ciclo),2,1) AS unidad_negocio FROM ciclos WHERE activo_sn = 1 AND ciclo LIKE '%INDUC%' ${escuela}
      AND fecha_inicio_ciclo >=  DATE(?)
      AND fecha_fin_ciclo <=  DATE(?) ;`,
        [ fecha_inicio_ciclo, fecha_fin_ciclo ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};


kpi.getCiclosInduccionCicloSemana = ( id_ciclo, semana ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT ga.id_alumno, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, 
      REPLACE(a.telefono,' ','') AS telefono, REPLACE(a.celular,' ','') AS celular, g.id_grupo, g.grupo, g.id_ciclo, c.ciclo, aa.adeudo, aa.pagado,
      REPLACE(tu.telefono,' ','') AS telefonoTutor, REPLACE(tu.celular,' ','') AS celularTutor,
      aa2.pagado AS pago_induccion, IF(LOCATE('FE',ciclo),2,1) AS unidad_negocio,
      (SELECT g2.id_grupo FROM gruposalumnos ga2
      LEFT JOIN grupos g2 ON g2.id_grupo = ga2.id_grupo
      WHERE ga2.id_alumno = ga.id_alumno
      AND g2.id_ciclo <> ? LIMIT 1) AS nuevo_grupo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      LEFT JOIN alumnotutor tu ON tu.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa2 ON aa2.id_alumno = ga.id_alumno AND aa2.id_grupo = ga.id_grupo
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = (SELECT g2.id_grupo FROM gruposalumnos ga2
      LEFT JOIN grupos g2 ON g2.id_grupo = ga2.id_grupo
      WHERE ga2.id_alumno = ga.id_alumno
      AND g2.id_ciclo <> ? LIMIT 1)
      WHERE g.id_ciclo = ?
      AND g.grupo LIKE '%S?%';`,
        [ id_ciclo, id_ciclo, id_ciclo, semana ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

kpi.getUsuariosxId = ( id_usuarios ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT id_usuario, nombre_completo FROM usuarios WHERE id_usuario IN ( ? );`, [ id_usuarios ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};


kpi.getNuevasMatriculas = ( fechaini, fechafin ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT a.matricula, ga.id_alumno, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, g.grupo, ga.fecha_alta, g.id_grupo, uf.id_grupo, uf.fecha_pago, p.plantel,
      a.id_usuario_ultimo_cambio, u.nombre_completo AS vendedora, IF(LOCATE('FAST', g.grupo)>0,2,1) AS unidad_negocio, aa.pagado, aa.adeudo, 
      IF(LOCATE('INDUC',g.grupo)>0,1,0) AS induccion, u.activo_sn
      FROM gruposalumnos ga
      LEFT JOIN (SELECT id_alumno, id_grupo, MIN(fecha_pago) AS primer_fecha 
      FROM ingresos
      GROUP by id_alumno) ng ON ng.id_alumno = ga.id_alumno AND ng.id_grupo = ga.id_grupo
      LEFT JOIN (SELECT id_alumno, id_grupo, MAX(fecha_pago) AS fecha_pago
      FROM ingresos 
      GROUP by id_alumno, id_grupo) uf ON uf.id_alumno = ng.id_alumno AND uf.id_grupo = ng.id_grupo
      LEFT JOIN grupos g ON g.id_grupo = ng.id_grupo
      LEFT JOIN alumnos a ON a.id_alumno = ng.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ng.id_alumno AND ng.id_grupo = aa.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE  DATE(ng.primer_fecha) BETWEEN ? AND ?
      AND DATE(uf.fecha_pago) BETWEEN ? AND ?
      ORDER BY ga.fecha_alta ;`, [ fechaini, fechafin, fechaini, fechafin ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

// kpi.getNuevasMatriculas = ( fechaini, fechafin ) => {
//   return new Promise((resolve, reject) => {
//     sqlERP.query(`SELECT a.matricula, ga.id_alumno, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, g.grupo, ga.fecha_alta, p.plantel,
//       a.id_usuario_ultimo_cambio, u.nombre_completo AS vendedora, IF(LOCATE('FAST', g.grupo)>0,2,1) AS unidad_negocio, aa.pagado, aa.adeudo, IF(LOCATE('INDUC',g.grupo)>0,1,0) AS induccion, u.activo_sn
//       FROM gruposalumnos ga
//       LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
//       LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
//       LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
//       LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = a.id_alumno AND ga.id_grupo = aa.id_grupo
//       LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
//       WHERE ga.id_alumno IN (SELECT id_alumno FROM alumnos WHERE DATE(fecha_alta) BETWEEN ? AND ?)
//       ORDER BY ga.fecha_alta ;
//       `, [ fechaini, fechafin ],(err, res) => {
//       if (err) { return reject({ message: err.sqlMessage }); }
//       resolve(res);
//     });
//   });
// };


kpi.getMatriculasAll = ( idNis, fechaini, fechafin ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT i.id_alumno, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, p.id_plantel, p.plantel,
      a.id_usuario_ultimo_cambio, u.nombre_completo AS vendedora, IF(LOCATE('FAST', g.grupo)>0,2,1) AS unidad_negocio, SUM(i.monto_pagado) AS pagado, aa.adeudo, u.activo_sn
      FROM ingresos i
      LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
      LEFT JOIN alumnos a ON a.id_alumno = i.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = i.id_alumno AND aa.id_grupo = i.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE i.id_alumno NOT IN(?) 
      AND DATE( i.fecha_pago ) BETWEEN ? AND ?
      AND aa.pagado > 0
      GROUP BY i.id_alumno;      `, [ idNis, fechaini, fechafin ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};



kpi.getCicloAnterior = ( fechaini, ciclo ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT *, REPLACE(ciclo,'CICLO ','') AS nombreCicloAnterior, REPLACE(?,'CICLO ','') AS nombreCicloActual  FROM ciclos 
      WHERE DATE_SUB(?,INTERVAL 1 day) BETWEEN fecha_inicio_ciclo AND fecha_fin_ciclo
      AND ciclo LIKE '%CICLO%'
      AND ciclo NOT LIKE '%FE%';`, [ ciclo, fechaini ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res[0]);
    });
  });
};


kpi.getAlumnosInducción = ( nombreCicloAnterior, nombreCicloActual ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT ga.id_alumno,g.grupo,a.id_usuario_ultimo_cambio, u.nombre_completo AS vendedora, IF(LOCATE('FAST', g.grupo)>0,2,1) AS unidad_negocio,
      CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, u.activo_sn,
      REPLACE(a.telefono,' ','') AS telefono, REPLACE(a.celular,' ','') AS celular,
      REPLACE(tu.telefono,' ','') AS telefonoTutor, REPLACE(tu.celular,' ','') AS celularTutor,
      aa.pagado, aa.adeudo, IF(LOCATE('INDUC',g.grupo)>0,1,0) AS induccion, a.nombre, a.matricula, p.plantel FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnotutor tu ON tu.id_alumno = ga.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = a.id_alumno AND ga.id_grupo = aa.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE g.grupo LIKE '%INDUCCION ${ nombreCicloAnterior }%'
      AND g.grupo NOT LIKE '%S1%'
      OR g.grupo LIKE '%INDUCCION ${ nombreCicloActual }%'
      AND g.grupo LIKE '%S1%';`, [ nombreCicloAnterior, nombreCicloActual ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};


kpi.getAlumnosGrupoNormal = ( idAlumnos ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT ga.id_alumno,g.grupo,a.id_usuario_ultimo_cambio, u.nombre_completo AS vendedora, IF(LOCATE('FAST', g.grupo)>0,2,1) AS unidad_negocio,
      aa.pagado, aa.adeudo, IF(LOCATE('INDUC',g.grupo)>0,1,0) AS induccion, a.nombre, a.matricula, p.plantel, u.activo_sn FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = a.id_alumno AND ga.id_grupo = aa.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE g.grupo NOT LIKE '%INDUCCION%'
      AND g.grupo NOT LIKE '%EXCI%'
      AND g.grupo NOT LIKE '%INVER%'
      AND g.grupo NOT LIKE '%CAMBIO%'
      AND ga.id_alumno IN ( ? )
      AND aa.pagado > 0;`, [ idAlumnos ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};


kpi.getAlumnosNI = ( id_ciclo, idAlumnos ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT ga.id_alumno,g.grupo,a.id_usuario_ultimo_cambio, u.nombre_completo AS vendedora, IF(LOCATE('FAST', g.grupo)>0,2,1) AS unidad_negocio,
      aa.pagado, aa.adeudo, IF(LOCATE('INDUC',g.grupo)>0,1,0) AS induccion,
      CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, 
      REPLACE(a.telefono,' ','') AS telefono, REPLACE(a.celular,' ','') AS celular,
      REPLACE(tu.telefono,' ','') AS telefonoTutor, REPLACE(tu.celular,' ','') AS celularTutor,
      a.nombre, a.matricula, p.plantel FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnotutor tu ON tu.id_alumno = ga.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = a.id_alumno AND ga.id_grupo = aa.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE g.grupo NOT LIKE '%INDUCCION%'
      AND g.grupo NOT LIKE '%EXCI%'
      AND g.grupo NOT LIKE '%INVER%'
      AND g.grupo NOT LIKE '%CAMBIO%'
      AND g.id_ciclo = ? 
      AND a.id_alumno NOT IN (SELECT ga2.id_alumno FROM gruposalumnos ga2 LEFT JOIN grupos g2 ON g2.id_grupo = ga2.id_grupo WHERE g2.grupo LIKE '%CICLO%' AND g2.id_ciclo < ?)
      AND a.id_alumno NOT IN (?)
      AND aa.pagado > 1;`,
      [ id_ciclo, id_ciclo, idAlumnos ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};




kpi.examenesInduccionCEU = ( ) => {
  return new Promise((resolve, reject) => {
    sqlFAST.query(`SELECT e.id, e.telefono, e.nombre, n.nombre AS nivel FROM contactos_eu.examen_ubicacion e
      LEFT JOIN contactos_eu.niveles_evaluacion n ON n.id = e.nivel ;`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

kpi.examenesInduccionCEU2 = ( ) => {
  return new Promise((resolve, reject) => {
    sqlFAST.query(`SELECT e.id, e.telefono, e.nombre, n.nombre AS nivel FROM contactos_eu2.examen_ubicacion e
      LEFT JOIN contactos_eu2.niveles_evaluacion n ON n.id = e.nivel ;`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

kpi.examenesInduccionCEU3 = ( ) => {
  return new Promise((resolve, reject) => {
    sqlFAST.query(`SELECT e.id, e.telefono, e.nombre, n.nombre AS nivel FROM contactos_eu2.examen_ubicacion e
      LEFT JOIN contactos_eu2.niveles_evaluacion n ON n.id = e.nivel ;`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

kpi.examenesInduccionCEI = ( ) => {
  return new Promise((resolve, reject) => {
    sqlFAST.query(`SELECT e.id, e.telefono, e.nombre, n.nombre AS nivel FROM examen_interno.examen_ubicacion e
      LEFT JOIN examen_interno.niveles_evaluacion n ON n.id = e.nivel ;`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

kpi.examenesInduccionEI = ( ) => {
  return new Promise((resolve, reject) => {
    sqlINBI.query(`SELECT e.id, e.telefono, e.nombre, n.nombre AS nivel FROM examen_interno.examen_ubicacion e
      LEFT JOIN examen_interno.niveles_evaluacion n ON n.id = e.nivel ;`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

kpi.examenesInduccionET = ( ) => {
  return new Promise((resolve, reject) => {
    sqlINBI.query(`SELECT e.id, e.telefono, e.nombre, n.nombre AS nivel FROM examen_teens.examen_ubicacion e
      LEFT JOIN examen_teens.niveles_evaluacion n ON n.id = e.nivel ;`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

kpi.examenesInduccionR1 = ( ) => {
  return new Promise((resolve, reject) => {
    sqlINBI.query(`SELECT e.id, e.telefono, e.nombre, n.nombre AS nivel FROM recolector.examen_ubicacion e
      LEFT JOIN recolector.niveles_evaluacion n ON n.id = e.nivel ;`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

kpi.examenesInduccionR2 = ( ) => {
  return new Promise((resolve, reject) => {
    sqlINBI.query(`SELECT e.id, e.telefono, e.nombre, n.nombre AS nivel FROM recolector_2.examen_ubicacion e
      LEFT JOIN recolector_2.niveles_evaluacion n ON n.id = e.nivel ;`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};


kpi.getFechasReporteSemanal = ( fecha ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DATE_SUB(?, INTERVAL ((ELT(WEEKDAY(?) + 1, 1, 2, 3, 4, 5, 6, 7))-1) DAY) AS fecha_inicio, 
      DATE_ADD(?, INTERVAL (7-(ELT(WEEKDAY(?) + 1, 1, 2, 3, 4, 5, 6, 7))) DAY) AS fecha_final;`,[ fecha, fecha, fecha, fecha ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res[0]);
    });
  });
};

kpi.getFechaAnterior = ( fecha_inicio ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DATE_SUB(?, INTERVAL 7 DAY) AS fecha_inicio, 
      DATE_SUB(?, INTERVAL 1 DAY) AS fecha_final;`,[ fecha_inicio, fecha_inicio ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res[0]);
    });
  });
};

kpi.getNuevasMatriculasAnteriores = ( fechaini, fechafin ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT a.matricula, ga.id_alumno, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, g.grupo, ga.fecha_alta, p.plantel,
      a.id_usuario_ultimo_cambio, u.nombre_completo AS vendedora, IF(LOCATE('FAST', g.grupo)>0,2,1) AS unidad_negocio, i.monto_pagado AS pagado, aa.adeudo, IF(LOCATE('INDUC',g.grupo)>0,1,0) AS induccion,
        (SELECT COUNT(*) FROM gruposalumnos ga2
        LEFT JOIN grupos g1 ON g1.id_grupo = ga2.id_grupo
        LEFT JOIN ciclos c2 ON c2.id_ciclo = g1.id_ciclo
        WHERE c2.fecha_inicio_ciclo < c.fecha_inicio_ciclo
        AND id_alumno IN (a.id_alumno)) AS cant_grupos FROM ingresos i
        LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
        LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
        LEFT JOIN alumnos a ON a.id_alumno = i.id_alumno
        LEFT JOIN gruposalumnos ga ON ga.id_alumno = i.id_alumno AND ga.id_grupo = i.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = i.id_alumno AND aa.id_grupo = i.id_grupo
        WHERE DATE(i.fecha_pago) BETWEEN ? AND ?
        AND DATE(a.fecha_alta) < ?;`,[ fechaini, fechafin, fechaini ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res );
    });
  });
};


kpi.fechasFormatos = ( fechaini, fechafin ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DATE_FORMAT(?, '%d %M') AS fecha_inicio_format, DATE_FORMAT(?, '%d %M') AS fecha_final_format;`,[ fechaini, fechafin ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res[0]);
    });
  });
};

kpi.getCiclosAll = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT *, EXTRACT(YEAR FROM fecha_inicio_ciclo) AS anio 
      FROM ciclos
      WHERE ciclo LIKE '%ciclo%'
      AND id_ciclo_relacionado
      AND ciclo NOT LIKE '%FE%';`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

kpi.getCiclosFecha = ( fecha ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT c.*, cF.ciclo AS cicloFAST, c.ciclo AS cicloINBI FROM ciclos c
      LEFT JOIN ciclos cF ON cF.id_ciclo = c.id_ciclo_relacionado
      WHERE c.ciclo LIKE '%ciclo%'
      AND c.id_ciclo_relacionado
      AND c.ciclo NOT LIKE '%FE%'
      AND c.fecha_inicio_ciclo >= ?  AND c.fecha_inicio_ciclo <= DATE_ADD( NOW(), INTERVAL 14 DAY );`,[ fecha ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

kpi.getCicloFinalUltimo = ( fecha ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT c.*, cF.ciclo AS cicloFAST, c.ciclo AS cicloINBI FROM ciclos c
      LEFT JOIN ciclos cF ON cF.id_ciclo = c.id_ciclo_relacionado
      WHERE c.ciclo LIKE '%ciclo%'
      AND c.id_ciclo_relacionado
      AND c.ciclo NOT LIKE '%FE%'
      AND c.fecha_inicio_ciclo > ?;`,[ fecha ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res[0]);
    });
  });
};

kpi.getFechasUnicas  = ( fecha_inicio, fecha_final ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DISTINCT DATE(ga.fecha_alta) AS fecha_creacion, DATE_FORMAT(ga.fecha_alta, '%M %e') AS fecha_formateada
      FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = a.id_usuario_ultimo_cambio
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = a.id_alumno AND ga.id_grupo = aa.id_grupo
      WHERE ga.id_alumno IN (SELECT id_alumno FROM alumnos WHERE DATE(fecha_alta) BETWEEN ? AND ?)
      ORDER BY ga.fecha_alta;`,[ fecha_inicio, fecha_final ],(err, res) => {
      if (err) { return reject({message: err.sqlMessage}) }
      resolve(res);
    });
  });
};


kpi.getinfoVendedoraFechaIni = ( fechaini, vendedora ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT
    u.id_usuario,
    u.nombre_completo,
    p.id_plantel,
    p.plantel,
    CASE
        WHEN p.plantel LIKE '%FAST%' THEN (SELECT c.id_ciclo FROM ciclos c WHERE c.ciclo LIKE '%FE%' AND c.fecha_inicio_ciclo <= ? AND c.fecha_fin_ciclo >= ? LIMIT 1)
        ELSE (SELECT c.id_ciclo FROM ciclos c WHERE c.ciclo LIKE '%ciclo%' AND c.ciclo NOT LIKE '%FE%' AND c.fecha_inicio_ciclo <= ? AND c.fecha_fin_ciclo >= ? LIMIT 1)
    END AS id_ciclo,
    CASE
        WHEN p.plantel LIKE '%FAST%' THEN (SELECT c.ciclo FROM ciclos c WHERE c.ciclo LIKE '%FE%' AND c.fecha_inicio_ciclo <= ? AND c.fecha_fin_ciclo >= ? LIMIT 1)
        ELSE (SELECT c.ciclo FROM ciclos c WHERE c.ciclo LIKE '%ciclo%' AND c.ciclo NOT LIKE '%FE%' AND c.fecha_inicio_ciclo <= ? AND c.fecha_fin_ciclo >= ? LIMIT 1)
    END AS ciclo_actual,
     CASE
        WHEN p.plantel LIKE '%FAST%' THEN (SELECT c.id_ciclo_relacionado FROM ciclos c WHERE c.ciclo LIKE '%FE%' AND c.fecha_inicio_ciclo <= ? AND c.fecha_fin_ciclo >= ? LIMIT 1)
        ELSE (SELECT c.id_ciclo_relacionado FROM ciclos c WHERE c.ciclo LIKE '%ciclo%' AND c.ciclo NOT LIKE '%FE%' AND c.fecha_inicio_ciclo <= ? AND c.fecha_fin_ciclo >= ? LIMIT 1)
    END AS ciclo_relacionado,
    CASE
        WHEN p.plantel LIKE '%FAST%' THEN (SELECT c.fecha_fin_ciclo FROM ciclos c WHERE c.ciclo LIKE '%FE%' AND c.fecha_inicio_ciclo <= ? AND c.fecha_fin_ciclo >= ? LIMIT 1)
        ELSE (SELECT c.fecha_fin_ciclo FROM ciclos c WHERE c.ciclo LIKE '%ciclo%' AND c.ciclo NOT LIKE '%FE%' AND c.fecha_inicio_ciclo <= ? AND c.fecha_fin_ciclo >= ? LIMIT 1)
    END AS fecha_fin_ciclo_actual,
    CASE
        WHEN p.plantel LIKE '%FAST%' THEN (SELECT c.id_ciclo FROM ciclos c WHERE c.ciclo LIKE '%FE%' AND c.fecha_inicio_ciclo <= DATE_ADD(fecha_fin_ciclo_actual, INTERVAL 1 DAY) AND c.fecha_fin_ciclo >= DATE_ADD(fecha_fin_ciclo_actual, INTERVAL 1 DAY) LIMIT 1)
        ELSE (SELECT c.id_ciclo FROM ciclos c WHERE c.ciclo LIKE '%ciclo%' AND c.ciclo NOT LIKE '%FE%' AND c.fecha_inicio_ciclo <= DATE_ADD(fecha_fin_ciclo_actual, INTERVAL 1 DAY) AND c.fecha_fin_ciclo >= DATE_ADD(fecha_fin_ciclo_actual, INTERVAL 1 DAY) LIMIT 1)
    END AS id_ciclo_siguiente,
     CASE
        WHEN p.plantel LIKE '%FAST%' THEN (SELECT c.ciclo FROM ciclos c WHERE c.ciclo LIKE '%FE%' AND c.fecha_inicio_ciclo <= DATE_ADD(fecha_fin_ciclo_actual, INTERVAL 1 DAY) AND c.fecha_fin_ciclo >= DATE_ADD(fecha_fin_ciclo_actual, INTERVAL 1 DAY) LIMIT 1)
        ELSE (SELECT c.ciclo FROM ciclos c WHERE c.ciclo LIKE '%ciclo%' AND c.ciclo NOT LIKE '%FE%' AND c.fecha_inicio_ciclo <= DATE_ADD(fecha_fin_ciclo_actual, INTERVAL 1 DAY) AND c.fecha_fin_ciclo >= DATE_ADD(fecha_fin_ciclo_actual, INTERVAL 1 DAY) LIMIT 1)
    END AS ciclo_siguiente,
    CASE
        WHEN p.plantel LIKE '%FAST%' THEN (SELECT c.id_ciclo_relacionado FROM ciclos c WHERE c.ciclo LIKE '%FE%' AND c.fecha_inicio_ciclo <= DATE_ADD(fecha_fin_ciclo_actual, INTERVAL 1 DAY) AND c.fecha_fin_ciclo >= DATE_ADD(fecha_fin_ciclo_actual, INTERVAL 1 DAY) LIMIT 1)
        ELSE (SELECT c.id_ciclo_relacionado FROM ciclos c WHERE c.ciclo LIKE '%ciclo%' AND c.ciclo NOT LIKE '%FE%' AND c.fecha_inicio_ciclo <= DATE_ADD(fecha_fin_ciclo_actual, INTERVAL 1 DAY) AND c.fecha_fin_ciclo >= DATE_ADD(fecha_fin_ciclo_actual, INTERVAL 1 DAY) LIMIT 1)
    END AS ciclo_relacionado_siguiente
    FROM usuarios u
    LEFT JOIN planteles p ON u.id_plantel = p.id_plantel
    WHERE u.id_usuario = ?;`,
     [ fechaini,fechaini,fechaini,fechaini,fechaini,fechaini,fechaini,fechaini,fechaini,fechaini,fechaini,fechaini,fechaini,fechaini,fechaini,fechaini, vendedora ],(err, res) => {
     if(err){ return reject({ message: err.sqlMessage }) }
     resolve(res)
   });
 });
};

module.exports = kpi;

