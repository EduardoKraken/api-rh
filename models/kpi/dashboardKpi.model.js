const { result } = require("lodash");
// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");

const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");


//const constructor
const dashboardKpi = function(e) {};

dashboardKpi.cicloActual = () => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DATE(fecha_inicio_ciclo) AS inicio, DATE(fecha_fin_ciclo) AS final, id_ciclo, ciclo FROM ciclos WHERE CURRENT_DATE() BETWEEN DATE(fecha_inicio_ciclo) AND DATE(fecha_fin_ciclo)
      AND ciclo NOT LIKE '%EXCI%'
      AND ciclo NOT LIKE '%INDUC%'
      AND ciclo NOT LIKE '%INVER%'
      AND ciclo NOT LIKE '%VERA%'
      AND activo_sn = 1
      LIMIT 2;`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

// dashboardKpi.getAlumnosCicloActual = ( ciclos ) => {
//   return new Promise((resolve, reject) => {
//     sqlERP.query(`SELECT DISTINCT ga.id_alumno, g.id_grupo, g.id_ciclo, g.id_plantel, a.adeudo, m.numero_teacher, m2.numero_teacher FROM gruposalumnos ga
//       LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
//       LEFT JOIN alumnos_grupos_especiales_carga a ON a.id_alumno = ga.id_alumno AND a.id_grupo = ga.id_grupo
//       LEFT JOIN maestrosgrupo m ON m.id_grupo = g.id_grupo AND m.numero_teacher = 1
//       LEFT JOIN maestrosgrupo m2 ON m2.id_grupo = g.id_grupo AND m2.numero_teacher = 2
//       WHERE g.id_ciclo IN ( ? ) AND a.adeudo <= 0 AND ga.activo_sn = 1 
//       AND g.grupo NOT LIKE '%CERTI%'
//       AND g.grupo NOT LIKE '%EXCI%'
//       AND g.grupo NOT LIKE '%INDUC%'
//       AND g.grupo NOT LIKE '%INVER%'
//       AND g.grupo NOT LIKE '%VERA%';`, [ ciclos ],(err, res) => {
//       if (err) {
//         reject(err);
//         return;
//       }
//       resolve(res);
//     });
//   });
// };

dashboardKpi.getAlumnosCicloActual = ( ciclos ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DISTINCT a.*,p.id_plantel, IF(LOCATE('FAST', p.plantel) > 0,2,1) AS escuela FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE a.id_ciclo IN (?)  AND a.adeudo = 0  AND g.grupo NOT LIKE '%INVER%' AND g.grupo NOT LIKE '%CERTI%' 
      AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
      GROUP BY a.id_alumno;`, [ ciclos ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

      // AND m.numero_teacher IS NOT NULL AND m2.numero_teacher IS NOT NULL
dashboardKpi.getAlumnosCicloSiguiente = ( ciclosActuales, ciclosSiguientes ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT  DISTINCT a.*,p.id_plantel, IF(LOCATE('FAST', p.plantel) > 0,2,1) AS escuela FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE a.id_ciclo IN (?) AND a.adeudo = 0 
      AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
      AND a.id_alumno IN(SELECT  DISTINCT a.id_alumno FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE a.id_ciclo IN (?) AND a.adeudo = 0  GROUP BY a.id_alumno)
      GROUP BY a.id_alumno;`, [ ciclosActuales, ciclosSiguientes ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

// dashboardKpi.getAlumnosCicloSiguienteAnterior = ( ciclos, alumnos ) => {
//   return new Promise((resolve, reject) => {
//     sqlERP.query(`SELECT DISTINCT ga.id_alumno, g.id_grupo, g.id_ciclo, g.id_plantel, a.adeudo, m.numero_teacher, m2.numero_teacher FROM gruposalumnos ga
//       LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
//       LEFT JOIN alumnos_grupos_especiales_carga a ON a.id_alumno = ga.id_alumno AND a.id_grupo = ga.id_grupo
//       LEFT JOIN maestrosgrupo m ON m.id_grupo = g.id_grupo AND m.numero_teacher = 1
//       LEFT JOIN maestrosgrupo m2 ON m2.id_grupo = g.id_grupo AND m2.numero_teacher = 2
//       WHERE g.id_ciclo IN (?) AND a.adeudo <= 0 AND ga.activo_sn = 1 
//       AND ga.id_alumno IN (?)
//       AND m.numero_teacher IS NOT NULL AND m2.numero_teacher IS NOT NULL;`, [ ciclos, alumnos ],(err, res) => {
//       if (err) {
//         reject(err);
//         return;
//       }
//       resolve(res);
//     });
//   });
// };

// dashboardKpi.getAlumnosCicloSiguienteAnterior = ( ciclos, alumnos ) => {
//   return new Promise((resolve, reject) => {
//     sqlERP.query(`SELECT  DISTINCT a.*,p.id_plantel FROM alumnos_grupos_especiales_carga a 
//       LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
//       LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
//       LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
//       WHERE a.id_ciclo IN (?) AND a.adeudo = 0
//       AND a.id_alumno IN (?) GROUP BY a.id_alumno;`, [ ciclos, alumnos ],(err, res) => {
//       if (err) {
//         reject(err);
//         return;
//       }
//       resolve(res);
//     });
//   });
// };

dashboardKpi.getAlumnosCicloSiguienteAnterior = ( ciclosActuales, ciclosSiguientes, fechaLimite ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT  DISTINCT a.*,p.id_plantel, IF(LOCATE('FAST', p.plantel) > 0,2,1) AS escuela FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE a.id_ciclo IN (?) AND a.adeudo = 0 
      AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
      AND a.id_alumno IN(SELECT  DISTINCT a.id_alumno FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE a.id_ciclo IN (?) AND a.adeudo = 0 AND DATE(a.fecha_registro) <= ? GROUP BY a.id_alumno)
      GROUP BY a.id_alumno;`, [ ciclosActuales, ciclosSiguientes, fechaLimite ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};


dashboardKpi.getCiclo = ( ciclo ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DATE(fecha_inicio_ciclo) AS inicio, DATE(fecha_fin_ciclo) AS final, c.* FROM ciclos c WHERE ciclo = '${ciclo}';`,(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

dashboardKpi.getPlanteles = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT *, IF(LOCATE('FAST', plantel) > 0,2,1) AS escuela 
      FROM planteles 
      WHERE plantel NOT LIKE '%Entrena%'
      AND plantel NOT LIKE '%Aseso%'
      AND plantel NOT LIKE '%Corpo%'
      ORDER BY plantel;`,(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

dashboardKpi.getDiasTranscurridos = ( fechaCicloAnt, fechaCicloActual ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DATE_ADD( ? , INTERVAL DATEDIFF(CURRENT_DATE(), ? ) DAY) AS dias_transcurridos;`,[ fechaCicloAnt, fechaCicloActual ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};


dashboardKpi.getNiAlumnos = (ciclos) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT count(DISTINCT ga.id_alumno) as total, p.id_plantel, p.plantel, IF(LOCATE('FAST', p.plantel) > 0,2,1) AS escuela FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo IN (?) AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo NOT IN (?)) AND aa.adeudo = 0 AND aa.pagado > 1 
      GROUP BY p.id_plantel;
      `,[ciclos,ciclos],(err,res)=>{
        if(err){
          reject(err);
          return;
        }
        resolve(res)
      });
  })
}

dashboardKpi.getNiAlumnosSiguientes = (ciclos, fecha) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT count(DISTINCT ga.id_alumno) as total, p.id_plantel, p.plantel, ga.fecha_alta, IF(LOCATE('FAST', p.plantel) > 0,2,1) AS escuela FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo IN (?) AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo NOT IN (?)) AND aa.adeudo = 0 AND aa.pagado > 1 
      AND DATE(ga.fecha_alta) <= ?
      GROUP BY ga.id_alumno;
      `,[ciclos,ciclos,fecha],(err,res)=>{
        if(err){
          reject(err);
          return;
        }
        resolve(res)
      });
  })
}

dashboardKpi.getContactosPorFecha = (fechaCicloActual,fechaFinCicloActual) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT p.*, pl.plantel, IF(LOCATE('FAST', pl.plantel) > 0,2,1) AS escuela FROM prospectos p
      LEFT JOIN plantel pl ON pl.idplantel = p.sucursal_interes 
      WHERE DATE(p.fecha_creacion) BETWEEN ? AND ?`,[ fechaCicloActual, fechaFinCicloActual ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

dashboardKpi.getDiaClase = ( ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT (ELT(WEEKDAY(CURDATE()) + 1,'domingo' ,'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado')) AS dia,
      (ELT(WEEKDAY(CURDATE()) + 1,7,1,2,3,5,4,5)) AS dia_numero;`,(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res[0])
    });
  });
};

dashboardKpi.getClasesFast = ( ciclos, dia ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id AS id_grupo,c.id AS id_ciclo, g.nombre AS grupo, g.id_plantel
      FROM grupos g
      LEFT JOIN cursos cu         ON cu.id = g.id_curso
      LEFT JOIN frecuencia f      ON f.id = cu.id_frecuencia
      LEFT JOIN horarios h        ON h.id = g.id_horario
      LEFT JOIN ciclos c          ON c.id = g.id_ciclo
      WHERE c.iderp IN ( ? ) AND ${ dia } = 1 AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0;`,[ ciclos, dia ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

dashboardKpi.getClasesInbi = ( ciclos, dia ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id AS id_grupo,c.id AS id_ciclo, g.nombre AS grupo, g.id_plantel
      FROM grupos g
      LEFT JOIN cursos cu         ON cu.id = g.id_curso
      LEFT JOIN frecuencia f      ON f.id = cu.id_frecuencia
      LEFT JOIN horarios h        ON h.id = g.id_horario
      LEFT JOIN ciclos c          ON c.id = g.id_ciclo
      WHERE c.iderp IN ( ? ) AND ${ dia } = 1 AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0;`,[ ciclos, dia ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

dashboardKpi.getClasesBuenasFast = ( ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id, g.id_plantel FROM indicador_clases i 
      LEFT JOIN grupos g ON g.id = i.id_grupo 
      WHERE DATE(i.fecha_creacion) = DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY)
      AND inicio_tiempo = 1 AND maestro_asignado = 1 AND contenido_correcto = 1 AND finaliza_tiempo = 1;`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

dashboardKpi.getClasesBuenasInbi = ( ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id, g.id_plantel FROM indicador_clases i 
      LEFT JOIN grupos g ON g.id = i.id_grupo 
      WHERE DATE(i.fecha_creacion) = DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY)
      AND inicio_tiempo = 1 AND maestro_asignado = 1 AND contenido_correcto = 1 AND finaliza_tiempo = 1;`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};


dashboardKpi.getClasesEvaluadas = ( ) => {
   return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT cr.idcalidad_eval_clase, SUM(cr.valor) AS calificacion, ce.id_grupo, ce.unidad_negocio FROM calidad_res_clase cr
      LEFT JOIN calidad_eval_clase ce ON ce.idcalidad_eval_clase = cr.idcalidad_eval_clase
      WHERE cr.estatus = 1 AND DATE(ce.fecha_creacion) = DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY)
      GROUP BY cr.idcalidad_eval_clase;`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

dashboardKpi.getGruposInbi = ( idgrupos ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM grupos WHERE id IN ( ? )`, [ idgrupos ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

dashboardKpi.getGruposFast = ( idgrupos ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM grupos WHERE id IN ( ? )`, [ idgrupos ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};


dashboardKpi.addNumeroVacantes = (u, result) => {
  sqlERPNUEVO.query(`INSERT INTO numero_vacantes(numero_vacantes,contratados)VALUES(?,?)`,[ u.numero_vacantes, u.contratados ],
  (err, res) => {
    if (err) { 
      result(err, null); 
      return; 
    }
    result(null, { id: res.insertId, ...u });
  })
}

dashboardKpi.getnumeroVacantesList = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM numero_vacantes WHERE deleted = 0`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


dashboardKpi.getNumeroVacantes = (  ) => {
   return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM numero_vacantes ORDER BY fecha_creacion DESC LIMIT 1;`,(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res[0])
    });
  });
};

dashboardKpi.getEgresos = ( ciclos ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`select SUM(costo_total) AS suma, t1.id, t2.id_plantel, t2.concepto, t9.id_ciclo, IF(LOCATE('FAST',pl.plantel)> 0, 2,1) AS escuela,
      case when t10.nuevo_monto is null then 'NO' else 'SI' end as costo_anterior
      from fyn_proc_egresos t1
      left join requisicion_compra_detalle t2 on t1.id_requisicion_compra_detalle = t2.id
      left join requisicion_compra t3 on t2.id_requisicion_compra = t3.id
      left join requisicion_compra_estatus t4 on t3.id_requisicion_compra_estatus = t4.id 
      left join ciclos t9 on t3.id_ciclo = t9.id_ciclo
      LEFT JOIN planteles pl ON pl.id_plantel = t2.id_plantel
      left join (select t1.nuevo_monto, t1.descripcion, t1.cambio, t1.tipo_cambio, t1.comentarios, t1.id_egreso
      from fyn_proc_ajustes_egresos t1
      WHERE t1.id IN(select max(id) as id
      from fyn_proc_ajustes_egresos
      group by id_egreso)) as t10 on t10.id_egreso = t1.id WHERE t9.id_ciclo IN ( ? )
      GROUP BY t2.id_plantel;`,[ ciclos ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};


dashboardKpi.getIngresos = ( ciclos ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT c.id_ciclo, IFNULL(c.ciclo,"Sin ciclo") AS ciclo, SUM(a.pagado) AS suma, g.id_plantel, IF(LOCATE('FE',c.ciclo)> 0, 2,1) AS escuela FROM alumnos_grupos_especiales_carga a
      LEFT JOIN ciclos c ON c.id_ciclo = a.id_ciclo
      LEFT  JOIN grupos g ON g.id_grupo = a.id_grupo
      WHERE c.activo_sn = 1 AND c.id_ciclo IN ( ? )
      GROUP BY id_plantel;`,[ ciclos ], (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

dashboardKpi.getMetasList = (result) => {
  sqlERPNUEVO.query(`SELECT m.*, p.plantel FROM 
    metas_dashboard_general m
    LEFT JOIN plantel p ON p.idplantel = m.id_plantel;`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


dashboardKpi.updateMeta = (p, result) => {
  sqlERPNUEVO.query(` UPDATE metas_dashboard_general SET ri=?, ventas=?, contactos=?, calidad_clase=?, calidad_teacher=?, vacantes=?, ingreso_egreso=?
    WHERE idmetas_dashboard_general = ?`, 
    [p.ri, p.ventas, p.contactos, p.calidad_clase, p.calidad_teacher, p.vacantes,p.ingreso_egreso, p.idmetas_dashboard_general],
    (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_fopd" }, null);
        return;
      }
      result(null, { ...p });
  });
};


dashboardKpi.getMetasSemanaList = (result) => {
  sqlERPNUEVO.query(`SELECT m.*, p.plantel FROM metas_semanales m
    LEFT JOIN plantel p ON p.idplantel = m.id_plantel;`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};

dashboardKpi.updateMetaSemana = (p, result) => {
  sqlERPNUEVO.query(` UPDATE metas_semanales SET semana1=?, semana2=?, semana3=?, semana4=?
    WHERE idmetas_semanales = ?`, 
    [p.semana1, p.semana2, p.semana3, p.semana4, p.idmetas_semanales],
    (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_fopd" }, null);
        return;
      }
      result(null, { ...p });
  });
};


// RESULTADOOS
dashboardKpi.existeResultadoUsuario = ( idcolaborador ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM resultados_personal WHERE DATE(fecha_creacion) = CURRENT_DATE AND idcolaborador = ?;`,[ idcolaborador ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
       resolve(res[0]);
    });
  });
};


dashboardKpi.addResultadoUsuario = (u, result) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`INSERT INTO resultados_personal(idcolaborador,idjefe,objetivo)VALUES(?,?,?)`,
      [ u.idcolaborador,u.idjefe,u.objetivo], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve({ id: res.insertId, ...u });
    })
  })
}

dashboardKpi.addResultadoActividad = ( id, u ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`INSERT INTO resultados_actividades(idresultados_personal,actividad,retroalimentacion)VALUES(?,?,?)`,
      [ id, u.actividad, u.retroalimentacion  ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve({ id: res.insertId, ...u });
    })
  })
}

dashboardKpi.getResultadosUsuarioFecha = ( idcolaboradores, fecha ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT r.*, 
      CONCAT( DATE_FORMAT(f.fecha_inicio, '%d'),' ', SUBSTRING(DATE_FORMAT(f.fecha_inicio, '%M'),1,3), ' a ',DATE_FORMAT(f.fecha_final, '%d'),' ', SUBSTRING(DATE_FORMAT(f.fecha_final, '%M'),1,3)) AS fecha_formato,
      (SELECT idfechas_resultados FROM fechas_resultados WHERE DATE(r.fecha_creacion) BETWEEN fecha_inicio AND fecha_final LIMIT 1) AS idfechas 
      FROM resultados_personal r
      LEFT JOIN fechas_resultados f ON f.idfechas_resultados = (SELECT idfechas_resultados FROM fechas_resultados WHERE DATE(r.fecha_creacion) BETWEEN fecha_inicio AND fecha_final LIMIT 1)
      WHERE r.idcolaborador IN(?)
      ${ fecha ? ' AND ? = (SELECT idfechas_resultados FROM fechas_resultados WHERE DATE(r.fecha_creacion) BETWEEN fecha_inicio AND fecha_final LIMIT 1) ' : '' };`,[ idcolaboradores, fecha ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res);
    });
  });
};

dashboardKpi.getResultadoHistorial = ( idcolaborador ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT r.*, 
      CONCAT( DATE_FORMAT(f.fecha_inicio, '%d'),' ', SUBSTRING(DATE_FORMAT(f.fecha_inicio, '%M'),1,3), ' a ',DATE_FORMAT(f.fecha_final, '%d'),' ', SUBSTRING(DATE_FORMAT(f.fecha_final, '%M'),1,3)) AS fecha_formato,
      (SELECT idfechas_resultados FROM fechas_resultados WHERE DATE(r.fecha_creacion) BETWEEN fecha_inicio AND fecha_final LIMIT 1) AS idfechas 
      FROM resultados_personal r
      LEFT JOIN fechas_resultados f ON f.idfechas_resultados = (SELECT idfechas_resultados FROM fechas_resultados WHERE DATE(r.fecha_creacion) BETWEEN fecha_inicio AND fecha_final LIMIT 1)
      WHERE r.idcolaborador = ?;`,[ idcolaborador ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res);
    });
  });
};


dashboardKpi.getActividadesResultado = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM resultados_actividades WHERE idresultados_personal IN( ? );`,[ id ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res);
    });
  });
};


dashboardKpi.getUltimoResultado = ( idcolaborador ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM resultados_personal WHERE idcolaborador = ? 
      AND fecha_creacion = (SELECT MAX(fecha_creacion) 
      FROM resultados_personal WHERE idcolaborador = ?)`,[ idcolaborador, idcolaborador ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res[0]);
    });
  });
};


dashboardKpi.updateResultado = ( idjefe, id ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(` UPDATE resultados_personal SET idjefe = ?, estatus = 1 WHERE idresultados_personal = ?`, [ idjefe, id ],
      (err, res) => {
        if (err) { return reject({ message: err.sqlMessage }) }

        if (res.affectedRows == 0) {
          return reject({ message: 'No se encontró un registro' });
        }

        resolve({ idjefe, id });
    });
  });
};

dashboardKpi.updateActididadResultado = ( retroalimentacion, idresultados_actividades ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(` UPDATE resultados_actividades SET retroalimentacion = ? WHERE idresultados_actividades = ?`, [ retroalimentacion, idresultados_actividades ],
      (err, res) => {
        if (err) { return reject({ message: err.sqlMessage }) }

        if (res.affectedRows == 0) {
          return reject({ message: 'No se encontró un registro' });
        }

        resolve({ retroalimentacion, idresultados_actividades });
    });
  });
};

dashboardKpi.fechasResultados = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT *, 
      CONCAT( DATE_FORMAT(fecha_inicio, '%d'),' ', SUBSTRING(DATE_FORMAT(fecha_inicio, '%M'),1,3), ' a ',DATE_FORMAT(fecha_final, '%d'),' ', SUBSTRING(DATE_FORMAT(fecha_final, '%M'),1,3)) AS fecha_formato  
      FROM fechas_resultados
      WHERE fecha_inicio <= CURRENT_DATE;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res);
    });
  });
};

dashboardKpi.fechaEspaniol = (  ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SET lc_time_names = 'es_ES';`, (err, res) => {
      if (err) {
        return reject( err );
      }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};

dashboardKpi.fechaEspaniolsqlERP = (  ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SET lc_time_names = 'es_ES';`, (err, res) => {
      if (err) {
        return reject( err );
      }
      // Retornamos solo un dato
      return resolve( res );
    });
  })
};


module.exports = dashboardKpi;
