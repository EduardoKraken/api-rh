const { result } = require("lodash");
// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");


//const constructor
const dashboardKpiDiario = function(e) {};

dashboardKpiDiario.cicloActual = () => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DATE(fecha_inicio_ciclo) AS inicio, DATE(fecha_fin_ciclo) AS final, id_ciclo, ciclo, id_ciclo_relacionado FROM ciclos 
			WHERE CURRENT_DATE() BETWEEN DATE(fecha_inicio_ciclo) AND DATE(fecha_fin_ciclo)
			AND ciclo NOT LIKE '%EXCI%'
			AND ciclo NOT LIKE '%INDUC%'
			AND ciclo NOT LIKE '%INVER%'
			AND ciclo NOT LIKE '%VERA%'
			AND ciclo NOT LIKE '%FE%'
			AND activo_sn = 1
			LIMIT 1;`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

dashboardKpiDiario.getCiclo = ( ciclo ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DATE(fecha_inicio_ciclo) AS inicio, DATE(fecha_fin_ciclo) AS final, c.* FROM ciclos c WHERE ciclo = '${ciclo}';`,(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

dashboardKpiDiario.getInscritosAyer = ( cicloSiguienteInbi, cicloSiguienteFast ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT COUNT(u.id_usuario) AS cantidad, IF(LOCATE('FAST',p.plantel) > 0,2,1) AS escuela, a.matricula, CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, 
			DATE(ga.fecha_alta) AS fecha, g.id_ciclo, c.ciclo,u.id_usuario,u.nombre_completo, aa.adeudo
			FROM gruposalumnos ga
			LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
			LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
			LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
			LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
			LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
			LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
			WHERE g.grupo LIKE '%CICLO%'
			AND g.id_ciclo IN (?,?)
      AND aa.pagado > 0
			AND DATE(ga.fecha_alta) = IF((WEEKDAY(DATE_SUB(CURDATE(), INTERVAL 1 DAY)) + 1) = 7, DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 1 DAY), DATE_SUB(CURDATE(), INTERVAL 1 DAY))
			AND ga.id_alumno NOT IN (SELECT t1.id_alumno FROM gruposalumnos t1
			LEFT JOIN grupos t2 ON t2.id_grupo = t1.id_grupo
			WHERE t2.grupo LIKE '%CICLO%'
			AND t2.id_ciclo NOT IN (?,?))
			GROUP BY u.id_usuario, c.id_ciclo, ga.id_alumno;`,[ cicloSiguienteInbi, cicloSiguienteFast, cicloSiguienteInbi, cicloSiguienteFast ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

dashboardKpiDiario.getInscritosTotalesCiclo = ( cicloSiguienteInbi, cicloSiguienteFast ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT COUNT(u.id_usuario) AS cantidad, IF(LOCATE('FAST',p.plantel) > 0,2,1) AS escuela,
			DATE(ga.fecha_alta) AS fecha, g.id_ciclo, c.ciclo,u.id_usuario,u.nombre_completo, aa.adeudo
			FROM gruposalumnos ga
			LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
			LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
			LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
			LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
			LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
			LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
			WHERE g.grupo LIKE '%CICLO%'
			AND g.id_ciclo IN (?,?)
      AND aa.pagado > 0
			AND ga.id_alumno NOT IN (SELECT t1.id_alumno FROM gruposalumnos t1
			LEFT JOIN grupos t2 ON t2.id_grupo = t1.id_grupo
			WHERE t2.grupo LIKE '%CICLO%'
			AND t2.id_ciclo NOT IN (?,?))
			GROUP BY u.id_usuario, c.id_ciclo, a.id_alumno;`,[ cicloSiguienteInbi, cicloSiguienteFast, cicloSiguienteInbi, cicloSiguienteFast ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

dashboardKpiDiario.getAlumnosCicloActual = ( cicloSiguienteInbi, cicloSiguienteFast ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DISTINCT a.*,p.id_plantel, IF(LOCATE('FAST', p.plantel) > 0,2,1) AS escuela, 
      IF(LOCATE('TEENS', g.grupo) > 0,0,IF(g.id_nivel = 40 OR g.id_nivel = 42,1,0)) AS catorce FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE a.id_ciclo IN (?,?)  AND a.adeudo = 0  AND g.grupo NOT LIKE '%INVER%' AND g.grupo NOT LIKE '%CERTI%' 
      AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
      GROUP BY a.id_alumno;`, [ cicloSiguienteInbi, cicloSiguienteFast ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

dashboardKpiDiario.getAlumnosCicloSiguiente = ( cicloActualInbi, cicloActualFast, ciclosSiguienteInbi, cicloSiguienteFast ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT  DISTINCT a.*,p.id_plantel, IF(LOCATE('FAST', p.plantel) > 0,2,1) AS escuela,
      IF(LOCATE('TEENS', g.grupo) > 0,0,IF(g.id_nivel = 40 OR g.id_nivel = 42,1,0)) AS catorce,
    (SELECT DATE(t1.fecha_alta) FROM gruposalumnos t1 
			LEFT JOIN grupos t2 ON t2.id_grupo = t1.id_grupo
			WHERE t1.id_alumno = a.id_alumno AND t2.id_ciclo IN (?,?)
			AND DATE(t1.fecha_alta) = IF((WEEKDAY(DATE_SUB(CURDATE(), INTERVAL 1 DAY)) + 1) = 7, DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 1 DAY), DATE_SUB(CURDATE(), INTERVAL 1 DAY))) AS fecha_alta_nuevo_grupo 
			FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE a.id_ciclo IN (?,?) AND a.adeudo = 0 
      AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
      AND a.id_alumno IN(SELECT  DISTINCT a.id_alumno FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE a.id_ciclo IN (?,?) AND a.adeudo = 0  GROUP BY a.id_alumno)
      GROUP BY a.id_alumno;`, [ ciclosSiguienteInbi, cicloSiguienteFast, cicloActualInbi, cicloActualFast, ciclosSiguienteInbi, cicloSiguienteFast ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

dashboardKpiDiario.getCiclo = ( ciclo ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DATE(fecha_inicio_ciclo) AS inicio, DATE(fecha_fin_ciclo) AS final, c.* FROM ciclos c WHERE ciclo = '${ciclo}';`,(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

dashboardKpiDiario.getPlanteles = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM planteles;`,(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

dashboardKpiDiario.getContactosVendedoraAyer  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT usuario_creo, usuario_asignado, DATE(fecha_creacion) AS fecha, idpuesto FROM prospectos
    	WHERE DATE(fecha_creacion) = IF((WEEKDAY(DATE_SUB(CURDATE(), INTERVAL 1 DAY)) + 1) = 7, DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 1 DAY), DATE_SUB(CURDATE(), INTERVAL 1 DAY));`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};


module.exports = dashboardKpiDiario;
