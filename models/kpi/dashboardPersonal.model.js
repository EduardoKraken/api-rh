const { result } = require("lodash");
// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");


//const constructor
const dashboarPersonal = function(e) {};


dashboarPersonal.getRecepcionistas = () => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM usuarios WHERE idpuesto = 19 AND deleted = 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  });
};

dashboarPersonal.getUsuariosDesarrollo = () => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM usuarios WHERE idpuesto = 12 AND deleted = 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  });
};

dashboarPersonal.getUsuariosAdministracion = () => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM usuarios WHERE idpuesto = 37 AND deleted = 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  });
};

dashboarPersonal.getUsuariosVentas = () => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM usuarios WHERE idpuesto = 18 AND deleted = 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  });
};

dashboarPersonal.getPlantelesUsuario = () => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM planteles_usuario;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  });
};

dashboarPersonal.getNombresRecepcionistas = ( iderps ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM usuarios WHERE id_usuario IN ( ? );`,[ iderps ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  });
};

dashboarPersonal.cicloActual = () => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DATE(fecha_inicio_ciclo) AS inicio, 
      DATE(fecha_fin_ciclo) AS final, id_ciclo, ciclo, id_ciclo_relacionado,
      CURDATE() AS actual, DATEDIFF(CURDATE(), DATE(fecha_inicio_ciclo)) AS diferencia FROM ciclos 
			WHERE CURRENT_DATE() BETWEEN DATE(fecha_inicio_ciclo) AND DATE(fecha_fin_ciclo)
			AND ciclo NOT LIKE '%EXCI%'
			AND ciclo NOT LIKE '%INDUC%'
			AND ciclo NOT LIKE '%INVER%'
			AND ciclo NOT LIKE '%VERA%'
			AND ciclo NOT LIKE '%FE%'
			AND activo_sn = 1
			LIMIT 1;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0]);
    });
  });
};

dashboarPersonal.getAlumnosCicloActual = ( cicloInbi, cicloFast ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DISTINCT a.*,p.id_plantel FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE a.id_ciclo IN (?,?)  AND a.adeudo = 0  AND g.grupo NOT LIKE '%INVER%' AND g.grupo NOT LIKE '%CERTI%' 
      AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
      GROUP BY a.id_alumno;`, [ cicloInbi, cicloFast ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  });
};

// Obtener las actividades registradas actuales de todos los usuarios
dashboarPersonal.getActividadesUsuario = () => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT a.*, p.puesto, WEEK(DATE(a.fecha_inicio)) AS semana, YEAR(DATE(a.fecha_inicio)) AS anio
      FROM actividades_personal a
      LEFT JOIN puesto p ON p.idpuesto = a.idpuesto 
      WHERE a.deleted = 0
      AND WEEK(fecha_inicio) = WEEK( NOW() )
      AND YEAR(fecha_inicio) = YEAR( NOW() );`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  });
};


      // AND m.numero_teacher IS NOT NULL AND m2.numero_teacher IS NOT NULL
dashboarPersonal.getAlumnosCicloSiguiente = ( cicloActualInbi, cicloActualFast, cicloSiguienteInbi, cicloSiguienteFast ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT  ga.id_alumno, ga.id_grupo, p.id_plantel, ga.fecha_alta, a.fecha_registro
			FROM alumnos_grupos_especiales_carga a 
			LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno AND ga.id_grupo = a.id_grupo
			LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
			LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
			WHERE a.id_ciclo IN (${cicloActualInbi},${cicloActualFast}) AND a.adeudo = 0 
			AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
			AND a.id_alumno IN(SELECT  DISTINCT a.id_alumno FROM alumnos_grupos_especiales_carga a 
			LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
			LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
			LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
			WHERE a.id_ciclo IN (${ cicloSiguienteInbi }, ${ cicloSiguienteFast }) AND a.adeudo = 0  GROUP BY a.id_alumno)
			AND WEEK(CURRENT_DATE) = (SELECT WEEK(DATE(t1.fecha_alta)) FROM gruposalumnos t1 
			LEFT JOIN grupos t2 ON t2.id_grupo = t1.id_grupo
			WHERE t2.id_ciclo IN (${ cicloSiguienteInbi }, ${ cicloSiguienteFast }) AND id_alumno = ga.id_alumno) 
			GROUP BY a.id_alumno;`,(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  });
};

/* calular los alumnos de RI del ciclo actual */
dashboarPersonal.getAlumnosNI = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT ga.id_alumno, ga.id_grupo, ga.id_usuario_ultimo_cambio, IF(LOCATE('FAST',p.plantel) > 0,2,1) AS escuela,
      CONCAT(a.nombre, ' ', IFNULL(a.apellido_paterno,''), ' ', IFNULL(a.apellido_materno,'')) AS nombre, 
      DATE(ga.fecha_alta) AS fecha, ga.fecha_alta, g.id_ciclo, c.ciclo
      FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno AND aa.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND aa.adeudo = 0 AND aa.pagado > 1 
      AND WEEK(DATE(ga.fecha_alta)) = WEEK(NOW()) AND EXTRACT(YEAR FROM ga.fecha_alta) = EXTRACT(YEAR FROM NOW())
      AND ga.id_alumno NOT IN (SELECT t1.id_alumno FROM gruposalumnos t1
      LEFT JOIN grupos t2 ON t2.id_grupo = t1.id_grupo
      WHERE t2.grupo LIKE '%CICLO%'
      AND t2.id_ciclo NOT IN (g.id_ciclo));`,(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  });
};

// get student's register by crm
dashboarPersonal.getAlumnosInscritosCRM = ( idalumnos ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM prospectos WHERE idalumno IN (?);`,[ idalumnos ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  });
};


dashboarPersonal.getAlumnosCicloSiguienteAnterior = ( ciclosActuales, ciclosSiguientes, fechaLimite ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT  DISTINCT a.*,p.id_plantel FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE a.id_ciclo IN (?) AND a.adeudo = 0 
      AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
      AND a.id_alumno IN(SELECT  DISTINCT a.id_alumno FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      WHERE a.id_ciclo IN (?) AND a.adeudo = 0 AND DATE(a.fecha_registro) <= ? GROUP BY a.id_alumno)
      GROUP BY a.id_alumno;`, [ ciclosActuales, ciclosSiguientes, fechaLimite ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  });
};


dashboarPersonal.getCiclo = ( ciclo ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DATE(fecha_inicio_ciclo) AS inicio, DATE(fecha_fin_ciclo) AS final, c.* FROM ciclos c WHERE ciclo = '${ciclo}';`,(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0]);
    });
  });
};

dashboarPersonal.getPlanteles = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM planteles;`,(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  });
};

dashboarPersonal.getDiasTranscurridos = ( fechaCicloAnt, fechaCicloActual ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DATE_ADD( ? , INTERVAL DATEDIFF(CURRENT_DATE(), ? ) DAY) AS dias_transcurridos;`,[ fechaCicloAnt, fechaCicloActual ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0]);
    });
  });
};


dashboarPersonal.getNiAlumnos = (ciclos) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT count(DISTINCT ga.id_alumno) as total, p.id_plantel, p.plantel FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo IN (?) AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo NOT IN (?)) AND aa.adeudo = 0 AND aa.pagado > 1 
      GROUP BY p.id_plantel;
      `,[ciclos,ciclos],(err,res)=>{
        if(err){
          return reject({ message: err.sqlMessage });
          return;
        }
        resolve(res)
      });
  })
}

dashboarPersonal.getNiAlumnosSiguientes = (ciclos, fecha) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT count(DISTINCT ga.id_alumno) as total, p.id_plantel, p.plantel, ga.fecha_alta FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo IN (?) AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo NOT IN (?)) AND aa.adeudo = 0 AND aa.pagado > 1 
      AND DATE(ga.fecha_alta) <= ?
      GROUP BY ga.id_alumno;
      `,[ciclos,ciclos,fecha],(err,res)=>{
        if(err){
          return reject({ message: err.sqlMessage });
          return;
        }
        resolve(res)
      });
  })
}

dashboarPersonal.getContactosPorFecha = (fechaCicloActual,fechaFinCicloActual) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM prospectos WHERE DATE(fecha_creacion) BETWEEN ? AND ?`,[ fechaCicloActual, fechaFinCicloActual ],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  });
};

dashboarPersonal.getDiaClase = ( ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT (ELT(WEEKDAY(CURDATE()) + 1,'domingo' ,'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado')) AS dia,
      (ELT(WEEKDAY(CURDATE()) + 1,7,1,2,3,5,4,5)) AS dia_numero;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    });
  });
};

dashboarPersonal.getClasesFast = ( ciclos, dia ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id AS id_grupo,c.id AS id_ciclo, g.nombre AS grupo, g.id_plantel
      FROM grupos g
      LEFT JOIN cursos cu         ON cu.id = g.id_curso
      LEFT JOIN frecuencia f      ON f.id = cu.id_frecuencia
      LEFT JOIN horarios h        ON h.id = g.id_horario
      LEFT JOIN ciclos c          ON c.id = g.id_ciclo
      WHERE c.iderp IN ( ? ) AND ${ dia } = 1 AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0;`,[ ciclos, dia ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    });
  });
};

dashboarPersonal.getClasesInbi = ( ciclos, dia ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id AS id_grupo,c.id AS id_ciclo, g.nombre AS grupo, g.id_plantel
      FROM grupos g
      LEFT JOIN cursos cu         ON cu.id = g.id_curso
      LEFT JOIN frecuencia f      ON f.id = cu.id_frecuencia
      LEFT JOIN horarios h        ON h.id = g.id_horario
      LEFT JOIN ciclos c          ON c.id = g.id_ciclo
      WHERE c.iderp IN ( ? ) AND ${ dia } = 1 AND g.editado = 1 AND g.optimizado = 0 AND g.deleted = 0;`,[ ciclos, dia ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    });
  });
};

dashboarPersonal.getClasesBuenasFast = ( ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id, g.id_plantel FROM indicador_clases i 
      LEFT JOIN grupos g ON g.id = i.id_grupo 
      WHERE DATE(i.fecha_creacion) = DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY);`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    });
  });
};


dashboarPersonal.getClasesEvaluadas = ( ) => {
   return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT cr.idcalidad_eval_clase, SUM(cr.valor) AS calificacion, ce.id_grupo, ce.unidad_negocio FROM calidad_res_clase cr
      LEFT JOIN calidad_eval_clase ce ON ce.idcalidad_eval_clase = cr.idcalidad_eval_clase
      WHERE cr.estatus = 1 AND DATE(ce.fecha_creacion) = DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY)
      GROUP BY cr.idcalidad_eval_clase;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    });
  });
};

dashboarPersonal.getGruposInbi = ( idgrupos ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM grupos WHERE id IN ( ? )`, [ idgrupos ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    });
  });
};

dashboarPersonal.getGruposFast = ( idgrupos ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM grupos WHERE id IN ( ? )`, [ idgrupos ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    });
  });
};


dashboarPersonal.addNumeroVacantes = (u, result) => {
  sqlERPNUEVO.query(`INSERT INTO numero_vacantes(numero_vacantes,contratados)VALUES(?,?)`,[ u.numero_vacantes, u.contratados ],
  (err, res) => {
    if (err) { 
      result(err, null); 
      return; 
    }
    result(null, { id: res.insertId, ...u });
  })
}

dashboarPersonal.getnumeroVacantes = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM numero_vacantes WHERE deleted = 0`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }
     result(null, res);
  });
};


dashboarPersonal.getNumeroVacantes = (  ) => {
   return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM numero_vacantes WHERE DATE(fecha_creacion) = DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY) LIMIT 1;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    });
  });
};

dashboarPersonal.getEgresos = ( ciclos ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`select SUM(costo_total) AS suma, t1.id, t2.id_plantel, t2.concepto, t9.id_ciclo,
      case when t10.nuevo_monto is null then 'NO' else 'SI' end as costo_anterior
      from fyn_proc_egresos t1
      left join requisicion_compra_detalle t2 on t1.id_requisicion_compra_detalle = t2.id
      left join requisicion_compra t3 on t2.id_requisicion_compra = t3.id
      left join requisicion_compra_estatus t4 on t3.id_requisicion_compra_estatus = t4.id 
      left join ciclos t9 on t3.id_ciclo = t9.id_ciclo
      left join (select t1.nuevo_monto, t1.descripcion, t1.cambio, t1.tipo_cambio, t1.comentarios, t1.id_egreso
      from fyn_proc_ajustes_egresos t1
      WHERE t1.id IN(select max(id) as id
      from fyn_proc_ajustes_egresos
      group by id_egreso)) as t10 on t10.id_egreso = t1.id WHERE t9.id_ciclo IN ( ? )
      GROUP BY t2.id_plantel;`,[ ciclos ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  });
};


dashboarPersonal.getIngresos = ( ciclos ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT c.id_ciclo, IFNULL(c.ciclo,"Sin ciclo") AS ciclo, SUM(a.pagado) AS suma, g.id_plantel, IF(LOCATE('FE',c.ciclo)> 0, 2,1) AS escuela FROM alumnos_grupos_especiales_carga a
      LEFT JOIN ciclos c ON c.id_ciclo = a.id_ciclo
      LEFT  JOIN grupos g ON g.id_grupo = a.id_grupo
      WHERE c.activo_sn = 1 AND c.id_ciclo IN ( ? )
      GROUP BY id_plantel;`,[ ciclos ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  });
};


dashboarPersonal.getProspectosVendedoraCantidad = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT COUNT(idprospectos) AS cantidad, usuario_asignado FROM prospectos 
      WHERE WEEK(DATE(fecha_creacion)) = WEEK(NOW()) AND EXTRACT(YEAR FROM fecha_creacion) = EXTRACT(YEAR FROM NOW())
      GROUP BY usuario_asignado;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  });
};


module.exports = dashboarPersonal;
