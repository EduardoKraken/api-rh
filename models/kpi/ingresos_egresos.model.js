const { result } = require("lodash");
const sqlERPViejo      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");


//const constructor
const ingresos_egresos = function(depas) {};

ingresos_egresos.getCiclos = result => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM ciclos WHERE activo_sn = 1 AND ciclo NOT LIKE '%capa%' AND ciclo NOT LIKE '%exci%' AND ciclo NOT LIKE '%indu%' AND ciclo NOT LIKE '%cambios%' ORDER BY ciclo `, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

ingresos_egresos.getEgresos = ( idCiclos ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`select  t1.id, t2.id AS id_partida, p.plantel, t2.concepto, t2.cantidad, t2.costo_unitario,
        case when t10.nuevo_monto is null then t2.costo_total else t10.nuevo_monto end as costo_total,
                t9.id_ciclo,
                case when t10.nuevo_monto is null then 'NO' else 'SI' end as costo_anterior
        from fyn_proc_egresos t1
        left join requisicion_compra_detalle t2 on t1.id_requisicion_compra_detalle = t2.id
        left join requisicion_compra t3 on t2.id_requisicion_compra = t3.id
        left join requisicion_compra_estatus t4 on t3.id_requisicion_compra_estatus = t4.id 
        left join ciclos t9 on t3.id_ciclo = t9.id_ciclo
        LEFT JOIN planteles p ON p.id_plantel = t2.id_plantel
        left join (select t1.nuevo_monto, t1.descripcion, t1.cambio, t1.tipo_cambio, t1.comentarios, t1.id_egreso
        from fyn_proc_ajustes_egresos t1
        WHERE t1.id IN(select max(id) as id
        from fyn_proc_ajustes_egresos
        group by id_egreso)) as t10 on t10.id_egreso = t1.id
        WHERE t2.tipo_pago > 0
        AND t9.id_ciclo IN ( ? )
        AND t2.activo_sn = 1;`, [ idCiclos ], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

ingresos_egresos.getIngresos = ( idCiclos ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT sum(i.monto_pagado) as suma, c.id_ciclo, IFNULL(c.ciclo,"Sin ciclo") AS ciclo FROM ingresos i
        LEFT JOIN grupos g ON g.id_grupo = i.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
        WHERE c.activo_sn = 1 AND c.ciclo NOT LIKE '%campa%' AND ciclo NOT LIKE '%exci%' AND ciclo NOT LIKE '%indu%' AND ciclo NOT LIKE '%cambios%' AND c.ciclo NOT LIKE '%capa%'
        GROUP BY g.id_ciclo;`,[ idCiclos ],  (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

module.exports = ingresos_egresos;