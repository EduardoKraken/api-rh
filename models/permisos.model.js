const { result } = require("lodash");
const sql = require("./db.js");

//const constructor
const Permisos = function(permisos) {
  this.idusuario = permisos.idusuario;
  this.iderp = permisos.iderp;
  this.idpuesto = permisos.idpuesto;
};

Permisos.getPermisos = result => {
  sql.query(`SELECT * FROM permisos`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Permisos.addPermisos = (u, result) => {
  sql.query(`INSERT INTO permisos(permiso,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?)`, [u.permiso,u.usuario_registro,u.fecha_creacion,u.fecha_actualizo,u.deleted],
    (err, res) => {
      if (err) { result(err, null); return; }
      result(null, { id: res.insertId, ...u });
    }
  )
}

Permisos.updatePermisos = (id, u, result) => {
  sql.query(` UPDATE permisos SET permiso=?,usuario_registro=?,fecha_creacion=?,fecha_actualizo=?,deleted=? WHERE idpermiso=?`, [u.permiso,u.usuario_registro,u.fecha_creacion,u.fecha_actualizo,u.deleted, id],
    (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id, ...u });
    }
  )
}



module.exports = Permisos;