const sql = require("./db.js");

//const constructor
const Unidad_negocio = function(unidad_negocio) {
    this.idunidad_negocio = unidad_negocio.idunidad_negocio;
    this.unidad_negocio = unidad_negocio.unidad_negocio;
    this.usuario_registro = unidad_negocio.usuario_registro;
    this.fecha_creacion = unidad_negocio.fecha_creacion;
    this.fecha_actualizo = unidad_negocio.fecha_actualizo;
    this.deleted = unidad_negocio.deleted;
};

Unidad_negocio.all_unidad_negocio = result => {
    sql.query(`SELECT * FROM unidad_negocio WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

Unidad_negocio.add_unidad_negocio = (un, result) => {
    sql.query(`INSERT INTO unidad_negocio(unidad_negocio,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?)`, [un.unidad_negocio, un.usuario_registro, un.fecha_creacion, un.fecha_actualizo],
        (err, res) => {
            if (err) { result(err, null); return; }

            result(null, { id: res.insertId, ...un });
        });
};

Unidad_negocio.update_unidad_negocio = (id, un, result) => {
    sql.query(` UPDATE unidad_negocio SET unidad_negocio=?,usuario_registro=?,fecha_creacion=?,fecha_actualizo=?
                WHERE idunidad_negocio = ?`, [un.unidad_negocio, un.usuario_registro, un.fecha_creacion, un.fecha_actualizo, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }
            result(null, { id: id, ...un });
        }
    );
};


module.exports = Unidad_negocio;