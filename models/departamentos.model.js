const { result } = require("lodash");
const sql = require("./db.js");

//const constructor
const departamentos = function(depas) {};

departamentos.getDepartamentos = result => {
  sql.query(`SELECT * FROM departamento WHERE deleted = 0`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

departamentos.addDepartamento = (z, result) => {
  sql.query(`INSERT INTO departamento(departamento,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?)`, [z.departamento, z.usuario_registro, z.fecha_creacion, z.fecha_actualizo],
    (err, res) => {
      if (err) { result(err, null); return; }
      result(null, { id: res.insertId, ...z });
    }
  )
}

departamentos.updateDepartamento = (id, z, result) => {
  sql.query(`UPDATE departamento SET departamento = ?,usuario_registro = ?,fecha_actualizo = ?, deleted = ? WHERE iddepartamento = ?`, 
    [z.departamento, z.usuario_registro, z.fecha_actualizo, z.deleted, id],
    (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id, ...z });
  })
}

departamentos.listDepartamentos = ( ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM departamento WHERE deleted = 0;`, 
       (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve( res );
    })
  })
};

departamentos.listPuestos = ( ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT p.*, d.departamento FROM puesto p 
      LEFT JOIN departamento d ON d.iddepartamento = p.iddepartamento
      WHERE p.deleted  = 0;`, (err, res) => {

      if (err) {
        return reject({ message: err.sqlMessage });
      }
      
      resolve( res );
    })
  })
};


module.exports = departamentos;