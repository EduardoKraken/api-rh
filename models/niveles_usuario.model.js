const { result } = require("lodash");
const sql = require("./db.js");

//const constructor
const NivelesUsuario = function(permisos) {
  this.idusuario = permisos.idusuario;
  this.iderp = permisos.iderp;
  this.idpuesto = permisos.idpuesto;
};

NivelesUsuario.getNivelesUsuario = result => {
  sql.query(`SELECT * FROM niveles_usuario`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};


NivelesUsuario.getNivelesUsuarioId = (id,result) => {
  sql.query(`SELECT * FROM niveles_usuario WHERE idusuario = ?`,[id], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};


NivelesUsuario.addNivelesUsuario = (u, result) => {
  sql.query(`INSERT INTO niveles_usuario(idusuario,nivel,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?,?)`,
   [u.idusuario,u.nivel,u.usuario_registro,u.fecha_creacion,u.fecha_actualizo,u.deleted],
    (err, res) => {
      if (err) { result(err, null); return; }
      result(null, { id: res.insertId, ...u });
    }
  )
}

NivelesUsuario.updateNivelesUsuario = (id, u, result) => {
  sql.query(` UPDATE niveles_usuario SET nivel=?,usuario_registro=?,fecha_creacion=?,fecha_actualizo=?,deleted=? WHERE idniveles_usuario=?`, 
    [u.nivel,u.usuario_registro,u.fecha_creacion,u.fecha_actualizo,u.deleted, id],
    (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id, ...u });
    }
  )
}


NivelesUsuario.deleteNivelesUsuario = (id, result) => {
  sql.query("DELETE FROM niveles_usuario WHERE idniveles_usuario = ?", id, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, res);
  });
};

/******************************************************************************************************************************/
/******************************************************************************************************************************/
/******************************************************************************************************************************/

NivelesUsuario.getNiveles = result => {
  sql.query(`SELECT * FROM niveles`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

NivelesUsuario.addNiveles= (u, result) => {
  sql.query(`INSERT INTO niveles(nivel,nombrenivel,prioridad,fecha_registro,fecha_actualizo,usuario_registro,deleted,unidad_negocio)VALUES(?,?,?,?,?,?,?,?)`, 
    [u.nivel,u.nombrenivel,u.prioridad,u.fecha_registro,u.fecha_actualizo,u.usuario_registro,u.deleted,u.unidad_negocio],
    (err, res) => {
      if (err) { result(err, null); return; }

      result(null, { id: res.insertId, ...u });
    }
  )
}

NivelesUsuario.updateNiveles= (id, u, result) => {
  sql.query(` UPDATE niveles SET nivel=?,nombrenivel=?,prioridad=?,fecha_registro=?,fecha_actualizo=?,usuario_registro=?,deleted=?,unidad_negocio=? WHERE idniveles=?`,
   [u.nivel,u.nombrenivel,u.prioridad,u.fecha_registro,u.fecha_actualizo,u.usuario_registro,u.deleted,u.unidad_negocio,id],
    (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id, ...u });
    }
  )
}


module.exports = NivelesUsuario;