const { result } = require("lodash");
const sql = require("../db.js");


//const constructor
const TipoPregunta = function(reguntas) {};

TipoPregunta.getTipoPreguntas = result => {
    sql.query(`SELECT * FROM tipo_pregunta WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

TipoPregunta.addTipoPregunta = (c, result) => {
    sql.query(`INSERT INTO tipo_pregunta(tipo_pregunta,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?)`, [c.tipo_pregunta, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo],
        (err, res) => {
            if (err) { result(err, null); return; }
            result(null, { id: res.insertId, ...c });
        }
    )
}

TipoPregunta.updateTipoPregunta = (id, c, result) => {
    sql.query(` UPDATE tipo_pregunta SET tipo_pregunta=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idtipo_pregunta=?`, [c.tipo_pregunta, c.usuario_registro, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            result(null, { id: id, ...c });
        })
}



module.exports = TipoPregunta;