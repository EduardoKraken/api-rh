const { result } = require("lodash");
const sql = require("../db.js");


//const constructor
const TipoEvaluacion = function(tipo_evaluacion) {};

TipoEvaluacion.getTipoEvaluacion = result => {
    sql.query(`SELECT * FROM tipo_evaluacion WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

TipoEvaluacion.addTipoEvaluacion = (c, result) => {
    sql.query(`INSERT INTO tipo_evaluacion(tipo_evaluacion,descripcion,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?,?)`, [c.tipo_evaluacion, c.descripcion, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo],
        (err, res) => {
            if (err) { result(err, null); return; }
            result(null, { id: res.insertId, ...c });
        }
    )
}

TipoEvaluacion.updateTipoEvaluacion = (id, c, result) => {
    sql.query(` UPDATE tipo_evaluacion SET tipo_evaluacion=?,descripcion=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idtipo_evaluacion=?`, [c.tipo_evaluacion, c.descripcion, c.usuario_registro, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            result(null, { id: id, ...c });
        })
}



module.exports = TipoEvaluacion;