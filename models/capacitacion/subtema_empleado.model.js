const { result } = require("lodash");
const sql = require("../db.js");


//const constructor
const SubtemaEmpleado = function(subtemaEmpleado) {};

SubtemaEmpleado.getSubtemaEmpleados = result => {
    sql.query(`SELECT * FROM subtema_empleado WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

SubtemaEmpleado.addSubtemaEmpleado = (c, result) => {
    sql.query(`INSERT INTO subtema_empleado(estatus,idsubtema,idempleado,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?,?,?)`, [c.estatus, c.idsubtema, c.idempleado, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo],
        (err, res) => {
            if (err) { result(err, null); return; }
            result(null, { id: res.insertId, ...c });
        }
    )
}

SubtemaEmpleado.updateSubtemaEmpleado = (id, c, result) => {
    sql.query(` UPDATE subtema_empleado SET estatus=?,idsubtema=?,idempleado=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idsubtema_empleado=?`, [c.estatus, c.idsubtema, c.idempleado, c.usuario_registro, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            result(null, { id: id, ...c });
        })
}



module.exports = SubtemaEmpleado;