const { result } = require("lodash");
const sql = require("../db.js");
const sqlErp = require("../db3.js");


//const constructor
const Calificaciones = function(calificaciones) {};

Calificaciones.getCalificaciones = result => {
  sql.query(`SELECT * FROM calificaciones WHERE deleted = 0`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Calificaciones.addCalificacion = ( calificacion, calificacion_total, numero_intento, idevaluacion, idempleado, usuario_registro ) => {
  return new Promise(( resolve, reject )=>{
    sql.query(`INSERT INTO calificaciones(calificacion,calificacion_total,numero_intento,idevaluacion,idempleado,usuario_registro)VALUES(?,?,?,?,?,?)`,
    [ calificacion, calificacion_total, numero_intento, idevaluacion, idempleado, usuario_registro ],
    (err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve({ id: res.insertId });
    })
  })
}

Calificaciones.updateCalificacion = (id, c, result) => {
  sql.query(` UPDATE calificaciones SET calificacion=?,calificacion_total=?,numero_intento=?,idevaluacion=?,idempleado=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idcalificacion=?;`, [c.calificacion, c.calificacion_total, c.numero_intento, c.idevaluacion, c.idempleado, c.usuario_registro, c.fecha_actualizo, c.deleted, id],
    (err, res) => {
    if (err) { result(null, err); return; }
    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }

    result(null, { id: id, ...c });
  })
}

//obtener todos los intentos por evaluacion y empleado
Calificaciones.getIntentosCalificacion = ( idempleado, idevaluacion ) => {
  return new Promise( (resolve, reject) =>{
    sql.query(`SELECT COUNT(*) AS cant_intentos FROM calificaciones WHERE idevaluacion=? AND idempleado=? AND deleted = 0;`, [ idevaluacion, idempleado ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0]);
    });
  })
};

Calificaciones.modificarString = ( ) => {
  return new Promise( (resolve, reject) =>{
    sql.query(`ALTER TABLE respuesta_empleado MODIFY COLUMN pregunta TEXT 
      CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  })
};


//obtener todos los intentos por evaluacion y empleado
Calificaciones.getIntentosUsuario = ( idempleado, idevaluacion ) => {
  return new Promise( (resolve, reject) =>{
    sql.query(`SELECT MAX(calificacion) AS calificacion FROM calificaciones WHERE idevaluacion=? AND idempleado=? AND deleted = 0;`, [ idevaluacion, idempleado ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0]);
    });
  })
};



Calificaciones.getCursosEmpledado = ( ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT u.iderp AS idempleado, c.curso, c.idcurso, p.puesto, p.idpuesto FROM curso_empleado ce
      LEFT JOIN cursos c ON c.idcurso = ce.idcurso
      LEFT JOIN usuarios u ON u.idusuario = ce.idempleado
      LEFT JOIN puesto p ON p.idpuesto = u.idpuesto
      WHERE ce.deleted = 0;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  })
};

Calificaciones.getCanEvaluacionesCurso = ( ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT COUNT( e.idevaluacion ) AS total_evaluaciones, c.idcurso FROM evaluaciones e
      LEFT JOIN subtemas s ON s.idsubtema = e.idsubtema
      LEFT JOIN temas t    ON t.idtema = s.idtema
      LEFT JOIN cursos c   ON c.idcurso = t.idcurso
      WHERE e.deleted = 0
      GROUP BY c.idcurso;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  })
};

Calificaciones.getCalificacionesEmpleados = ( ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT c.idcurso, AVG(((ca.calificacion / ca.calificacion_total) * 100)) AS calificacion, COUNT(c.idcurso) AS total_calificaciones, u.iderp AS idempleado, c.curso 
      FROM cursos c 
      JOIN temas t ON c.idcurso = t.idcurso
      JOIN subtemas st ON t.idtema = st.idtema 
      JOIN evaluaciones e ON st.idsubtema = e.idsubtema 
      JOIN calificaciones ca ON e.idevaluacion= ca.idevaluacion 
      LEFT JOIN usuarios u ON u.idusuario = ca.idempleado
      WHERE c.deleted=0 AND e.deleted=0 AND ((ca.calificacion / ca.calificacion_total) * 100) >=70 
      GROUP BY c.idcurso, ca.idempleado
      ORDER BY c.idcurso;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  })
};

Calificaciones.getCalificacionesEvaluacionesEmpleados = ( ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT ca.idevaluacion, AVG(((ca.calificacion / ca.calificacion_total) * 100)) AS calificacion, COUNT(c.idcurso) AS total_calificaciones, u.iderp AS idempleado, e.evaluacion, st.subtema, t.tema, c.idcurso, c.curso, ca.numero_intento
      FROM cursos c 
      JOIN temas t ON c.idcurso = t.idcurso
      JOIN subtemas st ON t.idtema = st.idtema 
      JOIN evaluaciones e ON st.idsubtema = e.idsubtema 
      JOIN calificaciones ca ON e.idevaluacion= ca.idevaluacion 
      LEFT JOIN usuarios u ON u.idusuario = ca.idempleado
      WHERE c.deleted=0 AND e.deleted=0 AND ((ca.calificacion / ca.calificacion_total) * 100) >=70 
      GROUP BY ca.idevaluacion, ca.idempleado
      ORDER BY c.idcurso;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  })
};

Calificaciones.usuariosNuevoERP = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT u.*, p.puesto FROM usuarios u
      LEFT JOIN puesto p ON p.idpuesto = u.idpuesto;`,(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};



Calificaciones.getPuestos = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM puesto WHERE deleted = 0 ORDER BY puesto;`,(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

Calificaciones.habilitarGroupERP = () => {
   return new Promise((resolve,reject)=>{
    sql.query(`SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));`,(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};


module.exports = Calificaciones;