const { result } = require("lodash");
const sql = require("../db.js");


//const constructor
const materiales = (materiales) => {};

materiales.getMateriales = result => {
    sql.query(`SELECT * FROM materiales WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

materiales.addMaterial = (c, result) => {
    sql.query(`INSERT INTO materiales(idtipo_material,url,nombre,idsubtema,usuario_registro,extension)VALUES(?,?,?,?,?,?)`, [c.idtipo_material, c.url, c.nombre, c.idsubtema, c.usuario_registro, c.extension],
        (err, res) => {
            if (err) { result(err, null); return; }
            result(null, { id: res.insertId, ...c });
        }
    )
}

materiales.updateMaterial = (id, c, result) => {
  sql.query(` UPDATE materiales SET idtipo_material=?,url=?,idsubtema=?,nombre = ?, usuario_registro=?,deleted=? WHERE idmaterial=?`, [c.idtipo_material, c.url, c.idsubtema, c.nombre, c.usuario_registro, c.deleted, id],
    (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id, ...c });
  })
}

materiales.deleteMaterial = (id, result) => {
  sql.query(` UPDATE materiales SET deleted = 1 WHERE idmaterial=?`, [id],
    (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id});
  })
}



materiales.getMaterialesSubtema = (id, result) => {
  sql.query(`SELECT m.*, tm.tipo_material, s.subtema FROM materiales m 
    LEFT JOIN tipo_material tm ON m.idtipo_material = tm.idtipo_material 
    LEFT JOIN subtemas s ON s.idsubtema = m.idsubtema 
    WHERE m.deleted=0 AND m.idsubtema = ?`, [id], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

materiales.getMaterialId = ( idmaterial ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT m.*, s.subtema, t.tema, c.curso, c.duracion, c.descripcion, c.idcapacitador, c.imagen FROM materiales m
      LEFT JOIN subtemas s ON s.idsubtema = m.idsubtema 
      LEFT JOIN temas t ON t.idtema = s.idtema
      LEFT JOIN cursos c ON c.idcurso = t.idcurso
      WHERE m.idmaterial = ?;`,[ idmaterial ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res[0])
    })
  })
}

materiales.getComentariosMaterial = (c, result) => {
  sql.query(`SELECT * FROM notas_capacitacion WHERE deleted = 0 AND iderp = ? AND idmaterial = ?;`, [c.iderp, c.idmaterial],
    (err, res) => {
    if (err) {
      return result(null, err);
    }
    result(null, res);
  })
}

materiales.getComentariosMaterialAll = (c, result) => {
  sql.query(`SELECT * FROM notas_capacitacion WHERE deleted = 0 AND idmaterial = ?;`, [ c.idmaterial],
    (err, res) => {
    if (err) {
      return result(null, err);
    }
    result(null, res);
  })
}


materiales.addComentariosMaterial = (c, result) => {
  sql.query(`INSERT INTO notas_capacitacion(idmaterial,iderp,nota)VALUES(?,?,?)`, [c.idmaterial, c.iderp, c.nota],
  (err, res) => {
    if (err) { result(err, null); return; }
    result(null, { id: res.insertId, ...c });
  })
}

materiales.getcomentariosUsuarios = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT n.idnotas_capacitacion, n.iderp, c.idcurso, c.curso, t.tema, s.subtema, m.nombre AS material, n.nota FROM notas_capacitacion n
      LEFT JOIN materiales m ON m.idmaterial = n.idmaterial
      LEFT JOIN subtemas s ON s.idsubtema = m.idsubtema
      LEFT JOIN temas t ON t.idtema = s.idtema
      LEFT JOIN cursos c ON c.idcurso = t.idcurso
      WHERE iderp;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

module.exports = materiales;