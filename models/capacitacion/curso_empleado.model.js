const { result } = require("lodash");
const sql = require("../db.js");
const sqlErp = require("../db3.js");
const moment = require("moment");
var hoy = moment().format("YYYY-MM-DD");
//const constructor
const CursoEmpleado = function(curso_empleado) {};

CursoEmpleado.getCursoEmpleado = result => {
    sql.query(`SELECT * FROM curso_empleado WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        var cursos_emp = [];

        for (const i in res) {
            //declaro el objeto que utilizare para llenar el arreglo
            var datos = {
                idcurso_empleado: res[i].idcurso_empleado,
                valoracion: res[i].valoracion,
                favorito: res[i].favorito,
                fecha_inicio: res[i].fecha_inicio,
                fecha_final: res[i].fecha_final,
                idasignacion_curso: res[i].idasignacion_curso,
                idcurso: res[i].idcurso,
                curso: "",
                idempleado: res[i].idempleado,
                empleado: "",
                usuario_registro: res[i].usuario_registro,
                fecha_creacion: res[i].fecha_creacion,
                fecha_actualizo: res[i].fecha_actualizo,
                deleted: res[i].deleted,
            }
            cursos_emp.push(datos);
        }

        sqlErp.query(`SELECT * FROM usuarios WHERE activo_sn = 1 ORDER BY nombre_completo`, (err, res) => {
            if (err) {
                result(null, err);
                return;
            }

            for (const i in cursos_emp) {
                for (const j in res) {
                    if (cursos_emp[i].idempleado == res[j].id_usuario) {
                        cursos_emp[i].empleado = res[j].nombre_completo;
                    }
                }
            }


        });


        sql.query(`SELECT * FROM cursos WHERE deleted = 0`, (err, res) => {
            if (err) {
                result(null, err);
                return;
            }

            for (const i in cursos_emp) {
                for (const j in res) {
                    if (cursos_emp[i].idcurso == res[j].idcurso) {
                        cursos_emp[i].curso = res[j].curso;
                    }
                }
            }

            result(null, cursos_emp);
        });
    });
};

CursoEmpleado.getCursoEmpleadoId = (id, result) => {
    sql.query(`SELECT * FROM curso_empleado WHERE deleted = 0 AND idempleado = ?`, [id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        var cursos_emp = [];

        for (const i in res) {
            //declaro el objeto que utilizare para llenar el arreglo
            var datos = {
                idcurso_empleado: res[i].idcurso_empleado,
                valoracion: res[i].valoracion,
                favorito: res[i].favorito,
                fecha_inicio: res[i].fecha_inicio,
                fecha_final: res[i].fecha_final,
                idasignacion_curso: res[i].idasignacion_curso,
                idcurso: res[i].idcurso,
                curso: "",
                idempleado: res[i].idempleado,
                empleado: "",
                usuario_registro: res[i].usuario_registro,
                fecha_creacion: res[i].fecha_creacion,
                fecha_actualizo: res[i].fecha_actualizo,
                deleted: res[i].deleted,
            }
            cursos_emp.push(datos);
        }

        sqlErp.query(`SELECT * FROM usuarios WHERE activo_sn = 1 ORDER BY nombre_completo`, (err, res) => {
            if (err) {
                result(null, err);
                return;
            }

            for (const i in cursos_emp) {
                for (const j in res) {
                    if (cursos_emp[i].idempleado == res[j].id_usuario) {
                        cursos_emp[i].empleado = res[j].nombre_completo;
                    }
                }
            }


        });


        sql.query(`SELECT * FROM cursos WHERE deleted = 0`, (err, res) => {
            if (err) {
                result(null, err);
                return;
            }

            for (const i in cursos_emp) {
                for (const j in res) {
                    if (cursos_emp[i].idcurso == res[j].idcurso) {
                        cursos_emp[i].curso = res[j].curso;
                    }
                }
            }

            result(null, cursos_emp);
        });

    });
};

CursoEmpleado.addCursoEmpleado = (c, result) => {
    sql.query(`INSERT INTO curso_empleado(valoracion,favorito,idcurso,idempleado,idasignacion_curso,fecha_inicio,fecha_final,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?,?,?,?,?,?,?)`, [c.valoracion, c.favorito, c.idcurso, c.idempleado, c.idasignacion_curso, c.fecha_inicio, c.fecha_final, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo, c.deleted],
        (err, res) => {
            if (err) { result(err, null); return; }
            result(null, { id: res.insertId, ...c });
        }
    )
}

CursoEmpleado.updateCursoEmpleado = (id, c, result) => {
    sql.query(` UPDATE curso_empleado SET valoracion=?,favorito=?,idcurso=?,idempleado=?,idasignacion_curso=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idcurso_empleado=?`, [c.valoracion, c.favorito, c.idcurso, c.idempleado, c.idasignacion_curso, c.usuario_registro, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            result(null, { id: id, ...c });
        })
}

//obtener cursos de cada empleado
CursoEmpleado.getCursoEmpleadoId = ( id ) => {
  return new Promise( (resolve, reject ) =>{
    sql.query(`SELECT distinct c.*,ce.fecha_inicio,ce.fecha_final, u.iderp AS id_usucario_capacitor, u2.iderp AS id_usuario_registro,
      (SELECT COUNT(*) FROM evaluaciones WHERE idsubtema IN (
      SELECT idsubtema FROM subtemas WHERE idtema IN (
      SELECT idtema FROM temas WHERE idcurso = c.idcurso)) AND deleted = 0) AS cantidad_evaluaciones,
      (SELECT COUNT(*) FROM calificaciones WHERE calificacion >= 70
      AND idevaluacion IN (SELECT idevaluacion FROM evaluaciones WHERE idsubtema IN (
      SELECT idsubtema FROM subtemas WHERE idtema IN (
      SELECT idtema FROM temas WHERE idcurso = c.idcurso)) AND deleted = 0)
      AND idempleado = ?) AS evaluaciones_contestadas, u3.iderp AS iderp, r.idcurso AS inscrito
      FROM curso_empleado ce 
      LEFT JOIN cursos c    on ce.idcurso = c.idcurso
      LEFT JOIN usuarios u  on u.idusuario = c.idcapacitador
      LEFT JOIN usuarios u2 on u2.idusuario = c.usuario_registro
      LEFT JOIN usuarios u3 on u3.idusuario = ce.idempleado
      LEFT JOIN registro_curso r ON r.iderp = u3.iderp AND r.idcurso = c.idcurso 
      WHERE ce.idempleado = ?;`, [id, id], (err, res) => {
      if ( err ) {
        return reject( err.sqlMessage );
      }

      resolve ( res )
    });
  })
};

// Var la cantidad de ejercicos por curso


CursoEmpleado.getCursoIdEmpleadoId = (c, result) => {
  sql.query(`SELECT * FROM curso_empleado WHERE deleted=0 AND idempleado = ? AND idcurso = ?`, [c.idempleado, c.idcurso], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};


CursoEmpleado.inscribirTeacherCurso = ( iderp, idcurso ) => {
  return new Promise( (resolve, reject ) =>{
    sql.query(`INSERT INTO registro_curso( iderp, idcurso ) VALUES(?,?)`, [iderp, idcurso], (err, res) => {
      if ( err ) {
        return reject( err.sqlMessage );
      }

      resolve ( res )
    });
  })
};

CursoEmpleado.getInscribirTeacherCurso = (  ) => {
  return new Promise( (resolve, reject ) =>{
    sql.query(`SELECT * FROM registro_curso;`,(err, res) => {
      if ( err ) {
        return reject( err.sqlMessage );
      }

      resolve ( res )
    });
  })
};

module.exports = CursoEmpleado;