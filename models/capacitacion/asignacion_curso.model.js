const { result } = require("lodash");
const sql = require("../db.js");
const sqlErp = require("../db3.js");

//const constructor
const asignacion = function(asignacion) {};

asignacion.getAsignacion = result => {
  sql.query(`SELECT * FROM asignacion_cursos WHERE deleted = 0`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }

    var asignacion = [];
    var tipo = "";
    var gral = "";

    for (const i in res) {

      //guardar el nombre del tipo de asigancion
      switch (res[i].tipo_asignacion) {
        case 1:
          tipo = "General"
          gral = "Sí";
        break;
        case 2:
          tipo = "Departamento"
        break;
        case 3:
          tipo = "Puesto"
          break
        case 4:
          tipo = "Empleado"
          break;
        default:
      }

      //declaro el objeto que utilizare para llenar el arreglo
      var datos = {
        idasignacion_curso: res[i].idasignacion_curso,
        idcurso: res[i].idcurso,
        curso: "",
        tipo_asignacion: res[i].tipo_asignacion,
        tipo: tipo,
        general: res[i].general,
        gral: gral,
        iddepartamento: res[i].iddepartamento,
        departamento: "",
        idpuesto: res[i].idpuesto,
        puesto: "",
        idempleado: res[i].idempleado,
        empleado: "",

        fecha_inicio: res[i].fecha_inicio,
        fecha_final: res[i].fecha_final,
        usuario_registro: res[i].usuario_registro,
        fecha_creacion: res[i].fecha_creacion,
        fecha_actualizo: res[i].fecha_actualizo,
        deleted: res[i].deleted,
      }
      asignacion.push(datos);
    }

    sql.query(`SELECT * FROM departamento  WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        for (const i in asignacion) {
            for (const j in res) {
                if (asignacion[i].iddepartamento === res[j].iddepartamento) {
                    asignacion[i].departamento = res[j].departamento
                }
            }
        }

        sql.query(`SELECT * FROM puesto WHERE deleted = 0`, (err, res) => {
            if (err) {
                result(null, err);
                return;
            }
            for (const i in asignacion) {
                for (const j in res) {
                    if (asignacion[i].idpuesto === res[j].idpuesto) {
                        asignacion[i].puesto = res[j].puesto
                    }
                }
            }



            sql.query(`SELECT * FROM usuarios WHERE deleted = 0`, (err, res) => {
                if (err) {
                    result(null, err);
                    return;
                }

                //
                var usuarios = [];

                for (const i in res) {
                    //declaro el objeto que utilizare para llenar el arreglo
                    var datos = {
                        idusuario: res[i].idusuario,
                        iderp: res[i].iderp,
                        empleado: "",
                        idpuesto: res[i].idpuesto,

                    }
                    usuarios.push(datos);
                }

                sqlErp.query(`SELECT * FROM usuarios WHERE activo_sn = 1 ORDER BY nombre_completo`, (err, res) => {
                    if (err) {
                        result(null, err);
                        return;
                    }

                    for (const i in usuarios) {
                        for (const j in res) {
                            if (usuarios[i].iderp == res[j].id_usuario) {
                                usuarios[i].empleado = res[j].nombre_completo;
                            }
                        }
                    }

                    for (const i in asignacion) {
                        for (const j in usuarios) {
                            if (asignacion[i].idempleado == usuarios[j].idusuario) {
                                asignacion[i].empleado = usuarios[j].empleado;
                            }
                        }
                    }


                    sql.query(`SELECT * FROM cursos WHERE deleted = 0`, (err, res) => {
                        if (err) {
                            result(null, err);
                            return;
                        }
                        for (const i in asignacion) {
                            for (const j in res) {
                                if (asignacion[i].idcurso == res[j].idcurso) {
                                    asignacion[i].curso = res[j].curso;

                                }
                            }
                        }

                        result(null, asignacion);
                    });
                });
            });
        });
    });

  });
};

// asignacion.addAsignacion = (c, result) => {
//   sql.query(`INSERT INTO asignacion_cursos (tipo_asignacion,idcurso,general,iddepartamento,idpuesto,idempleado,fecha_inicio,fecha_final,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?,?,?,?,?,?,?,?)`, 
//     [c.tipo_asignacion, c.idcurso, c.general, c.iddepartamento, c.idpuesto, c.idempleado, c.fecha_inicio, c.fecha_final, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo, c.deleted],(err, res) => {
//       if (err) { result(err, null); return; }
//       result(null, { id: res.insertId, ...c });
//     }
//   )
// }

asignacion.updateAsignacion = (id, c, result) => {
  sql.query(` UPDATE asignacion_cursos SET tipo_asignacion=?,idcurso=?,general=?,iddepartamento=?,idpuesto=?,idempleado=?,fecha_inicio=?, fecha_final=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idasignacion_curso=?`, 
    [c.tipo_asignacion, c.idcurso, c.general, c.iddepartamento, c.idpuesto, c.idempleado, c.fecha_inicio, c.fecha_final, c.usuario_registro, c.fecha_actualizo, c.deleted, id],(err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...c });
  })
}

asignacion.getCursoEmpleado = ( idempleado, idcurso ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM curso_empleado WHERE deleted = 0 AND idempleado = ${ idempleado } AND idcurso = ${ idcurso };`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  });
};

asignacion.addAsignacion = ( c ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO asignacion_cursos (tipo_asignacion,idcurso,general,iddepartamento,idpuesto,idempleado,fecha_inicio,fecha_final,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?,?,?,?,?,?,?,?)`, 
      [c.tipo_asignacion, c.idcurso, c.general, c.iddepartamento, c.idpuesto, c.idempleado, c.fecha_inicio, c.fecha_final, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo, c.deleted],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve({ idasignacion: res.insertId, ...c })
    })
  })
}

asignacion.addCursoEmpleado = ( c, idasignacion_curso ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO curso_empleado(valoracion,favorito,idcurso,idempleado,idasignacion_curso,fecha_inicio,fecha_final,usuario_registro,deleted)VALUES(?,?,?,?,?,?,?,?,?)`, 
      [0, 0, c.idcurso, c.idempleado, idasignacion_curso, c.fecha_inicio, c.fecha_final, c.usuario_registro, c.deleted],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve({ id: res.insertId, ...c })
    })
  })
}


module.exports = asignacion;