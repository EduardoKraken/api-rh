const { result } = require("lodash");
const sql        = require("../db.js");
const sqlERP     = require("../db3.js");


//const constructor
const evaluaciones = function(evaluaciones) {};

evaluaciones.getEvaluaciones = result => {
  sql.query(`SELECT * FROM evaluaciones WHERE deleted = 0`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

evaluaciones.addEvaluacion = (c, result) => {
  sql.query(`INSERT INTO evaluaciones(evaluacion,idtipo_evaluacion,idsubtema,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?,?,?)`, [c.evaluacion, c.idtipo_evaluacion, c.idsubtema, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo],
    (err, res) => {
      if (err) { result(err, null); return; }
      result(null, { id: res.insertId, ...c });
    }
    )
}

evaluaciones.updateEvaluacion = (id, c, result) => {
  sql.query(` UPDATE evaluaciones SET evaluacion=?,idtipo_evaluacion=?,idsubtema=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idevaluacion=?`, [c.evaluacion, c.idtipo_evaluacion, c.idsubtema, c.usuario_registro, c.fecha_actualizo, c.deleted, id],
    (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...c });
    })
}


evaluaciones.getEvaluacionesSubtema = (id, result) => {
  sql.query(`SELECT * FROM evaluaciones WHERE idsubtema=? AND deleted = 0`, [id], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

evaluaciones.getMaestrosGrupo = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT u.id_usuario AS id_maestro, g.id_grupo, g.grupo, n.nivel,
      IF(LOCATE('FAST', grupo) > 0,2,IF(LOCATE('TEENS', grupo) > 0,3,1)) AS cursos
      FROM maestrosgrupo mg
      LEFT JOIN grupos g ON g.id_grupo = mg.id_grupo
      LEFT JOIN niveles n ON n.id_nivel = g.id_nivel
      LEFT JOIN fyn_cat_trabajadores t ON t.id = mg.id_maestro
      LEFT JOIN usuarios u ON u.id_trabajador = t.id
      WHERE mg.activo_sn = 1 AND u.id_usuario IN ( ? );`, [id], (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }

      resolve(res)
    });
  })
};

evaluaciones.getSucursalesTeacher = ( id ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT u.iderp, p.plantel FROM planteles_usuario pu
        LEFT JOIN plantel p ON p.idplantel = pu.idplantel
        LEFT JOIN usuarios u ON u.idusuario = pu.idusuario
        WHERE pu.deleted = 0 AND u.iderp IN (?)
        ORDER BY escuela, plantel;`, [id], (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }

      resolve(res)
    });
  })
};


evaluaciones.getHorariosTeacher = ( id ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT d.*, u.iderp FROM disponibilidad_usuario d
      LEFT JOIN usuarios u ON u.idusuario = d.idusuario
      WHERE u.iderp IN (?);`, [id], (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }

      resolve(res)
    });
  })
};


evaluaciones.getCalificacionesTeacher = ( id ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT u.iderp AS idempleado, e.evaluacion, s.subtema, t.tema,
      IF(LOCATE('FAST', t.tema) > 0,2,IF(LOCATE('TEENS', t.tema) > 0,3,1)) AS cursos FROM calificaciones c
      LEFT JOIN evaluaciones e ON e.idevaluacion = c.idevaluacion
      LEFT JOIN subtemas s ON s.idsubtema = e.idsubtema
      LEFT JOIN temas t ON t.idtema = s.idtema
      LEFT JOIN usuarios u ON u.idusuario = c.idempleado
      WHERE c.calificacion_total = 100 AND s.idtema IN (98,99,100);`, [id], (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }

      resolve(res)
    });
  })
};

module.exports = evaluaciones;