const { result } = require("lodash");
const sql = require("../db.js");


//const constructor
const Preguntas = function(reguntas) {};

Preguntas.getPreguntas = result => {
    sql.query(`SELECT * FROM preguntas WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

Preguntas.addPregunta = (c, result) => {
    sql.query(`INSERT INTO preguntas(pregunta,numero_pregunta,valor,autoevaluable,idevaluacion,idtipo_pregunta,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?,?,?,?,?,?)`, [c.pregunta, c.numero_pregunta, c.valor, c.autoevaluable, c.idevaluacion, c.idtipo_pregunta, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo],
        (err, res) => {
            if (err) { result(err, null); return; }
            result(null, { id: res.insertId, ...c });
        }
    )
}

Preguntas.updatePregunta = (id, c, result) => {
    sql.query(` UPDATE preguntas SET pregunta=?,numero_pregunta=?,valor=?,autoevaluable=?,idevaluacion=?,idtipo_pregunta=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idpregunta=?`, [c.pregunta, c.numero_pregunta, c.valor, c.autoevaluable, c.idevaluacion, c.idtipo_pregunta, c.usuario_registro, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            result(null, { id: id, ...c });
        })
}

Preguntas.getPreguntasEvaluacion = (id, result) => {
    sql.query(`SELECT * FROM preguntas WHERE idevaluacion=? AND deleted = 0`, [id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

Preguntas.getPreguntasOpcion = ( id ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT p.*, pm.* from preguntas p JOIN pregunta_multiple pm ON pm.idpregunta = p.idpregunta and p.deleted=0 and p.idevaluacion=? ORDER BY p.numero_pregunta`, [id], (err, res) => {
      if (err) {
        return  reject(err.sqlMessage);
      }
      resolve(res);
    });
  })
};







module.exports = Preguntas;