const { result } = require("lodash");
const sql = require("../db.js");


//const constructor
const PreguntaMultiple = function(pregunta_multiple) {};

PreguntaMultiple.getPreguntaMultiple = result => {
    sql.query(`SELECT * FROM pregunta_multiple WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

PreguntaMultiple.addPreguntaMultiple = (c, result) => {
    sql.query(`INSERT INTO pregunta_multiple(a,b,c,d,opcion_correcta,idpregunta,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?,?,?,?,?,?)`, [c.a, c.b, c.c, c.d, c.opcion_correcta, c.idpregunta, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo],
        (err, res) => {
            if (err) { result(err, null); return; }
            result(null, { id: res.insertId, ...c });
        }
    )
}

PreguntaMultiple.updatePreguntaMultiple = (id, c, result) => {
    sql.query(` UPDATE pregunta_multiple SET a=?,b=?,c=?,d=?,opcion_correcta=?,idpregunta=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idpregunta_multiple=?`, [c.a, c.b, c.c, c.d, c.opcion_correcta, c.idpregunta, c.usuario_registro, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            result(null, { id: id, ...c });
        })
}

PreguntaMultiple.getOpcionMultiplePregunta = (id, result) => {
    sql.query(`SELECT * FROM pregunta_multiple WHERE idpregunta=? AND deleted = 0`, [id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

module.exports = PreguntaMultiple;