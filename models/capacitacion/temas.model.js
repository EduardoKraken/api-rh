const { result } = require("lodash");
const sql = require("../db.js");


//const constructor
const Temas = function(temas) {};

Temas.getTemas = result => {
    sql.query(`SELECT * FROM temas WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

Temas.addTema = (c, result) => {
    sql.query(`INSERT INTO temas(tema,idcurso,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?,?)`, [c.tema, c.idcurso, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo],
        (err, res) => {
            if (err) { result(err, null); return; }
            result(null, { id: res.insertId, ...c });
        }
    )
}

Temas.updateTema = (id, c, result) => {
    sql.query(` UPDATE temas SET tema=?,idcurso=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idtema=?`, [c.tema, c.idcurso, c.usuario_registro, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }
            result(null, { id: id, ...c });
        })
}


Temas.getTemasCurso = ( id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT t.*, c.curso FROM temas t
      LEFT JOIN cursos c ON c.idcurso = t.idcurso WHERE t.idcurso = ? AND t.deleted = 0;`,[ id ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

Temas.getSubtemasTema = ( idtemas ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM subtemas WHERE idtema IN ( ? ) AND deleted = 0;`,[ idtemas ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

Temas.getMaterialesSubtema = ( idsubtemas ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT m.*, s.subtema, t.tema, c.curso, c.duracion, c.descripcion, c.idcapacitador, c.imagen, c.idcurso, t.idtema FROM materiales m
      LEFT JOIN subtemas s ON s.idsubtema = m.idsubtema 
      LEFT JOIN temas t ON t.idtema = s.idtema
      LEFT JOIN cursos c ON c.idcurso = t.idcurso
      WHERE m.idsubtema IN( ? ) AND m.deleted = 0;`,[ idsubtemas ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

Temas.getEvaluacionesSubtema = ( idsubtemas ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM evaluaciones WHERE idsubtema IN( ? ) AND deleted = 0;`,[ idsubtemas ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}


Temas.getPreguntasEvaluacion = ( idevaluaciones ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM preguntas WHERE idevaluacion IN( ? ) AND deleted = 0;`,[ idevaluaciones ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

Temas.getTemasxCurso = (id, result) => {
    sql.query(` SELECT * FROM temas WHERE idcurso=? AND deleted = 0`, [id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);

    });
};


module.exports = Temas;