const { result } = require("lodash");
const sql = require("../db.js");


//const constructor
const Subtemas = function(subtemas) {};

Subtemas.getSubtemas = result => {
    sql.query(`SELECT * FROM subtemas WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

Subtemas.addSubtema = (c, result) => {
    sql.query(`INSERT INTO subtemas(subtema,duracion,idtema,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?,?,?)`, [c.subtema, c.duracion, c.idtema, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo],
        (err, res) => {
            if (err) { result(err, null); return; }
            result(null, { id: res.insertId, ...c });
        }
    )
}

Subtemas.updateSubtema = (id, c, result) => {
    sql.query(` UPDATE subtemas SET subtema=?,duracion=?,idtema=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idsubtema=?`, [c.subtema, c.duracion, c.idtema, c.usuario_registro, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }
            result(null, { id: id, ...c });
        })
}


Subtemas.getSubtemasTema = (id, result) => {
  sql.query(`SELECT s.*, t.tema FROM subtemas s
    LEFT JOIN temas t ON t.idtema = s.idtema WHERE s.idtema = ? AND s.deleted = 0;`, [id], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};


module.exports = Subtemas;