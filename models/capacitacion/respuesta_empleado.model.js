const { result } = require("lodash");
const sql = require("../db.js");


//const constructor
const Respuestas = function(respuesta) {};

Respuestas.getRespuestas = result => {
  sql.query(`SELECT * FROM respuesta_empleado WHERE deleted = 0`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Respuestas.addRespuesta = ( p, idempleado, usuario_registro ) => {
  return new Promise((resolve, reject) => {
    sql.query(`INSERT INTO respuesta_empleado(numero_pregunta,pregunta,valor,autoevaluable,a, b, c, d, opcion_correcta, eleccion,respuesta,idempleado,idevaluacion,idtipo_pregunta,idpregunta,usuario_registro)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
      [p.numero_pregunta, p.pregunta, p.valor, p.autoevaluable, p.a, p.b, p.c, p.d, p.opcion_correcta, p.eleccion, p.respuesta, idempleado, p.idevaluacion, p.idtipo_pregunta, p.idpregunta, usuario_registro],
    (err, res) => {
      if( err ){ return reject({ message: err.sqlMessage }) }
      resolve({ id: res.insertId, ...p });
    })
  })
}

Respuestas.updateRespuesta = (id, c, result) => {
  sql.query(` UPDATE respuesta_empleado SET numero_pregunta=?,pregunta=?,valor=?,autoevaluable=?,a=?, b=?, c=?, d=?, opcion_correcta=?, eleccion=?,respuesta=?,idempleado=?,idevaluacion=?,idtipo_pregunta=?,idpregunta=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idrespuesta_empleado=?`, [c.numero_pregunta, c.pregunta, c.valor, c.autoevaluable, c.a, c.b, c.c, c.d, c.opcion_correcta, c.eleccion, c.respuesta, c.idempleado, c.idevaluacion, c.idtipo_pregunta, c.idpregunta, c.usuario_registro, c.fecha_actualizo, c.deleted, id],
  (err, res) => {
    if (err) { result(null, err); return; }
    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }

    result(null, { id: id, ...c });
  })
}




module.exports = Respuestas;