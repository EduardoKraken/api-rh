const { result } = require("lodash");
const sql = require("../db.js");


//const constructor
const TipoMaterial = function(tipo_material) {};

TipoMaterial.getTipoMateriales = result => {
    sql.query(`SELECT * FROM tipo_material WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

TipoMaterial.addTipoMaterial = (c, result) => {
    sql.query(`INSERT INTO tipo_material(tipo_material,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?)`, [c.tipo_material, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo],
        (err, res) => {
            if (err) { result(err, null); return; }
            result(null, { id: res.insertId, ...c });
        }
    )
}

TipoMaterial.updateTipoMaterial = (id, c, result) => {
    sql.query(` UPDATE tipo_material SET tipo_material=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idtipo_material=?`, [c.tipo_material, c.usuario_registro, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }
            result(null, { id: id, ...c });
        })
}



module.exports = TipoMaterial;