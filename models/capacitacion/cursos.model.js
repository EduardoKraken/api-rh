const { result } = require("lodash");
const sql = require("../db.js");
const sqlErp = require("../db3.js");

//const constructor
const cursos = function(cursos) {};

// Obtener los cursos
cursos.getCursos = (  ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT c.*,u.iderp AS id_usucario_capacitor, u2.iderp AS id_usuario_registro FROM cursos c 
        LEFT JOIN usuarios u on u.idusuario = c.idcapacitador
        LEFT JOIN usuarios u2 on u2.idusuario = c.usuario_registro
        WHERE c.deleted = 0;`,(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

cursos.addCurso = (c, result) => {
  sql.query(`INSERT INTO cursos(curso,duracion,descripcion,imagen,idcapacitador,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?,?,?,?,?)`, [c.curso, c.duracion, c.descripcion, c.imagen, c.idcapacitador, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo],
    (err, res) => {
      if (err) { result(err, null); return; }
      result(null, { id: res.insertId, ...c });
    }
  )
}

cursos.updateCurso = (id, c, result) => {
  sql.query(` UPDATE cursos SET curso=?,duracion=?,descripcion=?,imagen=?,idcapacitador=?,usuario_registro=?,fecha_actualizo=?,deleted=? WHERE idcurso=?`, [c.curso, c.duracion, c.descripcion, c.imagen, c.idcapacitador, c.usuario_registro, c.fecha_actualizo, c.deleted, id],
    (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...c });
  })
}

cursos.getCursoId = (id, result) => {
    sql.query(`SELECT * FROM cursos WHERE deleted = 0 AND idcurso=?`, [id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        result(null, res);
    });
};

cursos.usuariosERP = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlErp.query(`SELECT * FROM usuarios;`,(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

module.exports = cursos;