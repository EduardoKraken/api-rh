const { result } = require("lodash");
const sql = require("../db.js");


//const constructor
const capacitador = function(temas) {};

capacitador.agregarMensajeCapacitador = ( m ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO mensajes_capacitacion(idcapacitador,idusuarioerp,mensaje,idcurso,idtema,idsubtema,idmaterial,emisor)VALUES(?,?,?,?,?,?,?,?)`,
      [m.idcapacitador, m.idusuarioerp, m.mensaje, m.idcurso, m.idtema, m.idsubtema, m.idmaterial, m.emisor],
      (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...m });
    })
  })
}


capacitador.getMensajesCapacitador = ( idcapacitador ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT m.*, ma.nombre AS material, s.subtema, t.tema, c.curso FROM mensajes_capacitacion m
      LEFT JOIN materiales ma ON ma.idmaterial = m.idmaterial
      LEFT JOIN subtemas s ON s.idsubtema = m.idsubtema
      LEFT JOIN temas t ON t.idtema = m.idtema
      LEFT JOIN cursos c ON c.idcurso = m.idcurso
      WHERE m.idcapacitador = ? AND m.deleted = 0;`,[ idcapacitador ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}


capacitador.getMensajesUsuario = ( idusuarioerp ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT m.*, ma.nombre AS material, s.subtema, t.tema, c.curso FROM mensajes_capacitacion m
      LEFT JOIN materiales ma ON ma.idmaterial = m.idmaterial
      LEFT JOIN subtemas s ON s.idsubtema = m.idsubtema
      LEFT JOIN temas t ON t.idtema = m.idtema
      LEFT JOIN cursos c ON c.idcurso = m.idcurso
      WHERE m.idusuarioerp = ? AND m.deleted = 0;`,[ idusuarioerp ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}


capacitador.getCapacitadorMensaje = ( idcapacitador ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT DISTINCT m.idusuarioerp, m.idcurso, c.curso FROM mensajes_capacitacion m
      LEFT JOIN cursos c ON c.idcurso = m.idcurso
      WHERE m.idcapacitador = ? AND m.deleted = 0;`,[ idcapacitador ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}

capacitador.getUsuariosMensaje = ( idusuarioerp ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT DISTINCT m.idcapacitador, m.idusuarioerp, m.idcurso, c.curso FROM mensajes_capacitacion m
      LEFT JOIN cursos c ON c.idcurso = m.idcurso
      WHERE m.idusuarioerp = ? AND m.deleted = 0;`,[ idusuarioerp ],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve(res)
    })
  })
}


capacitador.updateMensajeCapacitador = ( id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`UPDATE mensajes_capacitacion SET visto = 1 WHERE idmensajes_capacitacion = ?`, [ id ],
      (err, res) => {
      if (err) { return reject({message: err.sqlMessage })}
      if (res.affectedRows == 0) { return reject({message: `No se encontro el mensaje con id: ${ id }` })}
      resolve( id )
    })
  })
}

module.exports = capacitador;