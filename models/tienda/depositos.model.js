const sql = require("./../dbTiendaINBI.js");

// constructor
const Depositos = function(pagos) {
  this.idpagos     = pagos.idpagos;
  this.importe     = pagos.importe;
  this.fecha       = pagos.fecha;
  this.fechalim    = pagos.fechalim;
  this.concepto    = pagos.concepto;
  this.idcliente   = pagos.idcliente;
  this.idfacturas   = pagos.idfacturas;
};

Depositos.addDepositos = (c, result) => {
	sql.query(`INSERT INTO pagoxfac( importe, fecha, idfacturas)VALUES(?,?,?)`,
		[c.importe,c.fecha,c.idfacturas], 
    (err, res) => {	
    if (err) {
    
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...c });
	});
};

Depositos.getDepositosId = (idpagos, result)=>{
	sql.query(`SELECT * FROM facturas WHERE idpagoxfac = ?`, [idpagos], (err,res)=>{
		if (err) {
      
      result(null, err);
      return;
    }
    
    result(null, res);
	})
};


Depositos.getDepositos = (id, result) => {
  sql.query(`SELECT p.idpagoxfac, p.importe, p.fecha, p.idfacturas, f.importe AS "importe2", (SELECT sum(importe) 
  	FROM pagoxfac WHERE idfacturas = p.idfacturas) AS "total" 
  	FROM pagoxfac p INNER JOIN facturas f ON p.idfacturas = f.idfacturas WHERE p.idfacturas= ?`,[id], (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }
    result(null, res);
  });
};


Depositos.updateDepositos = (id, cli, result) => {
  sql.query(` UPDATE facturas SET
      		importe= ?
   			WHERE idpagoxfac= ?`, [cli.importe,id],
    (err, res) => {
      if (err) {
        
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...cli });
    }
  );
};

module.exports = Depositos;