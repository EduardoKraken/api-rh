const sql = require("./../dbTiendaINBI.js");

// constructor
const Pagos = function(pagos) {
  this.idpagos     = pagos.idpagos;
  this.idweb       = pagos.idweb;
  this.importe     = pagos.importe;
  this.fecha       = pagos.fecha;
  this.fechalim    = pagos.fechalim;
  this.concepto    = pagos.concepto;
  this.idcliente   = pagos.idcliente;
};

Pagos.addPagos = (c, result) => {
	sql.query(`INSERT INTO pagos(importe,fecha,fechalim,concepto,idcliente)VALUES(?,?,?,?,?)`,
		[c.importe,c.fecha,c.fechalim,c.concepto,c.idcliente], 
    (err, res) => {	
    if (err) {
    
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...c });
	});
};

Pagos.getPagosId = (idpagos, result)=>{
	sql.query(`SELECT p.idpagos, p.importe, p.fecha, p.fechalim, p.concepto, p.idcliente, c.nomcli 
		FROM pagos p INNER JOIN clientes c ON c.idweb = p.idcliente WHERE p.idpagos = ?`, [idpagos], (err,res)=>{
		if (err) {
      
      result(null, err);
      return;
    }
    
    result(null, res);
	})
};


Pagos.getPagos = result => {
  sql.query(`SELECT p.idpagos, p.importe, p.fecha, p.fechalim, p.concepto, p.idcliente, c.nomcli, 
			  (SELECT sum(importe) FROM facturas WHERE idpagos = p.idpagos) AS "pagado", 
				p.importe - ( SELECT sum(importe)   FROM facturas WHERE idpagos = p.idpagos) AS "faltante" FROM
 				pagos p INNER JOIN clientes c ON p.idcliente = c.idweb`, (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }
    result(null, res);
  });
};


Pagos.updatePagos  = (cli, result) => {
  sql.query(`UPDATE pagos SET importe=?,fecha=?,fechalim=?,concepto=?,idcliente=? WHERE idpagos = ?`, [cli.importe,cli.fecha,cli.fechalim,cli.concepto,cli.idcliente,cli.idpagos],
    (err, res) => {
      if (err) {
        
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null);
    }
  );
};

module.exports = Pagos;