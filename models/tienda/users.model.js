const sql = require("./../dbTiendaINBI.js");

// constructor
const Users = function(user) {
  this.email     = user.email;
  this.nomuser   = user.nomuser;
  this.estatus   = user.estatus;
  this.nomper    = user.nomper;
  this.password  = user.password;
  this.nivel     = user.nivel;
  this.estatus   = user.estatus;
};
// VARIABLES PARA QUERYS

Users.login = (loger,result) =>{
  var user_schools = [];
  sql.query("SELECT * FROM usuariosweb WHERE email = ? AND password = ? OR nomuser = ? AND password = ?"
    , [loger.email, loger.password, loger.email, loger.password],(err, res) => {
    if (err) { 
       
      result(err, null);
      return 
    }

    result(null, res[0])
    
    // result({ kind: "not_found" }, null);
  });
};

// ----------------------------------------------------------------------
// ----------------------------------------------------------------------
  Users.updatexId = (id, user, result)=>{
    sql.query("UPDATE usuariosweb SET nomuser = ?, email = ?, password = ?,nivel = ?, estatus=?, nomper=? WHERE idusuariosweb = ?", 
              [user.nomuser, user.email, user.password, user.nivel, user.estatus, user.nomper, id], (err, res) => 
      {
        if (err) { 
          
          result(null, err);
          return;
        }
        if (res.affectedRows == 0) {
          // not found user with the id
          result({ kind: "not_found" }, null);
          return;
        }
        result(null, { id: id, ...user });
      }
    );
  };

  Users.deleteSchoolsxUser = (id, result) => {
    sql.query("DELETE FROM usuariosweb WHERE id_user = ?", id, (err, res) => {
      if (err) {
        
        result(null, err);
        return;
      }
  
      result(null, res);
    });
  };

  Users.createSchoolsxUser = (id,school, result) => {
    sql.query("INSERT INTO user_schools(id_user, id_school)VALUE(?,?)",[id,school], (err, res) => {
      if (err) {
        result(err, null);
        return;
      }

      // result(null, res);
    });
  };
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------

// --- SIN USAR---->
Users.create = (newUser, result) => {
  sql.query("INSERT INTO usuariosweb SET ?", newUser, (err, res) => {
    if (err) {
      
      result(err, null);
      return;
    }

    result(null, { id: res.insertId, ...newUser });
  });
};

Users.findById = (userid, result) => {
  sql.query(`SELECT * FROM usuariosweb WHERE idusuariosweb = ${userid}`, (err, res) => {
    if (err) {
      
      result(err, null);
      return;
    }

    if (res.length) {
      result(null, res[0]);
      return;
    }
    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

Users.getxEmail = (data, result) => {
  if(data.nomuser == ''){
    sql.query(`SELECT * FROM usuariosweb WHERE email = ?`,[data.email], (err, res) => {
      if (err) {
        
        result(err, null);
        return;
      }

      if (res.length) {
        result(null, res[0]);
        return;
      }
      result({ kind: "not_found" }, null);
    });
  }else{
    sql.query(`SELECT * FROM usuariosweb WHERE email = ? OR nomuser = ?`,[data.email, data.nomuser], (err, res) => {
      if (err) {
        
        result(err, null);
        return;
      }

      if (res.length) {
        result(null, res[0]);
        return;
      }
      // not found Customer with the id
      result({ kind: "not_found" }, null);
    });
  }
  
};

Users.getAll = result => {
  sql.query("SELECT * FROM usuariosweb", (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }

    result(null, res);
  });
};

Users.updateById = (id, user, result) => {
  sql.query("UPDATE usuariosweb SET email = ?, name = ?, active = ? WHERE id = ?", [user.email, user.name, user.active, id],
    (err, res) => {
      if (err) {
        
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found user with the id
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...user });
    }
  );
};

Users.remove = (id, result) => {
  sql.query("DELETE FROM usuariosweb WHERE id = ?", id, (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, res);
  });
};

Users.removeAll = result => {
  sql.query("DELETE FROM usuariosweb", (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }

    result(null, res);
  });
};

Users.activarUsuario = (idusuario, result)=>{
  sql.query( `UPDATE usuariosweb SET estatus = 2 WHERE idusuariosweb = ?`, [idusuario], (err, res) => {
      if (err) {   result(null, err); return;  } // SI OCURRE UN ERROR LO RETORNO
      if (res.affectedRows == 0) { result({ kind: "No encontrado" }, null); return; } // SI NO ENCUENTRO EL ID

      result(null, { idusuario: idusuario});
    }
  );
};

module.exports = Users;