const sql = require("./../dbTiendaINBI.js");

const Articulos = articulos => {
    this.id             = articulos.id;
    this.nomart         = articulos.nomart;
    this.codigo         = articulos.codigo;
    this.idlaboratorio  = articulos.idlaboratorio;
    this.nomlab         = articulos.nomlab;
    this.descrip        = articulos.descrip;
    this.statusweb      = articulos.statusweb;
    this.precio1        = articulos.precio1;
    this.sal            = articulos.sal;
    this.novedades      = articulos.novedades;
    this.destacados     = articulos.destacados;
    this.pjedesc         = articulos.pjedesc;
}


Articulos.addArticulos = (art, result) => {
  sql.query(`INSERT INTO arts(nomart,codigo,descrip,descripLarga,ubicacion)VALUES(?,?,?,?,?)`,
    [art.nomart,art.codigo,art.descrip,art.descripLarga,art.ubicacion], (err, res) => {  
    if (err) {
      
      result(err, null);
      return;
    }
    result(null, { id: res.insertId, ...art });
  });
};


Articulos.obtener_articulos_random = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.precio1, a.precio2, a.pjedesc,f.image_name as foto 
                FROM arts a LEFT JOIN fotos f ON a.codigo = f.codigo
              WHERE a.estatus = 1 ORDER BY a.nomart;`, (err, res) => {
    if (err) { result(null, err);  return; }
    result(null, res);
  });
};

Articulos.getAll = result => {
  sql.query(`SELECT * FROM arts;`, (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Articulos.getArticulosActivo = result => {
  sql.query(`SELECT a.*, f.familia, IFNULL(c.categoria,'NA') AS categoria, IFNULL(s.subcategoria,'NA') AS subcategoria FROM arts a 
    LEFT JOIN categorias c ON c.idcategorias = a.idcategorias
    LEFT JOIN familias f ON f.idfamilias = a.idfamilias
    LEFT JOIN subcategorias s ON s.idsubcategorias = a.idsubcategorias WHERE a.estatus = 1;`, (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Articulos.getArticuloId = (id,result) => {
  sql.query(`SELECT a.* FROM arts a WHERE a.id = ?;`,[id], (err, res) => {
    if (err) {
      
      result(err, null);
      return;
    }
    result(null, res[0]);
  });
};

Articulos.getNovedades = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.novedades, 
    a.destacados, a.pjedesc FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio WHERE a.novedades = 1 AND a.statusweb = 1;`, (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Articulos.getDestacados = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.novedades, 
    a.destacados, a.pjedesc FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio WHERE a.destacados = 1 AND a.statusweb = 1;`, (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Articulos.getArtxLab =(id, result) => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.pjedesc
    FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio 
    WHERE  a.statusweb = 1 AND a.idlaboratorio = ?;`,[id], (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Articulos.getPromociones = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.pjedesc
    FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio WHERE a.pjedesc > 0 AND a.statusweb = 1;`, (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }
    result(null, res);
  });
};


Articulos.fotosArt = result => {
  sql.query(`SELECT idfotos, codigo, image_name, principal FROM fotos`, (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Articulos.fotosxArt = (codigo, result) => {
  sql.query(`SELECT idfotos, codigo, image_name, principal FROM fotos WHERE codigo = ?`,[codigo], (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Articulos.addFoto = (foto, result) => {
  sql.query(`INSERT INTO fotos(codigo, image_name, principal)VALUES(?,?,?)`,
    [foto.codigo,foto.image_name,foto.principal], (err, res) => {  
    if (err) {
      
      result(err, null);
      return;
    }
    result(null, { id: res.insertId, ...foto });
  });
};




Articulos.getArticuloCodigo = (codigo, result) => {
  sql.query(`SELECT a.* FROM arts a  WHERE a.codigo = ? OR a.nomart like '%` + codigo +`%';`,[ codigo ], (err, res) => {
    if (err) {
       
      result(err, null);
      return;
    }
    result(null, res);
  });
};


Articulos.getArticuloTienda = (codigo, result) => {
  sql.query(`SELECT a.*, f.familia, c.categoria,s.subcategoria FROM arts a 
    LEFT JOIN categorias c ON c.idcategorias = a.idcategorias
    LEFT JOIN familias f ON f.idfamilias = a.idfamilias
    LEFT JOIN subcategorias s ON s.idsubcategorias = a.idsubcategorias
    WHERE a.codigo = ? OR a.nomart like '%` + codigo +`%';`,[ codigo ], (err, res) => {
    if (err) {
       
      result(err, null);
      return;
    }
    result(null, res);
  });
};


Articulos.updateArticulos = (id, art, result) => {
  sql.query(` UPDATE arts SET nomart = ?, estatus = ?, descrip = ?, descripLarga =?,
             precio1 = ?, pjedesc = ?, preciocompra = ? WHERE id = ?;`, 
                [art.nomart, art.estatus,art.descrip,art.descripLarga,art.precio1,art.pjedesc,art.preciocompra,  id],
    (err, res) => {
      if (err) {
        
        result(err, null);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id, ...art });
    }
  );
};

Articulos.updateNovedades = (id, art, result) => {
  sql.query(` UPDATE arts SET novedades = ?, destacados = ? WHERE id = ?;`, [art.novedades,art.destacados, id],
    (err, res) => {
      if (err) {
        
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...art });
    }
  );
};

Articulos.deleteFoto = (id, result) => {
  sql.query("DELETE FROM fotos where idfotos = ?;",[id], (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }

    result(null, res);
  });
};

Articulos.updateFotoPrincipal = (id, foto, result) => {
  sql.query(` UPDATE fotos SET principal = ?  WHERE idfotos = ?;`, [foto.principal, id],
    (err, res) => {
      if (err) {
        
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id, ...foto });
    }
  );
};


Articulos.getArticuloFotos = (codigo, result) => {
  sql.query(`SELECT * FROM fotos WHERE codigo = ? ORDER BY principal DESC`,[ codigo ], (err, res) => {
    if (err) {
       
      result(err, null);
      return;
    }
    result(null, res);
  });
};

Articulos.getArticuloFam = (id, result) => {
  sql.query(`SELECT f.*, fa.familia FROM familias_art f
      LEFT JOIN familias fa ON fa.idfamilias = f.idfamilias WHERE f.idarticulo = ? `,[ id ], (err, res) => {
    if (err) {
       
      result(err, null);
      return;
    }
    result(null, res);
  });
};

Articulos.getArticuloCat = (id, result) => {
  sql.query(`SELECT c.*, ca.categoria FROM categorias_art c
LEFT JOIN categorias ca ON ca.idcategorias = c.idcategorias WHERE c.idarticulo = ?`,[ id ], (err, res) => {
    if (err) {
       
      result(err, null);
      return;
    }
    result(null, res);
  });
};

Articulos.getArticuloSub = (id, result) => {
  sql.query(`SELECT s.*, sa.subcategoria FROM subcategorias_art s
LEFT JOIN subcategorias sa ON sa.idsubcategorias = s.idsubcategorias WHERE s.idarticulo = ?`,[ id ], (err, res) => {
    if (err) {
       
      result(err, null);
      return;
    }
    result(null, res);
  });
};


Articulos.addSolicitud = ( idplantel, iderp ) => {
  return new Promise((resolve, reject) => {
    sql.query(`INSERT INTO solicitudes(iderp,idplantel)VALUES(?,?)`,[ iderp, idplantel ], (err, res) => {  
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, iderp, idplantel });
    })
  })
};

Articulos.addDesgloseSolicitud = (idsolicitudes,idarticulo,foto,cantidad) => {
  return new Promise((resolve, reject) => {
    sql.query(`INSERT INTO desgloce_solicitud(idsolicitudes,idarticulo,foto,cantidad)VALUES(?,?,?,?)`,[ idsolicitudes, idarticulo, foto, cantidad ], (err, res) => {  
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, idsolicitudes, idarticulo, foto, cantidad });
    })
  })
};


Articulos.getSolicitudes = ( ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM solicitudes WHERE deleted = 0 ORDER BY fecha_creacion DESC;`, (err, res) => {  
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve( res );
    })
  })
};

Articulos.getDesglose = ( ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT d.*, a.nomart FROM desgloce_solicitud d
      LEFT JOIN arts a ON a.id = d.idarticulo WHERE d.deleted = 0;`, (err, res) => {  
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve( res );
    })
  })
};


Articulos.eliminarSolicitud = ( id ) => {
  return new Promise((resolve, reject) => {
    sql.query(`UPDATE solicitudes SET deleted = 1 WHERE idsolicitudes = ?;`,[ id ],(err, res) => {  
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve( res );
    })
  })
};

Articulos.aceptarSolicitud = ( id, estatus ) => {
  return new Promise((resolve, reject) => {
    sql.query(`UPDATE solicitudes SET estatus = ? WHERE idsolicitudes = ?;`,[ estatus,  id ],(err, res) => {  
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve( res );
    })
  })
};


Articulos.updateDeslgose = ( cantidad_recibida, estatus, iddesgloce_solicitud ) => {
  return new Promise((resolve, reject) => {
    sql.query(`UPDATE desgloce_solicitud SET cantidad_recibida = ?, estatus = ?, fecha_entrega = NOW() WHERE iddesgloce_solicitud = ?;`,
      [ cantidad_recibida, estatus, iddesgloce_solicitud ],(err, res) => {  
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve( res );
    })
  })
};

Articulos.diaActual = ( ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT (ELT(WEEKDAY(CURDATE()) + 1, 1, 2, 3, 4, 5, 6, 7)) AS dia;`,(err, res) => {  
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve( res[0] );
    })
  })
};


module.exports = Articulos;