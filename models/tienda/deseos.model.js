const sql = require("./../dbTiendaINBI.js");

// constructor
const Deseos = bann => {
    this.idciudades   = bann.idciudades;
};
Deseos.buscar_producto_lista_deseos = (c,result) => {
  sql.query('SELECT * FROM lista_deseos WHERE id_producto = ? AND id_cliente = ?', [c.id_producto, c.id_cliente], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Deseos.anadir_lista_deseos = (c, result) => {
	sql.query(`INSERT INTO lista_deseos(id_producto,id_cliente)VALUES(?,?)`,[c.id_producto,c.id_cliente], 
    (err, res) => {	
    if (err) {
    
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...c });
	});
};

Deseos.obtener_lista_deseos = (id_cliente ,result) => {
  sql.query(`SELECT ld.idlista, ld.id_producto, ld.id_cliente, a.nomart, a.codigo, a.precio1 
              FROM lista_deseos ld LEFT JOIN arts a ON ld.id_producto = a.id 
             WHERE ld.id_cliente = ?`, [id_cliente], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Deseos.eliminar_producto_lista_deseos = (idlista, result) => {
  sql.query("DELETE FROM lista_deseos where idlista = ?;",[idlista], (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }
    result(null, res);
  });
};



module.exports = Deseos;