const sql = require("./../dbTiendaINBI.js");

// constructor
const Banners = bann => {
    this.idbanners   = bann.idbanners;
    this.nombanner   = bann.nombanner;
};

// Agregar un laboratorio
Banners.addBanner = (newBanner, result) => {
	sql.query(`INSERT INTO banners(nombanner)VALUES(?);`,
						 [newBanner.nombanner], (err, res) => {	
    if (err) {
	    
	    result(err, null);
	    return;
    }
    result(null, { id: res.insertId, ...newBanner });
	});
};


// Traer laboratior activos
Banners.getBanners = result => {
  sql.query('SELECT * FROM banners;', (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Banners.deleteBanner = (id, result) => {
  sql.query("DELETE FROM banners where idbanners = ?;",[id], (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }

    result(null, res);
  });
};


module.exports = Banners;