const sql = require("./../dbTiendaINBI.js");

const Direcciones = function(direccion) {
    this.iddireccion  = direccion.iddireccion;
};

Direcciones.direcciones_cliente = (id, result)=>{
	sql.query(`SELECT d.*, c.nombre as nomciudad 
              FROM direcciones d LEFT JOIN ciudades c ON d.id_ciudad = c.id
            WHERE idcliente=? `, [id], (err,res)=>{
		if (err) {
      
      result(null, err);
      return;
    }
    result(null, res);
	})
};
Direcciones.agregar_direccion_cliente = (d, result) => {
	sql.query(`INSERT INTO direcciones(idcliente,id_ciudad,nombre,apellido,direccion,apartamento,cp,email,telefono)
		VALUES(?,?,?,?,?,?,?,?,?)`,[d.idcliente ,d.data.ciudad.id,d.data.nombre,d.data.apellido,d.data.direccion,
                                d.data.apartamento,d.data.cp,d.data.email,d.data.telefono], (err, res) => {	
    if (err) {
    
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...d });
	});
};
Direcciones.actualiza_direccion_envio = (d, result) => {
  sql.query(` UPDATE direcciones SET id_ciudad = ?,nombre = ?,apellido = ?,direccion = ?, apartamento = ?,cp = ?,email = ?,telefono = ?
                WHERE iddireccion = ?`, [d.data.ciudad.id, d.data.nombre ,d.data.apellido,d.data.direccion,d.data.apartamento,
                                         d.data.cp,d.data.email,d.data.telefono, d.data.iddireccion],
    (err, res) => {
      if (err) {
        
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: d });
    }
  );
};
Direcciones.cambiar_direccion_envio_activa = (d, result) => {
  sql.query(`SELECT iddireccion FROM direcciones WHERE idcliente = ?`, [d.idcliente],  (err,res)=>{
		if (err) { result(null, err);  return; }
    for(i in res){
      sql.query(` UPDATE direcciones SET activo = 0 WHERE iddireccion = ?`, [res[i].iddireccion]);
    }
    sql.query(`UPDATE direcciones SET activo = 1 WHERE iddireccion = ?`, [d.data.iddireccion] );
    result(null, true);
	})
};


Direcciones.direcciones_cliente_facturacion = (id, result)=>{
	sql.query(`SELECT d.*, c.nombre as nomciudad 
              FROM facturaciones d LEFT JOIN ciudades c ON d.id_ciudad = c.id
            WHERE idcliente=? `, [id], (err,res)=>{
		if (err) {
      
      result(null, err);
      return;
    }
    result(null, res);
	})
};
Direcciones.agregar_direccion_cliente_facturacion = (d, result) => {
	sql.query(`INSERT INTO facturaciones(idcliente,id_ciudad,nombre,apellido,direccion,apartamento,cp,email,telefono)
		VALUES(?,?,?,?,?,?,?,?,?)`,[d.idcliente ,d.data.ciudad.id,d.data.nombre,d.data.apellido,d.data.direccion,
                                d.data.apartamento,d.data.cp,d.data.email,d.data.telefono], (err, res) => {	
    if (err) {
    
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...d });
	});
};
Direcciones.actualiza_direccion_facturacion = (d, result) => {
  sql.query(` UPDATE facturaciones SET id_ciudad = ?,nombre = ?,apellido = ?,direccion = ?, apartamento = ?,cp = ?,email = ?,telefono = ?
                WHERE idfacturacion = ?`, [d.data.ciudad.id, d.data.nombre ,d.data.apellido,d.data.direccion,d.data.apartamento,
                                         d.data.cp,d.data.email,d.data.telefono, d.data.idfacturacion],
    (err, res) => {
      if (err) {
        
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: d });
    }
  );
};
Direcciones.cambiar_direccion_facturacion_activa = (d, result) => {
  sql.query(`SELECT idfacturacion FROM facturaciones WHERE idcliente = ?`, [d.idcliente],  (err,res)=>{
		if (err) { result(null, err);  return; }
    for(i in res){
      sql.query(` UPDATE facturaciones SET activo = 0 WHERE idfacturacion = ?`, [res[i].idfacturacion]);
    }
    sql.query(`UPDATE facturaciones SET activo = 1 WHERE idfacturacion = ?`, [d.data.idfacturacion] );
    result(null, true);
	})
};


Direcciones.deleteDireccion = (id, result) => {
  sql.query("DELETE FROM direcciones WHERE iddirecciones = ?", id, (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    result(null, res);
  });
};


module.exports = Direcciones;