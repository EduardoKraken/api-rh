const sql = require("./../dbTiendaINBI.js");

// constructor
const Clientes = function(clientes) {
  this.idcliente = clientes.idcliente;
  this.nombre    = clientes.nombre;
  this.password  = clientes.password;
  this.telefono  = clientes.telefono;
  this.estatus   = clientes.estatus;
};

Clientes.login_cliente = (loger,result) =>{
  sql.query("SELECT * FROM clientes WHERE email = ? AND password = ?"
    , [loger.email, loger.password],(err, res) => {
    if (err) { 
       
      result(err, null);
      return 
    }
    result(null, res[0])
  });
};


Clientes.addCliente = (c, result) => {
	sql.query(`INSERT INTO clientes(nombre,email,password,telefono)VALUES(?,?,?,?)`,[c.nombre,c.email,c.password,c.telefono], 
    (err, res) => {	
    if (err) {
    
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...c });
	});
};

Clientes.getClienteId = (params, result)=>{
	sql.query(`SELECT SELECT idcliente, nombre, email, telefono,password, estatus FROM clientes 
    FROM clientes WHERE idcliente=?`, [params.idweb], (err,res)=>{
		if (err) {
      
      result(null, err);
      return;
    }
    
    result(null, res);
	})
};


Clientes.getClientes = result => {
  sql.query(`SELECT idcliente, nombre, email, telefono,password, estatus FROM clientes`, (err, res) => {
    if (err) {
      
      result(null, err);
      return;
    }
    result(null, res);
  });
};


Clientes.updateCliente = (cli, result) => {
  sql.query(` UPDATE clientes SET nombre=?, apellido=?, usuario =?, email=?,password=?,telefono=?
                WHERE idcliente = ?`, 
              [cli.data.nombre, cli.data.apellido, cli.data.usuario,cli.data.email,cli.password,cli.data.telefono, cli.data.idcliente],
    (err, res) => {
      if (err) {
        
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, true);
    }
  );
};

module.exports = Clientes;