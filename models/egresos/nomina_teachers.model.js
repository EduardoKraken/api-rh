const { result }  = require("lodash");
const sqlERP      = require("../dbErpPrueba.js");


//const constructor
const exci = function(clases) {};

exci.getRegistrosExci = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM exci_registro`,(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve(res)
    })
  })
}

exci.getBecas = ( ids ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM fyn_proc_becas_prospectos_alumnos WHERE id_alumno IN ( ? )`,[ ids ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve(res)
    })
  })
}


exci.gruposAlumnosExci = ( ids ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT ga.*, g.id_plantel FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE ga.id_alumno IN ( ? )`,[ ids ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve(res)
    })
  })
}


exci.gruposExci = ( ids ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT *, m.id_maestro, m2.id_maestro AS id_maestro_2 FROM grupos g
      LEFT JOIN maestrosgrupo m   ON m.id_grupo  = g.id_grupo AND m.activo_sn  = 1 AND m.numero_teacher = 1
      LEFT JOIN maestrosgrupo m2  ON m2.id_grupo = g.id_grupo AND m2.activo_sn = 1 AND m2.numero_teacher = 2
      WHERE g.id_grupo IN(?)`,[ ids ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve(res)
    })
  })
}

exci.pagosExci = ( idsGrupos, idAlumnos ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM alumnos_grupos_especiales_carga WHERE id_grupo IN(?) AND id_alumno IN (?);`,[ idsGrupos, idAlumnos ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve(res)
    })
  })
}


exci.updatePagoExci = ( u ) => {
	return new Promise(( resolve, reject )=>{
	  sqlFAST.query(`UPDATE exci_registro SET pago_realizado = ?, adeudo = ?, estatus = ?, mediopago = ?  WHERE idexci_registro = ?`,[ u.pago, u.adeudo, u.estatus, u.mediopago, u.idexci_registro ],
	    (err, res) => {
      if (err) {
        return reject({message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( u );
	  });
	});
};

exci.getAlumnosAll = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT id_alumno, nombre, apellido_paterno, apellido_materno, matricula,
      CONCAT(nombre, ' ', apellido_paterno, ' ', IFNULL(apellido_materno,''), ' ', matricula) AS nombre_completo FROM alumnos
      WHERE activo_sn = 1;`,(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve(res)
    })
  })
}


exci.addMatriculaAlumno = ( id_alumno, matricula, idexci_registro ) => {
  return new Promise(( resolve, reject )=>{
    sqlFAST.query(`UPDATE exci_registro SET id_alumno = ?, matricula = ? WHERE idexci_registro = ?`,[ id_alumno, matricula, idexci_registro ],
      (err, res) => {
      if (err) {
        return reject({message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( {id_alumno, matricula, idexci_registro} );
    });
  });
};


exci.addSeguimientoAlumnoExci = ( idexci_registro, seguimiento ) => {
  return new Promise(( resolve, reject )=>{
    sqlFAST.query(`UPDATE exci_registro SET seguimiento = ? WHERE idexci_registro = ?`,[ seguimiento, idexci_registro ],
      (err, res) => {
      if (err) {
        return reject({message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( {seguimiento, idexci_registro} );
    });
  });
};


exci.alumnosAdeudo = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM exci_registro WHERE pago_realizado = 0;`,(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve(res)
    })
  })
}


exci.updateCorreoExci = ( correo, idexci_registro ) => {
  return new Promise(( resolve, reject )=>{
    sqlFAST.query(`UPDATE exci_registro SET correo = ? WHERE idexci_registro = ?`,[ correo, idexci_registro ],
      (err, res) => {
      if (err) {
        return reject({message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( {correo, idexci_registro} );
    });
  });
};

exci.activarCuentas = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlFAST.query(`UPDATE datos_alumno SET activo = 1 WHERE id > 0`,(err, res) => {
      if (err) {
        return reject({message: err.sqlMessage });
      }

      resolve( res );
    });
  });
};

exci.datosAlumno = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM datos_alumno WHERE activo = 0;`,(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve(res)
    })
  })
}


exci.getRegistrosExciBeca = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT ex.idexci_registro, ex.nombre, ex.telefono, ex.pago_realizado, ex.id_alumno, u.id, u.iderp, e.idencuesta_beca_nivel, ex.id_grupo AS id_grupo_encuesta, c.iderp AS id_ciclo,
      IF(e.persona_beca=1,'Yo','Alguien más') AS persona_beca, e.nombre AS alguien_mas, ex.matricula, g.nombre AS grupo, ga.id AS grupo_lms, g.id AS id_grupo FROM encuesta_beca_nivel e
      LEFT JOIN usuarios u ON u.id = e.id_alumno
      LEFT JOIN exci_registro ex ON ex.id_alumno = u.iderp
      LEFT JOIN grupos g ON g.iderp = ex.id_grupo
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN grupo_alumnos ga ON ga.id_alumno = u.id AND ga.id_grupo = g.id AND e.deleted = 0 AND ex.deleted = 0;`,(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve(res)
    })
  })
}

exci.getExciGruposCiclo = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM grupos WHERE id_ciclo = 188 AND activo_sn = 1;`,(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve(res)
    })
  })
}

exci.agregarGrupoAlumno = ( idexci_registro, id_grupo ) => {
  return new Promise(( resolve, reject )=>{
    sqlFAST.query(`UPDATE exci_registro SET id_grupo = ? WHERE idexci_registro = ?;`,[ id_grupo, idexci_registro],(err, res) => {
      if (err) {
        return reject({message: err.sqlMessage });
      }

      resolve( res );
    });
  });
};



exci.solicitarBeca = ( id_alumno, id_grupo_encuesta ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO fyn_proc_becas_prospectos_alumnos(id_alumno,id_grupo,id_beca,aprobadoSN,comentarios,tipo_registro,id_usuario_ultimo_cambio)VALUES(?,?,4,1,'EXCI BECA NIVEL',1,334)`, 
    [id_alumno, id_grupo_encuesta],(err, res) => {
      if(err){
        return reject({message: err.sqlMessage });
      }
      resolve({ id: res.insertId, id_alumno, id_grupo_encuesta })
    })
  })
}


exci.getExciConAdeudo = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM exci_registro WHERE pago_realizado BETWEEN 0 AND 1398 AND adeudo BETWEEN 0 AND 1399;`, (err, res) => {
      if(err){
        return reject({message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}
module.exports = exci;