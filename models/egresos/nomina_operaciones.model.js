const { result } = require("lodash");
const sqlERP = require("../dbErpPrueba.js");


//const constructor
const egresos = (clases) => { };

egresos.addRequisicionOperaciones = (u) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`INSERT INTO requisicion_compra( id_usuario_solicita, id_sucursal_solicita, id_usuario_compra, id_ciclo, id_requisicion_compra_estatus, comentarios, id_usuario_ultimo_cambio, tipo_requisicion, id_proyecto )
      VALUES(?,?,?,?,?,?,?,?,?)`,
      [u.id_usuario_solicita, u.id_sucursal_solicita, u.id_usuario_compra, u.id_ciclo, u.id_requisicion_compra_estatus, u.comentarios, u.id_usuario_ultimo_cambio, u.tipo_requisicion, u.id_proyecto], (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }
        resolve({ idrequisicion_compra: res.insertId, ...u })
      })
  })
}

egresos.addFotoCotizaciones = (nombre_base, name, idrequisicion_compra, id_usuario_ultimo_cambio, extension) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`INSERT INTO fotos_egresos(name_base, name, tipo, id_egresos, id_usuario_subio, extension) VALUES (?,?,1,?,?,?);`, [nombre_base, name, idrequisicion_compra, id_usuario_ultimo_cambio, extension], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, nombre_base, name, idrequisicion_compra, id_usuario_ultimo_cambio, extension });
    });
  });
};

egresos.addDetalleRequiOper = (idrequisicion_compra, u, id_usuario_ultimo_cambio) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`INSERT INTO requisicion_compra_detalle( id_requisicion_compra, id_elemento_requisicion, id_plantel, concepto, cantidad, costo_unitario, costo_total, id_usuario_ultimo_cambio, id_empleado )
      VALUES(?,?,?,?,?,?,?,?,?)`,
      [idrequisicion_compra, u.id_elemento_requisicion, u.id_plantel, u.concepto, u.cantidad, u.costo_unitario, u.costo_total, id_usuario_ultimo_cambio, u.id_empleado], (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }
        resolve({ iddetalle_requi: res.insertId, ...u })
      })
  })
}

egresos.actualizarRequisicionPartida = (id_requisicion_compra, id_elemento_requisicion, id_plantel, concepto, cantidad, costo_unitario, costo_total, id_usuario_ultimo_cambio, id_empleado) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`INSERT INTO requisicion_compra_detalle( id_requisicion_compra, id_elemento_requisicion, id_plantel, concepto, cantidad, costo_unitario, costo_total, id_usuario_ultimo_cambio, id_empleado )
      VALUES(?,?,?,?,?,?,?,?,?)`,
      [id_requisicion_compra, id_elemento_requisicion, id_plantel, concepto, cantidad, costo_unitario, costo_total, id_usuario_ultimo_cambio, id_empleado], (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }
        resolve({ iddetalle_requi: res.insertId, })
      })
  })
}

egresos.addFyn_proc_egresosOper = (iddetalle_requi, id_usuario_ultimo_cambio) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`INSERT INTO fyn_proc_egresos( id_requisicion_compra_detalle, id_usuario_ultimo_cambio )VALUES(?,?)`, [iddetalle_requi, id_usuario_ultimo_cambio], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve({ idfyn_proc_egresos_oper: res.insertId, iddetalle_requi, id_usuario_ultimo_cambio })
    })
  })
}

egresos.consultarNominaOperaciones = (fecha_inicio, fecha_final) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT r.id, u.nombre_completo, c.ciclo, e.descripcion, r.comentarios FROM requisicion_compra r 
      LEFT JOIN usuarios u ON u.id_usuario = r.id_usuario_solicita
      LEFT JOIN ciclos c ON c.id_ciclo = r.id_ciclo
      LEFT JOIN requisicion_compra_estatus e ON e.id = r.id_requisicion_compra_estatus
      WHERE DATE(r.fecha_alta) BETWEEN ? AND ? 
      AND r.tipo_requisicion = 1;`, [fecha_inicio, fecha_final], (err, res) => {
      if (err) { return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

egresos.getMisRequisiciones = (fecha_inicio, fecha_final, id_usuario) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT r.id, u.nombre_completo, c.id_ciclo, c.ciclo, e.descripcion, r.comentarios, r.id_requisicion_compra_estatus, p.proyecto FROM requisicion_compra r 
      LEFT JOIN usuarios u ON u.id_usuario = r.id_usuario_solicita
      LEFT JOIN ciclos c ON c.id_ciclo = r.id_ciclo
      LEFT JOIN requisicion_compra_estatus e ON e.id = r.id_requisicion_compra_estatus
      LEFT JOIN proyectos p ON p.id_proyecto = r.id_proyecto
      WHERE DATE(r.fecha_alta) BETWEEN ? AND ? 
      AND r.id_usuario_solicita = ?;`, [fecha_inicio, fecha_final, id_usuario], (err, res) => {
      if (err) { return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

egresos.getRequisiciones = (fecha_inicio, fecha_final, ciclo) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT r.id, u.nombre_completo, c.ciclo, e.descripcion, r.comentarios, r.fecha_alta, p.proyecto FROM requisicion_compra r 
      LEFT JOIN usuarios u ON u.id_usuario = r.id_usuario_solicita
      LEFT JOIN ciclos c ON c.id_ciclo = r.id_ciclo
      LEFT JOIN requisicion_compra_estatus e ON e.id = r.id_requisicion_compra_estatus
      LEFT JOIN ciclos ci ON ci.id_ciclo = r.id_ciclo
      LEFT JOIN proyectos p ON p.id_proyecto = r.id_proyecto
      WHERE 1 = 1 ${ciclo ? ` AND ci.ciclo = '${ciclo}' ` : ''}
      ${fecha_inicio ? ` AND (DATE(r.fecha_alta) BETWEEN ? AND ?) ;` : ''}`, [fecha_inicio, fecha_final], (err, res) => {
      if (err) { return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

egresos.consultarDetalleRequiId = (ids) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT r.*, p.plantel, e.elemento_mantenimiento, c.nomina, c.empleado, c.cuenta, pr.proyecto FROM requisicion_compra_detalle r
      LEFT JOIN planteles p ON p.id_plantel= r.id_plantel
      LEFT JOIN elementosmantenimiento e ON e.id_elemento_mantenimiento = r.id_elemento_requisicion 
      LEFT JOIN configuracion_nomina c ON c.id_usuario = r.id_empleado
      LEFT JOIN requisicion_compra re ON re.id = r.id_requisicion_compra
      LEFT JOIN proyectos pr ON pr.id_proyecto = re.id_proyecto
      WHERE id_requisicion_compra IN (?) AND r.activo_sn = 1;`, [ids], (err, res) => {
      if (err) { return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

egresos.consultarPartidasEgresos = (ciclo, fecha_inicio, fecha_final) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT r.*, p.plantel, e.elemento_mantenimiento, c.nomina, c.empleado, ci.ciclo, c.cuenta, pr.proyecto, 
    CASE r.tipo_pago 
      WHEN 1 THEN 'Tarjeta' 
      WHEN 2 THEN 'Transferencia/Deposito' 
      WHEN 3 THEN 'Efectivo'
    ELSE 'Tipo de pago desconocido' 
    END AS tipo_pago, 
	CASE r.estatus
      WHEN 1 THEN 'Sin Comprobante' 
      WHEN 2 THEN 'Con Comprobante' 
      WHEN 3 THEN 'Autorizado' 
      WHEN 7 THEN 'En Revision' 
      WHEN 8 THEN 'Rechazado' 
    ELSE 'Sin Estatus' 
    END AS estatus,
    (SELECT name from fotos_comprobantes WHERE id_partida = r.id AND deleted = 0 limit 1) AS nombre_foto,
    (SELECT idfotos_comprobantes from fotos_comprobantes WHERE id_partida = r.id limit 1) AS fecha_orden,
    IF(ce.estatus = 0, 'editar','') AS clase_row, ce.idcambios_egresos, ce.nuevo_ciclo, ce.idpartida, ce.idsolicito, ce.idautorizo, ce.eliminar, ce.cambio_ciclo, 
    ce.ciclo_anterior, ce.nuevo_ciclo, ce.notas, ce.concepto AS nuevo_concepto, ce.estatus AS estatusSolicitud, u.nombre_completo
    FROM requisicion_compra_detalle r
    LEFT JOIN planteles p ON p.id_plantel= r.id_plantel
    LEFT JOIN elementosmantenimiento e ON e.id_elemento_mantenimiento = r.id_elemento_requisicion 
    LEFT JOIN configuracion_nomina c ON c.id_usuario = r.id_empleado
    LEFT JOIN requisicion_compra rc ON r.id_requisicion_compra = rc.id
    LEFT JOIN ciclos ci ON ci.id_ciclo = rc.id_ciclo
    LEFT JOIN proyectos pr ON pr.id_proyecto = rc.id_proyecto
    LEFT JOIN cambios_egresos ce ON ce.idpartida = r.id
    LEFT JOIN usuarios u ON u.id_usuario = ce.idsolicito
    WHERE (r.activo_sn != 0) AND (r.tipo_pago != 0) 
    ${ciclo ? ` AND ci.ciclo = '${ciclo}' ` : ''}
    ${fecha_inicio ? ` AND (DATE(r.fecha_alta) BETWEEN ? AND ?) ` : ''} AND r.activo_sn = 1
    ORDER BY fecha_orden DESC;`, [fecha_inicio, fecha_final], (err, res) => {
      if (err) { return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

egresos.getElementosCompra = () => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM elementosmantenimiento;`, (err, res) => {
      if (err) { return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

egresos.getTipoRequisiciones = () => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM tipo_requisiciones ORDER BY tipo_requisicion;`, (err, res) => {
      if (err) { return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

egresos.updatePagoegresos = (u) => {
  return new Promise((resolve, reject) => {
    sqlFAST.query(`UPDATE egresos_registro SET pago_realizado = ?, adeudo = ?, estatus = ?, mediopago = ?  WHERE idegresos_registro = ?`, [u.pago, u.adeudo, u.estatus, u.mediopago, u.idegresos_registro],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }

        if (res.affectedRows == 0) {
          return reject({ kind: "not_found" });
        }

        resolve(u);
      });
  });
};

egresos.addArchivoEgreso = (u) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`INSERT INTO fotos_egresos( name_base, name, tipo, id_egresos, extension, id_usuario_subio )VALUES(?,?,?,?,?,?)`,
      [u.name_base, u.name, u.tipo, u.id_egresos, u.extension, u.id_usuario_subio], (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }
        resolve({ idfotos_egresos: res.insertId, ...u })
      })
  })
}

egresos.addComprobantePartida = (u) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`INSERT INTO fotos_comprobantes( name_base, name, id_partida, extension)VALUES(?,?,?,?);`,
      [u.name_base, u.name, u.id_partida, u.extension], (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }
        resolve({ idfotos_comprobantes: res.insertId, ...u })
      })
  })
}


egresos.getFotoRequi = (id) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM fotos_egresos WHERE id_egresos IN (?) AND deleted = 0;`, [id], (err, res) => {
      if (err) { return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

egresos.getFotoPartida = (id) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM fotos_comprobantes WHERE id_partida IN (?) AND deleted = 0;`, [id], (err, res) => {
      if (err) { return reject({ message: 'No se pudieron traer los comprobantes' }) }
      resolve(res)
    })
  })
}

egresos.eliminarPartida = (id) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE requisicion_compra_detalle SET activo_sn = 0 WHERE id = ?;`, [id],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }

        if (res.affectedRows == 0) {
          return reject({ kind: "not_found" });
        }

        resolve({ id });
      });
  });
};

egresos.preAutorizarRequi = (id_usuario_autoriza_rechaza, id_requisicion_compra_estatus, id_requisicion) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE requisicion_compra SET id_usuario_autoriza_rechaza = ?, id_requisicion_compra_estatus = ? WHERE id = ?;`, [id_usuario_autoriza_rechaza, id_requisicion_compra_estatus, id_requisicion],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }

        if (res.affectedRows == 0) {
          return reject({ kind: "not_found" });
        }

        resolve({ id_usuario_autoriza_rechaza, id_requisicion_compra_estatus, id_requisicion });
      });
  });
};

egresos.updateEstatusRequi = (id_requisicion_compra_estatus, id_requisicion) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE requisicion_compra SET id_requisicion_compra_estatus = ? WHERE id = ?;`, [id_requisicion_compra_estatus, id_requisicion],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }

        if (res.affectedRows == 0) {
          return reject({ kind: "not_found" });
        }

        resolve({ id_requisicion_compra_estatus, id_requisicion });
      });
  });
};

egresos.updateEstatusPartida = (id) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE requisicion_compra_detalle SET estatus = 2 WHERE id = ?;`, [id],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }

        if (res.affectedRows == 0) {
          return reject({ kind: "not_found" });
        }

        resolve({ id });
      });
  });
};

egresos.updateEstatusPartidaRevision = (id) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE requisicion_compra_detalle SET estatus = 8 WHERE id = ?;`, [id],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }
        resolve(res);
      });
  });
};

egresos.updateAceptarComprobante = (id) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE requisicion_compra_detalle SET estatus = 3 WHERE id = ?;`, [id],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }

        if (res.affectedRows == 0) {
          return reject({ kind: "not_found" });
        }

        resolve({ id });
      });
  });
};

egresos.updateRechazarPartidaRequi = (id) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE requisicion_compra_detalle SET estatus = 4 WHERE id = ?;`, [id],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }

        if (res.affectedRows == 0) {
          return reject({ kind: "not_found" });
        }

        resolve({ id });
      });
  });
};

egresos.updateTipoPago = (tipo_pago, id_requisicion_compra) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE requisicion_compra_detalle SET tipo_pago = ?, estatus = 1 
    WHERE id_requisicion_compra = ? AND id > 0;`, [tipo_pago, id_requisicion_compra],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }

        if (res.affectedRows == 0) {
          return reject({ message: 'No existen partidas' });
        }

        resolve({ tipo_pago, id_requisicion_compra });
      });
  });
};

egresos.updateEgresoMensaje = (mensaje, id) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE historial_egreso SET mensaje = ? WHERE id = ?;`, [mensaje, id],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }

        if (res.affectedRows == 0) {
          return reject({ kind: "not_found" });
        }

        resolve({ mensaje, id });
      });
  });
};


egresos.addegresoMensaje = (u, result) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`INSERT INTO historial_egreso (id_partida, mensaje) VALUES (?,?);`,
      [u.id_partida, u.mensaje],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }

        resolve({ ...u });
      });
  });
};

egresos.consultarMensajesRechazoEgreso = (id_partida) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT idhistorial_egreso, id_partida, mensaje, fecha, estatus, respuesta FROM historial_egreso WHERE id_partida = ?;`, [id_partida], (err, res) => {
      if (err) { return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

egresos.updateDeleteComprobante = (id) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE fotos_comprobantes SET deleted = 1 WHERE id_partida = ?;`, [id],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }

        resolve({ id });
      });
  });
};


egresos.updateMensajeRechazoRespuesta = (respuesta, id_partida, id) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE historial_egreso
    SET respuesta = ?
    WHERE id_partida = ?
    AND idhistorial_egreso = (
        SELECT idhistorial_egreso
        FROM historial_egreso
        WHERE id_partida = ?
        ORDER BY idhistorial_egreso DESC
        LIMIT 1);`, [respuesta, id_partida, id],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }
        resolve(res);
      });
  });
};


egresos.addComentarioComprobante = (id_partida, sueldo_1, sueldo_2, comisiones, caja_de_ahorro, a_depositar, dep_1, dep_2, deuda) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`INSERT INTO comentario_comprobante(id_partida, sueldo_1, sueldo_2, comisiones, caja_de_ahorro, a_depositar, dep_1, dep_2, deuda) 
    VALUES (?,?,?,?,?,?,?,?,?);`, [id_partida, sueldo_1, sueldo_2, comisiones, caja_de_ahorro, a_depositar, dep_1, dep_2, deuda], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, id_partida, sueldo_1, sueldo_2, comisiones, caja_de_ahorro, a_depositar, dep_1, dep_2, deuda });
    });
  });
};


egresos.getComentarioComprobante = (id_partida) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT id_comentario_comprobante, id_partida, sueldo_1, sueldo_2, comisiones, caja_de_ahorro, a_depositar, dep_1, dep_2, deuda FROM comentario_comprobante WHERE id_partida = ?
    ORDER BY id_comentario_comprobante DESC
    LIMIT 1;`, [id_partida], (err, res) => {
      if (err) { return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

egresos.addElementoRequisicion = (elemento_mantenimiento, id_usuario_ultimo_cambio) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`INSERT INTO elementosmantenimiento(elemento_mantenimiento, id_tipo_elemento, activo_sn, id_usuario_ultimo_cambio) 
    VALUES (?,2,1,?);`, [elemento_mantenimiento, id_usuario_ultimo_cambio], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, elemento_mantenimiento, id_usuario_ultimo_cambio });
    });
  });
};

egresos.getProyectos = () => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT id_proyecto, proyecto, estatus, deleted FROM proyectos WHERE deleted = 0
    ORDER BY proyecto;`, [], (err, res) => {
      if (err) { return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

egresos.updateProyectos = (id, u, result) => {
  sqlERP.query(`UPDATE proyectos SET proyecto = ?, estatus = ?, deleted = ? WHERE id_proyecto = ?`, [u.proyecto, u.estatus, u.deleted, id],
    (err, res) => {
      if (err) { result(err, null); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id, ...u });
    })
}


egresos.addProyectos = (u, result) => {
  sqlERP.query(`INSERT INTO proyectos(proyecto, estatus) 
  VALUES (?,?);`, [u.proyecto, u.estatus],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      result(null, { id: res.insertId, ...u });
    })
};



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//26/03/23//
egresos.updateMandarEgreso = (tipo_pago, id) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE requisicion_compra_detalle SET tipo_pago = ?, estatus = 1 
    WHERE id = ? AND id > 0;`, [tipo_pago, id],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }

        if (res.affectedRows == 0) {
          return reject({ message: 'No existen partidas' });
        }

        resolve({ tipo_pago, id });
      });
  });
};


egresos.getSinProcesarPartidas = ( id_requisicion ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM requisicion_compra_detalle WHERE id_requisicion_compra = ? AND estatus = 0 AND activo_sn = 1;`,[  id_requisicion ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


egresos.getCiclosEgresos = result => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT ci.id_ciclo, ci.ciclo, COUNT(r.id) AS Numero_Egresos, CONCAT('$',FORMAT(SUM(r.costo_total), 2, 'en-us')) AS Total,
    CASE WHEN ci.ciclo LIKE '%FE%' THEN 2
    WHEN ci.ciclo LIKE '%INBI%' THEN 1
    ELSE 1 END AS escuela,
    (SELECT COUNT(r2.id) FROM requisicion_compra_detalle r2
    LEFT JOIN requisicion_compra rc2 ON r2.id_requisicion_compra = rc2.id
    WHERE r2.activo_sn = 1 AND rc2.id_ciclo = ci.id_ciclo AND r2.estatus = 1 AND r2.tipo_pago != 0) AS Sin_Comprobante,
    (SELECT COUNT(r2.id) FROM requisicion_compra_detalle r2
    LEFT JOIN requisicion_compra rc2 ON r2.id_requisicion_compra = rc2.id
    WHERE r2.activo_sn = 1 AND rc2.id_ciclo = ci.id_ciclo AND r2.estatus = 2 AND r2.tipo_pago != 0) AS Con_Comprobante,
    (SELECT COUNT(r2.id) FROM requisicion_compra_detalle r2
    LEFT JOIN requisicion_compra rc2 ON r2.id_requisicion_compra = rc2.id
    WHERE r2.activo_sn = 1 AND rc2.id_ciclo = ci.id_ciclo AND r2.estatus = 3 AND r2.tipo_pago != 0) AS Autorizado,
    (SELECT COUNT(r2.id) FROM requisicion_compra_detalle r2
    LEFT JOIN requisicion_compra rc2 ON r2.id_requisicion_compra = rc2.id
    WHERE r2.activo_sn = 1 AND rc2.id_ciclo = ci.id_ciclo AND r2.estatus = 7 AND r2.tipo_pago != 0) AS En_Revision,
    (SELECT COUNT(r2.id) FROM requisicion_compra_detalle r2
    LEFT JOIN requisicion_compra rc2 ON r2.id_requisicion_compra = rc2.id
    WHERE r2.activo_sn = 1 AND rc2.id_ciclo = ci.id_ciclo AND r2.estatus = 8 AND r2.tipo_pago != 0) as Rechazado
    FROM requisicion_compra_detalle r
    LEFT JOIN requisicion_compra rc ON r.id_requisicion_compra = rc.id
    LEFT JOIN ciclos ci ON ci.id_ciclo = rc.id_ciclo
    LEFT JOIN proyectos pr ON pr.id_proyecto = rc.id_proyecto
    WHERE (r.activo_sn != 0) AND (r.tipo_pago != 0) AND r.activo_sn = 1 AND ci.activo_sn = 1
    GROUP BY ci.id_ciclo
    ORDER BY ci.ciclo;`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};


egresos.updatePartida = (id_elemento_requisicion, id_plantel, concepto, cantidad, costo_unitario, costo_total, id) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE requisicion_compra_detalle SET id_elemento_requisicion = ?, id_plantel = ?, concepto = ?, cantidad = ?, costo_unitario = ?, costo_total = ?
    WHERE id = ?;`, [id_elemento_requisicion, id_plantel, concepto, cantidad, costo_unitario, costo_total, id],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }

        if (res.affectedRows == 0) {
          return reject({ kind: "not_found" });
        }

        resolve(id_elemento_requisicion, id_plantel, concepto, cantidad, costo_unitario, costo_total, id);
      });
  });
};


egresos.updateRequi = (id_ciclo, comentarios, id) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE requisicion_compra SET id_ciclo = ?, comentarios = ?
    WHERE id = ?;`, [id_ciclo, comentarios, id],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }

        if (res.affectedRows == 0) {
          return reject({ kind: "not_found" });
        }

        resolve(id_ciclo, comentarios, id);
      });
  });
};



egresos.solicitarCambioEgreso = (u, result) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`INSERT INTO cambios_egresos(idpartida, idsolicito, idautorizo, eliminar, cambio_ciclo, ciclo_anterior, nuevo_ciclo, notas, concepto ) VALUES (?,?,?,?,?,?,?,?,?);`, 
      [u.idpartida, u.idsolicito, u.idautorizo, u.eliminar, u.cambio_ciclo, u.ciclo_anterior, u.nuevo_ciclo, u.notas, u.concepto],(err, res) => {
      if (err) {
        reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};


egresos.updateCambioEgreso = ( idautorizo, estatus, deleted, id ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE cambios_egresos SET idautorizo = ?, estatus = ?, deleted = ? WHERE idcambios_egresos = ?`, [idautorizo, estatus, deleted, id ],
      (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return reject({ kind: "not_found" });
      }

      resolve({ idautorizo, estatus, deleted, id });
    });
  });
};

egresos.getEgresosEliminados = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT c.* FROM cambios_egresos c WHERE c.deleted = 0 AND c.autorizo > 0 AND estatus = 1;`,(err, res) => {
      if (err) {
        reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};


egresos.updateCicloRequi = ( nuevo_ciclo, id_requisicion_compra ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE requisicion_compra SET id_ciclo = ? WHERE id = ?;`,[ nuevo_ciclo, id_requisicion_compra ],(err, res) => {
      if (err) {
        reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};


egresos.eliminarPartida = ( id ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE requisicion_compra_detalle SET activo_sn = 0 WHERE id = ? ;`,[id],(err, res) => {
      if (err) {
        reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};


egresos.updateConcepto = ( nuevo_concepto, id ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`UPDATE requisicion_compra_detalle SET concepto = ? WHERE id = ? ;`,[ nuevo_concepto, id ],(err, res) => {
      if (err) {
        reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};

egresos.papeleraEgresos = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT r.*, p.plantel, e.elemento_mantenimiento, c.nomina, c.empleado, ci.ciclo, c.cuenta, pr.proyecto, 
    CASE r.tipo_pago 
      WHEN 1 THEN 'Tarjeta' 
      WHEN 2 THEN 'Transferencia/Deposito' 
      WHEN 3 THEN 'Efectivo'
    ELSE 'Tipo de pago desconocido' 
    END AS tipo_pago, 
  CASE r.estatus
      WHEN 1 THEN 'Sin Comprobante' 
      WHEN 2 THEN 'Con Comprobante' 
      WHEN 3 THEN 'Autorizado' 
      WHEN 7 THEN 'En Revision' 
      WHEN 8 THEN 'Rechazado' 
    ELSE 'Sin Estatus' 
    END AS estatus,
    (SELECT name from fotos_comprobantes WHERE id_partida = r.id AND deleted = 0 limit 1) AS nombre_foto,
    (SELECT idfotos_comprobantes from fotos_comprobantes WHERE id_partida = r.id limit 1) AS fecha_orden,
    IF(ce.estatus = 0, 'editar','') AS clase_row, ce.idcambios_egresos, ce.nuevo_ciclo, ce.idpartida, ce.idsolicito, ce.idautorizo, ce.eliminar, ce.cambio_ciclo, 
    ce.ciclo_anterior, ce.nuevo_ciclo, ce.notas, ce.concepto AS nuevo_concepto, ce.estatus AS estatusSolicitud, u.nombre_completo, u2.nombre_completo AS nombreAutorizo
    FROM cambios_egresos ce 
    LEFT JOIN requisicion_compra_detalle r ON r.id = ce.idpartida
    LEFT JOIN planteles p ON p.id_plantel= r.id_plantel
    LEFT JOIN elementosmantenimiento e ON e.id_elemento_mantenimiento = r.id_elemento_requisicion 
    LEFT JOIN configuracion_nomina c ON c.id_usuario = r.id_empleado
    LEFT JOIN requisicion_compra rc ON r.id_requisicion_compra = rc.id
    LEFT JOIN ciclos ci ON ci.id_ciclo = rc.id_ciclo
    LEFT JOIN proyectos pr ON pr.id_proyecto = rc.id_proyecto
    LEFT JOIN usuarios u ON u.id_usuario = ce.idsolicito
    LEFT JOIN usuarios u2 ON u2.id_usuario = ce.idautorizo
    WHERE (r.activo_sn != 0) AND (r.tipo_pago != 0)
    ORDER BY fecha_orden DESC;`, (err, res) => {
      if (err) {
        reject({ message: err.sqlMessage })
      }
      resolve( res )
    })
  })
};



module.exports = egresos;