const { result }  = require("lodash");
const sqlERP      = require("../dbErpPrueba.js");


//const constructor
const egresos = (clases) => {};

egresos.addRequisicionOperaciones = ( u ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO requisicion_compra( id_usuario_solicita, id_sucursal_solicita, id_usuario_compra, id_ciclo, id_requisicion_compra_estatus, comentarios, id_usuario_ultimo_cambio, tipo_requisicion )
      VALUES(?,?,?,?,?,?,?,?)`, 
      [ u.id_usuario_solicita, u.id_sucursal_solicita, u.id_usuario_compra, u.id_ciclo, u.id_requisicion_compra_estatus, u.comentarios, u.id_usuario_ultimo_cambio, u.tipo_requisicion ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ idrequisicion_compra: res.insertId, ...u })
    })
  })
}

egresos.addDetalleRequiOper = ( idrequisicion_compra, u, id_usuario_ultimo_cambio ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO requisicion_compra_detalle( id_requisicion_compra, id_elemento_requisicion, id_plantel, concepto, cantidad, costo_unitario, costo_total, id_usuario_ultimo_cambio, id_empleado )
      VALUES(?,?,?,?,?,?,?,?,?)`, 
      [ idrequisicion_compra, u.id_elemento_requisicion, u.id_plantel, u.concepto, u.cantidad, u.costo_unitario, u.costo_total, id_usuario_ultimo_cambio, u.id_empleado ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ iddetalle_requi: res.insertId, ...u })
    })
  })
}

egresos.addFyn_proc_egresosOper = ( iddetalle_requi, id_usuario_ultimo_cambio ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO fyn_proc_egresos( id_requisicion_compra_detalle, id_usuario_ultimo_cambio )VALUES(?,?)`, [ iddetalle_requi, id_usuario_ultimo_cambio ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ idfyn_proc_egresos_oper: res.insertId, iddetalle_requi, id_usuario_ultimo_cambio })
    })
  })
}

egresos.consultarPagoServicios = ( fecha_inicio, fecha_final ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT r.id, u.nombre_completo, c.ciclo, e.descripcion, r.comentarios FROM requisicion_compra r 
      LEFT JOIN usuarios u ON u.id_usuario = r.id_usuario_solicita
      LEFT JOIN ciclos c ON c.id_ciclo = r.id_ciclo
      LEFT JOIN requisicion_compra_estatus e ON e.id = r.id_requisicion_compra_estatus
      WHERE DATE(r.fecha_alta) BETWEEN ? AND ? 
      AND r.tipo_requisicion = 3;`,[ fecha_inicio, fecha_final ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

egresos.consultarDetalleRequiId = ( ids ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT r.*, p.plantel, e.elemento_mantenimiento, c.nomina, c.empleado, c.cuenta FROM requisicion_compra_detalle r
      LEFT JOIN planteles p ON p.id_plantel= r.id_plantel
      LEFT JOIN elementosmantenimiento e ON e.id_elemento_mantenimiento = r.id_elemento_requisicion 
      LEFT JOIN configuracion_nomina c ON c.id_usuario = r.id_empleado
      WHERE id_requisicion_compra IN (?);`,[ ids ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

egresos.getElementosCompra = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM elementosmantenimiento;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

egresos.updatePagoegresos = ( u ) => {
	return new Promise(( resolve, reject )=>{
	  sqlFAST.query(`UPDATE egresos_registro SET pago_realizado = ?, adeudo = ?, estatus = ?, mediopago = ?  WHERE idegresos_registro = ?`,[ u.pago, u.adeudo, u.estatus, u.mediopago, u.idegresos_registro ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( u );
	  });
	});
};

module.exports = egresos;