// const sqlERP   = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");
const sqlERPNUEVO = require("../db.js");
const md5 = require('md5');
// constructor
const catalogos = (data) => {};



catalogos.getCicloEscuela = ( ciclo, escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT * FROM ciclos WHERE iderp = ?;`,[ ciclo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res[0])
    })
  });
};


/***************************************************/
/**********************HORARIOS********************/
/***************************************************/

catalogos.getHorariosLMSFAST = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT * FROM horarios WHERE deleted = 0`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.horariosAddFast = (h, escuela) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`INSERT INTO horarios ( nombre, hora_inicio, hora_fin, comentarios, estatus, deleted, iderp) VALUES (?,?,?,?,?,?,?)`,
      [ h.horario, h.hora_inicio.substr(0,5), h.hora_fin.substr(0,5), h.comentarios , h.activo_sn , 0 ,h.id_horario ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve({ id_horario: res.insertId, ...h })
    })
  });
};


catalogos.getHorariosLMSERP = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM horarios WHERE activo_sn = 1;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};


/***************************************************/
/************************CICLOS*********************/
/***************************************************/

catalogos.getCiclosLMSFAST = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT * FROM ciclos WHERE deleted = 0`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.ciclosAddFast = (h) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`INSERT INTO ciclos ( nombre, estatus, fecha_inicio, fecha_fin, fecha_inicio_excluir, fecha_fin_excluir, comentarios, iderp) VALUES (?,?,?,?,?,?,?,?)`,
      [ h.ciclo, h.activo_sn, h.fecha_inicio_ciclo, h.fecha_fin_ciclo , null, null ,h.comentarios, h.id_ciclo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve({ id_horario: res.insertId, ...h })
    })
  });
};

catalogos.ciclosAddInbi = (h) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`INSERT INTO ciclos ( nombre, estatus, fecha_inicio, fecha_fin, fecha_inicio_excluir, fecha_fin_excluir, comentarios, iderp) VALUES (?,?,?,?,?,?,?,?)`,
      [ h.ciclo, h.activo_sn, h.fecha_inicio_ciclo, h.fecha_fin_ciclo , null, null ,h.comentarios, h.id_ciclo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve({ id_horario: res.insertId, ...h })
    })
  });
};

catalogos.getCiclosLMSERP = ( busqueda ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM ciclos WHERE activo_sn = 1 ${ busqueda };`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};


/***************************************************/
/************************GRUPOS*********************/
/***************************************************/

catalogos.getGruposLMSFAST = ( ciclo, escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT g.*, cu.nombre AS curso, c.nombre AS ciclo, h.hora_inicio, h.hora_fin FROM grupos g
			LEFT JOIN ciclos c ON c.id = g.id_ciclo
			LEFT JOIN cursos cu ON cu.id = g.id_curso
			LEFT JOIN horarios h ON h.id = g.id_horario
			WHERE g.deleted = 0 AND c.iderp = ${ ciclo };`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.getGruposLMSFASTAll = ( ciclo, escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT g.*, cu.nombre AS curso, c.nombre AS ciclo, h.hora_inicio, h.hora_fin FROM grupos g
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN cursos cu ON cu.id = g.id_curso
      LEFT JOIN horarios h ON h.id = g.id_horario
      WHERE c.iderp = ${ ciclo };`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.gruposAddFast = (h, ciclo, horario, nivel, escuela) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`INSERT INTO grupos ( nombre, estatus, id_unidad_negocio, id_plantel, id_ciclo, id_curso, id_horario, id_nivel, iderp) VALUES (?,?,?,?,?,?,?,?,?)`,
      [ h.grupo, h.activo_sn, 2, h.id_plantel , ciclo, 0 , horario, nivel, h.id_grupo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve({ id_grupo: res.insertId, ...h })
    })
  });
};


catalogos.getGruposLMSERP = ( ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM grupos WHERE id_ciclo = ${ ciclo };`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.updateGruposLMSFAST = ( optimizado, deleted, online, id, escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`UPDATE grupos SET optimizado = ${ optimizado }, deleted = ${ deleted }, online = ${ online } WHERE id = ${ id }`,
      (err, res) => {
      if (err) { reject(err); return; }
      if (res.affectedRows == 0) {
        reject(err)
        return;
      }
      resolve(res);
    })
  });
};

/***************************************************/
/***********************NIVELES*********************/
/***************************************************/

catalogos.getNivelesLMSFAST = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT * FROM niveles_evaluacion WHERE deleted = 0`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};


/***************************************************/
/**********************USUARIOS*********************/
/***************************************************/


catalogos.getUsuariosLMSFAST = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT * FROM usuarios WHERE deleted = 0 AND roll = 1 AND iderp <> 0;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.getGrupoAlumnosLMSFAST = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT ga.id_alumno, g.id_ciclo FROM grupo_alumnos ga LEFT JOIN grupos g ON g.id = ga.id_grupo;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.usuariosAddFast = (u, escuela) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`INSERT INTO usuarios (nombre,apellido_paterno,apellido_materno, usuario, password, email, estado, telefono, movil, id_direccion, roll, id_tipo_usuario, deleted, iderp, contra_universal, id_plantel) 
      VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
      [ u.nombre, u.apellido_paterno, u.apellido_materno, u.matricula, md5(u.matricula), u.email, 1 , u.telefono, u.celular, 0, 1, 1, 0, u.id_alumno, md5('12345r2'), 1 ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve({ id_usuario: res.insertId, ...u })
    })
  });
};

catalogos.registrarExci = ( u, id_ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`INSERT INTO exci_registro(nombre,correo,id_alumno,matricula,id_ciclo,pago_realizado,adeudo)VALUES(?,?,?,?,?,1399,0)`,
      [u.nombre,u.email,u.iderp,u.matricula,id_ciclo],(err, res) => {
      if(err){
        return reject( err.sqlMessage );
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

catalogos.existeUsuarioFast = ( id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM exci_registro WHERE id_alumno = ?;`,[ id_alumno ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res[0])
    })
  })
}

catalogos.addAlumnoFast = (u, escuela) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`INSERT INTO alumnos (id_usuario,matricula,id_plantel,id_unidad_negocio,comentarios,deleted) VALUES(?,?,?,?,?,?)`,
      [ u.id_usuario,u.matricula, 2 , 2 ,'',0 ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve({ id: res.insertId, ...u })
    })
  });
};

catalogos.addAlumnoGrupoFast = (a, id_grupo, escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`INSERT INTO grupo_alumnos (id_grupo, id_curso, id_alumno, deleted, estatus) VALUES(?,?,?,?,?)`,
      [ id_grupo, 1 ,a.id_usuario, 0, 1 ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve({ id: res.insertId, ...a })
    })
  });
};

catalogos.getUsuariosLMSINBI = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM usuarios WHERE deleted = 0 AND roll = 1 AND iderp <> 0;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.getGrupoAlumnosLMSINBI = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT ga.id_alumno, g.id_ciclo FROM grupo_alumnos ga LEFT JOIN grupos g ON g.id = ga.id_grupo;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.usuariosAddInbi = (u) => {
  return new Promise((resolve,reject)=>{
    console.log( u.nombre, u.apellido_paterno, u.apellido_materno, u.matricula, md5(u.matricula), u.email, 1 , u.telefono, u.celular, 0, 1, 1, 0, u.id_alumno, md5('12345r2'), 1 )
    sqlINBI.query(`INSERT INTO usuarios (nombre,apellido_paterno,apellido_materno, usuario, password, email, estado, telefono, movil, id_direccion, roll, id_tipo_usuario, deleted, iderp, contra_universal, id_plantel) 
      VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
      [ u.nombre, u.apellido_paterno, u.apellido_materno, u.matricula, md5(u.matricula), u.email, 1 , u.telefono, u.celular, 0, 1, 1, 0, u.id_alumno, md5('12345r2'), 1 ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve({ id_usuario: res.insertId, ...u })
    })
  });
};

catalogos.addAlumnoInbi = (u) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`INSERT INTO alumnos (id_usuario,matricula,id_plantel,id_unidad_negocio,comentarios,deleted) VALUES(?,?,?,?,?,?)`,
      [ u.id_usuario,u.matricula, 2 , 2 ,'',0 ], (err, res) => {
      if(err){
        console.log( 2)
        return reject({ message: err.sqlMessage })
      }
      resolve({ id: res.insertId, ...u })
    })
  });
};

catalogos.addAlumnoGrupoInbi = (a, id_grupo) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`INSERT INTO grupo_alumnos (id_grupo, id_curso, id_alumno) VALUES(?,?,?,?)`,
      [ id_grupo, 1 ,a.id_usuario ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve({ id: res.insertId, ...a })
    })
  });
};

catalogos.getUsuariosLMSERP = ( ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT a.id,a.id_alumno,a.id_grupo,a.id_ciclo,a.fecha_registro,g.id_plantel,g.id_horario,g.id_ciclo, g.id_nivel,
		  (SELECT n.nivel FROM niveles n WHERE n.id_nivel = g.id_nivel) AS nivel,
      al.*
		  FROM alumnos_grupos_especiales_carga a 
		  LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN alumnos al ON al.id_alumno = a.id_alumno
		  where a.id_ciclo = ${ ciclo } AND a.adeudo <= 0; `, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.getUsuariosLMSERPConAdeudo = ( ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT a.id,al.id_alumno,a.id_grupo,a.id_ciclo,a.fecha_registro,g.id_plantel,g.id_horario,g.id_ciclo, g.id_nivel,
      (SELECT n.nivel FROM niveles n WHERE n.id_nivel = g.id_nivel) AS nivel,
      al.*
      FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN alumnos al ON al.id_alumno = a.id_alumno
      where a.id_ciclo = ${ ciclo }; `, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.getUsuariosLMSERPEXCI = ( ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT a.id,a.id_alumno,a.id_grupo,a.id_ciclo,a.fecha_registro,g.id_plantel,g.id_horario,g.id_ciclo, g.id_nivel,
      (SELECT n.nivel FROM niveles n WHERE n.id_nivel = g.id_nivel) AS nivel,
      al.*
      FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN alumnos al ON al.id_alumno = a.id_alumno
      where a.id_ciclo = ${ ciclo } AND a.adeudo <= 0; `, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

/* ADEUDOS */
catalogos.getUsuariosAdeudosFAST = ( usuario ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM alumnos_adeudos WHERE matricula = ?;`,[ usuario ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.getUsuariosAdeudosINBI = ( usuario ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM alumnos_adeudos WHERE matricula = ?;`,[ usuario ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

/* ACCESOS */

catalogos.getUsuariosAccesoFAST = ( usuario ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM cardex_curso WHERE id_alumno = ? AND deleted = 0 ORDER BY fecha_registro desc LIMIT 1;`,[ usuario ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.getUltimoGrupoAlumnoFast = ( usuario ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT id_grupo, g.id_nivel FROM grupo_alumnos ga 
      LEFT JOIN grupos g ON g.id = ga.id_grupo
      WHERE ga.id_alumno = ?;`,[ usuario ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};


catalogos.getUsuariosAccesoINBI = ( usuario ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM cardex_curso WHERE id_alumno = ? AND deleted = 0 ORDER BY fecha_registro desc LIMIT 1;`,[ usuario ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.getUltimoGrupoAlumnoInbi = ( usuario ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT id_grupo, g.id_nivel FROM grupo_alumnos ga 
      LEFT JOIN grupos g ON g.id = ga.id_grupo
      WHERE ga.id_alumno = ?;`,[ usuario ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

/***************************************************/
/**********************TEACHERS*********************/
/***************************************************/

catalogos.getTeachersLMSFAST = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT *, CONCAT(nombre, ' ', apellido_paterno, ' ', IFNULL(apellido_materno,'')) AS nombre_teacher FROM usuarios WHERE deleted = 0 AND roll = 3;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.teachersAddFast = (u, email, escuela) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`INSERT INTO usuarios (nombre,apellido_paterno,apellido_materno, usuario, password, email, estado, telefono, movil, id_direccion, roll, id_tipo_usuario, deleted, iderp, contra_universal, id_plantel) 
      VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
      [ u.nombre_usuario, u.apellido_usuario, '', email, md5('12345'), u.email, 1 , u.telefono, u.telefono, 0, 3, 3, 0, u.id_usuario, md5('12345r2'), 1 ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve({ id_usuario: res.insertId, ...u })
    })
  });
};

catalogos.getTeachersLMSERP = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM usuarios u
      LEFT JOIN perfilesusuario pu ON pu.id_usuario = u.id_usuario
      LEFT JOIN perfiles p ON p.id_perfil = pu.id_perfil
      WHERE p.id_perfil = 9 AND u.activo_sn = 1
      ORDER BY nombre_completo;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};



/***************************************************/
/*******************GRUPO_TEACHERS******************/
/***************************************************/

catalogos.getGruposTeacherLMSFAST = ( ciclo, escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT g.id AS id_grupo, g.iderp, g.editado, g.nombre AS grupo, gt.id_teacher, gt.id_teacher_2, CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher,
      g.id_nivel, g.id_curso, cu.nombre AS curso, g.id_ciclo, c.fecha_inicio, c.fecha_fin,
      CONCAT(u2.nombre, " ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno,"")) AS teacher2 FROM grupo_teachers gt
      LEFT JOIN grupos g ON g.id = gt.id_grupo
      LEFT JOIN usuarios u ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN cursos cu ON cu.id = g.id_curso
      WHERE c.iderp = ${ ciclo } ORDER BY g.editado;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.addGruposTeacherFast = (id_grupo, id_teacher, id_teacher_2, escuela) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`INSERT INTO grupo_teachers ( id_grupo, id_teacher, id_teacher_2) VALUES (?,?,?)`,
      [ id_grupo, id_teacher, id_teacher_2 ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve({ id_grupo, id_teacher, id_teacher_2 })
    })
  });
};

catalogos.updateGruposTeacherFast = (id_grupo, id_teacher, id_teacher_2, escuela) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`UPDATE grupo_teachers SET id_grupo =?, id_teacher =?, id_teacher_2 =?  WHERE id > 0 AND id_grupo = ?`,
      [id_grupo, id_teacher, id_teacher_2, id_grupo],
      (err, res) => {
        if (err) { reject(err); return; }
        if (res.affectedRows == 0) {
          reject(err)
          return;
        }
        resolve(res);
    })
  });
};

catalogos.getGruposTeacherLMSINBI = ( ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id AS id_grupo, g.iderp,g.nombre AS grupo, gt.id_teacher, gt.id_teacher_2, CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher,
      CONCAT(u2.nombre, " ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno,"")) AS teacher2 FROM grupo_teachers gt
      LEFT JOIN grupos g ON g.id = gt.id_grupo
      LEFT JOIN usuarios u ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      WHERE c.iderp = ${ ciclo };`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.getGruposTeacherLMSERP = ( ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT m.id_grupo, u.email, m.numero_teacher, u.id_usuario FROM maestrosgrupo m
      LEFT JOIN grupos g ON g.id_grupo = m.id_grupo
      LEFT JOIN usuarios u ON u.id_trabajador = m.id_maestro
      WHERE g.id_ciclo = ${ ciclo };`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};


/***************************************************/
/*******************GRUPO_ALUMNOS******************/
/***************************************************/

catalogos.getGruposAlumnosLMS = ( ciclo, escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT ga.id AS id_grupo_alumnos, g.id AS id_grupo, g.iderp, g.nombre AS grupo,  u.id AS id_alumno, u.iderp AS u_iderp, g.id_curso, g.id_nivel,
      CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL( u.apellido_materno, "")) AS nombre, u.usuario AS matricula, g.id_plantel, 
      IFNULL(DATE_FORMAT(u.ultimo_movimiento, "%d - %M - %Y"), 'SIN DATO') AS ultimo_movimiento,
      g.deleted, cc.calificacion_final_primera_oportunidad, calificacion_final_segunda_oportunidad, cc.id AS idkardex, cc.deleted AS kardexHabil FROM grupo_alumnos ga
      LEFT JOIN grupos g ON g.id = ga.id_grupo
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN usuarios u ON u.id = ga.id_alumno
      LEFT JOIN cardex_curso cc ON cc.id_alumno = ga.id_alumno AND cc.id_grupo = ga.id_grupo
      WHERE c.iderp = ${ ciclo } AND u.iderp > 0;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};


catalogos.getGruposAlumnosLMSExci = ( ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT e.* FROM exci_registro e WHERE e.id_ciclo = ${ ciclo };`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};


catalogos.getAlumnosLMSFAST = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT * FROM usuarios WHERE roll = 1 AND iderp > 0;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.addGrupoAlumnoFast = (id_grupo, id_alumno, escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`INSERT INTO grupo_alumnos (id_grupo,id_curso,id_alumno,deleted,estatus) VALUES(?,?,?,?,?)`,[ id_grupo, 1, id_alumno, 0, 1 ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve({ id: res.insertId })
    })
  });
};

catalogos.updateGruposAlumnoFast = (id_tabla, id_grupo, escuela) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`UPDATE grupo_alumnos SET id_grupo = ${ id_grupo} WHERE id = ${ id_tabla }`,(err, res) => {
      if (err) { reject(err); return; }
      if (res.affectedRows == 0) {
        reject(err)
        return;
      }
      resolve(res);
    })
  });
};

catalogos.addGrupoAlumnoInbi = (id_grupo, id_alumno) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`INSERT INTO grupo_alumnos (id_grupo,id_curso,id_alumno,deleted,estatus) 
      VALUES(?,?,?,?,?)`,
      [ id_grupo, 1, id_alumno, 0, 1 ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve({ id: res.insertId })
    })
  });
};

catalogos.updateGruposAlumnoInbi = (id_tabla, id_grupo) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`UPDATE grupo_alumnos SET id_grupo = ${ id_grupo} WHERE id = ${ id_tabla }`,(err, res) => {
      if (err) { reject(err); return; }
      if (res.affectedRows == 0) {
        reject(err)
        return;
      }
      resolve(res);
    })
  });
};

catalogos.getGruposAlumnosLMSERP = ( ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT a.id,a.id_alumno,a.id_grupo,a.id_ciclo,a.fecha_registro,g.id_plantel,g.id_horario,g.id_ciclo, g.id_nivel,
      (SELECT n.nivel FROM niveles n WHERE n.id_nivel = g.id_nivel) AS nivel, a.adeudo, al.* 
      FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
      LEFT JOIN alumnos al ON al.id_alumno = a.id_alumno
      where a.id_ciclo = ${ ciclo } AND a.adeudo <= 0 AND g.grupo NOT LIKE '%CAMBIO%';`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};


/***************************************************/
/*********************ASISTENCIAS*******************/
/***************************************************/

catalogos.getAsistenciasFast = ( id_grupo, id_usuario, escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT * FROM asistencia_grupo WHERE id_usuario = ${ id_usuario } AND id_grupo = ${ id_grupo } ;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.updateAsistenciasFast = ( nuevo, viejo, usuario, escuela) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`UPDATE asistencia_grupo SET id_grupo = ${ nuevo } WHERE id > 0 AND id_usuario = ${ usuario } AND id_grupo = ${ viejo };`,(err, res) => {
      if (err) { reject(err); return; }
      if (res.affectedRows == 0) {
        reject(err)
        return;
      }
      resolve(res);
    })
  });
};


/* VACACIONES */
catalogos.gruposFast = ( ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM grupos WHERE id_ciclo =  ${ ciclo } AND deleted = 0 AND editado = 1;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};


catalogos.gruposInbi = ( ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM grupos WHERE id_ciclo =  ${ ciclo } AND deleted = 0 AND editado = 1;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};


/***************************************************/
/************************ L M S ********************/
/***************************************************/

catalogos.getMatricula = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM alumnos WHERE id_alumno = ${ id }`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res[0])
    })
  });
};

catalogos.getResultadoToeflFAST = ( matricula ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM resul_toefl WHERE matricula = ? AND paso > 0 AND deleted = 0;`, [ matricula ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res[0])
    })
  });
};


catalogos.getResultadoToeflINBI = ( matricula ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM resul_toefl WHERE matricula = ? AND paso > 0 AND deleted = 0;`, [ matricula ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res[0])
    })
  });
};

catalogos.getResultadoToeflFAST2 = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT IF(SUM(c.respuesta) <>'',IF(SUM(c.respuesta) >= 30,2,1),0) AS paso FROM control_respuestas c
      LEFT JOIN recursos r ON r.id_evaluacion = c.idevaluacion WHERE c.idalumno = ? AND r.toefl = 1;`, [ id ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res[0])
    })
  });
};


catalogos.getResultadoToeflINBI2 = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT IF(SUM(c.respuesta) <>'',IF(SUM(c.respuesta) >= 30,2,1),0) AS paso FROM control_respuestas c
      LEFT JOIN recursos r ON r.id_evaluacion = c.idevaluacion WHERE c.idalumno = ? AND r.toefl = 1;`, [ id ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res[0])
    })
  });
};


catalogos.getCalificacionEvaluacionCertificacionFast = ( id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT c.calificacion FROM control_grupos_calificaciones_evaluaciones c
      LEFT JOIN grupos g ON g.id = c.id_grupo 
      WHERE c.id_evaluacion = 1470 
      AND c.id_alumno = ? 
      AND c.deleted = 0
      AND g.deleted = 0
      AND c.calificacion < 60
      LIMIT 1;
      `, [ id_alumno ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res[0])
    })
  });
};


catalogos.getCalificacionEvaluacionCertificacionInbi = ( id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT c.calificacion FROM control_grupos_calificaciones_evaluaciones c
      LEFT JOIN grupos g ON g.id = c.id_grupo 
      WHERE c.id_evaluacion = 2981 
      AND c.id_alumno = ? 
      AND c.deleted = 0
      AND g.deleted = 0
      AND c.calificacion < 60
      LIMIT 1;
      `, [ id_alumno ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res[0])
    })
  });
};


catalogos.existeClaseActivaFast = ( id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT ga.id_grupo, ga.id_alumno, h.hora_inicio, h.hora_fin, ci.iderp AS id_ciclo,
      (ELT(WEEKDAY(CURDATE()) + 1, 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado', 'domingo' )) AS dia_nombre,
      IF((ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,gt.id_teacher,gt.id_teacher_2) AS id_teacher,
      (ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 2)) AS num_teacher,
      (ELT(WEEKDAY(CURDATE()) + 1, lunes, 1, 1, 2, 2, 1, 2)) AS dia,
      TIMESTAMPDIFF(MINUTE,CURTIME(),TIME(h.hora_fin)) AS antes,
      TIMESTAMPDIFF(MINUTE,TIME(h.hora_fin),TIME(CURTIME())) AS despues
      FROM grupo_alumnos ga
      LEFT JOIN grupos g          ON g.id     = ga.id_grupo
      LEFT JOIN horarios h        ON h.id     = g.id_horario
      LEFT JOIN cursos c          ON c.id     = g.id_curso  
      LEFT JOIN ciclos ci         ON ci.id    = g.id_ciclo
      LEFT JOIN frecuencia f      ON f.id     = c.id_frecuencia
      LEFT JOIN grupo_teachers gt ON gt.id_grupo  = g.id
      LEFT JOIN asistencia_grupo a ON a.id_usuario = ${ id_alumno } AND DATE(fecha_asistencia) = CURDATE()
      WHERE ga.id_alumno = ${ id_alumno } 
        AND ga.deleted = 0
        AND a.valor_asistencia = 1 
          AND (ELT(WEEKDAY(CURDATE()) + 1, lunes, martes, miercoles, jueves, viernes, sabado, domingo )) = 1 
          AND g.id_ciclo IN (SELECT id FROM ciclos WHERE CURDATE() BETWEEN fecha_inicio AND fecha_fin AND deleted = 0)
          AND TIMESTAMPDIFF(MINUTE,TIME(CURTIME()),TIME(h.hora_fin)) <= 10;
      `, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res[0])
    })
  });
};


catalogos.existeClaseActivaInbi = ( id_alumno ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT ga.id_grupo, ga.id_alumno, h.hora_inicio, h.hora_fin, ci.iderp AS id_ciclo,
      (ELT(WEEKDAY(CURDATE()) + 1, 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado', 'domingo' )) AS dia_nombre,
      IF((ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,gt.id_teacher,gt.id_teacher_2) AS id_teacher,
      (ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 2)) AS num_teacher,
      (ELT(WEEKDAY(CURDATE()) + 1, lunes, 1, 1, 2, 2, 1, 2)) AS dia,
      TIMESTAMPDIFF(MINUTE,CURTIME(),TIME(h.hora_fin)) AS antes,
      TIMESTAMPDIFF(MINUTE,TIME(h.hora_fin),TIME(CURTIME())) AS despues
      FROM grupo_alumnos ga
      LEFT JOIN grupos g          ON g.id     = ga.id_grupo
      LEFT JOIN horarios h        ON h.id     = g.id_horario
      LEFT JOIN cursos c          ON c.id     = g.id_curso
      LEFT JOIN ciclos ci         ON ci.id    = g.id_ciclo  
      LEFT JOIN frecuencia f      ON f.id     = c.id_frecuencia
      LEFT JOIN grupo_teachers gt ON gt.id_grupo  = g.id
      LEFT JOIN asistencia_grupo a ON a.id_usuario = ${ id_alumno } AND DATE(fecha_asistencia) = CURDATE()
      WHERE ga.id_alumno = ${ id_alumno } 
        AND ga.deleted = 0
        AND a.valor_asistencia = 1 
          AND (ELT(WEEKDAY(CURDATE()) + 1, lunes, martes, miercoles, jueves, viernes, sabado, domingo )) = 1 
          AND g.id_ciclo IN (SELECT id FROM ciclos WHERE CURDATE() BETWEEN fecha_inicio AND fecha_fin AND deleted = 0)
          AND TIMESTAMPDIFF(MINUTE,TIME(CURTIME()),TIME(h.hora_fin)) <= 10;
      `, [ id_alumno ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res[0])
    })
  });
};

catalogos.existeValoracion = ( id_usuario, id_grupo, id_ciclo, escuela, dia ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM evaluacion_alumno_clase WHERE id_alumno = ? AND id_grupo = ? AND id_ciclo = ? AND escuela = ? AND dia = ? AND DATE(NOW()) = DATE(fecha_creacion);`
      ,[ id_usuario, id_grupo, id_ciclo, escuela, dia ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res[0])
    })
  });
};


catalogos.existeValoracion = ( id_usuario, id_grupo, id_ciclo, escuela, dia ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM evaluacion_alumno_clase WHERE id_alumno = ? AND id_grupo = ? AND id_ciclo = ? AND escuela = ? AND dia = ? AND DATE(NOW()) = DATE(fecha_creacion);`
      ,[ id_usuario, id_grupo, id_ciclo, escuela, dia ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res[0])
    })
  });
};

catalogos.agregarValoracion = ( id_alumno,id_grupo,id_ciclo,id_teacher, num_teacher, valoracion, escuela, dia ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`INSERT INTO evaluacion_alumno_clase(id_alumno,id_grupo,id_ciclo,id_teacher, num_teacher, valoracion, escuela, dia)
    VALUES(?,?,?,?,?,?,?,?)`,[id_alumno,id_grupo,id_ciclo,id_teacher, num_teacher, valoracion, escuela, dia],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve({ id: res.insertId })
    })
  })
}

catalogos.getValoracion = ( cicloInbi, cicloFast, fecha, fecha2, escuela ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT AVG(valoracion) AS promedio, id_teacher, escuela, count(id_teacher) AS cant_calif FROM evaluacion_alumno_clase
      WHERE id_ciclo IN( ?, ? ) AND escuela = ?
      AND DATE(fecha_creacion) BETWEEN ? AND ? 
      GROUP BY id_teacher, id_ciclo, escuela;`
      ,[ cicloInbi, cicloFast, escuela, fecha, fecha2 ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.getValoracionGrupo = ( cicloInbi, cicloFast, fecha, fecha2, escuela, interval ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT AVG(valoracion) AS promedio, id_teacher, escuela, count(id_teacher) AS cant_calif, id_grupo FROM evaluacion_alumno_clase
      WHERE id_ciclo IN( ?, ? ) AND escuela = ?
      AND DATE(fecha_creacion) BETWEEN DATE_ADD(DATE(?), INTERVAL ${ interval } DAY) AND DATE_ADD(DATE(?), INTERVAL ${ interval } DAY) 
      GROUP BY id_teacher, id_ciclo, escuela, id_grupo;`
      ,[ cicloInbi, cicloFast, escuela, fecha, fecha2 ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.getPromedioClaseFecha = ( cicloInbi, cicloFast, fecha, fecha2, escuela, interval ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT AVG(valoracion) AS promedio, count(id_alumno) AS cantidad FROM evaluacion_alumno_clase
      WHERE id_ciclo IN( ?, ? ) AND escuela = ?
      AND DATE(fecha_creacion) BETWEEN DATE_ADD(DATE(?), INTERVAL ${ interval } DAY) AND DATE_ADD(DATE(?), INTERVAL ${ interval } DAY) ;`
      ,[ cicloInbi, cicloFast, escuela, fecha, fecha2 ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res[0])
    })
  });
};


catalogos.alumnosGrupoFast = ( id_grupos ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT id_grupo AS cantidad, id_grupo FROM grupo_alumnos 
      WHERE id_grupo IN ( ? );
      `, [ id_grupos ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};


catalogos.alumnosGrupoInbi = ( id_grupos ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT id_grupo AS cantidad, id_grupo FROM grupo_alumnos 
      WHERE id_grupo IN ( ? );
      `, [ id_grupos ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.teachersFastDía = ( id_ciclo, fecha ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT gt.id_grupo, g.id_ciclo, g.id_curso, g.optimizado, g.deleted, g.editado, g.nombre AS grupo,
      (SELECT COUNT(id_grupo) AS cantidad FROM grupo_alumnos WHERE id_grupo = g.id) AS cant_alumnos,
      IF((ELT(WEEKDAY(DATE(?)) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")),CONCAT(u2.nombre, " ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno, ""))) AS teacher1,
      IF((ELT(WEEKDAY(DATE(?)) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1, gt.id_teacher, gt.id_teacher_2 ) AS id_teacher,
      IF((ELT(WEEKDAY(DATE(?)) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1, 1, 2 ) AS number_teacher
      FROM grupos g
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id 
      LEFT JOIN ciclos c      ON c.id  = g.id_ciclo
      LEFT JOIN usuarios u    ON u.id  = gt.id_teacher
      LEFT JOIN usuarios u2   ON u2.id = gt.id_teacher_2
      LEFT JOIN cursos cu     ON cu.id = g.id_curso  
      LEFT JOIN frecuencia f  ON f.id  = cu.id_frecuencia
      WHERE c.iderp = ? AND g.deleted = 0 AND g.editado = 1 AND id_teacher > 0 AND id_teacher_2 > 0 AND g.nombre NOT LIKE '%CERTI%'
      AND (SELECT COUNT(id_grupo) AS cantidad FROM grupo_alumnos WHERE id_grupo = g.id) > 0
      AND (ELT(WEEKDAY(DATE(?)) + 1, lunes, martes, miercoles, jueves, viernes, sabado, domingo )) = 1;`, [ fecha, fecha, fecha, id_ciclo, fecha ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.teachersFastDiaMaestros = ( id_ciclo, fecha, interval ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT gt.id_grupo, g.id_ciclo, g.id_curso, g.optimizado, g.deleted, g.editado, g.nombre AS grupo,
      (SELECT COUNT(id_grupo) AS cantidad FROM grupo_alumnos WHERE id_grupo = g.id) AS cant_alumnos,
      CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")) teacher1, gt.id_teacher,
      CONCAT(u2.nombre, " ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno, "")) AS teacher2, gt.id_teacher_2,
      DATE_ADD(DATE(?), INTERVAL ${ interval } DAY) AS fecha, cu.diasdeFrecuencia,
      (ELT(WEEKDAY(DATE_ADD(DATE(?), INTERVAL ${ interval } DAY)) + 1, 1, 1, 1, 2, 2, 1, 2)) AS num_teacher
      FROM grupos g
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id 
      LEFT JOIN ciclos c      ON c.id  = g.id_ciclo
      LEFT JOIN usuarios u    ON u.id  = gt.id_teacher
      LEFT JOIN usuarios u2   ON u2.id = gt.id_teacher_2
      LEFT JOIN cursos cu     ON cu.id = g.id_curso  
      LEFT JOIN frecuencia f  ON f.id  = cu.id_frecuencia
      WHERE c.iderp = ? AND g.deleted = 0 AND g.editado = 1 AND id_teacher > 0 AND id_teacher_2 > 0 AND g.nombre NOT LIKE '%CERTI%'
      AND (SELECT COUNT(id_grupo) AS cantidad FROM grupo_alumnos WHERE id_grupo = g.id) > 0
      AND (ELT(WEEKDAY(DATE_ADD(DATE(?), INTERVAL ${ interval } DAY)) + 1, lunes, martes, miercoles, jueves, viernes, sabado, domingo )) = 1;`, [ fecha, fecha, id_ciclo, fecha ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.teachersInbiDiaMaestros = ( id_ciclo, fecha, interval ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT gt.id_grupo, g.id_ciclo, g.id_curso, g.optimizado, g.deleted, g.editado, g.nombre AS grupo,
      (SELECT COUNT(id_grupo) AS cantidad FROM grupo_alumnos WHERE id_grupo = g.id) AS cant_alumnos,
      CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")) teacher1, gt.id_teacher,
      CONCAT(u2.nombre, " ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno, "")) AS teacher2, gt.id_teacher_2,
      DATE_ADD(DATE(?), INTERVAL ${ interval } DAY) AS fecha, cu.diasdeFrecuencia,
      (ELT(WEEKDAY(DATE_ADD(DATE(?), INTERVAL ${ interval } DAY)) + 1, 1, 1, 1, 2, 2, 1, 2)) AS num_teacher
      FROM grupos g
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id 
      LEFT JOIN ciclos c      ON c.id  = g.id_ciclo
      LEFT JOIN usuarios u    ON u.id  = gt.id_teacher
      LEFT JOIN usuarios u2   ON u2.id = gt.id_teacher_2
      LEFT JOIN cursos cu     ON cu.id = g.id_curso  
      LEFT JOIN frecuencia f  ON f.id  = cu.id_frecuencia
      WHERE c.iderp = ? AND g.deleted = 0 AND g.editado = 1 AND id_teacher > 0 AND id_teacher_2 > 0 AND g.nombre NOT LIKE '%CERTI%'
      AND (SELECT COUNT(id_grupo) AS cantidad FROM grupo_alumnos WHERE id_grupo = g.id) > 0
      AND (ELT(WEEKDAY(DATE_ADD(DATE(?), INTERVAL ${ interval } DAY)) + 1, lunes, martes, miercoles, jueves, viernes, sabado, domingo )) = 1;`, [ fecha, fecha, id_ciclo, fecha ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.getDiferenciaDias = ( fecha, fecha2 ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT TIMESTAMPDIFF(DAY, ? , ? ) AS dias;`, [ fecha, fecha2 ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res[0])
    })
  });
};

catalogos.returnFecha = ( fecha, interval ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT DATE_ADD(DATE(?), INTERVAL ${ interval } DAY) AS fecha2;`, [ fecha ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res[0])
    })
  });
};

catalogos.teachersInbi1 = ( id_ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT gt.id_grupo, gt.id_teacher, g.nombre AS grupo,
      (SELECT COUNT(id_grupo) AS cantidad FROM grupo_alumnos WHERE id_grupo = g.id) AS cant_alumnos,
      CONCAT(u.nombre, ' ', u.apellido_paterno, IFNULL( u.apellido_materno ,'')) AS teacher1
      FROM grupos g
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id 
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN usuarios u ON u.id = gt.id_teacher
      WHERE c.iderp = ? AND g.deleted = 0 AND g.editado = 1 AND id_teacher > 0 AND id_teacher_2 > 0 AND g.nombre NOT LIKE '%CERTI%'
      AND (SELECT COUNT(id_grupo) AS cantidad FROM grupo_alumnos WHERE id_grupo = g.id) > 0;`, [ id_ciclo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.teachersInbi2 = ( id_ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT gt.id_grupo, gt.id_teacher, g.nombre AS grupo,
      (SELECT COUNT(id_grupo) AS cantidad FROM grupo_alumnos WHERE id_grupo = g.id) AS cant_alumnos,
      CONCAT(u2.nombre, ' ', u2.apellido_paterno, IFNULL( u2.apellido_materno ,'')) AS teacher1
      FROM grupos g
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id 
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
      WHERE c.iderp = ? AND g.deleted = 0 AND g.editado = 1 AND id_teacher > 0 AND id_teacher_2 > 0 AND g.nombre NOT LIKE '%CERTI%'
      AND (SELECT COUNT(id_grupo) AS cantidad FROM grupo_alumnos WHERE id_grupo = g.id) > 0;`, [ id_ciclo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.getCicloActual = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT id_ciclo AS ciclo_inbi, id_ciclo_relacionado AS ciclo_fast, fecha_inicio_ciclo, fecha_fin_ciclo FROM ciclos 
      WHERE activo_sn = 1 
      AND CURRENT_DATE() BETWEEN fecha_inicio_ciclo AND fecha_fin_ciclo
      AND ciclo NOT LIKE '%INDUC%'
      AND ciclo NOT LIKE '%EXCI%'
      AND ciclo NOT LIKE '%INVER%'
      AND ciclo NOT LIKE '%CAMPA%'
      AND ciclo NOT LIKE '%CAMBI%'
      AND ciclo NOT LIKE '%FE%'
      AND ciclo LIKE '%CICLO%';`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  });
};

catalogos.getAlumnosCicloActual = ( ciclo_inbi, ciclo_fast ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT a.id_alumno, CONCAT( a.nombre, ' ', a.apellido_paterno, ' ', IFNULL(a.apellido_materno,'')) AS alumno, a.matricula, a.telefono, a.celular,
      ac.adeudo, g.id_grupo, g.grupo, IF(LOCATE('FAST',g.grupo),2,1) AS escuela FROM alumnos_grupos_especiales_carga ac 
      LEFT JOIN alumnos a ON a.id_alumno = ac.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = ac.id_grupo
      WHERE ac.id_ciclo IN ( ?, ? ) ORDER BY adeudo DESC;`,[ ciclo_inbi, ciclo_fast ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};

// Verficar que exista el grupo
catalogos.existeGrupoInbi = ( id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM grupos WHERE iderp = ?;`,[ id_grupo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  });
};

catalogos.existeGrupoFast = ( id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM grupos WHERE iderp = ?;`,[ id_grupo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  });
};

catalogos.eliminarAsistenciasFast = ( id_usuario, id_ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`DELETE FROM asistencia_grupo WHERE id > 0 AND id_usuario = ? AND id_grupo IN (SELECT g.id FROM grupos g LEFT JOIN ciclos c ON c.id = g.id_ciclo WHERE c.iderp = ?)`,
      [ id_usuario, id_ciclo ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.eliminarAsistenciasInbi = ( id_usuario, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`DELETE FROM asistencia_grupo WHERE id > 0 AND id_usuario = ? AND id_grupo IN (SELECT g.id FROM grupos g LEFT JOIN ciclos c ON c.id = g.id_ciclo WHERE c.iderp = ?)`,
      [ id_usuario, id_ciclo ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.getFechasAsistenciaGrupoFast = ( id_grupo, id_nivel ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM grupo_${id_grupo}_cursociclo_clave_0_nivel_${id_nivel};`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};

catalogos.getFechasAsistenciaGrupoInbi = ( id_grupo, id_nivel ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM grupo_${id_grupo}_cursociclo_clave_0_nivel_${id_nivel};`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};

catalogos.addAsistenciaFast = ( id_grupo, id_alumno, numerosemanaanual, fecha ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`INSERT INTO asistencia_grupo ( id_grupo, id_usuario, numero_de_semana, fecha_asistencia ) VALUES ( ?, ?, ?, ?);`,[ id_grupo, id_alumno, numerosemanaanual, fecha ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};

catalogos.addAsistenciaInbi = ( id_grupo, id_alumno, numerosemanaanual, fecha ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`INSERT INTO asistencia_grupo ( id_grupo, id_usuario, numero_de_semana, fecha_asistencia ) VALUES ( ?, ?, ?, ?);`,[ id_grupo, id_alumno, numerosemanaanual, fecha ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};


catalogos.getAsistenciaAlumnoFast = ( idalumnos, id_ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM asistencia_grupo WHERE id_usuario IN ( ? ) AND id_grupo IN (SELECT g.id FROM grupos g LEFT JOIN ciclos c ON c.id = g.id_ciclo WHERE c.iderp = ?);`,[ idalumnos, id_ciclo ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};

catalogos.getAsistenciaAlumnoInbi = ( idalumnos, id_ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM asistencia_grupo WHERE id_usuario IN ( ? ) AND id_grupo IN (SELECT g.id FROM grupos g LEFT JOIN ciclos c ON c.id = g.id_ciclo WHERE c.iderp = ?);`,[ idalumnos, id_ciclo ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};

catalogos.ultimoAccesoFast = ( id_ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT u.usuario, CONCAT(u.nombre, ' ', u.apellido_paterno, ' ', IFNULL(u.apellido_materno,'')) AS alumno,
      g.id_plantel, g.nombre AS grupo, IFNULL(DATE_FORMAT(u.ultimo_movimiento, "%d - %M - %Y %H:%i "), 'SIN DATO') AS ultimo_movimiento
      FROM grupo_alumnos ga
      LEFT JOIN grupos g ON g.id = ga.id_grupo
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN usuarios u ON u.id = ga.id_alumno
      WHERE c.iderp = ?;`,[ id_ciclo ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};

catalogos.ultimoAccesoInbi = ( id_ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT u.usuario, CONCAT(u.nombre, ' ', u.apellido_paterno, ' ', IFNULL(u.apellido_materno,'')) AS alumno,
      g.id_plantel, g.nombre AS grupo, IFNULL(DATE_FORMAT(u.ultimo_movimiento, "%d - %M - %Y %H:%i "), 'SIN DATO') AS ultimo_movimiento
      FROM grupo_alumnos ga
      LEFT JOIN grupos g ON g.id = ga.id_grupo
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN usuarios u ON u.id = ga.id_alumno
      WHERE c.iderp = ?;`,[ id_ciclo ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};


catalogos.addCofigGrupo = ( id_grupo, id_curso, id_ciclo, id_nivel, sesion, dia, fecha_inicio, incremento, escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI 
  return new Promise((resolve,reject)=>{
    db.query(`INSERT INTO grupo_fechas ( idgrupo, idcurso, idciclo, idnivel, numerosemanaincremental, numerosemanaanual, sesion, fecha, dia ) VALUES (?,?,?,?,?,?,?,DATE_ADD(?, INTERVAL ? DAY ),?)`,
      [ id_grupo, id_curso, id_ciclo, id_nivel, 0, 0, sesion, fecha_inicio, incremento, dia ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve({ id_horario: res.insertId, id_grupo, id_curso, id_ciclo, id_nivel, sesion, dia, fecha_inicio, incremento, escuela })
    })
  });
};

catalogos.existeConfiguracionGrupo = ( idgrupo, escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI 
  return new Promise((resolve,reject)=>{
    db.query(`SELECT * FROM grupo_fechas WHERE idgrupo = ?;`,[ idgrupo ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  });
};


catalogos.eliminarConfiguracionGrupo = ( idgrupo, escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI 
  return new Promise((resolve,reject)=>{
    db.query("DELETE FROM grupo_fechas WHERE idgrupo_fechas > 0 AND idgrupo = ?", [ idgrupo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        // not found Customer with the id
        return reject({ message: "not_found" });
      }

      resolve(res)
    });
  });
};


catalogos.gruposCiclo66 = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM grupos WHERE id_ciclo = 77 AND id_curso > 1;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};


catalogos.getCursosLMS = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT cu.*, f.lunes, f.martes, f.miercoles, f.jueves, f.viernes, f.sabado, f.domingo 
      FROM cursos cu
      LEFT JOIN frecuencia f ON f.id = cu.id_frecuencia ;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};

catalogos.updateCursoGrupo = ( id_grupo, id_curso, escuela) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`UPDATE grupos SET id_curso = ?, editado = 1 WHERE id = ?`, [ id_curso, id_grupo],
      (err, res) => {
        if (err) { reject(err); return; }
        if (res.affectedRows == 0) {
          reject(err)
          return;
        }
        resolve(res);
    })
  });
};

catalogos.habilitarGroupByEscuela = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));`,(err, res) => {
      if(err){
        return reject({message:err.sqlMessage})
      }
      resolve(res)
    });
  });
};


catalogos.asistenciasRepetidas = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT * FROM asistencia_grupo WHERE id IN(
        SELECT id FROM asistencia_grupo GROUP BY id_grupo, id_usuario, fecha_asistencia HAVING COUNT(*) > 1);`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};

catalogos.deleteAsistenciaRepetida = ( escuela, id ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`DELETE FROM asistencia_grupo WHERE id  = ?;`,[ id ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.calificacionesRepet = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT * FROM control_grupos_calificaciones_evaluaciones 
      GROUP BY id_alumno, id_grupo, id_evaluacion
      HAVING count(*) > 1 AND deleted = 0;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};

catalogos.deleteCalifRepet = ( escuela, id ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`DELETE FROM control_grupos_calificaciones_evaluaciones WHERE id  = ?;`,[ id ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.alumnosRepet = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT * FROM grupo_alumnos WHERE id IN(
      SELECT id FROM grupo_alumnos GROUP BY id_grupo, id_alumno HAVING COUNT(*) > 1);`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};

catalogos.deleteAlumnoRepet = ( escuela, id ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`DELETE FROM grupo_alumnos WHERE id  = ?;`,[ id ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.datosRepetAlum = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT * FROM datos_alumno WHERE id IN(
        SELECT id FROM datos_alumno GROUP BY id_usuario HAVING COUNT(*) > 1);`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};

catalogos.deleteDatosRepet = ( escuela, id ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`DELETE FROM datos_alumno WHERE id  = ?;`,[ id ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};

catalogos.datosAvatares = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT * FROM avatar WHERE idavatar IN(
        SELECT idavatar FROM avatar GROUP BY id_alumno HAVING COUNT(*) > 1);`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};

catalogos.deleteAvatares = ( escuela, id ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`DELETE FROM avatar WHERE idavatar  = ?;`,[ id ], (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  });
};


catalogos.getCiclosCron = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT id_ciclo, ciclo, IF(LOCATE('FE', ciclo)>1,2,1) AS escuela FROM ciclos WHERE DATE_ADD(fecha_inicio_ciclo, interval 30 day ) >= CURRENT_DATE
      AND ciclo NOT LIKE '%CAMBIOS%'
      AND ciclo NOT LIKE '%INDUCCION%'
      AND ciclo NOT LIKE '%INVER%'
      AND ciclo NOT LIKE '%CAPACI%'
      AND activo_sn LIMIT 4;`, (err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};

/****************************************************/
/****************************************************/
catalogos.getGruposERP = ( id_ciclo, ciclo, id_ciclo_nuevo ) => {
  console.log( ciclo )
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT g.id_grupo, g.id_plantel, g.id_curso, ${ id_ciclo_nuevo } AS id_ciclo, g.id_horario, g.id_salon, n2.id_nivel, 
      g.cupo, c.precio_nivel AS precio, 0 AS inscritos, g.cupo AS disponibles, '' AS comentarios, NOW() AS fecha_alta, 
      g.grupo AS grupo_viejo, 1 AS actio_sn, 334 AS id_usuario_ultimo_cambio,
      NOW() AS fecha_ultimo_cambio , n.id_nivel_consecutivo, g.cupo AS disponibles,
      CONCAT(p.plantel, ' - ', '${ciclo}', ' - ', h.horario, ' - ', n2.nivel) AS grupo, 
      n2.nivel, h.horario, p.plantel, c.precio_nivel, IF(g.id_grupo_consecutivo > 0, 1, 0) AS estatus FROM grupos g
      LEFT JOIN niveles n ON n.id_nivel = g.id_nivel 
      LEFT JOIN niveles n2 ON n2.id_nivel = n.id_nivel_consecutivo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN horarios h ON h.id_horario = g.id_horario
      LEFT JOIN cursos c ON c.id_curso = g.id_curso
      WHERE g.id_ciclo = ?
      AND IF(LOCATE('CERTI', grupo) > 0, 1, 0) = 0;`,[ id_ciclo ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  });
};



module.exports = catalogos;