// const sqlERP      = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");

const sqlINBI = require("../dbINBI.js");
const sqlFAST = require("../dbFAST.js");
// constructor
const Prospectos = (data) => {};

Prospectos.getExisteCicloFast = ( cicloFast ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM ciclos WHERE iderp = ${ cicloFast }`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      if(res.length > 0){
      	resolve(res[0])
      }else{
      	resolve( false )
      }
    })
  });
};


Prospectos.getExisteCicloInbi = ( cicloInbi ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM ciclos WHERE iderp = ${ cicloInbi }`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      if(res.length > 0){
        resolve(res[0])
      }else{
        resolve( false )
      }
    })
  });
};

Prospectos.getcicloERP = ( ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM ciclos WHERE id_ciclo = ${ ciclo }`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      if(res.length > 0){
      	resolve(res[0])
      }else{
      	resolve( false )
      }
    })
  });
};

Prospectos.getAlumnosFASTERP = ( fecha ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT ll.* FROM llamadas ll 
      LEFT JOIN ciclos c ON c.id_ciclo = ll.id_ciclo_interes WHERE 
      ll.fecha_hora_alta BETWEEN CONCAT(?, " 00:00:00") AND CONCAT(?, " 23:59:59")
      AND c.ciclo LIKE '%FE%';`,[ fecha, fecha ], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  });
};

Prospectos.getAlumnosInbiERP = ( fecha ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT ll.* FROM llamadas ll 
      LEFT JOIN ciclos c ON c.id_ciclo = ll.id_ciclo_interes WHERE 
      ll.fecha_hora_alta BETWEEN CONCAT(?, " 00:00:00") AND CONCAT(?, " 23:59:59")
      AND c.ciclo NOT LIKE '%FE%';`,[ fecha, fecha ], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  });
};

Prospectos.getAlumnoFAST = ( usuario ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM usuarios WHERE usuario = ? `,[ usuario ], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      if(res.length > 0){
        resolve(res[0])
      }else{
        resolve( false )
      }
    })
  });
};

Prospectos.getAlumnoINBI = ( usuario ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM usuarios WHERE usuario = ? `,[ usuario ], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      if(res.length > 0){
        resolve(res[0])
      }else{
        resolve( false )
      }
    })
  });
};


Prospectos.addCicloFast = (c) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`INSERT INTO ciclos ( nombre , estatus , fecha_inicio , fecha_fin , fecha_inicio_excluir , fecha_fin_excluir ,comentarios , usuario_creo , usuario_modifico , deleted, iderp) VALUES (?,?,?,?,?,?,?,?,?,?,?)`,
      [ c.nombre , c.estatus , c.fecha_inicio , c.fecha_fin , c.fecha_inicio_excluir , c.fecha_fin_excluir ,c.comentarios , c.usuario_creo , c.usuario_modifico , c.deleted, c.iderp ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve({ id_ciclo: res.insertId, ...c })
    })
  });
};


Prospectos.addCicloInbi = (c) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`INSERT INTO ciclos ( nombre , estatus , fecha_inicio , fecha_fin , fecha_inicio_excluir , fecha_fin_excluir ,comentarios , usuario_creo , usuario_modifico , deleted, iderp) VALUES (?,?,?,?,?,?,?,?,?,?,?)`,
      [ c.nombre , c.estatus , c.fecha_inicio , c.fecha_fin , c.fecha_inicio_excluir , c.fecha_fin_excluir ,c.comentarios , c.usuario_creo , c.usuario_modifico , c.deleted, c.iderp ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve({ id_ciclo: res.insertId, ...c })
    })
  });
};


Prospectos.getAlumnoGrupo = ( id_alumno, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM grupo_alumnos WHERE id_grupo = ? AND id_alumno = ? ;`, [ id_alumno, id_grupo ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      if(res.length > 0){
        resolve(res[0])
      }else{
        resolve( false )
      }
    })
  });
};

Prospectos.getAlumnoGrupoInbi = ( id_alumno, id_grupo ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM grupo_alumnos WHERE id_grupo = ? AND id_alumno = ? ;`, [ id_alumno, id_grupo ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      if(res.length > 0){
        resolve(res[0])
      }else{
        resolve( false )
      }
    })
  });
};

Prospectos.getGrupoFast = ( ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM grupos WHERE id_ciclo = ${ ciclo } AND nombre LIKE '%indu%';`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      if(res.length > 0){
      	resolve(res[0])
      }else{
      	resolve( false )
      }
    })
  });
};

Prospectos.getGrupoInbi = ( ciclo ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM grupos WHERE id_ciclo = ${ ciclo } AND nombre LIKE '%indu%';`, (err, res) => {
      if(err){
        reject(err);
        return;
      }
      if(res.length > 0){
        resolve(res[0])
      }else{
        resolve( false )
      }
    })
  });
};

Prospectos.addGrupoFast = (g) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`INSERT INTO grupos (nombre, estatus, id_unidad_negocio, id_plantel, id_ciclo, id_curso, id_horario, id_salon,id_nivel,codigo_acceso,cupo,precio,inscritos,disponibles, usuario_creo, usuario_modifico,url_imagen, deleted, iderp, editado,  optimizado, id_zona) 
    	VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
    	[ g.nombre, g.estatus, g.id_unidad_negocio, g.id_plantel, g.id_ciclo, g.id_curso, g.id_horario, g.id_salon,g.id_nivel,g.codigo_acceso,g.cupo,g.precio,g.inscritos,g.disponibles, g.usuario_creo, g.usuario_modifico,g.url_imagen, g.deleted, g.iderp, g.editado,  g.optimizado, g.id_zona ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve({ id_grupo: res.insertId, ...g })
    })
  });
};

Prospectos.addGrupoInbi = (g) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`INSERT INTO grupos (nombre, estatus, id_unidad_negocio, id_plantel, id_ciclo, id_curso, id_horario, id_salon,id_nivel,codigo_acceso,cupo,precio,inscritos,disponibles, usuario_creo, usuario_modifico,url_imagen, deleted, iderp, editado,  optimizado, id_zona) 
      VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
      [ g.nombre, g.estatus, g.id_unidad_negocio, g.id_plantel, g.id_ciclo, g.id_curso, g.id_horario, g.id_salon,g.id_nivel,g.codigo_acceso,g.cupo,g.precio,g.inscritos,g.disponibles, g.usuario_creo, g.usuario_modifico,g.url_imagen, g.deleted, g.iderp, g.editado,  g.optimizado, g.id_zona ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve({ id_grupo: res.insertId, ...g })
    })
  });
};


Prospectos.addUsuarioFast = (u) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`INSERT INTO usuarios (nombre,apellido_paterno,apellido_materno, usuario, password, email, estado, telefono, movil, id_direccion, roll, id_tipo_usuario, deleted, iderp, contra_universal, id_plantel) 
      VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
      [ u.nombre,u.apellido_paterno,u.apellido_materno, u.usuario, u.password, u.email, u.estado, u.telefono, u.movil, u.id_direccion, u.roll, u.id_tipo_usuario, u.deleted, u.iderp, u.contra_universal, u.id_plantel ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve({ id_usuario: res.insertId, ...u })
    })
  });
};

Prospectos.addUsuarioInbi = (u) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`INSERT INTO usuarios (nombre,apellido_paterno,apellido_materno, usuario, password, email, estado, telefono, movil, id_direccion, roll, id_tipo_usuario, deleted, iderp, contra_universal, id_plantel) 
      VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
      [ u.nombre,u.apellido_paterno,u.apellido_materno, u.usuario, u.password, u.email, u.estado, u.telefono, u.movil, u.id_direccion, u.roll, u.id_tipo_usuario, u.deleted, u.iderp, u.contra_universal, u.id_plantel ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve({ id_usuario: res.insertId, ...u })
    })
  });
};


Prospectos.addAlumnoFast = (a) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`INSERT INTO alumnos (id_usuario,matricula,id_plantel,id_unidad_negocio,comentarios) 
      VALUES(?,?,?,?,?)`,
      [ a.id_usuario,a.matricula,a.id_plantel,a.id_unidad_negocio,a.comentarios ], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve({ id: res.insertId, ...a })
    })
  });
};

Prospectos.addAlumnoInbi = (a) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`INSERT INTO alumnos (id_usuario,matricula,id_plantel,id_unidad_negocio,comentarios) 
      VALUES(?,?,?,?,?)`,
      [ a.id_usuario,a.matricula,a.id_plantel,a.id_unidad_negocio,a.comentarios ], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve({ id: res.insertId, ...a })
    })
  });
};


Prospectos.addGrupoAlumnoFast = (a) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`INSERT INTO grupo_alumnos (id_grupo,id_curso,id_alumno,deleted,estatus) 
      VALUES(?,?,?,?,?)`,
      [ a.id_grupo,a.id_curso,a.id_alumno,a.deleted,a.estatus ], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve({ id: res.insertId, ...a })
    })
  });
};

Prospectos.addGrupoAlumnoInbi = (a) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`INSERT INTO grupo_alumnos (id_grupo,id_curso,id_alumno,deleted,estatus) 
      VALUES(?,?,?,?,?)`,
      [ a.id_grupo,a.id_curso,a.id_alumno,a.deleted,a.estatus ], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve({ id: res.insertId, ...a })
    })
  });
};


Prospectos.getNivelesTeacherInbi = ( idcicloInbi, email, data ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT DISTINCT g.id_nivel
      FROM grupo_teachers gt
      LEFT JOIN grupos g ON g.id = gt.id_grupo
      LEFT JOIN usuarios u ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
      LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
      WHERE ci.iderp = ? AND g.deleted = 0 AND g.editado = 1 AND u.email = ?
      OR ci.iderp = ? AND g.deleted = 0 AND g.editado = 1 AND u2.email = ?`,[ idcicloInbi, email, idcicloInbi, email ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  });
};



Prospectos.getEvaluacionesCalificiacionInbi = ( idcicloFast, idcicloInbi, id_usuario ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM calificacion_evaluacion_leccion WHERE id_teacher = ? AND id_ciclo = ?`,[ id_usuario, idcicloInbi ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  });
};



module.exports = Prospectos;