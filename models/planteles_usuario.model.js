const sql = require("./db.js");

//const constructor
const Planteles_usuario = function(planteles_usuario) {
    this.idplanteles_usuario = planteles_usuario.idplanteles_usuario;
    this.idplantel = planteles_usuario.idplantel;
    this.idusuario = planteles_usuario.idusuario;
    this.usuario_registro = planteles_usuario.usuario_registro;
    this.fecha_creacion = planteles_usuario.fecha_creacion;
    this.fecha_actualizo = planteles_usuario.fecha_actualizo;
    this.deleted = planteles_usuario.deleted;
};

Planteles_usuario.all_planteles_usuario = result => {
  sql.query(`SELECT * FROM planteles_usuario`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};


Planteles_usuario.getPlantelesUsuarioId = (id,result) => {
  sql.query(`SELECT * FROM planteles_usuario WHERE idusuario = ?`,[id], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Planteles_usuario.add_planteles_usuario = (pu, result) => {
  sql.query(`INSERT INTO planteles_usuario(idusuario,idplantel,usuario_registro,fecha_creacion,fecha_actualizo,prioridad)VALUES(?,?,?,?,?,?)`, 
    [pu.idusuario, pu.idplantel, pu.usuario_registro, pu.fecha_creacion, pu.fecha_actualizo, pu.prioridad],(err, res) => {
      if (err) { result(err, null); return; }
      result(null, { id: res.insertId, ...pu });
  });
};

Planteles_usuario.update_planteles_usuario = (id, pu, result) => {
  sql.query(` UPDATE planteles_usuario SET idusuario=?, idplantel=?,usuario_registro=?,fecha_creacion=?,fecha_actualizo=?, prioridad =?
              WHERE idplanteles_usuarios = ?`, [pu.idusuario, pu.idplantel, pu.usuario_registro, pu.fecha_creacion, pu.fecha_actualizo,pu.prioridad, id],
    (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id, ...pu });
    }
  );
};


Planteles_usuario.delete_planteles_usuario = (id, result) => {
  sql.query("DELETE FROM planteles_usuario WHERE idplanteles_usuarios = ?", id, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }
    result(null, res);
  });
};


Planteles_usuario.plantelesVendedora = ( ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT p.idplantel, p.plantel, p.acronimo, pu.idusuario, u.iderp, p.escuela FROM planteles_usuario pu
      LEFT JOIN plantel p ON p.idplantel = pu.idplantel
      LEFT JOIN usuarios u ON u.idusuario = pu.idusuario 
      WHERE pu.deleted = 0 AND u.idpuesto = 18 AND u.deleted = 0;`, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};


module.exports = Planteles_usuario;