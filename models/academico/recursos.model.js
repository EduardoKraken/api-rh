const { result } = require("lodash");
const sqlERP = require("../db3.js");
const sqlINBI = require("../dbINBI.js");
const sqlFAST = require("../dbFAST.js");
const sqlERPNUEVO = require("../db.js");


//const constructor
const recursos = function () { };


recursos.getCursosAlumnos = (escuela) => {
    const db = escuela == 2 ? sqlFAST : sqlINBI
    return new Promise((resolve, reject) => {
      db.query(`SELECT id, nombre FROM cursos WHERE deleted = 0 ORDER BY nombre;`, (err, res) => {
        if (err) { return reject({ message: err.sqlMessage }) }
        resolve(res)
      })
    })
};


recursos.getRecursosAlumnos = (escuela) => {
    const db = escuela == 2 ? sqlFAST : sqlINBI
    return new Promise((resolve, reject) => {
      db.query(`SELECT * FROM recursos;`,(err, res) => {
        if (err) { return reject({ message: err.sqlMessage }) }
        resolve(res)
      })
    })
};


recursos.getPreguntasAlumnos = (escuela, idEvaluacion) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve, reject) => {
    db.query(`SELECT * FROM preguntas WHERE idEvaluacion = ?;`,[idEvaluacion],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
};


recursos.getRespuestasAlumnos = (escuela, idEvaluacion) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve, reject) => {
    db.query(`SELECT * FROM pregunta_opcion_multiple WHERE idEvaluacion = ? ;`,[idEvaluacion],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
};


recursos.getRespuestasAlumnosPORpregunta = (escuela, idPregunta) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve, reject) => {
    db.query(`SELECT * FROM pregunta_opcion_multiple WHERE idPregunta = ? ;`,[idPregunta],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
};


module.exports = recursos;

//ANGEL RODRIGUEZ -- TODO