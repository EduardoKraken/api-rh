const sqlERP       = require("../db3.js");
const sqlERPNUEVO  = require("../db.js");
const sqlINBI      = require("../dbINBI.js");
const sqlFAST      = require("../dbFAST.js");
// constructor
const certificacion = (data) => {
    this.idcertificacion    = data.idcertificacion;
    this.codigo             = data.codigo;
    this.login      				= data.login;
    this.password   				= data.password;
    this.nombre     				= data.nombre;
    this.status     				= data.status;
    this.id_alumno  				= data.id_alumno;
};

certificacion.getCodigosFast = (data) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT c.*, CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")) AS alumno, 2 escuela, u.iderp FROM certificacion c
			LEFT JOIN usuarios u ON u.id = c.id_alumno ORDER BY alumno;`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};

certificacion.getCodigosInbi = (data) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT c.*, CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")) AS alumno, 1 As escuela, u.iderp FROM certificacion c
			LEFT JOIN usuarios u ON u.id = c.id_alumno ORDER BY alumno;`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};

certificacion.gruposAlumnosPlantel = ( idAlumnos ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT ga.id_alumno, p.plantel, ga.fecha_alta FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        WHERE g.grupo NOT LIKE '%CERTI%' AND g.id_nivel IN (40,41,42,51) AND ga.id_alumno IN ( ? )
        ORDER BY ga.fecha_alta DESC;`,[ idAlumnos ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};


certificacion.getAlumnosCertiFast = (data) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT u.id AS id_alumno, CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")) AS nombre_completo, u.id_plantel FROM usuarios u
			LEFT JOIN grupo_alumnos ga ON ga.id_alumno = u.id
			LEFT JOIN grupos g ON g.id = ga.id_grupo
			WHERE g.deleted = 0 AND g.nombre LIKE '%certi%' ORDER BY nombre_completo; `,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};

certificacion.getAlumnosCertiInbi = (data) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT u.id AS id_alumno, CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")) AS nombre_completo, u.id_plantel FROM usuarios u
			LEFT JOIN grupo_alumnos ga ON ga.id_alumno = u.id
			LEFT JOIN grupos g ON g.id = ga.id_grupo
			WHERE g.deleted = 0 AND g.nombre LIKE '%certi%' ORDER BY nombre_completo;`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};

certificacion.getCodigoFast = (codigo) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM certificacion WHERE idcertificacion = ? `,[ codigo ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};

certificacion.updateCodigoFast = (codigo, id_alumno, nombre) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`UPDATE certificacion SET codigo = ?, id_alumno  = ?, nombre = ? WHERE idcertificacion > 0 AND codigo = ?`,[ codigo, id_alumno, nombre, codigo ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};


certificacion.addCodigoFast = (codigo, id_alumno, nombre) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`INSERT INTO certificacion(codigo, nombre, id_alumno)VALUES(?,?,?)`,[ codigo, nombre, id_alumno ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};


certificacion.getCodigoInbi = (codigo) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM certificacion WHERE idcertificacion = ? `,[ codigo ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};


certificacion.getCertificadoCambridgeFast = ( idusuario ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT *, (ELT(estatus+1, 'EN PROCESO', 'GENERANDO DIPLOMA', 'CREADO','SOLICITUD EN PROCESO','IMPRESO','ENVIADO', 'EN SUCURSAL','ENTREGADO' )) AS nombre_estatus FROM certificacion WHERE id_alumno = ?;`,[ idusuario ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};

certificacion.getCertificadoCambridgeInbi = ( idusuario ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT *, (ELT(estatus+1, 'EN PROCESO', 'GENERANDO DIPLOMA', 'CREADO','SOLICITUD EN PROCESO','IMPRESO','ENVIADO', 'EN SUCURSAL','ENTREGADO' )) AS nombre_estatus FROM certificacion WHERE id_alumno = ?;`,[ idusuario ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};

certificacion.updateCodigoInbi = (codigo, id_alumno, nombre) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`UPDATE certificacion SET codigo = ?, id_alumno  = ?, nombre = ? WHERE idcertificacion > 0 AND codigo = ?`,[ codigo, id_alumno, nombre, codigo ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};

certificacion.addCodigoInbi = (codigo, id_alumno, nombre) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`INSERT INTO certificacion(codigo,nombre, id_alumno)VALUES(?,?,?)`,[ codigo, nombre, id_alumno ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};

certificacion.updateCodigoNivelFast = (idcertificacion, nivel, status) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`UPDATE certificacion SET nivel = ?, status = ? WHERE idcertificacion = ?`,[ nivel, status, idcertificacion ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};

certificacion.updateCodigoNivelInbi = (idcertificacion, nivel, status) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`UPDATE certificacion SET nivel = ?, status = ? WHERE idcertificacion = ?`,[ nivel, status, idcertificacion ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};


certificacion.updateCertificadoFast = (idcertificacion,  nombre, estatus) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`UPDATE certificacion SET nombre = ?, estatus = ? WHERE idcertificacion = ?`,[ nombre, estatus, idcertificacion ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};

certificacion.updateCertificadoInbi = (idcertificacion,  nombre, estatus) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`UPDATE certificacion SET nombre = ?, estatus = ? WHERE idcertificacion = ?`,[ nombre, estatus, idcertificacion ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};



certificacion.getAlumnosUltimoNivel = ( escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`SELECT ga.id_alumno, g.id, g.nombre, g.id_nivel,u.datos_actualizados, ${escuela} AS escuela,
      CONCAT(u.nombre, ' ', u.apellido_paterno, IFNULL(CONCAT(' ',u.apellido_materno),'')) AS alumno,
      CONCAT(d.nombre, ' ', d.apellido_paterno, IFNULL(CONCAT(' ',d.apellido_materno),'')) AS alumno2,
      u.nombre, u.apellido_paterno, u.apellido_materno, u.usuario AS matricula
      FROM grupo_alumnos ga 
      LEFT JOIN grupos g ON g.id = ga.id_grupo
      LEFT JOIN usuarios u ON u.id = ga.id_alumno
      LEFT JOIN datos_alumno d ON d.id_usuario = ga.id_alumno
      WHERE g.id_nivel = 14 AND u.datos_actualizados;`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};

certificacion.getExisteSolicitud = (iderp) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT *, (ELT(estatus+1, 'EN PROCESO', 'CREADO', 'ENVIADO', 'EN SUCURSAL','ENTREGADO' )) AS nombre_estatus FROM  certificado_fisico WHERE deleted = 0 AND estatus != 4 AND iderp = ${ iderp };`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};

certificacion.getExisteSolicitudAlumno = (iderp) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT *, (ELT(estatus+1, 'EN PROCESO', 'CREADO', 'ENVIADO', 'EN SUCURSAL','ENTREGADO' )) AS nombre_estatus FROM  certificado_fisico WHERE deleted = 0 AND iderp = ${ iderp } LIMIT 1;`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};


certificacion.getCertificadosActivos = () => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM  certificado_fisico WHERE deleted = 0 AND estatus != 4;`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};

certificacion.addSolicitudCertificado = (u) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`INSERT INTO certificado_fisico(iderp,nombre_completo, usuario_registro, estatus, url, unidad_negocio)VALUES(?,?,?,?,?,?)`,
    [u.iderp, u.nombre_completo, u.usuario_registro, u.estatus, u.url, u.unidad_negocio], (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

certificacion.getCertificadosSolicitados = result => {
  sqlERPNUEVO.query(`SELECT * FROM certificado_fisico WHERE deleted = 0;`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};




certificacion.getalumnosActivos = result => {
  sqlERP.query(`SELECT id_alumno, UPPER(CONCAT(nombre, " ", apellido_paterno, " ", IFNULL(apellido_materno,""))) AS nombre FROM alumnos WHERE activo_sn = 1 ORDER BY nombre;`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};


certificacion.updateSolicitudCertificado = (id, data, result) => {
  sqlERPNUEVO.query(`UPDATE certificado_fisico SET estatus = ?, nombre_completo = ?, deleted =? WHERE idcertificado_fisico = ?`, 
    [data.estatus, data.nombre_completo, data.deleted, id],
    (err, res) => {
      if (err) { result(err, null); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id, ...data });
  })
}

certificacion.updateSolicitudCertificado2 = (id) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`UPDATE certificado_fisico SET estatus = ?  WHERE idcertificado_fisico = ?`, 
    [ 1 , id],
    (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

certificacion.updateSolicitudCertificadoFoto = ( id, archivo ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`UPDATE certificado_fisico SET estatus = 4, url = ?, fecha_entrega = NOW() WHERE idcertificado_fisico = ?`, 
    [ archivo, id],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  });
};


certificacion.updateCertificadoCambridgeFAST = (idcertificacion) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`UPDATE certificacion SET estatus = 2  WHERE idcertificacion = ?`, [ idcertificacion ],
    (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

certificacion.updateCertificadoCambridgeINBI = (idcertificacion) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`UPDATE certificacion SET estatus = 2  WHERE idcertificacion = ?`, [ idcertificacion ],
    (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}


certificacion.updateCambridgeFotoFAST = (idcertificacion, foto) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`UPDATE certificacion SET estatus = 3, url = ?, fecha_creado = NOW()  WHERE idcertificacion = ?`, [ foto, idcertificacion ],
    (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

certificacion.updateCambridgeFotoINBI = (idcertificacion, foto) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`UPDATE certificacion SET estatus = 3, url = ?, fecha_creado = NOW()  WHERE idcertificacion = ?`, [ foto, idcertificacion ],
    (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}


certificacion.updateSolicitudCertificadoCambridgeFast = (data, result) => {
  sqlFAST.query(`UPDATE certificacion SET estatus = ? WHERE idcertificacion = ?`, 
    [data.estatus, data.idcertificacion],
    (err, res) => {
      if (err) { result(err, null); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, {...data });
  })
}

certificacion.updateSolicitudCertificadoCambridgeInbi = (data, result) => {
  sqlINBI.query(`UPDATE certificacion SET estatus = ? WHERE idcertificacion = ?`, 
    [data.estatus, data.idcertificacion],
    (err, res) => {
      if (err) { result(err, null); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, {...data });
  })
}

/************************************************************************/
/************************************************************************/
certificacion.getAlumnoMatriculaFast = ( matricula ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT * FROM usuarios WHERE usuario = ?`, [ matricula ],
    (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res[0])
    })
  })
}

certificacion.getAlumnoMatriculaInbi = ( matricula ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT * FROM usuarios WHERE usuario = ?`, [ matricula ],
    (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res[0])
    })
  })
}

/*   ACTUALIZACION DE DATOS  */

certificacion.updateAlumnoMatriculaFast = ( u ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`UPDATE usuarios SET nombre = ?, apellido_paterno = ?, apellido_materno = ?, datos_actualizados = ?  WHERE id = ?`, 
    [ u.nombre, u.apellido_paterno, u.apellido_materno, u.datos_actualizados, u.id ],
    (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

certificacion.updateAlumnoMatriculaInbi = ( u ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`UPDATE usuarios SET nombre = ?, apellido_paterno = ?, apellido_materno = ?, datos_actualizados = ?  WHERE id = ?`, 
    [ u.nombre, u.apellido_paterno, u.apellido_materno, u.datos_actualizados, u.id ],
    (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

/* VERIFICAR QUE EXISTA UNA CERTIFICACION ACTIVA Y CON DATOS ACTUALIZADOS */
certificacion.verificarExisteCertificacionFast = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT c.*, CONCAT(u.nombre, ' ', u.apellido_paterno, ' ', IFNULL(u.apellido_materno,'')) AS nombre_completo,
    (ELT(estatus+1, 'EN PROCESO', 'GENERANDO DIPLOMA', 'CREADO','SOLICITUD EN PROCESO','IMPRESO','ENVIADO', 'EN SUCURSAL','ENTREGADO' )) AS nombre_estatus FROM certificacion c
    LEFT JOIN usuarios u ON u.id = c.id_alumno 
    WHERE u.id =  ? AND c.status <> ''`, [ id ],
    (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

certificacion.verificarExisteCertificacionInbi = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT c.*, CONCAT(u.nombre, ' ', u.apellido_paterno, ' ', IFNULL(u.apellido_materno,'')) AS nombre_completo,
    (ELT(estatus+1, 'EN PROCESO', 'GENERANDO DIPLOMA', 'CREADO','SOLICITUD EN PROCESO','IMPRESO','ENVIADO', 'EN SUCURSAL','ENTREGADO' )) AS nombre_estatus FROM certificacion c
    LEFT JOIN usuarios u ON u.id = c.id_alumno 
    WHERE u.id =  ? AND c.status <> ''`, [ id ],
    (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}


/* VERIFICAR QUE EXISTA UN NIVEL 14 Pasadoo */
certificacion.verificarCatorceNivelesFast = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT c.*, g.id_plantel FROM cardex_curso c
        LEFT JOIN grupos g ON g.id = c.id_grupo  WHERE id_alumno = ${ id } AND nivel = 14 AND calificacion_final_primera_oportunidad >= 70 OR
        id_alumno = ${ id } AND nivel = 14 AND calificacion_final_segunda_oportunidad >= 70 ;`, (err, res) => {
      if(err){
        reject(null);
        return;
      }
      resolve(res[0])
    })
  })
}

certificacion.verificarCatorceNivelesInbi = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT c.*, g.id_plantel FROM cardex_curso c
        LEFT JOIN grupos g ON g.id = c.id_grupo  WHERE id_alumno = ${ id } AND nivel = 14 AND calificacion_final_primera_oportunidad >= 70 AND c.id_curso IN (2,3,4,7,8,9) OR
        id_alumno = ${ id } AND nivel = 14 AND calificacion_final_segunda_oportunidad >= 70 AND c.id_curso IN (2,3,4,7,8,9) OR
        id_alumno = ${ id } AND nivel = 16 AND calificacion_final_primera_oportunidad >= 70 AND c.id_curso IN (10,11,12,13) OR
        id_alumno = ${ id } AND nivel = 16 AND calificacion_final_segunda_oportunidad >= 70 AND c.id_curso IN (10,11,12,13);`, (err, res) => {
      if(err){
        reject(null);
        return;
      }
      resolve(res[0])
    })
  })
}



module.exports = certificacion;

