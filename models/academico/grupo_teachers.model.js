// const sqlERP   = require("../db3.js");
const sqlERP      = require("../dbErpPrueba.js");

// constructor
const grupoTeachers = (data) => {
    this.id_grupo         = data.id_grupo;
    this.grupo            = data.grupo;
    this.teacher1         = data.teacher1;
    this.teacher2         = data.teacher2;
    this.nombre_teacher1  = data.nombre_teacher1;
    this.nombre_teacher2  = data.nombre_teacher2;
};

grupoTeachers.getGrupoTeachers = ( idciclo, idciclo2, result ) => {
    sqlERP.query(`SELECT id_grupo, grupo, IFNULL((SELECT id_maestro FROM maestrosgrupo WHERE activo_sn = 1 AND g.id_grupo = id_grupo LIMIT 1),"") AS teacher1, IFNULL(t.nombre_completo,"") AS nombre_teacher1,
					IFNULL((SELECT id_maestro FROM maestrosgrupo WHERE activo_sn = 1 AND g.id_grupo = id_grupo AND numero_teacher = 2 LIMIT 1),"") AS teacher2, IFNULL(t2.nombre_completo,"") AS nombre_teacher2,
          n.nivel, IF(LOCATE('FAST',g.grupo)>0,2,1) AS escuela FROM grupos g
          LEFT JOIN niveles n ON n.id_nivel = g.id_nivel 
					LEFT JOIN fyn_cat_trabajadores t ON t.id = (SELECT id_maestro FROM maestrosgrupo WHERE activo_sn = 1 AND g.id_grupo = id_grupo LIMIT 1)
					LEFT JOIN fyn_cat_trabajadores t2 ON t2.id = (SELECT id_maestro FROM maestrosgrupo WHERE activo_sn = 1 AND g.id_grupo = id_grupo AND numero_teacher = 2 LIMIT 1)
					WHERE g.id_ciclo IN ( ?, ?) AND g.activo_sn = 1;`, [ idciclo, idciclo2 ],(err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      result(null, res);
    })
};


grupoTeachers.getTeachersActivos = ( result ) => {
  sqlERP.query(`SELECT u.id_usuario, u.nombre_completo, t.id AS id_trabajador, p.id_perfil FROM fyn_cat_trabajadores t 
		LEFT JOIN usuarios u ON u.id_trabajador = t.id
		LEFT JOIN perfilesusuario pu ON pu.id_usuario = u.id_usuario
		LEFT JOIN perfiles p ON p.id_perfil = pu.id_perfil WHERE p.id_perfil = 9 AND u.activo_sn = 1 ORDER BY nombre_completo;`,(err, res) => {
    if (err) {
      result(err, null);
      return;
    }
    result(null, res);
  })
};

grupoTeachers.getTeacherGrupo = (data) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM maestrosgrupo WHERE id_grupo = ? AND numero_teacher = ?;`,[data.id_grupo, data.numero_teacher], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

grupoTeachers.updateTeacherGrupo = (data) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE maestrosgrupo SET id_maestro = ? WHERE id_maestro_grupo > 0 AND id_grupo = ? AND numero_teacher = ?;`,[data.id_maestro, data.id_grupo, data.numero_teacher], (err, res) => {
      if (err) { reject(err); return; }
      if (res.affectedRows == 0) {
        reject({ kind: "not_found" });
        return;
      }
      resolve({ id: data.id_grupo, ...data });
    });
  });
};

grupoTeachers.addTeacherGrupo = (data) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO maestrosgrupo(id_maestro,id_grupo,activo_sn,numero_teacher,id_usuario_ultimo_cambio) VALUES(?,?,?,?,17);`, [data.id_maestro, data.id_grupo, data.activo_sn, data.numero_teacher],(err, res) => {
      if (err) { 
      	reject(err); 
      	return; 
      }
      resolve({ id: res.insertId, ...data });
    })
  });
};

module.exports = grupoTeachers;