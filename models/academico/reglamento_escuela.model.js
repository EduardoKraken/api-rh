const { result }  = require("lodash");
const sqlERP      = require("../db3.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");
const sqlERPNUEVO = require("../db.js");


//const constructor
const reglamento_escuela = function() {};

reglamento_escuela.getReglas = ( ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT idreglamento, regla, idcurso FROM reglamento WHERE deleted != 1;`,(err, res) => {
      if(err){
        return reject({message: err.sqlMessage })  
      }
      resolve(res)
    })
  })
};


reglamento_escuela.addReglas = (u, result) => {
    sqlINBI.query(`INSERT INTO reglamento(regla, idcurso) VALUES (?, ?);`,[u.regla,u.idcurso],
    (err, res) => {
      if (err) { 
          result(err, null); 
          return; 
      }
      result(null, { id: res.insertId, ...u });
    })
};


reglamento_escuela.updateReglas = (id, u, result) => {
    sqlINBI.query(`UPDATE reglamento SET regla=?, idcurso=? ,deleted=? WHERE idreglamento=?`, [ u.regla, u.idcurso, u.deleted, id],
    (err, res) => {
      if (err) { result(err,null); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id, ...u });
    })
  }

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

module.exports = reglamento_escuela;

//ANGEL RODRIGUEZ -- TODO