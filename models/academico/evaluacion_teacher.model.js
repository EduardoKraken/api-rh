const sqlERP  = require("../db3.js");
const sqlINBI = require("../dbINBI.js");
const sqlFAST = require("../dbFAST.js");
// constructor
const evaluacion_teacher = (data) => {
    this.idcertificacion    = data.idcertificacion;
    this.codigo             = data.codigo;
    this.login      				= data.login;
    this.password   				= data.password;
    this.nombre     				= data.nombre;
    this.status     				= data.status;
    this.id_alumno  				= data.id_alumno;
};

evaluacion_teacher.getUsuarioTeacherIderp = ( id_usuario, data ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM usuarios WHERE id_usuario = ?;`,[ id_usuario ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res[0])
    })
  });
};

evaluacion_teacher.getClasesTeacherFast = ( idcicloFast, email, data ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.nombre AS grupo, g.id_curso, g.id_unidad_negocio, g.id_nivel, c.nombre AS curso, c.total_dias_laborales, f.lunes, f.martes, f.miercoles, f.jueves, f.viernes, f.sabado, f.domingo, c.diasdeFrecuencia,
      u.id AS id_teacher, CONCAT(u.nombre," ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher1,
      u2.id AS id_teacher_2, CONCAT(u2.nombre," ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno,"")) AS teacher2, u.email AS email1, u2.email AS email2
       FROM grupo_teachers gt
      LEFT JOIN grupos g ON g.id = gt.id_grupo
      LEFT JOIN cursos c ON c.id = g.id_curso
      LEFT JOIN usuarios u ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
      LEFT JOIN frecuencia f ON f.id = c.id_frecuencia 
      LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
      WHERE ci.iderp = ? AND g.deleted = 0 AND g.editado = 1 AND u.email = ? AND g.optimizado = 0
      OR ci.iderp = ? AND g.deleted = 0 AND g.editado = 1 AND u2.email = ? AND g.optimizado = 0
      ORDER BY curso;`,[ idcicloFast, email, idcicloFast, email ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  });
};

evaluacion_teacher.getClasesTeacherInbi = ( idcicloInbi, email, data ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.nombre AS grupo, g.id_curso, g.id_unidad_negocio, g.id_nivel, c.nombre AS curso, c.total_dias_laborales, f.lunes, f.martes, f.miercoles, f.jueves, f.viernes, f.sabado, f.domingo, c.diasdeFrecuencia,
      u.id AS id_teacher, CONCAT(u.nombre," ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher1,
      u2.id AS id_teacher_2, CONCAT(u2.nombre," ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno,"")) AS teacher2, u.email AS email1, u2.email AS email2
       FROM grupo_teachers gt
      LEFT JOIN grupos g ON g.id = gt.id_grupo
      LEFT JOIN cursos c ON c.id = g.id_curso
      LEFT JOIN usuarios u ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
      LEFT JOIN frecuencia f ON f.id = c.id_frecuencia 
      LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
      WHERE ci.iderp = ? AND g.deleted = 0 AND g.editado = 1 AND u.email = ? AND g.optimizado = 0
      OR ci.iderp = ? AND g.deleted = 0 AND g.editado = 1 AND u2.email = ? AND g.optimizado = 0
      ORDER BY curso;`,[ idcicloInbi, email, idcicloInbi, email ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  });
};

evaluacion_teacher.getNivelesTeacherFast = ( idcicloFast, email, data ) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT DISTINCT g.id_nivel
      FROM grupo_teachers gt
      LEFT JOIN grupos g ON g.id = gt.id_grupo
      LEFT JOIN usuarios u ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
      LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
      WHERE ci.iderp = ? AND g.deleted = 0 AND g.editado = 1 AND u.email = ?
      OR ci.iderp = ? AND g.deleted = 0 AND g.editado = 1 AND u2.email = ?`,[ idcicloFast, email, idcicloFast, email ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  });
};

evaluacion_teacher.getNivelesTeacherInbi = ( idcicloInbi, email, data ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT DISTINCT g.id_nivel
      FROM grupo_teachers gt
      LEFT JOIN grupos g ON g.id = gt.id_grupo
      LEFT JOIN usuarios u ON u.id = gt.id_teacher
      LEFT JOIN usuarios u2 ON u2.id = gt.id_teacher_2
      LEFT JOIN ciclos ci ON ci.id = g.id_ciclo
      WHERE ci.iderp = ? AND g.deleted = 0 AND g.editado = 1 AND u.email = ?
      OR ci.iderp = ? AND g.deleted = 0 AND g.editado = 1 AND u2.email = ?`,[ idcicloInbi, email, idcicloInbi, email ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  });
};

evaluacion_teacher.getEvaluacionesClasesFast = ( nivel, lecciones, data) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM preguntas_evaluacion_leccion WHERE id_nivel = ? AND unidad_negocio = 2 AND id_leccion IN ( ? )`,[ nivel, lecciones ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  });
};

evaluacion_teacher.getEvaluacionesClasesInbi = ( nivel, lecciones, data) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM preguntas_evaluacion_leccion WHERE id_nivel = ? AND unidad_negocio = 1 AND id_leccion IN ( ? )`,[ nivel, lecciones ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  });
};

evaluacion_teacher.getEvaluacionesCalificiacionFast = ( idcicloFast, idcicloInbi, id_usuario ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM calificacion_evaluacion_leccion WHERE id_teacher = ? AND id_ciclo = ?`,[ id_usuario, idcicloFast ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  });
};


evaluacion_teacher.getEvaluacionesCalificiacionInbi = ( idcicloFast, idcicloInbi, id_usuario ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM calificacion_evaluacion_leccion WHERE id_teacher = ? AND id_ciclo = ?`,[ id_usuario, idcicloInbi ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  });
};


evaluacion_teacher.getRespuestaEvalaucionClase = ( data ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM respuestas_evaluacion_leccion WHERE idpreguntas_evaluacion_leccion = ? AND id_teacher = ? AND id_ciclo = ?`,
      [ data.idpreguntas_evaluacion_leccion, data.id_usuario, data.id_ciclo ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  });
};

evaluacion_teacher.getRespuestaEvalaucionClaseTeacher = ( id_usuario, idcicloFast, idcicloInbi ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM respuestas_evaluacion_leccion WHERE id_teacher = ? AND id_ciclo IN ( ?, ? );`,[ id_usuario, idcicloFast, idcicloInbi ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    })
  });
};

evaluacion_teacher.addRespuestaEvalaucionClase = ( e ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`INSERT INTO respuestas_evaluacion_leccion(idpreguntas_evaluacion_leccion, unidad_negocio, id_nivel, id_leccion, pregunta, respuesta_1, respuesta_2, respuesta_3, respuesta_4, correcta, id_teacher, id_ciclo, eleccion, valor) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
     [e.idpreguntas_evaluacion_leccion, e.unidad_negocio, e.id_nivel, e.id_leccion, e.pregunta, e.respuesta_1, e.respuesta_2, e.respuesta_3, e.respuesta_4, e.correcta, e.id_teacher, e.id_ciclo, e.eleccion, e.valor],
      (err, res) => {
        if (err) { 
          reject(err); 
          return; 
        }
        resolve({ id: res.insertId, ...e });
      }
    )
  })
}

evaluacion_teacher.updateRespuestaEvalaucionClase = (e, result) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE respuestas_evaluacion_leccion SET eleccion = ?, valor = ? WHERE idrespuestas_evaluacion_leccion > 0 AND idpreguntas_evaluacion_leccion = ? AND id_teacher = ? AND id_ciclo = ?`,
     [e.eleccion, e.valor, e.idpreguntas_evaluacion_leccion, e.id_usuario, e.id_ciclo],
      (err, res) => {
        if (err) { reject(err); return; }
        if (res.affectedRows == 0) {
          reject(err)
          return;
        }
        resolve(res);
    })
  })
}

evaluacion_teacher.addCalificacionEvalaucionClase = (c, result) => {
  sqlERP.query(`INSERT INTO calificacion_evaluacion_leccion(id_teacher, id_ciclo, estatus, calificacion, total_clases, total_correcta, total_incorrecta) VALUES (?,?,?,?,?,?,?)`, 
    [c.id_teacher, c.id_ciclo, c.estatus, c.calificacion, c.total_clases, c.total_correcta, c.total_incorrecta],
    (err, res) => {
      if (err) { result(err, null); return; }
      result(null, { id: res.insertId, ...c});
    }
  )
}


module.exports = evaluacion_teacher;