const { result }  = require("lodash");

const sqlERP      = require("../dbErpPrueba.js");
const sqlERPNUEVO = require("../db.js");


//const constructor
const reporte_modelo_ventas = () => {};


reporte_modelo_ventas.getFechasReporteSemanal = ( fecha ) => {
    return new Promise((resolve, reject) => {
      sqlERP.query(`SELECT DATE_SUB(?, INTERVAL ((ELT(WEEKDAY(?) + 1, 1, 2, 3, 4, 5, 6, 7))-1) DAY) AS fecha_inicios, 
        DATE_ADD(?, INTERVAL (7-(ELT(WEEKDAY(?) + 1, 1, 2, 3, 4, 5, 6, 7))) DAY) AS fecha_finals;`,[ fecha, fecha, fecha, fecha ],(err, res) => {
        if (err) { return reject({ message: err.sqlMessage }); }
        resolve(res[0]);
      });
    });
  };
  
  reporte_modelo_ventas.getFechaAnterior = ( fecha_inicio ) => {
    return new Promise((resolve, reject) => {
      sqlERP.query(`SELECT DATE_SUB(?, INTERVAL 7 DAY) AS fecha_inicioss, 
        DATE_SUB(?, INTERVAL 1 DAY) AS fecha_finalss;`,[ fecha_inicio, fecha_inicio ],(err, res) => {
        if (err) { return reject({ message: err.sqlMessage }); }
        resolve(res[0]);
      });
    });
  };

  reporte_modelo_ventas.graficaContactosVendedora  = ( fecha_inicio, fecha_final ) => {
    return new Promise((resolve, reject) => {
      sqlERPNUEVO.query(`SELECT COUNT(escuela) AS conteo, DATE(fecha_creacion) AS fecha_creacion, usuario_asignado FROM prospectos 
        WHERE DATE(fecha_creacion) BETWEEN ? AND ?  AND escuela > 0
        GROUP BY escuela, DATE(fecha_creacion), usuario_asignado;`,[ fecha_inicio, fecha_final ],(err, res) => {
        if (err) { return reject({message: err.sqlMessage}) }
        resolve(res);
      });
    });
  };

  reporte_modelo_ventas.cantidadContactosSemanaActual  = ( fecha_inicio, fecha_final ) => {
    return new Promise((resolve, reject) => {
      sqlERPNUEVO.query(`SELECT usuario_asignado, SUM(CASE WHEN escuela > 0 THEN conteo ELSE 0 END) AS conteo_semana_actual
      FROM (SELECT usuario_asignado, COUNT(escuela) AS conteo, escuela FROM prospectos 
             WHERE DATE(fecha_creacion) BETWEEN ? AND ? GROUP BY usuario_asignado, escuela
      ) AS subquery
      GROUP BY
      usuario_asignado;`,[ fecha_inicio, fecha_final ],(err, res) => {
        if (err) { return reject({message: err.sqlMessage}) }
        resolve(res);
      });
    });
  };

  reporte_modelo_ventas.cantidadContactosSemanaActualINBI  = ( fecha_inicio, fecha_final ) => {
    return new Promise((resolve, reject) => {
      sqlERPNUEVO.query(`SELECT usuario_asignado, SUM(CASE WHEN escuela > 0 THEN conteo ELSE 0 END) AS conteo_semana_actual
      FROM (SELECT usuario_asignado, COUNT(escuela) AS conteo, escuela FROM prospectos 
             WHERE DATE(fecha_creacion) BETWEEN ? AND ? AND escuela = 1 GROUP BY usuario_asignado, escuela
      ) AS subquery
      GROUP BY
      usuario_asignado;`,[ fecha_inicio, fecha_final ],(err, res) => {
        if (err) { return reject({message: err.sqlMessage}) }
        resolve(res);
      });
    });
  };

  reporte_modelo_ventas.cantidadContactosSemanaActualFAST  = ( fecha_inicio, fecha_final ) => {
    return new Promise((resolve, reject) => {
      sqlERPNUEVO.query(`SELECT usuario_asignado, SUM(CASE WHEN escuela > 0 THEN conteo ELSE 0 END) AS conteo_semana_actual
      FROM (SELECT usuario_asignado, COUNT(escuela) AS conteo, escuela FROM prospectos 
             WHERE DATE(fecha_creacion) BETWEEN ? AND ? AND escuela = 2 GROUP BY usuario_asignado, escuela
      ) AS subquery
      GROUP BY
      usuario_asignado;`,[ fecha_inicio, fecha_final ],(err, res) => {
        if (err) { return reject({message: err.sqlMessage}) }
        resolve(res);
      });
    });
  };


  // dashboardVentas.graficaContactos  = ( fecha_inicio, fecha_final ) => {
  //   return new Promise((resolve, reject) => {
  //     sqlERPNUEVO.query(`SELECT COUNT(escuela) AS conteo, DATE(fecha_creacion) AS fecha_creacion, IF( escuela = 2, 'FAST', 'INBI') AS escuela FROM prospectos 
  //       WHERE DATE(fecha_creacion) BETWEEN ? AND ?  AND escuela > 0
  //       GROUP BY escuela, DATE(fecha_creacion) ;`,[ fecha_inicio, fecha_final ],(err, res) => {
  //       if (err) { return reject({message: err.sqlMessage}) }
  //       resolve(res);
  //     });
  //   });
  // };
  

  
module.exports = reporte_modelo_ventas;