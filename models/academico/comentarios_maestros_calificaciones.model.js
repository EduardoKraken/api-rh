const { result } = require("lodash");
const sqlERP = require("../db3.js");
const sqlINBI = require("../dbINBI.js");
const sqlFAST = require("../dbFAST.js");
const sqlERPNUEVO = require("../db.js");


//const constructor
const comentarios_maestros_calificaciones = function () { };

comentarios_maestros_calificaciones.getMensajeINBI = () => {
  return new Promise((resolve, reject) => {
    sqlINBI.query(`SELECT i.usuario AS Matricula,
    m.idmaestro_mensaje,
    CONCAT(u.nombre, ' ', u.apellido_paterno, ' ', u.apellido_materno) AS Maestro,
    CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', a.apellido_materno) AS Alumno,
    g.nombre AS Grupo,
    1 AS escuela,
    m.id_alumno,
    m.id_grupo,
    m.mensaje AS Mensaje,
    m.estado
FROM maestro_mensaje m
LEFT JOIN usuarios u ON u.id = m.id_usuario
LEFT JOIN usuarios a ON a.id = m.id_alumno
LEFT JOIN usuarios i ON i.id = m.id_alumno
LEFT JOIN grupos g ON g.id = m.id_grupo
WHERE (m.id_alumno, m.id_grupo, m.fecha_creacion) IN (
 SELECT id_alumno, id_grupo, MAX(fecha_creacion)
 FROM maestro_mensaje
 GROUP BY id_alumno, id_grupo
);`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};

comentarios_maestros_calificaciones.getMensajeFAST = () => {
  return new Promise((resolve, reject) => {
    sqlFAST.query(`SELECT i.usuario AS Matricula,
    m.idmaestro_mensaje,
    CONCAT(u.nombre, ' ', u.apellido_paterno, ' ', u.apellido_materno) AS Maestro,
    CONCAT(a.nombre, ' ', a.apellido_paterno, ' ', a.apellido_materno) AS Alumno,
    g.nombre AS Grupo,
    2 AS escuela,
    m.id_alumno,
    m.id_grupo,
    m.mensaje AS Mensaje,
    m.estado
FROM maestro_mensaje m
LEFT JOIN usuarios u ON u.id = m.id_usuario
LEFT JOIN usuarios a ON a.id = m.id_alumno
LEFT JOIN usuarios i ON i.id = m.id_alumno
LEFT JOIN grupos g ON g.id = m.id_grupo
WHERE (m.id_alumno, m.id_grupo, m.fecha_creacion) IN (
 SELECT id_alumno, id_grupo, MAX(fecha_creacion)
 FROM maestro_mensaje
 GROUP BY id_alumno, id_grupo
);`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

comentarios_maestros_calificaciones.updateMensaje = (escuela, idmaestro_mensaje, id_alumno) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve, reject) => {
    db.query(`UPDATE maestro_mensaje SET estado = 1 WHERE idmaestro_mensaje = ? AND id_alumno = ?`, [idmaestro_mensaje, id_alumno], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      if (res.affectedRows == 0) {
        return reject({ message: "not_found" });

      }

      resolve(res);
    });
  });
};


comentarios_maestros_calificaciones.updateMensajeRechazo = (escuela, idmaestro_mensaje, id_alumno) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve, reject) => {
    db.query(`UPDATE maestro_mensaje SET estado = 2 WHERE idmaestro_mensaje = ? AND id_alumno = ?;`, [idmaestro_mensaje, id_alumno], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }

      resolve(res);
    });
  });
};



comentarios_maestros_calificaciones.updateKardex = (escuela, id_alumno, id_grupo) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve, reject) => {
    db.query(`UPDATE cardex_curso 
      SET calificacion_final_segunda_oportunidad = 70 
      WHERE id_alumno = ? AND id_grupo = ? AND id > 0;;`, [id_alumno, id_grupo], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }

      resolve(res);
    });
  });
};


//Nuevo

comentarios_maestros_calificaciones.consultarMensajesRechazo = (escuela, id_alumno, id_grupo) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve, reject) => {
    db.query(`SELECT idmaestro_mensaje, id_usuario, id_alumno, mensaje, id_grupo, respuesta FROM maestro_mensaje
      WHERE id_alumno = ? AND id_grupo = ?;`, [id_alumno, id_grupo], (err, res) => {
      if (err) { return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
};


comentarios_maestros_calificaciones.addIncidenciasMensajeRespuesta = (escuela, respuesta, id_alumno, id_grupo, id_alumno2, id_grupo2) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve, reject) => {
    db.query(`UPDATE maestro_mensaje
      SET respuesta = ?
      WHERE id_alumno = ?
        AND id_grupo = ?
        AND idmaestro_mensaje = (
          SELECT max_id
          FROM (SELECT MAX(idmaestro_mensaje) AS max_id
                FROM maestro_mensaje
                WHERE id_alumno = ?
					AND id_grupo = ?) AS subquery);`,
      [respuesta, id_alumno, id_grupo, id_alumno2, id_grupo2],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }

        resolve(res);
      });
  });
};


comentarios_maestros_calificaciones.updateEstatusIncidencias = (escuela, estatus, id) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve, reject) => {
    db.query(`UPDATE maestro_mensaje SET estado = ?  WHERE idmaestro_mensaje = ?;`, [estatus, id],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }
        resolve(res);
      });
  });
};



// comentarios_maestros_calificaciones.getIdUsuario = ( escuela, id_alumno ) => {
//   const db = escuela == 2 ? sqlFAST : sqlINBI
//   return new Promise((resolve,reject)=>{
//     db.query(`SELECT id 
//     FROM usuarios
//      WHERE iderp = ?;`, [ id_alumno],(err, res) => {
//       if(err){
//         return reject({message: err.sqlMessage })  
//       }
//       resolve(res)
//     })
//   })
// };


// comentarios_maestros_calificaciones.getIdGrupo = ( escuela, id_grupo ) => {
//   const db = escuela == 2 ? sqlFAST : sqlINBI
//   return new Promise((resolve,reject)=>{
//     db.query(`SELECT id 
//     FROM grupos
//      WHERE iderp = ?;`, [ id_grupo],(err, res) => {
//       if(err){
//         return reject({message: err.sqlMessage })  
//       }
//       resolve(res)
//     })
//   })
// };

module.exports = comentarios_maestros_calificaciones;

//ANGEL RODRIGUEZ -- TODO