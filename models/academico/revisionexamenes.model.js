const { result }  = require("lodash");
const sqlERP      = require("../db3.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");
const sqlERPNUEVO = require("../db.js");


//const constructor
const examenes = function() {};

examenes.examenPaginaFast = ( ) => {
  return new Promise((resolve, reject) => {
    sqlFAST.query(`SELECT e.*, n.nombre AS nivel, s.nombre AS plantel, s.id_plantel, "" AS folio, 
      CONCAT("https://www.fastenglish.com.mx/examen-ubicacion/diploma.php?id=", e.id ) AS liga, "contactos_eu" AS bd,
      "ubicacion" AS tipoExamen, "" AS nombre_alumno, "" AS edad_alumno, "" AS tiempo_estudio
      FROM contactos_eu.examen_ubicacion e
      LEFT JOIN contactos_eu.niveles_evaluacion n ON n.id = e.nivel 
      LEFT JOIN contactos_eu.sucursales s ON s.id = e.sucursal
      WHERE e.cant IS NOT NULL
      ORDER BY e.nivel;`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

examenes.examenInternoFast = ( ) => {
  return new Promise((resolve, reject) => {
    sqlFAST.query(`SELECT e.*, n.nombre AS nivel, s.nombre AS plantel, s.id_plantel, "" AS folio, 
      CONCAT("https://www.fastenglish.com.mx/examen-interno/diploma.php?id=", e.id ) AS liga, "examen_interno" AS bd,
      "interno" AS tipoExamen, "" AS nombre_alumno, "" AS edad_alumno, "" AS tiempo_estudio
      FROM examen_interno.examen_ubicacion e
      LEFT JOIN examen_interno.niveles_evaluacion n ON n.id = e.nivel 
      LEFT JOIN examen_interno.sucursales s ON s.id = e.sucursal
      WHERE e.cant IS NOT NULL
      ORDER BY e.nivel;`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

examenes.examenReubicacionFast = ( ) => {
  return new Promise((resolve, reject) => {
    sqlFAST.query(`SELECT e.*, n.nombre AS nivel, s.nombre AS plantel, s.id_plantel, "" AS folio, 
      CONCAT("https://www.fastenglish.com.mx/examen-reubicacion/diploma.php?id=", e.id ) AS liga, "examen_reubicacion" AS bd,
      "reubicacion" AS tipoExamen, "" AS nombre_alumno, "" AS edad_alumno, "" AS tiempo_estudio
      FROM examen_reubicacion.examen_ubicacion e
      LEFT JOIN examen_reubicacion.niveles_evaluacion n ON n.id = e.nivel 
      LEFT JOIN examen_reubicacion.sucursales s ON s.id = e.sucursal
      WHERE e.cant IS NOT NULL
      ORDER BY e.nivel;`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};


examenes.examenPaginaInbi = ( ) => {
  return new Promise((resolve, reject) => {
    sqlINBI.query(`SELECT e.*, n.nombre AS nivel, s.nombre AS plantel, s.id_plantel, "" AS folio, 
      CONCAT("https://www.inbi.mx/examen-ubicacion/diploma.php?id=", e.id ) AS liga, "recolector" AS bd,
      "ubicacion" AS tipoExamen, "" AS nombre_alumno, "" AS edad_alumno, "" AS tiempo_estudio
      FROM recolector.examen_ubicacion e
      LEFT JOIN recolector.niveles_evaluacion n ON n.id = e.nivel 
      LEFT JOIN recolector.sucursales s ON s.id = e.sucursal
      WHERE e.cant IS NOT NULL
      ORDER BY e.nivel;`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

examenes.examenInternoInbi = ( ) => {
  return new Promise((resolve, reject) => {
    sqlINBI.query(`SELECT e.*, n.nombre AS nivel, s.nombre AS plantel, s.id_plantel, "" AS folio, 
      CONCAT("https://www.inbi.mx/examen-interno/diploma.php?id=", e.id ) AS liga, "examen_interno" AS bd,
      "interno" AS tipoExamen, "" AS nombre_alumno, "" AS edad_alumno, "" AS tiempo_estudio
      FROM examen_interno.examen_ubicacion e
      LEFT JOIN examen_interno.niveles_evaluacion n ON n.id = e.nivel 
      LEFT JOIN examen_interno.sucursales s ON s.id = e.sucursal
      WHERE e.cant IS NOT NULL
      ORDER BY e.nivel;`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

examenes.examenTeensInbi = ( ) => {
  return new Promise((resolve, reject) => {
    sqlINBI.query(`SELECT e.*, n.nombre AS nivel, s.nombre AS plantel, s.id_plantel, "" AS folio, 
      CONCAT("https://www.inbi.mx/examen-teens/diploma.php?id=", e.id ) AS liga, "examen_teens" AS bd,
      "teens" AS tipoExamen 
      FROM examen_teens.examen_ubicacion e
      LEFT JOIN examen_teens.niveles_evaluacion n ON n.id = e.nivel 
      LEFT JOIN examen_teens.sucursales s ON s.id = e.sucursal
      WHERE e.cant IS NOT NULL
      ORDER BY e.nivel;`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

examenes.examenReubicacionInbi = ( ) => {
  return new Promise((resolve, reject) => {
    sqlINBI.query(`SELECT e.*, n.nombre AS nivel, s.nombre AS plantel, s.id_plantel, "" AS folio, 
      CONCAT("https://www.inbi.mx/examen-reubicacion/diploma.php?id=", e.id ) AS liga, "examen_reubicacion" AS bd,
      "reubicacion" AS tipoExamen, "" AS nombre_alumno, "" AS edad_alumno, "" AS tiempo_estudio
      FROM examen_reubicacion.examen_ubicacion e
      LEFT JOIN examen_reubicacion.niveles_evaluacion n ON n.id = e.nivel 
      LEFT JOIN examen_reubicacion.sucursales s ON s.id = e.sucursal
      WHERE e.cant IS NOT NULL
      ORDER BY e.nivel;`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};


examenes.getNivelesEvaluacion = ( ) => {
  return new Promise((resolve, reject) => {
    sqlINBI.query(`SELECT * FROM examen_interno.niveles_evaluacion;`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

examenes.getOpcionRespuestasMalas = ( ) => {
  return new Promise((resolve, reject) => {
    sqlINBI.query(`SELECT * FROM examen_interno.opciones_respuesta_preguntas_incorrectas WHERE nombre LIKE '%12%';`,(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

examenes.asignarNivel = ( escuela, c ) => {
  const bd = escuela == 2 ? sqlFAST : sqlINBI 
  return new Promise((resolve, reject) => {
    bd.query(`UPDATE ${ c.bd }.examen_ubicacion SET nivel = ? WHERE id = ?;`,[ c.nivel, c.id ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};


examenes.existeExamenUbicacion = ( escuela, c ) => {
  const bd = escuela == 2 ? sqlFAST : sqlINBI 
  return new Promise((resolve, reject) => {
    bd.query(`SELECT * FROM ${ c.bd }.clasificacion_respuestas WHERE id_examen_ubicacion = ?;`,[ c.id ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res[0]);
    });
  });
};

examenes.agregarRespuestasUbicacion = ( escuela, c ) => {
  const bd = escuela == 2 ? sqlFAST : sqlINBI 
  return new Promise((resolve, reject) => {
    bd.query(`INSERT INTO ${ c.bd }.clasificacion_respuestas(id_examen_ubicacion,valor_11,opcion_11) VALUES (?,?,?);`,[ c.id, c.respuesta, c.repuesta_incorrecta ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

examenes.updateRespuestasUbicacion = ( escuela, c ) => {
  const bd = escuela == 2 ? sqlFAST : sqlINBI 
  return new Promise((resolve, reject) => {
    bd.query(`UPDATE ${ c.bd }.clasificacion_respuestas SET valor_11 = ?, opcion_11 = ? WHERE id > 0 AND  id_examen_ubicacion = ?;`,[ c.respuesta, c.repuesta_incorrecta, c.id ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

examenes.eliminarExamenUbicacion = ( escuela, c ) => {
  const bd = escuela == 2 ? sqlFAST : sqlINBI 
  return new Promise((resolve, reject) => {
    bd.query(`DELETE FROM ${ c.bd }.examen_ubicacion WHERE id = ?;`,[ c.id ],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};


examenes.getFolioProspectos = ( folios ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM prospectos WHERE idprospectos IN (?);`,[folios],(err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    });
  });
};

module.exports = examenes;