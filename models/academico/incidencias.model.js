const { result } = require("lodash");
const sqlERP = require("../db3.js");
const sqlINBI = require("../dbINBI.js");
const sqlFAST = require("../dbFAST.js");
const sqlERPNUEVO = require("../db.js");


//const constructor
const incidencias = function () { };

incidencias.getAlumnos = () => {
  return new Promise((resolve, reject) => {
    sqlINBI.query(`SELECT u.id, u.iderp, CONCAT(u.nombre, ' ', u.apellido_paterno, ' ', u.apellido_materno) as nombre_completo, g.id_grupo, gr.nombre,
    CASE
        WHEN gr.nombre LIKE '%teens%' THEN 3
        WHEN gr.nombre LIKE '%kids%' THEN 4
        ELSE NULL
    END AS tipo_curso
    FROM usuarios u
    LEFT JOIN (
        SELECT id_alumno, MAX(id_grupo) AS id_grupo
        FROM grupo_alumnos
        GROUP BY id_alumno
    ) g ON g.id_alumno = u.id
    LEFT JOIN grupos gr ON gr.id = g.id_grupo
    WHERE u.id_tipo_usuario = 1 AND u.iderp != 0 AND gr.nombre LIKE '%teens%' OR gr.nombre LIKE '%kids%'
    ORDER BY nombre_completo;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};

incidencias.getCiclos = () => {
  return new Promise((resolve, reject) => {
    sqlINBI.query(`SELECT id, iderp, nombre FROM ciclos;`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};


incidencias.getIncidencias = ( idcreo ) => {
  return new Promise((resolve, reject) => {
    sqlINBI.query(`SELECT i.idincidencias, CONCAT(u.nombre,' ', u.apellido_paterno,' ', u.apellido_materno) as nombre_completo, i.estatus,
    i.idreglamento, r.idcurso, r.regla, i.notas, g.id_grupo, gr.nombre as grupo, gr.id_ciclo, c.nombre as ciclo, u.iderp
    FROM incidencias i
    LEFT JOIN usuarios u ON i.idusuarioerp = u.iderp
    LEFT JOIN reglamento r ON i.idreglamento = r.idreglamento
    LEFT JOIN (
        SELECT id_alumno, MAX(id_grupo) AS id_grupo
        FROM grupo_alumnos
        GROUP BY id_alumno
    ) g ON g.id_alumno = u.id
    LEFT JOIN grupos gr ON gr.id = g.id_grupo
    LEFT JOIN ciclos c ON c.id = gr.id_ciclo
    WHERE i.deleted = 0 AND i.idcreo = ?
    ORDER BY i.idincidencias;`,[ idcreo ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res)
    })
  })
};


incidencias.addIncidencia = (u, result) => {
  sqlINBI.query(`INSERT INTO incidencias(idreglamento, notas, idusuarioerp, estatus, idcreo) VALUES (?,?,?,?,?);`, [u.idreglamento, u.notas, u.idusuarioerp, u.estatus, u.idcreo],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }
      result(null, { id: res.insertId, ...u });
    })
};


incidencias.updateIncidencia = (id, u, result) => {
  sqlINBI.query(`UPDATE incidencias SET idreglamento = ?, notas = ?, estatus = ?, deleted = ? WHERE idincidencias = ?;`, [u.idreglamento, u.notas, u.estatus, u.deleted, id],
    (err, res) => {
      if (err) { result(err, null); return; }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id, ...u });
    })
};


incidencias.consultarMensajesRechazoIncidencias = ( iderp, idincidencias ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT idnotas_incidencias, idincidencias, iderp, nota, nota_respuesta FROM notas_incidencias
    WHERE iderp = ? AND idincidencias = ?;`,[ iderp, idincidencias ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
};


incidencias.addIncidenciasMensajeRespuesta = ( nota_respuesta, idincidencias, iderp, idincidencias2,  iderp2 ) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`UPDATE notas_incidencias
    SET nota_respuesta = ?
    WHERE idincidencias = ?
      AND iderp = ?
      AND idnotas_incidencias = (
        SELECT max_id
        FROM (SELECT MAX(idnotas_incidencias) AS max_id
              FROM notas_incidencias
              WHERE idincidencias = ?
                AND iderp = ?) AS subquery);`,
    [ nota_respuesta, idincidencias, iderp, idincidencias2,  iderp2 ],
    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      resolve(res);
    });
  });
};


incidencias.updateEstatusIncidencias = ( estatus, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlINBI.query(`UPDATE incidencias SET estatus = ?  WHERE idincidencias = ?;`,[ estatus, id ],
      (err, res) => {
        if (err) {
          return reject({ message: err.sqlMessage });
        }     
      resolve(res);
    });
  });
};

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

module.exports = incidencias;

//ANGEL RODRIGUEZ -- TODO