const sql = require("./db.js");

//const constructor
const Planteles = function(planteles) {
    this.idplantel = planteles.idplantel;
    this.plantel = planteles.plantel;
    this.idzona = planteles.idzona;
    this.idunidad_negocio = planteles.idunidad_negocio;
    this.usuario_registro = planteles.usuario_registro;
    this.fecha_creacion = planteles.fecha_creacion;
    this.fecha_actualizo = planteles.fecha_actualizo;
    this.deleted = planteles.deleted;
};

Planteles.all_planteles = result => {
  sql.query(`SELECT * FROM plantel WHERE deleted = 0`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Planteles.add_planteles = (p, result) => {
  sql.query(`INSERT INTO plantel(plantel,idzona,idunidad_negocio,usuario_registro,fecha_creacion,fecha_actualizo)VALUES(?,?,?,?,?,?)`, [p.plantel, p.idzona, p.idunidad_negocio, p.usuario_registro, p.fecha_creacion, p.fecha_actualizo],
  (err, res) => {
    if (err) { result(err, null); return; }
    result(null, { id: res.insertId, ...p });
  });
};

Planteles.update_plantel = (id, p, result) => {
  sql.query(` UPDATE plantel SET plantel=?, idzona=?, idunidad_negocio=?, usuario_registro=?, fecha_creacion=?, fecha_actualizo=?, deleted=?
    WHERE idplantel = ?`, [p.plantel, p.idzona, p.idunidad_negocio, p.usuario_registro, p.fecha_creacion, p.fecha_actualizo,p.deleted, id],
  (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) {
          result({ kind: "not_fopd" }, null);
          return;
      }
      result(null, { id: id, ...p });
  });
};

Planteles.getPlantelVendedora = result => {
  sql.query(`SELECT * FROM plantel WHERE deleted = 0`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};



module.exports = Planteles;