const sqlERP = require('../dbSistemaWeb.js');

// CONSTRUCTOR
const lecciones = (lecciones) => {};


// AGREGAR LECCION
lecciones.addLeccion = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO lecciones( idarchivos, idniveles, leccion ) 
      VALUES( ?, ?, ? )`,
      [ c.idarchivos, c.idniveles, c.leccion ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR LECCION MEDIANTE ID
lecciones.updateLeccion = ( c, id_lecciones ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE lecciones SET idarchivos = ?, idniveles = ?, leccion = ?, deleted = ?
      WHERE idlecciones = ?`,
      [ c.idarchivos, c.idniveles, c.leccion, c.deleted, id_lecciones ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }); }

      resolve({ id_lecciones, ...c });
    });
  });
};


// OBTENER LECCIONES POR NIVEL
lecciones.getLecciones = ( id_nivel ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT l.*,
      l.leccion,
      a.nombre AS archivo, a.descripcion, a.extension, a.url, a.tipo_reproduccion, a.interno
    FROM lecciones l
    LEFT JOIN niveles n ON n.idniveles = l.idniveles
    LEFT JOIN archivos a ON a.idarchivos = l.idarchivos
    WHERE l.idniveles = ?
    AND l.deleted = 0;`, [ id_nivel ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) return reject({ message: err.sqlMessage });

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER LECCION MEDIANTE ID
lecciones.getLeccion = ( id_lecciones ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM lecciones
    WHERE idlecciones = ?`, [ id_lecciones ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) return reject({ message: err.sqlMessage });

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};

module.exports = lecciones;