const sqlERP = require('../dbSistemaWeb.js');
// CONSTRUCTOR
const diapositivas = (diapositivas) => {};


// AGREGAR DIAPOSITIVA
diapositivas.addDiapositiva = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO diapositivas( idarchivos, idlecciones, diapositiva, portada )
      VALUES( ?, ?, ?, ? )`,
      [ c.idarchivos, c.idlecciones, c.diapositiva, c.portada ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR DIAPOSITIVA MEDIANTE ID
diapositivas.updateDiapositiva = ( c, id_diapositiva ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE diapositivas SET idarchivos = ?, idlecciones = ?, diapositiva = ?, portada = ?, deleted = ?
      WHERE iddiapositivas = ?`,
      [ c.idarchivos, c.idlecciones, c.diapositiva, c.portada, c.deleted, id_diapositiva ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }); }

      resolve({ id_diapositiva, ...c });
    });
  });
};


// OBTENER DIAPOSITIVAS POR LECCION
diapositivas.getDiapositivas = ( id_leccion ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT d.*, a.nombre AS urlPortada, a2.nombre AS urlArchivo, a2.tipo_reproduccion, a2.extension
    FROM diapositivas d
    LEFT JOIN lecciones l ON l.idlecciones = d.idlecciones
    LEFT JOIN archivos a ON a.idarchivos = d.portada
    LEFT JOIN archivos a2 ON a2.idarchivos = d.idarchivos
    WHERE d.idlecciones = ?;`, [ id_leccion ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) return reject({ message: err.sqlMessage });

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER DIAPOSITIVA MEDIANTE ID
diapositivas.getDiapositiva = ( id_diapositiva ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM diapositivas
    WHERE iddiapositivas = ?`, [ id_diapositiva ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) return reject({ message: err.sqlMessage });

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};

module.exports = diapositivas;