const sqlERP = require('../dbSistemaWeb.js');

// CONSTRUCTOR
const niveles = (niveles) => {};


// AGREGAR NIVEL
niveles.addNivel = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO niveles( idarchivos, idescuelas, nivel ) 
      VALUES( ?, ?, ? )`,
      [ c.idarchivos, c.idescuelas, c.nivel ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR NIVEL MEDIANTE ID
niveles.updateNivel = ( c, id_niveles ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE niveles SET idarchivos = ?, idescuelas = ?, nivel = ?, deleted = ?
      WHERE idniveles = ?`,
      [ c.idarchivos, c.idescuelas, c.nivel, c.deleted, id_niveles ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }); }

      resolve({ id_niveles, ...c });
    });
  });
};


// OBTENER NIVELES por escuela
niveles.getNiveles = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT n.*, a.nombre AS archivo, a.descripcion, a.extension, a.url, a.tipo_reproduccion, a.interno FROM niveles n
      LEFT JOIN archivos a ON a.idarchivos = n.idarchivos
      WHERE n.deleted = 0 AND n.idescuelas = ?;`,[ id ],(err, res) => {

      // VALIDAR ERRORES
      if (err) return reject({ message: err.sqlMessage });

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER NIVEL MEDIANTE ID
niveles.getNivel = ( id_niveles ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM niveles
    WHERE idniveles = ?`, [ id_niveles ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) return reject({ message: err.sqlMessage });

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};

module.exports = niveles;