const sqlERP = require('../dbSistemaWeb.js');
// CONSTRUCTOR
const archivos = (archivos) => {};

// AGREGAR ARCHIVO
archivos.addArchivo = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO archivos( idescuelas, nombre, descripcion, extension, url, tipo_reproduccion, interno )
      VALUES( ?, ?, ?, ?, ?, ?, ? )`,
      [ c.idescuelas, c.nombreUuid, c.descripcion, c.extensionArchivo, c.url, c.tipo_reproduccion, c.interno ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ELIMINAR ARCHIVO POR NOMBRE
archivos.deleteArchivo = ( nombre ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE archivos SET deleted = 1 WHERE nombre = ?`,
      [ nombre ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ nombre });
    });
  });
};


// ELIMINAR ARCHIVO POR URL
archivos.deleteArchivoUrl = ( url ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE archivos SET deleted = 1
      WHERE url = ?`,
      [ url ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ url });
    });
  });
};


module.exports = archivos;