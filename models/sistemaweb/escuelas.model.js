const sqlERP = require('../dbSistemaWeb.js');

// CONSTRUCTOR
const escuelas = (escuelas) => {};


// AGREGAR ESCUELA
escuelas.addEscuela = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO escuelas( escuela ) 
      VALUES( ? )`,[ c.escuela ], 
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR ESCUELA MEDIANTE ID
escuelas.updateEscuela = ( c, id_escuelas ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE escuelas SET escuela = ?, portada = ?, portada_niveles = ?, portada_lecciones = ?, deleted = ? 
      WHERE idescuelas = ?`,
      [ c.escuela, c.portada, c.portada_niveles, c.portada_lecciones, c.deleted, id_escuelas ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }); }

      resolve({ id_escuelas, ...c });
    });
  });
};


// OBTENER ESCUELAS
escuelas.getEscuelas = () => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM escuelas WHERE deleted = 0;`,
    (err, res) => {

      // VALIDAR ERRORES
      if (err) return reject({ message: err.sqlMessage });

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER ESCUELA MEDIANTE ID
escuelas.getEscuela = ( id_escuelas ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM escuelas
    WHERE idescuelas = ?`, [ id_escuelas ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) return reject({ message: err.sqlMessage });

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER INFORMACIÓN DEL SISTEMA MEDIANTE ESCUELA( ID ) Y ACCESO( IP )
escuelas.getEscuelasAcceso = ( idescuela, idequipo ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT e.*
    FROM escuelas e
    LEFT JOIN acceso_academico a ON a.idescuelas = e.idescuelas
    WHERE e.idescuelas = ?`, [ idescuela, idequipo ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) return reject({ message: err.sqlMessage });

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


// OBTENER NIVELES MEDIANTE ESCUELA( ID )
escuelas.getNivelesEscuela = ( id_escuela ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT n.*
    FROM niveles n
    LEFT JOIN escuelas e ON e.idescuelas = n.idescuelas
    WHERE n.idescuelas = ?
    AND n.deleted = 0`, [ id_escuela ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) return reject({ message: err.sqlMessage });

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER LECCIONES MEDIANTE NIVELES( ID )
escuelas.getLeccionesNiveles = ( id_niveles ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT l.*
    FROM lecciones l
    LEFT JOIN niveles n ON n.idniveles = l.idniveles
    WHERE l.idniveles IN (?)
    AND l.deleted = 0`, [ id_niveles ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) return reject({ message: err.sqlMessage });

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER DIAPOSITIVAS MEDIANTE LECCIONES( ID )
escuelas.getDiapositivasLecciones = ( id_lecciones ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT d.*
    FROM diapositivas d
    LEFT JOIN lecciones l ON l.idlecciones = d.idlecciones
    WHERE d.idlecciones IN (?)
    AND d.deleted = 0`, [ id_lecciones ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) return reject({ message: err.sqlMessage });

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER ARCHIVOS MEDIANTE ESCUELA( ID )
escuelas.getArchivosEscuela = ( id_escuela ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT a.*
    FROM archivos a
    LEFT JOIN escuelas e ON e.idescuelas = a.idescuelas
    WHERE a.idescuelas IN (?)
    AND a.deleted = 0`, [ id_escuela ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) return reject({ message: err.sqlMessage });

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


module.exports = escuelas;
