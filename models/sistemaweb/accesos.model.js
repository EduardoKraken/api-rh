const sqlERP = require('../dbSistemaWeb.js');

// CONSTRUCTOR
const accesos = (accesos) => {};


// AGREGAR ACCESO
accesos.addAcceso = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO acceso_academico( idequipo, idescuelas ) 
      VALUES( ?, ? )`,
      [ c.idequipo, c.idescuela ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  });
};


// ACTUALIZAR ACCESO MEDIANTE ID
accesos.updateAcceso = ( c, id_acceso ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`UPDATE acceso_academico SET idescuelas = ?, deleted = ?
      WHERE idacceso_academico = ?`,
      [ c.idescuela, c.deleted, id_acceso ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }); }

      resolve({ id_acceso, ...c });
    });
  });
};


// OBTENER ACCESOS MEDIANTE IDESCUELA
accesos.getAccesos = ( id_escuela ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM acceso_academico
    WHERE idescuelas = ?`, [ id_escuela ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) return reject({ message: err.sqlMessage });

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


// OBTENER ACCESO MEDIANTE ID
accesos.getAcceso = ( id_acceso ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM acceso_academico
    WHERE idacceso_academico = ?`, [ id_acceso ],
    (err, res) => {

      // VALIDAR ERRORES
      if (err) return reject({ message: err.sqlMessage });

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};

module.exports = accesos;