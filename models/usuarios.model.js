const { result } = require("lodash");

// const sqlERP  = require("./db3.js");
const sqlERP  = require("./dbErpPrueba.js");

// const sql     = require("./db.js");
const sqlERPNUEVO = require("./db.js");

//const constructor
const Usuarios = function (usuarios) {
  this.idusuario = usuarios.idusuario;
  this.iderp     = usuarios.iderp;
  this.idpuesto  = usuarios.idpuesto;
};

Usuarios.all_usuarios = (result) => {
  sqlERPNUEVO.query(`SELECT u.*, p.puesto FROM usuarios u
    LEFT JOIN puesto p ON p.idpuesto = u.idpuesto WHERE u.deleted = 0;`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }

    //
    var usuarios = [];

    for (const i in res) {
      //declaro el objeto que utilizare para llenar el arreglo
      var datos = {
        idusuario: res[i].idusuario,
        iderp: res[i].iderp,
        empleado: "",
        idpuesto: res[i].idpuesto,
        usuario_registro: res[i].usuario_registro,
        fecha_creacion: res[i].fecha_creacion,
        fecha_actualizo: res[i].fecha_actualizo,
        deleted: res[i].deleted,
        prioridad: res[i].prioridad,
        online: res[i].online,
        puesto: res[i].puesto,
      };
      usuarios.push(datos);
    }

    sqlERP.query(`SELECT * FROM usuarios WHERE activo_sn = 1 ORDER BY nombre_completo`,
      (err, res) => {
        if (err) {
          result(null, err);
          return;
        }

        for (const i in usuarios) {
          for (const j in res) {
            if (usuarios[i].iderp == res[j].id_usuario) {
              usuarios[i].empleado = res[j].nombre_completo;
            }
          }
        }

        result(null, usuarios);
      }
    );
    //
  });
};

Usuarios.getUsuarioId = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT u.*, p.puesto, 
      IF(MONTH(fecha_nacimiento) = MONTH(NOW()), IF(DAY(fecha_nacimiento) = DAY(NOW()), 1,0),0) AS cumple_hoy FROM usuarios u LEFT JOIN puesto p ON u.idpuesto = p.idpuesto WHERE  u.iderp =  ?;`,[id],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  }); 
};

Usuarios.getNuevoERPUsuario = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT u.*, p.puesto FROM usuarios u LEFT JOIN puesto p ON u.idpuesto = p.idpuesto WHERE u.deleted = 0 AND u.iderp = ? `,[id],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res[0])
    });
  }); 
};

Usuarios.getUsuariosERPID = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT * FROM usuarios WHERE id_usuario = ?;`,[ id ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res[0])
    })
  })
}

Usuarios.getPlantelesUsuario = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM planteles_usuario WHERE idusuario = ?`,[id],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

Usuarios.add_usuarios = (u, result) => {
  sqlERPNUEVO.query(
    `INSERT INTO usuarios(iderp,idpuesto,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?,?)`,
    [
      u.iderp,
      u.idpuesto,
      u.usuario_registro,
      u.fecha_creacion,
      u.fecha_actualizo,
      u.deleted,
    ],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }

      result(null, { id: res.insertId, ...u });
    }
  );
};

Usuarios.update_usuario = (id, u, result) => {
  sqlERPNUEVO.query(
    ` UPDATE usuarios SET idpuesto=?,usuario_registro=?,deleted=?,prioridad=?,online=? WHERE idusuario=?`,
    [u.idpuesto, u.usuario_registro, u.deleted, u.prioridad, u.online, id],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, { id: id, ...u });
    }
  );
};

//// usuarios por departamento
Usuarios.getusuariosDepto = (id, result) => {
  sqlERPNUEVO.query(
    `SELECT u.idusuario, u.iderp FROM usuarios u JOIN puesto p ON u.idpuesto = p.idpuesto JOIN departamento d ON p.iddepartamento = d.iddepartamento AND d.iddepartamento=? AND u.deleted=0;`,
    [id],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }
      result(null, res);
    }
  );
};

//// usuarios por puesto
Usuarios.getusuariosPuesto = (id, result) => {
  sqlERPNUEVO.query(
    `SELECT * FROM usuarios WHERE deleted = 0 AND idpuesto=?`,
    [id],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }
      result(null, res);
    }
  );
};
/*********************************************************/

Usuarios.all_usuariosERP = (result) => {
  sqlERP.query(`SELECT * FROM usuarios WHERE activo_sn = 1 ORDER BY nombre_completo`,(err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Usuarios.all_usuariosCAPA = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT u.*, p.plantel FROM usuarios u  LEFT JOIN planteles p  ON p.id_plantel = u.id_plantel WHERE u.activo_sn = 1 AND u.id_usuario = ? `,[id],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};



//obtener info de un usuario erp
Usuarios.usuario_id = (id, result) => {
  sqlERP.query(`SELECT * FROM usuarios WHERE activo_sn = 1 AND id_usuario = ?`,[id],(err, resERP) => {
    if (err) {
      result(null, err);
      return;
    }
    sqlERPNUEVO.query(`SELECT u.*, p.puesto FROM usuarios u
      LEFT JOIN puesto p ON p.idpuesto = u.idpuesto WHERE iderp = ?`,[id],(err, res) => {
      if (err) {
        result(null, err);
        return;
      }

      var data = [{
        nombre_completo : resERP[0].nombre_completo,
        nombre_usuario  : resERP[0].nombre_usuario,
        apellido_usuario: resERP[0].apellido_usuario,
        puesto:           res[0].puesto,
        idpuesto:         res[0].idpuesto,
        idusuario:        res[0].idusuario
      }]
      result(null, data);
    })
  });
};

/*********************************************************/
Usuarios.getUsuariosNiveles = (result) => {
  sqlERPNUEVO.query(`SELECT * FROM organigrama_niveles`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Usuarios.addUsuariosNiveles = (u, result) => {
  sqlERPNUEVO.query(
    `INSERT INTO organigrama_niveles(nivel,prioridad)VALUES(?,?)`,
    [u.nivel, u.prioridad],
    (err, res) => {
      if (err) {
        result(err, null);
        return;
      }

      result(null, { id: res.insertId, ...u });
    }
  );
};

Usuarios.updateUsuariosNiveles = (id, u, result) => {
  sqlERPNUEVO.query(
    ` UPDATE organigrama_niveles SET nivel =?, prioridad =? WHERE idorganigrama_niveles=?`,
    [u.nivel, u.prioridad, id],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...u });
    }
  );
};

Usuarios.getPuestoUsuario = (id_usuario) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(
      `SELECT usuarios.idpuesto FROM usuarios WHERE deleted = 0 AND idusuario=?`,
      [id_usuario],
      (err, res) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(res);
      }
    );
  });
};

Usuarios.loginCapa = (u, result) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT u.*, u.id_usuario AS iderp, IF(j.id_usuario, "Jefe", "Auxiliar") AS cargo, IF(j.idareas_ticket, j.idareas_ticket, au.idareas_ticket) AS idareas_ticket
      FROM usuarios u 
      LEFT JOIN jefe_area j ON j.id_usuario = u.id_usuario AND j.activo = 1
      LEFT JOIN auxi_area au ON au.id_usuario = u.id_usuario AND au.activo = 1
      LEFT JOIN areas_ticket a ON a.idareas_ticket = j.idareas_ticket
      WHERE u.usuario = ?
      AND u.password = ? AND u.activo_sn = 1;`,[ u.email, u.password],(err, res) => {
      if (err)
        return reject({ message : err.sqlMessage }) ;
      
      resolve(res[0]);
    });
  })
};

Usuarios.loginCapa2 = (id_usuario, result) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT u.*, p.puesto FROM usuarios u LEFT JOIN puesto p ON p.idpuesto = u.idpuesto WHERE iderp = ?`,[id_usuario],(err, res) => {
      if (err) {
        return reject({ message : err.sqlMessage }) ;
      }
      resolve(res[0]);
    });  
  })
};

Usuarios.getUsuarioVendedora = () => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM usuarios WHERE deleted = 0 AND idpuesto = 18`,
    (err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res);
    });
  });
};

Usuarios.getUsuarios = () => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM usuarios ORDER BY nombre_completo; `,
    (err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res);
    });
  });
};

Usuarios.getUsuariosActivos = () => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM usuarios WHERE activo_sn = 1 ORDER BY nombre_completo; `,
    (err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res);
    });
  });
};

Usuarios.getEntradasEmpleados = ( fecha, interval ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT  t.id ,t.nombre_completo, t.id_puesto, t.id_plantel, p.plantel, s.hora_registro, s.id AS id_salida, DATE(s.fecha_registro) AS fecha, u.id_usuario, s.id AS idchecador
      FROM fyn_proc_entradas_salidas s
      LEFT JOIN fyn_cat_trabajadores t ON t.id = s.id_trabajador
      LEFT JOIN planteles p ON p.id_plantel = t.id_plantel
      LEFT JOIN usuarios u ON u.id_trabajador = t.id
      WHERE DATE(s.fecha_registro) = DATE_ADD(DATE(?), INTERVAL ${ interval } DAY);`,[ fecha ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res);
    });
  });
};

Usuarios.getPuestosEmpleado = ( idUsuarios ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT u.iderp, p.puesto FROM usuarios u 
      LEFT JOIN puesto p ON p.idpuesto = u.idpuesto
      WHERE u.iderp IN ( ? );`,[ idUsuarios ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res);
    });
  });
};

Usuarios.getExisteUsuario = ( usuario ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM usuarios WHERE email = CONCAT( ? ,'@inbi.mx') AND activo_sn = 1
      OR email = CONCAT( ? ,'@fastenglish.com.mx') AND activo_sn = 1;`,[ usuario, usuario ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res[0]);
    });
  });
};

Usuarios.valdiarContrasenia = ( usuario, password ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM usuarios WHERE email = ? AND password = ?; `,[ usuario, password ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res[0]);
    });
  });
};

Usuarios.existeCodigo = ( id_usuario, id_grupo, unidad_negocio ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT * FROM codigos_asistencia 
      WHERE id_usuario = ? 
      AND id_grupo = ? 
      AND unidad_negocio = ? 
      AND DATE(fecha_creacion) = CURRENT_DATE; `,[ id_usuario, id_grupo, unidad_negocio ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res[0]);
    });
  });
};

Usuarios.addCodigoAsistencia = ( id_trabajador, id_usuario, codigo, id_grupo, unidad_negocio ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`INSERT INTO codigos_asistencia(id_trabajador, id_usuario, codigo, id_grupo, unidad_negocio )VALUES(?,?,?,?,?)`,
      [ id_trabajador, id_usuario, codigo, id_grupo, unidad_negocio ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve({ id: res.insertId, id_trabajador, id_usuario, codigo });
    });
  });
};


/***** Archivos *****/

Usuarios.addArchivo = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO archivos( archivo ) 
      VALUES( ? )`,
      [ c.archivo ],
      (err, res) => {

      // Validar errores
      if (err) { return reject({ message: err.sqlMessage }); }

      // Respuesta al controlador
      return resolve({ id: res.insertId, ...c });
    });
  });
};

Usuarios.updateArchivo = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE archivos SET
      archivo = ?, deleted = ? 
      WHERE idarchivos = ?`,
      [ c.archivo, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ ...c });
    });
  });
};

Usuarios.getArchivo = ( id_archivo ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM archivos
      WHERE idarchivos = ? 
      AND deleted = 0;`, 
      [ id_archivo ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};

Usuarios.getArchivos = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM archivos
      WHERE deleted = 0;`,
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


Usuarios.getArchivoNombre = ( archivo ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM archivos
      WHERE archivo = ?
      AND deleted = 0;`,
      [ archivo ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


/***** Archivos Empleados *****/

Usuarios.addArchivoEmpleado = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`INSERT INTO archivos_empleado( idarchivos, idempleados, documento ) 
      VALUES( ?, ?, ? )`,
      [ c.idarchivos, c.idempleados, c.documento ],
      (err, res) => {

      // Validar errores
      if (err) { return reject({ message: err.sqlMessage }); }

      // Respuesta al controlador
      return resolve({ id: res.insertId, ...c });
    });
  });
};

Usuarios.updateArchivoEmpleado = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`UPDATE archivos_empleado SET
      idarchivos = ?, idempleados = ?, documento = ?, deleted = ?
      WHERE idarchivos_empleado = ?`,
      [ c.idarchivos, c.idempleados, c.documento, c.deleted, id ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ ...c });
    });
  });
};

Usuarios.getArchivoEmpleado = ( id_archivo ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM archivos_empleado
      WHERE idarchivos_empleado = ? 
      AND deleted = 0;`, 
      [ id_archivo ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};

Usuarios.getArchivosEmpleado = ( idempleados ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT a.archivo, ae.documento, ae.idarchivos_empleado
      FROM archivos_empleado ae
      INNER JOIN archivos a ON ae.idarchivos = a.idarchivos
      WHERE ae.idempleados = ?
      AND ae.deleted = 0
      AND a.deleted = 0;`, 
      [ idempleados ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


Usuarios.getArchivosNombre = ( idsarchivos ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT
        documento
      FROM archivos_empleado
      WHERE idarchivos_empleado IN (?)
      AND deleted = 0;`, 
      [ idsarchivos ],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  });
};


Usuarios.getArchivoNombre = ( archivo ) => {
  return new Promise(( resolve, reject )=>{
    sqlERPNUEVO.query(`SELECT * FROM archivos
      WHERE archivo = ?
      AND deleted = 0;`,
      [ archivo ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};


Usuarios.getNombreEmpleado = ( idempleados ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT nombre_completo 
      FROM usuarios
      WHERE id_usuario = ?;`,
      [ idempleados ], (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  });
};

module.exports = Usuarios;
