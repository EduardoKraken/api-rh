const { result } = require("lodash");
const sqlERP  = require("./dbErpPrueba.js");
const sqlRH   = require("./db.js");
const sqlFAST = require("./dbFAST.js");
const sqlINBI = require("./dbINBI.js");


//const constructor
const Roles = function(calificaciones) {};

// Obtener todos los ciclos
Roles.getCiclos = result => {
  sqlERP.query(`SELECT * FROM ciclos WHERE activo_sn = 1`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};


// Obtener todos los ciclos
Roles.getGruposRoles = (ciclo,ciclo2,result) => {
  sqlFAST.query(`SELECT c.nombre AS curso,g.id_nivel, h.nombre AS horario,p.nombre AS plantel, g.nombre As grupo, p.id AS id_plantel,c.id AS id_curso, c.nombre AS curso, g.optimizado,
    h.hora_inicio, h.hora_fin, z.zona, z.idareas, g.id_unidad_negocio FROM grupos g
    LEFT JOIN planteles p ON p.id = g.id_plantel
    LEFT JOIN cursos c ON c.id = g.id_curso
    LEFT JOIN horarios h ON h.id = g.id_horario
    LEFT JOIN ciclos ci ON g.id_ciclo = ci.id
    LEFT JOIN areas z ON z.idareas = p.idzona
    WHERE ci.iderp = ?
    GROUP BY c.nombre,g.id_nivel, h.nombre, p.nombre, g.nombre, p.id, c.id,
    g.optimizado, h.hora_inicio, h.hora_fin, z.zona, z.idareas, g.id_unidad_negocio
    LIMIT 20;`,[ciclo], (err, gruposFast) => {
    if (err) {
      result(null, err);
      return;
    }
    
    sqlINBI.query(`SELECT c.nombre AS curso,g.id_nivel, h.nombre AS horario,p.nombre AS plantel, g.nombre As grupo, p.id AS id_plantel,c.id AS id_curso, c.nombre AS curso, g.optimizado,
    h.hora_inicio, h.hora_fin, z.zona, z.idareas, g.id_unidad_negocio FROM grupos g
    LEFT JOIN planteles p ON p.id = g.id_plantel
    LEFT JOIN cursos c ON c.id = g.id_curso
    LEFT JOIN horarios h ON h.id = g.id_horario
    LEFT JOIN ciclos ci ON g.id_ciclo = ci.id
    LEFT JOIN areas z ON z.idareas = p.idzona
      WHERE ci.iderp = ?
      GROUP BY c.nombre,g.id_nivel, h.nombre, p.nombre, g.nombre, p.id, c.id,
      g.optimizado, h.hora_inicio, h.hora_fin, z.zona, z.idareas, g.id_unidad_negocio
      LIMIT 20;`,[ciclo2], (err, gruposInbi) => {
      if (err) {
        result(null, err);
        return;
      }

      var gruposConcat = gruposFast.concat(gruposInbi);
      result(null, gruposConcat);
    });
  });
};

// Obtener todos los ciclos
Roles.getTeachersRoles = result => {
  var configUsuario = []
  sqlRH.query(`SELECT * FROM usuarios WHERE idpuesto = 10 ORDER BY prioridad DESC, online DESC;`, (err, teachers) => {
    if (err) {
      result(null, err);
      return;
    }
    // Consultamos primero las zonas del usuario
    sqlRH.query(`SELECT DISTINCT pl.idzona, p.idusuario, iderp, IFNULL(p.prioridad,100) AS prioridad2, pl.plantel FROM planteles_usuario p
      LEFT JOIN usuarios u ON u.idusuario = p.idusuario
      LEFT JOIN plantel pl ON pl.idplantel = p.idplantel
      WHERE idpuesto = 10 ORDER BY prioridad2 ASC, pl.plantel;`, (err, zonas) => {
      if (err) {
        result(null, err);
        return;
      } 
      // Generamos un objeto que se estará llenando dependiendo de los datos del teacher
      for(const i in teachers){
        var data = {
          idusuario: teachers[i].idusuario,
          iderp:     teachers[i].iderp,
          online:    teachers[i].online,
          prioridad: teachers[i].prioridad,
          nombre:'',
          zonas: [],
          sucursales:[],
          niveles:[],
          disponibilidad:[]
        }
        configUsuario.push(data)
      }

      for(const i in configUsuario){
        for(const j in zonas){
          if(configUsuario[i].idusuario == zonas[j].idusuario){
            configUsuario[i].zonas.push(zonas[j])
          }
        }
      }
      // como segundo paso consultamos las sucursales del usuario
      sqlRH.query(`SELECT p.*, pl.idzona, IFNULL(p.prioridad,100) AS prioridad2 FROM planteles_usuario p
        LEFT JOIN usuarios u ON u.idusuario = p.idusuario
        LEFT JOIN plantel pl ON pl.idplantel = p.idplantel
        WHERE idpuesto = 10 ORDER BY prioridad2;`, (err, sucursales) => {
        if (err) {
          result(null, err);
          return;
        } 
        // Llenamos las sucursales
        for(const i in configUsuario){
          for(const j in sucursales){
            if(configUsuario[i].idusuario == sucursales[j].idusuario){
              configUsuario[i].sucursales.push(sucursales[j])
            }
          }
        }
        // Ahora consultamos la disponibilidad del usuario
        sqlRH.query(`SELECT d.* FROM disponibilidad_usuario d
          LEFT JOIN usuarios u ON u.idusuario = d.idusuario
          WHERE idpuesto = 10 ORDER BY d.inicio DESC, d.dia;`, (err, disponibilidad) => {
          if (err) { 
            result(null, err);
            return;
          } 
          // Llenamos las disponibilidades
          for(const i in configUsuario){
            for(const j in disponibilidad){
              if(configUsuario[i].idusuario == disponibilidad[j].idusuario){
                configUsuario[i].disponibilidad.push(disponibilidad[j])
              }
            }
          }
          // Ahora consultamos la disponibilidad del usuario
          sqlRH.query(`SELECT n.* FROM niveles_usuario n
            LEFT JOIN usuarios u ON u.idusuario = n.idusuario
            WHERE idpuesto = 10;`, (err, niveles) => {
            if (err) {
              result(null, err);
              return;
            } 
            // Llenamos los niveles
            for(const i in configUsuario){
              for(const j in niveles){
                if(configUsuario[i].idusuario == niveles[j].idusuario){
                  configUsuario[i].niveles.push(niveles[j])
                }
              }
            }
            sqlERP.query(`SELECT * FROM usuarios WHERE activo_sn = 1;`, (err, usuariosERP) => {
              if (err) {
                result(null, err);
                return;
              } 
              // Llenamos los niveles
              for(const i in configUsuario){
                for(const j in usuariosERP){
                  if(configUsuario[i].iderp == usuariosERP[j].id_usuario){
                    configUsuario[i].nombre = usuariosERP[j].nombre_completo
                  }
                }
              }
              result(null, configUsuario);
            });
          });
        });
      });
    });
  });
};




module.exports = Roles;