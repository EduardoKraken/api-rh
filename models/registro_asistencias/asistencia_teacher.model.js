const { result } = require("lodash");
const sqlERPNUEVO = require("../db.js");
const sqlINBI     = require("../dbINBI.js");
const sqlFAST     = require("../dbFAST.js");
const sqlERP      = require("../db3.js");


//const constructor
const asistencia_teacher = function(e) {};

asistencia_teacher.validarAccesoFast  = ( usuario, password ) => {
  return new Promise((resolve, reject) => {
    sqlFAST.query(`SELECT * FROM usuarios WHERE usuario = ? AND password = ?;`, [ usuario, password ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

asistencia_teacher.validarAccesoInbi  = ( usuario, password ) => {
  return new Promise((resolve, reject) => {
    sqlINBI.query(`SELECT * FROM usuarios WHERE usuario = ? AND password = ?;`, [ usuario, password ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

asistencia_teacher.validarGruposFast  = ( id_teacher ) => {
  return new Promise((resolve, reject) => {
    sqlFAST.query(`SELECT g.id AS id_grupo, 
			IF((ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,gt.id_teacher,gt.id_teacher_2) AS id_teacher, h.hora_inicio, h.hora_fin, g.nombre AS grupo,
			TIMESTAMPDIFF(MINUTE,TIME(h.hora_inicio),TIME(CURTIME())) AS antes, TIMESTAMPDIFF(MINUTE,TIME(CURTIME()),TIME(h.hora_inicio)) AS despues,
			(ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 2)) AS num_teacher,
			(ELT(WEEKDAY(CURDATE()) + 1, 1, 2, 3, 4, 5, 6, 7)) AS dia, ci.iderp AS id_ciclo
			FROM grupos g
			LEFT JOIN horarios h         ON h.id         = g.id_horario
			LEFT JOIN cursos c           ON c.id 		     = g.id_curso  
			LEFT JOIN frecuencia f       ON f.id 		     = c.id_frecuencia
			LEFT JOIN grupo_teachers gt  ON gt.id_grupo  = g.id
			LEFT JOIN ciclos ci          ON ci.id 	     = g.id_ciclo
			WHERE g.id_ciclo IN (SELECT id FROM ciclos WHERE CURDATE() BETWEEN fecha_inicio AND fecha_fin AND deleted = 0)
			AND   g.deleted = 0
			AND   g.editado = 1
			AND   g.optimizado = 0
			AND   IF((ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,gt.id_teacher,gt.id_teacher_2) = ?
			AND   (ELT(WEEKDAY(CURDATE()) + 1, lunes, martes, miercoles, jueves, viernes, sabado, domingo )) = 1 
			AND   TIMESTAMPDIFF(MINUTE,TIME(CURTIME()),TIME(h.hora_inicio)) >= -15 /* VALIDAR QUE SEA 5 MINUTOS ANTES */
			AND   TIMESTAMPDIFF(MINUTE,TIME(CURTIME()),TIME(h.hora_inicio)) <= 15 /* VALIDAR QUE SEA 10 MINUTOS DESPUES */;`, [ id_teacher ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

asistencia_teacher.validarGruposInbi  = ( id_teacher ) => {
  return new Promise((resolve, reject) => {
    sqlINBI.query(`SELECT g.id AS id_grupo, 
			IF((ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,gt.id_teacher,gt.id_teacher_2) AS id_teacher, h.hora_inicio, h.hora_fin, g.nombre AS grupo,
			TIMESTAMPDIFF(MINUTE,TIME(h.hora_inicio),TIME(CURTIME())) AS antes, TIMESTAMPDIFF(MINUTE,TIME(CURTIME()),TIME(h.hora_inicio)) AS despues,
			(ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 2)) AS num_teacher,
			(ELT(WEEKDAY(CURDATE()) + 1, 1, 2, 3, 4, 5, 6, 7)) AS dia, ci.iderp AS id_ciclo
			FROM grupos g
			LEFT JOIN horarios h         ON h.id         = g.id_horario
			LEFT JOIN cursos c           ON c.id 		     = g.id_curso  
			LEFT JOIN frecuencia f       ON f.id 		     = c.id_frecuencia
			LEFT JOIN grupo_teachers gt  ON gt.id_grupo  = g.id
			LEFT JOIN ciclos ci          ON ci.id 	     = g.id_ciclo
			WHERE g.id_ciclo IN (SELECT id FROM ciclos WHERE CURDATE() BETWEEN fecha_inicio AND fecha_fin AND deleted = 0)
			AND   g.deleted = 0
			AND   g.editado = 1
			AND   g.optimizado = 0
			AND   IF((ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,gt.id_teacher,gt.id_teacher_2) = ?
			AND   (ELT(WEEKDAY(CURDATE()) + 1, lunes, martes, miercoles, jueves, viernes, sabado, domingo )) = 1 
			AND   TIMESTAMPDIFF(MINUTE,TIME(CURTIME()),TIME(h.hora_inicio)) >= -15 /* VALIDAR QUE SEA 5 MINUTOS ANTES */
			AND   TIMESTAMPDIFF(MINUTE,TIME(CURTIME()),TIME(h.hora_inicio)) <= 15 /* VALIDAR QUE SEA 10 MINUTOS DESPUES */;`, [ id_teacher ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

asistencia_teacher.addAsistenciaTeacher = ( u, escuela, suplente, id_suplente ) => {
	return new Promise((resolve, reject) => {
	  sqlERPNUEVO.query(`INSERT INTO asistencia_teachers(id_teacher,num_teacher,dia,id_grupo,escuela,id_ciclo,suplente,id_suplente)
	  	VALUES(?,?,?,?,?,?,?,?)`,
   		[u.id_teacher,u.num_teacher,u.dia,u.id_grupo,escuela,u.id_ciclo,suplente,id_suplente],(err, res) => {
	    if (err) {
	      reject(err);
	      return;
	    }
	   resolve({ id: res.insertId, ...u });
	  });
	});
};

asistencia_teacher.validaAsistencia  = ( id_teacher, id_grupo ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`SELECT * FROM asistencia_teachers
			WHERE id_teacher = ? AND  id_grupo = ?
			AND dia = (ELT(WEEKDAY(CURDATE()) + 1, 1, 2, 3, 4, 5, 6, 7));`, [ id_teacher, id_grupo ],(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res[0]);
    });
  });
};

asistencia_teacher.getGruposHoyFast  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlFAST.query(`SELECT g.id AS id_grupo, g.online, g.id_plantel,p.nombre AS plantel,
			IF((ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,gt.id_teacher,gt.id_teacher_2) AS id_teacher, h.hora_inicio, h.hora_fin, g.nombre AS grupo,
			IF((ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 1)) = 1,
				CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")),
					CONCAT(u2.nombre, " ", u2.apellido_paterno, " ", IFNULL(u2.apellido_materno, ""))) AS teacher,
			(ELT(WEEKDAY(CURDATE()) + 1, 1, 1, 1, 2, 2, 1, 2)) AS num_teacher,
			(ELT(WEEKDAY(CURDATE()) + 1, 1, 2, 3, 4, 5, 6, 7)) AS dia, ci.iderp AS id_ciclo
			FROM grupos g
			LEFT JOIN horarios h         ON h.id         = g.id_horario
			LEFT JOIN cursos c           ON c.id 		     = g.id_curso  
			LEFT JOIN frecuencia f       ON f.id 		     = c.id_frecuencia
			LEFT JOIN grupo_teachers gt  ON gt.id_grupo  = g.id
			LEFT JOIN ciclos ci          ON ci.id 	     = g.id_ciclo
			LEFT JOIN usuarios u         ON u.id         = gt.id_teacher
			LEFT JOIN usuarios u2        ON u2.id        = gt.id_teacher_2
			LEFT JOIN planteles p        ON p.id         = g.id_plantel
			WHERE g.id_ciclo IN (SELECT id FROM ciclos WHERE CURDATE() BETWEEN fecha_inicio AND fecha_fin AND deleted = 0)
			AND   g.deleted = 0
			AND   g.editado = 1
			AND   g.optimizado = 0
			AND   (ELT(WEEKDAY(CURDATE()) + 1, lunes, martes, miercoles, jueves, viernes, sabado, domingo )) = 1 
			AND   TIMESTAMPDIFF(MINUTE,TIME(CURTIME()),TIME(h.hora_inicio)) >= -360 /* VALIDAR QUE SEA 5 MINUTOS ANTES */
			AND   TIMESTAMPDIFF(MINUTE,TIME(CURTIME()),TIME(h.hora_inicio)) <= 360 /* VALIDAR QUE SEA 10 MINUTOS DESPUES */;`,(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

asistencia_teacher.getAsistenciasHoy  = ( ) => {
  return new Promise((resolve, reject) => {
    sqlERP.query(`SELECT DISTINCT a.id_plantel, t.id_puesto,
			CONCAT(t.nombres, ' ', t.apellido_paterno, ' ', IFNULL( t.apellido_materno, '' )) AS nombre_completo,
			p.plantel FROM fyn_proc_entradas_salidas a 
			LEFT JOIN fyn_cat_trabajadores t ON t.id = a.id_trabajador
			LEFT JOIN planteles p ON p.id_plantel = t.id_plantel WHERE DATE(fecha_registro) = CURDATE() AND t.id_puesto IN (3,4);`,(err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

module.exports = asistencia_teacher;
