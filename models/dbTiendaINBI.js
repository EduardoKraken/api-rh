const mysql    = require('mysql');
const dbConfig = require("../config/dbTiendaINBI.js");

// Create a connection to the database
const connectionTiendaINBI = mysql.createConnection({
  host: dbConfig.HOST,
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  database: dbConfig.DB,
  dateStrings: true
});

// open the MySQL connection
connectionTiendaINBI.connect(error => {
  if (error) throw error;
  console.log("|********* CONECCION A" +" "+ dbConfig.DB  +" "+ "CORRECTAMENTE **********|");
});

module.exports = connectionTiendaINBI;