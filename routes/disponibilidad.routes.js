module.exports = app => {
  const disponibilidad = require('../controllers/disponibilidad_usuario.controllers') // const para utilizar nuestro controllador--> ADDED THIS
  app.get("/disponibilidad.all"                                , disponibilidad.getDisponibilidadUsuario); // OBTENER
  app.get("/disponibilidad.usuario/:idusuario"                 , disponibilidad.getDisponibilidadUsuarioId); // OBTENER
  app.post("/disponibilidad.add"                               , disponibilidad.addDisponibilidadUsuario); //AGREGAR USUARIO
  app.put("/disponibilidad.update/:iddisponibilidad_usuario"   , disponibilidad.updateDisponibilidadUsuario); //actualizar usuario
  app.delete("/disponibilidad.delete/:iddisponibilidad_usuario", disponibilidad.deleteDisponibilidadUsuario); //eliminar
};