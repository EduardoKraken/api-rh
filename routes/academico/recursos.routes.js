module.exports = app => {
    const recursos = require('../../controllers/academico/recursos.controllers') // --> ADDED THIS
  
    app.post("/get.all.cursos",                           recursos.getCursosAlumnos);
    app.post("/get.all.recursos",                         recursos.getRecursosAlumnos);
    app.post("/get.all.preguntas",                        recursos.getPreguntasAlumnos);
    app.post("/get.all.respuestas",                       recursos.getRespuestasAlumnos);
    app.post("/get.all.respuestasporpregunta",            recursos.getRespuestasAlumnosPORpregunta);
  };