module.exports = app => {
    const incidencias = require('../../controllers/academico/incidencias.controllers') // --> ADDED THIS
    app.get("/incidencias.get.alumnos",                   incidencias.getAlumnos); 
    app.get("/incidencias.get.ciclos",                    incidencias.getCiclos); 
    app.post("/incidencias.get.incidencias",              incidencias.getIncidencias); 
    app.post('/incidencias.add.incidencia',               incidencias.addIncidencia); 
    app.put('/incidencias.update/:id' ,                   incidencias.updateIncidencia); 
    app.post("/consultar.mensajes.rechazo.incidencias",   incidencias.consultarMensajesRechazoIncidencias); 
    app.post("/update.incidencias.mensaje.respuesta",     incidencias.addIncidenciasMensajeRespuesta); 
    app.post("/update.estatus.incidencias",               incidencias.updateEstatusIncidencias); 
  };