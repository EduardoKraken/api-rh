module.exports = app => {
    const certificacion = require('../../controllers/academico/certificacion.controllers') // --> ADDED THIS
    app.post("/academico.getcodigos",					certificacion.getCodigos); 
    app.post("/academico.codigo.add",					certificacion.addCodigo); 
    app.get("/academico.alumnos.certificacion",         certificacion.getAlumnosCerti); 
    app.post("/academico.codigo.update",                certificacion.updateCodigo);

    // codigos asignados
    app.get("/academico.alumnos.codigos",                        certificacion.getAlumnosCodigo); 
    app.post("/academico.certificacion.usuario",                 certificacion.getCertificadoCambridgeAlumno); 
    app.put("/academico.certificacion.update/:id",               certificacion.updateCertificado); 
    app.put("/academico.codigo.nivel",                           certificacion.updateCodigoNivel);
    app.post("/academico.crear.certificado.cabridge",            certificacion.crearCertificadoCambridge);
    app.post("/academico.update.certificado.cabridge",           certificacion.updateSolicitudCertificadoCambridge);
    app.post("/academico.cambridge.imagen/:id/:ext/:escuela",    certificacion.addImagenEntregaCambridge);


    // Alumnos de ultimo nivel
    app.get("/academico.alumnos.ultimonivel"            ,certificacion.getAlumnosUltimoNivel); 
    app.post("/academico.solicitar.certificado"         ,certificacion.addSolicitudCertificado);
    app.get("/academico.certificados.solicitados"       ,certificacion.getCertificadosSolicitados);
    app.put("/academico.actualizar.certificado/:id"     ,certificacion.updateSolicitudCertificado);
    app.post("/academico.certificado.imagen/:id/:ext"   ,certificacion.addImagenSolicitudCertificado);
    app.get("/academico.certificado.alumno/:id"         ,certificacion.getCertificadoAlumno);
    app.post("/academico.crear.certificado/:id"         ,certificacion.crearCertificado);

    // Actualización de datos
    app.post("/academico.alumno.buscar/:escuela"         ,certificacion.getAlumnoMatricula);
    app.post("/academico.alumno.update/:escuela"         ,certificacion.updateAlumnoMatricula);
    // Genrear copia de 14 nvieles
    app.post("/academico.copia.niveles/:escuela"         ,certificacion.generarCopiaNiveles);
    app.post("/academico.copia.niveles2/:escuela"        ,certificacion.generarCopiaNiveles2);
    app.post("/academico.copia.cambridge/:escuela"       ,certificacion.generarCopiaCambridge);

    // ejemplos de get, put, delete, post
    // app.get("/academico.certificacion.all",                     certificacion.getAccesos);
    // app.post("/academico.certificacion.add",                    certificacion.addAcceso); 
    //app.delete("/accesos.delete/:idacceso_ticket", accesos.delAccesos);
};

