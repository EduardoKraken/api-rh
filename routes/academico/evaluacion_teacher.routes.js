module.exports = app => {
    const evaluacion_teacher = require('../../controllers/academico/evaluacion_teacher.controllers') // --> ADDED THIS
    app.post("/academico.evaluacion.teachers",	  		      evaluacion_teacher.getEvaluacionClaseTeacher); 
    app.post("/academico.evaluacion.pregunta",	  		      evaluacion_teacher.addRespuestaEvalaucionClase); 
    app.post("/academico.evaluacion.teacher.calificacion",	evaluacion_teacher.addCalificacionEvalaucionClase); 

    // ejemplos de get, put, delete, post
    // app.get("/academico.certificacion.all",                     certificacion.getAccesos);
    // app.post("/academico.certificacion.add",                    certificacion.addAcceso); 
    //app.delete("/accesos.delete/:idacceso_ticket", accesos.delAccesos);
};

