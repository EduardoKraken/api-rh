module.exports = app => {
    const grupo_teachers = require('../../controllers/academico/grupo_teachers.controllers') // --> ADDED THIS
    app.get("/academico.grupo_teachers.get/:idciclo/:idciclo2",          grupo_teachers.getGrupoTeachers); // CREAR UN NUEVO GRUPO
    app.get("/academico.teachers.activos",                               grupo_teachers.getTeachersActivos); // CREAR UN NUEVO GRUPO
    app.post("/academico.add.grupoteacher",                              grupo_teachers.addGrupoTeacher); // CREAR UN NUEVO GRUPO


    // ejemplos de get, put, delete, post
    // app.get("/academico.grupo_teachers.all",                     grupo_teachers.getAccesos);
    // app.put("/academico.grupo_teachers.update/:idacceso_ticket", grupo_teachers.putAccesos);
    // app.post("/academico.grupo_teachers.add",                    grupo_teachers.addAcceso); 
    //app.delete("/accesos.delete/:idacceso_ticket", accesos.delAccesos);
};