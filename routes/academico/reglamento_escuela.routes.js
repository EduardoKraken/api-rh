module.exports = app => {
    const reglamento_escuela = require('../../controllers/academico/reglamento_escuela.controllers') // --> ADDED THIS
    app.get("/reglamento.escuela.get.regla",    reglamento_escuela.getRegla); 
    app.post('/reglamento.escuela.add.regla',   reglamento_escuela.addRegla); 
    app.put('/reglamento.escuela.update/:id' ,  reglamento_escuela.updateRegla); 


  };