module.exports = app => {
  const revisionexamenes = require('../../controllers/academico/revisionexamenes.controllers') // --> ADDED THIS
  app.get("/examenes.revision/:id"          ,    revisionexamenes.getExamenesRevision); 
  app.get("/examenes.niveles"               ,    revisionexamenes.getNivelesEvaluacion); 
  app.post("/examenes.asignarnivel"         ,    revisionexamenes.asignarNivel); 
  app.get("/examenes.oprespuestas"          ,    revisionexamenes.getOpcionRespuestasMalas); 
  app.post("/examenes.eliminar"             ,    revisionexamenes.eliminarExamenUbicacion); 
  // app.post("/mensaje.maestro.update.mensaje",     revisionexamenes.updateMensaje); 
  // app.post("/mensaje.maestro.update.mensaje.rechazo",     revisionexamenes.updateMensajeRechazo); 
  // app.post("/mensaje.maestro.update.kardex",     revisionexamenes.updateKardex); 

};

//ANGEL RODRIGUEZ -- TODO