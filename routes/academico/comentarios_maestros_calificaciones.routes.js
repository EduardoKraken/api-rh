module.exports = app => {
  const comentarios_maestros_calificaciones = require('../../controllers/academico/comentarios_maestros_calificaciones.controllers') // --> ADDED THIS
  app.get("/mensaje.maestro.get.mensaje",                comentarios_maestros_calificaciones.getMensaje);
  app.post("/mensaje.maestro.update.mensaje",            comentarios_maestros_calificaciones.updateMensaje);
  app.post("/mensaje.maestro.update.mensaje.rechazo",    comentarios_maestros_calificaciones.updateMensajeRechazo);
  app.post("/mensaje.maestro.update.kardex",             comentarios_maestros_calificaciones.updateKardex);
  //Nuevos
  app.post("/consultar.mensajes.rechazo",                comentarios_maestros_calificaciones.consultarMensajesRechazo);
  app.post("/update.incidencias.mensaje.respuesta",      comentarios_maestros_calificaciones.addIncidenciasMensajeRespuesta);
  app.post("/update.estatus.incidencias",                comentarios_maestros_calificaciones.updateEstatusIncidencias); 

};

//ANGEL RODRIGUEZ -- TODO