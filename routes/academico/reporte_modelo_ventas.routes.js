
module.exports = app => {
    const reporte_modelo_ventas = require('../../controllers/academico/reporte_modelo_ventas.controllers') // --> ADDED THIS
    app.post("/reporte_modelo_ventas.semanal.ventas"                       , reporte_modelo_ventas.reporteSemanalVentas);
    app.post('/reporte_modelo_ventas.marketing.contactos'                  , reporte_modelo_ventas.graficasContacto); 
    app.post('/reporte_modelo_ventas.reporte'                              , reporte_modelo_ventas.reporte);

  };  