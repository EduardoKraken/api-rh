module.exports = app => {
  const formaspago = require('../../controllers/catalogos/formaspago.controllers') // const para utilizar nuestro controllador--> ADDED THIS
  app.get("/formaspago.list"           , formaspago.getFormasPago);
  app.get("/formaspago.activos"        , formaspago.getFormasPagoActivos);
  app.post("/formaspago.add"           , formaspago.addFormasPago); //AGREGAR 
  app.put("/formaspago.update/:id"     , formaspago.updateFormasPago); //actualizar 
  app.put("/formaspago.eliminar/:id"   , formaspago.deleteFormasPago); //actualizar 
};