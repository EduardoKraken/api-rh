module.exports = app => {
  const entradas = require('../../controllers/catalogos/entradas.controllers') // const para utilizar nuestro controllador--> ADDED THIS
  /*
    TODAS LAS APIS DE AQUÍ SE UTILIZARAN PARA VALIDAR LAS ENTRADAS DE LAS RECEPCIONISTAS
  */ 

  app.get("/validar.entradas",               entradas.getEntradasSucursal);
  app.get("/obtener.horarios.apertura",      entradas.getHorariosApertura);
  app.get("/obtener.trabajadores.usuarios",  entradas.getTrabajadoresUsuarios);
  app.post("/relacion.usuarios",             entradas.crearRelacionUsuarios);
  app.post("/agregar.horarios",              entradas.agregarHorarios);
  app.delete("/horario.apertura.delete/:id", entradas.eliminarHorarioApertura);
  app.delete("/apertura.usuario/:id",        entradas.eliminarUsuarioApertura);
  app.put("/apertura.usuario.update",        entradas.desactivarUsuario);
  app.get("/asistencias.visuales",           entradas.asistenciasVisuales);
  app.get("/asistencias.visuales.teachers",  entradas.asistenciasVisualesTeachers);
  app.get("/asistencias.alumnos",            entradas.validarAsistenciaAlumno);
  app.post("/actualizar.sucursal.apertura",  entradas.actualizarSucursalApertura);
};