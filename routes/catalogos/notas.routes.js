module.exports = app => {
    const notas = require('../../controllers/catalogos/notas.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/notas.all", notas.getNotas); // OBTENER
    app.post("/notas.add", notas.addNota); //AGREGAR 
    app.put("/notas.update/:idNota", notas.updateNota); //actualizar 

};