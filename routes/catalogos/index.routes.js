module.exports = app => {
	require("./notas.routes")(app)
	require("./notificaciones.routes")(app)
	require("./notificaciones_usuario.routes")(app)
	require("./insignias.routes")(app)
	
	require("./tutores.routes")(app)
	require("./clientes.routes")(app)
	require("./prospectos.routes")(app)
	require("./whatsapp.route")(app)
	require("./comentarios.routes")(app)
	require("./citas.routes")(app)
	require("./whatsapp.route")(app)

	// Nuevo ERP -> Viejo ERP
	require("./ciclos.routes")(app)
	require("./horarios.routes")(app)
	require("./niveles.routes")(app)
	require("./salones.routes")(app)
	require("./cursoserp.routes")(app)
	require("./formaspago.routes")(app)
	require("./grupos.routes")(app)
	require("./planteles.routes")(app)
	require("./descuentoprontopago.routes")(app)

	// Obtener los horarios de apertura
	require("./entradas.routes")(app)

	// Permisos
	require("./permisos.routes")(app)

	require("./usuarioserp.routes")(app)
	require("./trabajadoreserp.routes")(app)
};