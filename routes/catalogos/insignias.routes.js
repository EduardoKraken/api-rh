module.exports = app => {
    const insignias = require('../../controllers/catalogos/insignias.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/insignias.all", insignias.getInsignias); // OBTENER
    app.post("/insignias.add", insignias.addInsignia); //AGREGAR 
    app.put("/insignias.update/:idInsignia", insignias.updateInsignia); //actualizar 

};