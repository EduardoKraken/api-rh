module.exports = app => {
    const notificaciones_usuario = require('../../controllers/catalogos/notificaciones_usuario.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/notificaciones_usuario.all", notificaciones_usuario.getNotificacionesUsuario); // OBTENER
    app.post("/notificaciones_usuario.add", notificaciones_usuario.addNotificacionUsuario); //AGREGAR 
    app.put("/notificaciones_usuario.update/:idNotificacionUsuario", notificaciones_usuario.updateNotificacionUsuario); //actualizar 

};