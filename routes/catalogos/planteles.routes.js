module.exports = app => {
  const planteles = require('../../controllers/catalogos/planteles.controllers') // const para utilizar nuestro controllador--> ADDED THIS
  // app.get("/horarios.list"           , horarios.getHorarios);
  app.get("/planteles.activos"          , planteles.getPlantelesActivos);
  app.get("/planteles.oficiales"        , planteles.getPlantelesOficiales);
  // app.post("/horarios.add"           , horarios.addHorario); //AGREGAR 
  // app.put("/horarios.update/:id"     , horarios.updateHorario); //actualizar 
  // app.put("/horarios.eliminar/:id"   , horarios.deleteHorario); //actualizar 
};