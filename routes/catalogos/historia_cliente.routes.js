module.exports = (app) => {
  const historialCliente = require("../../controllers/catalogos/materiales.controllers"); // const para utilizar nuestro controllador--> ADDED THIS
  app.get("/historia-cliente.all/:id", historialCliente.getHistoriaCliente);
  app.post("/materiales.add", historialCliente.addMaterial); //AGREGAR
  app.put("/materiales.update/:idMaterial", historialCliente.updateMaterial); //actualizar
};
