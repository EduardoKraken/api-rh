module.exports = app => {
  const permisos = require('../../controllers/catalogos/permisos.controllers')
  app.get("/menu.get"           , permisos.getMenu); //Obtener Menu
  app.get("/menu.get.activos"   , permisos.getMenuActivos); //Obtener Menu
  app.post("/menu.add"          , permisos.addMenu); //Agregar Menu
  app.put("/menu.eliminar"      , permisos.deleteMenu); //Eliminar Menu
  app.put("/menu.actualizar"    , permisos.updateMenu); //Actualizar Menu

  app.get("/submenu.get"         , permisos.getSubmenu); //Obtener Submenu
  app.get("/submenu.get.activos" , permisos.getSubmenuActivos); //Obtener Submenu
  app.post("/submenu.add"        , permisos.addSubmenu); //Agregar Submenu
  app.put("/submenu.eliminar"    , permisos.deleteSubmenu); //Eliminar Submenu
  app.put("/submenu.actualizar"  , permisos.updateSubmenu); //Actualizar SubMenu

  app.get("/actions.get"         , permisos.getActions); //Obtener Actions
  app.get("/actions.get.activos" , permisos.getActionsActivos); //Obtener Actions
  app.post("/actions.add"        , permisos.addActions); //Agregar Actions
  app.put("/actions.eliminar"    , permisos.deleteActions); //Eliminar Actions


  app.post("/permisos.puestos.add"       , permisos.addPermisos); //Agregar Actions
  app.get("/permisos.puestos.get/:id"    , permisos.getPermisosPuesto); //Agregar Actions

  app.put("/actions.actualizar"  , permisos.updateActions); //Actualizar Actions
};