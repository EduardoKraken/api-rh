module.exports = app => {
    const trabajadoreserp = require('../../controllers/catalogos/trabajadoreserp.controllers') // --> ADDED THIS
    app.get("/trabajadores.erp.get.trabajadoreserp",               trabajadoreserp.getTrabajadoresERP); 
    app.get("/trabajadores.erp.get.puestoserp",                    trabajadoreserp.getPuestosERP); 
    app.get("/trabajadores.erp.get.jornadalaboralerp",             trabajadoreserp.getJornadaLaboralERP); 
    app.get("/trabajadores.erp.get.departamentoerp",               trabajadoreserp.getDepartamentoERP); 
    app.get("/trabajadores.erp.get.tipotrabajadorerp",             trabajadoreserp.getTipoTrabajadorERP);
    app.get("/trabajadores.erp.get.nivelerp",                      trabajadoreserp.getNivelERP);
    app.get("/trabajadores.erp.get.cursoerp",                      trabajadoreserp.getCursoERP);
    app.get("/trabajadores.erp.get.perfilesERP",                   trabajadoreserp.getPerfilesERP); 
    app.get("/trabajadores.erp.get.trabajadoresERP",               trabajadoreserp.getTrabajadoresERP); 
    app.post("/trabajadores.erp.update.trabajadorerp",             trabajadoreserp.updateTrabajadorERP); 
    app.post("/trabajadores.erp.add.trabajadoreserp",              trabajadoreserp.addTrabajadorERP); //AGREGAR 
    app.delete("/trabajadores.erp.delete.usuariosERP/:id_usuario", trabajadoreserp.deleteUsuarioERP);
    

  };

//ANGEL RODRIGUEZ -- TODO