module.exports = (app) => {
  const clientes = require("../../controllers/catalogos/clientes.controllers"); // const para utilizar nuestro controllador--> ADDED THIS
  app.get("/clientes.all", clientes.getClientes);
  app.get("/clientes.id/:id", clientes.getCliente); // OBTENER
  app.post("/clientes.add", clientes.addCliente); //AGREGAR
  app.put("/clientes.update/:idCliente", clientes.updateCliente); //actualizar
  app.delete("/clientes.delete/:idCliente", clientes.deleteCliente); //actualizar
};
