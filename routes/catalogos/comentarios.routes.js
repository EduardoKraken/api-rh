module.exports = (app) => {
  const comentarios = require("../../controllers/catalogos/comentarios.controller"); // const para utilizar nuestro controllador--> ADDED THIS
  //   app.get("/tutores.all", tutores.getTutores);
  app.get("/comentarios.id/:prospectoId", comentarios.getComentarios); // OBTENER
  app.post("/comentarios.add", comentarios.addComentario); //AGREGAR
  //   app.put("/tutores.update/:idTutor", tutores.updateTutor); //actualizar
  //   app.delete("/tutores.delete/:idTutor", tutores.deleteTutor); //actualizar
};
