module.exports = app => {
  const niveles = require('../../controllers/catalogos/niveles.controllers') // const para utilizar nuestro controllador--> ADDED THIS
  app.get("/niveles.list"           , niveles.getNiveles);
  app.get("/niveles.activos"        , niveles.getNivelesActivos);
  app.post("/niveles.add"           , niveles.addNivel); //AGREGAR 
  app.put("/niveles.update/:id"     , niveles.updateNivel); //actualizar 
  app.put("/niveles.eliminar/:id"   , niveles.deleteNivel); //actualizar 
};