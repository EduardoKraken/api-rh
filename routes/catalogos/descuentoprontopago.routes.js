module.exports = app => {
  const descProntoPago = require('../../controllers/catalogos/descuentoprontopago.controllers') // const para utilizar nuestro controllador--> ADDED THIS
  app.get("/descprontopago.grupo/:id"            , descProntoPago.getDescuentoProntoPagoGrupo);
  app.post("/descprontopago.grupo.add"           , descProntoPago.addDescuentoGrupo); //AGREGAR 
  app.put("/descprontopago.grupo.eliminar/:id"   , descProntoPago.deleteDescuentoGrupo); //actualizar 
};