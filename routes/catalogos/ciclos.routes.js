module.exports = app => {
    const ciclos = require('../../controllers/catalogos/ciclos.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/ciclos.list"           , ciclos.getCiclos);
    app.get("/ciclos.activos.erp"    , ciclos.getCiclosActivos);
    app.post("/ciclos.add"           , ciclos.addCiclo); //AGREGAR 
    app.put("/ciclos.update/:id"     , ciclos.updateCiclo); //actualizar 
    app.put("/ciclos.eliminar/:id"   , ciclos.deleteCiclo); //actualizar 
    app.put("/ciclos.relacionar/:id" , ciclos.relacionarCiclo); //actualizar 

};