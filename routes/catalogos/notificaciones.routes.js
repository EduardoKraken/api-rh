module.exports = app => {
    const notificaciones = require('../../controllers/catalogos/notificaciones.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/notificaciones.all"                   , notificaciones.getNotificaciones); // OBTENER
    app.post("/notificaciones.add"                  , notificaciones.addNotificacion); //AGREGAR 
    app.put("/notificaciones.update/:idNotificacion", notificaciones.updateNotificacion); //actualizar 
};