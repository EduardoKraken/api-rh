module.exports = app => {
  const grupos = require('../../controllers/catalogos/grupos.controllers') // const para utilizar nuestro controllador--> ADDED THIS
  app.get("/grupos.list"           , grupos.getGrupos);
  app.get("/grupos.activos"        , grupos.getGruposActivos);
  app.post("/grupos.add"           , grupos.addGrupo); //AGREGAR 
  app.put("/grupos.update/:id"     , grupos.updateGrupo); //actualizar 
  app.put("/grupos.eliminar/:id"   , grupos.deleteGrupo); //actualizar 
  app.get("/grupos.alumnos/:id"    , grupos.getGruposAlumnos); //actualizar 
  app.get("/grupos.teachers/:id"   , grupos.getGrupoTeachers); //actualizar 

  app.get("/grupos.habiles/:id"    , grupos.getGruposHabiles);

};