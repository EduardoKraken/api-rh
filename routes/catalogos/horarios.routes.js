module.exports = app => {
  const horarios = require('../../controllers/catalogos/horarios.controllers') // const para utilizar nuestro controllador--> ADDED THIS
  app.get("/horarios.list"           , horarios.getHorarios);
  app.get("/horarios.activos"        , horarios.getHorariosActivos);
  app.post("/horarios.add"           , horarios.addHorario); //AGREGAR 
  app.put("/horarios.update/:id"     , horarios.updateHorario); //actualizar 
  app.put("/horarios.eliminar/:id"   , horarios.deleteHorario); //actualizar 
};