module.exports = app => {
  const cursoserp = require('../../controllers/catalogos/cursoserp.controllers') // const para utilizar nuestro controllador--> ADDED THIS
  app.get("/cursoserp.list"           , cursoserp.getCursoserp);
  app.get("/cursoserp.activos"        , cursoserp.getCursoserpActivos);
  app.post("/cursoserp.add"           , cursoserp.addCursoErp); //AGREGAR 
  app.put("/cursoserp.update/:id"     , cursoserp.updateCursoErp); //actualizar 
  app.put("/cursoserp.eliminar/:id"   , cursoserp.deleteCursoErp); //actualizar 
};