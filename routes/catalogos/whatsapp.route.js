module.exports = (app) => {
  const wpp = require("../../controllers/whatsapp.controllers"); // const para utilizar nuestro controllador--> ADDED THIS
  app.get("/wpp.message.id/:id", wpp.getAllMessage);
  // app.pot("/wpp.newchat/:", wpp.getAllMessage);
  app.get("/wpp.chats.all/:idEmpleado", wpp.getAllChats);
};
