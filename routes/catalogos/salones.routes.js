module.exports = app => {
  const salones = require('../../controllers/catalogos/salones.controllers') // const para utilizar nuestro controllador--> ADDED THIS
  app.get("/salones.list"           , salones.getSalones);
  app.get("/salones.activos"        , salones.getSalonesActivos);
  app.post("/salones.add"           , salones.addSalon); //AGREGAR 
  app.put("/salones.update/:id"     , salones.updateSalon); //actualizar 
  app.put("/salones.eliminar/:id"   , salones.deleteSalon); //actualizar 
};