module.exports = (app) => {
  const citas = require("../../controllers/catalogos/citas.controllers"); // const para utilizar nuestro controllador--> ADDED THIS
  app.get("/citas.all/:idEmpleado", citas.getCitasEmpleado); //Obtener todas las citas de los empleados
  app.get("/citas.prospecto.all/:idProspecto", citas.getCitasProspecto); //Obtener todas las citas de los empleados
  app.get("/citas.id/:id", citas.getCita); // OBTENER una cita por id
  app.post("/citas.add", citas.addCita); //AGREGAR una cita
  app.put("/citas.update/:id", citas.updateCita); //actualizar
  app.put("/prospectos.cancel/:idCita", citas.cancelCita); //Cancelar cita
};
