module.exports = (app) => {
  const tutores = require("../../controllers/catalogos/tutores.controllers"); // const para utilizar nuestro controllador--> ADDED THIS
  app.get("/tutores.all", tutores.getTutores);
  app.get("/tutores.id/:id", tutores.getTutor); // OBTENER
  app.post("/tutores.add", tutores.addTutor); //AGREGAR
  app.put("/tutores.update/:idTutor", tutores.updateTutor); //actualizar
  app.delete("/tutores.delete/:idTutor", tutores.deleteTutor); //actualizar
};
