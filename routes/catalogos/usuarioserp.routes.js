module.exports = app => {
    const usuarioserp = require('../../controllers/catalogos/usuarioserp.controllers') // --> ADDED THIS
    app.get("/usuarios.erp.get.usuariosERP",      usuarioserp.getUsuariosERP); 
    app.get("/usuarios.erp.get.perfilesERP",      usuarioserp.getPerfilesERP); 
    app.get("/usuarios.erp.get.trabajadoresERP",  usuarioserp.getTrabajadoresERP); 
    app.get("/usuarios.erp.get.IdUsuarioERP",  usuarioserp.getIdUsuarioERP); 
    app.post("/usuarios.erp.update.usuariosERP",  usuarioserp.updateUsuarioERP); 
    app.post("/usuarios.erp.update.perfilusuariosERP",  usuarioserp.updatePerfilUsuario); 
    app.post("/usuarios.erp.add.usuariosERP",     usuarioserp.addUsuarioERP); //AGREGAR 
    app.post("/usuarios.erp.add.usuarioperfilERP",     usuarioserp.addPerfilUsuarioERP); //AGREGAR 
    app.delete("/usuarioserp.erp.delete.usuariosERP/:id_usuario", usuarioserp.deleteUsuarioERP);
    

  };

//ANGEL RODRIGUEZ -- TODO