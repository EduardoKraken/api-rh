module.exports = app => {
    const permiso_usuario = require('../controllers/permiso_usuario.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/permisos_usuario.all"                       , permiso_usuario.getPermisoUsuario); // OBTENER
    app.post("/permisos_usuario.add"                      , permiso_usuario.addPermisoUsuario); //AGREGAR USUARIO
    app.put("/permisos_usuario.update/:idpermisos_usuario", permiso_usuario.updatePermisoUsuario); //actualizar usuario

};