module.exports = app => {
    const zonas = require('../controllers/zonas.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/zonas.all", zonas.all_zonas); // OBTENER
    app.post("/zonas.add", zonas.add_zonas); //AGREGAR USUARIO
    app.put("/zonas.update/:idzonas", zonas.update_zona); //actualizar usuario

};