module.exports = app => {
  const usuarios = require('../controllers/usuarios.controllers') // const para utilizar nuestro controllador--> ADDED THIS
  app.get("/usuarios.all", usuarios.all_usuarios); // OBTENER

  // 
  app.get("/usuarios.id/:idusuarios"     , usuarios.getUsuarioId); //
  app.get('/usuarios.id.info/:id'        , usuarios.getUsuarioInfo)

  app.post("/usuarios.add"               , usuarios.add_usuarios); //AGREGAR USUARIO
  app.put("/usuarios.update/:idusuarios" , usuarios.update_usuario); //actualizar usuario

  app.get("/usuarios.all.erp"            , usuarios.all_usuariosERP); // OBTENER
  app.get("/usuarios.all.capa/:id"       , usuarios.all_usuariosCAPA); // OBTENER
  app.get("/usuario.id/:id"              , usuarios.usuario_id); //Obtener informacion de un usuario del erp
  app.get("/usuarios.puesto.all/:id"     , usuarios.getusuariosPuesto); //obtener usuarios por puesto
  app.get("/usuarios.depto.all/:id"      , usuarios.getusuariosDepto); //obtener usuarios por depto

  // Niveles de los usuarios
  app.get("/usuarios.niveles.all"        , usuarios.getUsuariosNiveles);
  app.post("/usuarios.niveles.add"       , usuarios.addUsuariosNiveles);
  app.put("/usuarios.niveles.update/:id" , usuarios.updateUsuariosNiveles);

  // Capacitacion
  app.post("/usuarios.data"              , usuarios.loginCapa);

  app.get("/usuarios.activos"            , usuarios.getUsuariosActivos);

  app.post("/usuarios.entradas"          , usuarios.getEntradasEmpleados);

  /*
    Ver si el teacher ya llego y generar un codigo de una letra y 4 numeros
  */

  app.post("/asistencia.teacher"         , usuarios.generarCodigoAsistenciaTeacher)


  /********** Archivos **********/

  // Archivos
  app.post("/usuarios.add_archivo"       , usuarios.addArchivo);    // Agregar archivo
  app.put("/usuarios.update_archivo/:id" , usuarios.updateArchivo); // Actualizar archivo
  app.get("/usuarios.get_archivo/:id"    , usuarios.getArchivo);    // Obtener archivo
  app.get("/usuarios.get_archivos"       , usuarios.getArchivos);   // Obtener archivos

  // Archivos Empleados
  app.post("/usuarios.add_archivo_empleado"       , usuarios.addArchivoEmpleado);    // Agregar archivo empleado
  app.put("/usuarios.update_archivo_empleado/:id" , usuarios.updateArchivoEmpleado); // Actualizar archivo empleado
  app.get("/usuarios.get_archivo_empleado/:id"    , usuarios.getArchivoEmpleado);    // Obtener archivo empleado
  app.post("/usuarios.get_archivos_empleado"      , usuarios.getArchivosEmpleado);   // Obtener archivos empleado

  // Archivos en el servidor
  app.post("/usuarios.add_archivo_servidor"       , usuarios.addArchivoServidor);     // Agregar archivo al servidor
  app.post("/usuarios.delete_archivo_servidor"    , usuarios.deleteArchivoServidor);  // Eliminar archivo del servidor
  app.get("/usuarios.get_archivos_servidor"       , usuarios.getArchivoServidor);     // Visualizar los archivos de la carpeta del servidor

  app.post("/usuarios.download_archivos"          , usuarios.downloadArchivosServidor);  // Descargar archivos seleccionados .zip
  
};