module.exports = app => {
  const asistencia_teacher = require('../../controllers/registro_asistencias/asistencia_teacher.controllers') 
  app.post("/asistencia_teacher.validar",   asistencia_teacher.validarAsistenciaTeacher);

  // Obtener todas las clases, asistencias y maestros del día de hoy para ver que todo este bine
  app.get('/asistencia_teacher.no.asistencias', asistencia_teacher.getNoAsistencias)
};