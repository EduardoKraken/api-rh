module.exports = app => {
  const preguntasfre = require('../../controllers/preguntasfre/preguntasfre.controllers') // --> ADDED THIS
  app.post("/preguntasfre.add",               preguntasfre.addPregunta); 
  app.put("/preguntasfre.responder",          preguntasfre.responderPreguntaUsuario); 
  app.post("/preguntasfre.add.frecuente",     preguntasfre.addPreguntaFrecuente); 
  app.get("/preguntasfre.mis.preguntas/:id",  preguntasfre.misPreguntas); 
  app.get("/preguntasfre.list.frecuentes",    preguntasfre.getPreguntasFrecuentes); 
  app.put("/preguntasfre.update/:id",         preguntasfre.updatePreguntaFrecuente); 
  app.put("/preguntasfre.eliminar/:id",       preguntasfre.eliminarFrecuente); 
  app.get("/preguntasfre.preguntas",          preguntasfre.getPreguntasUsuario); 
  // app.get("/preguntasfre.alumnos",            preguntasfre.getAlumnosAll); 
};

