module.exports = app => {
  const exci = require('../../controllers/exci/exci.controllers') // --> ADDED THIS
  app.post("/enviar.recibo",                 exci.enviarRecibo); 
  
  app.get("/exci.registros",                 exci.getRegistrosExci); 
  app.get("/exci.alumnos",                   exci.getAlumnosAll); 
  app.put("/exci.alumno.matricula",          exci.addMatriculaAlumno); 
  app.put("/exci.alumno.seguimiento",        exci.addSeguimientoAlumnoExci); 
  app.post("/exci.alumno.adeudo",            exci.sendRecordatorioPago); 
  app.post("/exci.update.correo",            exci.updateCorreoExci); 
  app.post("/exci.enviar.matricula",         exci.sendMatricula); 
  app.post("/exci.enviar.zoom",              exci.sendZoom); 
  app.post("/exci.enviar.recordatorio.pago", exci.sendRecordatorioPagoIndividual); 
  app.get("/exci.becas.ingles",              exci.getBecasExciIngles); 
  app.post("/exci.enviar.liga.medio.pago"  , exci.sendLigaMedioPagoArriba); 
  app.get("/exci.grupos.acitivos",           exci.getExciGruposCiclo)
  app.post("/exci.grupos.agregar",           exci.agregarGrupoAlumno)
  app.post("/exci.solicitar.beca",           exci.solicitarBeca)
  app.post("/exci.remarketing",              exci.enviarRemarketing)
  app.post("/exci.pruebas",                  exci.pruebasMasivas)
};

