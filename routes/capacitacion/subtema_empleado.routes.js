module.exports = app => {
    const subtema_empleado = require('../../controllers/capacitacion/subtema_empleado.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/subtema_empleado.all", subtema_empleado.getSubtemaEmpleados); // OBTENER
    app.post("/subtema_empleado.add", subtema_empleado.addSubtemaEmpleado); //AGREGAR 
    app.put("/subtema_empleado.update/:idSubtemaEmpleado", subtema_empleado.updateSubtemaEmpleado); //actualizar 

};