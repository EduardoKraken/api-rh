module.exports = app => {
    const tipo_materiales = require('../../controllers/capacitacion/tipo_materiales.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/tipo_materiales.all", tipo_materiales.getTipoMateriales); // OBTENER
    app.post("/tipo_materiales.add", tipo_materiales.addTipoMaterial); //AGREGAR 
    app.put("/tipo_materiales.update/:idTipoMaterial", tipo_materiales.updateTipoMaterial); //actualizar 

};