module.exports = app => {
    const temas = require('../../controllers/capacitacion/temas.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/temas.all", temas.getTemas); // OBTENER
    app.get("/temas.cursos.all/:id", temas.getTemasCurso); // OBTENER POR CURSO ESPECIFICO
    app.post("/temas.add", temas.addTema); //AGREGAR 
    app.put("/temas.update/:idTema", temas.updateTema); //actualizar 

    app.get("/temas.cursos.id/:id", temas.getTemasxCurso); // OBTENER POR CURSO ESPECIFICO

};