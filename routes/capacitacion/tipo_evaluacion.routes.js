module.exports = app => {
    const tipo_evaluacion = require('../../controllers/capacitacion/tipo_evaluacion.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/tipo_evaluacion.all", tipo_evaluacion.getTipoEvaluacion); // OBTENER
    app.post("/tipo_evaluacion.add", tipo_evaluacion.addTipoEvaluacion); //AGREGAR 
    app.put("/tipo_evaluacion.update/:idTipoEvaluacion", tipo_evaluacion.updateTipoEvaluacion); //actualizar 

};