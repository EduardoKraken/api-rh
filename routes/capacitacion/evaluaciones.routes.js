module.exports = app => {
    const evaluaciones = require('../../controllers/capacitacion/evaluaciones.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/evaluaciones.all",                  evaluaciones.getEvaluaciones); // OBTENER
    app.get("/evaluaciones.subtemas.all/:id",     evaluaciones.getEvaluacionesSubtema);
    app.post("/evaluaciones.add",                 evaluaciones.addEvaluacion); //AGREGAR 
    app.put("/evaluaciones.update/:idEvaluacion", evaluaciones.updateEvaluacion); //actualizar 


    // AQUÍ VAMOS A PONER EL SEGUIMIENTO Y PANEL DE TEACHERS
    app.get('/panel.capacitacion.teacher',        evaluaciones.getPanelCapacitacionTeacher )
    app.get('/panel.capacitacion.usuario',        evaluaciones.getPanelCapacitacionUsuario )
};