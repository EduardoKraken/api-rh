module.exports = app => {
    const calificaciones = require('../../controllers/capacitacion/calificaciones.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/calificaciones.all", calificaciones.getCalificaciones); // OBTENER
    app.post("/calificaciones.add", calificaciones.addCalificacion); //AGREGAR 
    app.put("/calificaciones.update/:idCalificacion", calificaciones.updateCalificacion); //actualizar 
    app.post("/calificaciones.intentos.all", calificaciones.getIntentosCalificacion); //obtenemos los intentos de las veces que ha respondido la evaluaciones

    app.get("/calificaciones.all.empleados", calificaciones.getCalificacionesEmpleados); // OBTENER TODAS LAS CALIFICACIONES DE LOS EMPLEADOS CON ASIGNACIONES
};