module.exports = app => {
  const capacitador = require('../../controllers/capacitacion/capacitador.controllers')
  app.post("/capacitador.mensaje.add",      capacitador.agregarMensajeCapacitador); 
  app.get("/capacitador.mensajes/:id",      capacitador.getMensajesCapacitador); 
  app.post("/capacitador.mensajes.update",  capacitador.updateMensajeCapacitador);
  app.get("/usuario.mensaje/:id",           capacitador.getMensajesUsuario); 



  // app.get("/evaluaciones.all", capacitador.getEvaluaciones); // OBTENER
  // app.get("/evaluaciones.subtemas.all/:id", capacitador.getEvaluacionesSubtema);
  // app.put("/evaluaciones.update/:idEvaluacion", capacitador.updateEvaluacion); //actualizar 
};