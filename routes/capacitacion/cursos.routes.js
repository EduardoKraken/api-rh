module.exports = app => {
  const cursos = require('../../controllers/capacitacion/cursos.controllers') // const para utilizar nuestro controllador--> ADDED THIS
  app.get("/cursos.all", cursos.getCursos);
  app.get("/cursos.id/:id", cursos.getCursoId); // OBTENER
  app.post("/cursos.add", cursos.addCurso); //AGREGAR 
  app.put("/cursos.update/:idCurso", cursos.updateCurso); //actualizar 
};