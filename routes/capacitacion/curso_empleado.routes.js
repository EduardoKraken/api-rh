module.exports = app => {
    const cursoempleado = require('../../controllers/capacitacion/curso_empleado.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/curso_empleado.all", cursoempleado.getCursoEmpleado); // OBTENER
    app.get("/curso_empleado.usuario/:id", cursoempleado.getCursoEmpleadoId); // OBTENER
    app.post("/curso_empleado.add", cursoempleado.addCursoEmpleado); //AGREGAR 
    app.put("/curso_empleado.update/:idCursoEmpleado", cursoempleado.updateCursoEmpleado); //actualizar getCursosEmpleado

    app.post("/curso_empleado.curso.empleado", cursoempleado.getCursoIdEmpleadoId); //buscar si ya existe 
    app.post("/inscripcion.curso", cursoempleado.inscribirTeacherCurso); //buscar si ya existe 
    app.get("/inscripcion.curso.teachers", cursoempleado.getInscribirTeacherCurso); //buscar si ya existe 
};