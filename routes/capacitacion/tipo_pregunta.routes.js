module.exports = app => {
    const tipo_preguntas = require('../../controllers/capacitacion/tipo_pregunta.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/tipo_preguntas.all", tipo_preguntas.getTipoPreguntas); // OBTENER
    app.post("/tipo_preguntas.add", tipo_preguntas.addTipoPregunta); //AGREGAR 
    app.put("/tipo_preguntas.update/:idTipoPregunta", tipo_preguntas.updateTipoPregunta); //actualizar 

};