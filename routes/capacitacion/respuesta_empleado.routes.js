module.exports = app => {
    const respuestas = require('../../controllers/capacitacion/respuesta_empleado.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/respuestas.all", respuestas.getRespuestas); // OBTENER
    app.post("/respuestas.add", respuestas.addRespuesta); //AGREGAR 
    app.put("/respuestas.update/:idRespuesta", respuestas.updateRespuesta); //actualizar 

};