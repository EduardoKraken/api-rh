module.exports = app => {
    const asignacion_curso = require('../../controllers/capacitacion/asignacion_curso.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/asignacion_cursos.all", asignacion_curso.getAsignacion); // OBTENER
    app.post("/asignacion_cursos.add", asignacion_curso.addAsignacion); //AGREGAR 
    app.put("/asignacion_cursos.update/:id", asignacion_curso.updateAsignacion); //actualizar 

    app.post("/asignacion_cursos.existe", asignacion_curso.getAsignacionExiste); //AGREGAR 


};