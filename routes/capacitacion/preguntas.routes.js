module.exports = app => {
    const preguntas = require('../../controllers/capacitacion/preguntas.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/preguntas.all", preguntas.getPreguntas); // OBTENER
    app.get("/preguntas.evaluaciones.all/:id", preguntas.getPreguntasEvaluacion); // OBTENER
    app.get("/preguntas.opcion.all/:id", preguntas.getPreguntasOpcion);
    app.post("/preguntas.add", preguntas.addPregunta); //AGREGAR 
    app.put("/preguntas.update/:idPregunta", preguntas.updatePregunta); //actualizar 

};