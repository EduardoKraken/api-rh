module.exports = app => {
    const pregunta_multiple = require('../../controllers/capacitacion/pregunta_multiple.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/pregunta_multiple.all", pregunta_multiple.getPreguntaMultiple); // OBTENER
    app.get("/preguntas_opcion_multiple.all/:id", pregunta_multiple.getOpcionMultiplePregunta); // OBTENER
    app.post("/pregunta_multiple.add", pregunta_multiple.addPreguntaMultiple); //AGREGAR 
    app.put("/pregunta_multiple.update/:idPreguntaMultiple", pregunta_multiple.updatePreguntaMultiple); //actualizar 

};