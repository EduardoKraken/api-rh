module.exports = app => {
    const materiales = require('../../controllers/capacitacion/materiales.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/materiales.all",                    materiales.getMateriales); // OBTENER
    app.get("/materiales.subtemas.all/:id",       materiales.getMaterialesSubtema);
    app.post("/materiales.add",                   materiales.addMaterial); //AGREGAR 
    app.put("/materiales.update/:idMaterial",     materiales.updateMaterial); //actualizar 
    app.put("/materiales.deleted/:idMaterial",    materiales.deleteMaterial); //actualizar 
    app.post("/materiales.comentarios",           materiales.getComentariosMaterial);
    app.post("/materiales.comentarios.all",       materiales.getComentariosMaterialAll);
    app.post("/materiales.comentarios.add",       materiales.addComentariosMaterial);

    app.get("/materiales.id/:id",                 materiales.getMaterialId); // OBTENER
    app.post("/capacitacion.materiale.add/:tipo", materiales.agregarMaterial); // agregar un material de capacitación 

    app.get("/materiales.comentarios",            materiales.getComentariosCursos);

};