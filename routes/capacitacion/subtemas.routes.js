module.exports = app => {
    const subtemas = require('../../controllers/capacitacion/subtemas.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/subtemas.all", subtemas.getSubtemas); // OBTENER
    app.get("/subtemas.temas.all/:id", subtemas.getSubtemasTema);
    app.post("/subtemas.add", subtemas.addSubtema); //AGREGAR 
    app.put("/subtemas.update/:idSubtema", subtemas.updateSubtema); //actualizar 

};