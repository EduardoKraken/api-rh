module.exports = app => {
    const puestos = require('../controllers/puestos.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/puestos.all", puestos.all_puestos); // OBTENER
    app.get("/puestos.deptos.all/:id", puestos.getPuestosDepto); // OBTENER
    app.post("/puestos.add", puestos.add_puestos); //AGREGAR USUARIO
    app.put("/puestos.update/:idpuestos", puestos.update_puesto); //actualizar usuario

};