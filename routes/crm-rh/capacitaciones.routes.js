module.exports = app => {

    const capacitaciones = require('../../controllers/crm-rh/capacitaciones.controllers');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia la capacitaciones personal
    */ 
  
    // AGREGAR CAPACITACIONES
    app.post("/capacitaciones.add",          capacitaciones.addCapacitacion);
  
    // ACTUALIZAR CAPACITACIONES
    app.put("/capacitaciones.update/:id",    capacitaciones.updateCapacitacion);
  
    // OBTENER CAPACITACIONES POR ID
    app.get("/capacitaciones.get/:id",       capacitaciones.getCapacitacion);

    // OBTENER CAPACITACIONES
    app.get("/capacitaciones.get",           capacitaciones.getCapacitaciones);

    // OBTENER CAPACITACIONES POR PUESTO
    app.get("/capacitaciones.get_puestos/:id",   capacitaciones.getCapacitacionesPuesto);



    // ----- CAPACITACIONES COMENTARIOS -----


    // AGREGAR CAPACITACIONES_COMENTARIOS
    app.post("/capacitaciones.add_comentario",                capacitaciones.addComentario);

    // ACTUALIZAR CAPACITACIONES_COMENTARIOS
    app.put("/capacitaciones.update_comentario/:id",          capacitaciones.updateComentario);
  
    // OBTENER CAPACITACIONES_COMENTARIOS POR ID_FORMULARIO (PROSPECTO)
    app.get("/capacitaciones.get_comentarios/:idformulario",  capacitaciones.getComentariosProspecto);



    // ----- ARCHIVOS DEL SERVIDOR -----


    // SUBIR ARCHIVO
    app.post("/capacitaciones.add_archivo",       capacitaciones.addArchivo);

    // ELIMINAR ARCHIVO
    app.post("/capacitaciones.delete_archivo",    capacitaciones.deleteArchivo);

    // OBTENER ARCHIVO
    app.get("/capacitaciones.get_archivos",       capacitaciones.getArchivos);

  
  };