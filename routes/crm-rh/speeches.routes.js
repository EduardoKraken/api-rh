module.exports = app => {

    const speeches = require('../../controllers/crm-rh/speeches.controllers');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia el speech
    */ 
  
    // AGREGAR SPEECH
    app.post("/speeches.add",          speeches.addSpeech);
  
    // ACTUALIZAR SPEECH
    app.put("/speeches.update/:id",    speeches.updateSpeech);
    
    // OBTENER SPEECH
    app.get("/speeches.get",           speeches.getSpeeches);
  
    // OBTENER SPEECH POR ID
    app.get("/speeches.get/:id",       speeches.getSpeech);


    // ----- VISTAS EXTRA -----
    
    
    // OBTENER SPEECH POR ID_PUESTO
    app.get("/speeches.get_puesto/:idpuesto",       speeches.getSpeechPuesto);
    
    

    // ----- SPEECH COMENTARIOS -----


    // AGREGAR SPEECH_COMENTARIOS
    app.post("/speeches.add_comentario",                speeches.addComentario);

    // ACTUALIZAR SPEECH_COMENTARIOS
    app.put("/speeches.update_comentario/:id",          speeches.updateComentario);
  
    // OBTENER SPEECH_COMENTARIOS POR ID_FORMULARIO (PROSPECTO)
    app.get("/speeches.get_comentario/:idformulario",   speeches.getProspecto);

  
  };