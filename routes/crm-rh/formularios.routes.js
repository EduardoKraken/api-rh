module.exports = app => {

    const formularios = require('../../controllers/crm-rh/formularios.controllers');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia los formularios
    */ 
  
    // AGREGAR FORMULARIOS
    app.post("/formulario.add",          formularios.addFormularios);
  
    // ACTUALIZAR FORMULARIOS
    app.put("/formulario.update/:id",    formularios.updateFormularios);
    
    // OBTENER TODOS LOS FORMULARIOS
    app.get("/formulario.get",           formularios.getFormularios);
  
    // OBTENER FORMULARIOS POR ID
    app.get("/formulario.get/:id",       formularios.getFormulario);


    // ARCHIVOS DEL SERVIDOR

    // SUBIR ARCHIVO
    app.post("/formulario.add_archivo",       formularios.addArchivo);

    // ELIMINAR ARCHIVO
    app.post("/formulario.delete_archivo",    formularios.deleteArchivo);

    // OBTENER ARCHIVO
    app.get("/formulario.get_archivos",       formularios.getArchivos);



    // VISTAS DE REPORTES

    // OBTENER FORMULARIO POR PUESTOS
    app.get("/formulario.get_puesto/:idpuesto",   formularios.getPuestos);


    // OBTENER FORMULARIO POR ESTATUS
    app.get("/formulario.get_estatus",            formularios.getEstatus);


    // OBTENER FORMULARIO POR FECHAS
    app.post("/formulario.get_fecha",             formularios.getFechas);

    
    // OBTENER FORMULARIO POR ENCARGADA ID
    app.post("/formulario.get_encargada",         formularios.getEncargada);

    
    // OBTENER FORMULARIOS POR ENCARGADAS
    app.get("/formulario.get_encargadas",         formularios.getEncargadas);
    
    // OBTENER FORMULARIO POR ETAPAS
    // app.get("/formulario.get_etapas/:idetapas",   formularios.getEtapas);
  
     app.get("/formulario.get_all_medio_vacante",   formularios.getAllMedioVacante);
    app.get("/formulario.get_all_puestos",         formularios.getPuestosFormularios);
  };