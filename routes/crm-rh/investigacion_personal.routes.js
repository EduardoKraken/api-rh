module.exports = app => {

    const investigacion_personal = require('../../controllers/crm-rh/investigacion_personal.controllers');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia la investigacion_personal personal
    */ 
  
    // AGREGAR INVESTIGACION
    app.post("/investigacion_personal.add",          investigacion_personal.addInvestigacion);
  
    // ACTUALIZAR INVESTIGACION
    app.put("/investigacion_personal.update/:id",    investigacion_personal.updateInvestigacion);
    
    // OBTENER INVESTIGACION
    app.get("/investigacion_personal.get",           investigacion_personal.getInvestigaciones);
  
    // OBTENER INVESTIGACION POR ID
    app.get("/investigacion_personal.get/:id",       investigacion_personal.getInvestigacion);
    
    

    // ----- INVESTIGACION COMENTARIOS -----


    // AGREGAR INVESTIGACION_COMENTARIOS
    app.post("/investigacion_personal.add_comentario",                investigacion_personal.addComentario);

    // ACTUALIZAR INVESTIGACION_COMENTARIOS
    app.put("/investigacion_personal.update_comentario/:id",          investigacion_personal.updateComentario);
  
    // OBTENER INVESTIGACION_COMENTARIOS POR ID_FORMULARIO (PROSPECTO)
    app.get("/investigacion_personal.get_comentario/:idformulario",   investigacion_personal.getProspecto);

  
  };