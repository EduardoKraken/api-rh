module.exports = app => {

    const examenes_medicos = require('../../controllers/crm-rh/examenes_medicos.controllers');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia la examenes_medicos personal
    */ 
  
    // AGREGAR EXAMEN
    app.post("/examenes_medicos.add",          examenes_medicos.addExamen);
  
    // ACTUALIZAR EXAMEN
    app.put("/examenes_medicos.update/:id",    examenes_medicos.updateExamen);
  
    // OBTENER EXAMEN POR ID
    app.get("/examenes_medicos.get/:id",       examenes_medicos.getExamen);


    // OBTENER EXAMEN POR ID_FORMULARIO (PROSPECTO)
    app.get("/examenes_medicos.get_formulario/:idprospecto",      examenes_medicos.getExamenProspecto);


    // ARCHIVOS DEL SERVIDOR


    // SUBIR ARCHIVO
    app.post("/examenes_medicos.add_archivo",       examenes_medicos.addArchivo);

    // ELIMINAR ARCHIVO
    app.post("/examenes_medicos.delete_archivo",    examenes_medicos.deleteArchivo);

    // OBTENER ARCHIVO
    app.get("/examenes_medicos.get_archivos",       examenes_medicos.getArchivos);


    /////////////////////////////////////////////////////////////////////////////////////////////////////

    //Agregado por Angel Rdz
    
    app.get("/examenes_medicos.get.all",       examenes_medicos.getExamenes);

  
  };