module.exports = app => {

    const pruebas_psicologicas = require('../../controllers/crm-rh/pruebas_psicologicas.controllers');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia las pruebas psicologicas
    */ 
  
    // AGREGAR PRUEBA_PSICOLOGICA
    app.post("/pruebas_psicologicas.add",             pruebas_psicologicas.addPrueba);
  
    // ACTUALIZAR PRUEBA_PSICOLOGICA
    app.put("/pruebas_psicologicas.update/:id",       pruebas_psicologicas.updatePrueba);
    
    // OBTENER PRUEBA_PSICOLOGICA
    app.get("/pruebas_psicologicas.get",              pruebas_psicologicas.getPruebas);

    // OBTENER PRUEBA_PSICOLOGICA POR ID
    app.get("/pruebas_psicologicas.get/:id",          pruebas_psicologicas.getPrueba);
  
    // OBTENER PRUEBA_PSICOLOGICA POR ID_PUESTO
    app.get("/pruebas_psicologicas.get_puesto/:id",   pruebas_psicologicas.getPruebaPuesto);
    
    
    // ----- PRUEBAS PSICOLÓGICAS EN EL SERVIDOR -----


    // SUBIR ARCHIVO PRUEBA TECNICA
    app.post("/pruebas_psicologicas.add_archivo",       pruebas_psicologicas.addArchivo);

    // ELIMINAR ARCHIVO PRUEBA TECNICA
    app.post("/pruebas_psicologicas.delete_archivo",    pruebas_psicologicas.deleteArchivo);

    // OBTENER ARCHIVO PRUEBA TECNICA
    app.get("/pruebas_psicologicas.get_archivos",       pruebas_psicologicas.getArchivos);


    // ----- AGENDA -----


    // AGREGAR AGENDA
    app.post("/pruebas_psicologicas.add_agenda",                  pruebas_psicologicas.addAgenda);

    // ACTUALIZAR AGENDA
    app.put("/pruebas_psicologicas.update_agenda/:id",            pruebas_psicologicas.updateAgenda);
    
    // OBTENER AGENDA POR RECLUTADORA
    app.post("/pruebas_psicologicas.get_agenda_reclutadora/:id",  pruebas_psicologicas.getAgendaReclutadora);

  
    // ----- PRUEBAS PSICOLOGICAS COMENTARIOS -----


    // AGREGAR PRUEBAS_NOTAS
    app.post("/pruebas_psicologicas.add_comentario",                pruebas_psicologicas.addNota);

    // ACTUALIZAR PRUEBAS_NOTAS
    app.put("/pruebas_psicologicas.update_comentario/:id",          pruebas_psicologicas.updateNota);
  
    // OBTENER PRUEBAS_NOTAS POR ID_AGENDA (PROSPECTO)
    app.get("/pruebas_psicologicas.get_comentario/:idagenda",       pruebas_psicologicas.getNotasAgenda);

  
  };