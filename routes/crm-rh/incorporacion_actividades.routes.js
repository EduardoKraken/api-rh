module.exports = app => {

    const incorporacion_actividades = require('../../controllers/crm-rh/incorporacion_actividades.controllers');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia las incorporacion_actividades
    */ 
  
    // AGREGAR INCORPORACION_ACTIVIDADES
    app.post("/incorporacion_actividades.add",          incorporacion_actividades.addActividades);
  
    // ACTUALIZAR INCORPORACION_ACTIVIDADES
    app.put("/incorporacion_actividades.update/:id",    incorporacion_actividades.updateActividades);
  
    // OBTENER INCORPORACION_ACTIVIDAD POR ID
    app.get("/incorporacion_actividades.get/:id",       incorporacion_actividades.getActividad);

    // OBTENER INCORPORACION_ACTIVIDADES
    app.get("/incorporacion_actividades.get",           incorporacion_actividades.getActividades);

    // OBTENER INCORPORACION_ACTIVIDADES POR PUESTO (PROSPECTO)
    app.get("/incorporacion_actividades.get_puesto/:id",   incorporacion_actividades.getActividadesPuesto);
  
  };