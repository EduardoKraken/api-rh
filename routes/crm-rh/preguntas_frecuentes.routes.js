module.exports = app => {

    const preguntas_frecuentes = require('../../controllers/crm-rh/preguntas_frecuentes.controllers');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia las preguntas_frecuentes
    */ 
  
    // AGREGAR PREGUNTAS
    app.post("/preguntas_frecuentes.add",          preguntas_frecuentes.addPregunta);
  
    // ACTUALIZAR PREGUNTAS
    app.put("/preguntas_frecuentes.update/:id",    preguntas_frecuentes.updatePregunta);
  
    // OBTENER PREGUNTAS POR ID
    app.get("/preguntas_frecuentes.get/:id",       preguntas_frecuentes.getPregunta);

    // OBTENER PREGUNTAS
    app.get("/preguntas_frecuentes.get",           preguntas_frecuentes.getPreguntas);
  
  };