module.exports = app => {

    const firma_laboral = require('../../controllers/crm-rh/firma_laboral.controllers');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia las FIRMA LABORAL
    */ 
  
    // AGREGAR FIRMA_LABORAL
    app.post("/firma_laboral.add",             firma_laboral.addFirma);
  
    // ACTUALIZAR FIRMA_LABORAL
    app.put("/firma_laboral.update/:id",       firma_laboral.updateFirma);
    
    // OBTENER FIRMA_LABORAL
    app.get("/firma_laboral.get",              firma_laboral.getFirmas);

    // OBTENER FIRMA_LABORAL POR ID
    app.get("/firma_laboral.get/:id",          firma_laboral.getFirma);
    
    
    // ----- PROPUESTA ECONOMICA -----


    // SUBIR ARCHIVO PROPUESTA_ECONOMICA
    app.post("/firma_laboral.add_propuesta",        firma_laboral.addPropuesta);

    // ELIMINAR ARCHIVO PROPUESTA_ECONOMICA
    app.post("/firma_laboral.delete_propuesta",     firma_laboral.deletePropuesta);

    // OBTENER ARCHIVO PROPUESTA_ECONOMICA
    app.get("/firma_laboral.get_propuesta",         firma_laboral.getPropuesta);



    // ----- PROPUESTA ECONOMICA FIRMADA -----


    // SUBIR ARCHIVO PROPUESTA_FIRMADA
    app.post("/firma_laboral.add_propuesta_firmada",        firma_laboral.addPropuestaFirmada);

    // ELIMINAR ARCHIVO PROPUESTA_FIRMADA
    app.post("/firma_laboral.delete_propuesta_firmada",     firma_laboral.deletePropuestaFirmada);

    // OBTENER ARCHIVO PROPUESTA_FIRMADA
    app.get("/firma_laboral.get_propuesta_firmada",         firma_laboral.getPropuestaFirmada);


    
    // ----- AGENDA -----


    // OBTENER AGENDA POR LAPSO DE TIEMPO
    app.post("/firma_laboral.get_agenda",                  firma_laboral.getAgenda);

  };