module.exports = app => {

    const inducciones = require('../../controllers/crm-rh/inducciones.controllers');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia la inducciones personal
    */ 
  
    // AGREGAR INDUCCIONES
    app.post("/inducciones.add",          inducciones.addInduccion);
  
    // ACTUALIZAR INDUCCIONES
    app.put("/inducciones.update/:id",    inducciones.updateInduccion);
  
    // OBTENER INDUCCIONES POR ID
    app.get("/inducciones.get/:id",       inducciones.getInduccion);

    // OBTENER INDUCCIONES
    app.get("/inducciones.get",           inducciones.getInducciones);

    // OBTENER INDUCCIONES POR PUESTO
    app.get("/inducciones.get_puestos/:id",   inducciones.getInduccionesPuesto);



    // ----- INDUCCIONES COMENTARIOS -----


    // AGREGAR INDUCCIONES_COMENTARIOS
    app.post("/inducciones.add_comentario",                inducciones.addComentario);

    // ACTUALIZAR INDUCCIONES_COMENTARIOS
    app.put("/inducciones.update_comentario/:id",          inducciones.updateComentario);
  
    // OBTENER INDUCCIONES_COMENTARIOS POR ID_FORMULARIO (PROSPECTO)
    app.get("/inducciones.get_comentarios/:idformulario",  inducciones.getComentariosProspecto);



    // ----- ARCHIVOS DEL SERVIDOR -----


    // SUBIR ARCHIVO
    app.post("/inducciones.add_archivo",       inducciones.addArchivo);

    // ELIMINAR ARCHIVO
    app.post("/inducciones.delete_archivo",    inducciones.deleteArchivo);

    // OBTENER ARCHIVO
    app.get("/inducciones.get_archivos",       inducciones.getArchivos);

  
  };