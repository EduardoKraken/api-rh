module.exports = app => {

    const encargadas = require('../../controllers/crm-rh/encargadas.controllers');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia los encargadas
    */ 
  
    // AGREGAR ENCARGADA
    app.post("/encargadas.add",                   encargadas.addEncargada);
  
    // ACTUALIZAR ENCARGADA
    app.put("/encargadas.update/:id",             encargadas.updateEncargada);
    
    // OBTENER TODOS LOS ENCARGADAS
    app.get("/encargadas.get",                    encargadas.getEncargadas);
  
    // OBTENER ENCARGADA POR ID
    app.get("/encargadas.get/:id",                encargadas.getEncargada);
  

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Agregado por Angel Rdz

    app.get("/encargadas.get.usuarios",           encargadas.getUsuarios);

    app.get("/encargadas.get.departamentos",      encargadas.getDepartamentos);
  };