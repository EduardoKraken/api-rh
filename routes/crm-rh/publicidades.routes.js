module.exports = app => {

    const publicidades = require('../../controllers/crm-rh/publicidades.controllers');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia los publicidad
    */ 
  
    // AGREGAR PUBLICIDAD
    app.post("/publicidades.add",          publicidades.addPublicidad);
  
    // ACTUALIZAR PUBLICIDAD
    app.put("/publicidades.update/:id",    publicidades.updatePublicidad);
    
    // OBTENER TODAS LAS PUBLICIDADES

    app.get("/publicidades.get",           publicidades.getPublicidades);
  
    // OBTENER PUBLICIDAD POR ID
    app.get("/publicidad.get/:id",       publicidades.getPublicidad);
  
  };