// CRM-RH

module.exports = app => {

    require('../crm-rh/capacitaciones.routes')           ( app );
    require('../crm-rh/encargadas.routes')               ( app );
    require('../crm-rh/eventos.routes')                  ( app );
    require('../crm-rh/examenes_medicos.routes')         ( app );
    require('../crm-rh/firma_laboral.routes')            ( app );
    require('../crm-rh/formularios.routes')              ( app );
    require('../crm-rh/incorporacion_actividades.routes')( app );
    require('../crm-rh/inducciones.routes')              ( app );
    require('../crm-rh/investigacion_personal.routes')   ( app );
    require('../crm-rh/llamadas_tecnicas.routes')        ( app );
    require('../crm-rh/preguntas_frecuentes.routes')     ( app );
    require('../crm-rh/pruebas_psicologicas.routes')     ( app );
    require('../crm-rh/pruebas_tecnicas.routes')         ( app );
    require('../crm-rh/publicidades.routes')             ( app );
    require('../crm-rh/reclutar_puestos.routes')         ( app );
    require('../crm-rh/speeches.routes')                 ( app );
    require('../crm-rh/examenes_medicos.routes')         ( app );

}   