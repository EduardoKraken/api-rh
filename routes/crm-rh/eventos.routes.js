module.exports = app => {

    const eventos = require('../../controllers/crm-rh/eventos.controllers');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia los eventos
    */ 
  
    // AGREGAR EVENTO
    app.post("/eventos.add",          eventos.addEvento);
  
    // ACTUALIZAR EVENTO
    app.put("/eventos.update/:id",    eventos.updateEvento);
  
    // OBTENER EVENTO POR ID
    app.get("/eventos.get/:id",       eventos.getEvento);

    // OBTENER EVENTOS
    app.get("/eventos.get",           eventos.getEventos);

    // OBTENER EVENTOS POR FORMULARIO (PROSPECTO)
    app.get("/eventos.get_formulario/:id",   eventos.getEventosFormulario);
  
  };