module.exports = app => {

    const pruebas_tecnicas = require('../../controllers/crm-rh/pruebas_tecnicas.controllers');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia las pruebas tecnicas
    */ 
  
    // AGREGAR PRUEBA_TECNICA
    app.post("/pruebas_tecnicas.add",             pruebas_tecnicas.addPrueba);
  
    // ACTUALIZAR PRUEBA_TECNICA
    app.put("/pruebas_tecnicas.update/:id",       pruebas_tecnicas.updatePrueba);
    
    // OBTENER PRUEBA_TECNICA
    app.get("/pruebas_tecnicas.get",              pruebas_tecnicas.getPruebas);

    // OBTENER PRUEBA_TECNICA POR ID
    app.get("/pruebas_tecnicas.get/:id",          pruebas_tecnicas.getPrueba);
  
    // OBTENER PRUEBA_TECNICA POR ID_PUESTO
    app.get("/pruebas_tecnicas.get_puesto/:id",   pruebas_tecnicas.getPruebaPuesto);
    
    
    // ----- PRUEBAS EN EL SERVIDOR -----


    // SUBIR ARCHIVO PRUEBA TECNICA
    app.post("/pruebas_tecnicas.add_archivo",       pruebas_tecnicas.addArchivo);

    // ELIMINAR ARCHIVO PRUEBA TECNICA
    app.post("/pruebas_tecnicas.delete_archivo",    pruebas_tecnicas.deleteArchivo);

    // OBTENER ARCHIVO PRUEBA TECNICA
    app.get("/pruebas_tecnicas.get_archivos",       pruebas_tecnicas.getArchivos);


    // ----- AGENDA -----


    // AGREGAR AGENDA
    app.post("/pruebas_tecnicas.add_agenda",                  pruebas_tecnicas.addAgenda);

    // ACTUALIZAR AGENDA
    app.put("/pruebas_tecnicas.update_agenda/:id",            pruebas_tecnicas.updateAgenda);
    
    // OBTENER AGENDA POR RECLUTADORA
    app.post("/pruebas_tecnicas.get_agenda_reclutadora/:id",  pruebas_tecnicas.getAgendaReclutadora);

  
    // ----- PRUEBAS PSICOLOGICAS COMENTARIOS -----


    // AGREGAR PRUEBAS_NOTAS
    app.post("/pruebas_tecnicas.add_comentario",                pruebas_tecnicas.addNota);

    // ACTUALIZAR PRUEBAS_NOTAS
    app.put("/pruebas_tecnicas.update_comentario/:id",          pruebas_tecnicas.updateNota);
  
    // OBTENER PRUEBAS_NOTAS POR ID_AGENDA (PROSPECTO)
    app.get("/pruebas_tecnicas.get_comentario/:idagenda",       pruebas_tecnicas.getNotasAgenda);
  
  };