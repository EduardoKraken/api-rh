module.exports = app => {

    const llamadas_tecnicas = require('../../controllers/crm-rh/llamadas_tecnicas.controllers');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia las llamadas_tecnicas
    */ 
  
    // AGREGAR LLAMADA_TECNICA
    app.post("/llamadas_tecnicas.add",            llamadas_tecnicas.addLlamada);
  
    // ACTUALIZAR LLAMADA_TECNICA
    app.put("/llamadas_tecnicas.update/:id",      llamadas_tecnicas.updateLlamada);

    // OBTENER LLAMADA_TECNICA POR ID
    app.get("/llamadas_tecnicas.get/:id",         llamadas_tecnicas.getLlamada);

    // OBTENER LLAMADAS_TECNICAS POR FECHA
    app.post("/llamadas_tecnicas.get_fecha",      llamadas_tecnicas.getFecha);


    
    // ----- PARTICIPANTES -----


    // AGREGAR PARTICIPANTES
    app.post("/llamadas_tecnicas.add_participante",           llamadas_tecnicas.addParticipantes);

    // ACTUALIZAR PARTICIPANTES
    app.put("/llamadas_tecnicas.update_participante/:id",     llamadas_tecnicas.updateParticipante);

    // OBTENER PARTICIPANTES LLAMADA_ID
    app.get("/llamadas_tecnicas.get_participantes/:id",       llamadas_tecnicas.getParticipanteLLamada);
    
    // OBTENER PARTICIPANTES POR LLAMADA
    app.post("/llamadas_tecnicas.get_participantes_llamada",  llamadas_tecnicas.getParticipantesLlamada);

    // OBTENER LLAMADAS POR IDEMPLEADO
    app.post("/llamadas_tecnicas.get_participante_agenda",    llamadas_tecnicas.getParticipantesAgenda);

  };