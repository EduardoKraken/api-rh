module.exports = app => {

    const reclutar_puestos = require('../../controllers/crm-rh/reclutar_puestos.controllers');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia los reclutar_puestos
    */ 
  
    // AGREGAR RECLUTADORA_PUESTOS
    app.post("/reclutar_puestos.add",                 reclutar_puestos.addReclutadora);
  
    // ACTUALIZAR RECLUTADORA_PUESTOS
    app.put("/reclutar_puestos.update/:id",           reclutar_puestos.updateReclutadora);
    
    // OBTENER RECLUTAR_PUESTOS POR ID
    app.get("/reclutar_puestos.get/:idreclutadora",   reclutar_puestos.getReclutadora);

    // OBTENER RECLUTAR_PUESTOS
    app.get("/reclutar_puestos.get",                  reclutar_puestos.getReclutadoras);


    // ----- ACCIONES EXTRA -----
    
    // OBTENER FORMULARIOS DE PROPSECTOS POR PUESTOS DE RECLUTADORA
    app.get("/reclutar_puestos.getpuesto/:idpuesto",    reclutar_puestos.getReclutadoraPuesto);

    // ACTUALIZAR PROSPECTO DE FORMULARIO EN EL CAMPO ACEPTADO
    app.put("/reclutar_puestos.update_prosprecto/:id",  reclutar_puestos.updateProspectoFormulario);


    // ----- AGENDA -----


    // AGREGAR LLAMADA A PROSPECTO DE FORMULARIO
    app.post("/reclutar_puestos.add_agenda",                    reclutar_puestos.addAgenda);

    // ACTUALIZAR AGENDA
    app.put("/reclutar_puestos.update_agenda/:id",              reclutar_puestos.updateAgenda);

    // AGREGAR DURACION DE LLAMADA
    app.put("/reclutar_puestos.update_agenda_duracion/:id",     reclutar_puestos.updateAgendaHorario);

  };