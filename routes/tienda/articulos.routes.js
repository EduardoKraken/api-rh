module.exports = app => {
    const articulos = require('../../controllers/tienda/articulos.controllers') // --> ADDED THIS

    app.get("/obtener.articulos.random", articulos.obtener_articulos_random);   // BUSCAR 


    // Apis generales
    app.get("/articulos.list"            , articulos.findAll);   // BUSCAR 
    app.post("/articulos.add"            , articulos.addArticulos);
    app.get("/articulos.codigo/:codigo"  , articulos.getArticuloCodigo);
    app.get("/articulos.fotos/:codigo"   , articulos.fotosxArt);
    app.post("/fotos.add"                , articulos.addFoto);
    app.get("/articulos.id/:id"          , articulos.getArticuloId);
    app.put("/articulos.update/:id"      , articulos.updateArticulos);
    app.get("/articulos.activos"         , articulos.getArticulosActivo);


    // Novedades
    app.put("/articulos.novedades/:id", articulos.updateNovedades);

    // Foto
    app.delete("/admin/foto.art.delete/:id",   articulos.deleteFoto);

    // Tienda
    app.get("/tienda/articulos.novedades.get",       articulos.getNovedades);
    app.get("/tienda/articulos.destacados.get",      articulos.getDestacados);
    app.get("/tienda/articulos.promociones.get",     articulos.getPromociones);
    app.get("/tienda/articulos.laboratorio.get/:id", articulos.getArtxLab);
    app.get("/tienda/articulos.codigo/:codigo",      articulos.getArticuloTienda);
    app.get("/tienda/articulos.articulo/:id",        articulos.getArticuloId);
    app.get("/tienda/articulos/fotos/:codigo",       articulos.getArticuloFotos)
    app.get("/tienda/articulos/fam/:id",             articulos.getArticuloFam)
    app.get("/tienda/articulos/cat/:id",             articulos.getArticuloCat)
    app.get("/tienda/articulos/sub/:id",             articulos.getArticuloSub)

    // Foto principal
    app.put("/update.foto.principal/:id", articulos.updateFotoPrincipal);


    // Agregar ventas
    app.post("/articulos.compra",                  articulos.addVenta)
    app.get("/articulos.solicitudes",              articulos.getSolicitudesCompra)
    app.get("/articulos.solicitudes",              articulos.getSolicitudesCompra)
    app.put("/articulos.solicitudes.eliminar/:id", articulos.eliminarSolicitud)
    app.put("/articulos.solicitudes.update/:id",   articulos.aceptarSolicitud)
    app.post("/articulos.solicitudes.update",      articulos.entregaArticulos)
    app.post("/articulos.pedido.recibido",         articulos.recibirPedido)
    app.get("/articulos.compra.habil",             articulos.diaHabilCompra)



  };