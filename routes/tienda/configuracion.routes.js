module.exports = app => {
  const configuracion = require('../../controllers/tienda/configuracion.controllers') // --> ADDED THIS

  // movil
  app.get("/config.list",       configuracion.getConfiguracion);   // BUSCAR
  app.put("/config.update", configuracion.updateConfig);

};