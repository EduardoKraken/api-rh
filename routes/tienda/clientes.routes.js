module.exports = app => {
  const clientes = require('../../controllers/tienda/clientes.controllers') // --> ADDED THIS

  app.post("/session.cliente"      , clientes.session_cliente);
  app.post("/clientes.add",  clientes.addCliente);   // CREAR UN NUEVO GRUPO
  app.get("/clientes.id",    clientes.getClienteId);   
  app.post("/clientes.list",   clientes.getClientes);   
  app.post("/clientes.update"      , clientes.updateCliente);

};