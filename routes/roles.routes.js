module.exports = app => {
    const roles = require('../controllers/roles.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    // Cardar los selectores
    // ciclos
    app.get("/roles.ciclos.all"                     , roles.getCiclos); // OBTENER

    // Obtener todos los grupos 
    app.get("/roles.grupos/:idciclo/:idciclo2"      , roles.getGruposRoles);

    // Obtener los teachers que si pueden dar clase
    app.get("/roles.teacher"                        , roles.getTeachersRoles);


};