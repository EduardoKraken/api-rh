module.exports = app => {
    const planteles = require('../controllers/planteles.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/planteles.all", planteles.all_planteles); // OBTENER
    app.post("/planteles.add", planteles.add_planteles); //AGREGAR 
    app.put("/planteles.update/:idplanteles", planteles.update_plantel); //actualizar 

};