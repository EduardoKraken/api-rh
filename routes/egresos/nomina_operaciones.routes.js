module.exports = app => {
  const egresos = require('../../controllers/egresos/nomina_operaciones.controllers') // --> ADDED THIS
  app.post("/generar.nomina.operaciones"  ,       egresos.generarNominaOperaciones); 
  app.post("/consultar.nomina.operaciones",       egresos.consultarNominaOperaciones); 
  app.get("/consultar.elemtos.compra",            egresos.getElementosCompra); 
  app.get("/consultar.requisiciones",             egresos.getTipoRequisiciones); 
  app.post("/consultar.mis.requisiciones",        egresos.getMisRequisiciones); 
  app.post("/consultar.requisiciones",            egresos.getRequisiciones); 
  app.post("/subir.archivos.requi",               egresos.subirArchivosRequi); 
  app.post("/subir.comprobantes.partidas",        egresos.subirComprobantePartida);
  app.put("/eleminar.partida/:id",                egresos.eliminarPartida); 
  app.post("/requisicion.preautorizar",           egresos.preAutorizarRequi); 
  app.post("/update.estatus.requisicion",         egresos.updateEstatusRequi); 
  app.post('/egresos.subir.fotos',                egresos.subirFotosCotizaciones);
  app.post('/egresos.actualizar.tipopago',        egresos.updateTipoPago);
  app.post("/consultar.partidas.egreso",          egresos.consultarPartidasEgreso); 
  app.post("/update.estatus.partida",             egresos.updateEstatusPartida); 
  app.post("/update.aceptar.comprobante",         egresos.updateAceptarComprobante); 
  app.post("/subir.post.cotizaciones"  ,          egresos.subirPostCotizaciones); 
  app.post("/actualizar.requisicion.partida"  ,   egresos.actualizarRequisiciones); 
  app.post("/add.egreso.mensaje",                 egresos.addegresoMensaje); 
  app.post("/update.estatus.partida.revision",    egresos.updateEstatusPartidaRevision); 
  app.post("/consultar.mensajes.rechazo.egreso",  egresos.consultarMensajesRechazoEgreso); 
  app.post("/update.delete.comprobante",          egresos.updateDeleteComprobante);
  app.post("/update.rechazar.partida.requi",      egresos.updateRechazarPartidaRequi);
  app.post("/update.mensaje.rechazo.respuesta",   egresos.updateMensajeRechazoRespuesta); 
  app.post("/consultar.comentario.comprobante",   egresos.getComentarioComprobante); 
  app.post("/add.elemento.requisicion",           egresos.addElementoRequisicion); 
  app.get("/consultar.proyectos",                 egresos.getProyectos); 
  app.put('/update.proyectos/:id',                egresos.updateProyectos); 
  app.post('/add.proyectos',                      egresos.addProyectos); 

  //////////////////////////////////////////////////////////////////////////////////////////
  //26/03/23//
  app.post('/egresos.mandar.egresos',             egresos.updateMandarEgreso);

  /////////////////////////////////////////////////////////////////////////////////////////

  app.get("/consultar.ciclos.egresos",            egresos.getCiclosEgresos); 
  app.post("/update.partida",                     egresos.updatePartida); 
  app.post("/update.requi",                       egresos.updateRequi); 

  // actualizar el egreso 
  app.post('/solicitar.cambio.egreso',            egresos.solicitarCambioEgreso);
  app.put('/solicitar.cambio.egreso/:id',         egresos.updateCambioEgreso);
  app.put('/egresos.eliminados'         ,         egresos.getEgresosEliminados);
  app.post('/procesar.cambio.egreso',             egresos.procesarCambioEgreso);
  app.post('/procesar.cambio.rechazado',          egresos.procesarCambioEgresoRechazado);
  app.post('/procesar.cambio.papelera',           egresos.papeleraEgresos);
};

