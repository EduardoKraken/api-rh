module.exports = app => {
  require("./nomina_teachers.routes")(app)
  require("./nomina_operaciones.routes")(app)
  require("./renta_sucursales.routes.js")(app)
  require("./pago_servicios.routes.js")(app)
};