module.exports = app => {
  const egresos = require('../../controllers/egresos/renta_sucursales.controllers') // --> ADDED THIS
  app.get("/get.ciclos.renta",                egresos.getCiclosRenta); 
  app.post("/generar.renta.sucursales",       egresos.generarRentaSucursales); 
  app.post("/consultar.renta.sucursales",     egresos.consultarRentaSucursales); 
  // app.get("/consultar.elemtos.compra",            egresos.getElementosCompra); 
};

