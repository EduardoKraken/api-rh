module.exports = app => {
  const accesos = require('../../controllers/sistemaweb/accesos.controllers.js');
  /*
    Todas las rutas aquí creadas estarán dirigidas hacia los ACCESOS
  */ 

  // AGREGAR ACCESO
  app.post("/acceso.add",             accesos.addAcceso);

  // ACTUALIZAR ACCESO
  app.put("/acceso.update",           accesos.updateAcceso);

  // OBTENER ACCESOS POR ESCUELA
  app.get("/accesos.get/:idescuela",  accesos.getAccesos);

  // OBTENER ACCESO POR ID_ACCESO_ACADEMICO
  app.get("/acceso.get/:idacceso",    accesos.getAcceso);
  
};