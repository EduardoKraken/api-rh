module.exports = app => {
    const archivos = require('../../controllers/sistemaweb/archivos.controllers.js');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia los archivos
    */ 
  
    // AGREGAR ARCHIVO
    app.post("/archivo.add",        archivos.addArchivo);

    // ELIMINAR ARCHIVO
    app.post("/archivo.delete",     archivos.deleteArchivo);

    // MOSTRAR ARCHIVOS
    app.get("/archivo.get",         archivos.getArchivos);

    app.post("/galeria.sistema",    archivos.addArchivoGaleria);

  
};