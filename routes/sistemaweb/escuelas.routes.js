module.exports = app => {
    const escuelas = require('../../controllers/sistemaweb/escuelas.controllers.js');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia las escuelas
    */ 
  
    // AGREGAR ESCUELA
    app.post("/escuela.add",              escuelas.addEscuela);
  
    // ACTUALIZAR ESCUELA
    app.put("/escuela.update/:id",        escuelas.updateEscuela);

    // OBTENER ESCUELAS
    app.get("/escuela.get",               escuelas.getEscuelas);

    // OBTENER ESCUELA
    app.get("/escuela.get/:id",           escuelas.getEscuela);

    //OBTENER TODA LA INFO DE LA ESCUELA (ESCUELA, PERMISO(IP)) NIVELES DIAPOSITIVAS MATERIAL
    app.post("/escuela.getInformacion",   escuelas.getEscuelasAcceso);

  };