module.exports = app => {
    const diapositivas = require('../../controllers/sistemaweb/diapositivas.controllers.js');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia las diapositivas
    */ 
  
    // AGREGAR DIAPOSITIVA
    app.post("/diapositiva.add",              diapositivas.addDiapositiva);

    // ACTUALIZAR DIAPOSITIVA
    app.put("/diapositiva.update/:id",        diapositivas.updateDiapositiva);

    // OBTENER DIAPOSITIVAS POR LECCION
    app.get("/diapositivas.get/:idleccion",   diapositivas.getDiapositivas);

    // OBTENER DIAPOSITIVA
    app.get("/diapositiva.get/:id",           diapositivas.getDiapositiva);
  
  };