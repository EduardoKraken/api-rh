module.exports = app => {
    // SISTEMA ACADÉMICO
    require('./escuelas.routes')    ( app );
    require('./niveles.routes')     ( app );
    require('./lecciones.routes')   ( app );
    require('./diapositivas.routes')( app );
    require('./archivos.routes')    ( app );
    require('./accesos.routes')    ( app );
}