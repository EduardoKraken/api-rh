module.exports = app => {
    const lecciones = require('../../controllers/sistemaweb/lecciones.controllers.js');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia las lecciones
    */ 
  
    // AGREGAR LECCION
    app.post("/leccion.add",            lecciones.addLeccion);

    // ACTUALIZAR LECCION
    app.put("/leccion.update/:id",      lecciones.updateLeccion);

    // OBTENER LECCIONES POR NIVEL
    app.get("/lecciones.get/:idnivel",  lecciones.getLecciones);

    // OBTENER LECCION
    app.get("/leccion.get/:id",         lecciones.getLeccion);
  
  };