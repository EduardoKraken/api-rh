module.exports = app => {
    const niveles = require('../../controllers/sistemaweb/niveles.controllers.js');
    /*
      Todas las rutas aquí creadas estarán dirigidas hacia los niveles
    */ 
  
    // AGREGAR NIVEL
    app.post("/nivel.add",        niveles.addNivel);

    // ACTUALIZAR NIVEL
    app.put("/nivel.update/:id",  niveles.updateNivel);

    // OBTENER NIVELES
    app.get("/nivel.get/:id",     niveles.getNiveles);

    // OBTENER NIVEL
    app.get("/nivel.get/:id",     niveles.getNivel);
  
  };