module.exports = app => {
    const permisos = require('../controllers/permisos.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/permisos.all"               , permisos.getPermisos); // OBTENER
    app.post("/permisos.add"              , permisos.addPermisos); //AGREGAR USUARIO
    app.put("/permisos.update/:idpermisos", permisos.updatePermisos); //actualizar usuario

};