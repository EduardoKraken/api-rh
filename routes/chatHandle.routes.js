const axios = require("axios");
const wpp = require("../models/whatsapp.model");
const Prospectos = require("../models/catalogos/prospectos.model");
module.exports = (app) => {
  app.io.of("/whats").on("connection", function (socket) {
    console.log("conectado");
    socket.join("chats");

    socket.on("sendMessage", async (message) => {
      if (message.file == false) {
        await axios
          .post("http://127.0.0.1:3025/", message)
          .then(async function (response) {
            console.log(response.data);
            await wpp.addMensajeWhatsApp(message.id_chat, 0, message.content);
            console.log(response);
          })
          .catch(function (error) {
            socket.emit("ErrorMessage", error);
            console.log(error.message);
          });
      } else {
        await axios
          .post("http://127.0.0.1:3025/", message)
          .then(async function (response) {
            console.log(response.data);
            await wpp.addMensajeWhatsAppFile(
              message.id_chat,
              0,
              message.content,
              message.fileName,
              message.fileExtension
            );
            console.log(response);
          })
          .catch(function (error) {
            socket.emit("ErrorMessage", error);
            console.log(error.message);
          });
      }
      return;
    });

    socket.on("seenMessage",async (idchat) =>{
      await wpp.seenNewMessage(idchat)
      socket.emit("updateSeenStatusChat", idchat);
    })
  });

  app.put("/prospectos.asignar/:idProspecto", async (req, res) => {
    let id_prospecto = req.params.idProspecto;
    let { id_empleado } = req.body;

    if (typeof id_prospecto == "undefined" || id_empleado == "undefined")
      res.status(400).send({
        error: true,
        message: "Debes enviar un prospecto y un prospecto",
      });

    let reasignado = await Prospectos.asignarProspecto(
      id_prospecto,
      id_empleado
    )
      .then((response) => {
        return true;
      })
      .catch((error) => {
        res.status(500).send({
          error: true,
          message:
            error.message ||
            "Se produjo algún errro al tratar de reasignar el prospecto",
        });
      });

    if (reasignado) {
      const respon = req.app.io
        .of("/whats")
        .to("chats")
        .emit("updateChats", true);

      res.status(200).send({
        error: false,
        message: "Se reasigno el usuario correctametne",
      });
      return;
    } else {
    }
  });

  app.post("/wpp.income.message", (req, res) => {
    const respon = req.app.io
      .of("/whats")
      .to("chats")
      .emit("incomeMessage", req.body);
    
    // console.log(respon);
    res.status(200).send({ status: "ok" });
  });

  app.post("/wpp.fileUpload", (req, res) => {
    let EDFile = req.files.file; //EDFile.name
    let { nombre, ext } = req.body;

    EDFile.mv(`../imageneswhatsapp/${nombre}.${ext}`, (err) => {
      if (err) return res.status(500).send({ message: err });
      return res.status(200).send({ message: "File upload" });
    });
  });
};
