module.exports = app => {
  const archivos = require('../controllers/archivos.controllers') // --> ADDED THIS
  app.post("/imagenes"           , archivos.addImagen);
  app.post("/pdfs"               , archivos.addPdf);

  app.post("/archivos"           , archivos.addArchivo);
  app.post("/crear-contrato/:id" , archivos.crearContraro);

  app.post("/audio.parse"        , archivos.parseAudio);
};