module.exports = app => {
  require("./clases.routes")(app)
  require("./preguntas.routes")(app)
  require("./encuestas.routes")(app)
};