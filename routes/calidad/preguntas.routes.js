module.exports = app => {
  const preguntas = require('../../controllers/calidad/preguntas.controllers') 
  app.get("/calidad.preguntas.all"      , preguntas.getPreguntasAll); 
  app.post("/calidad.preguntas.add"     , preguntas.addPreguntas); 
  app.put("/calidad.preguntas.update"   , preguntas.updatePreguntas); 

};