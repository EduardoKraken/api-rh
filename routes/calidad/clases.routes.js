module.exports = app => {
  const clases    = require('../../controllers/calidad/clases.controllers') 
  const rolClases = require('../../controllers/calidad/rolclases.controllers')

  app.post("/calidad.clases.plantel"           , clases.getGruposPlantel); 
  app.post("/calidad.clases.evaluacion"        , clases.addEvaluacionClase); 
  app.post("/calidad.clases.imagen/:id/:ext"   , clases.addEvaluacionClaseImagen); 
  app.post("/calidad.clases.evaluadas"         , clases.getClasesEvaluadas); 
  app.post("/calidad.clases.usuario"           , clases.getClasesEvaluadasUsuario); 
  app.get("/calidad.clases.respuestas/:id"     , clases.getClasesRespuestas); 
  app.post("/calidad.clases.all"               , clases.getClasesEvaluadasAll); 
  app.post("/calidad.clases.teacher"           , clases.getClasesEvaluadasTeacher); 
  app.put("/calidad.clases.evaluacion.update"  , clases.evaluacionClaseUpdate); 


  app.post("/rol.clases"                       , rolClases.getRolClases)
  app.post("/rol.clases.add"                   , rolClases.agregarRolClases)
  app.post("/rol.clases.reemplazo"             , rolClases.agregarRolReemplazo)
};

