module.exports = app => {
    const planteles_usuario = require('../controllers/planteles_usuario.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/planteles_usuario.all"                           , planteles_usuario.all_planteles_usuario); // OBTENER
    app.get("/planteles_usuario.usuario/:idusuario"            , planteles_usuario.getPlantelesUsuarioId); // OBTENER
    app.post("/planteles_usuario.add"                          , planteles_usuario.add_planteles_usuario); //AGREGAR USUARIO
    app.put("/planteles_usuario.update/:idplanteles_usuario"   , planteles_usuario.update_planteles_usuario); //actualizar usuario
    app.delete("/planteles_usuario.delete/:idplanteles_usuario", planteles_usuario.delete_planteles_usuario); //eliminar
};