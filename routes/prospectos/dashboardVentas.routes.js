module.exports = app => {
  const dashboardVentas          = require('../../controllers/prospectos/dashboardVentas.controllers') 

  app.get('/data.dashboard/:id'          , dashboardVentas.getDashboardVendedora); 
};
