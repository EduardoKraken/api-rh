module.exports = app => {
	// Etapas
  const etapas           = require('../../controllers/prospectos/etapas.controllers') 
  const mediocontacto    = require('../../controllers/prospectos/mediocontacto.controllers') 
  const anuncios         = require('../../controllers/prospectos/anuncios.controllers') 
  const tareas           = require('../../controllers/prospectos/tareas.controllers') 
  const erpviejo         = require('../../controllers/prospectos/erpviejo.controllers') 
  const campanias        = require('../../controllers/prospectos/campanias.controllers') 
  const motivos          = require('../../controllers/prospectos/motivos.controllers') 
  const cursos           = require('../../controllers/prospectos/cursos.controllers') 
  const fuentes          = require('../../controllers/prospectos/fuentes.controllers') 
  // const cursos           = require('../../controllers/prospectos/cursos.controllers') 

  // Rutas de las apis
  app.get('/etapas.list'      		  , etapas.getEtapasList); 
  app.post('/etapas.add'      		  , etapas.addEtapas); 
  app.put('/etapas.update/:id'      , etapas.updateEtapas); 
  app.get('/etapas.activas'         , etapas.getEtapasActivas); 

  // MEDIO DE CONTACTO	
  app.get('/mediocontacto.list'      	  , mediocontacto.getMedioContactoList); 
  app.post('/mediocontacto.add'         , mediocontacto.addMedioContacto); 
  app.put('/mediocontacto.update/:id'   , mediocontacto.updateMedioContacto);
  app.get('/mediocontacto.activas'      , mediocontacto.getMedioActivas); 


  // ANUNCIOS
  app.get('/anuncios.list'      		, anuncios.getAnunciosList); 
  app.post('/anuncios.add'      		, anuncios.addAnuncios); 
  app.put('/anuncios.update/:id'    , anuncios.updateAnuncios); 
  app.get('/anuncios.activos'       , anuncios.getAnunciosActivos); 

  // TAREAS
  app.get('/tareas.list'                , tareas.getTareasList); 
  app.post('/tareas.add'                , tareas.addTareas); 
  app.put('/tareas.update/:id'          , tareas.updateTareas); 
  app.get('/tareas.activos'             , tareas.getTareasActivos); 
  app.get('/tareas.activos.formulario'  , tareas.getTareasActivosFormulario); 

  // CAMAPAÑAS
  app.get('/campanias.list'          , campanias.getCampaniasList); 
  app.post('/campanias.add'          , campanias.addCampanias); 
  app.put('/campanias.update/:id'    , campanias.updateCampanias); 
  app.get('/campanias.activos'       , campanias.getCampaniasActivos); 

  // MOTIVOS
  app.get('/motivos.list'               , motivos.getMotivoList); 
  app.post('/motivos.add'               , motivos.addMotivo); 
  app.put('/motivos.update/:id'         , motivos.updateMotivo); 
  app.get('/motivos.activos'            , motivos.getMotivoActivos); 
  app.get('/motivos.activos.formulario' , motivos.getMotivoActivosFormulario); 

  
  /*
    APIS EXTERNAS A EL ERP VIEJO
  */
  // app.get('/cursos.activos'       , erpviejo.getCursosActivos); 
  app.get('/ciclos.activos'       , erpviejo.getCiclosActivos); 
  app.get('/vendedoras.list'      , erpviejo.getVendedoras); 

  /*
    APIS PARA PODER VER LOS CURSOS
  */
  app.get('/escuela.cursos.list'            , cursos.getCursosList); 
  app.post('/escuela.cursos.add'            , cursos.addCursos); 
  app.put('/escuela.cursos.update/:id'      , cursos.updateCursos); 
  app.get('/escuela.cursos.activos'         , cursos.getCursosActivos); 
  // LAS MODALIDADES DE LOS CURSOS
  app.get('/escuela.modalidades.list'       , cursos.getModalidades);
  app.post('/escuela.modalidades.add'       , cursos.addModalidades); 
  app.put('/escuela.modalidades.update/:id' , cursos.updateModalidades); 
  app.get('/escuela.modalidades.activos'    , cursos.getModalidadesActivas); 
  // LAS FRECUENCIAS DE LAS MODALIDADES
  app.get('/escuela.frecuencias.list'       , cursos.getFrecuencias);
  app.post('/escuela.frecuencias.add'       , cursos.addFrecuencias); 
  app.put('/escuela.frecuencias.update/:id' , cursos.updateFrecuencias); 
  app.get('/escuela.frecuencias.activos'    , cursos.getFrecuenciasActivas); 

  /*  APIS PARA FUENTES */
  app.get('/fuentes.list'          , fuentes.getFuenteList); 
  app.post('/fuentes.add'          , fuentes.addFuente); 
  app.put('/fuentes.update/:id'    , fuentes.updateFuente); 
  app.get('/fuentes.activos'       , fuentes.getFuenteActivos);

  app.get('/fuentes.detalle.list'          , fuentes.getFuenteDetalleList); 
  app.post('/fuentes.detalle.add'          , fuentes.addFuenteDetalle); 
  app.put('/fuentes.detalle.update/:id'    , fuentes.updateFuenteDetalle); 
  app.get('/fuentes.detalle.activos'       , fuentes.getFuenteDetalleActivos); 
};

