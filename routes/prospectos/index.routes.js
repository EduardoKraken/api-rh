module.exports = app => {
  require("./catalagos.routes")(app)
  require("./prospectos.routes")(app)
  require("./contactsGoogle.routes")(app)
  require("./dashboardVentas.routes")(app)
  require("./dashboardMarketing.routes")(app)
  require("./activarProspectos.routes")(app)   //ANGEL RODRIGUEZ -- LINEA
  require("./organigrama.routes")(app)   //ANGEL RODRIGUEZ -- LINEA
  require("./cambiarVendedora.routes")(app)   //ANGEL RODRIGUEZ -- LINEA
  require("./cambiarProspecto.routes")(app)   //ANGEL RODRIGUEZ -- LINEA
  require("./conmutador.routes")(app) 
  require("./recomienda.routes")(app) 

};
