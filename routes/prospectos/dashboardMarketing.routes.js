module.exports = app => {
  const dashboardMarketing  = require('../../controllers/prospectos/dashboardMarketing.controllers') 
  app.post('/data.dashboard.marketing'  , dashboardMarketing.getDashboardMarketing); 
  app.post('/dashboard.marketing'       , dashboardMarketing.dashboardMarketing); 
  app.post('/marketing.contactos'       , dashboardMarketing.graficasContacto); 
  app.post('/marketing.inscritos'       , dashboardMarketing.graficasInscritos);

  // Enviar correo masivo para el convenio
  app.post('/marketing.convenio'        , dashboardMarketing.enviarcorreoConvenio); 

};
