module.exports = app => {
  const cambiarVendedora = require('../../controllers/prospectos/cambiarVendedora.controllers') // --> ADDED THIS
  app.get("/cambiar.vendedora.obtener.alumno",             cambiarVendedora.getAlumnos); 
  app.get("/cambiar.vendedora.obtener.vendedora",          cambiarVendedora.getVendedora); 
  app.post("/cambiar.vendedora.updatealumnovendedora",     cambiarVendedora.updateAlumnoVendedora);
};

//ANGEL RODRIGUEZ -- TODO