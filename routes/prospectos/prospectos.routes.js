module.exports = app => {
  // Etapas
  const prospectos      = require('../../controllers/prospectos/prospectos.controllers') 
  const familiares      = require('../../controllers/prospectos/familiares.controllers') 
  const leds            = require('../../controllers/prospectos/leds.controllers') 

  // Rutas de las apis
  app.post('/prospectos.add'      		       , prospectos.addProspectos); 
  app.get('/prospectos.list'                 , prospectos.getProspectos); 
  app.get('/prospectos.prospecto/:id'        , prospectos.getProspecto); 
  app.post('/prospectos.comentarios'         , prospectos.addComentario); 
  app.post('/formularios.comentarios'        , prospectos.addComentarioFormulario); 
  app.put('/prospectos.etapa/:id'            , prospectos.updateProspectoEtapa); 
  app.put('/formulario.etapa/:id'            , prospectos.updateFormularioEtapa); 
  app.put('/prospectos.update/:id'           , prospectos.updateProspecto); 
  app.get('/prospectos.datos/:id'            , prospectos.getDatosProspecto); 
  app.get('/formulario.datos/:id'            , prospectos.getDatosFormulario); 
  app.post('/prospectos.reasignar'           , prospectos.reasignarProspecto); 
  app.post('/prospectos.finalizar.ok'        , prospectos.finalizarProspecto); 
  app.post('/prospectos.finalizar.nok'       , prospectos.finalizarProspectoRechazo); 
  app.post('/prospectos.reactivar.cerrado'   , prospectos.activarProspectoCerrado);   //Angel Rodriguez
  app.post('/formularios.reactivar.cerrado'  , prospectos.activarProspectoCerradoRecluta);   //Angel Rodriguez
  app.get('/prospectos.grupos/:id'           , prospectos.getGrupos); 
  app.get('/prospectos.nuevos/:id'           , prospectos.getNuevosProspectos); 
  app.get('/prospecto.llamada.activa/:id'    , prospectos.getLlamadaActiva);
  app.put('/formulario.etapa.prospecto/:id'  , prospectos.updateEtapaActualFormulario); 


  app.post('/formularios.finalizar.ok'        , prospectos.finalizarProspectoFormulario); 
  app.post('/formularios.finalizar.nok'       , prospectos.finalizarProspectoRechazoFormulario); 
  app.get('/colaboradores.list'               , prospectos.getColaboradoresList); 


  // Acompañantes
  app.post('/familiar.add'                , familiares.addFamiliar); 
  app.post('/familiar.delete'             , familiares.deleteFamiliar); 
  app.post('/familiar.finalizar.ok'       , familiares.finalizarFamiliar); 
  app.post('/familiar.finalizar.nok'      , familiares.finalizarFamiliarRechazo); 
 


  // Nueva rutas de los prospectos
  app.get('/prospectos.vendedora/:tipo/:id'   , prospectos.getProspectosVendedora);
  app.get('/prospectos.reclutadora'           , prospectos.getProspectosReclutadora);
  app.get('/prospectos.vendedora.cantidad'    , prospectos.getProspectosVendedoraCantidad);

  app.get('/prospectos.all'                 , prospectos.getProspectosFinalizados);

  // Tareas
  app.post('/prospectos.tarea.add'        , prospectos.addTareaProspecto); 
  app.put('/prospectos.tarea.update'      , prospectos.updateTareaProspecto); 
  app.delete('/prospectos.tarea/:id'      , prospectos.deletedTareaProspecto);
  app.put('/prospectos.tarea.check/:id'   , prospectos.updateTareaCheck); 
  app.get('/prospectos.tarea.usuario/:id' , prospectos.getTareasUsuario);

  // Tareas Reclutadora
  app.post('/reclutadora.tarea.add'        , prospectos.addTareaReclutadora); 
  app.put('/reclutadora.tarea.update'      , prospectos.updateTareaProspecto); 
  app.delete('/reclutadora.tarea/:id'      , prospectos.deletedTareaReclutadora);
  app.put('/reclutadora.tarea.check/:id'   , prospectos.updateTareaCheckRecluta); 
  app.get('/reclutadora.tarea.usuario/:id' , prospectos.getTareasUsuarioRecluta);

  // Llamadas
  app.post('/prospectos.llamadas.add'     , prospectos.addUsuarioLlamada); 
  app.get('/prospectos.llamadas/:id'      , prospectos.getEstatusllamada); 
  app.get('/prospectos.llamadas.list/:id' , prospectos.getEstatusllamadaList); 

  app.post('/reclutadora.llamadas.add'     , prospectos.addUsuarioLlamadaRecluta); 
  // app.get('/reclutadora.llamadas/:id'      , prospectos.getEstatusllamada); 
  // app.get('/reclutadora.llamadas.list/:id' , prospectos.getEstatusllamadaList); 

  // WhatsApp
  app.post('/prospectos.whatsapp.add'     , prospectos.addProspectoWhatsApp); 

  // Puntos de la llamada
  app.post('/prospectos.puntos_llamada'   , prospectos.addPuntosLlamada); 
  app.get('/prospectos.notallamada/:id'   , prospectos.getNotasLlamada); 
  app.post('/prospectos.notallamada.add'  , prospectos.addNotaLlamada); 

  // MARKETING
  app.get('/prospectos.remaketing/:id'        , prospectos.getRemarketing);
  app.post('/prospectos.imagen.campania'      , prospectos.addImagenCampania);
  app.post('/prospectos.enviar.campania'      , prospectos.enviarCampania);

  // Inscritos
  app.post('/prospectos.inscritos'            , prospectos.getProspectosInscritos);

  // Información que se le da al usuario
  app.post('/prospectos.add.info.escuela'     , prospectos.addInfoEscuelaProspecto)

  // Eventos Llamadaaaa
  app.post('/prospectos.add.evento.llamada'    , prospectos.addEventosLlamada);
  app.post('/reclutadora.add.evento.llamada'   , prospectos.addEventosLlamadaRecluta);

  // Agregar LEDS
  app.post('/leds.add'                        , leds.addLeds);
  app.put('/leds.update'                      , leds.updateLead);
  app.get('/leds.list/:escuela'               , leds.getLeds);
  app.post('/leds.telefono'                   , leds.buscarTelefono);
  app.delete('/leds.delete/:id'               , leds.deleteLead)


  app.post('/prospectos.add.clasificacion'    , prospectos.calisificarProspecto);

  app.get('/prospectos.seguimiento/:tipo/:id' , prospectos.seguimientoProspectosNI)

  app.post('/reporte.calidad.prospectos'      , prospectos.getCalidadProspectos)
  app.post('/prospectos.respuesta.mkt'        , prospectos.addRespuestaMkt)


};    

