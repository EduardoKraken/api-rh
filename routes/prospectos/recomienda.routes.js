module.exports = app => {
  const recomienda = require('../../controllers/prospectos/recomienda.controllers') // --> ADDED THIS

  /*
    Configuración del conmutador para las vendedoras
  */
  app.get("/recomienda.list",        recomienda.getRecomienda); 
  app.get("/recomienda.inscritos",   recomienda.getRecomiendaInscritos); 
  app.post("/recomienda.add",        recomienda.addRecomienda); 
  app.put("/recomienda.update",      recomienda.updateRecomienda);
};

