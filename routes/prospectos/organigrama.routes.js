module.exports = app => {
  const organigrama = require('../../controllers/prospectos/organigrama.controllers')
  app.get("/organigrama.all",              organigrama.getOrganigrama);
  app.get("/usuarios.all.organigrama",     organigrama.getUsuariosOrganigrama);
  app.get("/organigrama.puestos",          organigrama.getPuestosOrganigrama);
  app.post("/organigrama.add",             organigrama.addOrganigrama);
  app.post("/organigrama.delete",          organigrama.deleteOrganigrama);
  app.post("/organigrama.update",          organigrama.updateOrganigrama);
  app.post("/organigrama.updatevacantes",  organigrama.updateTablaVacantes);
  app.post("/organigrama.vacante",         organigrama.addVacante);
};

//ANGEL RODRIGUEZ -- TODO