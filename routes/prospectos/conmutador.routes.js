module.exports = app => {
  const conmutador = require('../../controllers/prospectos/conmutador.controllers') // --> ADDED THIS

  /*
    Configuración del conmutador para las vendedoras
  */
  app.get("/conmutador.list",       conmutador.getConmutador); 
  app.post("/conmutador.add",       conmutador.addConmutador); 
  app.put("/conmutador.update",     conmutador.updateConmutador);


  /*
    CARGA DE ARCHIVOS DE EXCEL AL SEVIDOR PARA LAS LLAMADAS
  */ 

  app.post("/conmutador.cargar.datos",  conmutador.cargarDatos);


  /*
    REPORTE DEL CONMUTADOR
  */

  app.post("/conmutador.reporte.llamadas",  conmutador.reporteLlamadas)
};

