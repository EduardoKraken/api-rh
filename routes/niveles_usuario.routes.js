module.exports = app => {
    const nivelesUsuario = require('../controllers/niveles_usuario.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/niveles_usuario.all"                         , nivelesUsuario.getNivelesUsuario); // OBTENER
    app.get("/niveles_usuario.usuario/:idusuario"          , nivelesUsuario.getNivelesUsuarioId); // OBTENER
    app.post("/niveles_usuario.add"                        , nivelesUsuario.addNivelesUsuario); //AGREGAR USUARIO
    app.put("/niveles_usuario.update/:idniveles_usuario"   , nivelesUsuario.updateNivelesUsuario); //actualizar usuario
    app.delete("/niveles_usuario.delete/:idniveles_usuario", nivelesUsuario.deleteNivelesUsuario);

    // Niveles
    app.get("/niveles.all"                         , nivelesUsuario.getNiveles);
    app.post("/niveles.add.erpnuevo"               , nivelesUsuario.addNiveles);
    app.put("/niveles.update.erpnuevo/:idniveles"  , nivelesUsuario.updateNiveles);
};