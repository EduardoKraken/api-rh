module.exports = app => {
  const whatsapp = require('../../controllers/whatsapp/whatsapp.controllers') // --> ADDED THIS
  app.post("/whatsapp.abrir",                whatsapp.abrirWhatsApp); 
  app.post("/whatsapp.enviarmensaje",        whatsapp.enviarMensaje); 
  app.get("/whatsapp.usuarios",              whatsapp.getUsuariosWhatsapp); 
  app.post("/whatsapp.chats",                whatsapp.getChatUsuario); 
  app.post("/whatsapp.mensajes",             whatsapp.getMensajesWhatsApp); 
  app.post("/whatsapp.recibo",               whatsapp.generarReciboWhatsApp); 
  app.post("/whatsapp.enviarrecibo",         whatsapp.enviarReciboErp); 
  app.post("/whatsapp.contactos",            whatsapp.getContactosWhats); 

  app.post("/whatsapp.activo",               whatsapp.verEstatusWhatsApp); 
  app.post("/whatsapp.qr",                   whatsapp.crearQR); 
  app.post("/whatsapp.destruir",             whatsapp.destroySesion);
  app.post("/whatsapp.update",               whatsapp.updateWhatsApp);
  app.post("/whatsapp.buscar.telefono",      whatsapp.buscarWhaProspecto);
  app.post("/whatsapp.chat.telefono",        whatsapp.buscarWhatsAppTelefono);

  // WHATSAPP POR VENDEDORAS
  app.post("/whatsapp.asignar",              whatsapp.asignarVendedoraWhats);
  app.get("/whatsapp.asignados/:id",         whatsapp.getWhatsAppAsignados);

};

