module.exports = app => {
  const catalogos = require('../../controllers/lms/catalogos.controllers.js') // --> ADDED THIS
  app.get("/lms.catalogos.horarios.all",                                      catalogos.getHorariosLMS); 
  app.get("/lms.catalogos.horarios.cron", 		                                catalogos.getHorariosLMSCron); 

  app.get("/lms.catalogos.ciclos.all",                                        catalogos.getCiclosLMS); 
  app.get("/lms.catalogos.ciclos.cron",                                       catalogos.getCiclosLMSCron); 

  app.get("/lms.catalogos.grupos.all/:cicloInbi/:cicloFast",                  catalogos.getGruposLMS); 
  app.get("/lms.catalogos.grupos.cron/:cicloInbi/:cicloFast",                 catalogos.getGruposLMSCron); 
  app.put("/lms.catalogos.grupos.upudate/:id",                                catalogos.updateGruposLMSCron); 

  app.get("/lms.catalogos.usuarios.all/:cicloInbi/:cicloFast",                catalogos.getUsuariosLMS); 
  app.get("/lms.catalogos.usuarios.cron/:cicloInbi/:cicloFast",               catalogos.getUsuariosLMSCron); 
  app.get("/lms.catalogos.usuarios.exci/:cicloFast",                          catalogos.getUsuariosLMSCronExci); 
  app.post("/lms.catalogos.usuarios.adeudo",                                  catalogos.getUsuariosLMSAdeudo); 
  app.post("/lms.catalogos.usuarios.acceso",                                  catalogos.getUsuariosLMSAcceso); 
  app.post("/lms.catalogos.usuarios.certificacion",                           catalogos.getEvaluacionCertificacion); 


  app.get("/lms.catalogos.teachers.all",                                      catalogos.getTeachersLMS); 
  app.get("/lms.catalogos.teachers.cron",                                     catalogos.getTeachersLMSCron); 

  app.get("/lms.catalogos.gruposteachers.all/:cicloInbi/:cicloFast",          catalogos.getGruposTeachersLMS); 
  app.get("/lms.catalogos.gruposteachers.cron/:cicloInbi/:cicloFast",         catalogos.getGruposTeachersLMSCron); 

  app.get("/lms.catalogos.gruposalumnos.all/:cicloInbi/:cicloFast",           catalogos.getGruposAlumnosLMS); 
  app.get("/lms.catalogos.gruposalumnos.cron/:cicloInbi/:cicloFast",          catalogos.getGruposAlumnosLMSCron); 
  app.post("/lms.catalogos.gruposalumnos.all.exci",                           catalogos.getGruposAlumnosLMSExci); 
  app.post("/lms.catalogos.gruposalumnos.cron.exci",                          catalogos.getGruposAlumnosLMSCronExci); 


  /*
    ESTA RUTA SERVIRÁ PARA PODER INDICAR QUE HABRÁ VACACIONES Y AUMENTARÁ A TODAS LAS TABLAS, 14 DÍAS O DEPENDE DE LO QUE SE NECESITE
  */
  // app.post("/lms.vacaciones/:cicloInbi/:cicloFast",                            catalogos.generarVaciones);

  /*
    RUTAS PRECISAS PARA EL LMS, TODO LO DEL LMS QUE SE OCUPE EXTERNO SE ENCONTRARÁ AQUÍ :)
  */

  app.get("/lms.resultado.toefl/:escuela/:iderp",                              catalogos.getResultadoToefl);
  app.post('/lms.validar.evaluacion',                                          catalogos.validarEvaluacion);
  app.post('/lms.validar.clase.activa',                                        catalogos.validarClaseActiva);

  // Obtener las calificaciones de la clase
  app.post("/lms.catalogos.grupos.calificacion",                               catalogos.getCalificacionClase); 
  app.post("/lms.catalogos.grupos.calificacion.desglose",                      catalogos.getCalificacionClaseDesglose); 
  app.get("/lms.catalogos.usuarios",                                           catalogos.getAlumnosCicloActual); 
  app.get("/lms.ultimo.acceso/:cicloInbi/:cicloFast",                          catalogos.ultimoAccesoAlumnos); 
  app.post('/lms.cron.catalogos',                                              catalogos.cargaCatalagos )
  app.post('/lms.configurar.grupo',                                            catalogos.configurarGrupo );
  app.get('/lms.cursos.configurar',                                            catalogos.getCursosLMS )


  /*
    Esta api ayudará a limpiar todas las tablas del sistemas
    1-. Eliminar
      a) Asistencias repetidas
      b) Calificaciones repetidas
      c) Alumnos repetidos
      d) Datos repetidos de los alumnos
      e) Avatares repetidos
    2-. Actualizar
      a) el grupo en el que esta el alumno en caso de que sea necesario
  */

  app.post('/lms.soporte.limpieza',                                            catalogos.soporteLimpieza);

  /*
    APIS CRON DEL LMS para que no este fallando
    Agregar usuarios y agregarlos a un grupo
  */

  app.get("/lms.cron.alumnos",               catalogos.cronUsuariosLMS); //Daily (00:00)

  app.post('/grupos.siguientes',             catalogos.gruposSiguiente)

};

