module.exports = app => {
  const prospectos = require('../../controllers/lms/prospectoslms.controllers') // --> ADDED THIS
  app.post("/lms.prospectos.add",			      prospectos.addProspectosLMS); 

  // ejemplos de get, put, delete, post
  // app.get("/academico.certificacion.all",                     certificacion.getAccesos);
  // app.post("/academico.certificacion.add",                    certificacion.addAcceso); 
  //app.delete("/accesos.delete/:idacceso_ticket", accesos.delAccesos);
};

