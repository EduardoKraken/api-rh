module.exports = app => {
    const perfiles_usuarios = require('../../controllers/tickets/perfiles_usuarios.controllers') // --> ADDED THIS
    app.post("/perfiles_usuarios.add", perfiles_usuarios.addPerfil_usuario); // CREAR UN NUEVO GRUPO
    app.get("/perfiles_usuarios.all", perfiles_usuarios.getPerfiles_usuarios);
    app.put("/perfiles_usuarios.update/:idperfilusuario", perfiles_usuarios.putPerfiles_usuarios);
};