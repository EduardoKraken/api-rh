module.exports = app => {
    const accesos = require('../../controllers/tickets/accesos.controllers') // --> ADDED THIS
    app.post("/accesos.add", accesos.addAcceso); // CREAR UN NUEVO GRUPO
    app.get("/accesos.all", accesos.getAccesos);
    app.put("/accesos.update/:idacceso_ticket", accesos.putAccesos);
    //app.delete("/accesos.delete/:idacceso_ticket", accesos.delAccesos);
};