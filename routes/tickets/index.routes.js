module.exports = app => {
  require('./accesos.routes')(app);
	require('./areas.routes')(app);
	require('./auxiliares.routes')(app);
	require('./jefe_area.routes')(app);
	require('./ticket.routes')(app);
	require('./historial_ticket.routes')(app);
	require('./usuarios.routes')(app);
	require('./perfiles.routes')(app);
	require('./perfiles_usuarios.routes')(app);
	require('./reportes.routes')(app);
	require('./app-ri-montos/app-ri-montos.routes')(app);
	require('./documentos.routes')(app); 
};