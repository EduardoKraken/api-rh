module.exports = app => {
  const areas = require('../../controllers/tickets/areas.controllers') // --> ADDED THIS
  app.post("/areas.add", areas.addArea); // CREAR UN NUEVO GRUPO
  app.get("/areas.all", areas.getAreas);
  app.put("/areas.update/:idareas_ticket", areas.putAreas);

  app.get("/sucursales.all" , areas.getSucursales);
};