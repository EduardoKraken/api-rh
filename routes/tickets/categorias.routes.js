module.exports = app => {
  const categorias = require('../../controllers/tickets/categorias.controllers') // --> ADDED THIS
  app.post("/categorias.x.usuario", categorias.catego_x_usuario);
  app.put("/put.datos.categoria/:idcatego", categorias.put_datos_catego);
  app.delete("/delete.categoria/:idcatego", categorias.delete_categoria);
  app.get("/categorias.x.usuario/:idusuariosweb", categorias.categorias_x_usuario)
};