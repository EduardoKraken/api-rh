module.exports = app => {
    const Auxiliares = require('../../controllers/tickets/auxiliares.controllers') // --> ADDED THIS
    app.post("/Auxiliares.add", Auxiliares.addAuxiliar); // CREAR UN NUEVO GRUPO
    app.get("/Auxiliares.all", Auxiliares.getAuxiliares);
    app.get("/auxiliares.porarea/:idauxi_area", Auxiliares.getAuxiliares_PorArea);
    app.put("/Auxiliares.update/:idauxi_area", Auxiliares.putAuxiliares);
    //app.delete("/Auxiliares.delete/:idauxi_area", Auxiliares.delAuxiliares);
};