module.exports = app => {
  const tickets = require('../../controllers/tickets/tickets.controllers') // --> ADDED THIS
  app.post("/tickets.add",                             tickets.addTicket); // CREAR UN NUEVO GRUPO

  app.get("/tickets.all",                              tickets.getTickets);
  app.get("/tickets.all.rh",                           tickets.getTicketsRH);
  app.get("/tickets.area/:id",                         tickets.getTickets_area);
  app.get("/tickets.auxiliar/:idauxi_area",            tickets.getTickets_auxiliar_area);
  app.put("/tickets.update/:idticket",                 tickets.putTickets);
  app.put("/tickets.updateFolio/:idticket",            tickets.putTickets_folio);
  app.put("/tickets.update.asignacion/:idticket",      tickets.putTicketAsignacion);
  app.put("/tickets.update.asignacion.auxi/:idticket", tickets.putTicketAsignacionAuxi);
  app.put("/tickets.update.estatus/:idticket",         tickets.putTicketEstatus);
  app.get("/tickets.get.usuario.lms/:id_usuario",      tickets.getTicketsUsuarioLMS);
  app.get("/tickets.get.usuario.erp/:id_usuario",      tickets.getTicketsUsuarioERP);
  app.get("/tickets.usuarios.getAll/:id_usuario",      tickets.getUsuariosALL);
  app.get("/tickets.usuarios.getCargo/:id_usuario",    tickets.getCargoUsuario);
  app.get("/tickets.all.id/:id_ticket",                tickets.getTicketsPorID);
  
  // Obtener las notas del ticket
  app.get("/tickets.notas/:idticket",                  tickets.getNotas);
  app.post("/tickets.nota",                            tickets.addNota);
  app.post("/tickets.update.visorrh",                  tickets.updateVisorRh);

  app.post('/tickets.subir.evidencia',                 tickets.subirEvidenciaTicket)
};