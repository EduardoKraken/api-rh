module.exports = app => {
    const Jefe_areas = require('../../controllers/tickets/jefe_area.controllers') // --> ADDED THIS
    app.post("/Jefe_areas.add", Jefe_areas.addJefe_area); // CREAR UN NUEVO GRUPO
    app.get("/Jefe_areas.all", Jefe_areas.getJefe_areas);
    app.get("/Jefe_areas/:idjefe_area", Jefe_areas.getJefe_areas_PorArea);
    app.put("/Jefe_areas.update/:idjefe_area", Jefe_areas.putJefe_areas);
    //app.delete("/Jefe_areas.delete/:idjefe_area", Jefe_areas.delJefe_areas);
};