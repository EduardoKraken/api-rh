module.exports = app => {
    const reportes = require('../../controllers/tickets/reportes.controllers') // --> ADDED THIS
    app.get("/reportes.total_tickets", reportes.getTotalTickets);
    app.get("/reportes.total_tickets_inbi", reportes.getTotalTicketsINBI);
    app.get("/reportes.total_tickets_fast_english", reportes.getTotalTicketsFASTENGLISH);
    app.get("/reportes.total_tickets_area", reportes.getTicketsAreaTiempo);
    app.get("/reportes.total_tickets_estatus1", reportes.getEstatus1);
    app.get("/reportes.total_tickets_estatus2", reportes.getEstatus2);
    app.get("/reportes.total_tickets_estatus3", reportes.getEstatus3);
    app.get("/reportes.total_tickets_estatus4", reportes.getEstatus4);
    app.get("/reportes.total_tickets_estatus5", reportes.getEstatus5);
    app.get("/reportes.total_tickets_estatus6", reportes.getEstatus6);
    app.get("/reportes.total_tickets_estatus7", reportes.getEstatus7);

    
    app.get("/reportes.total_ticket_por_area", reportes.getTicketsPorArea);


    
    app.get("/reportes.total_tickets_estatus1_area/:id", reportes.getEstatus1_area);
    app.get("/reportes.total_tickets_estatus2_area/:id", reportes.getEstatus2_area);
    app.get("/reportes.total_tickets_estatus3_area/:id", reportes.getEstatus3_area);
    app.get("/reportes.total_tickets_estatus4_area/:id", reportes.getEstatus4_area);
    app.get("/reportes.total_tickets_estatus5_area/:id", reportes.getEstatus5_area);
    app.get("/reportes.total_tickets_estatus6_area/:id", reportes.getEstatus6_area);
    app.get("/reportes.total_tickets_estatus7_area/:id", reportes.getEstatus7_area);

};