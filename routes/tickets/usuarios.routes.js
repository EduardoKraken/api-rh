module.exports = app => {
    const usuarios = require('../../controllers/tickets/usuarios.controllers') // --> ADDED THIS
    app.get("/usuarios.all.erp", usuarios.getUsuariosERP);
    app.post("/agregar.registro",      usuarios.addRegistro);
    app.get("/traer.registro",         usuarios.getRegistro);
    app.put("/actualizar.registro/:id", usuarios.updateRegistro);
};