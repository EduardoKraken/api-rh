module.exports = app => {
    const appRIMontos = require('../../../controllers/tickets/app-ri-montos/app-ri-montos.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    // Cardar los selectores
    // planteles
    app.get("/app.planteles.all", appRIMontos.getPlanteles); // OBTENER

    // ciclos
    app.get("/app.ciclos.all", appRIMontos.getCiclos); // OBTENER
    
    // Apis para obtener los datos de RIMONTOS
    app.get("/app.cantactual/:id/:idfast",    appRIMontos.getCantActual); // obtener los alumnos inscritos actualmente
    app.get("/app.cantsiguiente/:id/:idfast", appRIMontos.getCantSiguiente); // obtener los alumnos inscritpos para el siguiente

};