module.exports = app => {
    const perfiles = require('../../controllers/tickets/perfiles.controllers') // --> ADDED THIS
    app.post("/perfiles.add", perfiles.addPerfil); // CREAR UN NUEVO GRUPO
    app.get("/perfiles.all", perfiles.getPerfiles);
    app.put("/perfiles.update/:idperfil", perfiles.putPerfiles);
};