module.exports = app => {
    const departamentos = require('../controllers/departamentos.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/departamentos.all"                    , departamentos.getDepartamentos); // OBTENER
    app.post("/departamentos.add"                   , departamentos.addDepartamento); //AGREGAR USUARIO
    app.put("/departamentos.update/:iddepartamento" , departamentos.updateDepartamento); //actualizar usuario
    app.get("/departamentos.tickets"                , departamentos.listDepartamentos); // OBTENER


};