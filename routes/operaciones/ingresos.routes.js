module.exports = app => {
  const ingresos = require('../../controllers/operaciones/ingresos.controllers') 
  // General
  app.get("/ingresos.ciclo/:id"           , ingresos.ingresosPorCiclo); 
  app.post("/ingreso.eliminar"            , ingresos.eliminarIngreso); 
  app.post("/ingreso.update.metodo"       , ingresos.updateIngresoMetodo); 
  // app.post("/datos.fiscales.add"       , ingresos.addDatosFiscales); 
  // app.put("/datos.fiscales.update/:id" , ingresos.updateDatosFiscales); 

  app.post('/reporte.ajustepagos'         , ingresos.reporteAjustePagos)
  app.get('/reporte.ajustepagos/:id'      , ingresos.getReporteAjustePagosUsuario)
  app.delete("/reporte.eliminar/:id"      , ingresos.eliminarReporte); 
  app.post("/reporte.padoduplicado"       , ingresos.getPagosDuplicados); 
  app.post("/reporte.montopago"           , ingresos.updateMontoPago); 
  app.post("/reporte.montodescuento"      , ingresos.updateMontoDescuento); 
  app.post("/reporte.bajaalumno"          , ingresos.reporteBajaAlumno); 
};

