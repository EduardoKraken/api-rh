module.exports = app => {
  const alumnos = require('../../controllers/operaciones/alumnos.controllers') // const para utilizar nuestro controllador--> ADDED THIS
  app.post("/alumnos.add"              , alumnos.addAlumnoErp);
  app.post("/alumnos.add.hermanos"     , alumnos.addAlumnosHermanos);
  app.post("/alumnos.tutor.add"        , alumnos.addAlumnoTutor);
  app.get("/alumnos.videos"            , alumnos.getVideosAlumnos)
};