module.exports = app => {
    const calificaciones = require('../../controllers/operaciones/calificaciones.controllers') 
    // General
    app.post("/calificaciones.alumno"         , calificaciones.getCalificacionesAlumno); 
    app.post("/habilitar.plataforma"          , calificaciones.habilitarPlataforma); 
    app.post("/actualizar.calificacion"       , calificaciones.actualizarCalificacion); 
    app.post("/calificaciones.alumno.estatus" , calificaciones.getEstatusAlumno); 

};

