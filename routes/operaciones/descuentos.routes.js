module.exports = app => {
	const descuentos = require('../../controllers/operaciones/descuentos.controllers') 

	/*
		API PARA APLICAR Y VER DESCUENTOS ESPECIALES
	*/

	app.post("/descuentos.alumnos.add"    , descuentos.addDescuentoAlumno); 
	app.post("/descuentos.grupos.add"     , descuentos.addDescuentoGrupo); 
	app.get("/descuentos.alumnos/:id"     , descuentos.getDescuentosAlumno); 
	app.get("/descuentos.all"             , descuentos.getDescuentoAll); 
	app.get("/descuentos.alumnos"         , descuentos.obtenerAlumnos); 
	app.get("/descuentos.grupos"          , descuentos.obtenerGruposDescuento); 
	app.put("/descuentos.eliminar/:id"    , descuentos.eliminarDescuento); 
};

