module.exports = app => {
  const actas = require('../../controllers/operaciones/actas.controllers') 
  // General
  app.get("/actas.list"         , actas.getActaAdministrativaList); 
  app.get("/actas.usuario/:id"  , actas.getActaAdministrativaUsuario); 
  app.post("/actas.add"         , actas.addActaAdministrativa); 
  app.put("/actas.update/:id"   , actas.updateActaAdministrativa); 


  app.get("/faltas.admin.list"         , actas.getFaltasAdminList); 
  app.post("/faltas.admin.add"         , actas.addFaltasAdmin); 
  app.put("/faltas.admin.update/:id"   , actas.updateFaltasAdmin); 


  app.get("/sanciones.admin.list"         , actas.getSancionesAdminList); 
  app.post("/sanciones.admin.add"         , actas.addSancionesAdmin); 
  app.put("/sanciones.admin.update/:id"   , actas.updateSancionesAdmin); 
};

