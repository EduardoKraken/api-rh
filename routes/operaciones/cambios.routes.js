module.exports = app => {
    const cambios = require('../../controllers/operaciones/cambios.controllers') 

    // General
    app.get("/cambios.tipo.cambio"                , cambios.getTipoCambios); 

    // Recepcionista
    app.post("/cambios.alumnos.grupos"            , cambios.getAlumnoxGrupo); 
    app.post("/cambios.grupos.ciclo"              , cambios.cambiosGruposxCiclo); 
    app.post("/cambios.nuevo.grupos.ciclo"        , cambios.cambiosNuevoGruposxAlumno); 
    app.post("/cambios.add"                       , cambios.addCambios); 
    app.get("/cambios.usuario/:idusuario"         , cambios.getCambiosUsuario);
    app.get("/cambios.grupo.usuario/:idusuario"   , cambios.getGruposUsuario);

	// Nivel superior 
    app.get("/cambios.all"                        , cambios.getCambiosAll); 
    app.put("/cambios.update/:id"                 , cambios.updateCambios); 
    app.post("/cambios.mensaje"                   , cambios.cambiosMensaje); 
    app.get("/cambios.mensaje/:id"                , cambios.cambiosGetMensaje); 
    
    /*
        alumnos cambio de grupo 
    */
    app.post("/cambios.grupo"                            , cambios.addCambioGrupo); 
    app.post("/cambios.nuevo.grupos.ciclo.prueba"        , cambios.cambiosNuevoGruposxAlumnoPrueba); 
    app.post("/cambios.alumnos.grupos.prueba"            , cambios.getAlumnoxGrupoPrueba); 
    app.get("/cambios.grupo.usuario.prueba/:idusuario"   , cambios.getGruposUsuarioPrueba);


};

