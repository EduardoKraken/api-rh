module.exports = app => {
  const facturas = require('../../controllers/operaciones/facturas.controllers') 
  // General
  app.get("/alumnos.facturas.list"     , facturas.getDatosAlumnos); 
  app.get("/facturas.municipios"       , facturas.getMunicipios); 
  app.get("/facturas.regimen.fiscal"   , facturas.getRegimenFiscal); 
  app.get("/facturas.list"             , facturas.getDatosFiscales); 

  app.post("/datos.fiscales.add"       , facturas.addDatosFiscales); 
  app.put("/datos.fiscales.update/:id" , facturas.updateDatosFiscales); 

  app.get("/facturas.solicitadas"      , facturas.getFacturasSolicitadas); 
  app.get("/facturas.solicitadas/:id"  , facturas.getFacturasSolicitadasUsuario); 
  app.get("/uso.cfdi"                  , facturas.getUsoCFDI); 
  app.get("/facturas.sucursales"       , facturas.getSucursalesFactura); 
  app.get("/facturas.ciclos"           , facturas.getCiclosFactura); 

  app.post("/solicitar.factura"        , facturas.solicitarFactura); 
  app.post("/factura.archivos/:id"     , facturas.subirFactura); 
  app.put("/factura.eliminar/:id"      , facturas.eliminarFactura); 




};

