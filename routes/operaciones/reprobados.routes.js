module.exports = app => {
	const reprobados = require('../../controllers/operaciones/reprobados.controllers') 
	app.get("/operaciones.reprobados.list/:cicloInbi/:cicloFast"        , reprobados.getReprobados); 
	app.post("/operaciones.reprobados.notas"                            , reprobados.getNotasAlumnos); 
	app.post("/operaciones.reprobados.notas.add"                        , reprobados.addNotasAlumnos); 
	app.post("/operaciones.reprobados.teachers"                         , reprobados.getGruposTeacher); 
	app.get("/operaciones.alumno.proximopago/:idinbi/:idfast"           , reprobados.getAlumnoProximoPago); 


};

