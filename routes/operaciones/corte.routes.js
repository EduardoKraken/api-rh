module.exports = app => {
  const corte = require('../../controllers/operaciones/corte.controllers') 
  // General
  app.post("/corte.sucursal"            , corte.getCorteSucursal); 
  app.post("/movimientos.corte"         , corte.getMovimientosCorte);
  app.post("/corte.recibo"              , corte.generarRecibo); 
  app.get('/ingresos.reporte.ciclo/:id' , corte.getReporteIngresosCiclo);
  app.post("/ingreso.clave.rastreo"     , corte.updateClaveRastreo); 
  app.post("/ingreso.folio.operacion"   , corte.updateFolioOperacion); 
  app.post("/ingreso.cuenta"            , corte.updateCuentaIngreso); 
  app.post("/ingreso.aceptado"          , corte.updateAceptado); 

};

