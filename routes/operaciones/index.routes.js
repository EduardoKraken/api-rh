module.exports = app => {
  require("./cambios.routes")(app);
	require("./lms.routes")(app);
	require("./contratos.routes")(app);
	require("./reprobados.routes")(app);
	require("./comentarios.routes")(app);
	require("./inscripciones.routes")(app);
	require("./encuestas.routes")(app);
	require("./calificaciones.routes")(app);
	require("./alumnos.routes")(app);
	require("./riesgo.routes")(app);
	require("./permisos.routes")(app);
	require("./rh.routes")(app);
	require("./facturas.routes")(app);
	require("./corte.routes")(app);
	require("./descuentos.routes")(app);
	require("./ingresos.routes")(app);
	require("./becas.routes")(app);
	
};