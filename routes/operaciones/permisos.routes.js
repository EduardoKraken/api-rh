module.exports = app => {
    const permisos = require('../../controllers/operaciones/permisos.controllers') 
    // CONSULTAS
    app.get("/permisos.usuario/:id"    , permisos.getPermisosUsuario); 
    app.get("/permisos.area/:id"       , permisos.getPermisosArea); 
    app.get("/permisos.rh"             , permisos.getPermisosRh);
    app.post("/permisos.agregar"       , permisos.addPermiso) 
    app.post("/permisos.aceptar.rh"    , permisos.updatePermisoRH) 
    app.post("/permisos.aceptar.jefe"  , permisos.updatePermisoJefe) 
};

