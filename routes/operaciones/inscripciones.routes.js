module.exports = app => {
  const inscripciones = require('../../controllers/operaciones/inscripciones.controllers') // const para utilizar nuestro controllador--> ADDED THIS
  
  app.get("/inscripciones.list/:ciclo"             , inscripciones.getinscripciones);
  app.get("/inscripciones.alumno/:id"              , inscripciones.getAlumnoInscrito);
  app.get("/inscripciones.alumno.grupos/:id"       , inscripciones.getGruposAlumnoInscrito);
  app.get("/inscripciones.becas"                   , inscripciones.getBecasInscripcion);
  app.get("/inscripciones.becas.alumno/:id"        , inscripciones.getBecasAlumno);
  app.get("/inscripciones.alumnos"                 , inscripciones.getListadoAlumnos);
  app.get("/inscripciones.alumnos/:id"             , inscripciones.getListadoAlumnosId);  
  app.post("/inscripciones.grupo.alumno"           , inscripciones.getGruposAlumno);
  app.post("/inscripcion.grupo.alumno"             , inscripciones.getGrupoAlumno);

  app.get("/inscripciones.alumno.becado/:id"       , inscripciones.alumnosEsBecado);

  app.post("/inscripciones.calcular.precio"        , inscripciones.calcularPrecioAlumno);

  // CALCULAR COSTO DE LA FACTURA
  app.post("/inscripciones.calcular.factura"       , inscripciones.calcularFacturaAlumno);

  app.post("/registrar.grupo.alumno"               , inscripciones.registrarAlumnoGrupo);
  app.post("/registrar.grupo.hermanos"             , inscripciones.registrarAlumnoGrupoHermanos);
  app.post("/registrar.transferencia"              , inscripciones.realizarTransferencia);

  app.post("/grabar.comprobante"                   , inscripciones.grabarComprobante)
  app.get("/grupos.disponibles/:id"                , inscripciones.getGruposDisponibles);

  app.post("/validar.hermanos"                     , inscripciones.validarHermanos);
  app.post("/validar.tipoalumno"                   , inscripciones.validarTipoAlumno);

  app.post('/actualizar.correo.alumno'             , inscripciones.updateCorreoAlumno);

  app.post("/pago.grupo"                           , inscripciones.getPagoGrupo);
  app.post("/inscripciones.matricula"              , inscripciones.getMatriculaViejita)
  app.post("/inscripciones.comparaciones"          , inscripciones.getIngresosComparaciones)

  app.get("/inscripciones.prospectos"              , inscripciones.getListadoProspectos);

  app.post("/inscripciones.comparaciones.ciclo"    , inscripciones.getIngresosComparacionesCiclo);

  app.post("/inscripciones.getcomparacioncicloerp" , inscripciones.getComparacionCicloERP);

  app.post("/inscripciones.generar.recibo"         , inscripciones.generarRecibo);
  app.post("/inscripciones.correo.bienvenida"      , inscripciones.enviarCorreoBienvenida);

  app.get("/tutor.alumno/:id"                      , inscripciones.getTutorAlumno);   //Angel Rodriguez

  app.get("/precios.alumno/:id"                    , inscripciones.getPreciosAlumnos);   //Angel Rodriguez

  app.get("/all.precios"                           , inscripciones.getAllPrecios);   //Angel Rodriguez

  app.post("/update.precio.alumno"                , inscripciones.updatePrecioAlumno);  //Angel Rodriguez


  // app.post("/correo.induccion"                    , inscripciones.sendCorreoInduccion);  //Angel Rodriguez


};
