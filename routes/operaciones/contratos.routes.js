module.exports = app => {
	const contratos = require('../../controllers/operaciones/contratos.controllers') 
	app.post("/operaciones.contrato.add"        				, contratos.addContrato); 
	app.post("/operaciones.contrato.valida"     				, contratos.validaContrato); 
	app.post("/operaciones.contrato.actualiza.datos"    , contratos.actualizaDatosContrato); 
	app.get("/operaciones.contrato.all"         				, contratos.getContratos); 
	// app.post("/contratos.add"                       	, contratos.addCambios); 
};

