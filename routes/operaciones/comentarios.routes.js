module.exports = app => {
  const comentarios = require('../../controllers/operaciones/comentarios.controllers') 

  // Recepcionista
  app.get("/operaciones.alumnos"                    , comentarios.getAlumnosCicloActual);
  app.post("/operaciones.comentario"      					, comentarios.addComentarioAlumno);
  app.get("/operaciones.comentario/:id/:escuela"   	, comentarios.getComentarioAlumno);

  // Teachers
  app.post("/operaciones.alumnos.teachers"      		, comentarios.getAlumnosTeacher);

  

};

