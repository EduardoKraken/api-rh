module.exports = app => {
  const riesgo = require('../../controllers/operaciones/riesgo.controllers') // const para utilizar nuestro controllador--> ADDED THIS
  app.get("/riesgo.ciclos.fast"        , riesgo.getCiclosFast);
  app.post("/riesgo.alumnos.registro"  , riesgo.getRiesgoAlumnoRegistro);
  app.post("/riesgo.alumnos.pago"      , riesgo.getAlumnosRiesgoPago);
  app.post("/riesgo.alumnos"           , riesgo.getAlumnosRiesgo);
  app.post("/riesgo.calificaciones"    , riesgo.getRiesgoAlumno);
  app.post("/riesgo.ejercicios"        , riesgo.getRecursoFalta);
  app.post("/riesgo.puntos"            , riesgo.getPuntosAlumnos);
  app.post("/subir.calificaciones"     , riesgo.subirCalificaciones);

  /*
    Seguimiento alumnos RI
    1-. El alumno entra a la plataforma? 
    2-. El alumno recibe mensajes contantes?
    3-. El alumno participa?
    4-. El alumno hace ejercicios
    5-. El alumno tiene algun dato vacio?
  */
  app.post('/riesgo.seguimiento.ri'    , riesgo.panelSeguimientoRI );
};