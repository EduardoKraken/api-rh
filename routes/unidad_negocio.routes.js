module.exports = app => {
    const unidad_negocio = require('../controllers/unidad_negocio.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/unidad_negocio.all", unidad_negocio.all_unidad_negocio); // OBTENER
    app.post("/unidad_negocio.add", unidad_negocio.add_unidad_negocio); //AGREGAR USUARIO
    app.put("/unidad_negocio.update/:idunidad_negocio", unidad_negocio.update_unidad_negocio); //actualizar usuario

};