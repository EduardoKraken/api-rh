module.exports = app => {

  const kpi = require('../../controllers/kpi/kpi.controllers') 

  /******************************************************************************************/
  app.post("/kpi.inscripciones.general"                  , kpi.kpiInscripcionesGeneral); 
  app.post("/kpi.inscripciones.alumnos.ciclo.rango"      , kpi.kpiInscripcionesAlumnosCicloRango);
  app.post("/kpi.inscripciones.alumnos.rango.fast"       , kpi.kpiInscripcionesAlumnosRangoFAST);
  app.post("/kpi.inscripciones.alumnos.rango.inbi"       , kpi.kpiInscripcionesAlumnosRangoINBI);
  app.post("/kpi.inscripciones.alumnos.dia.fast"         , kpi.kpiInscripcionesAlumnosDiaFAST);
  app.post("/kpi.inscripciones.alumnos.dia.inbi"         , kpi.kpiInscripcionesAlumnosDiaINBI);
  app.get("/kpi.inscripciones.total.fast/:actual"        , kpi.kpiInscripcionesTotalFAST);
  app.get("/kpi.inscripciones.total.inbi/:actual"        , kpi.kpiInscripcionesTotalINBI);

  app.post("/kpi.inscripciones.porciclo"                 , kpi.kpiInscripcionesPorCiclo)
  app.post("/kpi.inscritos.induccion"                    , kpi.kpiInscripcionesInduccion)


  // 
  /******************************************************************************************/
  app.post("/kpi.inscripciones.ciclo.dia.fast"          , kpi.kpiInscripcionesCicloDiaFAST);
  app.post("/kpi.inscripciones.ciclo.dia.inbi"          , kpi.kpiInscripcionesCicloDiaINBI);

  app.post("/kpi.inscripciones.ciclo.rango.fast"        , kpi.kpiInscripcionesCicloRangoFAST);
  app.post("/kpi.inscripciones.ciclo.rango.inbi"        , kpi.kpiInscripcionesCicloRangoINBI);

  // planteles
  /*******************************************************************************************/
  app.get("/kpi.planteles.all",     kpi.getPlanteles);
  app.get("/kpi.ciclos.all",        kpi.getCiclos);
  app.get("/kpi.ciclos.all.exci",   kpi.getCiclosExci);
  app.get("/kpi.ciclos.induccion",  kpi.getCiclosInduccion);
  
  // Apis p los datos de RIMONTOS
  app.post("/kpi.cantactual",                         kpi.getCantActual);
  app.post("/kpi.cantactual.montos",                  kpi.getCantActualMontos);
  app.get("/kpi.cantsiguiente/:id/:idfast",           kpi.getCantSiguiente);
  app.get("/kpi.cantsiguiente.avance/:id/:idfast",    kpi.getCantSiguienteAvance);

  // Teens
  app.get("/kpi.cantactual.teens/:id/:idfast",           kpi.getCantActualTeens);
  app.get("/kpi.cantsiguiente.teens/:id/:idfast",        kpi.getCantSiguienteTeens);
  
  // TEACHERS
  app.post("/kpi.cantactual.teachers",                   kpi.getCantActualTeachers);

  // Vendedora
  /*******************************************************************************************/
  app.post("/kpi.inscripciones.vendedora.ciclo.general"            , kpi.kpiInscripcionesVendedoraCicloGeneral);
  app.post("/kpi.inscripciones.vendedora.ciclo.rango.general"      , kpi.kpiInscripcionesVendedoraCicloRangoGeneral);

  app.post("/kpi.inscripciones.vendedora.rango.general"            , kpi.kpiInscripcionesVendedoraRangoGeneral);


  app.post("/kpi.inscripciones.vendedora.dia.fast"                 , kpi.kpiInscripcionesVendedoraDiaFAST);
  app.post("/kpi.inscripciones.vendedora.dia.inbi"                 , kpi.kpiInscripcionesVendedoraDiaINBI);

  // KPI general
  /*******************************************************************************************/
  // Ventas por sucursal
  app.get("/kpi.ni.porciclo.general.FAST/:ciclo"      , kpi.kpiNiPorCicloGeneralFAST)
  app.get("/kpi.ni.porciclo.general.INBI/:ciclo"      , kpi.kpiNiPorCicloGeneralINBI)
  app.get("/kpi.ni.porciclo.general.TEENS/:ciclo"     , kpi.kpiNiPorCicloGeneralTEENS)
  
  app.get("/kpi.ni.vendedora.ciclo.fast/:ciclo"       , kpi.kpiNiVendedoraCicloFAST);
  app.get("/kpi.ni.vendedora.ciclo.inbi/:ciclo"       , kpi.kpiNiVendedoraCicloFAST);
  app.get("/kpi.ni.vendedora.ciclo.teens/:ciclo"      , kpi.kpiNiVendedoraCicloTEENS);

  /**********************************************************************************************/
  // RI por grupos
  app.get("/kpi.ri.grupos/:inbi/:fast/:sigInbi/:sigFast"    , kpi.kpiRiGrupos);


  app.get("/kpi.horarios/:cicloFast/:cicloInbi"             , kpi.getHorariosClases);
  app.post("/kpi.clases.hora.dia"                           , kpi.getClasesHoraDia);
  app.post("/kpi.clases.hora.dia.update"                    , kpi.getClasesHoraDiaUpdate);
  app.post("/kpi.clases.hora.dia.reporte"                   , kpi.getClasesHoraDiaReporte);
  app.get("/kpi.teacher.activos/:cicloFast/:cicloInbi"      , kpi.getTeacherActivos);
  app.post("/kpi.clases.hora.dia.reporte.teacher"           , kpi.getClasesHoraDiaReporteTeacher);
  app.post("/kpi.clases.teachers"                           , kpi.getClasesReporteTeacher);


  app.post("/nuevas.matriculas"                            , kpi.getNuevasMatriculas);
  app.post("/nuevas.matriculas.ciclo"                      , kpi.getNuevasMatriculasCiclo);

  // KPI para ver los inscritos por ciclo y por MMKT
  app.post("/inscritos.marketing.recpcionista"             , kpi.getInscritosMarketingRecepcion)

  app.post("/reporte.semanal.ventas"                       , kpi.reporteSemanalVentas);

  app.get("/reporte.vendedoras"                            , kpi.getReporteVendedora);
  app.post("/reporte.semanal.vendedora"                    , kpi.reporteSemanalVendedora);
  app.post("/reporte.semanal.vendedora.zip"                , kpi.reporteSemanalVendedoraZip);
  app.get("/reporte.anios.ciclos"                          , kpi.getAniosCiclos);
  app.post("/reporte.historial.ventas"                     , kpi.getHistorialVentas);
  app.post("/reporte.historial.ventas.plantel"             , kpi.getHistorialVentasPlantel);
  app.post("/reporte.historial.ventas.plantel.ri"          , kpi.getHistorialVentasPlantelRi);
  app.post("/reporte.historial.ventas.plantel.ri.fecha"    , kpi.getHistorialVentasPlantelRiFecha);
  app.post("/reporte.historial.ventas.fecha"               , kpi.getHistorialVentasFecha);
  app.post("/reporte.historial.ventas.plantel.fecha"       , kpi.getHistorialVentasPlantelFecha);

  app.post("/kpi.getrifecha",                              kpi.getRIFecha);
};