module.exports = app => {
  const reporteVentas          = require('../../controllers/kpi/reporteventas.controllers') 

  app.post('/reporte.ventas.ciclos'    , reporteVentas.getReporteVentasCiclos); 
  app.post('/reporte.ventas.fechas'    , reporteVentas.getReporteVentasFechas); 

};
