module.exports = app => {
  const dashboardKpi          = require('../../controllers/kpi/dashboardKpi.controllers') 

  app.get('/data.dashboard.kpi'          , dashboardKpi.getDashboardKpiGeneral); 
  app.post('/numero.vacantes.add'        , dashboardKpi.addNumeroVacantes); 
  app.get('/numero.vacantes.list'        , dashboardKpi.getnumeroVacantesList); 
  app.get('/metas.list'                  , dashboardKpi.getMetasList)
  app.put('/metas.update'                , dashboardKpi.updateMeta)

  //******************************************
  app.get('/metas.semana.list'           , dashboardKpi.getMetasSemanaList)
  app.put('/metas.semana.update'         , dashboardKpi.updateMetaSemana)

  // Obtener mis colaboradores
  app.get('/resultados.usuarios/:id'     , dashboardKpi.getResultadosUsuario) 
  
  // Agregar mi resutlado semanal
  app.post('/resultados.add'             , dashboardKpi.addResultadoUsuario) 
  
  // Obtener resultado del usuario en la semana seleccionada
  app.post('/resultados.resultado'       , dashboardKpi.getResultadoUsuario) 
  
  // Obtener todo el historial de resultados del usuario selecionado
  app.get('/resultados.historial/:id'    , dashboardKpi.getResultadoHistorial) 

  // Actualizar los resultados
  app.put('/resultados.update/:id'       , dashboardKpi.updateResultado) 

};
