module.exports = app => {
  require("./kpi.routes")(app)
  require("./reportes.routes")(app);
  require("./ingresos_egresos.routes")(app);
  require("./dashboardKpi.routes")(app)
  require("./dashboardPersonal.routes")(app)
  require("./ri_montos.routes")(app)
  require("./registro_actividades.routes")(app)
  require("./kpi_diario.routes")(app)
  require("./reporteventas.routes")(app)
};