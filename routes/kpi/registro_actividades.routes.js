module.exports = app => {
  const registro_actividades          = require('../../controllers/kpi/registro_actividades.controllers') 

  // Rutas de las apis
  app.get('/registro.actividades.list'        , registro_actividades.getRegistroActividades); 
  app.post('/registro.actividades.add'        , registro_actividades.addRegistroActividades);
  app.get('/registro.actividades.usuario/:id' , registro_actividades.getRegistroActividadesUsuario); 
  app.post('/registro.mi.actividad'           , registro_actividades.addRegistroMiActividades);


  app.post('/registro.vacantes'               , registro_actividades.addVacantesDisponibles); 
  app.get('/vacantes.disponibles'             , registro_actividades.getVacantesDisponibles); 

};

